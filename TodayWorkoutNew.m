//
//  TodayWorkoutNew.m
//  gyminutes
//
//  Created by Mayank Verma on 5/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "TodayWorkoutNew.h"
#import "MuscleListTVC.h"
#import "RecordExercise.h"
#import "RecordExerciseMenu.h"
#import "NavMenu.h"
#import "ExerciseSummary.h"
#import <Crashlytics/Crashlytics.h>
#import "RecWorkout.h"
#import "TodayWorkoutItems.h"
#import "EditTodayWorkout.h"
#import "RecordCardio.h"
#import "RecordCardioFast.h"
@interface TodayWorkoutNew () {NSArray *summaryItems, *allExerciseForCalendar;
    NSMutableArray *exerciseList;
    NSMutableDictionary *calendarColorDict;
    WorkoutList *exerciseSelected;
    ExerciseMetaInfo *exMetaInfo;
    NSString *date;
    NSString *currentDate;

    UIRefreshControl *refreshControl;
    NSArray *allExerciseList;
    UISearchBar *searchBar;
    UIView *additionOptionView;
    BOOL showWorkoutCompleteBtn, calendarFlag;

    // new today wrkout light weight
    NSMutableArray *cardsArray;
    NSMutableArray *dataArr, *heightArr;
    float lblHeight;
    NYSegmentedControl *foursquareSegmentedControl;
    
}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation TodayWorkoutNew

@synthesize collectionView, recordDate, coachMarksView;
-(void) viewDidLoad {
    [super viewDidLoad];
    lblHeight = 20;
    calendarFlag = false;
    
    currentDate = date = [Utilities getCurrentDate];
    [self.datepicker fillDatesFromDate:[NSDate dateWithYear:2015 month:01 day:01] toDate:[NSDate dateWithYear:2020 month:12 day:31]];
    //[self.datepicker selectDate:[dateFormat dateFromString:newDate]];
    [self.datepicker selectDate:[NSDate date]];
    [self.datepicker addTarget:self action:@selector(updateSelectedDate) forControlEvents:UIControlEventValueChanged];

    [self getCalendarData];
    
    foursquareSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"WORKOUT", @"SUMMARY"]];
    foursquareSegmentedControl.titleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    foursquareSegmentedControl.backgroundColor = FlatWhiteDark;
    foursquareSegmentedControl.borderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorInset = 2.0f;
    foursquareSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    [foursquareSegmentedControl sizeToFit];
    foursquareSegmentedControl.cornerRadius = 5.0f;
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];

#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0
    foursquareSegmentedControl.usesSpringAnimations = YES;
#endif
    [foursquareSegmentedControl addTarget:self action:@selector(switchTodayView:) forControlEvents:UIControlEventValueChanged];
    
    foursquareSegmentedControl.frame = CGRectMake(9, CGRectGetMaxY(self.datepicker.frame) + 6, CGRectGetWidth(self.view.frame) - 18, 35);

//    foursquareSegmentedControl.layer.shadowOffset = CGSizeMake(-8, 8);
//    foursquareSegmentedControl.layer.shadowRadius = 5;
//    foursquareSegmentedControl.layer.shadowOpacity = 0.5;

    // Add the control to your view
    [self.view addSubview:foursquareSegmentedControl];
    
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.emptyDataSetDelegate = self;
    collectionView.emptyDataSetSource = self;
   
    NSLog(@"** date us %@", date);
    UIBarButtonItem *calendatBtn =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconCalendar"] style:UIBarButtonItemStylePlain target:self action:@selector(showCalender)];

    self.navigationItem.rightBarButtonItems = @[calendatBtn];
    self.collectionView.alwaysBounceVertical = YES;
    
    allExerciseList = [ExerciseList MR_findAllSortedBy:@"exerciseName" ascending:YES];
    exerciseList = [[NSMutableArray alloc] init];
    
    heightArr = [[NSMutableArray alloc] init];
    dataArr = [[NSMutableArray alloc] init];
    
}

-(IBAction)editWorkout:(id)sender {
    if (exerciseList.count == 0) {
        return;
    }
    [self performSegueWithIdentifier:@"editTodayWorkoutSegue" sender:self];
}
-(IBAction)switchTodayView:(id)sender {
    cardsArray = nil;
    NSLog(@"segment selected is %lu", (unsigned long)foursquareSegmentedControl.selectedSegmentIndex);
    [heightArr removeAllObjects];
    [dataArr removeAllObjects];
    if (foursquareSegmentedControl.selectedSegmentIndex == 0) {
        cardsArray = [[NSMutableArray alloc]initWithArray:@[@"Workout"]];
        [self getWorkoutData];
    } else {
        cardsArray = [[NSMutableArray alloc]initWithArray:@[@"Workout Summary", @"Personal Records", @"Workout Timeline", @"Workout Stats", @"Muscle Stats", @"Exercise Stats"]];
        [self getSummaryData];
    }	
    [self.collectionView reloadData];
}
-(void) viewDidAppear:(BOOL)animated {
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];
    
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];
    
    self.title = @"Workout";
    self.navigationController.navigationBarHidden = NO;
    [self reloadData];
    
    [self switchTodayView:foursquareSegmentedControl];
//    [self getAllData];

    [self askUserToMigrateToAmrap];
}

-(void) getWorkoutData {
    NSMutableDictionary *temp;
    NSNumber *height = nil;
    NSArray *orderArray = nil;
    float heightChart = 175;
    
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSArray *todayWorkout = [Utilities getAllExerciseForDate:date];
    
    if ([todayWorkout count] == 0) {
        
        return;
    }
    
    temp = [TodayWorkoutItems getStartWorkout:todayWorkout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    /*
    temp =[TodayWorkoutItems setExerciseGoals:todayWorkout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:SINGLE_BUTTON]) {
        height = [NSNumber numberWithInt:35 * (int)[orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    */
        
    for (WorkoutList *exercise in todayWorkout) {
        temp = [TodayWorkoutItems getPerExerciseInformation:exercise workoutList:todayWorkout];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
            height = [NSNumber numberWithInt:heightChart];
        } else {
            height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];

    }
    [self.collectionView reloadData];

}
-(void) getSummaryData {
    NSMutableDictionary *temp;
    NSNumber *height = nil;
    NSArray *orderArray = nil;
    float heightChart = 175;
    
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    
//    temp =[TodayWorkoutItems getDailyStats:date];
//    orderArray = temp[AT_ORDER];
//    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
//        height = [NSNumber numberWithInt:heightChart];
//    } else {
//        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
//    }
//    [heightArr addObject:height];
//    [dataArr addObject:temp];

    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    // NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@",date];
    //NSArray *workout = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    
    NSArray *todayWorkout = [Utilities getAllExerciseForDate:date];//[UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES    withPredicate:predicate inContext:localContext];
    
    if ([todayWorkout count] == 0) {
        return;
    }
    
    NSArray *tempSetsRoutines = nil;
    NSMutableArray *userSetsRoutine = [[NSMutableArray alloc] init];
    tempSetsRoutines = [ExerciseSet MR_findAllSortedBy:@"date,workoutName,timeStamp" ascending:NO withPredicate:predicate inContext:localContext];
    for (ExerciseSet *ex in tempSetsRoutines) {
        [userSetsRoutine addObject:ex];
    }
    
    if ([userSetsRoutine count] > 0) {
        
        temp =[TodayWorkoutItems getWorkoutSummary:userSetsRoutine];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
            height = [NSNumber numberWithInt:heightChart];
        } else {
            height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
        
        
        temp =[TodayWorkoutItems getPersonalRecords:date];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:IMAGE_TYPE]) {
            if (temp[@"New Records"]) {
                height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
            }
            else {
                height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
                if ([height intValue]< 100)
                    height = [NSNumber numberWithInt:130];
            }
            //height = [NSNumber numberWithInt:[height intValue] + 30];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
        
        if (temp[@"New Records"] == nil && [Utilities askForRating] == true) {
            temp =[TodayWorkoutItems rateUsMenu];
            [cardsArray insertObject:[NSString stringWithFormat:@"Rate US"] atIndex:2];
            height = [NSNumber numberWithInt:35];
            [heightArr addObject:height];
            [dataArr addObject:temp];
        }

        temp =[TodayWorkoutItems getWorkoutTimeLine:userSetsRoutine];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:BAR_CHART]) {
            height = [NSNumber numberWithInt:heightChart];
        } else {
            height = [NSNumber numberWithInt:40 * (int)[orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
        

        
        
        temp =[TodayWorkoutItems getMuscleStats:userSetsRoutine];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
            height = [NSNumber numberWithInt:heightChart];
        } else {
            height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
        
        //    // this is special
        for (WorkoutList *key in todayWorkout) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", key.exerciseName];
            NSArray *exerciseArray = [userSetsRoutine filteredArrayUsingPredicate:exPredicate];
            
            if ([exerciseArray count] == 0)
                continue;
            
            temp = [TodayWorkoutItems getExerciseStats:exerciseArray];
            
            orderArray = temp[AT_ORDER];
            if ([temp[DATA_TYPE] isEqualToString:LIST_AND_LINE_CHART]) {
                height = [NSNumber numberWithInt:lblHeight * [temp[LINE_COUNT] intValue] + heightChart * [temp[CHART_COUNT] intValue]];
            }
            
            if  ([temp[AT_NAME_KEY] isEqualToString:@"None"])
                [temp setObject:key forKey:AT_NAME_KEY];
            [heightArr addObject:height];
            [dataArr addObject:temp];
        }
        
        [cardsArray addObject:@"Workout Reminder"];
        temp =[TodayWorkoutItems setNotificationForWorkout];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:SINGLE_BUTTON]) {
            height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
        

        temp =[TodayWorkoutItems featureRequest];
        [cardsArray addObject:@"Feature Suggestion"];
        height = [NSNumber numberWithInt:35];
        [heightArr addObject:height];
        [dataArr addObject:temp];
        

    }
}
-(void) showCoachMarks {
    bool shownTodayWorkoutHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownTodayWorkoutHint"];
    
    if ([Utilities isHintEnabled] || shownTodayWorkoutHint == false) {
        float width = self.view.frame.size.width;
        float height = lblHeight * 10.5;
        
        CGRect coachmark2 = CGRectMake(0, CGRectGetMinY(self.collectionView.frame), width, height);
        CGRect coachmark3 = CGRectMake(0, -30, 40, 40);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Click to Start Workout",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark3],
                                    @"caption": @"Click to Edit Workout",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    }

                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownTodayWorkoutHint"]);
    
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownTodayWorkoutHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void) showCalender {
    if (calendarFlag == true) {
        for (RSDFDatePickerView *lbl in [self.view subviews]) {
            if ([lbl isKindOfClass:[RSDFDatePickerView class]]) {
                [lbl removeFromSuperview];
            }
        }
        calendarFlag = false;
        return;
    } else {
        
        RSDFDatePickerView *datePickerView = [[RSDFDatePickerView alloc] initWithFrame:self.view.bounds];
        datePickerView.delegate = self;
        datePickerView.dataSource = self;
        datePickerView.pagingEnabled =  YES;
        datePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.view addSubview:datePickerView];
        calendarFlag = true;
    }
}

- (void)updateSelectedDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    //    formatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEEddMMMM" options:0 locale:nil];
    
    //self.selectedDateLabel.text = [formatter stringFromDate:self.datepicker.selectedDate];
    NSLog(@"selected date is... %@",[formatter stringFromDate:self.datepicker.selectedDate]);
    date = [formatter stringFromDate:self.datepicker.selectedDate];
    [self reloadData];
    [self switchTodayView:foursquareSegmentedControl];
}


-(void) getCalendarData {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        allExerciseForCalendar = [ExerciseSet MR_findAllSortedBy:@"date,exerciseNumber" ascending:YES];
        calendarColorDict = [[NSMutableDictionary alloc] init];
        
        NSMutableArray * unique  = [NSMutableArray array];
        NSMutableSet * processed = [NSMutableSet set];
        for (ExerciseSet *set in allExerciseForCalendar) {
            
            NSString *string = set.date;
            
            // Crash #15: nil string was causing the crash. Making sure such entires are skipped.
            if (string == nil) {
                NSLog(@"string is null");
                continue;
            }
            
            if ([processed containsObject:string] == NO) {
                [unique addObject:string];
                [processed addObject:string];
                [calendarColorDict setObject:set.majorMuscle forKey:string];
            }
        }
    });
}

-(void) viewDidDisappear:(BOOL)animated {
    [additionOptionView removeFromSuperview];
}


-(void) reloadData {
    //    exerciseList = [Utilities getAllExerciseForDate:date];
    [exerciseList removeAllObjects];
    for (WorkoutList *workout in [Utilities getAllExerciseForDate:date]) {
        [exerciseList addObject:workout];
    }
    //    exerciseList = [Utilities getAllExerciseForDateByExGroup:date];
    
    for (UserWorkout *wrk in exerciseList) {
        NSLog(@"%@", wrk.exerciseGroup);
    }
    NSLog(@"getting data for date %@ %lu", date, [exerciseList count]);
    
    if ([exerciseList count] == 0) {
        self.view.backgroundColor = [UIColor whiteColor];
        foursquareSegmentedControl.selectedSegmentIndex = 1;
    } else {
        self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
        foursquareSegmentedControl.selectedSegmentIndex = 0;
    }

    
    if ([exerciseList count] > 0) {
        [self showCoachMarks];
        [Utilities syncExerciseMetaInfoToParse];
    }
    
//    if ([exerciseList count] != 0 && [date isEqualToString:[Utilities getCurrentDate]]) {
        UIBarButtonItem *editWorkoutBtn =[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemEdit target:self action:@selector(editWorkout:)];
        self.navigationItem.leftBarButtonItems = @[editWorkoutBtn];
//    } else {
//        self.navigationItem.leftBarButtonItem =  nil;
//    }

    [self.collectionView reloadData];
}


-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    NSLog(@"count is %lu", [dataArr count]);
    return [dataArr count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    UICollectionViewCell *cell=[self.collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.layer.shadowRadius = 2;
    cell.layer.shadowOpacity = 0.5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[PieChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[LineChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[BarChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    NSMutableDictionary *item = [dataArr objectAtIndex:indexPath.row];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(cell.frame) - 10, 30)];
    title.text = item[AT_NAME_KEY];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor whiteColor];
    [title setFont:[UIFont fontWithName:@HAL_REG_FONT size:16]];
    
    int iterate = 0;
    if ([item[DATA_TYPE] isEqualToString:PIE_CHART]) {
        _pieChartView = [[PieChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 170)];
        [self setupPieChart];
        [self setPieChartDataValues:item];
        [cell.contentView addSubview:_pieChartView];
    } else if ([item[DATA_TYPE] isEqualToString:RIGHT_SIDE_BUTTON]) {
        NSArray *orderDisp = item[AT_ORDER];
        float width = CGRectGetWidth(cell.frame)/2 - 5;
        float customHeight = lblHeight;
        for (NSString *key in orderDisp) {
            
            if ([key isEqualToString:AT_NAME_KEY])
                continue;
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * (customHeight +5), width, customHeight)];
            UIButton *right = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * (customHeight +5), CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 10, customHeight)];
            left.text = [NSString stringWithFormat:@"  %@", key];
            [right setTitle:[NSString stringWithFormat:@"%@", item[key]] forState:UIControlStateNormal];
            left.textAlignment = NSTextAlignmentLeft;
            
            [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
            [right setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            [right.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:16]];
            
            left.textColor = [UIColor whiteColor];
            [right addTarget:self action:@selector(setExerciseGoalNow:) forControlEvents:UIControlEventTouchUpInside];
            
            right.tag = iterate;
            right.backgroundColor = [UIColor whiteColor];
            right.layer.cornerRadius = 5;
            
            [cell.contentView addSubview:left];
            [cell.contentView addSubview:right];
            iterate++;
        }

    } else if ([item[DATA_TYPE] isEqualToString:IMAGE_TYPE]) {
        if (item[@"New Records"]) {
            NSArray *orderDisp = item[AT_ORDER];
            float width = 0;
            if (indexPath.row == 0) {
                width = CGRectGetWidth(cell.frame)/4*3 - 5;
            } else {
                width = CGRectGetWidth(cell.frame)/2 - 5;
            }
            for (NSString *key in orderDisp) {
                
                if ([key isEqualToString:AT_NAME_KEY])
                    continue;
                
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, width, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 10, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                left.textAlignment = NSTextAlignmentLeft;
                right.textAlignment = NSTextAlignmentRight;
                
                if ([key isEqualToString:@"Exercise"]) {
                    [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                    
                } else
                    [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];//[left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                
                [right setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                iterate++;
            }
        } else {
            NSString *imageName = [NSString stringWithFormat:@"motivation%d", rand()%41];
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame)/3 - 5, 100)];
            [imageView setImage:[UIImage imageNamed:imageName]];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            NSArray *orderDisp = item[AT_ORDER];
            float width = CGRectGetWidth(cell.frame) - CGRectGetWidth(imageView.frame);
            float maxY = 0;
            
            for (NSString *key in orderDisp) {
                
                if ([key isEqualToString:AT_NAME_KEY])
                    continue;
                
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(imageView.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, width/3*2 - 10, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, width/3 - 2, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                left.textAlignment = NSTextAlignmentLeft;
                right.textAlignment = NSTextAlignmentRight;
                
                if ([key isEqualToString:@"Exercise"]) {
                    [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                    
                } else
                    [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];//[left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                
                [right setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                maxY = CGRectGetMaxY(right.frame);
                iterate++;
            }
            [cell.contentView addSubview:imageView];
            
            if (maxY < CGRectGetMaxY(imageView.frame))
                maxY = CGRectGetMaxY(imageView.frame);
            
            UIButton *shareIt = [[UIButton alloc] initWithFrame:CGRectMake(15, maxY, CGRectGetWidth(cell.frame) - 30, 30)];
            
            [shareIt addTarget:self action:@selector(shareButton:) forControlEvents:UIControlEventTouchUpInside];
            [shareIt setTitle:@"Share Success" forState:UIControlStateNormal];
            shareIt.backgroundColor = [UIColor whiteColor];
            [shareIt setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            [shareIt.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
            shareIt.layer.cornerRadius =  5;

            
            [cell.contentView addSubview:shareIt];
        }
    } else if ([item[DATA_TYPE] isEqualToString:BAR_CHART]) {
        _barChartView = [[BarChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 170)];
        [self setupBarChart];
        [self setBarChartDataValues:item];
        [cell.contentView addSubview:_barChartView];
    } else if ([item[DATA_TYPE] isEqualToString:LIST]) {
        NSArray *orderDisp = item[AT_ORDER];
        float width = 0;
        if (indexPath.row == 0) {
            width = CGRectGetWidth(cell.frame)/4*3 - 5;
        } else {
            width = CGRectGetWidth(cell.frame)/2 - 5;
        }
        for (NSString *key in orderDisp) {
            
            if ([key isEqualToString:AT_NAME_KEY])
                continue;
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, width, lblHeight)];
            left.text = [NSString stringWithFormat:@"  %@", key];
            left.textAlignment = NSTextAlignmentLeft;
            if ([key isEqualToString:@"Exercise"]) {
                [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
            } else
                [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];//[left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];

            left.textColor = [UIColor whiteColor];
            
            NSString *multiSetCheck = [NSString stringWithFormat:@"%@=MULTISET", key];
            if ([item[multiSetCheck] intValue] > 1) {
                left.clipsToBounds = YES;
                
                CALayer *rightBorder = [CALayer layer];
                rightBorder.borderColor = FlatWhite.CGColor;
                rightBorder.borderWidth = 4;
                NSString *multiSetCheckCount = [NSString stringWithFormat:@"%@=MULTISET-COUNT", key];
                if ([item[multiSetCheckCount] intValue] == 0) {
                    rightBorder.frame = CGRectMake(0, 4, 4, CGRectGetHeight(left.frame) - 4);
                    //NSLog(@"first");
                } else if ([item[multiSetCheckCount] intValue] == [item[multiSetCheck] intValue] - 1) {
                    //NSLog(@"last");
                    rightBorder.frame = CGRectMake(0, 0, 4, CGRectGetHeight(left.frame) - 4);
                } else {
                    //NSLog(@"middle");
                    rightBorder.frame = CGRectMake(0, 0, 4, CGRectGetHeight(left.frame));
                }
                //rightBorder.frame = CGRectMake(-1, -1, CGRectGetWidth(left.frame), CGRectGetHeight(left.frame)+2);
                
                
                [left.layer addSublayer:rightBorder];
                
            }
            
            if ([key isEqualToString:@"Goal"]) {
                UIButton *right = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * (lblHeight), CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 10, lblHeight)];
                [right setTitle:[NSString stringWithFormat:@"%@", item[key]] forState:UIControlStateNormal];
                [right setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
                [right.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
                
                [right addTarget:self action:@selector(setExerciseGoalNow:) forControlEvents:UIControlEventTouchUpInside];
                right.tag = indexPath.row;
                right.backgroundColor = [UIColor whiteColor];
                right.layer.cornerRadius = 5;
                [cell.contentView addSubview:right];
                
            } else {
                
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 10, lblHeight)];
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                right.textAlignment = NSTextAlignmentLeft;
                [right setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                right.textColor = [UIColor whiteColor];
                [cell.contentView addSubview:right];
            }
            
            [cell.contentView addSubview:left];
            
            iterate++;
        }
    } else if ([item[DATA_TYPE] isEqualToString:LIST_AND_LINE_CHART]) {
        NSArray *orderDisp = item[AT_ORDER];
        float maxY = CGRectGetMaxY(title.frame);
        for (NSString *key in orderDisp) {
            
            if ([key isEqualToString:SETS_PROGRESS]) {
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: SETS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatRed forKey:@"Dots"];
                [colorDict setObject:FlatRedDark forKey:@"Cover"];
                [self setLineChartDataValues:item[SETS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else if ([key isEqualToString:REPS_PROGRESS]) {
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: REPS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatYellow forKey:@"Dots"];
                [colorDict setObject:FlatYellowDark forKey:@"Cover"];
                [self setLineChartDataValues:item[REPS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
                
            } else if ([key isEqualToString:VOL_PROGRESS]) {
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY  + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: VOL_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatMint forKey:@"Dots"];
                [colorDict setObject:FlatMintDark forKey:@"Cover"];
                [self setLineChartDataValues:item[VOL_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else {
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                if ([right.text isEqualToString:@"-1"]) {
                    [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                    right.text = @"";
                } else {
                    left.textAlignment = NSTextAlignmentLeft;
                    right.textAlignment = NSTextAlignmentRight;
                    [left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                    [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                }
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                iterate++;
                maxY = CGRectGetMaxY(left.frame);
            }
        }
    } else if ([item[DATA_TYPE] isEqualToString:BOX]) {
        NSArray *orderDisp = item[AT_ORDER];
        float width = 0;
//        NSLog(@"%lu is selected", foursquareSegmentedControl.selectedSegmentIndex);
//        if (indexPath.row == 0  && foursquareSegmentedControl.selectedSegmentIndex == 1) {
//            width = CGRectGetWidth(cell.frame)/4*3 - 5;
//        } else {
            width = CGRectGetWidth(cell.frame)/2 - 5;
//        }
        for (NSString *key in orderDisp) {
            
            if ([key isEqualToString:AT_NAME_KEY])
                continue;
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, width, lblHeight)];
            UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame) + 5, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 15, lblHeight)];
            left.text = [NSString stringWithFormat:@"%@", item[key]];
            right.text = key;
            left.textAlignment = NSTextAlignmentRight;
            right.textAlignment = NSTextAlignmentLeft;
            
            [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
            [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];//[right setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
            
            left.textColor = [UIColor whiteColor];
            right.textColor = [UIColor whiteColor];
            
            [cell.contentView addSubview:left];
            [cell.contentView addSubview:right];
            iterate++;
        }
    } else if ([item[DATA_TYPE] isEqualToString:BUTTON]) {
        float width = CGRectGetWidth(cell.frame)/2 - 15;
        title.text = item[@"Desc"];
        UIButton *left = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(title.frame) + iterate * lblHeight, width, 30)];
        UIButton *right = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame)+ 10, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 40, 30)];
        [left setTitle:item[@"NO"] forState:UIControlStateNormal];
        [right setTitle:item[@"YES"] forState:UIControlStateNormal];
        left.backgroundColor = [UIColor clearColor];
        [left setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [left.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        left.layer.cornerRadius =  5;
        left.layer.borderWidth = 1;
        left.layer.borderColor = [UIColor whiteColor].CGColor;
        
        right.backgroundColor = [UIColor whiteColor];
        [right setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
        [right.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
        right.layer.cornerRadius =  5;
        //cell.backgroundColor = FlatMint;
        
        [left addTarget:self action:@selector(noPressed:) forControlEvents:UIControlEventTouchUpInside];
        if ([item[@"AskFurther"] boolValue] == false)
            [right addTarget:self action:@selector(yesPressed:) forControlEvents:UIControlEventTouchUpInside];
        else
            [right addTarget:self action:@selector(askPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        [cell.contentView addSubview:left];
        [cell.contentView addSubview:right];
    } else if ([item[DATA_TYPE] isEqualToString:SINGLE_BUTTON]) {
        float btnWidth = CGRectGetWidth(cell.frame) - 30;
        title.text = item[@"Desc"];
        if ([item[@"Desc"] isEqualToString:@"Workout Reminder"]) {
            
            NSArray *orderDisp = item[AT_ORDER];
            float width = 0;
            width = CGRectGetWidth(cell.frame)/2 - 5;
            for (NSString *key in orderDisp) {
                
                if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:@"YES"] || [key isEqualToString:@"Desc"])
                    continue;
                
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, width, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame) + 5, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame) - CGRectGetWidth(left.frame) - 15, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                left.textAlignment = NSTextAlignmentLeft;
                right.textAlignment = NSTextAlignmentRight;
                
                [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];//[right setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                iterate++;
            }

            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(title.frame) + iterate * lblHeight + 5, btnWidth, 30)];
            [button setTitle:item[@"YES"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
            [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
            button.layer.cornerRadius =  5;
            [button addTarget:self action:@selector(setReminder:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:button];
        } else {
            UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(15, CGRectGetMaxY(title.frame) + iterate * lblHeight + 5, btnWidth, 30)];
            [button setTitle:item[@"YES"] forState:UIControlStateNormal];
            button.backgroundColor = [UIColor whiteColor];
            [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
            button.layer.cornerRadius =  5;
            [button addTarget:self action:@selector(sendFeatureSuggestion:) forControlEvents:UIControlEventTouchUpInside];
            [cell.contentView addSubview:button];
        }

    }

    
    [cell.contentView addSubview:title];
    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (foursquareSegmentedControl.selectedSegmentIndex == 0) {
        if (indexPath.row == 0)
            exerciseSelected = [exerciseList objectAtIndex:0];
        else
            exerciseSelected = [exerciseList objectAtIndex:indexPath.row - 1];
        [self performSegueWithIdentifier:@"recordWorkoutSegue" sender:self];
    } else {
//        if (indexPath.row == 0)
//            [self performSegueWithIdentifier:@"dailyStatsSegue" sender:self];
//        else {
            exerciseSelected = [exerciseList objectAtIndex:0];
            [self performSegueWithIdentifier:@"recordWorkoutSegue" sender:self];
//        }

    }
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.view.frame) - 20, [[heightArr objectAtIndex:indexPath.row] floatValue] + 35);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 5, 5, 5);
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    if ([currentDate isEqualToString:date])
        text = @"No workout performed TODAY.";
    else
        text = [NSString stringWithFormat:@"No workout performed on %@.", date];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"To start a workout, select \"HOME\" (Bottom Left) and click \"START WORKOUT\"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"Icon_Home"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"selectRoutineSegue"]) {
        //        SelectRoutine *destVC = segue.destinationViewController;
        //        destVC.date = date;
    } else if ([segue.identifier isEqualToString:@"trackSegueNew"]) {
        //ExerciseTrackingVC *destVC = segue.destinationViewController;
        RecordExercise *destVC = segue.destinationViewController;
        NSLog(@"selected exercise is %@", exerciseSelected.exerciseName);
        destVC.exerciseObj = exerciseSelected;
        destVC.date = date;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.exerciseNumber = exerciseSelected.exerciseNumber;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"showSummarySegue"]) {
        ExerciseSummary *destVC = segue.destinationViewController;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.date = date;
    } else if ([segue.identifier isEqualToString:@"recordWorkoutSegue"]) {
        //ExerciseTrackingVC *destVC = segue.destinationViewController;
        //        RecordWorkout *destVC = segue.destinationViewController;
        RecWorkout *destVC = segue.destinationViewController;
        NSLog(@"selected exercise is %@, group %@", exerciseSelected.exerciseName, exerciseSelected.exerciseGroup);
        destVC.exerciseObj = exerciseSelected;
        destVC.date = date;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.exerciseNumber = exerciseSelected.exerciseNumber;
        destVC.exerciseGroup = exerciseSelected.exerciseGroup;
        destVC.exerciseNumInGroup = exerciseSelected.exerciseNumInGroup;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"editTodayWorkoutSegue"]) {
        NSArray *todayExercise = [Utilities getAllExerciseForDate:date];
        EditTodayWorkout *destVC = segue.destinationViewController;
        destVC.date = date;
        WorkoutList *item = [todayExercise objectAtIndex:0];
        destVC.routineName = item.routiineName;
        destVC.workoutName = item.workoutName;
        self.title = @"";
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"recordCardioSegue"]) {
        RecordCardioFast *destVC = segue.destinationViewController;
        //destVC.date = date;
        destVC.hidesBottomBarWhenPushed = TRUE;
        self.title = @"";
    }  else if ([segue.identifier isEqualToString:@"shareSegue"]) {
        ShareViewController *destVC = segue.destinationViewController;
        destVC.date = date;
        destVC.hidesBottomBarWhenPushed = TRUE;
        self.title = @"";
    } else if ([segue.identifier isEqualToString:@"dailyStatsSegue"]) {
        DailyStatsVC *destVC = segue.destinationViewController;
        destVC.hidesBottomBarWhenPushed = TRUE;
        self.title = @"";
    }
}

- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *headingStyle = NSMutableParagraphStyle.new;
    headingStyle.lineBreakMode = NSLineBreakByWordWrapping;
    headingStyle.alignment = NSTextAlignmentCenter;
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Workout Summary!" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];
    
    NSAttributedString *subTitle = [[NSAttributedString alloc] initWithString:date attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:16], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.numberOfLines = 0;
    subTitleLabel.attributedText = subTitle;
    
    
    /*
     // this is to get time spent during workout... START
     NSArray *timeSpent = [Utilities getSetsForAllExerciseForDateSortedByTime:date];
     NSMutableArray *timeSpentDate = [[NSMutableArray alloc] init];
     NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
     [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
     
     // this is imporant - we set our input date format to match our input string
     // if format doesn't match you'll get nil from your string, so be careful
     [dateFormatter setDateFormat:@"HH:mm:ss"];
     // voila!
     for (ExerciseSet *set in timeSpent) {
     [timeSpentDate addObject:[dateFormatter dateFromString:set.timeStamp]];
     NSLog(@"exercise name is %@ at timestamp %@", set.majorMuscle, set.timeStamp);
     //        weightMvd += [set.weight floatValue] * [set.rep intValue];
     }
     
     NSDate* date2 = [timeSpentDate objectAtIndex:0];
     NSDate* date1 = [timeSpentDate objectAtIndex:[timeSpentDate count]- 1];
     NSTimeInterval secs = [date1 timeIntervalSinceDate:date2];
     NSLog(@"Workout Duration: %ld:%ld:%ld", (long) secs/3600, (long)(secs/60)%60, (long) secs %60);
     // this is to get time spent during workout... END
     */
    // get all sets performed today.
    NSArray *allExerciseSet = [Utilities getSetsForAllExerciseForDate:date];
    
    int exercises = ([exerciseList count] != 0) ? (int)[exerciseList count] : 0;
    float weightMvd = 0;
    
    int setCount =([allExerciseSet count] != 0) ? (int)[allExerciseSet count] : 0;
    
    for (ExerciseSet *set in allExerciseSet) {
        //        NSLog(@"exercise name is %@ at timestamp %@", set.majorMuscle, set.timeStamp);
        weightMvd += [set.weight floatValue] * [set.rep intValue];
    }
    
    // this is to get all the muscle worked...
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in allExerciseSet) {
        NSString *string = data.majorMuscle;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    //  NSLog(@"muscle array %@", unique);
    UIView *customViewMuscle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(self.view.frame)- 30, 30)];
    float viewWidthMuscle = CGRectGetWidth(customViewMuscle.frame)/[unique count];
    for (int i = 0 ; i < [unique count]; i++) {
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(i * viewWidthMuscle, 0, viewWidthMuscle - 5, 30)];
        lbl.text = [unique objectAtIndex:i];
        [lbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
        lbl.layer.cornerRadius = 15.0f;
        lbl.clipsToBounds=YES;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [Utilities getMajorMuscleColor:[unique objectAtIndex:i]];
        lbl.textColor = [UIColor whiteColor];
        [customViewMuscle addSubview:lbl];
    }
    // this is to get all sets, exercise and wt moved..
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(self.view.frame)- 30, 70)];
    // customView.backgroundColor = [UIColor lightGrayColor];
    
    // NSLog(@"frame %f, cv %f", CGRectGetWidth(self.view.frame), CGRectGetWidth(customView.frame));
    float viewWidth = CGRectGetWidth(customView.frame)/3;
    for (int i = 0 ; i < 3; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(i * viewWidth, 0, viewWidth, 70)];
        
        UILabel *subViewData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 40)];
        subViewData.textAlignment = NSTextAlignmentCenter;
        subViewData.numberOfLines = 2;
        
        UILabel *subViewtitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(subViewData.frame), viewWidth, 30)];
        subViewtitle.textAlignment = NSTextAlignmentCenter;
        
        //    NSLog(@"x %f, wd: %f, lb %f, %f", i * viewWidth, viewWidth, CGRectGetMinX(view.frame), CGRectGetWidth(view.frame));
        [subViewtitle setFont:[UIFont fontWithName: @"HelveticaNeue-Thin" size:14]];
        [subViewData setFont:[UIFont fontWithName: @"HelveticaNeue" size:30]];
        
        switch (i) {
            case 0:
                subViewtitle.text = @"Exercises";
                subViewData.text = [NSString stringWithFormat:@"%d", exercises];
                subViewData.textColor = FlatMintDark;
                break;
            case 1:
                subViewtitle.text = [NSString stringWithFormat:@"Wt. Mvd (%@)", [Utilities getUnits]];
                subViewData.text = [NSString stringWithFormat:@"%d", (int) weightMvd];
                subViewData.textColor = FlatRedDark;
                break;
            case 2:
                subViewtitle.text = @"Sets !";
                subViewData.text = [NSString stringWithFormat:@"%d", setCount];
                subViewData.textColor = FlatOrangeDark;
                break;
            default:
                break;
        }
        [view addSubview:subViewtitle];
        [view addSubview:subViewData];
        [customView addSubview:view];
    }
    
    //    NSAttributedString *muscle = [[NSAttributedString alloc] initWithString:@"Workout Summary!" attributes:@{NSFontAttributeName : [UIFont fontWithName: @"HelveticaNeue-Bold" size:20], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatBlueDark}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Share" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        //        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    if ([unique count] > 0) {
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, subTitleLabel, customViewMuscle, customView]];
        
    } else
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, subTitleLabel, customView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
}

#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}


-(IBAction)saveToParse:(id)sender {
    
    [Utilities logout];
}


//-(void) workoutComplete {
//    if (showWorkoutCompleteBtn == true) {
//        additionOptionView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44)];
//        
//        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
//            
//            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame) - 44, CGRectGetWidth(self.view.frame), 44);
//            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
//            
//        } completion:^(BOOL finished) {
//            
//        }];
//        NSLog(@"reloading the end workout...");
//        additionOptionView.backgroundColor = FlatOrange;
//        
//        UIButton *dropSet = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(self.view.frame)/2 , 35)];
//        dropSet.backgroundColor = [UIColor whiteColor];
//        dropSet.layer.cornerRadius = 5.0f;
//        [dropSet setTitle:@"Workout Complete!" forState:UIControlStateNormal];
//        [dropSet setTitleColor:FlatOrange forState:UIControlStateNormal];
//        [dropSet addTarget:self action:@selector(workoutCompleteSync:) forControlEvents:UIControlEventTouchUpInside];
//        
//        dropSet.center = CGPointMake(additionOptionView.frame.size.width  / 2,
//                                     additionOptionView.frame.size.height / 2);
//        
//        [additionOptionView addSubview:dropSet];
//        [self.view addSubview:additionOptionView];
//    }
//}
//
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
    } completion:^(BOOL finished) {
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame) - 44, CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
        
    } completion:^(BOOL finished) {
        
    }];
    
}

-(IBAction)workoutCompleteSync:(id)sender {
    NSLog(@"workout completed. Syncing now.... %@", date);
    [Utilities syncExerciseSetToParse:date];
    
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
    } completion:^(BOOL finished) {
    }];
}


// Returns YES if the date should be highlighted or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldHighlightDate:(NSDate *)date
{
    return YES;
}

// Returns YES if the date should be selected or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldSelectDate:(NSDate *)myDate
{
    return YES;
}

// Prints out the selected date.
- (void)datePickerView:(RSDFDatePickerView *)view didSelectDate:(NSDate *)calDate
{
    
    [view removeFromSuperview];
    NSLog(@"%@", [calDate description]);
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *temp = [formatter dateFromString:[calDate formattedDateWithFormat:@"yyyy-MM-dd"]];
    
    [self.datepicker selectDate:calDate];
    
    NSLog(@"selected date is... %@, temp %@",[formatter stringFromDate:self.datepicker.selectedDate], temp);
    
    date = [formatter stringFromDate:calDate];
    [self reloadData];
    calendarFlag = false;
    
    
}

// calendar data source
// Returns YES if the date should be marked or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldMarkDate:(NSDate *)mydate
{
    // The date is an `NSDate` object without time components.
    // So, we need to use dates without time components.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    
    if ([calendarColorDict objectForKey:[formatter stringFromDate:mydate]]) {
        
        return YES;
    }
    
    
    if ([[formatter stringFromDate:today] isEqualToString:[formatter stringFromDate:mydate]])
        return YES;
    
    
    return NO;
}

// Returns the color of the default mark image for the specified date.
- (UIColor *)datePickerView:(RSDFDatePickerView *)view markImageColorForDate:(NSDate *)myDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    
    if ([[formatter stringFromDate:today] isEqualToString:[formatter stringFromDate:myDate]])
        return FlatRed;
    
    
    //NSLog(@"date %@ myDate %@, color %@", today, myDate, [calendarColorDict objectForKey:myDate]);
    
    
    return [Utilities getMajorMuscleColor:[calendarColorDict objectForKey:[formatter stringFromDate:myDate]]];
}

//// Returns the mark image for the specified date.
//- (UIImage *)datePickerView:(RSDFDatePickerView *)view markImageForDate:(NSDate *)date
//{
//    NSLog(@"=========>>>>> %s",__func__);
//    if (arc4random() % 2 == 0) {
//        return [UIImage imageNamed:@"IconComplete"];
//    } else {
//        return [UIImage imageNamed:@"IconIncomplete"];
//    }
//}



#pragma mark PremiumPackage

-(void)showPurchaseMenu:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"UPGRADE TO PREMIUM" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Trial expired. Please upgrade to premium package." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"To continue using the GYMINUTES, please upgrade to premium package." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    button.enabled = false;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"TRIAL EXPIRED" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
    button.layer.cornerRadius = 4;
    button.layer.borderWidth = 2;
    button.layer.borderColor = FlatGrayDark.CGColor;
    button.tintColor = FlatOrangeDark;
    button.backgroundColor = FlatGrayDark;
    
    NSNumberFormatter * _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            NSArray *prod = products;
            
            button.enabled = true;
            button.layer.borderColor = FlatOrangeDark.CGColor;
            button.backgroundColor = FlatOrangeDark;
            
            for (SKProduct * product in prod) {
                if ([product.productIdentifier isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
                    [_priceFormatter setLocale:product.priceLocale];
                    [button setTitle:[NSString stringWithFormat:@"BUY %@", [_priceFormatter stringFromNumber:product.price]] forState:UIControlStateNormal];
                    break;
                }
            }
        }
    }];
    
    
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatOrangeDark;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    //    self.popupController.theme.backgroundColor = [UIColor colorWithRed:(160/255.0) green:(97/255.0) blue:(5/255.0) alpha:1];
    //    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(void) setupLineChartView: (NSString *) name {
    _lineChartView.delegate = self;
    
    _lineChartView.descriptionText = name;
    _lineChartView.noDataText = @"No data.";
    
    _lineChartView.dragEnabled = YES;
    [_lineChartView setScaleEnabled:YES];
    _lineChartView.pinchZoomEnabled = YES;
    _lineChartView.drawGridBackgroundEnabled = NO;
    _lineChartView.backgroundColor = [UIColor clearColor];//[Utilities getAppColorLight];
    _lineChartView.layer.cornerRadius = 4;
    
    
    ChartXAxis *xAxis = _lineChartView.xAxis;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    xAxis.granularity = 1;
    xAxis.drawLabelsEnabled = NO;
    
    ChartYAxis *leftAxis = _lineChartView.leftAxis;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.drawZeroLineEnabled = YES;
    leftAxis.enabled = NO;
    
    ChartYAxis *rightAxis = _lineChartView.rightAxis;
    rightAxis.drawAxisLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    
    _lineChartView.legend.enabled = false;
    
    _lineChartView.rightAxis.enabled = NO;
    _lineChartView.xAxis.axisLineColor = FlatWhite;
    _lineChartView.xAxis.labelTextColor = FlatWhite;
    _lineChartView.leftAxis.axisLineColor = FlatWhite;
    _lineChartView.leftAxis.labelTextColor = FlatWhite;
    
    [_lineChartView.viewPortHandler setMaximumScaleY: 2.f];
    [_lineChartView.viewPortHandler setMaximumScaleX: 2.f];
    
    //    ChartMarker *marker = [[ChartMarker alloc] initWithColor:[UIColor colorWithWhite:180/255. alpha:1.0] font:[UIFont systemFontOfSize:12.0] insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    //    marker.minimumSize = CGSizeMake(80.f, 40.f);
    //    _chartView.marker = marker;
    
    //    _chartView.legend.form = ChartLegendFormLine;
    
    [_lineChartView animateWithXAxisDuration:2.5 easingOption:ChartEasingOptionEaseInOutQuart];
    
    
}
-(void) setLineChartDataValues: (NSMutableDictionary *) chartData colorDict:(NSMutableDictionary *) colorDict {
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    NSArray *items = chartData[@"xvals"];
    for (int i = 0; i < [items count]; i++)
    {
        [xVals addObject:[items objectAtIndex:i]];
    }
    
    NSArray *yvalItems = chartData[@"yvals"];
    for (int i = 0; i < [yvalItems count]; i++)
    {
        int value = [[yvalItems objectAtIndex:i] intValue] ;
        [yVals addObject:[[ChartDataEntry alloc] initWithX:i y:value]];
    }
    
    LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals label:@""];
    
    set1.lineDashLengths = @[@5.f, @2.5f];
    set1.highlightLineDashLengths = @[@5.f, @2.5f];
    [set1 setColor:colorDict[@"Dots"]];
    [set1 setCircleColor:colorDict[@"Cover"]];
    set1.lineWidth = 1.5;
    set1.circleRadius = 5.0;
    set1.drawCircleHoleEnabled = NO;
    set1.valueFont = [UIFont systemFontOfSize:9.f];
    set1.valueTextColor = FlatWhite;
    set1.fillAlpha = 65/255.0;
    set1.fillColor = colorDict[@"Dots"];
    set1.fillAlpha = 1.f;
    set1.drawFilledEnabled = YES;
    
    set1.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    
    
    //    LineChartDataSet *set2 = [[LineChartDataSet alloc] initWithYVals:yVals1 label:@"DataSet 2"];
    //    set2.axisDependency = AxisDependencyRight;
    //    [set2 setColor:UIColor.redColor];
    //    [set2 setCircleColor:UIColor.whiteColor];
    //    set2.lineWidth = 2.0;
    //    set2.circleRadius = 3.0;
    //    set2.fillAlpha = 65/255.0;
    //    set2.fillColor = UIColor.redColor;
    //    set2.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    //    set2.drawCircleHoleEnabled = NO;
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    //    [dataSets addObject:set2];
    
    LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@HAL_REG_FONT size:8.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _lineChartView.data = data;
    [_lineChartView highlightValues:nil];
    
}

-(void) setupPieChart {
    _pieChartView.usePercentValuesEnabled = YES;
    _pieChartView.holeColor = [UIColor clearColor];
    _pieChartView.noDataText = @"No data.";
    _pieChartView.holeRadiusPercent = 0.40;
    _pieChartView.transparentCircleRadiusPercent = 0.21;
    _pieChartView.descriptionText = @"";
    _pieChartView.drawCenterTextEnabled = YES;
    _pieChartView.drawHoleEnabled = YES;
    _pieChartView.rotationAngle = 0.0;
    _pieChartView.rotationEnabled = YES;
    _pieChartView.legend.enabled = YES;
    _pieChartView.drawSliceTextEnabled = NO;
    _pieChartView.holeColor = [Utilities getAppColor];
    
    //        ChartLegend *l = _pieChartView.legend;
    //        l.position = ChartLegendPositionPiechartCenter;
    //        l.xEntrySpace = 7.0;
    //        l.yEntrySpace = 0.0;
    //        l.yOffset = 0.0;
    
    
    _pieChartView.legend.form = ChartLegendFormSquare;
    _pieChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _pieChartView.legend.textColor = UIColor.whiteColor;
    _pieChartView.legend.position = ChartLegendPositionLeftOfChart;
    
    [_pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
    
}


-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    bool colorFlag = false;
    for (id key in chartData)
    {
        if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE]) {
            continue;
        }
        
        if ([key isEqualToString:AT_NAME_MUSCLE]) {
            colorFlag = true;
            continue;
        }
        [xVals addObject:key];
        [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:[chartData[key] floatValue] label:key]];
        if (colorFlag == true) {
            [colors addObject:[Utilities getMajorMuscleColor:key]];
        }
        
    }
    
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:yVals1 label:@""];
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    if (colorFlag == false) {
        [colors addObject:FlatYellowDark];
        [colors addObject:FlatRedDark];
        [colors addObject:FlatGreenDark];
        [colors addObject:FlatBlueDark];
        [colors addObject:FlatPinkDark];
        [colors addObject:FlatOrangeDark];
    }
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _pieChartView.data = data;
    [_pieChartView highlightValues:nil];
    
}


-(void) setupBarChart {
    _barChartView.delegate = self;
    
    _barChartView.descriptionText = @"";
    _barChartView.noDataText = @"No data.";
    
    _barChartView.drawBarShadowEnabled = NO;
    _barChartView.drawValueAboveBarEnabled = YES;
    _barChartView.backgroundColor = [UIColor clearColor];
    _barChartView.gridBackgroundColor = [UIColor clearColor];
    _barChartView.drawGridBackgroundEnabled = NO;
    _barChartView.maxVisibleCount = 60;
    
    ChartXAxis *xAxis = _barChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    
    ChartYAxis *leftAxis = _barChartView.leftAxis;
    leftAxis.enabled = YES;
    leftAxis.labelFont = [UIFont systemFontOfSize:10.f];
    leftAxis.labelCount = 8;
    leftAxis.labelTextColor = [UIColor whiteColor];
//    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
//    leftAxis.valueFormatter.maximumFractionDigits = 1;
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.gridColor = [UIColor clearColor];
    
    ChartYAxis *rightAxis = _barChartView.rightAxis;
    rightAxis.enabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont systemFontOfSize:10.f];
    rightAxis.labelCount = 8;
    rightAxis.valueFormatter = leftAxis.valueFormatter;
    rightAxis.spaceTop = 0.15;
    
    _barChartView.legend.position = ChartLegendPositionBelowChartLeft;
    _barChartView.legend.form = ChartLegendFormSquare;
    _barChartView.legend.formSize = 9.0;
    _barChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _barChartView.legend.xEntrySpace = 4.0;
    
}

-(void) setBarChartDataValues: (NSMutableDictionary *) chartData {
    
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    NSArray *items = chartData[@"xvals"];
    for (int i = 0; i < [items count]; i++)
    {
        [xVals addObject:[items objectAtIndex:i]];
    }

    NSArray *yvalItems = chartData[@"yvals"];
    for (int i = 0; i < [yvalItems count]; i++)
    {
        int value = [[yvalItems objectAtIndex:i] intValue] ;
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i y:value]];
    }
    NSLog(@"xval count %lu yval count %lu", (unsigned long)[xVals count], (unsigned long)[yVals count]);
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals label:[NSString stringWithFormat:@"%@ (%@)", @"Volume/Set", [[Utilities getUnits] lowercaseString]]];
    set1.colors = @[[UIColor whiteColor]];

    [set1 setDrawValuesEnabled:NO];
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    
    BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@HAL_REG_FONT size:10.f]];
    
    _barChartView.data = data;
    
}


#pragma mark - pieChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    if ([chartView isKindOfClass:[LineChartView class]]) {
    } else if ([chartView isKindOfClass:[PieChartView class]]) {
    }
    
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
//    NSLog(@"chartValueNothingSelected");
}

-(IBAction) noPressed:(id)sender {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert setCustomViewColor:[Utilities getAppColor]];

    [alert addButton:@"No, thanks" actionBlock:^{
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ReviewLastAsked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self getAllData];
        [self switchTodayView:foursquareSegmentedControl];
    }];
    
    [alert addButton:@"Yes, sure" actionBlock:^{
        [self sendFeedback:@"Feedback: Gyminutes"];
    }];
    [alert showNotice:self.parentViewController title:@"FEEDBACK" subTitle:@"Would you mind giving us some feedback?" closeButtonTitle:nil duration:0.0f];
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"NoReview" withParameters:articleParams];
}

-(IBAction) yesPressed:(id)sender {
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"YesReview" withParameters:articleParams];
    [self takeMeToTheChopper];
}

-(IBAction) askPressed:(id)sender {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert setCustomViewColor:[Utilities getAppColor]];

    
    [alert addButton:@"No, thanks" actionBlock:^{
        [self noPressed:self];
        [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ReviewLastAsked"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        //[self getAllData];
        [self switchTodayView:foursquareSegmentedControl];
    }];
    
    [alert addButton:@"Yes, sure" actionBlock:^{
        [self takeMeToTheChopper];
    }];
    [alert showInfo:self.parentViewController title:@"RATE US" subTitle:@"How about a rating us on the app store?" closeButtonTitle:nil duration:0.0f];
}

-(void) takeMeToTheChopper {
#define YOUR_APP_STORE_ID 1059671198 //Change this one to your ID
    NSString * appId = @"1059671198";
    NSString * theUrl = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appId];
    if ([[UIDevice currentDevice].systemVersion integerValue] > 6)
        theUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appId];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"RateAppClicked" withParameters:articleParams];
    
//    [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ReviewLastAsked"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//    
}
-(void) sendFeedback: (NSString *) subjectString {
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if (mailClass != nil) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:subjectString];
        [mc setMessageBody:@"" isHTML:NO];
        [mc setToRecipients:@[@"gyminutes@gmail.com"]];
        // Present mail view controller on screen
        if([mailClass canSendMail]) {
            [self presentViewController:mc animated:YES completion:^{
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"EmailedFeedback" withParameters:articleParams];
                
            }];
        }
    }

}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed: {
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [Flurry logError:@"emailFailure" message:[NSString stringWithFormat:@"Date:%@,UserID:%@", [NSDate date], [PFUser currentUser].objectId] error:error];
        }
            break;
        default:
            break;
    }
    
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}
-(IBAction)sendFeatureSuggestion:(id)sender {
    [self sendFeedback:@"Feature Suggestion: GYMINUTES"];
}

-(IBAction)setExerciseGoalNow:(id)sender {
    UIButton *goalButton = (UIButton *) sender;
    WorkoutList *exercise = [exerciseList objectAtIndex:goalButton.tag - 1];
   // NSLog(@"tag is %ld %@", (long)goalButton.tag, exercise.exerciseName);
    [self showCalenderForGoals:exercise.exerciseName];

}

-(void) showCalenderForGoals:(NSString *) exerciseName {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];

    NSMutableParagraphStyle *headingStyle = NSMutableParagraphStyle.new;
    headingStyle.lineBreakMode = NSLineBreakByWordWrapping;
    headingStyle.alignment = NSTextAlignmentCenter;
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:exerciseName attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];
    
    NSAttributedString *subTitle = [[NSAttributedString alloc] initWithString:exerciseName attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:20], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];
    
    UILabel *subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.numberOfLines = 0;
    subTitleLabel.attributedText = subTitle;
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 200, 55)];
    customView.backgroundColor = [Utilities getAppColor];
    
    JVFloatLabeledTextField *textFied = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(0, 10, 200, 40)];
    textFied.borderStyle = UITextBorderStyleRoundedRect;
    textFied.placeholder = [NSString stringWithFormat:@"Weight (%@)", [[Utilities getUnits] lowercaseString]];
    textFied.keyboardType = UIKeyboardTypeDecimalPad;
    textFied.textAlignment = NSTextAlignmentCenter;
    [customView addSubview:textFied];
    
    
    UIDatePicker  *datePickerGoal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 250, 180)];
    datePickerGoal.datePickerMode = UIDatePickerModeDate;
    [datePickerGoal setValue:[UIColor whiteColor] forKey:@"textColor"];

    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SET GOAL" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        int goalWeight = 0;
        if ([textFied.text floatValue] <= 0 || [textFied.text isEqual: @""]) {
            // dont update anything
        } else {
            goalWeight = [textFied.text floatValue]
            ;
            NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
            [dateFormat setDateFormat:@"yyyy-MM-dd"];
            
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
            ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
            
            ExerciseMetaInfo *exMetaInfoGoal = [ExerciseMetaInfo MR_findFirstWithPredicate:exPredicate inContext:localContext];
            if (exMetaInfoGoal == nil) {
                NSLog(@"meta info absent");
                exMetaInfoGoal = [ExerciseMetaInfo MR_createEntityInContext:localContext];
                exMetaInfoGoal.exerciseName = exercise.exerciseName;
                exMetaInfoGoal.muscle = exercise.muscle;
                exMetaInfoGoal.seatHeight = [NSNumber numberWithInt:0];
                exMetaInfoGoal.handAngle= [NSNumber numberWithInt:0];
                exMetaInfoGoal.maxWeight = [NSNumber numberWithFloat:0];
                exMetaInfoGoal.maxRep = [NSNumber numberWithInt:0];
                exMetaInfoGoal.maxDate = date;
                exMetaInfoGoal.syncedState = [NSNumber numberWithBool:false];
                exMetaInfoGoal.comment = @"";
                exMetaInfoGoal.goalWeightOrRep = [NSNumber numberWithFloat:goalWeight];
                exMetaInfoGoal.goalAchieveDate = datePickerGoal.date;
                [localContext MR_saveToPersistentStoreAndWait];
            } else {
                NSLog(@"meta info exist %@", exMetaInfoGoal);
                exMetaInfoGoal.syncedState = [NSNumber numberWithBool:false];
                exMetaInfoGoal.goalWeightOrRep = [NSNumber numberWithFloat:goalWeight];
                exMetaInfoGoal.goalAchieveDate = datePickerGoal.date;
                [localContext MR_saveToPersistentStoreAndWait];
            }
            [Utilities syncExerciseMetaInfoToParse];
            [self switchTodayView:foursquareSegmentedControl];
        }
        [self.popupController dismissPopupControllerAnimated:YES];

    };
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, customView, datePickerGoal, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.delegate = self;
    self.popupController.theme.backgroundColor = [Utilities getAppColor];
    
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)shareButton:(id)sender {
    [self performSegueWithIdentifier:@"shareSegue" sender:self];
}

-(IBAction)setReminder:(id)sender {
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"WORKOUT REMINDER" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];

    MultiSelectSegmentedControl *daysControl = [[MultiSelectSegmentedControl alloc] initWithItems:@[@"Sun", @"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat"]];
    daysControl.frame = CGRectMake(0, 0, 250, 40);
    //[daysControl selectAllSegments:YES];
    daysControl.tintColor = [UIColor whiteColor];
    
    UIDatePicker  *datePickerGoal = [[UIDatePicker alloc] initWithFrame:CGRectMake(0, 0, 250, 180)];
    datePickerGoal.datePickerMode = UIDatePickerModeTime;
    [datePickerGoal setValue:[UIColor whiteColor] forKey:@"textColor"];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"SET REMINDER" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
    button.layer.cornerRadius = 5;
    button.selectionHandler = ^(CNPPopupButton *button){
        NSLog(@"selected days are : %@", [daysControl selectedSegmentTitles]);
        NSLog(@"selected time is : %@", datePickerGoal.date);
        UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                        UIUserNotificationTypeBadge |
                                                        UIUserNotificationTypeSound);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                                 categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];

        // before we create any new ones, cancel all existing notifications
        //[[UIApplication sharedApplication]cancelAllLocalNotifications];
        
        if ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone) {
            
            for (NSString *day in [daysControl selectedSegmentTitles]) {

                if ([day isEqualToString:@"Sun"]) {
                    [self setWorkoutReminders:3 date:datePickerGoal.date message:NSLocalizedString(@"Time To Workout", @"") weekStr:@"Sun"];
                }
                
                if ([day isEqualToString:@"Mon"])
                    [self setWorkoutReminders:4 date:datePickerGoal.date message:NSLocalizedString(@"Consistency is the Key. Let's workout.", @"") weekStr:@"Mon"];
                
                if ([day isEqualToString:@"Tue"])
                    [self setWorkoutReminders:5 date:datePickerGoal.date message:NSLocalizedString(@"Let's get it done.", @"") weekStr:@"Tue"];
                
                if ([day isEqualToString:@"Wed"])
                    [self setWorkoutReminders:6 date:datePickerGoal.date message:NSLocalizedString(@"Weights are waiting to be lifted. Let's go.", @"") weekStr:@"Wed"];
                
                if ([day isEqualToString:@"Thu"])
                    [self setWorkoutReminders:7 date:datePickerGoal.date message:NSLocalizedString(@"Time to lift some weight.", @"") weekStr:@"Thu"];
                
                if ([day isEqualToString:@"Fri"])
                    [self setWorkoutReminders:1 date:datePickerGoal.date message:NSLocalizedString(@"Let's Lift", @"") weekStr:@"Fri"];
                
                if ([day isEqualToString:@"Sat"])
                    [self setWorkoutReminders:2 date:datePickerGoal.date message:NSLocalizedString(@"Time to build some muscle.", @"") weekStr:@"Sat"];

            }
        }
        [self.popupController dismissPopupControllerAnimated:YES];
        [self switchTodayView:foursquareSegmentedControl];
    };
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, daysControl, datePickerGoal, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.theme.backgroundColor = [Utilities getAppColor];
    
    [self.popupController presentPopupControllerAnimated:YES];
}

-(UILocalNotification *) setWorkoutReminders:(NSInteger) weekDay date:(NSDate *) reminderTime message:(NSString *) message weekStr:(NSString *)weekStr{
    
    NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for (UILocalNotification *not in notifications) {
        NSString *dateString = [NSString stringWithFormat:@"%@", [not.userInfo valueForKey:@"key"]];
        NSString *weekDayKey = [NSString stringWithFormat:@"%ld", weekDay];
//        NSLog(@"notification date string %@ %@", dateString, weekDayKey);
        
        if ([dateString isEqualToString:weekDayKey])
        {
            [[UIApplication sharedApplication] cancelLocalNotification:not];
        }
    }
    
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *componentsForFireDate = [calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday |  NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond ) fromDate: reminderTime];
    
    [componentsForFireDate setDay:weekDay] ;
    [componentsForFireDate setHour: [reminderTime hour]];
    [componentsForFireDate setMinute: [reminderTime minute]] ;
    [componentsForFireDate setSecond: [reminderTime second]] ;
    
    NSDate *fireDateOfNotification = [calendar dateFromComponents: componentsForFireDate];
    UILocalNotification *notification = [[UILocalNotification alloc]  init] ;
    notification.fireDate = fireDateOfNotification;
    notification.timeZone = [NSTimeZone localTimeZone] ;
    notification.repeatInterval = NSCalendarUnitWeekOfYear;
    notification.alertBody = message;
    
    notification.userInfo= [NSDictionary dictionaryWithObject:[NSNumber numberWithInteger:weekDay] forKey:@"key"];
    notification.soundName=UILocalNotificationDefaultSoundName;
    
    NSLog(@"notification: %@ %ld %ld %ld %ld",notification, (long)weekDay, (long)[reminderTime hour], (long)[reminderTime minute], (long)[reminderTime second]);
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    
    return notification;

}

#pragma MIGRATE TO AMRAP
-(void) askUserToMigrateToAmrap {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULT_UPDATE_TO_AMRAP] == true) {
        return;
    }
    
    [UpdateHandler v2_4_update_UserWorkout_to_AMRAP];
    [UpdateHandler v2_4_update_WorkoutList_to_AMRAP];
    [[NSUserDefaults standardUserDefaults] setBool:YES   forKey:USER_DEFAULT_UPDATE_TO_AMRAP];
    [[NSUserDefaults standardUserDefaults] synchronize];

}

@end
