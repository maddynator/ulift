//
//  AnalyzeRoutine.m
//  uLift
//
//  Created by Mayank Verma on 9/23/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AnalyzeRoutine.h"
#import "commons.h"
#import "AnalyzeVC.h"
#import "AnalysisHome.h"

@interface AnalyzeRoutine () {
    NSMutableArray *routineNames;
    NSString *selectedRoutine;
    NSArray *routineNameStored;
}

@end

@implementation AnalyzeRoutine

- (void)viewDidLoad {
    [super viewDidLoad];

//    routineNames = [[NSMutableArray alloc] initWithObjects:@"All Routines", nil];
    routineNames = [[NSMutableArray alloc] init];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    //routineNameStored = [RoutineMetaInfo MR_findAllInContext:localContext];
    routineNameStored  = [RoutineMetaInfo MR_findAllSortedBy:@"routineCreatedDate" ascending:NO inContext:localContext];

    
    for(RoutineMetaInfo *data in routineNameStored) {
        [routineNames addObject:data.routineName];
    }
    
    //self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconMenu"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationController.navigationBar.translucent = YES;
    self.tableView.backgroundColor = [UIColor whiteColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    selectedRoutine = @"";
    self.title = @"Analyze Routines";
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [routineNames count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel *routine = (UILabel *)[cell.contentView viewWithTag:100];
    routine.text = [[routineNames objectAtIndex:indexPath.row] uppercaseString];
    //routine.textColor = [UIColor whiteColor];
    [routine setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];

    
//    if (indexPath.row == 0) {
//        cell.backgroundColor = FlatOrange;
//        
//    } else {
        // Subtracting 1 as "ALL" routines will be at the start.
        RoutineMetaInfo *data = [routineNameStored objectAtIndex:indexPath.row]; // removing -1 as we dont have "All"
    routine.textColor = [Utilities colorForString:data.routineColor];;
    cell.backgroundColor = [UIColor whiteColor];//[Utilities colorForString:data.routineColor];
//    }
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    [cell.contentView addSubview:lineView];

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    if (indexPath.row == 0) {
//        selectedRoutine = @"All";
//    } else {
        RoutineMetaInfo *data = [routineNameStored objectAtIndex:indexPath.row];// removing -1 as we dont have "All"
        selectedRoutine = data.routineName;
//    }
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineName", selectedRoutine,
                                   nil];
    [Flurry logEvent:@"RoutineAnalyzed" withParameters:articleParams];
    
    //[self performSegueWithIdentifier:@"analyzeRoutineSegue" sender:self];
    [self performSegueWithIdentifier:@"analyzeHomeSegue" sender:self];

}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

//-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Remove seperator inset
//    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
//        [cell setSeparatorInset:UIEdgeInsetsZero];
//    }
//    
//    // Prevent the cell from inheriting the Table View's margin settings
//    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
//        [cell setPreservesSuperviewLayoutMargins:NO];
//    }
//    
//    // Explictly set your cell's layout margins
//    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
//        [cell setLayoutMargins:UIEdgeInsetsZero];
//    }
//}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"analyzeRoutineSegue"]) {
        AnalyzeVC *destVC = segue.destinationViewController;
        destVC.routineName = selectedRoutine;
    } else if ([segue.identifier isEqualToString:@"analyzeHomeSegue"]) {
        AnalysisHome *destVC = segue.destinationViewController;
        destVC.routineName = selectedRoutine;
    }
    self.title = @"";
}



@end
