//
//  SelectRoutine.h
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "MPCoachMarks.h"

@interface SelectRoutine : UIViewController <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, MPCoachMarksViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSString *date;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;

@end
