//
//  MilestonesVC.m
//  gyminutes
//
//  Created by Mayank Verma on 7/7/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "MilestonesVC.h"
#import "commons.h"
#import "Utilities.h"
@interface MilestonesVC() {
    NSDictionary *dataDict;
    NSMutableDictionary *newDataDict;
    float weightLifted;
    int pointerCell;
    UIColor *appColor;
    JVFloatLabeledTextField *value;
    UIView *headerWeightView;
    UIView *headerLine;
}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation MilestonesVC

@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    dataDict = @{
                 @1: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone1"],
                         @"title": @"Rolls-Royce Phantom",
                         @"desc" : @"Rolls-Royce Phantom weighs around 5,840 lbs. Fun Fact: Rolls-Royce is owner by BMW. Rolls-Royce Phantom was first manufactured in 2003 and costs about $417k.",
                         @"weight": @"5k lbs",
                         @"lifted": @5000
                         },
                 @2: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone2"],
                         @"title": @"Tyrannosaurus (T Rex)",
                         @"desc" : @"T REX weighs around 17000 lbs.",
                         @"weight": @"25K lbs",
                         @"lifted": @25000
                         },
                 @3: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone3"],
                         @"title": @"Windmill",
                         @"desc" : @"A windmill can weight between 40k lbs - 200K lbs, depending on size.",
                         @"weight": @"50K lbs",
                         @"lifted": @50000
                         },
                 @4: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone4"],
                         @"title": @"Semi Truck",
                         @"desc" : @"A fully loaded semitruck weighs 80 - 100K lbs.",
                         @"weight": @"100K lbs",
                         @"lifted": @100000
                         },
                 @5: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone5"],
                         @"title": @"Blue Whale",
                         @"desc" : @"Blue whale can weight between 150K – 275K lbs. The maximum recorded weight is around 350K lbs.",
                         @"weight": @"250K lbs",
                         @"lifted": @250000
                         },
                 @6: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone6"],
                         @"title": @"Statue of Liberty",
                         @"desc" : @"Weighs about 450k lbs",
                         @"weight": @"500K lbs",
                         @"lifted": @500000
                         },
                 @7: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone7"],
                         @"title": @"Boeing 747",
                         @"desc" : @"Boeing 747 maximum takeoff weight can range between 740K - 970K lbs.",
                         @"weight": @"750K lbs",
                         @"lifted": @750000
                         },
                 @8: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone8"],
                         @"title": @"International Space Station",
                         @"desc" : @"ISS weights around 900K lbs",
                         @"weight": @"1.0M lbs",
                         @"lifted": @1000000
                         },
                 @9: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone9"],
                         @"title": @"Clouds",
                         @"desc" : @"An actual cloud can weight around 1.1M - 1.25M lbs.",
                         @"weight": @"1.25M lbs",
                         @"lifted": @1250000
                         },
                 @10: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone10"],
                         @"title": @"Christ the Redeemer",
                         @"desc" : @"The statue of christ weighs around 1.4M lbs",
                         @"weight": @"1.5M lbs",
                         @"lifted": @1500000
                         },
                 @11: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone11"],
                         @"title": @"F16 Fighter Fleet",
                         @"desc" : @"One F16 fighter plane weighs arounf 38K lbs. A fleet of 50 F16 will weigh 1.75M lbs ",
                         @"weight": @"1.75M lbs",
                         @"lifted": @1750000
                         },
                 @12: @{
                         @"image": [UIImage imageNamed:@"ImgMilestone12"],
                         @"title": @"Giant Sequoia Trees",
                         @"desc" : @"Giant Sequoia trees can weight 3.0M lbs.",
                         @"weight": @"2.0M lbs",
                         @"lifted": @2000000
                         },

                 };
    appColor = [Utilities getAppColor];

    headerLine = [[UIView alloc] initWithFrame:CGRectMake(97, 70, 5, 100)];
    headerLine.backgroundColor = appColor;

    
    headerWeightView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.navigationController.navigationBar.frame), self.view.frame.size.width, 130)];
    
    
    value = [[JVFloatLabeledTextField alloc] init];
    value.textAlignment = NSTextAlignmentCenter;
    value.textColor = [UIColor whiteColor];
    [value setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:34]];
    value.layer.cornerRadius = 5;
    
    value.placeholder = @"Weight Lifted";
    
    value.enabled = false;
    value.tintColor = [UIColor whiteColor];
    value.floatingLabel.textColor = [UIColor whiteColor];
    value.placeholderColor = [UIColor whiteColor];
    value.clipsToBounds = YES;
    [value sizeToFit];
    value.frame = CGRectMake(10, 20, CGRectGetWidth(headerWeightView.frame) - 20, 80);
    value.backgroundColor = appColor;
    
    [headerWeightView addSubview:headerLine];
    [headerWeightView addSubview:value];
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerWeightView.frame) + 10, self.view.frame.size.width, CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.navigationController.navigationBar.frame) - CGRectGetHeight(headerWeightView.frame) - 160) style:UITableViewStylePlain];
    
    //[tableView registerClass:[MilestoneCell class] forCellReuseIdentifier:@"HistoryCell"];

    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor whiteColor];
    tableView.emptyDataSetDelegate = self;
    tableView.emptyDataSetSource = self;
    
    NSLog(@"inside milestones...");
    [self.view addSubview:headerWeightView];
    [self.view addSubview:tableView];
    newDataDict = [[NSMutableDictionary alloc] init];

    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"MilestoneViewed" withParameters:articleParams];

    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];

}

-(void)viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];

    appColor = [Utilities getAppColor];
    headerLine.backgroundColor = appColor;
    
    value.backgroundColor = appColor;
    weightLifted = 0;
    NSArray *allSets = [ExerciseSet MR_findAll];
    for (ExerciseSet *set in allSets) {
        weightLifted += [set.rep floatValue] * [set.weight floatValue];
    }
    
    if ([Utilities isKgs]) {
        weightLifted = weightLifted*0.45;
    }
    
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    
    value.placeholder =  @"Weight Lifted";
    value.text = [NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:weightLifted]], [[Utilities getUnits] lowercaseString]];

    
    if (weightLifted < 5000)
        headerLine.backgroundColor = [UIColor whiteColor];
    
    [self indicatorCell];
    [self.tableView reloadData];
    //[self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:11 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    //[self sendTestEmail];
}

-(void) sendTestEmail {
    [PFCloud callFunctionInBackground:@"welcomeEmail" withParameters:@{@"templateName" : @"WelcomeSignup",
                                                                       @"toEmail" :@"verma.mayank@gmail.com",
                                                                       @"toName": @"Mayank"}
                                block:^(NSString *response, NSError *error) {
                                    if (!error) {
                                        NSLog(@"email successfully sent..");
                                    } else {
                                        NSLog(@"Error sending email: %@\n%@", error.description, error);
                                    }
                                }];
}
-(void) indicatorCell {
    [newDataDict removeAllObjects];
    
    pointerCell = 1;
    for (int i = 1; i <=12 ; i++) {
        NSDictionary *item = [dataDict objectForKey:[NSNumber numberWithInt:i]];
        if ([[item objectForKey:@"lifted"] floatValue] <= weightLifted) {
            pointerCell = i;
            [newDataDict setObject:item forKey:[NSNumber numberWithInt:i]];
        }
        else {
            break;
        }
    }
    NSLog(@"pointer %d", pointerCell);
}
#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [[newDataDict allKeys] count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    // Similar to UITableViewCell, but
    UITableViewCell *cell = (UITableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    //MilestoneCell *cell = (MilestoneCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];

    UIView *line, *circle, *connectLine, *container;
    UILabel *weight;
    UIImageView *image;
    UILabel *name ;

    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
        line = [[UIView alloc] init];//
        line.frame  = CGRectMake(97, 0, 5, 140);
        
        line.backgroundColor = [UIColor whiteColor];
        line.tag = 100;
        
        circle = [[UIView alloc] initWithFrame:CGRectMake(90, 20, 20, 20)];
        circle.backgroundColor = [UIColor whiteColor];
        circle.layer.cornerRadius = 10;
        circle.tag = 200;

        connectLine = [[UIView alloc] initWithFrame:CGRectMake(105, 27, 30, 5)];
        connectLine.backgroundColor = [UIColor whiteColor];
        connectLine.tag = 300;
        
        container = [[UIView alloc] initWithFrame:CGRectMake(130, 10, CGRectGetWidth(self.tableView.frame) - 150, 130)];
        container.layer.cornerRadius = 5;
        container.tag = 400;

        [cell.contentView addSubview:line];
        [cell.contentView addSubview:circle];
        [cell.contentView addSubview:connectLine];
        
        weight = [[UILabel alloc] init];
        weight.textColor = appColor;
        weight.textAlignment = NSTextAlignmentCenter;
        [weight setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
        [weight sizeToFit];
        weight.tag = 1;
        weight.frame = CGRectMake(0, 15, 100, 30);
        weight.backgroundColor = [UIColor clearColor];
        
        image = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, CGRectGetWidth(container.frame) - 30, 80)];
        [image setContentMode:UIViewContentModeScaleAspectFit];
        image.backgroundColor = [UIColor clearColor];
        image.tag = 2;
        
        name = [[UILabel alloc] init];
        name.textAlignment = NSTextAlignmentCenter;
        name.textColor = [UIColor whiteColor];
        [name setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
        name.backgroundColor = [UIColor clearColor];
        [name sizeToFit];
        name.frame = CGRectMake(0, CGRectGetMaxY(image.frame), CGRectGetWidth(container.frame), 30);
        name.tag = 3;
        
        [container addSubview:image];
        [container addSubview:name];
        
        [cell.contentView addSubview:container];
        [cell.contentView addSubview:weight];
    }
    
//    for (UIView *lbl in [cell.contentView subviews]) {
//        if ([lbl isKindOfClass:[UILabel class]]) {
//            [lbl removeFromSuperview];
//        } else if ([lbl isKindOfClass:[UIImageView class]]) {
//            [lbl removeFromSuperview];
//        } else if ([lbl isKindOfClass:[UIView class]]) {
//            if (lbl.tag >=100 && lbl.tag <= 400)
//            [lbl removeFromSuperview];
//        }
//    }
    
    NSDictionary *item = [newDataDict objectForKey:[NSNumber numberWithInt:(int)[[newDataDict allKeys] count] - (int)indexPath.row]];
        
        weight = (UILabel *) [cell viewWithTag:1];
        image = (UIImageView *) [cell viewWithTag:2];
        name  = (UILabel *) [cell viewWithTag:3];
        line = (UIView *) [cell viewWithTag:100];
        circle = (UIView *) [cell viewWithTag:200];
        connectLine = (UIView *) [cell viewWithTag:300];
        container = (UIView *) [cell viewWithTag:400];
        
        weight.text = [item objectForKey:@"weight"];
        [image setImage:[item objectForKey:@"image"]];
        name.text = [item objectForKey:@"title"];
    
//    if (indexPath.row == [[newDataDict allKeys] count] -1) {
//        line.frame  = CGRectMake(97, 0, 5, 40);
//        NSLog(@"which milestone %@", name.text);
//    }

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        if ([[item objectForKey:@"lifted"] floatValue] < weightLifted) {
            line.backgroundColor = appColor;
            container.backgroundColor = appColor;
            circle.backgroundColor = appColor;
            connectLine.backgroundColor = appColor;
        }
        else {
            line.backgroundColor = [UIColor whiteColor];
            circle.backgroundColor = [UIColor whiteColor];
            connectLine.backgroundColor = [UIColor whiteColor];
            container.backgroundColor = FlatRed;
        }

    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    NSDictionary *item = [newDataDict objectForKey:[NSNumber numberWithInt:(int)[[newDataDict allKeys] count] - (int)indexPath.row]];
    NSLog(@"selected %ld row", (long)indexPath.row);
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    float timesWeight = weightLifted/[[item objectForKey:@"lifted"] floatValue];
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"MILESTONE ACHIEVED" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"You have lifted %@ %@ till date.", [numberformatter stringFromNumber:[NSNumber numberWithFloat:weightLifted]], [[Utilities getUnits] lowercaseString]] attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ weighs %@ (%.1f kgs). You have lifted %.1f times its weight. Congratulations on reaching this milestone.", [item objectForKey:@"title"], [item objectForKey:@"weight"], [[item objectForKey:@"weight"] floatValue]*0.45, timesWeight] attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
//    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 35)];
//    [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
//    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
//    [button setTitle:@"Share" forState:UIControlStateNormal];
//    button.backgroundColor = [UIColor whiteColor];
//    button.layer.cornerRadius = 4;
//    button.selectionHandler = ^(CNPPopupButton *button){
////        [self.popupController dismissPopupControllerAnimated:YES];
////        NSLog(@"Block for button: %@", button.titleLabel.text);
//    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    lineOneLabel.textColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[item objectForKey:@"image"]];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    lineTwoLabel.textColor = [UIColor whiteColor];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, imageView, lineTwoLabel]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = [Utilities getAppColor];
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 140;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"";
    
    if (weightLifted == 0)
        text = @"Welcome to Milstone. No workout recorded yet. Start recording weights to reach new milestones.";
    else if (weightLifted < 5000)
        text = @"First volume milestone is 5000 lbs.";
            
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"To start a workout, select \"HOME\" (Bottom Left) and click \"START WORKOUT\"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"Icon_Home"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

@end
