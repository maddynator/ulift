//
//  WorkoutList+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "WorkoutList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface WorkoutList (CoreDataProperties)

+ (NSFetchRequest<WorkoutList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *dayName;
@property (nullable, nonatomic, copy) NSNumber *exerciseGroup;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSNumber *exerciseNumber;
@property (nullable, nonatomic, copy) NSNumber *exerciseNumInGroup;
@property (nullable, nonatomic, copy) NSNumber *isTempExercise;
@property (nullable, nonatomic, copy) NSString *majorMuscle;
@property (nullable, nonatomic, copy) NSString *muscle;
@property (nullable, nonatomic, copy) NSNumber *repsSuggested;
@property (nullable, nonatomic, copy) NSNumber *restSuggested;
@property (nullable, nonatomic, copy) NSString *routiineName;
@property (nullable, nonatomic, copy) NSNumber *setsSuggested;
@property (nullable, nonatomic, copy) NSString *timeStamp;
@property (nullable, nonatomic, copy) NSString *workoutName;
@property (nullable, nonatomic, copy) NSString *repsPerSet;
@property (nullable, nonatomic, copy) NSString *restPerSet;

@end

NS_ASSUME_NONNULL_END
