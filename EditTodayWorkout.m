//
//  EditTodayWorkout.m
//  gyminutes
//
//  Created by Mayank Verma on 6/13/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "EditTodayWorkout.h"
#import "commons.h"

@interface EditTodayWorkout () {
    NSMutableArray *exerciseInWorkout, *exerciseGroupInWorkout;
    NSIndexPath *selectedIndex;
    BOOL changesMade, groupCreate;
    NSManagedObjectContext *localContext;
    UIToolbar* numberToolbar;
    UIBarButtonItem *editButton;
    UITextField *newLocationTextField;
    WorkoutList *selectedExercise;
    NSMutableArray *repArray, *amarpArray, *timerArray;
    UILabel *setsValue;
    int setCount;
}
@property (nonatomic, strong) CNPPopupController *popupController;


@end

@implementation EditTodayWorkout

@synthesize popupController, coachMarksView, workoutName, routineName;

- (void)viewDidLoad {
    [super viewDidLoad];
    localContext = [NSManagedObjectContext MR_defaultContext];
    changesMade = false;
    groupCreate = false;
    repArray = [[NSMutableArray alloc] init];
    amarpArray = [[NSMutableArray alloc] init];
    timerArray = [[NSMutableArray alloc] init];
    self.title = @"EDIT WORKOUT";
    NSLog(@"**** EDIT WORKOUT NEW ****");
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addExercise)];
    editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleDone target:self action:@selector(EditTable:)];
    
    
    self.navigationItem.rightBarButtonItems = @[addBtn, editButton];//, saveBtn];
    [self getExercises];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    numberToolbar.items = [NSArray arrayWithObjects:
                           //                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    exerciseInWorkout = [[NSMutableArray alloc] init];
    exerciseGroupInWorkout = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,                                
                                   nil];
    [Flurry logEvent:@"WorkoutListEdited" withParameters:articleParams];

}

- (IBAction) EditTable:(id)sender{
    bool allowEdit = false;
    
    if ([Utilities showPowerRoutinePackage]) {
        [self showPurchasePopUp:@"Creating multi-set exercises is restricted in free version"];
    } else {
        allowEdit = true;
    }
    
    if (allowEdit == true) {
        if(self.editing)
        {
            [super setEditing:NO animated:NO];
            [self.tableView setEditing:NO animated:YES];
            
            editButton.title = @"Edit";
            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
            [self reorderExerciseNumber];
            //        [self.tableView reloadData];
            //        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            //            int count = 0;
            //            for(UserWorkout *entry in exerciseInWorkout) {
            //                NSLog(@"$$$$ entry %@, %@, %@, %@", entry.exerciseGroup, entry.exerciseNumInGroup, entry.exerciseName, entry.exerciseNumber);
            //                entry.exerciseNumber = [NSNumber numberWithInt:count];
            //                entry.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
            //                count++;
            //            }
            //        }];
        }
        else
        {
            [super setEditing:YES animated:YES];
            editButton.title = @"Done";
            [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
            [self.tableView setEditing:YES animated:YES];
            //        [self.tableView reloadData];
            
        }
    }
}


- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [self.tableView setContentInset:edgeInsets];
        [self.tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [self.tableView setContentInset:edgeInsets];
        [self.tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)cancelNumberPad{
}

-(void)doneWithNumberPad{
    
    [self saveExerciseChanges];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [self getExercises];
   // [self printAllWorkoutSets];
}

//-(void) printAllWorkoutSets {
//    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
//    NSArray *workoutArray = [WorkoutList MR_findAllInContext:localContext];
//    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
//        for (WorkoutList *workout in workoutArray) {
//            // if we have already updated them, we dont do it again
//            if (workout.restPerSet == nil || workout.repsPerSet == nil)
//                NSLog(@"Date:%@ %@", workout.date, workout);
//        }
//    } completion:^(BOOL contextDidSave, NSError *error) {
//        NSLog(@"All workout list updated.");
//    }];
//}
-(void) viewDidDisappear:(BOOL)animated {
    [self reorderExerciseNumber];
    [self saveExerciseChanges];
}
-(void) getExercises {
    [exerciseInWorkout removeAllObjects];
    [exerciseGroupInWorkout removeAllObjects];
    NSArray *allExerciseInWorkout = [Utilities getAllExerciseForDate:_date];
    
    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    
    for (WorkoutList *data in allExerciseInWorkout) {
        [exerciseInWorkout addObject:data];
        
        //        NSString *groupString = [NSString stringWithFormat:@"%@", data.exerciseGroup];
        NSNumber *groupString = data.exerciseGroup;
        
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:groupString];
            [processedGroup addObject:data.exerciseGroup];
        }
    }
    NSLog(@"Number of groups are %lu %@", (unsigned long)[processedGroup count], processedGroup);
    for (int i = 0; i < [processedGroup count]; i++) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %d", i];
        
        NSArray *allExInWorkout = [exerciseInWorkout filteredArrayUsingPredicate:predicate];
        NSLog(@"AllExInWorkout %lu", (unsigned long)[allExInWorkout count]);
        NSMutableArray *ex = [[NSMutableArray alloc] init];
        for (WorkoutList *item in allExInWorkout) {
            [ex addObject:item];
            NSLog(@"print sets & reps: %@, %@", item.repsPerSet, item.restPerSet);
        }
        [exerciseGroupInWorkout addObject:ex];
        NSLog(@"exGroup %d, exNuminGroup %lu", i, (unsigned long)[[exerciseGroupInWorkout objectAtIndex:i] count]);
    }
    
    NSLog(@"exercise in workouts are %lu", (long)[exerciseInWorkout count]);
    
    if ([exerciseInWorkout count] > 0) {
        [self showCoachMarks];
    }
    [self.tableView reloadData];
}

-(void) addExercise {
    bool addEx = false;
    if ([Utilities showPowerRoutinePackage]) {
        [self showPurchasePopUp:@"Adding exercises is restricted in free version"];
    } else {
        addEx = true;
    }
    if (addEx) {
        [self performSegueWithIdentifier:@"addTempExerciseSegue" sender:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.tableView)
        return [exerciseGroupInWorkout count];
    else if (tableView == self.customRepsTableView)
        return 1;
    else // if (tableView == self.customRestTableView)
        return 1;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (tableView == self.tableView)
        return 5.01f;
    else if (tableView == self.customRepsTableView)
        return 0.1f;
    else //if (tableView == self.customRestTableView)
        return 0.1f;
    
}

-(UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if (tableView == self.tableView) {
        UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(header.frame)/3 - 5, 30)];
        
        int exCount = (int)[[exerciseGroupInWorkout objectAtIndex:section] count];
        
        if (exCount == 1) {
            lbl.text = [NSString stringWithFormat:@"EXERCISE # %d", (int)section + 1];
        } else if (exCount == 2) {
            lbl.text = [NSString stringWithFormat:@"SUPER SET"];
        } else if (exCount == 3 ) {
            lbl.text = [NSString stringWithFormat:@"TRI SET"];
        } else if (exCount > 3 ) {
            lbl.text = [NSString stringWithFormat:@"GIANT SET"];
        }
        
        if ([exerciseGroupInWorkout count] == 1) {
            lbl.text = [NSString stringWithFormat:@"CIRCUIT"];
        }
        WorkoutList *workoutInfo = [[exerciseGroupInWorkout objectAtIndex:section] objectAtIndex:0];
        header.backgroundColor = [Utilities getMajorMuscleColorSelected:workoutInfo.majorMuscle];
        
        lbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:14];
        lbl.textColor = [UIColor whiteColor];
        
        
        UIButton *upArrow = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(header.frame) - 50, 10, 50, 20)];
        UIImage *upImage = [UIImage imageNamed:@"IconUpArrow"];
        [upArrow setImage:upImage forState:UIControlStateNormal];
        upArrow.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        //    upArrow.backgroundColor = FlatWhiteDark;
        //    [edit setBackgroundImage:[UIImage imageNamed:@"IconMenu"] forState:UIControlStateNormal];
        [upArrow addTarget:self action:@selector(handleSectionUpMove:) forControlEvents:UIControlEventTouchUpInside];
        upArrow.tag = section;
        
        UIButton *downArrow = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(header.frame) - 100, 10, 50, 20)];
        UIImage *downImage = [UIImage imageNamed:@"IconDownArrow"];
        [downArrow setImage:downImage forState:UIControlStateNormal];
        downArrow.imageEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 15);
        [downArrow addTarget:self action:@selector(handleSectionDownMove:) forControlEvents:UIControlEventTouchUpInside];
        downArrow.tag = section;
        
        [header addSubview:lbl];
        [header addSubview:upArrow];
        [header addSubview:downArrow];
        return header;
    } else //if (tableView == self.customRepsTableView) {
        return [[UIView alloc] initWithFrame:CGRectZero];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView)
        return [[exerciseGroupInWorkout objectAtIndex:section] count];
    else //if (tableView == self.customRepsTableView)
        return setCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tableView) {
        
        
        WorkoutList *workoutInfo = [[exerciseGroupInWorkout objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        for (JVFloatLabeledTextField *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[JVFloatLabeledTextField class]]) {
                [lbl removeFromSuperview];
            } else {
            }
        }
        for (UILabel *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            } else {
                //            NSLog(@"catn remove label");
            }
        }
        
        
        float cellWidth = CGRectGetWidth(cell.frame);
        
        UILabel *exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, cellWidth, 30)];
        exerciseName.textAlignment = NSTextAlignmentCenter;
        exerciseName.tag = 100;
        exerciseName.textColor = [UIColor whiteColor];
        [exerciseName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
        exerciseName.text = workoutInfo.exerciseName;

        cell.backgroundColor = [Utilities getMajorMuscleColor:workoutInfo.majorMuscle];


        UIButton *cellBtn = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(exerciseName.frame), cellWidth - 20, CGRectGetHeight(cell.frame) - CGRectGetHeight(exerciseName.frame) - 10)];
        cellBtn.backgroundColor = [Utilities getMajorMuscleColorSelected:workoutInfo.majorMuscle];
        cellBtn.layer.cornerRadius = 5;
        
        UILabel *setsLbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 50, CGRectGetHeight(cellBtn.frame))];
        setsLbl.text = [NSString stringWithFormat:@"Sets\n%@", workoutInfo.setsSuggested];
        setsLbl.numberOfLines = 2;
        setsLbl.textAlignment = NSTextAlignmentCenter;
        setsLbl.textColor = [UIColor whiteColor];
        
        [cellBtn addSubview:setsLbl];

        UILabel *repsLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsLbl.frame) + 5, 5, CGRectGetWidth(cellBtn.frame) - CGRectGetWidth(setsLbl.frame) - 20, 20)];
        repsLbl.text = [NSString stringWithFormat:@"Reps: %@", workoutInfo.repsPerSet];
        repsLbl.textAlignment = NSTextAlignmentCenter;
        repsLbl.textColor = [UIColor whiteColor];
        
        [cellBtn addSubview:repsLbl];

        UILabel *restLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsLbl.frame) + 5, CGRectGetMaxY(repsLbl.frame) + 3, CGRectGetWidth(cellBtn.frame) - CGRectGetWidth(setsLbl.frame) - 20, 20)];
        restLbl.text = [NSString stringWithFormat:@"Rest: %@", workoutInfo.restPerSet];
        restLbl.textColor = [UIColor whiteColor];
        restLbl.textAlignment = NSTextAlignmentCenter;
        
        [cellBtn addTarget:self action:@selector(changeRepsPerSet:) forControlEvents:UIControlEventTouchUpInside];
        [cellBtn addSubview:restLbl];

        
        [setsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        [repsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        [restLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        
        [cell.contentView addSubview:exerciseName];
        [cell.contentView addSubview:cellBtn];
        
        
        //configure right buttons
        if ([[exerciseGroupInWorkout objectAtIndex:indexPath.section] count] > 1) {
            cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]],
                                  [MGSwipeButton buttonWithTitle:@"Ungroup" backgroundColor:FlatTeal]];
        } else {
            cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
        }
        
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate = self;
        
//        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
//        
//        lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
//        [cell.contentView addSubview:lineView];

        /*
        
        UILabel *setsLbl = [[UILabel alloc] initWithFrame:CGRectMake(gapWidth*2, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
        UILabel *repsLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsLbl.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
        UILabel *restTimeLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repsLbl.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
        
        setsLbl.text = @"Sets";
        repsLbl.text = @"Reps";
        restTimeLbl.text = @"Rest Timer(s)";
        
        setsLbl.textColor = floatingLabelColor;
        repsLbl.textColor = floatingLabelColor;
        restTimeLbl.textColor = floatingLabelColor;
        [setsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
        [repsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
        [restTimeLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
        setsLbl.textAlignment = repsLbl.textAlignment = restTimeLbl.textAlignment = NSTextAlignmentCenter;
        
        [cell.contentView addSubview:setsLbl];
        [cell.contentView addSubview:repsLbl];
        [cell.contentView addSubview:restTimeLbl];
        
        //UITextField *sets = [[UITextField alloc] initWithFrame:CGRectMake(gapWidth*2, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
        //    UITextField *reps = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sets.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame)+ 15, smallCellWidth, 30)];
        UIButton *sets = [[UIButton alloc] initWithFrame:CGRectMake(gapWidth*2, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
        UIButton *reps = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sets.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame)+ 15, smallCellWidth, 30)];
        
        //UITextField *restTime = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(reps.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
        UIButton *restTime = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(reps.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
        
        exerciseName.textAlignment = NSTextAlignmentCenter;
        
        sets.layer.cornerRadius = 5;
        reps.layer.cornerRadius = 5;
        restTime.layer.cornerRadius = 5;
        
        //[restTime setBorderStyle:UITextBorderStyleRoundedRect];
        // as we are calling the same method for both rest and rep.
        [sets addTarget:self action:@selector(changeRepsPerSet:) forControlEvents:UIControlEventTouchUpInside];
        [reps addTarget:self action:@selector(changeRepsPerSet:) forControlEvents:UIControlEventTouchUpInside];
        [restTime addTarget:self action:@selector(changeRepsPerSet:) forControlEvents:UIControlEventTouchUpInside];
        //[restTime addTarget:self action:@selector(changeRestPerSet:) forControlEvents:UIControlEventTouchUpInside];
        exerciseName.tag = 100;
        sets.tag = 101;
        reps.tag = 102;
        restTime.tag = 103;
        
        cell.backgroundColor = [Utilities getMajorMuscleColor:workoutInfo.majorMuscle];
        restTime.backgroundColor = reps.backgroundColor = sets.backgroundColor = [Utilities getMajorMuscleColorSelected:workoutInfo.majorMuscle];
        
        
        //    sets.placeholder =  @"     Sets";
        //    reps.placeholder = @"      Reps";
        //    restTime.placeholder = @"      Rest Time(s)";
        
        exerciseName.textColor = [UIColor whiteColor];
        sets.titleLabel.textColor = [UIColor whiteColor];
        reps.titleLabel.textColor = [UIColor whiteColor];
        restTime.titleLabel.textColor = [UIColor whiteColor];
        
        [exerciseName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
        [sets.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        [reps.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        [restTime.titleLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        
        NSLog(@"exercise name %@, Num: %d, Gr:%@, ExInGrp:%@", workoutInfo.exerciseName, [workoutInfo.exerciseNumber intValue], workoutInfo.exerciseGroup, workoutInfo.exerciseNumInGroup);
        exerciseName.text = workoutInfo.exerciseName;
        [sets setTitle:[NSString stringWithFormat:@"%d", [workoutInfo.setsSuggested intValue]] forState:UIControlStateNormal];
        [reps setTitle:[NSString stringWithFormat:@"%@", (workoutInfo.repsPerSet == nil) ? workoutInfo.repsSuggested: workoutInfo.repsPerSet] forState:UIControlStateNormal];
        [restTime setTitle:[NSString stringWithFormat:@"%@", (workoutInfo.restPerSet == nil) ? workoutInfo.restSuggested: workoutInfo.restPerSet] forState:UIControlStateNormal];
        
        [cell.contentView addSubview:exerciseName];
        [cell.contentView addSubview:sets];
        [cell.contentView addSubview:reps];
        [cell.contentView addSubview:restTime];
        
        //configure right buttons
        if ([[exerciseGroupInWorkout objectAtIndex:indexPath.section] count] > 1) {
            cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]],
                                  [MGSwipeButton buttonWithTitle:@"Ungroup" backgroundColor:FlatTeal]];
        } else {
            cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
        }
        
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate = self;
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
        
        lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
        [cell.contentView addSubview:lineView];
         */
        return cell;
         
    } else if (tableView == self.customRepsTableView) {
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepsCell"];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int itemHeight = 35;
        float popupWidth = CGRectGetWidth(cell.frame);
        UILabel *setNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 75, 30)];
        setNum.text = [NSString stringWithFormat:@"%d", (int)indexPath.row + 1];
        setNum.textAlignment = NSTextAlignmentCenter;
        //setNum.backgroundColor = FlatRed;
        
        UIButton *repMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), 5, 30, itemHeight)];
        [repMinus setTitle:@"-" forState:UIControlStateNormal];
        repMinus.backgroundColor = FlatMintDark;
        repMinus.tag = 1000 + indexPath.row;
        [repMinus addTarget:self action:@selector(repDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *repTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinus.frame), 5, popupWidth/4, itemHeight)];
        
        repTextFied.placeholder = @"Reps";
        repTextFied.textAlignment = NSTextAlignmentCenter;
        repTextFied.editable = false;
        repTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        repTextFied.tag = 2000 + indexPath.row;
        repTextFied.textContainer.maximumNumberOfLines = 1;
        repTextFied.scrollEnabled = false;
        //repTextFied.floatingLabelYPadding = 0;
        repTextFied.placeholderYPadding = -10;
        repTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        
        UIButton *repPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repTextFied.frame), 5, 30, itemHeight)];
        [repPlus setTitle:@"+" forState:UIControlStateNormal];
        repPlus.backgroundColor = FlatMintDark;
        repPlus.tag = 3000 + indexPath.row;
        [repPlus addTarget:self action:@selector(repInc:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == true) {
            repMinus.enabled = false;
            repPlus.enabled = false;
            repTextFied.backgroundColor = FlatGrayDark;
            repTextFied.text = @"MAX";
        } else {
            repMinus.enabled = true;
            repPlus.enabled = true;
            repTextFied.backgroundColor = [UIColor whiteColor];
            repTextFied.text = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:indexPath.row]];
        }

        UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];

        WPmaskLayer.frame = self.view.bounds;
        WPmaskLayer.path  = WPmaskPath.CGPath;
        WMmaskLayer.frame = self.view.bounds;
        WMmaskLayer.path  = WMmaskPath.CGPath;
        repPlus.layer.mask = WPmaskLayer;
        repMinus.layer.mask = WMmaskLayer;

        UISwitch *amarpSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.frame) - 75, 5, 75, 30)];
        amarpSwitch.tag = 4000 + indexPath.row;
        //amarpSwitch.tintColor = FlatRed;
        amarpSwitch.onTintColor = FlatGreen;
        [amarpSwitch setOn:[[amarpArray objectAtIndex:indexPath.row] boolValue]];
        [amarpSwitch addTarget:self action:@selector(amarpIsChanged:) forControlEvents:UIControlEventValueChanged];
        
        
        UIButton *timerMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerMinus setTitle:@"-" forState:UIControlStateNormal];
        timerMinus.backgroundColor = FlatMintDark;
        timerMinus.tag = 5000 + indexPath.row;
        [timerMinus addTarget:self action:@selector(timerDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *timerTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerMinus.frame), CGRectGetMaxY(repMinus.frame) + 5, popupWidth/4, itemHeight)];
        
        timerTextFied.placeholder = @"Rest";
        timerTextFied.textAlignment = NSTextAlignmentCenter;
        timerTextFied.editable = false;
        timerTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        timerTextFied.tag = 6000 + indexPath.row;
        timerTextFied.textContainer.maximumNumberOfLines = 1;
        timerTextFied.scrollEnabled = false;
        timerTextFied.placeholderYPadding = -10;
        timerTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        timerTextFied.text = [NSString stringWithFormat:@"%d", [[timerArray objectAtIndex:indexPath.row] intValue]];
        
        UIButton *timerPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerTextFied.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerPlus setTitle:@"+" forState:UIControlStateNormal];
        timerPlus.backgroundColor = FlatMintDark;
        timerPlus.tag = 7000 + indexPath.row;
        [timerPlus addTarget:self action:@selector(timerInc:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *TPmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *TMmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *TPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *TMmaskLayer = [[CAShapeLayer alloc] init];
        
        TPmaskLayer.frame = self.view.bounds;
        TPmaskLayer.path  = TPmaskPath.CGPath;
        TMmaskLayer.frame = self.view.bounds;
        TMmaskLayer.path  = TMmaskPath.CGPath;
        
        
        timerPlus.layer.mask = TPmaskLayer;
        timerMinus.layer.mask = TMmaskLayer;

        
        [cell.contentView addSubview:setNum];
        [cell.contentView addSubview:repMinus];
        [cell.contentView addSubview:repTextFied];
        [cell.contentView addSubview:repPlus];
        [cell.contentView addSubview:amarpSwitch];
        [cell.contentView addSubview:timerMinus];
        [cell.contentView addSubview:timerTextFied];
        [cell.contentView addSubview:timerPlus];
        
        return cell;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.tableView)
        return 40;
    else //if (tableView == self.customRepsTableView)
        return 0.1f;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView)
        return 90;
    else //if (tableView == self.customRepsTableView)
        return 90;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
    //    [self performSegueWithIdentifier:@"workoutEditSegue" sender:self];
    if (tableView == self.tableView) {
        NSLog(@"something edited...");
        selectedIndex = indexPath;
    //    [tableView endEditing:YES];
    } else {
        
    }
}

#pragma mark Row reordering

-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView)
        return YES;
    else
        return NO;
}
-(BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView)
        return YES;
    else
        return NO;

}
- (void)moveSection:(NSInteger)section toSection:(NSInteger)newSection {
//    NSLog(@"Will now move %ld to new section %ld",(long) section, (long)newSection);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
    
    NSMutableArray *fromArr = [exerciseGroupInWorkout objectAtIndex:sourceIndexPath.section];
    WorkoutList *item = [fromArr objectAtIndex:sourceIndexPath.row];
    [fromArr removeObjectAtIndex:sourceIndexPath.row];
    [exerciseGroupInWorkout removeObjectAtIndex:sourceIndexPath.section];
    [exerciseGroupInWorkout insertObject:fromArr atIndex:sourceIndexPath.section];
    
    NSMutableArray *toArr = [exerciseGroupInWorkout objectAtIndex:destinationIndexPath.section];
    [toArr insertObject:item atIndex:destinationIndexPath.row];
    [exerciseGroupInWorkout removeObjectAtIndex:destinationIndexPath.section];
    [exerciseGroupInWorkout insertObject:toArr atIndex:destinationIndexPath.section];
    
    int i = 0, removeSectionIndex = -1;
    bool emptySection = false;
    NSMutableArray *emptySections = [[NSMutableArray alloc] init];
    for (NSMutableArray *itemArr in exerciseGroupInWorkout) {
        if ([itemArr count] == 0) {
            NSLog(@"no item in array %d", i);
            removeSectionIndex = i;
            emptySection = true;
            [emptySections addObject:[NSNumber numberWithInt:removeSectionIndex]];
        }
        i++;
    }
    
    if (emptySection) {
        for (NSNumber *index in emptySections) {
            [exerciseGroupInWorkout removeObjectAtIndex:[index intValue]];
        }
    }
    [self reorderExerciseNumber];
    //    [self.tableView reloadData  ];
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    return nil;
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    bool delEx = false;
    
    NSIndexPath * path = [self.tableView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        if ([Utilities showPowerRoutinePackage]) {
            [self showPurchasePopUp:@"Deleting exercises is restricted in free version"];
        } else {
            delEx = true;
        }
        if (delEx) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            
            [alert addButton:@"Yes" actionBlock:^{
                //TODO: we need to check if user has performed this workout. We have to delete all that or dont allow user to delete this
                
                WorkoutList *data = [[exerciseGroupInWorkout objectAtIndex:path.section] objectAtIndex:path.row];
                
                NSMutableArray *fromArr = [exerciseGroupInWorkout objectAtIndex:path.section];
                [fromArr removeObjectAtIndex:path.row];
                
                //    item.exerciseGroup = [NSNumber numberWithInteger:destinationIndexPath.section];
                //    item.exerciseNumInGroup = [NSNumber numberWithInteger:destinationIndexPath.row];
                //    [localContext MR_saveOnlySelfAndWait];
                
                int i = 0, removeSectionIndex = -1;
                bool emptySection = false;
                NSMutableArray *emptySections = [[NSMutableArray alloc] init];
                for (NSMutableArray *itemArr in exerciseGroupInWorkout) {
                    if ([itemArr count] == 0) {
                        NSLog(@"no item in array %d", i);
                        removeSectionIndex = i;
                        emptySection = true;
                        [emptySections addObject:[NSNumber numberWithInt:removeSectionIndex]];
                    }
                    i++;
                }
                
                if (emptySection) {
                    for (NSNumber *index in emptySections) {
                        [exerciseGroupInWorkout removeObjectAtIndex:[index intValue]];
                    }
                }
                                
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContextNew)  {
                    [data MR_deleteEntityInContext:localContextNew];
                } completion:^(BOOL contextDidSave, NSError *error) {
                    [self reorderExerciseNumber];
                    [self saveExerciseChanges];
                    [self.tableView reloadData];
                }];
                //            [localContext MR_saveToPersistentStoreAndWait];
                // this may not be necessary but we are still doing it for now..
            }];
            [alert showWarning:self title:@"Delete exercise?" subTitle:@"Are you sure you want to delete this exercise from workout?" closeButtonTitle:@"No" duration:0.0f];
        }
        return NO; //Don't autohide to improve delete expansion animation
    } else if (direction == MGSwipeDirectionRightToLeft && index == 1) {
        NSMutableArray *fromArr = [exerciseGroupInWorkout objectAtIndex:path.section];
        WorkoutList *item = [fromArr objectAtIndex:path.row];
        int newSectionIndex = (int)[exerciseGroupInWorkout count];
        item.exerciseGroup = [NSNumber numberWithInteger:newSectionIndex + 1];
        item.exerciseNumInGroup = [NSNumber numberWithInteger:0];
        [localContext MR_saveOnlySelfAndWait];
        
        [fromArr removeObjectAtIndex:path.row];
        NSMutableArray *newSec = [[NSMutableArray alloc] init];
        [newSec addObject:item];
        [exerciseGroupInWorkout addObject:newSec];
        [self reorderExerciseNumber];
        [self.tableView reloadData];
    }
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    changesMade = true;
    NSLog(@"user clicked on it.. may have made some change... lets save it...");
    textField.inputAccessoryView = numberToolbar;
    
    //    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.tableView];
    //    CGPoint contentOffset = self.tableView.contentOffset;
    //
    //    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
    //
    //    NSLog(@"contentOffset is: %@, %f", NSStringFromCGPoint(contentOffset), textField.inputAccessoryView.frame.size.height);
    //    [self.tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    //    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
    //    {
    //        CGPoint buttonPosition = [textField convertPoint:CGPointZero
    //                                                  toView: self.tableView];
    //        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    //
    //        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
    //    }
    
    return YES;
}


#pragma SAVE EXERCISE
-(void) saveExerciseChanges {
    [self.tableView endEditing:YES];
    
    if (changesMade == false) {
        NSLog(@"no changes made.. just scrolled.");
        return;
    }
    
    for (int section = 0; section < [exerciseGroupInWorkout count]; section++) {
        for (int row = 0; row < [[exerciseGroupInWorkout objectAtIndex:section ] count]; row++) {
            
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow: row inSection: section];
            
            UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
            WorkoutList *wkExercise = [[exerciseGroupInWorkout objectAtIndex:section] objectAtIndex:row];
            //NSLog(@"exName %@, set %d rep %d rest %d", wkExercise.exerciseName, [wkExercise.setsSuggested intValue], [wkExercise.repsSuggested intValue], [wkExercise.restSuggested intValue]);
            for (UIView *view in  cell.contentView.subviews){
                if ([view isKindOfClass:[UITextField class]]){
                    int sets = 0, reps = 0, restTimer = 0;
                    UITextField* txtField = (UITextField *)view;
                    
                    if (txtField.tag == 100) {
                        //                        NSLog(@"TextField.tag:%ld and Data %@", (long) txtField.tag, txtField.text);
                    }
                    if (txtField.tag == 101) {
                        //                        NSLog(@"TextField.tag:%ld and Data %@ set %@", (long)txtField.tag, txtField.text, wkExercise.sets);
                        sets = [txtField.text intValue];
                        if ([wkExercise.setsSuggested intValue] != sets) {
                            // we need to make sure that the number of sets is equal to repsPerSet and setsPerSet calculation.
                            NSLog(@"Old %d New %d", [wkExercise.setsSuggested intValue], sets);
                            if ([wkExercise.setsSuggested intValue] < sets) {
                                int diff = sets - [wkExercise.setsSuggested intValue];
                                [self addMoreSets:wkExercise setsToAdd:diff];
                            } else if ([wkExercise.setsSuggested intValue] > sets) {
                                int diff = [wkExercise.setsSuggested intValue] - sets;
                                [self deleteExtraSets:wkExercise setsToDelete:diff];
                            }
                            wkExercise.setsSuggested = [NSNumber numberWithInt:sets];
                        }
                    }
                    if (txtField.tag == 102) {
                        //                        NSLog(@"TextField.tag:%ld and Data %@, %@", (long)txtField.tag, txtField.text, wkExercise.reps);
                        reps = [txtField.text intValue];
                        if (wkExercise.repsSuggested != [NSNumber numberWithInt:reps]) {
                            wkExercise.repsSuggested = [NSNumber numberWithInt:reps];
                        }
                    }
                    if (txtField.tag == 103) {
                        //                        NSLog(@"TextField.tag:%ld and Data %@, %@", (long)txtField.tag, txtField.text, wkExercise.restTimer);
                        restTimer = [txtField.text intValue];
                        if (wkExercise.restSuggested != [NSNumber numberWithInt:restTimer]) {
                            wkExercise.restSuggested = [NSNumber numberWithInt:restTimer];
                        }
                    }
                } // End of Cell Sub View
            }// Counter
            [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
                if (!error) {
                    //                    NSLog(@"set saved...");
                }
            }];
        }
    }// section for loop
    changesMade = false;
    [self.tableView reloadData];
}

-(void) addMoreSets:(WorkoutList *) workoutList setsToAdd:(int) addSets {
    NSLog(@"Adding more sets %d %@", addSets, workoutList.repsPerSet);
    NSString *repsPerSet = workoutList.repsPerSet;
    NSString *restPerSet = workoutList.restPerSet;
    NSString *newReps = @"";
    NSString *newRest = @"";

    for (int i = 0; i < addSets; i++) {
        if ([newReps isEqualToString:@""]) {
            if ([repsPerSet isEqualToString:@""]) {
                newReps = [NSString stringWithFormat:@"%d", [Utilities getDefaultRepsValue]];
                newRest = [NSString stringWithFormat:@"%d", [Utilities getDefaultRestTimerValue]];
            } else {
                newReps = [NSString stringWithFormat:@",%d", [Utilities getDefaultRepsValue]];
                newRest = [NSString stringWithFormat:@",%d", [Utilities getDefaultRestTimerValue]];
            }
        } else {
            newReps = [NSString stringWithFormat:@"%@,%d", newReps, [Utilities getDefaultRepsValue]];
            newRest = [NSString stringWithFormat:@"%@,%d", newRest, [Utilities getDefaultRestTimerValue]];
        }
    }
    workoutList.repsPerSet = [NSString stringWithFormat:@"%@%@", repsPerSet, newReps];
    workoutList.restPerSet = [NSString stringWithFormat:@"%@%@", restPerSet, newRest];
    NSLog(@"%@--%@", workoutList.repsPerSet, workoutList.restPerSet);
}

-(void) deleteExtraSets:(WorkoutList *) workoutList setsToDelete:(int) deleteSets {
    NSLog(@"deleting sets %d", deleteSets);
    NSString *repsPerSet = workoutList.repsPerSet;
    NSString *newReps = @"";

    NSString *restPerSet = workoutList.restPerSet;
    NSString *newRest = @"";

    NSArray *repsSetsArray = [repsPerSet componentsSeparatedByString:@","];
    NSArray *restSetsArray = [restPerSet componentsSeparatedByString:@","];
    
    for (int i = 0; i < repsSetsArray.count - deleteSets; i++) {
        if ([newReps isEqualToString:@""]) {
            newReps = [NSString stringWithFormat:@"%@", [repsSetsArray objectAtIndex:i]];
            newRest =[NSString stringWithFormat:@"%@", [restSetsArray objectAtIndex:i]];
        } else {
            newReps = [NSString stringWithFormat:@"%@,%@", newReps, [repsSetsArray objectAtIndex:i]];
            newRest = [NSString stringWithFormat:@"%@,%@", newRest, [restSetsArray objectAtIndex:i]];
        }
        NSLog(@"NewReps %@, new Rest %@", newReps, newRest);
    }
    workoutList.repsPerSet = newReps;
    workoutList.restPerSet = newRest;
}

-(void) saveRepsPerSetExerciseChanges:(NSString *) repsPerSet {
    selectedExercise.repsPerSet = repsPerSet;
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        if (!error) {
        } else {
            NSLog(@"Reps Saved Succeessfullt");
        }
    }];
}

-(void) saveRestPerSetExerciseChanges:(NSString *) restPerSet {
    selectedExercise.restPerSet = restPerSet;
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        if (!error) {
        } else {
            NSLog(@"Rest Saved Succeessfullt");
        }
    }];
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No exercise added yet."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Please click + to add exercise.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

-(void) searchExercises {
    [self performSegueWithIdentifier:@"addTempExerciseSegue" sender:self];
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addTempExerciseSegue"]) {
        MuscleListTVC *destVC = segue.destinationViewController;
        destVC.date = [Utilities getCurrentDate];
        destVC.workoutName = workoutName;
        destVC.routineName = routineName;
        if ([exerciseGroupInWorkout count] == 0)
            destVC.exerciseGroup = [NSNumber numberWithInt:(int)[exerciseGroupInWorkout count]];
        else
            destVC.exerciseGroup = [NSNumber numberWithInt:(int)[exerciseGroupInWorkout count] + 1];
        destVC.exerciseNumInGroup = [NSNumber numberWithInt:0];
        destVC.isTempEx = true;
        NSLog(@"date being passed is %@ %@ %@", destVC.date, destVC.exerciseGroup, destVC.exerciseNumInGroup);
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ROUTINE PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ROUTINE_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@. Please upgrade to Premium or Power Routine Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

#pragma scroll view

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    //    NSLog(@"will save data now...");
    [self saveExerciseChanges];
}

-(void) reorderExerciseNumber {
    //[MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        int count = 0;
        for (int section = 0; section < [exerciseGroupInWorkout count]; section++) {
            for (int row = 0; row < [[exerciseGroupInWorkout objectAtIndex:section] count]; row++) {
                NSLog(@"**** UPDATING ENTRY Section:%d Row:%d", section, row);
                WorkoutList *entry = [[exerciseGroupInWorkout objectAtIndex:section] objectAtIndex:row];
                entry.exerciseNumber = [NSNumber numberWithInt:count];
                entry.exerciseGroup = [NSNumber numberWithInt:section];
                entry.exerciseNumInGroup = [NSNumber numberWithInt:row];
                count++;
            }
        }
    
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                [self saveExerciseChanges];
                [self.tableView reloadData];
            }
        }];
}

-(IBAction)handleSectionUpMove:(id)sender {
    UIButton *moveSectionBtn = (UIButton *)sender;
    int fromIndex = (int)moveSectionBtn.tag;
    if (fromIndex == 0) {
        NSLog(@"cant move above 0");
        return;
    }
    int toIndex = fromIndex - 1;
    NSArray *tempSection = [exerciseGroupInWorkout objectAtIndex:fromIndex];
    [exerciseGroupInWorkout removeObjectAtIndex:fromIndex];
    [exerciseGroupInWorkout insertObject:tempSection atIndex:toIndex];// since its starts from 0
    //        [self.tableView moveSection:secIndex toSection:newIndex];
    [self reorderExerciseNumber];
    [self saveExerciseChanges];
    [self.tableView reloadData];
}

-(IBAction)handleSectionDownMove:(id)sender {
    UIButton *moveSectionBtn = (UIButton *)sender;
    int fromIndex = (int)moveSectionBtn.tag;
    if (fromIndex + 1 == [exerciseGroupInWorkout count]) {
        NSLog(@"cant move below last ");
        return;
    }
    int toIndex = fromIndex + 1;
    NSArray *tempSection = [exerciseGroupInWorkout objectAtIndex:fromIndex];
    [exerciseGroupInWorkout removeObjectAtIndex:fromIndex];
    [exerciseGroupInWorkout insertObject:tempSection atIndex:toIndex];// since its starts from 0
    //        [self.tableView moveSection:secIndex toSection:newIndex];
    [self reorderExerciseNumber];
    [self saveExerciseChanges];
    [self.tableView reloadData];
}


-(void) showCoachMarks {
    
    bool showSuperSetHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownCreateWorkoutHint"];
    
    if ([Utilities isHintEnabled] || showSuperSetHint == false) {
        float width = self.view.frame.size.width;
        
        //        CGRect coachmark1 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame) + 10, 40, 38);
        CGRect coachmark2 = CGRectMake(width - 100, 0, 100, 40);
        CGRect coachmark3 = CGRectMake(0, -30,  0, 0);
        CGRect coachmark4 = CGRectMake(0, CGRectGetHeight(self.view.frame)/2, 0, 0);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                //                                @{
                                //                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                //                                    @"caption": @"Click detail button for workout summary.",
                                //                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Move group up and down",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark3],
                                    @"caption": @"\n\n\n\n\n\n\n\n\nClick 'EDIT' to combine exercises into supersets, tri-sets, giant sets or circuits.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark4],
                                    @"caption": @"Slide LEFT on exercise to UNGROUP (Remove from super-set) or DELETE.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    }
                                
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownCreateWorkoutHint"]);
    //    aleadyShowingHint = false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownCreateWorkoutHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma CUSTOM REPS
-(IBAction)changeRepsPerSet:(id)sender  {
    [self doneWithNumberPad];

    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.tableView];
    NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
    [self changeRepsForThisSet:indexPath];
}

-(NSString *) createRepsPerSet {
    NSString *repsPerSet = @"";
    
    for (int i = 0; i < repArray.count; i++) {
        if ([[amarpArray objectAtIndex:i] boolValue] == true) {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = @"MAX";//[NSString stringWithFormat:@"%d", [repArray objectAtIndex:i]];
            } else  {
                repsPerSet = [NSString stringWithFormat:@"%@,MAX", repsPerSet];
            }
        }
        else {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:i]];
            } else {
                repsPerSet = [NSString stringWithFormat:@"%@,%@", repsPerSet, [repArray objectAtIndex:i]];
            }
        }
        
    }
    return repsPerSet;
}

-(NSString *) createRestPerSet {
    NSString *restPerSet = @"";
    
    for (int i = 0; i < timerArray.count; i++) {
            if ([restPerSet isEqualToString:@""]) {
                restPerSet = [NSString stringWithFormat:@"%@", [timerArray objectAtIndex:i]];
            } else {
                restPerSet = [NSString stringWithFormat:@"%@,%@", restPerSet, [timerArray objectAtIndex:i]];
            }
    }
    return restPerSet;
}


//-(NSString *)temp_createRestPerSet:(int ) numSets rep:(int) repCount {
//    NSString *repsPerSet = @"";
//    
//    for (int i = 0; i < numSets; i++) {
//        if ([repsPerSet isEqualToString:@""])
//            repsPerSet = [NSString stringWithFormat:@"%d", repCount];
//        else
//            repsPerSet = [NSString stringWithFormat:@"%@,%d", repsPerSet, repCount];
//    }
//    return repsPerSet;
//}

- (void)changeRepsForThisSet:(NSIndexPath *) index {
    
    selectedExercise = [[exerciseGroupInWorkout objectAtIndex:index.section] objectAtIndex:index.row];
    
    [repArray removeAllObjects];
    [amarpArray removeAllObjects];
    [timerArray removeAllObjects];
    
    int sets  = [selectedExercise.setsSuggested  intValue];
    setCount = sets;
    
    NSString *repsPerSetStr = selectedExercise.repsPerSet;
    NSArray *repsArrayTemp = [repsPerSetStr componentsSeparatedByString:@","];
    
    NSString *timerPerSetStr = selectedExercise.restPerSet;
    NSArray *timerArrayTemp = [timerPerSetStr componentsSeparatedByString:@","];


    for (int i = 0; i < [selectedExercise.setsSuggested  intValue] ; i++) {
        //todo: replace with the new field anddo strtok on it. for now, add that many reps as is
        NSLog(@"%@", [repsArrayTemp objectAtIndex:i]);
        if ([[repsArrayTemp objectAtIndex:i] isEqualToString:@"MAX"]) {
            [amarpArray addObject:@1];
            [repArray addObject:@([Utilities getDefaultRepsValue])];// setting it back to default rep value.
        } else {
            [repArray addObject:@([[repsArrayTemp objectAtIndex:i] intValue])];
            [amarpArray addObject:@0];
        }
        [timerArray addObject:@([[timerArrayTemp objectAtIndex:i] intValue])];
    }
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:selectedExercise.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UIView *setsHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    setsValue = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    
    UIStepper *setsStepper = [[UIStepper alloc] init];
    setsStepper.value = sets;
    setsStepper.minimumValue = 1;
    setsStepper.maximumValue = 20;
    setsStepper.frame = CGRectMake(CGRectGetMaxX(setsHeader.frame) - 100, 0, 100, 30);
    [setsStepper addTarget:self action:@selector(setsValueChanged:) forControlEvents:UIControlEventValueChanged];
    [setsHeader addSubview:setsValue];
    [setsHeader addSubview:setsStepper];

    
    UIView *customHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    UILabel *setN  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    setN.text = @"Set #";
    setN.textAlignment = NSTextAlignmentCenter;
    //setN.backgroundColor = FlatRedDark;
    
    
    UILabel *setsNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setN.frame), 0, CGRectGetWidth(customHeader.frame)/2 + 10, 30)];
    setsNum.text = @"Per Set";
    setsNum.textAlignment = NSTextAlignmentCenter;
    
    UILabel *maxRep  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsNum.frame), 0, CGRectGetWidth(customHeader.frame)/4, 30)];
    maxRep.text = @"AMRAP";
    maxRep.textAlignment = NSTextAlignmentCenter;

    
    [customHeader addSubview:setN];
    [customHeader addSubview:setsNum];
    [customHeader addSubview:maxRep];
    float itemHeight = 35, defaultSets = 5;
    
    self.customRepsTableView = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(maxRep.frame), 0, 320, (itemHeight + 5) * defaultSets) style:UITableViewStylePlain];
    [self.customRepsTableView registerClass:[UITableViewCell self] forCellReuseIdentifier:@"RepsCell"];
    self.customRepsTableView.backgroundColor = [UIColor clearColor];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    self.customRepsTableView.delegate = self;
    self.customRepsTableView.dataSource = self;
    
    
    UIView *customBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    
    CNPPopupButton *cancelButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *saveExercisebtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cancelButton.frame) + 10, 0, 120, 40)];
    [saveExercisebtn setTitleColor:FlatBlueDark forState:UIControlStateNormal];
    saveExercisebtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    [saveExercisebtn setTitle:@"SAVE" forState:UIControlStateNormal];
    saveExercisebtn.backgroundColor = [UIColor whiteColor];
    saveExercisebtn.layer.cornerRadius = 4;
    saveExercisebtn.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self saveRepsPerSetExerciseChanges:[self createRepsPerSet]];
        [self saveRestPerSetExerciseChanges:[self createRestPerSet]];
        
        selectedExercise.setsSuggested = [NSNumber numberWithInt:setCount];
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                //                    NSLog(@"set saved...");
            }
        }];
        [self.tableView reloadData];
    };
    
    [customBtnView addSubview:cancelButton];
    [customBtnView addSubview:saveExercisebtn];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, setsHeader,customHeader, self.customRepsTableView, customBtnView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)repDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsTableView];
    NSIndexPath *indexPath = [self.customRepsTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"decreasing rep %ld", (long)indexPath.row);
        if ([[repArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] - 1)];
    }
    [self.customRepsTableView reloadData];
}
-(IBAction)repInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsTableView];
    NSIndexPath *indexPath = [self.customRepsTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rep %ld", (long)indexPath.row);
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] + 1)];
    }
    [self.customRepsTableView reloadData];
}
-(IBAction)amarpIsChanged:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsTableView];
    NSIndexPath *indexPath = [self.customRepsTableView indexPathForRowAtPoint:buttonPosition];
    NSLog(@"AMARP changed %ld", (long)indexPath.row);
    if (indexPath != nil)
    {
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == false) {
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@1];//true
        } else
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@0];
    }
    [self.customRepsTableView reloadData];
}

-(IBAction)timerDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsTableView];
    NSIndexPath *indexPath = [self.customRepsTableView indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSLog(@"decreasing rest %ld", (long)indexPath.row);
        if ([[timerArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] - 5)];
    }
    [self.customRepsTableView reloadData];
}
-(IBAction)timerInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsTableView];
    NSIndexPath *indexPath = [self.customRepsTableView indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rest %ld", (long)indexPath.row);
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] + 5)];
    }
    [self.customRepsTableView reloadData];
}

-(IBAction)setsValueChanged:(id)sender {
    UIStepper *setsVal = (UIStepper *)sender;
    int sets = setsVal.value;
    
    if (sets < setCount) {
        // remove sets
        [repArray removeObjectAtIndex:setCount - 1];
        [amarpArray removeObjectAtIndex:setCount - 1];
        [timerArray removeObjectAtIndex:setCount - 1];
    } else if (sets > setCount){
        // add sets
        [repArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRepsValue]]];
        [amarpArray addObject:@0];
        [timerArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRestTimerValue]]];
    }
    setCount = sets;
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    [self.customRepsTableView reloadData];
}

@end
