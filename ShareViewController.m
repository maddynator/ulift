//
//  ShareViewController.m
//  gyminutes
//
//  Created by Mayank Verma on 6/24/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "ShareViewController.h"
#define WIDTH_FACTOR    40
@interface ShareViewController () {
    NSArray *todayAchievements, *darkColor, *lightColor;
    float getMaxHeight, cellMinY;
    SMPageControl *horizontalPageControl;
    NSDateFormatter *fancyFormatter, *formatter;
}
@end

@implementation ShareViewController
@synthesize _collectionView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    darkColor = @[FlatSkyBlueDark, FlatMintDark, FlatRedDark, FlatMagentaDark, FlatYellowDark];
    lightColor = @[FlatSkyBlue, FlatMint, FlatRed, FlatMagenta, FlatYellow];

    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    self.navigationController.navigationBarHidden = NO;

    _collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    _collectionView.pagingEnabled = YES;
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:[UIColor whiteColor]];
    self.title = @"SHARE";
    [self.view addSubview:_collectionView];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"maxDate == %@", _date];
    todayAchievements = [ExerciseMetaInfo MR_findAllWithPredicate:datePredicate inContext:localContext];

    horizontalPageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(0, 75, CGRectGetWidth(self.view.frame), 30)];
    horizontalPageControl.alignment = SMPageControlAlignmentCenter;
    horizontalPageControl.hidesForSinglePage = YES;
    horizontalPageControl.enabled = false;
    horizontalPageControl.pageIndicatorTintColor = FlatBlack;
    horizontalPageControl.numberOfPages = [todayAchievements count];
    horizontalPageControl.currentPage = 0;
    horizontalPageControl.currentPageIndicatorTintColor = FlatWhite;
    
    [self.view addSubview:horizontalPageControl];
    fancyFormatter = [[NSDateFormatter alloc] init];
    [fancyFormatter setDateFormat:@"MMMM dd yyyy"];
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [todayAchievements count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.layer.cornerRadius = 5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    ExerciseMetaInfo *today = [todayAchievements objectAtIndex:indexPath.row];
    int randColor = rand()%[darkColor count];
    
    cell.backgroundColor = [darkColor objectAtIndex:randColor];
    _collectionView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.frame andColors:@[[lightColor objectAtIndex:randColor], [darkColor objectAtIndex:randColor]]];

    UILabel *header =[[UILabel alloc] init];
    header.text = @"Personal Record";
    header.textColor = [UIColor whiteColor];
    header.textAlignment = NSTextAlignmentCenter;
    [header setFont:[UIFont fontWithName:@HAL_REG_FONT size:30]];
    [header sizeToFit];
    header.frame = CGRectMake(0, 20, cell.frame.size.width, 30);
    
    UILabel *maxDate =[[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(header.frame), cell.frame.size.width, 30)];
    maxDate.text = [fancyFormatter stringFromDate:[formatter dateFromString:today.maxDate]];
    maxDate.textColor = [UIColor whiteColor];
    maxDate.textAlignment = NSTextAlignmentCenter;
    [maxDate setFont:[UIFont fontWithName:@HAL_REG_FONT size:20]];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(maxDate.frame) + 10, CGRectGetWidth(cell.frame), 125)];
    NSString *imageName = [NSString stringWithFormat:@"motivation%d", (int)random()%41];
    [imageView setImage:[UIImage imageNamed:imageName]];
    imageView.contentMode = UIViewContentModeScaleAspectFit;
    //imageView.backgroundColor = FlatSkyBlueDark;
    
    UILabel *maxSet =[[UILabel alloc] init];
    maxSet.text = [NSString stringWithFormat:@"%@ X %@ %@", today.maxRep, today.maxWeight, [[Utilities getUnits] lowercaseString]];
    maxSet.textColor = [UIColor whiteColor];
    maxSet.textAlignment = NSTextAlignmentCenter;
    [maxSet setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:40]];
    [maxSet sizeToFit];
    maxSet.frame = CGRectMake(0, CGRectGetMaxY(imageView.frame) + 10, cell.frame.size.width, 50);
    
    
    UILabel *exerciseName =[[UILabel alloc] init];
    exerciseName.text = today.exerciseName;
    exerciseName.textColor = [UIColor whiteColor];
    exerciseName.textAlignment = NSTextAlignmentCenter;
    [exerciseName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
    [exerciseName sizeToFit];
    exerciseName.frame = CGRectMake(0, CGRectGetMaxY(maxSet.frame), cell.frame.size.width, 40);
    
    UIButton *share = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(exerciseName.frame) + 30 , cell.frame.size.width, 30)];
    [share setTitle:@"Share" forState:UIControlStateNormal];
    share.backgroundColor = [lightColor objectAtIndex:randColor];
    share.tag = indexPath.row;
    [share addTarget:self action:@selector(shareIt:) forControlEvents:UIControlEventTouchUpInside];
    
    getMaxHeight = CGRectGetMinY(share.frame);
    cellMinY = CGRectGetMinY(cell.frame);
    [cell.contentView addSubview:header];
    [cell.contentView addSubview:exerciseName];
    [cell.contentView addSubview:imageView];
    [cell.contentView addSubview:maxSet];
    [cell.contentView addSubview:maxDate];
    [cell.contentView addSubview:share];

    horizontalPageControl.currentPage = indexPath.row;
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return CGSizeMake(CGRectGetWidth(collectionView.frame) - WIDTH_FACTOR, CGRectGetHeight(collectionView.frame) - 250);

}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
        return WIDTH_FACTOR;
}

-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
        NSInteger numberOfCells = self.view.frame.size.width / (CGRectGetWidth(self.view.frame) - WIDTH_FACTOR);
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * (CGRectGetWidth(self.view.frame) - WIDTH_FACTOR))) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
}

-(IBAction) shareIt:(id)sender {
    
    [self.view makeToast:@"PR info copied. Just click Paste."
                duration:2
                position:CSToastPositionTop
                   title:@"Info"];
    
    UIButton *buttonTag = (UIButton *)sender;
    
    ExerciseMetaInfo *sharedExercise = [todayAchievements objectAtIndex:buttonTag.tag];
    
    CGRect cropRect = CGRectMake(40, cellMinY + CGRectGetMinY(_collectionView.frame) , CGRectGetWidth(_collectionView.frame) - 80, getMaxHeight );
    
    
    UIImage *backgroundImage = [self rp_screenshotImageViewWithCroppingRect:cropRect];
    UIImage *watermarkImage = [UIImage imageNamed:@"IconAppIcon"];
    UILabel *name = [[UILabel alloc] init];
    name.text = @"GYMINUTES";
    [name setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
    name.textColor = [UIColor whiteColor];
    
    UIGraphicsBeginImageContext(backgroundImage.size);
    [backgroundImage drawInRect:CGRectMake(0, 0, backgroundImage.size.width, backgroundImage.size.height)];
    [watermarkImage drawInRect:CGRectMake(backgroundImage.size.width/3, backgroundImage.size.height - 30, 20 , 20)];
    [name drawTextInRect:CGRectMake(backgroundImage.size.width/3 + 25, backgroundImage.size.height - 30, 100 , 20)];
    
    UIImage *result = UIGraphicsGetImageFromCurrentImageContext();
    NSData *compressedImage = UIImageJPEGRepresentation(result, 1);
    UIGraphicsEndImageContext();
    
    //    UIImageWriteToSavedPhotosAlbum(viewImage.image, nil, nil, nil);
    NSString *text = [NSString stringWithFormat:@"#PersonalRecord for #%@: %@x%@ %@ #GYMINUTES", sharedExercise.exerciseName, sharedExercise.maxRep, sharedExercise.maxWeight, [[Utilities getUnits] lowercaseString]];
    NSURL *url = [NSURL URLWithString:@"http://gyminutesapp.com"];
    
    
    UIActivityViewController *controller =
    [[UIActivityViewController alloc]
     initWithActivityItems:@[text, url, compressedImage]
     applicationActivities:nil];
    
    UIPasteboard *pb = [UIPasteboard generalPasteboard];
    [pb setString:text];
    
    [self presentViewController:controller animated:YES completion:^{
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       nil];
        [Flurry logEvent:@"ShareVC" withParameters:articleParams];

//        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
//                                       @"Date", [NSDate date],
//                                       @"UserID", [PFUser currentUser].objectId,
//                                       @"RtInfo", [NSString stringWithFormat:@"%@,%@", ex.routiineName, ex.workoutName],
//                                       nil];
//        [Flurry logEvent:@"RtSummary" withParameters:articleParams];
    }];

}

/**
 Takes a screenshot of a UIView at a specific point and size, as denoted by
 the provided croppingRect parameter. Returns a UIImageView of this cropped
 region.
 
 CREDIT: This is based on @escrafford's answer at http://stackoverflow.com/a/15304222/535054
 */
- (UIImage *)rp_screenshotImageViewWithCroppingRect:(CGRect)croppingRect {
    // For dealing with Retina displays as well as non-Retina, we need to check
    // the scale factor, if it is available. Note that we use the size of teh cropping Rect
    // passed in, and not the size of the view we are taking a screenshot of.
    
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        //UIGraphicsBeginImageContextWithOptions(croppingRect.size, NO, [UIScreen mainScreen].scale);
            UIGraphicsBeginImageContextWithOptions(croppingRect.size, self.view.opaque, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(croppingRect.size);
    }


    // Create a graphics context and translate it the view we want to crop so
    // that even in grabbing (0,0), that origin point now represents the actual
    // cropping origin desired:
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -croppingRect.origin.x, -croppingRect.origin.y);
    [self.view.layer renderInContext:ctx];
    
    // Retrieve a UIImage from the current image context:
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the image in a UIImageView:
    return snapshotImage;
}
@end
