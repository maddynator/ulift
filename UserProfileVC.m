//
//  UserProfileVC.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "UserProfileVC.h"
#import "UserProfileForm.h"
#import "LandingSyncVC.h"
#import "commons.h"

@interface UserProfileVC () {
    UserProfile *profileData;
    float height, weight;
    NSInteger age;
}

@end
@implementation UserProfileVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"PROFILE";

    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];

    NSArray *userProfileData = [UserProfile MR_findAllInContext:localContext];
    
    if ([userProfileData count] > 0) {
        profileData = [userProfileData objectAtIndex:0];
    }
   // NSLog(@"elements in profile table are %lu %@", [userProfileData count], profileData.age);
    
    UserProfileForm *form = [[UserProfileForm alloc] init];
    form.email = [PFUser currentUser].email;
    form.name = [Utilities getUserFullNameFromFacebookOrTwitter];
    age = form.age = [profileData.age intValue];
    form.gender = [profileData.sex boolValue];
    weight = form.weight = [profileData.weight floatValue];
    height = form.height = [profileData.height floatValue];
    form.experience = [profileData.experienceLevel intValue];
    form.units = [profileData.units boolValue];
    
    self.formController.form = form;
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(updateProfile)];
}

-(void) viewDidAppear:(BOOL)animated {
    UserProfileForm *form = self.formController.form;
    form.age = age;
    form.weight = weight;
    form.height = height;
    self.formController.form = form;
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)updateProfile {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    UserProfileForm *form = self.formController.form;
    
    if ([form.email length] == 0 || form.email == nil) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.parentViewController title:@"Email required" subTitle:@"Email cannot be empty. This is your username for logging in." closeButtonTitle:@"Ok" duration:0.0f];
        return;
    }
    profileData.age = [NSNumber numberWithFloat:form.age];
    profileData.sex = [NSNumber numberWithBool:form.gender];
    profileData.userName = form.name;
    profileData.weight = [NSNumber numberWithFloat:form.weight];
    profileData.height =[NSNumber numberWithFloat:form.height];
    profileData.experienceLevel = [NSNumber numberWithInt:form.experience];
    profileData.units = [NSNumber numberWithBool:form.units];
    
    // if user updates their email address, then they will give same email address to login everytime. So we are updating username and email to the new email address as for us, username and email address is same thing for now.
    [[PFUser currentUser] setUsername:form.email];
    [[PFUser currentUser] setEmail:form.email];
    [[PFUser currentUser] saveInBackground];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:profileData.userName forKey:@"userFullName"];
    [defaults synchronize];

    
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        if (!error) {
            NSLog(@"set saved...");
        }
    }];
    
    PFQuery *query = [PFQuery queryWithClassName:@P_USERPROFILE_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query setLimit:1];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        PFObject *data;
        data = [task.result objectAtIndex:0];
        data[@"UserId"] = [PFUser currentUser].objectId;
        data[@"Age"] = (profileData.age == 0) ? @18: profileData.age;
        data[@"Sex"] = ([profileData.sex boolValue] == false) ? @NO: @YES;
        data[@"Name"] = profileData.userName;
        data[@"Weight"] = profileData.weight;
        data[@"Height"] = profileData.height;
        data[@"ExperienceLevel"] = profileData.experienceLevel;
        data[@"Units"] = ([profileData.units boolValue] == false) ? @NO: @YES;
        data[@"MembershipType"] = [UpdateHandler getMembershipType];
        data[@"DeviceType"] = [Utilities getIphoneName];
        [data saveEventually];
        
        return task;
    }];
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert addButton:@"Ok" actionBlock:^(void) {
//        [self.navigationController popToRootViewControllerAnimated:true];
    }];
    [alert showSuccess:self title:@"SUCCESS!" subTitle:@"Profile updated." closeButtonTitle:nil duration:0.0f];
    
    NSLog(@"You successfully updated your context.");
}

- (void)logoutForm:(UITableViewCell<FXFormFieldCell> *)cell {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert addButton:@"Yes, I am sure." actionBlock:^{
        [Utilities logout];
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        //creating the menu items
        LandingSyncVC *firstViewController = [storyboard instantiateViewControllerWithIdentifier:@"LandingSync"];
        [self presentViewController:firstViewController animated:YES completion:nil];
    }];
    
    [alert showWarning:self.parentViewController title:@"LOGOUT" subTitle:@"Are your sure you want to logout?" closeButtonTitle:@"No" duration:0.0f];
}
@end
