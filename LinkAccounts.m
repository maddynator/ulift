//
//  LinkAccounts.m
//  uLift
//
//  Created by Mayank Verma on 10/23/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "LinkAccounts.h"
#import "commons.h"
@interface LinkAccounts ()

@end

@implementation LinkAccounts

- (void)viewDidLoad {
    [super viewDidLoad];

    self.title = @"Stay Connected";
    [self createView];
}
-(void) createView {
    NSArray *imageArrayArr = [[NSArray alloc] initWithObjects:@"IconLinkFacebook", @"IconLinkTwitter", @"IconLinkFacebookLike", nil];
    NSArray *imageTextArr = [[NSArray alloc] initWithObjects:
                             @"Connect your account with facebook. You can then use facebook to login in future. Note: We will not post anything on your wall without your permission.",
                             @"Connect your account with twitter. You can then use twitter to login in future. Note: We will not tweet anything without your permission.",
                             @"Like us on facebook. You can interact with us and join our community where members discuss their progress and keep each other motivated.", nil];
    
    NSArray *imageButtonTextArr = [[NSArray alloc] initWithObjects:@"Link With Facebook", @"Link With Twitter", @"Like us on Facebook", nil];
    
    switch (_socialMedia) {
        case 0: {
            // Do any additional setup after loading the view.
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, CGRectGetWidth(self.view.frame), 100)];
            [image setContentMode:UIViewContentModeScaleAspectFit];
            
            UILabel *imageText = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(image.frame) + 30, CGRectGetWidth(self.view.frame) - 10, 90)];
            imageText.numberOfLines = 4;
            [imageText setTextAlignment:NSTextAlignmentCenter];
            imageText.tag = 100;
            [imageText setFont:[UIFont fontWithName:@HAL_REG_FONT size:18]];
            
            
            UIButton *connect = [[UIButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(imageText.frame) + 30, CGRectGetWidth(self.view.frame)- 10, 50)];
            
            connect.layer.cornerRadius = 4;
            connect.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            
            [image setImage:[UIImage imageNamed:[imageArrayArr objectAtIndex:_socialMedia]]];
            if (![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
                imageText.text = [imageTextArr objectAtIndex:_socialMedia];
                [connect setTitle:[imageButtonTextArr objectAtIndex:_socialMedia] forState:UIControlStateNormal];
                [connect addTarget:self action:@selector(linkWithFacebook) forControlEvents:UIControlEventTouchUpInside];
                connect.backgroundColor = UIColorFromRGB(0x3B5998);
                
            } else {
                NSLog(@"user is already linked...");
                imageText.text = @"Your account is linked with facebook. You can use facebook to login into gyminutes.";
                [connect setTitle:@"Disconnect Facebook" forState:UIControlStateNormal];
                [connect addTarget:self action:@selector(unLinkWithFacebook) forControlEvents:UIControlEventTouchUpInside];
                connect.backgroundColor = FlatRed;

            }
            [self.view addSubview:connect];
            [self.view addSubview:image];
            [self.view addSubview:imageText];
            
        }
            break;
        case 1: {
            // Do any additional setup after loading the view.
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, CGRectGetWidth(self.view.frame), 100)];
            [image setContentMode:UIViewContentModeScaleAspectFit];
            
            UILabel *imageText = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(image.frame) + 30, CGRectGetWidth(self.view.frame) - 10, 90)];
            imageText.numberOfLines = 4;
            imageText.tag = 100;
            [imageText setTextAlignment:NSTextAlignmentCenter];
            [imageText setFont:[UIFont fontWithName:@HAL_REG_FONT size:18]];
            
            
            UIButton *connect = [[UIButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(imageText.frame) + 30, CGRectGetWidth(self.view.frame)- 10, 50)];
            
            connect.layer.cornerRadius = 4;
            connect.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            
            [image setImage:[UIImage imageNamed:[imageArrayArr objectAtIndex:_socialMedia]]];
            if (![PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]) {
                imageText.text = [imageTextArr objectAtIndex:_socialMedia];
                [connect setTitle:[imageButtonTextArr objectAtIndex:_socialMedia] forState:UIControlStateNormal];
                [connect addTarget:self action:@selector(linkWithTwitter) forControlEvents:UIControlEventTouchUpInside];
                connect.backgroundColor = UIColorFromRGB(0x60D2F6);
            } else {
                imageText.text = @"Your account is linked with Twitter. You can use twitter to login into gyminutes.";
                [connect setTitle:@"Disconnect Twitter" forState:UIControlStateNormal];
                [connect addTarget:self action:@selector(unLinkWithTwitter) forControlEvents:UIControlEventTouchUpInside];
                connect.backgroundColor = FlatRed;
            }
            [self.view addSubview:image];
            [self.view addSubview:imageText];
            [self.view addSubview:connect];
        }
            break;
        case 2: {
            // Do any additional setup after loading the view.
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 50, CGRectGetWidth(self.view.frame), 100)];
            [image setContentMode:UIViewContentModeScaleAspectFit];
            [image setImage:[UIImage imageNamed:[imageArrayArr objectAtIndex:_socialMedia]]];

            UILabel *imageText = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(image.frame) + 30, CGRectGetWidth(self.view.frame) - 10, 90)];
            imageText.numberOfLines = 4;
            [imageText setTextAlignment:NSTextAlignmentCenter];
            [imageText setFont:[UIFont fontWithName:@HAL_REG_FONT size:18]];
            imageText.tag = 100;
            imageText.text = [imageTextArr objectAtIndex:_socialMedia];
            
            FBSDKLikeControl *likeButton = [[FBSDKLikeControl alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(imageText.frame) + 30, CGRectGetWidth(self.view.frame)- 10, 50)];
            likeButton.objectID = @"https://www.facebook.com/Gyminutes-881595001917352";
//            likeButton.center = self.view.center;
            likeButton.likeControlStyle = FBSDKLikeControlStyleBoxCount;
            likeButton.likeControlHorizontalAlignment = FBSDKLikeControlHorizontalAlignmentCenter;
            [self.view addSubview:image];
            [self.view addSubview:imageText];
            [self.view addSubview:likeButton];
        }
            break;
        case 3:
            [self twitterButton];
            break;
        default:
            break;
    }
    
    
}

-(void) reloadView {
    NSLog(@"inside subview removal..");
    for (UIView *lbl in [self.view subviews]) {
        NSLog(@"removing subviews.");
        [lbl removeFromSuperview];
    }
    [self createView];
}
-(void) linkWithFacebook {
    PFUser *user = [PFUser currentUser];
    if (![PFFacebookUtils isLinkedWithUser:user]) {
        NSLog(@"old user not linked..");
        
        [PFFacebookUtils linkUserInBackground:user withPublishPermissions:@[@"publish_actions"] block:^(BOOL succeeded, NSError * _Nullable error) {
            NSLog(@"Woohoo, user is linked with Facebook!");
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"facebookLinked" withParameters:articleParams];
            [self reloadView];
        }];
    }

}
-(void) unLinkWithFacebook {
    
    PFUser *user = [PFUser currentUser];
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"Yes, Disconnect." actionBlock:^{
        if ([PFFacebookUtils isLinkedWithUser:user]) {
            [PFFacebookUtils unlinkUserInBackground:user block:^(BOOL succeeded, NSError * _Nullable error) {
                NSLog(@"oho, user unlinked facebook!");
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"facebookUnlinked" withParameters:articleParams];
                [self reloadView];
            }];
        }
    }];
    [alert showWarning:self title:@"Disconnect Facebook" subTitle:@"Are you sure you want to disconnect facebook from gyminutes account?" closeButtonTitle:@"No" duration:0.0f];
}

-(void) linkWithTwitter {
    PFUser *user = [PFUser currentUser];
    
    if (![PFTwitterUtils isLinkedWithUser:user]) {
        [PFTwitterUtils linkUser:user block:^(BOOL succeeded, NSError *error) {
            if ([PFTwitterUtils isLinkedWithUser:user]) {
                NSLog(@"Woohoo, user logged in with Twitter!");
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"twitterLinked" withParameters:articleParams];

                [self reloadView];
            }
        }];
    }
    
}

-(void) unLinkWithTwitter {
    
    PFUser *user = [PFUser currentUser];
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"Yes, Disconnect." actionBlock:^{
        if ([PFTwitterUtils isLinkedWithUser:user]) {
            [PFTwitterUtils unlinkUserInBackground:user block:^(BOOL succeeded, NSError * _Nullable error) {
                NSLog(@"oho, user unlinked twiter!");
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"twitterUnlinked" withParameters:articleParams];
                [self reloadView];
            }];
        }
    }];
    [alert showWarning:self title:@"Disconnect Twitter" subTitle:@"Are you sure you want to disconnect twitter from gyminutes account?" closeButtonTitle:@"No" duration:0.0f];
}


-(void)twitterButton
{
    NSString *twitterAccount= @"gyminutesapp";
    NSArray *urls = [NSArray arrayWithObjects:
                     @"http://twitter.com/{handle}",
                     nil];
    
    UIApplication *application = [UIApplication sharedApplication];
    
    for (NSString *candidate in urls) {
        NSURL *url = [NSURL URLWithString:[candidate stringByReplacingOccurrencesOfString:@"{handle}" withString:twitterAccount]];
        if ([application canOpenURL:url])
        {
            UIWebView*   Twitterweb =[[UIWebView alloc] initWithFrame:self.view.bounds];
            Twitterweb.delegate=nil;
            Twitterweb.hidden=NO;
            NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
            [Twitterweb loadRequest:requestObj];
            [self.view addSubview:Twitterweb];
            return;
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
