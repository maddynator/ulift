//
//  AchievementChecker.h
//  gyminutes
//
//  Created by Mayank Verma on 6/21/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AchievementChecker : NSObject
+ (AchievementChecker *)sharedInstance;

@end
