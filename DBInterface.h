//
//  DBInterface.h
//  gyminutes
//
//  Created by Mayank Verma on 5/10/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DBInterface : NSObject

+(NSMutableDictionary *) getDailyStatsCard:(NSString *)date;

// OVERALL ANALYTICS
+(NSMutableDictionary *) getMostWorkedOutMuscle: (NSArray *) userSets;
+(NSMutableDictionary *) getMostWorkedOutMajorMuscle: (NSArray *) userSets;
+(NSMutableDictionary *) getMostWorkedOutExercise: (NSArray *) userSets;
+(NSMutableDictionary *) getMostWorkedOutWorkout: (NSArray *) userSets;
+(NSMutableDictionary *) getAverageRepRange: (NSArray *) userSets;
+(NSMutableDictionary *) totalWorkoutTime: (NSArray *) userSets;

// ROUTINE ANALYTICS
+(NSMutableDictionary *) getRoutineDate:(NSArray *)data;
+(NSMutableDictionary *) getRoutineFavorites:(NSArray *) data;
+(NSMutableDictionary *) getRoutineWeekday:(NSArray *)data;
+(NSMutableDictionary *) getRoutineTime:(NSArray *)data;
+(NSMutableDictionary *) getRoutineOverallStats:(NSArray *) data;
+(NSMutableDictionary *) getRoutineOverallTotalStats:(NSArray *) data;
+(NSMutableDictionary *) getRoutineWorkouts:(NSArray *) data;
+(NSMutableDictionary *) getRoutinePerWorkoutsEst:(NSArray *) data;
// WORKOUT ANALYTICS
+(NSMutableDictionary *) getWorkoutFavorites:(NSArray *) data;
+(NSMutableDictionary *) getWorkoutPerWeek: (NSArray *) userSets;
+(NSMutableDictionary *) getWorkoutPerMonth: (NSArray *) userSets;
+(NSMutableDictionary *) getWorkoutDates: (NSArray *) userSets;
+(NSMutableDictionary *) getShortestWorkout:(NSArray *) data;
+(NSMutableDictionary *) getLongestWorkout:(NSArray *)data;
+(NSMutableDictionary *) getWorkoutStats:(NSArray *)data;
+(NSMutableDictionary *) getRoutineAvgWorkoutEst:(NSArray *) data;
+(NSMutableDictionary *) getRoutineAvgWorkoutAct:(NSArray *) data;
+(NSMutableDictionary *) getPerWorkoutStats:(NSArray *) data;


// MUSCLE ANALYTICS
+(NSMutableDictionary *) getMuscleFavoriteForRoutine:(NSArray *) data;
+(NSMutableDictionary *) getMuscleStatsEst:(NSArray *) data;
+(NSMutableDictionary *) getMuscleStatsAct:(NSArray *) data;
+(NSMutableDictionary *) getMajorMuscleDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getMuscleDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getFrontToBackDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getUpperToBottomDistributionOfRoutineEst:(NSArray *) data;

+(NSMutableDictionary *) getMajorMuscleDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getMuscleDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getFrontToBackDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getUpperToBottomDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getPerMuscleStats:(NSArray *) data;


// EXERCISE ANALYTICS
+(NSMutableDictionary *) getExerciseFavoriteForRoutine:(NSArray *) data;
+(NSMutableDictionary *) getExerciseStatsEst:(NSArray *) data;
+(NSMutableDictionary *) getExerciseStatsAct:(NSArray *) data;
+(NSMutableDictionary *) getExerciseEquipmentDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getExerciseEquipmentDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getExerciseMechanicsDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getExerciseMechanicsDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getExerciseTypeDistributionOfRoutineEst:(NSArray *) data;
+(NSMutableDictionary *) getExerciseTypeDistributionOfRoutineAct:(NSArray *) data;
+(NSMutableDictionary *) getPerExerciseStats:(NSArray *) data;

//+(NSMutableDictionary *) getRoutineAvgRepRangeAct:(NSArray *)data;
//+(NSMutableDictionary *) getRoutineAvgRestTimeAct:(NSArray *)data;


+(NSMutableDictionary*) getWorkoutStats:(NSArray *) data workoutName:(NSString *) workoutName;

@end
