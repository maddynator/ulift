//
//  Utilities.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "Utilities.h"
#import "commons.h"

@implementation Utilities

// =========================  GENERAL UTILITY FUNCTIONS ===================
+(BOOL) askForRating {
    int days = [self daysSinceAccount];
    if (days < FREE_VERSION_TRIAL_PERIOD) {
        return false;
    } else {
        NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:@"ReviewLastAsked"];
        if (date == nil) {
            [[NSUserDefaults standardUserDefaults] setObject:[NSDate date] forKey:@"ReviewLastAsked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            return true;
        } else {
            NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                                fromDate:date
                                                                  toDate:[NSDate date]
                                                                 options:NSCalendarWrapComponents];
            NSLog(@"days since no review %ld", (long)[components day]);
            if ([components day] >= 1) {
                return true;
            }
            
            return false;
        }
    }
    return true;
}
+(void) setTodayWidgetData {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];    
    
    NSUserDefaults *sharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:WIDGET_GROUP];
    if ([[sharedDefaults objectForKey:@"LastWorkoutDate"] isEqualToString:[formatter stringFromDate:today]]) {
        return;
    } else {
        NSString *workouts = [self getNumberOfWorkoutThisWeek];
        [sharedDefaults setObject:workouts forKey:@"NumWorkoutsThisWeek"];
        [sharedDefaults setObject:[formatter stringFromDate:today] forKey:@"LastWorkoutDate"];
        [sharedDefaults synchronize];
    }
}
+(NSString *) getNumberOfWorkoutThisWeek {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierISO8601];

    NSNumber *thisWeek = [NSNumber numberWithLong:[[calendar components: NSCalendarUnitWeekOfYear fromDate:[NSDate date]] weekOfYear]];

    NSMutableArray *subArray = [[NSMutableArray alloc] init];
    for (ExerciseSet *set in userSets) {
        NSDate *date = [dateFormatter dateFromString:set.date];
        NSInteger workoutYear = [date year];
        if (workoutYear != currentYear)
            continue;
        
        NSNumber *weekNumber = [NSNumber numberWithLong:[[calendar components: NSCalendarUnitWeekOfYear fromDate:date] weekOfYear]];
        if ([weekNumber isEqualToNumber: thisWeek]		) {
            [subArray addObject:set];
        }
    }
    
    NSMutableDictionary *workoutPerWeekDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in subArray) {
        NSString *identityString = [NSString stringWithFormat:@"%@ %@", set.workoutName, set.date];
        NSNumber *weekCount = [workoutPerWeekDict objectForKey:identityString];
        if (weekCount == nil) {
            // insert object with value 1
            weekCount = [NSNumber numberWithInt:1];
            [workoutPerWeekDict setObject:weekCount forKey:identityString];
        }
    }
    
    int count = (int)[[workoutPerWeekDict allKeys] count];
    NSLog(@"Workouts this week %d", count);
    return [NSString stringWithFormat:@"%d workouts", count];
}

+(UIColor *) getAppColor {
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppColor"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
}
+(UIColor *) getAppColorLight {
    NSData *colorData = [[NSUserDefaults standardUserDefaults] objectForKey:@"AppColorLight"];
    return [NSKeyedUnarchiver unarchiveObjectWithData:colorData];
}
/*
 * DESC: Get current date
 * Returns: Date in String format
 */
+(NSString *) getCurrentDate {
    NSDate *today = [NSDate date];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *dateString = [dateFormat stringFromDate:today];
    return dateString;
}

/*
 * DESC: Get Yesterday date based on date passed
 * Returns: Date in String format
 */

+(NSString *) getYesterdayDate: (NSString * ) todayDate {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    today = [dateFormatter dateFromString:todayDate];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = -1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:today
                                                  options:0];
    
    
    NSString *date_String=[dateFormatter stringFromDate:yesterday];
    return date_String;
    
}

/*
 * DESC: Get Tommorow date based on date passed
 * Returns: Date in String format
 */

+(NSString *) getTomorrowDate: (NSString * ) todayDate {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    today = [dateFormatter dateFromString:todayDate];
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = +1;
    NSDate *yesterday = [gregorian dateByAddingComponents:dayComponent
                                                   toDate:today
                                                  options:0];
    
    
    NSString *date_String=[dateFormatter stringFromDate:yesterday];
    return date_String;
    
}


+(NSArray *) getAllExerciseForMuscle:(NSString *) muscle {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@", muscle];
    
    NSArray *exerciseArray = [ExerciseList MR_findAllSortedBy:@"equipment" ascending:YES withPredicate:predicate inContext:localContext];
    return exerciseArray;
    
}

+(NSArray *) getAllExerciseForMajorMuscle:(NSString *) muscle {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", muscle];
    
    NSArray *exerciseArray = [ExerciseList MR_findAllSortedBy:@"equipment,exerciseName" ascending:YES withPredicate:predicate inContext:localContext];
    return exerciseArray;
    
}

+(UIColor *) getExerciseColor: (NSString *) muscle {
    if ([muscle isEqualToString:@"Abs"])
        return FlatRedDark;
    if ([muscle isEqualToString:@"Back"])
        return FlatOrangeDark;
    if ([muscle isEqualToString:@"Biceps"])
        return FlatMagentaDark;
    if ([muscle isEqualToString:@"Calves"])
        return FlatGreenDark;
    if ([muscle isEqualToString:@"Chest"])
        return FlatBlueDark;
    if ([muscle isEqualToString:@"Forearm"])
        return FlatMagentaDark;
    if ([muscle isEqualToString:@"Glutes"])
        return FlatGreenDark;
    if ([muscle isEqualToString:@"Hamstring"])
        return FlatGreenDark;
    if ([muscle isEqualToString:@"Lats"])
        return FlatOrangeDark;
    if ([muscle isEqualToString:@"Quads"])
        return FlatGreenDark;
    if ([muscle isEqualToString:@"Shoulders"])
        return FlatWatermelonDark;
    if ([muscle isEqualToString:@"Traps"])
        return FlatWatermelonDark;
    if ([muscle isEqualToString:@"Triceps"])
        return FlatMagentaDark;
    
    return FlatCoffee;
}

+(UIColor *) getMajorMuscleColor: (NSString *) muscle {
    if ([muscle isEqualToString:@"Abs"])
        return FlatRedDark;
    if ([muscle isEqualToString:@"Arms"])
        return FlatMagentaDark;
    if ([muscle isEqualToString:@"Back"])
        return FlatOrangeDark;
    if ([muscle isEqualToString:@"Chest"])
        return FlatBlueDark;
    if ([muscle isEqualToString:@"Legs"])
        return FlatGreenDark;
    if ([muscle isEqualToString:@"Shoulders"])
        return FlatWatermelonDark;
    return FlatCoffee;
}

+(UIColor *) getMajorMuscleColorSelected:(NSString *)muscle {
    if ([muscle isEqualToString:@"Abs"])
        return FlatRed;
    if ([muscle isEqualToString:@"Arms"])
        return FlatMagenta;
    if ([muscle isEqualToString:@"Back"])
        return FlatOrange;
    if ([muscle isEqualToString:@"Chest"])
        return FlatBlue;
    if ([muscle isEqualToString:@"Legs"])
        return FlatGreen;
    if ([muscle isEqualToString:@"Shoulders"])
        return FlatWatermelon;
    return FlatCoffee;
}
+(NSArray *) getAllExerciseForMuscleAndEquipment: (NSString *) muscle equip:(NSString*) equipment {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND equipment == %@", muscle, equipment];
    NSArray *exerciseArray = [ExerciseList MR_findAllSortedBy:@"exerciseName" ascending:YES withPredicate:predicate inContext:localContext];
    // NSLog(@"Number of exercise for %@ with equipment %@ are %lu", muscle, equipment, (long) [exerciseArray count]);
    return exerciseArray;
    
}


+(BOOL) showWorkoutFinishButton: (NSString *) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND syncedState == %@", date, [NSNumber numberWithBool:SYNC_NOT_DONE]];
    
    NSArray *unsyncedSets = [ExerciseSet MR_findAllWithPredicate:predicate inContext:localContext];
    if ([unsyncedSets count] > 0)
        return true;
    
    return false;
}

+(UIImage *) getDetailedImage {
    return SCLAlertViewStyleKit.imageOfNotice;
}
+(UIImage *) getCompleteMark {
    return SCLAlertViewStyleKit.imageOfCheckmark;
}

+(UIImage *) getIncompleteMark {
    return SCLAlertViewStyleKit.imageOfInfo;
    
}
+(UIImage *) getStartMark {
    return SCLAlertViewStyleKit.imageOfEdit;
}

+(int) getDefaultSetsValue {
    NSString *key = [NSString stringWithFormat:@"defaultSetsKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"timer val %d", value);
    if (value == 0) {
        return 3;
    } else {
        return value;
    }
}

+(int) getDefaultRepsValue {
    NSString *key = [NSString stringWithFormat:@"defaultRepsKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"timer val %d", value);
    if (value == 0) {
        return 8;
    } else {
        return value;
    }
}

+(int) getDefaultRestTimerValue {
    NSString *key = [NSString stringWithFormat:@"defaultRestTimerKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"timer val %d", value);
    if (value == 0) {
        return 45;
    } else {
        return value;
    }
}

+(int) getAdditionalRestTimerValue {
    NSString *key = [NSString stringWithFormat:@"defaultAdditionalRestTimerKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"timer val %d", value);
    if (value == 0) {
        return 45;
    } else {
        return value;
    }
}


+(int) getDetailedSummaryValue {
    NSString *key = [NSString stringWithFormat:@"detailedSummaryKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"detailed summary val %d", value);
    if (value == 0) {
        return 0;
    } else {
        return 1;
    }
}

+(int) getDefaultRestTimerValueBetweenExercises {
    NSString *key = [NSString stringWithFormat:@"defaultExerciseRestTimerKey"];
    int value = [[[NSUserDefaults standardUserDefaults] objectForKey:key] intValue];
    NSLog(@"timer val %d", value);
    if (value == 0) {
        return 90;
    } else {
        return value;
    }
}

+(NSString *) getUserFullNameFromFacebookOrTwitter {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fullName = [defaults objectForKey:@"userFullName"];
    
    if (fullName == nil) {
        return @"";
    }
    return fullName;
}

+(NSString *)  getUnits {
    UserProfile *data = [UserProfile MR_findFirst];
    if ([data.units boolValue] == 0)
        return @"Lbs";
    else
        return @"Kgs";
}

+(BOOL) isKgs {
    UserProfile *data = [UserProfile MR_findFirst];
    if ([data.units boolValue] == 0)
        return false;
    else
        return true;
}


+(bool) isIncBy25Enabled {
    bool value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultWtIncKey"] boolValue];

    NSLog(@"increment value is %d", (int) value);
    return value;
}
+(BOOL) isCustomKeyboardEnabled {
    BOOL value = [[[NSUserDefaults standardUserDefaults] objectForKey:@"defaultCustomKeyBoard"] boolValue];
    return value;
}

+(UIColor *) getLighterColorFor:(UIColor *) color {
    /*
     Red = 0,
     Blue,
//     Orange,
     Pink,
     Magenta,
     Brown,
     Coffee,
     Maroon,
     Mint
     */
    if ([color isEqual:FlatRedDark])
        return FlatRed;
    if ([color isEqual:FlatBlueDark])
        return FlatBlue;
//    if ([color isEqual:FlatOrangeDark])
//        return FlatOrange;
    if ([color isEqual:FlatPinkDark])
        return FlatPink;
    if ([color isEqual:FlatMagentaDark])
        return FlatMagenta;
    if ([color isEqual:FlatBrownDark])
        return FlatBrown;
    if ([color isEqual:FlatMaroonDark])
        return FlatMaroon;
    if ([color isEqual:FlatMintDark])
        return FlatMint;
    
    return FlatRed;
}
+(UIColor *) getDarkerColorFor:(UIColor *) color {
    if ([color isEqual:FlatRed])
        return FlatRedDark;
    if ([color isEqual:FlatBlue])
        return FlatBlueDark;
//    if ([color isEqual:FlatOrange])
//        return FlatOrangeDark;
    if ([color isEqual:FlatPink])
        return FlatPinkDark;
    if ([color isEqual:FlatMagenta])
        return FlatMagentaDark;
    if ([color isEqual:FlatBrown])
        return FlatBrownDark;
    if ([color isEqual:FlatMaroon])
        return FlatMaroonDark;
    if ([color isEqual:FlatMint])
        return FlatMintDark;
    
    return FlatRedDark;
}


+(UIColor *) colorForString:(NSString *) colorStr {
    if ([colorStr isEqualToString:@"Red"])
        return FlatRed;
    if ([colorStr isEqualToString:@"Blue"])
        return FlatBlue;
    if ([colorStr isEqualToString:@"Orange"])
        return FlatOrange;
    if ([colorStr isEqualToString:@"Pink"])
        return FlatPink;
    if ([colorStr isEqualToString:@"Magenta"])
        return FlatMagenta;
    if ([colorStr isEqualToString:@"Brown"])
        return FlatBrown;
    if ([colorStr isEqualToString:@"Maroon"])
        return FlatMaroon;
    if ([colorStr isEqualToString:@"Coffee"])
        return FlatMaroon;
    if ([colorStr isEqualToString:@"Mint"])
        return FlatMint;
    
    return FlatRed;
}



+(NSString *) exportAllDataForRoutine:(NSString *)routineName {
    
    NSArray *allSets;
    
    if ([routineName isEqualToString:@"All"]) {
        allSets = [ExerciseSet MR_findAllSortedBy:@"date,routineName,workoutName,exerciseName,exerciseNumber" ascending:YES];
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
        allSets = [ExerciseSet MR_findAllSortedBy:@"date,routineName,workoutName,exerciseName,exerciseNumber" ascending:YES withPredicate:predicate];
    }
    
    NSMutableString *csv = [NSMutableString stringWithString:@"Date,RoutineName,WorkoutName,ExerciseName,Muscle,SubMuscle,Reps,Weight"];
    [csv appendFormat:@"\n"];
    // provided all arrays are of the same length
    for (ExerciseSet *set in allSets) {
        [csv appendFormat:@"%@,%@,%@,%@,%@,%@,%d,%.1f\n",
         set.date,set.routineName, set.workoutName, set.exerciseName, set.majorMuscle, set.muscle,  [set.rep intValue], [set.weight floatValue]
         ];
    }
    
    NSString *tempDir = NSTemporaryDirectory();
    NSLog(@"%@", tempDir);
    NSString *yourFileName = [NSString stringWithFormat:@"%@/gyminutes-%@.csv", tempDir, [self getCurrentDate]];
    NSError *error;
    BOOL res = [csv writeToFile:yourFileName atomically:YES encoding:NSUTF8StringEncoding error:&error];
    
    if (!res) {
        NSLog(@"Error %@ while writing to file %@", [error localizedDescription], yourFileName );
    }
    
    return yourFileName;
}

+(NSString *) getIphoneName {
    struct utsname systemInfo;
    uname(&systemInfo);
    
    NSString *iOSDeviceModelsPath = [[NSBundle mainBundle] pathForResource:@"iOSDeviceModelMapping" ofType:@"plist"];
    NSDictionary *iOSDevices = [NSDictionary dictionaryWithContentsOfFile:iOSDeviceModelsPath];
    
    NSString* deviceModel = [NSString stringWithCString:systemInfo.machine
                                               encoding:NSUTF8StringEncoding];
    
    return [iOSDevices valueForKey:deviceModel];
}
+ (BOOL)connected {
    return [AFNetworkReachabilityManager sharedManager].reachable;
}

+(NSArray *) getAllExercises {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    return [ExerciseList MR_findAllInContext:localContext];
}

+(UIImage *) getMuscleImage:(NSString *)muscle {
    if ([muscle isEqualToString:@"Abs"]) {
        return [UIImage imageNamed:@"IconAbs"];
    }
    
    if ([muscle isEqualToString:@"Arms"]) {
        return [UIImage imageNamed:@"IconArms"];
    }
    
    if ([muscle isEqualToString:@"Back"]) {
        return [UIImage imageNamed:@"IconBack"];
    }
    
    if ([muscle isEqualToString:@"Chest"]) {
        return [UIImage imageNamed:@"IconChest"];
    }
    
    if ([muscle isEqualToString:@"Legs"]) {
        return [UIImage imageNamed:@"IconLegs"];
    }
    
    if ([muscle isEqualToString:@"Shoulders"]) {
        return [UIImage imageNamed:@"IconShoulders"];
    }
    return nil;
    
}
+(BOOL) isHintEnabled {
    //EnableHintKey - basically checks is hint is enabled in settings.
    NSLog(@"HINT IS %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"EnableHintKey"]);
    bool  retVal = [[NSUserDefaults standardUserDefaults] boolForKey:@"EnableHintKey"];
    if (retVal == true) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"EnableHintKey"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
    }
    return retVal;
}

+(BOOL) isEarlyBird {
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
    [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
    [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
    
    NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSDate *accountDate = [PFUser currentUser].createdAt;
    if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
        return true;
    }

    return false;
}

// ================================= MUSCLE ==========================================
/*
 * DESC: Iterate over all exercises in the exerciselist and find unique muscle (not major muscle) and update MuscleList DB Locally
 * Returns: nothing
 */

+(void) syncMuscleGroups {
    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        NSArray *exerciseArray = [ExerciseList MR_findAllInContext:localContext];
        NSLog(@"exercise counts is %lu", (long) [exerciseArray count]);
        for (ExerciseList *data in exerciseArray) {
            // Still keeping this to muscle but adding major muscle as we introduced parent muscle grp.
            NSString *string = data.muscle;
            
            if ([processed containsObject:string] == NO) {
                [unique addObject:string];
                [processed addObject:string];
                MuscleList *muscleData = [MuscleList MR_createEntityInContext:localContext];
                muscleData.muscleName = string;
                muscleData.majorMuscle = data.majorMuscle;
                NSLog(@"major muscle is %@, %@", data.majorMuscle, string);
            }
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        NSLog(@"muscle synced...");
    }];
}

+(NSMutableArray *) getMuscleList {
    //muscleName
    NSArray *temp = [MuscleList MR_findAllSortedBy:@"majorMuscle" ascending:YES];
    
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (MuscleList *data in temp) {
        NSString *string = data.majorMuscle;
        //        NSLog(@"major muscle is %@, %@", data.majorMuscle, data.muscleName);
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    return unique;
}

+(void) crashAnalyticsUserData {
    PFUser *currentUser = [PFUser currentUser];
    if (currentUser != nil) {
        [CrashlyticsKit setUserIdentifier:currentUser.objectId];
        [CrashlyticsKit setUserEmail:currentUser.email];
        [CrashlyticsKit setUserName:currentUser.username];
    } else {
        [CrashlyticsKit setUserIdentifier:@"None"];
        [CrashlyticsKit setUserEmail:@"none@gyminutes.com"];
        [CrashlyticsKit setUserName:@"none"];
    }
}

+(int) daysSinceAccount {
    NSTimeInterval interval = [PFUser currentUser].createdAt.timeIntervalSinceNow * -1;
    
    //NSLog(@"user created date is %f", interval);
    int days = 0;
    
    if (interval > 0) {
        div_t d = div(interval, 86400);
        int day = d.quot;
        days = day;
//        div_t h = div(d.rem, 3600);
//        int hour = h.quot;
//        div_t m = div(h.rem, 60);
//        int min = m.quot;
       // NSLog(@"days before.. %d",days);
        
    }
    return days;
}

+(BOOL) showWorkoutPackage {

    if ([self showPremiumPackage] == false) {
        return false;
    }

    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedWorkoutPackage"];
    NSLog(@"Woprkout Package is %d, %d", isPurchased, [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedWorkoutPackage"]);
    if (isPurchased == false) {
        // this means either the user is a old user who paid for app or this is new user who has 15 days free access to app....
        // first we check if user is withing 15 days trail period.
        int days = [self daysSinceAccount];
        if (days < FREE_VERSION_TRIAL_PERIOD) {
            NSLog(@"days since account %d", [self daysSinceAccount]);
            return false;
        } else if (days >= FREE_VERSION_TRIAL_PERIOD) { // this means trial is expired. Now we check
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
            [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
            [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
            NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
            NSDate *accountDate = [PFUser currentUser].createdAt;
            if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
                NSLog(@"WORKOUT PACK PAID FOR APP => account date %@ is earlier than release date %@", accountDate, releaseDate);
                return false;
            } else {
                // As app is not purchased and trial period has expired, and account creation date is after release date of free version, we are setting the PremiumPurchased to false
//                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedWorkoutPackage"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                return true;
            }
        }
    }
    
    return false;
}

+(BOOL) showDataPackage {
    
    if ([self showPremiumPackage] == false) {
        return false;
    }

    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedDataPackage"];
    NSLog(@"Data package is %d, %d", isPurchased, [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedDataPackage"]);
    if (isPurchased == false) {
        // this means either the user is a old user who paid for app or this is new user who has 15 days free access to app....
        // first we check if user is withing 15 days trail period.
        int days = [self daysSinceAccount];
        if (days < FREE_VERSION_TRIAL_PERIOD) {
            NSLog(@"days since account %d", [self daysSinceAccount]);
            return false;
        } else if (days >= FREE_VERSION_TRIAL_PERIOD) { // this means trial is expired. Now we check
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
            [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
            [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
            NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
            NSDate *accountDate = [PFUser currentUser].createdAt;
            if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
                NSLog(@"PAID FOR APP => account date %@ is earlier than release date %@", accountDate, releaseDate);
                return false;
            } else {
                // As app is not purchased and trial period has expired, and account creation date is after release date of free version, we are setting the PremiumPurchased to false
//                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedDataPackage"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                return true;
            }
        }
    }
    
    return false;
}

+(BOOL) showPowerRoutinePackage {    
    if ([self showPremiumPackage] == false) {
        return false;
    }

    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPowerRoutinePackage"];
    NSLog(@"PurchasedPowerRoutinePackage is %d, %d", isPurchased, [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPowerRoutinePackage"]);
    if (isPurchased == false) {
        // this means either the user is a old user who paid for app or this is new user who has 15 days free access to app....
        // first we check if user is withing 15 days trail period.
        int days = [self daysSinceAccount];
        if (days < FREE_VERSION_TRIAL_PERIOD) {
            NSLog(@"days since account %d", [self daysSinceAccount]);
            return false;
        } else if (days >= FREE_VERSION_TRIAL_PERIOD) { // this means trial is expired. Now we check
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
            [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
            [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
            NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
            NSDate *accountDate = [PFUser currentUser].createdAt;
            if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
                NSLog(@"PAID FOR APP => account date %@ is earlier than release date %@", accountDate, releaseDate);
                return false;
            } else {
                // As app is not purchased and trial period has expired, and account creation date is after release date of free version, we are setting the PremiumPurchased to false
//                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedPowerRoutinePackage"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                return true;
            }
        }
    }
    
    return false;
}

+(BOOL) showAnalyticsPackage {
    
    if ([self showPremiumPackage] == false) {
        return false;
    }
    
    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedAnalyticsPackage"];
    NSLog(@"Analytics Package is %d, %d", isPurchased, [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedAnalyticsPackage"]);
    if (isPurchased == false) {
        // this means either the user is a old user who paid for app or this is new user who has 15 days free access to app....
        // first we check if user is withing 15 days trail period.
        int days = [self daysSinceAccount];
        if (days < FREE_VERSION_TRIAL_PERIOD) {
            NSLog(@"days since account %d", [self daysSinceAccount]);
            return false;
        } else if (days >= FREE_VERSION_TRIAL_PERIOD) { // this means trial is expired. Now we check
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
            [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
            [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
            NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
            NSDate *accountDate = [PFUser currentUser].createdAt;
            if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
                NSLog(@"WORKOUT PACK PAID FOR APP => account date %@ is earlier than release date %@", accountDate, releaseDate);
                return false;
            } else {
                // As app is not purchased and trial period has expired, and account creation date is after release date of free version, we are setting the PremiumPurchased to false
                //                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedWorkoutPackage"];
                //                [[NSUserDefaults standardUserDefaults] synchronize];
                return true;
            }
        }
    }
    
    return false;
}

+(bool) showPremiumPackage {

    BOOL isPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPremium"];
    NSLog(@"isPurchase is %d, %d", isPurchased, [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPremium"]);
    if (isPurchased == false) {
        // this means either the user is a old user who paid for app or this is new user who has 15 days free access to app....
        // first we check if user is withing 15 days trail period.
        int days = [self daysSinceAccount];
        if (days < FREE_VERSION_TRIAL_PERIOD) {
            NSLog(@"days since account %d", [self daysSinceAccount]);
            return false;
        } else if (days >= FREE_VERSION_TRIAL_PERIOD) { // this means trial is expired. Now we check
            NSDateComponents *comps = [[NSDateComponents alloc] init];
            [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
            [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
            [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
            NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
            NSDate *accountDate = [PFUser currentUser].createdAt;
            if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
                //NSLog(@"Premium PAID FOR APP => account date %@ is earlier than release date %@", accountDate, releaseDate);
                return false;
            } else {
                // As app is not purchased and trial period has expired, and account creation date is after release date of free version, we are setting the PremiumPurchased to false
//                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedPremium"];
//                [[NSUserDefaults standardUserDefaults] synchronize];
                return true;
            }
        }
    }
    
    return false;
}
// ================================= EXERCISES ==========================================
/*
 * DESC: Download upto 100 exercises from parse...
 * Returns: Nothing
 */

+(void) syncAllExercises:(id)sender {

    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISELIST_CLASS];
    [query setLimit: 1000];
    NSArray *data = [query findObjects];
    [self saveToLocalDB:data];
}

/*
 * DESC: Get all user created exercises from parse based on userId
 * Returns: Nothing
 */

+(void) syncAllUserExercises:(id)sender  {
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_USEREXERCISE_CLASS];
    [queryUE whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    
    [queryUE setLimit: 1000];
    
    [[queryUE findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        [self saveToLocalDB:task.result];
        return task;
    }];
}

/*
 * DESC: Sync All exercises downloaded from parse (original + user created) and store them in local BD (ExerciseList). Finally updates MuscleListDb as well.
 * Returns: Nothing
 */

+(void) saveToLocalDB:(NSArray *) exerciseData {
    int limit = (int)[exerciseData count] ;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *exercise = [exerciseData objectAtIndex:i];
            
            ExerciseList *data = [ExerciseList MR_createEntityInContext:localContext];
            data.exerciseName = exercise[@"ExerciseName"];
            data.expLevel = exercise[@"ExpLevel"];
            data.mechanics = exercise[@"Mechanics"];
            data.muscle = exercise[@"Muscle"];
            data.type = exercise[@"Type"];
            data.equipment = exercise[@"Equipment"];
            data.majorMuscle = exercise[@"MajorMuscle"];
            data.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            
            //NSLog(@"major muscle is %@", data.majorMuscle);
            data.exercisePerformed = [NSNumber numberWithBool:false];
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        // we do not want update muscle again when user exercises are synced...
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ExerciseListDBSync"];
        if ([MuscleList MR_countOfEntities] == 0) {
            NSLog(@"UPDATING MUSCLE DB muscle ...");
            [self syncMuscleGroups];
        }
        
    }];
   
}


/*
 * DESC: Get all user daily stats from parse based on userId
 * Returns: Nothing
 */

+(void) syncAllDailyStats:(id)sender  {
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_DAILY_STATS];
    [queryUE whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    
    [queryUE setLimit: 1000];
    
    [[queryUE findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        [self saveDailyStatsToLocalDB:task.result];
        return task;
    }];
}

/*
 * DESC: Save user daily stats to localdb
 * Returns: Nothing
 */

+(void) saveDailyStatsToLocalDB:(NSArray *) exerciseData {
    int limit = (int)[exerciseData count] ;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *stats = [exerciseData objectAtIndex:i];
            
            DailyStats *data = [DailyStats MR_createEntityInContext:localContext];
            data.date =  stats[@"Date"];
            data.bodyFat =  stats[@"BodyFat"];
            data.bodyMassIndex = stats[@"BodyMassIndex"];
            data.caloriesBurned = stats[@"CaloriesBurned"];
            //                rObj[@"ElMorning"] = item.elMorning;
            //                rObj[@"ElPreWorkout"] = item.elPreWorkout;
            //                rObj[@"ElPostWorkout"] = item.elPostWorkout;
            data.steps = stats[@"Steps"];
            data.weight = stats[@"Weight"];
            data.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
    }];
    
}

+(void) syncExerciseMetaInfoToParse {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncedState == %@", [NSNumber numberWithBool:SYNC_NOT_DONE]];
    NSArray *exMetaInfoArr = [ExerciseMetaInfo MR_findAllWithPredicate:predicate inContext:localContext];
    //NSLog(@"sncing routine info... %lu\n %@", (long) [routines count], [RoutineMetaInfo MR_findAllInContext:localContext]);
    
    NSLog(@"****** %s %lu", __FUNCTION__, (long) [exMetaInfoArr count]);


        for (ExerciseMetaInfo *exercise in exMetaInfoArr) {
            
            NSLog(@"sncing routine info... %@", exercise.exerciseName);
            
            PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISEMETAINFO_CLASS];
            [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
            [query whereKey:@"ExerciseName" equalTo:exercise.exerciseName];
            [query setLimit: 1];
            [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
                if (task.error) {
                    NSLog(@"Error: %@", task.error);
                    return task;
                }
                PFObject *rObj;
                if ([task.result count] == 0) {
                    NSLog(@"Did not find a routine");
                    rObj = [PFObject objectWithClassName:@P_EXERCISEMETAINFO_CLASS];
                    rObj[@"Comment"] = ([exercise.comment length] > 0) ? exercise.comment : @"None";
                    rObj[@"ExerciseName"] = exercise.exerciseName;//str
                    rObj[@"HandAngle"] = (exercise.handAngle == nil) ? [NSNumber numberWithInt:0]: exercise.handAngle;//num
                    rObj[@"MaxDate"] = ([exercise.maxDate length] > 0) ? exercise.maxDate: @"";//str
                    rObj[@"MaxRep"] = (exercise.maxRep == nil) ? [NSNumber numberWithInt:0]: exercise.maxRep; //num
                    rObj[@"MaxWeight"] = (exercise.maxWeight == nil) ? [NSNumber numberWithFloat:0]: exercise.maxWeight; //num
                    rObj[@"SeatHeight"] = (exercise.seatHeight == nil) ? [NSNumber numberWithInt:0]: exercise.seatHeight;//num
                    rObj[@"Muscle"] = ([exercise.muscle length] > 0) ? exercise.muscle: @"";;//str
                    rObj[@"UserId"] = [PFUser currentUser].objectId;
                    rObj[@"GoalWeightOrRep"] = (exercise.goalWeightOrRep == nil) ? [NSNumber numberWithInt:0]: exercise.goalWeightOrRep;
                    rObj[@"GoalAchieveDate"] = (exercise.goalAchieveDate == nil)? [NSDate date] : exercise.goalAchieveDate;
                    [rObj saveEventually];                    
                    NSLog(@"Exercise Meta info created...");
                } else {
                    NSLog(@"routine found.. updating.. %d", (int)[task.result count]);
                    rObj = [task.result objectAtIndex:0];
                    rObj[@"Comment"] = ([exercise.comment length] > 0) ? exercise.comment : @"None";
                    rObj[@"ExerciseName"] = exercise.exerciseName;//str
                    rObj[@"HandAngle"] = (exercise.handAngle == nil) ? [NSNumber numberWithInt:0]: exercise.handAngle;//num
                    rObj[@"MaxDate"] = ([exercise.maxDate length] > 0) ? exercise.maxDate: @"";//str
                    rObj[@"MaxRep"] = (exercise.maxRep == nil) ? [NSNumber numberWithInt:0]: exercise.maxRep; //num
                    rObj[@"MaxWeight"] = (exercise.maxWeight == nil) ? [NSNumber numberWithFloat:0]: exercise.maxWeight; //num
                    rObj[@"SeatHeight"] = (exercise.seatHeight == nil) ? [NSNumber numberWithInt:0]: exercise.seatHeight;//num
                    rObj[@"Muscle"] = ([exercise.muscle length] > 0) ? exercise.muscle: @"";;//str
                    rObj[@"UserId"] = [PFUser currentUser].objectId;
                    rObj[@"GoalWeightOrRep"] = (exercise.goalWeightOrRep == nil) ? [NSNumber numberWithInt:0]: exercise.goalWeightOrRep;
                    rObj[@"GoalAchieveDate"] = (exercise.goalAchieveDate == nil)? [NSDate date] : exercise.goalAchieveDate;
                    NSLog(@"Exercise Meta Info updated...");
                    [rObj saveEventually];
                    
                    
                }
                return task;
            }];
            NSLog(@"ex meta info sync state updated....");
            exercise.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            [localContext MR_saveToPersistentStoreAndWait];
        }
    
    NSLog(@"##### %lu",(long)[[ExerciseMetaInfo MR_findAllWithPredicate:predicate inContext:localContext] count]);
}
/*
 * Desc: Iterate over ExerciseSet that contains user recorded data. And the create the Meta information about the exercise locally. This way we know what are the PRs and when they were created.
 * Return: Nothing.
 */

+(void) syncAllExerciseMetaInfo:(id)sender  {
    NSLog(@"syncing exercise meta info now....");
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *allExerciseSets = [ExerciseSet MR_findAllSortedBy:@"exerciseName" ascending:YES inContext:localContext];//[ExerciseSet findAllSortedBy:@"exerciseName" ascending:YES inContext:localContext];
    
    
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in allExerciseSets) {
        NSString *string = data.exerciseName;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
            
        }
    }
    
    for (NSString *exercise in unique) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise];
        NSArray *exerciseNameArray = [allExerciseSets filteredArrayUsingPredicate:predicate];
        
        float weight = 0;
        int indexMax = 0, rep = 0;
        NSLog(@"count of sets are : %lu %@", (long)[exerciseNameArray count], exercise);
        for (int i = 0; i < [exerciseNameArray count]; i++) {
            ExerciseSet *data = [exerciseNameArray objectAtIndex:i];
//            if ([data.exerciseName isEqualToString:@"Squat"]) {
//                NSLog(@"SQ %d %@, %@, %@", i, data.weight, data.rep, data.date);
//            }
            if ([data.weight floatValue] > weight) {
                indexMax = i;
                weight = [data.weight floatValue];
                rep = [data.rep intValue];
            } else if ([data.weight floatValue] == weight) {
                if ([data.rep intValue] >= rep) {
                    indexMax = i;
                    weight = [data.weight floatValue];
                    rep = [data.rep intValue];
                }
            }
            //NSLog(@"weight %.1f", [data.weight floatValue]);
        }
        ExerciseSet *maxSet = [exerciseNameArray objectAtIndex:indexMax];
        NSLog(@"index with max is %d, %@", indexMax, maxSet.exerciseName);

        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            ExerciseMetaInfo *exMetaInfp = [ExerciseMetaInfo MR_createEntityInContext:localContext];
            //NSLog(@"MAX Data: %@", exMetaInfp);
            exMetaInfp.maxDate = maxSet.date;
            exMetaInfp.handAngle =  [NSNumber numberWithInt:0];
            exMetaInfp.seatHeight =  [NSNumber numberWithInt:0];
            exMetaInfp.maxRep =  maxSet.rep;
            exMetaInfp.maxWeight =  maxSet.weight;
            exMetaInfp.exerciseName = maxSet.exerciseName;
            exMetaInfp.muscle = maxSet.muscle;
            exMetaInfp.syncedState = [NSNumber numberWithBool:true];
            exMetaInfp.comment = @"";
            //NSLog(@"Exercise Meta info saved for %@", maxSet.exerciseName);
        }];
    }
    
//    this is wrong...
    NSLog(@"Not sure why this is wrong...");
    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISEMETAINFO_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query setLimit: 1000];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            NSLog(@"Number of item meta info recieved is %lu", [task.result count]);
            for (PFObject *maxSet in task.result) {
                ExerciseMetaInfo *exMetaInfp;
                NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", maxSet[@"ExerciseName"]];
                NSArray *metaItems = [ExerciseMetaInfo MR_findAllWithPredicate:exPredicate inContext:localContext];
            
                if ([metaItems count] == 0) {
                    exMetaInfp = [ExerciseMetaInfo MR_createEntityInContext:localContext];
                } else {
                    exMetaInfp = [metaItems objectAtIndex:0];
//                    NSLog(@"item found in DB %@", exMetaInfp);
                }
                // we are not over writing reps, weight and date because all those are better computed when downloading the user data and processing it. We download everything else.
//                exMetaInfp.maxRep =  maxSet[@"MaxRep"];
//                exMetaInfp.maxWeight = maxSet[@"MaxWeight"];
//                exMetaInfp.maxDate = maxSet[@"MaxDate"];
                exMetaInfp.handAngle =  maxSet[@"HandAngle"];
                exMetaInfp.seatHeight =  maxSet[@"SeatHeight"];
                exMetaInfp.exerciseName = maxSet[@"ExerciseName"];
                exMetaInfp.muscle = maxSet[@"Muscle"];
                exMetaInfp.comment = maxSet[@"Comment"];
                exMetaInfp.goalAchieveDate = (maxSet[@"GoalAchieveDate"] == nil) ? nil: maxSet[@"GoalAchieveDate"];
                exMetaInfp.goalWeightOrRep = (maxSet[@"GoalWeightOrRep"] == nil) ? @0: maxSet[@"GoalWeightOrRep"];
                exMetaInfp.syncedState = [NSNumber numberWithBool:true];
            }
        } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
            NSLog(@"Meta Information is saved now...");
        }];
        
        return task;
    }];
}

+(void) printAllExMetaInfo {

    NSArray *allMeta = [ExerciseMetaInfo MR_findAllSortedBy:@"exerciseName" ascending:YES];
    for (ExerciseMetaInfo *info in allMeta) {
        NSLog(@"%@ %@ %@ %@ %@ %@ %@", info.maxDate, info.handAngle, info.seatHeight, info.maxRep, info.maxWeight, info.exerciseName, info.muscle);
    }
}
+(NSArray *) getAllExerciseForDate:(NSString*) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", date];
    NSArray *exerciseArray = [WorkoutList MR_findAllSortedBy:@"exerciseGroup,exerciseNumInGroup,exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];

    // Build 13: bug 39: When we create WorkoutList and user deletes last exericse or a middle exercise, the exercise numbers goes out of order. Because of this, if user clicks on last exercise, since the array is not small it will result in a crash. So now when ever we read WorkoutList, we make sure that exerciseNumbers are in correct order. This will also help with adding temporary exercises to the workout...
    
    int count = 0;
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (WorkoutList *data in exerciseArray) {
        data.exerciseNumber = [NSNumber numberWithInt:count];
        [temp addObject:data];
        //NSLog(@"current grp %@ %@ %@", data.exerciseGroup, data.exerciseNumInGroup, data.exerciseNumber);
        count++;
    }
    return (NSArray *) temp;
}

+(NSArray *) getAllExerciseForDateByExGroup:(NSString*) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", date];
    NSArray *exerciseArray = [WorkoutList MR_findAllSortedBy:@"exerciseGroup,exerciseNumInGroup,exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
    
    // Build 13: bug 39: When we create WorkoutList and user deletes last exericse or a middle exercise, the exercise numbers goes out of order. Because of this, if user clicks on last exercise, since the array is not small it will result in a crash. So now when ever we read WorkoutList, we make sure that exerciseNumbers are in correct order. This will also help with adding temporary exercises to the workout...
    /*
    int count = 0;
    NSMutableArray *temp = [[NSMutableArray alloc] init];
    for (WorkoutList *data in exerciseArray) {
        data.exerciseNumber = [NSNumber numberWithInt:count];
        [temp addObject:data];
        count++;
    }
    
    //return (NSArray *) temp;
     */
    return [self fixUserWorkoutOrderOnExerciseDelete:exerciseArray];
}

+(NSArray *) fixUserWorkoutOrderOnExerciseDelete:(NSArray *) exerciseArray {
    
    int exGroupCounter = 0;

    NSMutableArray *temp = [[NSMutableArray alloc] init];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];

    for (WorkoutList *data in exerciseArray) {
        NSNumber *groupString = data.exerciseGroup;
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:groupString];
            [processedGroup addObject:data.exerciseGroup];
            NSLog(@"processed %@", data.exerciseGroup);
        }
    }

    NSLog(@"unique is %@", unique);
    int exerciseCount = 0, exerciseGroupRunning = 0;
    for (int i = 0; i < [unique count]; i++) {
        NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", [NSNumber numberWithInt:exGroupCounter]];
        NSArray *exerciseInThisGroup = [exerciseArray filteredArrayUsingPredicate:groupPredicate];
        
        if ([exerciseInThisGroup count] == 0) {
            //this means that intermediate ex got deleted... so we need to be careful.
            NSLog(@"all ex of group got deleted.. lets move on to next one and re-adjust everyone %d", i);
        } else {
            for (int j = 0; j < [exerciseInThisGroup count]; j++) {
                WorkoutList *data = [exerciseInThisGroup objectAtIndex:j];
                data.exerciseGroup = [NSNumber numberWithInt:exerciseGroupRunning];
                data.exerciseNumInGroup = [NSNumber numberWithInt:j];
                data.exerciseNumber = [NSNumber numberWithInt:exerciseCount];
                [temp addObject:data];
                exerciseCount++;
            }
            exerciseGroupRunning++;
        }
        exGroupCounter++;
    }
    
    return temp;
}

+(NSArray *) getSetForExerciseOnDate:(NSString *)exercise date:(NSString *) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@ AND date == %@", exercise, date];
    
    NSArray *sets = [ExerciseSet MR_findAllSortedBy:@"timeStamp" ascending:NO withPredicate:predicate inContext:localContext];
    return sets;
}

+(NSArray *) getSetsForAllExerciseForDate:(NSString*) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", date];
    
    NSArray *sets = [ExerciseSet MR_findAllWithPredicate:predicate inContext:localContext];
    
    return sets;
}

+(NSArray *) getSetsForAllExerciseForDateSortedByTime:(NSString*) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", date];
    
    NSArray *sets = [ExerciseSet MR_findAllSortedBy:@"timeStamp" ascending:YES withPredicate:predicate inContext:localContext];
    
    return sets;
}

+(NSArray *) getSetsForAllExerciseForGivenWorkoutAndRoutine:(NSString*) rName workout:(NSString *) wName {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", rName, wName];
    
    NSArray *sets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:YES withPredicate:predicate inContext:localContext];
    return sets;
}

+(void) getWeightMovedForGivenRoutineAndWorkout:(NSString*) rName workout:(NSString *) wName dateArr:(NSMutableArray **)dateArr weightArray:(NSMutableArray **) weightArr {
    NSArray *allSets = [self getSetsForAllExerciseForGivenWorkoutAndRoutine:rName workout:wName];

    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableArray * processed = [[NSMutableArray alloc] init];
    
    for (ExerciseSet *data in allSets) {
        NSString *string = data.date;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
            NSLog(@"%s, DATE: %@", __FUNCTION__, string);
        }
    }
    for (NSString *date in processed) {
       // NSLog(@"DATE BEING PROCESSED: %@", date);
        NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", date];
        NSArray *workoutDayData = [allSets filteredArrayUsingPredicate:datePredicate];
        float weightMovedInDay = 0;
        for (ExerciseSet *set in workoutDayData) {
            weightMovedInDay += [set.rep intValue] * [set.weight floatValue];
        }
        NSNumber *ttWt = [NSNumber numberWithFloat:weightMovedInDay];
        [*dateArr addObject:date];
        [*weightArr addObject:ttWt];
    }
}

+(NSArray *) getSetForExerciseOnPreviosDate:(NSString *)exercise date:(NSString *) date rName:(NSString *) routineName wName:(NSString *) workoutName{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    
    //reverting back to old method as user liked that more. We will show history  even it they are from different workouts. May be build one for existing workouts. FEATURE:
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@ AND workoutName == %@ AND routineName == %@", exercise, workoutName, routineName];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise];

    // this has to be in this order because we first want all same date data then sort it based on timestamp and finally exerciseNumber. Otherwise we may not necessarily get previous exercise date.
    
    NSArray *sets = [ExerciseSet MR_findAllSortedBy:@"date,timeStamp,exerciseNumber" ascending:NO withPredicate:predicate inContext:localContext];
    
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in sets) {
        NSString *string = data.date;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    
    
    NSString *previousDate;
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSDate *passedDate = [dateFormat dateFromString:date];
    NSDate *tempDate;
    
    int hisCount = (int) [unique count];
    if (hisCount == 0) {
        return nil;
    } else {
        for (int i = 0; i < hisCount; i++) {
            tempDate = [dateFormat dateFromString:[unique objectAtIndex:i]];
            if ([tempDate isEarlierThan:passedDate]) {
                previousDate = [unique objectAtIndex:i];
                break;
            }
        }
    }
    /*
    if ([unique count] > 1) {
            NSLog(@"returnig data for %@ Date **** %@\n",exercise, unique);
//        if ([[self getCurrentDate] isEqualToString:[unique objectAtIndex:0]])
        if ([date isEqualToString:[unique objectAtIndex:0]])
            previousDate = [unique objectAtIndex:1];
        else
            previousDate = [unique objectAtIndex:0];
        
    } else if ([unique count] == 1) {
        previousDate = [unique objectAtIndex:0];
        if ([previousDate isEqualToString:[Utilities getCurrentDate]] || [previousDate isEqualToString:date]) {
            // ony current date workout is present..
            NSLog(@"only 1 workout is available.. ");
            return nil;
        }
    } else
        return nil;
    */
    
    NSPredicate *previousPredicate = [NSPredicate predicateWithFormat:@"date == %@", previousDate];
    NSArray *previousSets = [sets filteredArrayUsingPredicate:previousPredicate];
    // we are reversing it so all sets are sorted by exercise number.
    return [[previousSets reverseObjectEnumerator] allObjects];
}
+(int) getMaxIndexForExerciseSetArray: (NSArray *) exerciseSetArray {
    float maxWgtMoved  = 0, maxIndex = 0, count = 0;
    
    for (ExerciseSet *data in exerciseSetArray) {
        float temp = [data.rep intValue] * [data.weight floatValue];
        if (maxWgtMoved < temp) {
            maxWgtMoved = temp;
            maxIndex = count;
        }
        count++;
    }
    return maxIndex;
}
+(NSArray *) getSetForExercise:(NSString *)exercise {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise];
    
    NSArray *sets = [ExerciseSet MR_findAllSortedBy:@"date,timeStamp,exerciseNumber" ascending:NO withPredicate:predicate inContext:localContext];
    if ([sets count] == 0) {
        NSLog(@"returning NIL %s", __FUNCTION__);
        return nil;
    }
    return sets;
}


+(ExerciseMetaInfo *) getExerciseMetaInfo: (NSString *) exercise {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise];
    
    NSArray *exMetaInfo = [ExerciseMetaInfo MR_findAllWithPredicate:predicate inContext:localContext];
   // NSLog(@"getting exercise meta info.. %lu", (long) [exMetaInfo count]);
    
    if ([exMetaInfo count] == 0) {
     //   NSLog(@"Meta info not created yet");
        return nil;
    }
  //  NSLog(@"meta info count %lu", (long)[exMetaInfo count]);
    return [exMetaInfo objectAtIndex:0];
}

+(void) markExerciseAsInActive:(NSString *)routineName workoutName:(NSString *)wkName exerciseName:(NSString *)exName {
    PFQuery *query = [PFQuery queryWithClassName:@P_WORKOUT_INFO_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query whereKey:@"RoutineName" equalTo:routineName];
    [query whereKey:@"WorkoutName" equalTo:wkName];
    [query whereKey:@"ExerciseName" equalTo:exName];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        if ([task.result count] == 0) {
            NSLog(@"Did not find a routine. Do nothing.");
        } else {
            for (PFObject *rObj in task.result) {
                rObj[@"IsActive"] = @NO;
                [rObj deleteEventually];
            }
        }
        return task.result;
    }];
}

+(void) deleteLocalNotificationWithBody: (NSString *) bodyText {
    NSArray *arrayOfLocalNotifications = [[UIApplication sharedApplication] scheduledLocalNotifications] ;
//    NSLog(@"schedule local notifications are %lu", (unsigned long)[arrayOfLocalNotifications count]);
    
    for (UILocalNotification *localNotification in arrayOfLocalNotifications) {
        if ([localNotification.alertBody isEqualToString:bodyText]) {
            NSLog(@"******* DELETING LOCAL NOTIFICION *****: %@", localNotification.alertBody);
            [[UIApplication sharedApplication] cancelLocalNotification:localNotification] ; // delete the notification from the system
            
        }
        
    }
}

// ================================= ROUTINE ==========================================
/*
 * DESC: Sync Local RoutineMetaInfo with parse.
 * Returns: Nothing
 */

+(void) syncRoutineInfo:(id)sender {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncedState == %@", [NSNumber numberWithBool:SYNC_NOT_DONE]];
    NSArray *routines = [RoutineMetaInfo MR_findAllWithPredicate:predicate inContext:localContext];
    //NSLog(@"sncing routine info... %lu\n %@", (long) [routines count], [RoutineMetaInfo MR_findAllInContext:localContext]);
    
    for (RoutineMetaInfo *routineObj in routines) {
        NSLog(@"sncing routine info... %@", routineObj.routineName);
        
        PFQuery *query = [PFQuery queryWithClassName:@P_ROUTINE_META_INF_CLASS];
        [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
        [query whereKey:@"Name" equalTo:routineObj.routineName];
        [query setLimit: 1];
        [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
            if (task.error) {
                NSLog(@"Error: %@", task.error);
                return task;
            }
            NSLog(@"number of objects found are %lu", (long) [task.result count]);
            PFObject *rObj;
            if ([task.result count] == 0) {
                NSLog(@"Did not find a routine");
                rObj = [PFObject objectWithClassName:@P_ROUTINE_META_INF_CLASS];
                rObj[@"AuthorName"] = @"None";
                rObj[@"Color"] = routineObj.routineColor;
                rObj[@"Complete"] = @NO;
                rObj[@"CreatedDate"] = routineObj.routineCreatedDate;
                rObj[@"Description"] = ([routineObj.routineDescription length] > 0) ? routineObj.routineDescription: @"";
                rObj[@"Duration"] = (routineObj.routineDuration) ? routineObj.routineDuration: @90;
                rObj[@"Goal"] = ([routineObj.routingGoal length] > 0) ? routineObj.routingGoal: @"";
                rObj[@"IsActive"] = @YES;
                rObj[@"Level"] = ([routineObj.routineLevel length] > 0) ? routineObj.routineLevel: @"";
                rObj[@"Name"] = ([routineObj.routineName length] > 0) ? routineObj.routineName: @"";
                rObj[@"Summary"] = ([routineObj.routineSummary length] > 0) ? routineObj.routineSummary: @"";
                rObj[@"Type"] = ([routineObj.routineType length] > 0) ? routineObj.routineType: @"";
                rObj[@"UserId"] = [PFUser currentUser].objectId;
                rObj[@"Instructions"]= ([routineObj.routineInstructions length] > 0) ? routineObj.routineInstructions: @"";
                rObj[@"Disclosure"] = ([routineObj.routineDisclosure length] > 0) ? routineObj.routineDisclosure: @"";
                rObj[@"Faqs"] = ([routineObj.routineFaqs length] > 0) ? routineObj.routineFaqs: @"";
                rObj[@"BundleId"] = routineObj.routineBundleId;
                NSLog(@"Routine saved...");
                [rObj saveEventually];
                
                
            } else {
                NSLog(@"routine found.. updating.. %lu", [task.result count]);
                rObj = [task.result objectAtIndex:0];
                rObj[@"AuthorName"] = @"None";
                rObj[@"Color"] = routineObj.routineColor;
                rObj[@"Complete"] = @NO;
                rObj[@"CreatedDate"] = routineObj.routineCreatedDate;
                rObj[@"Description"] = ([routineObj.routineDescription length] > 0) ? routineObj.routineDescription: @"";
                rObj[@"Duration"] = (routineObj.routineDuration) ? routineObj.routineDuration: @90;
                rObj[@"Goal"] = ([routineObj.routingGoal length] > 0) ? routineObj.routingGoal: @"";
                rObj[@"IsActive"] = @YES;
                rObj[@"Level"] = ([routineObj.routineLevel length] > 0) ? routineObj.routineLevel: @"";
                rObj[@"Name"] = ([routineObj.routineName length] > 0) ? routineObj.routineName: @"";
                rObj[@"Summary"] = ([routineObj.routineSummary length] > 0) ? routineObj.routineSummary: @"";
                rObj[@"Type"] = ([routineObj.routineType length] > 0) ? routineObj.routineType: @"";
                rObj[@"UserId"] = [PFUser currentUser].objectId;
                rObj[@"Instructions"]= ([routineObj.routineInstructions length] > 0) ? routineObj.routineInstructions: @"";
                rObj[@"Disclosure"] = ([routineObj.routineDisclosure length] > 0) ? routineObj.routineDisclosure: @"";
                rObj[@"Faqs"] = ([routineObj.routineFaqs length] > 0) ? routineObj.routineFaqs: @"";
                rObj[@"BundleId"] = routineObj.routineBundleId;
                NSLog(@"Routine updated....");
                [rObj saveEventually];
                
                
            }
            return task;
        }];
        
        NSLog(@"routine sync state updated....");
        routineObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        [localContext MR_saveToPersistentStoreAndWait];
    }
    
    
}

/*
 * DESC: Get all user created Routine meta info from parse
 * Returns: Nothing
 */

+(void) syncAllUserRoutines:(id)sender {
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_ROUTINE_META_INF_CLASS];
    [queryUE whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [queryUE whereKey:@"IsActive" equalTo:@YES];
    
    [queryUE setLimit: 1000];
    
    [[queryUE findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        [self saveRoutineToLocalDB:task.result];
        return task;
    }];
    
}

/*
 * DESC: Save downloaded user created routines to localDB in RoutineMetaInfo DB
 * Returns: Nothing
 */

+(void) saveRoutineToLocalDB:(NSArray *) routineData {
    int limit = (int)[routineData count] ;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *routine = [routineData objectAtIndex:i];
            
            RoutineMetaInfo *newRoutine = [RoutineMetaInfo MR_createEntityInContext:localContext];
            newRoutine.routineName = routine[@"Name"];
            newRoutine.routineAuthor = routine[@"AuthorName"];
            newRoutine.routineSummary = routine[@"Summary"];
            newRoutine.routineDescription = routine[@"Description"];
            newRoutine.routingGoal = routine[@"Goal"];
            newRoutine.routineDuration = routine[@"Duration"];
            newRoutine.routineColor  = routine[@"Color"];
            newRoutine.routineType = routine[@"Type"];
            newRoutine.routineLevel = routine[@"Level"];
            newRoutine.routineCreatedDate = routine[@"CreatedDate"];
            newRoutine.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            newRoutine.routineBundleId = routine[@"BundleId"];
            newRoutine.routineInstructions = routine[@"Instructions"];
            newRoutine.routineFaqs = routine[@"Faqs"];
            newRoutine.routineDisclosure = routine[@"Disclaimer"];
            newRoutine.routineBundleId = routine[@"BundleId"];
            newRoutine.routineUserCreated = [NSNumber numberWithBool:true];

        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        // we do not want update muscle again when user exercises are synced...
        NSLog(@"**** ROUTINE SYNC DONE ****");
    }];
}

+(void) markRoutineAsInActive:(NSString *)routineName {
    PFQuery *query = [PFQuery queryWithClassName:@P_ROUTINE_META_INF_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query whereKey:@"Name" equalTo:routineName];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Routine Delete Error: %@", task.error);
            return task;
        }
        if ([task.result count] == 0) {
            NSLog(@"Did not find a routine. Do nothing.");
        } else {
//            NSLog(@"number of routines found are :%lu", (long)[task.result count]);
            for (PFObject *rObj in task.result) {
//                NSLog(@"rouinte found..deleteing it.. ACL: %@", rObj[@"ACL"]);
                [rObj deleteEventually];
            }
        }
        return task.result;
    }];
}

+(BOOL) downloadIAPRoutineFromStore:(NSString *) routineBundleId view:(id) sender {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *routineCheck = [NSPredicate predicateWithFormat:@"routineBundleId == %@", routineBundleId];
    RoutineMetaInfo *isPresent = [RoutineMetaInfo MR_findFirstWithPredicate:routineCheck inContext:localContext];
    
    if (isPresent != nil) {
        NSLog(@"routine already present.. no need to download..");
        return true;
    }

    NSError *error;
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_IAP_ROUTINES];
    [queryUE whereKey:@"RoutineBundleID" equalTo:routineBundleId];
    [queryUE setLimit: 1];
    [KVNProgress showWithStatus:@"Downloading routine.."];
    NSArray *routineData = [queryUE findObjects:&error];
    NSLog(@"downloading routine ********** %@, %@, %lu", routineBundleId, error, (long)[routineData count]);
    if (!error && [routineData count] > 0) {
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            PFObject *routine = [routineData objectAtIndex:0];
            RoutineMetaInfo *newRoutine = [RoutineMetaInfo MR_createEntityInContext:localContext];
            newRoutine.routineName = routine[@"RoutineName"];
            newRoutine.routineSummary = routine[@"RoutineSummary"];
            newRoutine.routineDescription = routine[@"RoutineDescription"];
            newRoutine.routingGoal = routine[@"RoutineGoal"];
            newRoutine.routineInstructions = routine[@"RoutineInstructions"];
            newRoutine.routineDuration = routine[@"RoutineDuration"];
            newRoutine.routineColor  = routine[@"RoutineColor"];
            newRoutine.routineType = routine[@"RoutineType"];
            newRoutine.routineLevel = routine[@"RoutineLevel"];
            newRoutine.routineCreatedDate = [Utilities getCurrentDate]; // as we dont have created date on server..
            newRoutine.routineFaqs = routine[@"RoutineFAQs"];
            newRoutine.routineDisclosure = routine[@"RoutineDisclaimer"];
            // we are not going to sync the default downloaded routine back to cloud...
            if ([newRoutine.routineName isEqualToString:@IAP_ROUTINE_BODYWEIGHT_MAX])
                newRoutine.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            else
                newRoutine.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
            
            newRoutine.routineAuthor = routine[@"AuthorName"];
            newRoutine.routineBundleId = routineBundleId;
            newRoutine.routineUserCreated = [NSNumber numberWithBool:false];
            NSLog(@"&&&&&& WIll be sycning routing now %@", newRoutine);
            NSLog(@"routine INFO %@", newRoutine);
        } completion:^(BOOL contextDidSave, NSError *error) {
            // we do not want update muscle again when user exercises are synced...

            [Utilities syncRoutineInfo:self];
            [self downloadIAPWorkoutForRoutine:routineBundleId];
            [KVNProgress showSuccessWithStatus:@"Download Complete."];
            [KVNProgress dismiss];
        }];
        return true;
    }
    [KVNProgress showErrorWithStatus:@"Error download routine. Please try again."];
    [KVNProgress dismiss];
    
    return false;
}
+(BOOL) downloadIAPWorkoutForRoutine:(NSString *) routineBundleId {
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_IAP_WORKOUTS];
    [queryUE whereKey:@"RoutineBundleId" equalTo:routineBundleId];
    
    NSError *error;
    NSArray *workoutData = [queryUE findObjects:&error];
    
    if (!error) {
        NSLog(@"Routine bundle id is: %@ %lu", routineBundleId, (long)[workoutData count]);
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            
            //RoutineMetaInfo *isPresent = [RoutineMetaInfo MR_findFirstWithPredicate:routineCheck inContext:localContext];

            for (int i = 0; i < [workoutData count]; i++) {
                PFObject *workoutEntry = [workoutData objectAtIndex:i];
                NSPredicate *workoutCheck = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@ AND exerciseName == %@", workoutEntry[@"RoutineName"], workoutEntry[@"WorkoutName"],  workoutEntry[@"ExerciseName"]];
                UserWorkout *workout = [UserWorkout MR_findFirstWithPredicate:workoutCheck inContext:localContext];
                
                if (workout != nil) {
                    NSLog(@"Workout already present and was downloaded before... %@ %@", workoutEntry[@"RoutineName"], workoutEntry[@"WorkoutName"]);
                    continue;
                } else {
                    workout = [UserWorkout MR_createEntityInContext:localContext];
                    workout.routineName = workoutEntry[@"RoutineName"];
                    workout.workoutName = workoutEntry[@"WorkoutName"];
                    workout.createdDate = workoutEntry[@"CreatedDate"];
                    workout.exerciseName = workoutEntry[@"ExerciseName"];
                    workout.majorMuscle = workoutEntry[@"MajorMuscle"];
                    workout.muscle = workoutEntry[@"Muscle"];
                    workout.exerciseNumber = workoutEntry[@"ExerciseNumber"];
                    workout.sets = workoutEntry[@"Sets"];
                    workout.reps = workoutEntry[@"Reps"];
                    workout.restTimer = workoutEntry[@"RestTimer"];
                    workout.superSetWith = workoutEntry[@"SuperSetWith"];
                    workout.hasDropSet = workoutEntry[@"HasDropSet"];
                    workout.hasPyramidSet = workoutEntry[@"HasPyramidSet"];
                    workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                    workout.routineBundleId = routineBundleId;
                    workout.workoutUserCreated = [NSNumber numberWithBool:false];
                    workout.exerciseNumInGroup = workoutEntry[@"ExerciseNumInGroup"];
                    workout.exerciseGroup = workoutEntry[@"ExerciseGroup"];
                    // we are just making sure reps per set is computed. So we dont crash.
                    workout.repsPerSet = (workoutEntry[@"RepsPerSet"] == nil) ? [Utilities createRepsPerSet:[workout.sets intValue] reps:[workout.reps intValue]] : workoutEntry[@"RepsPerSet"];
                    workout.restPerSet = (workoutEntry[@"RestPerSet"] == nil) ? [Utilities createRestPerSet:[workout.sets intValue] rest:[workout.restTimer intValue]] : workoutEntry[@"RestPerSet"];
                    NSLog(@"downloaded workout %@", workout);
                }
            }
        } completion:^(BOOL contextDidSave, NSError *error) {
            // we do not want update muscle again when user exercises are synced...
            NSLog(@"**** USERWORKOUT SYNC DONE ****");
            [Utilities syncWorkoutInfo:self];
        }];
        return true;
    }

    return false;
}
+(BOOL) isEditingAllowed:(NSString *) routineName {
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];

    RoutineMetaInfo *data = [RoutineMetaInfo MR_findFirstWithPredicate:predicate inContext:localContext];
    NSLog(@"Routine data %@", data);
    if ([data.routineUserCreated boolValue] == true) {
        return true;
    }
    return false;
}
// ================================= WORKOUT ==========================================
/*
 * DESC: Sync user created workout to parse..
 * Returns: Nothing
 */

+(void) syncWorkoutInfo:(id)sender {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncedState == %@", [NSNumber numberWithBool:SYNC_NOT_DONE]];
    NSArray *workouts = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    NSLog(@"will sync workout info now... %lu", (long) [workouts count]);

    for (UserWorkout *workoutObj in workouts) {

        NSLog(@"sncing routine info %@ and workout info... %@, %@, %@", workoutObj.routineName, workoutObj.workoutName, workoutObj.exerciseName, [PFUser currentUser].objectId);
        NSLog(@"workout %@, %@, %@\n", workoutObj.syncedState, workoutObj.exerciseName, workoutObj.exerciseNumber);
        
        
//        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            workoutObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
//            NSLog(@"sync state updated...");
//        }];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

        
        PFQuery *query = [PFQuery queryWithClassName:@P_WORKOUT_INFO_CLASS];
        [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
        [query whereKey:@"RoutineName" equalTo:workoutObj.routineName];
        [query whereKey:@"WorkoutName" equalTo:workoutObj.workoutName];
        [query whereKey:@"ExerciseName" equalTo:workoutObj.exerciseName];
        [query setLimit: 1];
        [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
            if (task.error) {
                NSLog(@"Error: %@", task.error);
                return task;
            }
            PFObject *rObj;
            NSLog(@"number of elements found %lu", (long)[task.result count]);
            if ([task.result count] == 0) {
                NSLog(@"Creating a new workout..");
                    rObj = [PFObject objectWithClassName:@P_WORKOUT_INFO_CLASS];
                    rObj[@"CreatedDate"] = workoutObj.createdDate;
                    rObj[@"ExerciseName"] = workoutObj.exerciseName;
                    rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                    rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                    rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                    rObj[@"IsActive"] = @YES;
                    rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                    rObj[@"Muscle"] = workoutObj.muscle;
                    rObj[@"Reps"]  = workoutObj.reps;
                    rObj[@"RestTimer"]  = workoutObj.restTimer;
                    rObj[@"RoutineName"] = workoutObj.routineName;
                    rObj[@"Sets"] = workoutObj.sets;
                    rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                    rObj[@"UserId"] = [PFUser currentUser].objectId;
                    rObj[@"WorkoutName"] = workoutObj.workoutName;
                    rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                    rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                    rObj[@"RepsPerSet"] = workoutObj.repsPerSet;
                    rObj[@"RestPerSet"] = workoutObj.restPerSet;
                    [rObj saveEventually];
            } else {
                NSLog(@"updating old workout");
                rObj = [task.result objectAtIndex:0];
                rObj[@"CreatedDate"] = workoutObj.createdDate;
                rObj[@"ExerciseName"] = workoutObj.exerciseName;
                rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                rObj[@"IsActive"] = @YES;
                rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                rObj[@"Muscle"] = workoutObj.muscle;
                rObj[@"Reps"]  = workoutObj.reps;
                rObj[@"RestTimer"]  = workoutObj.restTimer;
                rObj[@"RoutineName"] = workoutObj.routineName;
                rObj[@"Sets"] = workoutObj.sets;
                rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                rObj[@"UserId"] = [PFUser currentUser].objectId;
                rObj[@"WorkoutName"] = workoutObj.workoutName;
                rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                rObj[@"RepsPerSet"] = workoutObj.repsPerSet;
                rObj[@"RestPerSet"] = workoutObj.restPerSet;
                [rObj saveEventually];

            }
            NSLog(@"Workout Synced.. %@", workoutObj.workoutName);
            return task.result;
        }];
    
    }
}

/*
 * DESC: Download all user created workouts from pars
 * Returns: Nothing
 */

+(void) syncAllUserWorksouts:(id)sender  {
    PFQuery *queryUE = [PFQuery queryWithClassName:@P_WORKOUT_INFO_CLASS];
    [queryUE whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [queryUE whereKey:@"IsActive" equalTo:@YES];
    
    [queryUE setLimit: 1000];

    [[queryUE findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        [self saveUserWorkoutToLocalDb:task.result];
        return task;
    }];
    
}

/*
 * DESC: Save all downloaded user created workouts from parse to local DB UserWorkout
 * Returns: Nothing
 */

+(void) saveUserWorkoutToLocalDb:(NSArray *) workoutData{
    int limit = (int)[workoutData count] ;
    __block int syncFlag = false;
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *workoutEntry = [workoutData objectAtIndex:i];
            
            UserWorkout *workout = [UserWorkout MR_createEntityInContext:localContext];
            workout.routineName = workoutEntry[@"RoutineName"];
            workout.workoutName = workoutEntry[@"WorkoutName"];
            workout.createdDate = workoutEntry[@"CreatedDate"];
            workout.exerciseName = workoutEntry[@"ExerciseName"];
            workout.majorMuscle = workoutEntry[@"MajorMuscle"];
            workout.muscle = workoutEntry[@"Muscle"];
            workout.exerciseNumber = workoutEntry[@"ExerciseNumber"];
            workout.sets = workoutEntry[@"Sets"];
            workout.reps = workoutEntry[@"Reps"];
            workout.restTimer = workoutEntry[@"RestTimer"];
            workout.superSetWith = workoutEntry[@"SuperSetWith"];
            workout.hasDropSet = workoutEntry[@"HasDropSet"];
            workout.hasPyramidSet = workoutEntry[@"HasPyramidSet"];
            workout.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            // 03-13-2016 the reason routineBundleId and workoutUserCreated are set to none and true, because we dont maintain them on parse. As we dont really care at this point.
            workout.routineBundleId = @"None";
            workout.workoutUserCreated = [NSNumber numberWithBool:true];
            workout.exerciseGroup = (workoutEntry[@"ExerciseGroup"] == nil) ? workoutEntry[@"ExerciseNumber"] : workoutEntry[@"ExerciseGroup"];
            workout.exerciseNumInGroup = (workoutEntry[@"ExerciseNumInGroup"] == nil) ? [NSNumber numberWithInt:0]: workoutEntry[@"ExerciseNumInGroup"];
            NSLog(@"WN:%@, EG:%@/%@ EGN:%@/%@", workout.workoutName, workoutEntry[@"ExerciseGroup"], workout.exerciseGroup, workoutEntry[@"ExerciseNumInGroup"], workout.exerciseNumInGroup);
            
            //this is the case when we have old user who deleted the app but now redownloaded it...
            if (workoutEntry[@"ExerciseGroup"] == nil) {
                workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                syncFlag = true;
            }
            
            if (workoutEntry[@"RestPerSet"] == nil || workoutEntry[@"RepsPerSet"] == nil) {
                workout.restPerSet = [Utilities createRestPerSet:[workout.sets intValue] rest:[workout.restTimer intValue]];
                workout.repsPerSet = [Utilities createRepsPerSet:[workout.sets intValue] reps:[workout.reps intValue]];
                workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                syncFlag = true;
            } else {
                workout.restPerSet = workoutEntry[@"RestPerSet"];
                workout.repsPerSet = workoutEntry[@"RepsPerSet"];
                NSLog(@"already exist. just updating them...");
            }
            
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        // we do not want update muscle again when user exercises are synced...
                NSLog(@"**** USERWORKOUT SYNC DONE ****");
        if (syncFlag == true) {
            [Utilities syncWorkoutInfo:self];
        } else {
            NSLog(@"MAYANK: No need to sync back");
        }
        
        // so we amrap update here so it doesnt update to the parse again.
        [[NSUserDefaults standardUserDefaults] setBool:YES   forKey:USER_DEFAULT_UPDATE_TO_AMRAP];
        [[NSUserDefaults standardUserDefaults] synchronize];    
    }];

}

// this doesnt seem to get called.
+(void) createWorkoutListForExerciseOnDate:(ExerciseList *) exercise date:(NSString *) date workoutInfo:(UserWorkout *) wkInfo {
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
        workout.exerciseName = exercise.exerciseName;
        workout.muscle = exercise.muscle;
        workout.majorMuscle = exercise.majorMuscle;
        workout.date = date;
        workout.timeStamp = [dateFormatter stringFromDate:now];
        workout.exerciseNumber = wkInfo.exerciseNumber;
        workout.routiineName = wkInfo.routineName;
        workout.workoutName = wkInfo.workoutName;
        workout.restSuggested = wkInfo.restTimer;
        workout.repsSuggested = wkInfo.reps;
        workout.setsSuggested = wkInfo.sets;
        workout.exerciseGroup = wkInfo.exerciseGroup;
        workout.exerciseNumInGroup = wkInfo.exerciseNumInGroup;
        workout.repsPerSet = wkInfo.repsPerSet;
        workout.restPerSet = wkInfo.restPerSet;
        exercise.exercisePerformed = [NSNumber numberWithBool:true];
    }];
}


+(void) addWorkoutListForExerciseOnDate:(ExerciseList *) exercise date:(NSString *) date workoutInfo:(UserWorkout *) wkInfo exerciseNumber :(int) exNumber maxExerciseGroup:(int) maxExGroup {
    
    // first check if exercise already present.. skip it other wise... we cant skip it now because a user may have added same exercise to multiple workouts and if we skip them then group number may get messed up
    
//    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
//    NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@ AND date == %@", exercise.exerciseName, date];
//    NSArray *todayExercises = [WorkoutList MR_findAllWithPredicate:exPredicate inContext:localContext];
//    
//    if ([todayExercises count] > 0) {
//        NSLog(@"Exercise (%@) already present for date (%@)", exercise.exerciseName, date);
//        return;
//    }
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
        workout.exerciseName = exercise.exerciseName;
        workout.muscle = exercise.muscle;
        workout.majorMuscle = exercise.majorMuscle;
        workout.date = date;
        workout.timeStamp = [dateFormatter stringFromDate:now];
        workout.exerciseNumber = [NSNumber numberWithInt:exNumber];
        workout.routiineName = wkInfo.routineName;
        workout.workoutName = wkInfo.workoutName;
        workout.restSuggested = wkInfo.restTimer;
        workout.repsSuggested = wkInfo.reps;
        workout.setsSuggested = wkInfo.sets;
        workout.exerciseNumInGroup = wkInfo.exerciseNumInGroup;
        workout.restPerSet = wkInfo.restPerSet;
        workout.repsPerSet = wkInfo.repsPerSet;
        workout.exerciseGroup = [NSNumber numberWithInt:maxExGroup];// we need to do this because a user can add multiple workouts
        exercise.exercisePerformed = [NSNumber numberWithBool:true];
        
    }];
}

+(BOOL) addExerciseToWorkout:(ExerciseList *) exercise date:(NSString *) date routineN:(NSString *)routineName workoutN:(NSString *)workoutName exerciseGroup:(NSNumber *) exGroup exerciseNumInGroup:(NSNumber *) exNumInGrp  customSet:(int) cSets customRep:(int) cRep customRest:(int) cRest repsPerSet:(NSString *) repsPSet restPerSet:(NSString *) restPSet {
    
    // first check if exercise already present.. skip it other wise...
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    
    NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@ AND routineName == %@ AND workoutName == %@", exercise.exerciseName, routineName, workoutName];
    NSArray *todayExercises = [UserWorkout MR_findAllWithPredicate:exPredicate inContext:localContext];
    
    if ([todayExercises count] > 0) {
        NSLog(@"Exercise (%@) already present in workout (%@)", exercise.exerciseName, workoutName);
        return false;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", routineName, workoutName];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        NSArray *elements = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
        int count = (int) [elements count];
        UserWorkout *workout = [UserWorkout MR_createEntityInContext:localContext];
        workout.routineName = routineName;
        workout.workoutName = workoutName;
        workout.createdDate = date;
        workout.exerciseName = exercise.exerciseName;
        workout.majorMuscle = exercise.majorMuscle;
        workout.muscle = exercise.muscle;
        workout.exerciseNumber = [NSNumber numberWithInt:count];
        workout.sets = [NSNumber numberWithInt:cSets];
        workout.reps = [NSNumber numberWithInt:cRep];
        workout.restTimer = [NSNumber numberWithInt:cRest];
        workout.repsPerSet = repsPSet;
        workout.restPerSet = restPSet;
        workout.superSetWith = [NSNumber numberWithInt:-1];
        workout.hasDropSet = [NSNumber numberWithBool:false];
        workout.hasPyramidSet = [NSNumber numberWithBool:false];
        workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
        workout.routineBundleId = @"None";
        workout.workoutUserCreated = [NSNumber numberWithBool:false];
        workout.exerciseGroup = exGroup;
        workout.exerciseNumInGroup = exNumInGrp;
         NSLog(@"Added workout. Not synced to parse yet...");
        // we need to sync this to parse...
    }];
    return true;
}

+(NSArray *) getWorkoutsDayExercises:(NSString *) routineName workoutN:(NSString *)workoutName {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", routineName, workoutName];
    NSArray *exerciseList = [UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
    return exerciseList;
}

+(void) markWorkoutAsInActive:(NSString *)routineName workoutName:(NSString *)wkName {
    // we are actually deleteing them as we dont need to store them...
    PFQuery *query = [PFQuery queryWithClassName:@P_WORKOUT_INFO_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query whereKey:@"RoutineName" equalTo:routineName];
    [query whereKey:@"WorkoutName" equalTo:wkName];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Workout Delete Error: %@", task.error);
            return task;
        }
        if ([task.result count] == 0) {
            NSLog(@"Did not find a routine. Do nothing.");
        } else {
            for (PFObject *rObj in task.result) {
                [rObj deleteEventually];
            }
        }
        return task.result;
    }];
}
+(NSString *) createTempWorkoutForDate:(NSString *)date routineName:(NSString *)rName workoutName:(NSString *)wName exerciseList:(ExerciseList *) exercise exerciseGroup:(NSNumber *) exGroup exerciseNumInGroup:(NSNumber *) exNumInGrp customSet:(int) cSets customRep:(int) cRep customRest:(int) cRest repsPerSet:(NSString *) repsPSet restPerSet:(NSString *) restPSet {
    
    // first check if exercise already present.. skip it other wise...
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    
    NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise.exerciseName];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", date];
    
    NSArray *todayExercises = [WorkoutList MR_findAllWithPredicate:predicate inContext:localContext];
    
    NSArray *existCheck = [todayExercises filteredArrayUsingPredicate:exPredicate];
    
    if ([existCheck count] > 0) {
        return ([NSString stringWithFormat:@"Exercise (%@) already present in today (%@) workout.", exercise.exerciseName, date]);
    }
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
        workout.exerciseName = exercise.exerciseName;
        workout.muscle = exercise.muscle;
        workout.majorMuscle = exercise.majorMuscle;
        workout.date = date;
        workout.timeStamp = [dateFormatter stringFromDate:now];
        workout.exerciseNumber = [NSNumber numberWithInteger:[todayExercises count] + 1];
        workout.routiineName = rName;
        workout.workoutName = wName;
        workout.restSuggested = [NSNumber numberWithInteger:cRest];//[self getDefaultRestTimerValue]];
        workout.repsSuggested = [NSNumber numberWithInteger:cRep];//[self getDefaultRepsValue]];
        workout.setsSuggested = [NSNumber numberWithInteger:cSets];//[self getDefaultSetsValue]];
        workout.exerciseGroup = exGroup;
        workout.repsPerSet = repsPSet;
        workout.restPerSet = restPSet;
        workout.exerciseNumInGroup = exNumInGrp;
        exercise.exercisePerformed = [NSNumber numberWithBool:false];
    }];

    return nil;
}
// =============================== HUD ======================
/*
 * Desc: Show syncing hud.
 * Return: Nothing.
 * NOT USED ANYWHERE
 */

+(void) showSyncingHudFor {
    
    NSArray *muscle = [[NSArray alloc] initWithObjects:@"Abs", @"Back", @"Arms", @"Chest", @"Legs", nil];
    [KVNProgress showWithStatus:@"Building Body.."];
    float progress = 0;
    for (int i = 0 ; i < [muscle count]; i++) {
        sleep(1);
        NSLog(@"sleeping");
        [KVNProgress showProgress:progress status:[muscle objectAtIndex:i]];
        progress += .2;
    }
    [KVNProgress showSuccessWithStatus:@"Body ready. Lets LIFT!"];
    [KVNProgress dismiss];
}

// ========================= CLEANUP ========================
+(void) logout {

    NSLog(@"%lu %lu %lu %lu %lu %lu %lu %lu", (unsigned long)[RoutineMetaInfo MR_countOfEntities], (unsigned long)[UserWorkout MR_countOfEntities],(unsigned long) [ExerciseSet MR_countOfEntities], (unsigned long)[WorkoutList MR_countOfEntities], (unsigned long)[ExerciseList MR_countOfEntities],(unsigned long) [ExerciseMetaInfo MR_countOfEntities], (unsigned long)[MuscleList MR_countOfEntities], (unsigned long)[UserProfile MR_countOfEntities]);

    
    NSArray *allEntities = [NSManagedObjectModel MR_defaultManagedObjectModel].entities;
    
    [allEntities enumerateObjectsUsingBlock:^(NSEntityDescription *entityDescription, NSUInteger idx, BOOL *stop) {
        [NSClassFromString([entityDescription managedObjectClassName]) MR_truncateAll];
        NSLog(@"class is %@", [entityDescription managedObjectClassName]);
    }];
    
    [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
    NSLog(@"saved cleanup");
    
    NSLog(@"%lu %lu %lu %lu %lu %lu %lu %lu", (unsigned long)[RoutineMetaInfo MR_countOfEntities], (unsigned long)[UserWorkout MR_countOfEntities],(unsigned long) [ExerciseSet MR_countOfEntities], (unsigned long)[WorkoutList MR_countOfEntities], (unsigned long)[ExerciseList MR_countOfEntities],(unsigned long) [ExerciseMetaInfo MR_countOfEntities], (unsigned long)[MuscleList MR_countOfEntities], (unsigned long)[UserProfile MR_countOfEntities]);
    
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        for (SKProduct *pid in products) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:pid.productIdentifier];
            if (productPurchased == true) {
                [[NSUserDefaults standardUserDefaults] setBool:NO forKey:pid.productIdentifier];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
        }
        
    }];

    // if user logout then we want to make sure all this is updated as such this is first time login..
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"HasLaunchedOnce"];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"EnableHintKey"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ParseSyncDone"];
    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"MultipleSetSupport"];    
    [[NSUserDefaults standardUserDefaults] synchronize];

    [PFUser logOut];
}

// ========================= EXERCISE SETS ======================

//+(void) parseQuery:(NSMutableArray *) allObjectsNew skip:(NSInteger ) skip limit:(NSInteger ) limit {
+(void) syncUserDataOnLogin:(NSMutableArray *) allObjectsNew skip:(NSInteger ) skip limit:(NSInteger ) limit {
    NSLog(@"skip is %lu %ld, limit %ld", [allObjectsNew count],(long)skip, (long)limit);
    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISESET_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query orderByAscending:@"createdAt"];

    [query setLimit: limit];
    [query setSkip: skip];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (!task.error) {
            [allObjectsNew addObjectsFromArray:task.result];
            NSLog(@"entires found %lu", (long) [task.result count]);
            if ([task.result count] == limit) {
                [self syncUserDataOnLogin:allObjectsNew skip:[allObjectsNew count] limit:limit];
            } else {
                NSLog(@"All downloaded... %lu", (long)[allObjectsNew count]);
                [self saveUserSetToLocalDB:allObjectsNew];
            }
        }
        return allObjectsNew;
    }];
}

+(void) syncUserDataOnLogin:(id)sender  {
    
    NSMutableArray *allObjects = [NSMutableArray array];
    __block NSUInteger limit = 300;
    __block NSUInteger skip = 0;
    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISESET_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query orderByAscending:@"createdAt"];
    [query setLimit: limit];
    [query setSkip: skip];

    [KVNProgress showWithStatus:@"Syncing.."];
    
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (!task.error) {
            // The find succeeded. Add the returned objects to allObjects
            [allObjects addObjectsFromArray:task.result];
            if ([task.result count] == limit) {
                // There might be more objects in the table. Update the skip value and execute the query again.
                skip += limit;
                [query setSkip: skip];
                BFTask *task = [query findObjectsInBackground];
                NSLog(@"got data till entry %lu.. getting more %lu", (unsigned long)limit, [task.result count]);
            } else {
                NSLog(@"got all data..no new data left... %lu", (long) [allObjects count]);
                [KVNProgress dismiss];
                [self saveUserSetToLocalDB:allObjects];
            }
        }
        return task.result;
    }];
     
//    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
//        if (task.error) {
//            NSLog(@"Error: %@", task.error);
//            return task;
//        }
//        [KVNProgress dismiss];
//        [self saveUserSetToLocalDB:task.result];
//        return task;
//    }];
}


+(void) saveUserSetToLocalDB: (NSArray *) userSets {
    int limit = (int)[userSets count] ;
    NSLog(@"user had %d sets", limit);
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *set = [userSets objectAtIndex:i];
            
            ExerciseSet *data = [ExerciseSet MR_createEntityInContext:localContext];
            data.exerciseName =  set[@"ExerciseName"];
            data.muscle =  set[@"Muscle"];
            data.majorMuscle = set[@"MajorMuscle"];
            data.date =  set[@"Date"];
            data.rep = set[@"Rep"];
            data.weight =  set[@"Weight"];
            data.timeStamp = set[@"TimeStamp"];
            data.isDropSet = set[@"IsDropSet"];
            data.isPyramidSet = set[@"IsPyramidSet"];
            data.isSuperSet = set[@"IsSuperSet"];
            data.routineName = set[@"RoutineName"];
            data.workoutName = set[@"WorkoutName"];
            data.exerciseNumber = set[@"ExerciseNumber"];
            data.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        NSLog(@"count of set saved... %lu", (unsigned long)[ExerciseSet MR_countOfEntities]);
        
        [self updateWorkoutListAfterLogin];
        
        //now we create the exerciseMetaInfo
        [self syncAllExerciseMetaInfo:self];
    }];
    //    +(void) createWorkoutListForExerciseOnDate:(ExerciseList *) exercise date:(NSString *) date {
}

+(void) updateWorkoutListAfterLogin {
    
    //once user has logged in... and we have saved all his data locally... we need to finally update thw WorkoutList locally so he can see if he browses back.
    //NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedExercise = [NSMutableSet set];
    NSMutableSet * processedDate = [NSMutableSet set];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        NSArray *allUserSets = [ExerciseSet MR_findAllSortedBy:@"date,timeStamp" ascending:YES inContext:localContext];
        
        for (ExerciseSet *data in allUserSets) {
            
            NSString *stringExerciseAndDate = [NSString stringWithFormat:@"%@,%@", data.exerciseName, data.date];
            BOOL flag = 0;
            
            if ([processedExercise containsObject:stringExerciseAndDate] == NO) {
                //NSLog(@"exercise was not added.... %@", stringExerciseAndDate);
                [unique addObject:stringExerciseAndDate];
                [processedExercise addObject:stringExerciseAndDate];
                [processedDate addObject:stringExerciseAndDate];
                flag = 1;
            }
            
            if (flag == 1) {
                NSPredicate *predicateMetaInfo = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@ AND exerciseName == %@", data.routineName, data.workoutName, data.exerciseName];
                
                UserWorkout *uwData = [UserWorkout MR_findFirstWithPredicate:predicateMetaInfo inContext:localContext];
                
                NSNumber *suggestedSets = [NSNumber numberWithInt:[Utilities getDefaultSetsValue]], *suggestedReps = [NSNumber numberWithInt:[Utilities getDefaultRepsValue]], *suggestedRest = [NSNumber numberWithInt:[Utilities getDefaultRestTimerValue]];
                //NSLog(@"entry found for workout and routine.. ");
                
                //Build 13: Bug 39: We cant rely on exerciseNumber as user can delete an exercise. So when we create a workout, we check how many WorkoutList items we have created for that date and then increment exerciseNumber by 1. This way if user logs out and login, even though we only created workoutList of exercises whose set user recorded, all the exercises are still numbered correctly.
                NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", data.date];
                NSArray *workoutListForDate = [WorkoutList MR_findAllWithPredicate:datePredicate inContext:localContext];
               // NSLog(@"***** WORKOUT LIST COUNT IS %lu, %@", (long)[workoutListForDate count], data.exerciseNumber);
                if (uwData == nil) {
                    // probably no workout is there
                } else {
                    suggestedReps = uwData.reps;
                    suggestedSets = uwData.sets;
                    suggestedRest = uwData.restTimer;
                }
                
                WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
                workout.routiineName = data.routineName;
                workout.workoutName = data.workoutName;
                workout.exerciseName = data.exerciseName;
                workout.majorMuscle = data.majorMuscle;
                workout.muscle = data.muscle;
                workout.date = data.date;
                workout.timeStamp = data.timeStamp;
                workout.exerciseNumber = [NSNumber numberWithInteger:[workoutListForDate count]];//data.exerciseNumber;
                workout.setsSuggested = suggestedSets;
                workout.repsSuggested = suggestedReps;
                workout.restSuggested = suggestedRest;
                workout.timeStamp = data.timeStamp;//[dateFormatter stringFromDate:now];
                workout.exerciseNumInGroup = [NSNumber numberWithInt:0];
                workout.exerciseGroup = [NSNumber numberWithInteger:[workoutListForDate count]];
                
                // for pre-exisiting user who just redownloaded the app...
                workout.repsPerSet = [Utilities createRepsPerSet:[suggestedSets intValue] reps:[suggestedReps intValue]];
                workout.restPerSet = [Utilities createRestPerSet:[suggestedSets intValue] rest:[suggestedRest intValue]];
                
                //NSLog(@"V1.3: WORKOUT LIST CREATED FOR DATE %@", data.date);
                // we are doing this because we have just download the user data. And now we need to set the exercises that have been performed to TRUE so that when user creates a new routine, he will see already perfomed exercises in "Recent" section in ExerciseList.
                
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", data.exerciseName];
                ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:predicate inContext:localContext];
                if (exercise != nil) {
                    exercise.exercisePerformed = [NSNumber numberWithBool:true];
                    //NSLog(@"setting exercise %@ to performed TRUE", data.exerciseName);
                }
            }
        }
        } completion:^(BOOL contextDidSave, NSError *error) {
            NSLog(@"finally all done with data download... we are done with parse.. we can not update any UI");
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ParseSyncDone"];
            [[NSUserDefaults standardUserDefaults] synchronize];

        }];
}

+(void) syncExerciseSetToParse: (NSString *) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND syncedState == %@", date, [NSNumber numberWithBool:SYNC_NOT_DONE]];
    
    NSArray *unsyncedSets = [ExerciseSet MR_findAllWithPredicate:predicate inContext:localContext];
    
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (ExerciseSet *exerciseObj in unsyncedSets) {
            PFObject *workoutList = [PFObject objectWithClassName:@P_EXERCISESET_CLASS];
            workoutList[@"ExerciseName"] = exerciseObj.exerciseName;
            workoutList[@"Muscle"] = exerciseObj.muscle;
            workoutList[@"MajorMuscle"] = exerciseObj.majorMuscle;
            workoutList[@"Date"] = exerciseObj.date;
            workoutList[@"Rep"] = [NSNumber numberWithInteger:[exerciseObj.rep integerValue]];
            workoutList[@"Weight"] = [NSNumber numberWithFloat:[exerciseObj.weight floatValue]];
            workoutList[@"UserId"] = [PFUser currentUser].objectId;
            workoutList[@"TimeStamp"] = exerciseObj.timeStamp;
            workoutList[@"IsDropSet"] = ([exerciseObj.isDropSet boolValue]) ? @YES: @NO;
            workoutList[@"IsPyramidSet"] = ([exerciseObj.isPyramidSet boolValue]) ? @YES: @NO;
            workoutList[@"IsSuperSet"] = ([exerciseObj.isSuperSet boolValue]) ? @YES: @NO;
            workoutList[@"RoutineName"] = exerciseObj.routineName;
            workoutList[@"WorkoutName"] = exerciseObj.workoutName;
            workoutList[@"ExerciseNumber"] = exerciseObj.exerciseNumber;
            [workoutList saveEventually];
            
            exerciseObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        }
    }];
    NSLog(@"Count of entries for today %lu", (long) [unsyncedSets count]);
    
}


+(void) syncDropSetToParse: (NSString *) date {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND syncedState == %@", date, [NSNumber numberWithBool:SYNC_NOT_DONE]];
    
    NSArray *unsyncedSets = [ExerciseSet MR_findAllWithPredicate:predicate inContext:localContext];
    
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (ExerciseSet *exerciseObj in unsyncedSets) {
            
            PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISESET_CLASS];
            [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
            [query whereKey:@"Date" equalTo:exerciseObj.date];
            [query whereKey:@"TimeStamp" equalTo:exerciseObj.timeStamp];
            [query setLimit:1];

            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                if ([objects count] > 0) {
                    // this should always pass as we are updating an object that has already been updated..
                    PFObject *workoutList = [objects objectAtIndex:0];
                    workoutList[@"ExerciseName"] = exerciseObj.exerciseName;
                    workoutList[@"Muscle"] = exerciseObj.muscle;
                    workoutList[@"MajorMuscle"] = exerciseObj.majorMuscle;
                    workoutList[@"Date"] = exerciseObj.date;
                    workoutList[@"Rep"] = [NSNumber numberWithInteger:[exerciseObj.rep integerValue]];
                    workoutList[@"Weight"] = [NSNumber numberWithFloat:[exerciseObj.weight floatValue]];
                    workoutList[@"UserId"] = [PFUser currentUser].objectId;
                    workoutList[@"TimeStamp"] = exerciseObj.timeStamp;
                    workoutList[@"IsDropSet"] = ([exerciseObj.isDropSet boolValue]) ? @YES: @NO;
                    workoutList[@"IsPyramidSet"] = ([exerciseObj.isPyramidSet boolValue]) ? @YES: @NO;
                    workoutList[@"IsSuperSet"] = ([exerciseObj.isSuperSet boolValue]) ? @YES: @NO;
                    workoutList[@"RoutineName"] = exerciseObj.routineName;
                    workoutList[@"WorkoutName"] = exerciseObj.workoutName;
                    workoutList[@"ExerciseNumber"] = exerciseObj.exerciseNumber;
                    [workoutList saveEventually];
                }
                
            }];
            exerciseObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        }
    }];
    NSLog(@"drop set count of entries for today %lu", (long) [unsyncedSets count]);
    
}

+(void) addExercisePersonalRecordEntry: (ExerciseMetaInfo *) exerciseObj setInfo: (ExerciseSet *) set setInfo:(WorkoutList *) exerciseSetInfo {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"HH:mm:ss"];
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    
    float userWt = [UserWeightManager getLastUserWeight];
    ExercisePersonalRecords *exercisePR = [ExercisePersonalRecords MR_createEntityInContext:localContext];
    exercisePR.exerciseName = exerciseObj.exerciseName;
    exercisePR.routineName = set.routineName;
    exercisePR.workoutName = set.workoutName;
    exercisePR.prRep = exerciseObj.maxRep;
    exercisePR.prWeight  = exerciseObj.maxWeight;
    exercisePR.prDate = [formatter dateFromString:exerciseObj.maxDate];
    exercisePR.prTime = [timeFormatter dateFromString:set.timeStamp];
    exercisePR.syncedState = [NSNumber numberWithBool:SYNC_DONE];
    exercisePR.roundNumber = @1;
    exercisePR.prUserWeight = [NSNumber numberWithFloat:userWt];
    exercisePR.prExerciseNumber = exerciseSetInfo.exerciseNumber;
    exercisePR.prGroupNumber = exerciseSetInfo.exerciseGroup;
    exercisePR.prExerciseNumInGroup = exerciseSetInfo.exerciseNumInGroup;
    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
        NSLog(@"PR SAVED");
         PFObject *pr = [PFObject objectWithClassName:@P_EX_PERSONAL_REC];
         pr[@"ExerciseName"] = exercisePR.exerciseName;
         pr[@"RoutineName"] = exercisePR.routineName;
         pr[@"WorkoutName"] = exercisePR.workoutName;
         pr[@"PrRep"] = exercisePR.prRep;
         pr[@"PrWeight"] = exercisePR.prWeight;
         pr[@"PrDate"] = exercisePR.prDate;
         pr[@"PrTime"] = exercisePR.prTime;
         pr[@"RoundNumber"] = exercisePR.roundNumber;
         pr[@"PrUserWeight"] = exercisePR.prUserWeight;
         pr[@"PrExerciseNumber"] = exercisePR.prExerciseNumber;
         pr[@"PrExerciseGroup"] = exercisePR.prGroupNumber;
         pr[@"PrExerciseNumInGroup"] = exercisePR.prExerciseNumInGroup;
         pr[@"UserId"] = [PFUser currentUser].objectId;
         [pr saveEventually];
    }];
}
+(void) syncExercisePersonalRecordEntry:(NSMutableArray *) allObjectsNew skip:(NSInteger ) skip limit:(NSInteger ) limit {
    NSLog(@"getting entires... skip is %lu %ld, limit %ld", [allObjectsNew count],(long)skip, (long)limit);
    PFQuery *query = [PFQuery queryWithClassName:@P_EX_PERSONAL_REC];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    
    [query setLimit: limit];
    [query setSkip: skip];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (!task.error) {
            [allObjectsNew addObjectsFromArray:task.result];
            NSLog(@"PR entires found %lu", (long) [task.result count]);
            if ([task.result count] == limit) {
                [self syncExercisePersonalRecordEntry:allObjectsNew skip:[allObjectsNew count] limit:limit];
            } else {
                NSLog(@"PR All downloaded... %lu", (long)[allObjectsNew count]);
                [self saveExercisePersonalRecordToLocalDB:allObjectsNew];
            }
        }
        return allObjectsNew;
    }];
}


+(void) saveExercisePersonalRecordToLocalDB:(NSMutableArray *) allPRs {
    int limit = (int)[allPRs count] ;
    NSLog(@"user had %d PRs", limit);
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (int i = 0; i < limit; i++) {
            PFObject *pr = [allPRs objectAtIndex:i];
            
            ExercisePersonalRecords *exercisePR = [ExercisePersonalRecords MR_createEntityInContext:localContext];
            exercisePR.exerciseName = pr[@"ExerciseName"];
            exercisePR.routineName = pr[@"RoutineName"];
            exercisePR.workoutName = pr[@"WorkoutName"];
            exercisePR.prRep = pr[@"PrRep"];
            exercisePR.prWeight  = pr[@"PrWeight"];
            exercisePR.prDate = pr[@"PrDate"];
            exercisePR.prTime = pr[@"PrTime"];
            exercisePR.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            exercisePR.roundNumber = pr[@"RoundNumber"];
            exercisePR.prUserWeight = pr[@"PrUserWeight"];
            exercisePR.prExerciseNumber = pr[@"PrExerciseNumber"];
            exercisePR.prGroupNumber = pr[@"PrExerciseGroup"];
            exercisePR.prExerciseNumInGroup = pr[@"PrExerciseNumInGroup"];
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        NSLog(@"count of set saved... %lu", (unsigned long)[ExerciseSet MR_countOfEntities]);
    }];
}

+(void) addExerciseSetToParse: (ExerciseSet *) exerciseObj {
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        PFObject *workoutList = [PFObject objectWithClassName:@P_EXERCISESET_CLASS];
        workoutList[@"ExerciseName"] = exerciseObj.exerciseName;
        workoutList[@"Muscle"] = exerciseObj.muscle;
        workoutList[@"MajorMuscle"] = exerciseObj.majorMuscle;
        workoutList[@"Date"] = exerciseObj.date;
        workoutList[@"Rep"] = [NSNumber numberWithInteger:[exerciseObj.rep integerValue]];
        workoutList[@"Weight"] = [NSNumber numberWithFloat:[exerciseObj.weight floatValue]];
        workoutList[@"UserId"] = [PFUser currentUser].objectId;
        workoutList[@"TimeStamp"] = exerciseObj.timeStamp;
        workoutList[@"IsDropSet"] = ([exerciseObj.isDropSet boolValue]) ? @YES: @NO;
        workoutList[@"IsPyramidSet"] = ([exerciseObj.isPyramidSet boolValue]) ? @YES: @NO;
        workoutList[@"IsSuperSet"] = ([exerciseObj.isSuperSet boolValue]) ? @YES: @NO;
        workoutList[@"RoutineName"] = exerciseObj.routineName;
        workoutList[@"WorkoutName"] = exerciseObj.workoutName;
        workoutList[@"ExerciseNumber"] = exerciseObj.exerciseNumber;
        [workoutList saveEventually];
        
        exerciseObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
    }];
}


+(void) updateExerciseSetToParse: (ExerciseSet *) exerciseObj {
    
    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISESET_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
//    [query whereKey:@"RoutineName" equalTo:exerciseObj.routineName];
//    [query whereKey:@"WorkoutName" equalTo:exerciseObj.workoutName];
//    [query whereKey:@"ExerciseName" equalTo:exerciseObj.exerciseNumber];
//    [query whereKey:@"Muscle" equalTo:exerciseObj.muscle];
//    [query whereKey:@"MajorMuscle" equalTo:exerciseObj.majorMuscle];
    [query whereKey:@"Date" equalTo:exerciseObj.date];
    [query whereKey:@"TimeStamp" equalTo:exerciseObj.timeStamp];
    [query setLimit:1];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if ([objects count] > 0) {
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                PFObject *workoutList = [objects objectAtIndex:0];
                // this should always be 1... we cannot have more than 1 set for a given user on a give date and time..
                workoutList[@"ExerciseName"] = exerciseObj.exerciseName;
                workoutList[@"Muscle"] = exerciseObj.muscle;
                workoutList[@"MajorMuscle"] = exerciseObj.majorMuscle;
                workoutList[@"Date"] = exerciseObj.date;
                workoutList[@"Rep"] = [NSNumber numberWithInteger:[exerciseObj.rep integerValue]];
                workoutList[@"Weight"] = [NSNumber numberWithFloat:[exerciseObj.weight floatValue]];
                workoutList[@"UserId"] = [PFUser currentUser].objectId;
                workoutList[@"TimeStamp"] = exerciseObj.timeStamp;
                workoutList[@"IsDropSet"] = ([exerciseObj.isDropSet boolValue]) ? @YES: @NO;
                workoutList[@"IsPyramidSet"] = ([exerciseObj.isPyramidSet boolValue]) ? @YES: @NO;
                workoutList[@"IsSuperSet"] = ([exerciseObj.isSuperSet boolValue]) ? @YES: @NO;
                workoutList[@"RoutineName"] = exerciseObj.routineName;
                workoutList[@"WorkoutName"] = exerciseObj.workoutName;
                workoutList[@"ExerciseNumber"] = exerciseObj.exerciseNumber;
                [workoutList saveEventually];
                exerciseObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
            }];
        }
        
    }];
    
}

+(void) deleteExerciseSetToParse: (NSString *) date timeStamp:(NSString *) timestamp {

    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISESET_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    //    [query whereKey:@"RoutineName" equalTo:exerciseObj.routineName];
    //    [query whereKey:@"WorkoutName" equalTo:exerciseObj.workoutName];
    //    [query whereKey:@"ExerciseName" equalTo:exerciseObj.exerciseNumber];
    //    [query whereKey:@"Muscle" equalTo:exerciseObj.muscle];
    //    [query whereKey:@"MajorMuscle" equalTo:exerciseObj.majorMuscle];
    [query whereKey:@"Date" equalTo:date];
    [query whereKey:@"TimeStamp" equalTo:timestamp];
    [query setLimit:1];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if ([objects count] > 0) {
            PFObject *workoutList = [objects objectAtIndex:0];
            // this should always be 1... we cannot have more than 1 set for a given user on a give date and time..
            [workoutList deleteEventually];
        }
    }];
}

// ====================== USER PROFILE DATA =====================

+(void) syncUserProfileOnLogin:(id)sender  {
    PFQuery *queryUP = [PFQuery queryWithClassName:@P_USERPROFILE_CLASS];
    [queryUP whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [queryUP setLimit: 1];
  
    NSArray *task = [queryUP findObjects];
    if ([task count] > 0) {
        PFObject *userData = [task objectAtIndex:0];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            UserProfile *data = [UserProfile MR_createEntityInContext:localContext];
            data.email = [PFUser currentUser].email;
            data.userName = userData[@"Name"];
            data.sex = userData[@"Sex"];
            data.units = userData[@"Units"];
            data.experienceLevel = userData[@"ExperienceLevel"];
            data.weight = userData[@"Weight"];
            data.age = userData[@"Age"];
            data.height = userData[@"Height"];
            data.membershipType = nil;
            data.deviceType = [Utilities getIphoneName];
            NSLog(@"User PROFILE SYNCED... %@", data);
            if ([data.userName length] == 0)
                data.userName = @"";
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:data.userName forKey:@"userFullName"];
            [defaults synchronize];
        }];
    }
}

+(void) createUserProfileOnSignUp:(id) sender {
    NSLog(@"Profuile created on localdb");
    if ([UserProfile MR_countOfEntities] == 0) {
        
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            UserProfile *data = [UserProfile MR_createEntityInContext:localContext];
            data.email = [PFUser currentUser].email;
            data.userName = [PFUser currentUser].username;
            data.sex = [NSNumber numberWithInt:0];
            data.units = [NSNumber numberWithInt:0];
            data.experienceLevel = [NSNumber numberWithInt:0];
            data.age = [NSNumber numberWithInt:0];
            data.weight = [NSNumber numberWithInt:0];
            data.height = [NSNumber numberWithInt:0];
            data.membershipType = @"FREE";
            data.deviceType = [Utilities getIphoneName];
        }];
        PFObject *data = [PFObject objectWithClassName:@P_USERPROFILE_CLASS];
        data[@"UserId"] = [PFUser currentUser].objectId;
        data[@"Name"] = [Utilities getUserFullNameFromFacebookOrTwitter];
        data[@"Age"] = [NSNumber numberWithInt:0];
        data[@"Sex"] = @NO;
        data[@"Weight"] = [NSNumber numberWithInt:0];
        data[@"Height"] = [NSNumber numberWithInt:0];
        data[@"ExperienceLevel"] = [NSNumber numberWithInt:0];
        data[@"Units"] = @NO;
        data[@"MembershipType"] = @"FREE";
        data[@"DeviceType"] = [Utilities getIphoneName];
        [data saveEventually];
        NSLog(@"Profuile created on parse");
    } else {
        NSLog(@"Userprofile has %lu elements", (unsigned long)[UserProfile MR_countOfEntities]);
    }

}

+(void) updateUserWorkoutToSetBasedSystem {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *workoutsList = [UserWorkout MR_findAllSortedBy:@"routineName,workoutName,exerciseNumber" ascending:YES inContext:localContext];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    
    for (UserWorkout *data in workoutsList) {
        NSString *groupString = [NSString stringWithFormat:@"%@-%@", data.routineName, data.workoutName];
        
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:data];
            [processedGroup addObject:groupString];
            NSLog(@"processed %@", groupString);
        }
    }

    //NSLog(@"list \n\n %@", unique);
    
    // we also need to correct the exercise order as it is possible user has deleted the exercise.
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        for (UserWorkout *data in unique) {
            NSPredicate *rtPredicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", data.routineName, data.workoutName];
            
            NSArray *rtWtArray = [workoutsList filteredArrayUsingPredicate:rtPredicate];
            
            int exCount = 0;
            for (UserWorkout *workout in rtWtArray) {
                // NSLog(@"Routine Name: %@, Workout name %@, exName: %@, exNumber: %@, sync:%@", workout.routineName, workout.workoutName, workout.exerciseName, workout.exerciseNumber, workout.syncedState);
                if (workout.exerciseGroup == nil) {
                    NSLog(@"group is nil.. need to be set accordingly...");
                    workout.exerciseNumInGroup = [NSNumber numberWithInt:0];
                    workout.exerciseGroup = [NSNumber numberWithInt:exCount];
                    workout.exerciseNumber = [NSNumber numberWithInt:exCount];
                    workout.syncedState = [NSNumber numberWithBool:false];
                    exCount++;
                } else {
                    NSLog(@"not updating.. group not nil %@", workout.exerciseGroup);
                }
            }
        }
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        NSLog(@"V1.3: Updated all user Workout sets to multiset...");
        [Utilities syncWorkoutInfo:self];
        [self updateWorkoutList];
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MultipleSetSupport"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSLog(@"DOWNLOADED EVERYTHING.. SETTING MultipleSetSupport TO YES.. %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"MultipleSetSupport"]);
    }];
}


// this method is to update the workoutList that is created and shown in TOdaysWorkout.

+(void) updateWorkoutList {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *workoutsList = [WorkoutList MR_findAllSortedBy:@"routiineName,workoutName,exerciseNumber" ascending:YES inContext:localContext];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    for (WorkoutList *data in workoutsList) {
        NSString *groupString = [NSString stringWithFormat:@"%@-%@-%@", data.routiineName, data.workoutName, data.date];
        
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:data];
            [processedGroup addObject:groupString];
            NSLog(@"processed %@", groupString);
        }
    }

    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        for (WorkoutList *data in unique) {
            NSPredicate *rtPredicate = [NSPredicate predicateWithFormat:@"routiineName == %@ AND workoutName == %@ AND date == %@", data.routiineName, data.workoutName, data.date];
            
            NSArray *rtWtArray = [workoutsList filteredArrayUsingPredicate:rtPredicate];
            
            int exCount = 0;
            for (WorkoutList *workout in rtWtArray) {
                // NSLog(@"Routine Name: %@, Workout name %@, exName: %@, exNumber: %@", workout.routiineName, workout.workoutName, workout.exerciseName, workout.exerciseNumber);
                if (workout.exerciseGroup == nil) {
                    NSLog(@"group is nil.. need to be set accordingly...");
                    workout.exerciseNumInGroup = [NSNumber numberWithInt:0];
                    workout.exerciseGroup = [NSNumber numberWithInt:exCount];
                    workout.exerciseNumber = [NSNumber numberWithInt:exCount];
                    exCount++;
                }
            }
        }
    } completion:^(BOOL contextDidSave, NSError * _Nullable error) {
        NSLog(@"V1.3: Updated all WorkoutList to multiset...");
    }];

}

+(int)getNumberOfGroupsInWorkoutRouting:(NSString *)routineName workoutName:(NSString *)wkName {
    NSArray *allExerciseInWorkout = [Utilities getWorkoutsDayExercises:routineName workoutN:wkName];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    
    for (UserWorkout *data in allExerciseInWorkout) {
        NSNumber *groupString = data.exerciseGroup;
        
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:groupString];
            [processedGroup addObject:data.exerciseGroup];
            NSLog(@"processed %@", data.exerciseGroup);
        }
    }
    NSLog(@"unique count is %lu", [unique count]);
    return (int)[unique count];
}

+(int)getNumberOfGroupsInCurrentWorkoutForTempExercise:(NSString *) date {
    
    NSArray *exerciseList = [Utilities getAllExerciseForDate:date];

    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    
    for (WorkoutList *data in exerciseList) {
        NSNumber *groupString = data.exerciseGroup;
        
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:groupString];
            [processedGroup addObject:data.exerciseGroup];
            NSLog(@"processed %@", data.exerciseGroup);
        }
    }
    NSLog(@"unique count is %lu", [unique count]);
    return (int)[unique count];
}

+(int) getNumberOfWorkoutThisWeek: (NSArray *) userSets {
    
    NSMutableDictionary *weekDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    
    for (ExerciseSet *set in userSets) {
        NSDate *date = [dateFormatter dateFromString:set.date];
        NSInteger workoutYear = [date year];
        if (workoutYear != currentYear)
            continue;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierISO8601];
        NSNumber *weekNumber = [NSNumber numberWithLong:[[calendar components: NSCalendarUnitWeekOfYear fromDate:date] weekOfYear]];
        //NSLog(@"Date: %@ week: %@", set.date, weekNumber);
        
        NSMutableDictionary *value = [weekDict objectForKey:weekNumber];
        NSNumber *workoutPerWeek;
        if (value  == nil) {
            // insert object with value 1
            value = [[NSMutableDictionary alloc] init];
            workoutPerWeek = [NSNumber numberWithInt:1];
        } else {
            
            //value = [NSNumber numberWithInt:[value intValue] + 1];
            workoutPerWeek = [value objectForKey:set.workoutName];
            if (workoutPerWeek == nil) {
                workoutPerWeek = [NSNumber numberWithInt:1];
            } else {
                workoutPerWeek = [NSNumber numberWithInt:[workoutPerWeek intValue] + 1];
            }
            [value setObject:workoutPerWeek forKey:set.workoutName];
        }
        [weekDict setObject:value forKey:weekNumber];
    }
    
    float avgWorkoutPerWeek = 0, maxWorkoutPerWeek = 0, totalWorkouts = 0;
    NSNumber *maxWorkoutWeek = nil;
    for (id key in weekDict) {
        NSMutableDictionary *temp = weekDict[key];
        int count = (int)[[temp allKeys] count];
        if (count >= maxWorkoutPerWeek) {
            maxWorkoutPerWeek = count;
            maxWorkoutWeek = key;
        }
        totalWorkouts += count;
    }
    avgWorkoutPerWeek = totalWorkouts/[[weekDict allKeys] count];
    //NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    return avgWorkoutPerWeek;
    
}

+(void) startWorkoutOnSignup:(NSString *)routineName workoutName:(NSString *)workoutName {
    
    //step:1 get all exercise from Workout that has to be added to today workout.
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", routineName, workoutName];
    NSArray *exercisesInWorkout = [UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSString *_date = [formatter stringFromDate:[NSDate date]];

    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        
        for (UserWorkout *workoutInfo in exercisesInWorkout) {
            WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
            workout.exerciseName = workoutInfo.exerciseName;
            workout.muscle = workoutInfo.muscle;
            workout.majorMuscle = workoutInfo.majorMuscle;
            workout.date = _date;
            workout.timeStamp = [dateFormatter stringFromDate:now];
            workout.exerciseNumber = workoutInfo.exerciseNumber;
            workout.routiineName = workoutInfo.routineName;
            workout.workoutName = workoutInfo.workoutName;
            workout.restSuggested = workoutInfo.restTimer;
            workout.repsSuggested = workoutInfo.reps;
            workout.setsSuggested = workoutInfo.sets;
            workout.exerciseNumInGroup = workoutInfo.exerciseNumInGroup;
            workout.exerciseGroup = workoutInfo.exerciseGroup;
            workout.repsPerSet = workoutInfo.repsPerSet;
            workout.restPerSet = workoutInfo.restPerSet;
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
    }];
}

+(void) checkAndSetNotification {
    
    UIUserNotificationType userNotificationTypes = (UIUserNotificationTypeAlert |
                                                    UIUserNotificationTypeBadge |
                                                    UIUserNotificationTypeSound);
    UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:userNotificationTypes
                                                                             categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    
    
    NSString *key = @"LAST_NOTIFICATION_SENT_DATE";
    NSDate *date = [[NSUserDefaults standardUserDefaults] objectForKey:key];
    //    if (date ==)
    NSLog(@"default date is %@", date);
    // You have to implement isDateLowerThanToday
    if (!date || [self isDateLowerThanToday:date]) {
        [self scheduleNotification];
        date = [NSDate date];
        [[NSUserDefaults standardUserDefaults] setObject:date forKey:key];
    }
    
}
+(bool)isDateLowerThanToday:(NSDate *)date {
    NSDate *now = [NSDate date];
    NSInteger daysApart = [date daysFrom:now];
    NSLog(@"date checking... result today:%@ yesterday:%@ %ld", now, date, (long)daysApart);
    
    return daysApart;//result == NSOrderedDescending;
}
+(void) cleanUpAllNotifications {
    //    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
    //    [NSKeyedArchiver archiveRootObject:notification toFile:path];
    //    UILocalNotification *noti = [NSKeyedUnarchiver unarchiveObjectWithFile:path];
    //    [[UIApplication sharedApplication] cancelLocalNotification:notification];
    //    [[NSFileManager defaultManager] removeItemAtPath:path error:nil];
    
}
+(void)scheduleNotification {
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    // before we create any new ones, cancel all existing notifications
    [[UIApplication sharedApplication]cancelAllLocalNotifications];
    
    if ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone) {
        NSLog(@"***** notification scheduled.. **** ");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        NSCalendar *cal = [NSCalendar currentCalendar];
        NSDateComponents *comp = [cal components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[NSDate date]];
        comp.hour = 17; // 17 = 5PM
        comp.minute = 5; // 5:00 PM
        comp.second = 01; // 5:01:01 PM
        
        localNotification.fireDate = [cal dateFromComponents:comp];
        localNotification.alertBody = @"You haven't recorded any workout today. Remember, consistency is the key.";
        localNotification.alertTitle = @"No Workout Recorded Today";
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.repeatInterval = NSCalendarUnitDay;
        
        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        // this will fire the notification right away, it will still also fire at the date we set
        //        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
    }
}

+(NSString *) createRestPerSet:(int) sets rest:(int) rest {
    NSString *restPerSet = @"";
    for (int i = 0; i < sets; i++) {
        if ([restPerSet isEqualToString:@""])
            restPerSet = [NSString stringWithFormat:@"%d", rest];
        else
            restPerSet = [NSString stringWithFormat:@"%@,%d", restPerSet, rest];
    }
    return restPerSet;

}
+(NSString *) createRepsPerSet:(int) sets reps:(int) reps {
    NSString *repsPerSet = @"";
    for (int i = 0; i < sets; i++) {
        if ([repsPerSet isEqualToString:@""])
            repsPerSet = [NSString stringWithFormat:@"%d", reps];
        else
            repsPerSet = [NSString stringWithFormat:@"%@,%d", repsPerSet, reps];
    }
    return repsPerSet;

}


@end
