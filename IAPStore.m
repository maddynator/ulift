//
//  IAPStore.m
//  gyminutes
//
//  Created by Mayank Verma on 7/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "IAPStore.h"
#import "WorkoutStore.h"
#import "PackageStore.h"

@interface IAPStore() 
@end


@implementation IAPStore
-(void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"Store";
    items = @[@"WORKOUTS", @"PACKAGES"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    [self style];
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(CGRectGetWidth(self.view.frame), 49)];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStylePlain target:self action:@selector(restoreTapped:)];
    
    

}
// Add new method
- (void)restoreTapped:(id)sender {
    
    if (![Utilities connected]) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"No Connectivity" subTitle:@"Unable to connect. Please check your network connectivity." closeButtonTitle:@"Ok" duration:0.0f];
        return;
    }
    [KVNProgress showWithStatus:@"Restoring.."];
    NSLog(@"trying to restore...");
    
    [[uLiftIAPHelper sharedInstance] restoreCompletedTransactions];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"RestoreWorkouts" withParameters:articleParams];
    
    //[KVNProgress dismiss];
    //    [self.refreshControl beginRefreshing];
    //    [self reload];
}

-(void) viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(CGRectGetWidth(self.view.frame), 49)];
    [self style];
    [carbonTabSwipeNavigation setIndicatorColor:[Utilities getAppColor]];    
}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];//[UIColor colorWithRed:24.0/255 green:75.0/255 blue:152.0/255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    int width = CGRectGetWidth(self.view.frame)/[items count];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [Utilities getAppColor];//[UIColor whiteColor];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:1];
    //    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:2];
    //    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:3];
    //    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:4];
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont boldSystemFontOfSize:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0: {
            WorkoutStore *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WorkoutStore"];
            return destVC;
        }
        case 1: {
            PackageStore *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PackageStore"];
            return destVC;
        }
    
        default:
            return nil;
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

@end
