//
//  AppDelegate.m
//  uLift
//
//  Created by Mayank Verma on 6/26/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AppDelegate.h"
#import <Parse/Parse.h>
#import "commons.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import "uLiftIAPHelper.h"
#import "Flurry.h"
#import "Mixpanel.h"
#import <AVFoundation/AVFoundation.h>

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {

    [[AFNetworkReachabilityManager sharedManager] startMonitoring];

    [Parse initializeWithConfiguration:[ParseClientConfiguration configurationWithBlock:^(id<ParseMutableClientConfiguration> configuration) {
        configuration.applicationId = @"myAppId";
        configuration.clientKey = @"";
        configuration.server = @"http://parse.gyminutesapp.com:1337/parse";
    }]];

    
    // Initialize Parse.
//    [Parse setApplicationId:@"6D0gtFauuXaPgNJAv0OZ5uY8RXa0QMACoTUD7qe9"
//                  clientKey:@"sXStLxunkQatGR2602W7v3oLUkIEJCjMu7gXHe1a"];

            
    
    // [Optional] Track statistics around application opens.
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    //Facebook login initialization
    [PFFacebookUtils initializeFacebookWithApplicationLaunchOptions:launchOptions];
    
    //Twitter login initialization
//    [PFTwitterUtils initializeWithConsumerKey:@"lKYh0ev955iWY51rYtl0w8jLk"
//                               consumerSecret:@"W2CAg6LL3BI8QS8YjdQoW1UlFkJTHmSoOrh3cyOMMpZUvdRuQo"];
/*
    UIColor *color = FlatOrange;

    UIColor *backgroundColor = FlatOrange;
    
    // set the bar background color
    [[UITabBar appearance] setBackgroundImage:[AppDelegate imageFromColor:backgroundColor forSize:CGSizeMake(320, 49) withCornerRadius:0]];
    
    
    // set the selected icon color
    [[UITabBar appearance] setTintColor:[UIColor blackColor]];
    // remove the shadow
    [[UITabBar appearance] setShadowImage:nil];
    
    // Set the dark color to selected tab (the dimmed background)
    //[[UITabBar appearance] setSelectionIndicatorImage:[AppDelegate imageFromColor:FlatOrange forSize:CGSizeMake(64, 49) withCornerRadius:0]];
    
    [[UITabBar appearance] setBarTintColor:FlatOrange];
    [[UITabBar appearance] setTintColor:FlatBlackDark];
    [[UITabBar appearance] setBackgroundColor:FlatBlackDark];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatWhite }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatBlack }
                                             forState:UIControlStateSelected];
    
    //    //unselected icon tint color
    [[UINavigationBar appearance] setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    [[UINavigationBar appearance] setBackgroundColor:color];
    [[UINavigationBar appearance] setTintColor:FlatWhite];
    [[UINavigationBar appearance] setBarTintColor:color];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [[UINavigationBar appearance] setTranslucent:NO];
*/
    
    UIColor *color = [Utilities getAppColor];
    UIColor *lightColor = [Utilities getAppColorLight];
    
    if (color == nil) {
        color = FlatOrangeDark;
        lightColor = [UIColor whiteColor];
        NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
        NSData *colorDataLight = [NSKeyedArchiver archivedDataWithRootObject:lightColor];
        [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"AppColor"];
        [[NSUserDefaults standardUserDefaults] setObject:colorDataLight forKey:@"AppColorLight"];
    }

    [Chameleon setGlobalThemeUsingPrimaryColor:color withSecondaryColor:[UIColor clearColor]  andContentStyle:UIContentStyleLight];
    // set the bar background color
    [[UITabBar appearance] setBackgroundImage:[AppDelegate imageFromColor:color forSize:CGSizeMake(320, 49) withCornerRadius:0]];
    
    // set the selected icon color
    [[UITabBar appearance] setTintColor:[UIColor blackColor]];
    // remove the shadow
    [[UITabBar appearance] setShadowImage:nil];
    
    // Set the dark color to selected tab (the dimmed background)
    //[[UITabBar appearance] setSelectionIndicatorImage:[AppDelegate imageFromColor:FlatOrange forSize:CGSizeMake(64, 49) withCornerRadius:0]];
    
    //[[UITabBar appearance] setBarTintColor:color];
    [[UITabBar appearance] setBarStyle:UIBarStyleBlack];
    [[UITabBar appearance] setTintColor:FlatBlack];
    [[UITabBar appearance] setBackgroundColor:FlatBlack];
    
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatWhite }
                                             forState:UIControlStateNormal];
    [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatBlack }
                                             forState:UIControlStateSelected];
    
    // NO Sleep
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    
    [MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"uLift"];
    [MagicalRecord setupAutoMigratingCoreDataStack];
    [MagicalRecord currentStack];
//    [Utilities logout];
    
//    [DDLog addLogger:[DDASLLogger sharedInstance]];
//    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
    [[NSUserDefaults standardUserDefaults] setObject:version
                                              forKey:@"version_preference"];
    
    NSString *build = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    [[NSUserDefaults standardUserDefaults] setObject:build
                                              forKey:@"build_preference"];

    [Fabric with:@[[Crashlytics class]]];

    BOOL flag = [[FBSDKApplicationDelegate sharedInstance] application:application
                                    didFinishLaunchingWithOptions:launchOptions];
    NSLog(@"%s %d", __FUNCTION__, flag);
    
    /*
    [Appirater setAppId:@"1059671198"];
    [Appirater setDaysUntilPrompt:15];
    [Appirater setUsesUntilPrompt:20];
    [Appirater setSignificantEventsUntilPrompt:-1];
    [Appirater setTimeBeforeReminding:3];
    [Appirater setDebug:NO];
    [Appirater appLaunched:YES];
    */
    
    
    [Utilities deleteLocalNotificationWithBody:@REST_TIMER_OVER_TEXT];

    // this is to make sure all defaults are registered properly.
    [[NSUserDefaults standardUserDefaults] registerDefaults:[NSDictionary dictionaryWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Defaults" ofType:@"plist"]]];

    // we need this here because user can quit after making a purchse.. so we need to make sure that transaction is complete next time user opens the app. So we register for the purchases made here.
    
    [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
        NSLog(@"Reachability: %@", AFStringFromNetworkReachabilityStatus(status));
        dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
            //Background Thread
            [uLiftIAPHelper sharedInstance];
        });

    }];
    
    [Utilities syncExerciseMetaInfoToParse];

    [Flurry startSession:@"WN53K9VRW8C7DPJ3GR87"];
    [Mixpanel sharedInstanceWithToken:MIXPANEL_TOKEN];

    // removing all local notification if any but we should be careful as this removes local notification for notify timer as well
//    [[UIApplication sharedApplication] cancelAllLocalNotifications];

    NSLog(@"Showing premium package %d", [Utilities showPremiumPackage]);
    //first time launch...
    if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
    {
        NSLog(@"first app launch");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"EnableHintKey"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"EnableHintKeyHome"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ParseSyncDone"];
        [[NSUserDefaults standardUserDefaults] setInteger:3 forKey:@"defaultSetsKey"];
        [[NSUserDefaults standardUserDefaults] setInteger:8 forKey:@"defaultRepsKey"];
        [[NSUserDefaults standardUserDefaults] setInteger:45 forKey:@"defaultRestTimerKey"];
        [[NSUserDefaults standardUserDefaults] setInteger:45 forKey:@"defaultAdditionalRestTimerKey"];
        [[NSUserDefaults standardUserDefaults] setInteger:90 forKey:@"defaultExerciseRestTimerKey"];
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"defaultCustomKeyBoard"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedPremium"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedWorkoutPackage"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedDataPackage"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedPowerRoutinePackage"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"PurchasedAnalyticsPackage"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"FreeAppInstall"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"ExerciseListDBSync"];
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"MultipleSetSupport"];        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    //[Utilities printAllExMetaInfo];
    
    // iOS 8:
    
    if ([UITableView instancesRespondToSelector:@selector(setLayoutMargins:)]) {
        
        [[UITableView appearance] setLayoutMargins:UIEdgeInsetsZero];
        [[UITableViewCell appearance] setLayoutMargins:UIEdgeInsetsZero];
        [[UITableViewCell appearance] setPreservesSuperviewLayoutMargins:NO];
        
    }
    NSLog(@"Bundle id :%@, %@",[[NSBundle mainBundle] bundleIdentifier], [PFUser currentUser].objectId);
 //   [Parse setLogLevel:PFLogLevelDebug];

    
    // now voice wil be sent even if phone is in silent.
    NSError *setCategoryErr = nil;
    NSError *activationErr  = nil;
    [[AVAudioSession sharedInstance] setCategory: AVAudioSessionCategoryPlayback error:&setCategoryErr];
    [[AVAudioSession sharedInstance] setActive:YES error:&activationErr];
    
    [EHPlainAlert updateHidingDelay:2];
    [EHPlainAlert updateNumberOfAlerts:2];
    [EHPlainAlert updateTitleFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    [EHPlainAlert updateSubTitleFont:[UIFont fontWithName:@HAL_BOLD_FONT size:10]];

    // Bug:prevent app from stopping the music
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryAmbient error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    

    KVNProgressConfiguration *configuration = [[KVNProgressConfiguration alloc] init];
    configuration.tapBlock = ^(KVNProgress *progressView) {
        if ([KVNProgress isVisible]) {
            [KVNProgress dismiss];
        }
    };
    [KVNProgress setConfiguration:configuration];



    return flag;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    [Appirater appEnteredForeground:YES];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    [FBSDKAppEvents activateApp];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
    // Store the deviceToken in the current installation and save it to Parse.
    PFInstallation *currentInstallation = [PFInstallation currentInstallation];
    [currentInstallation setDeviceTokenFromData:deviceToken];
    currentInstallation.channels = @[ @"global" ];
    [currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    [PFPush handlePush:userInfo];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication  annotation:(id)annotation {
    BFURL *parsedUrl = [BFURL URLWithInboundURL:url sourceApplication:sourceApplication];
    if ([parsedUrl appLinkData]) {
        // this is an applink url, handle it here
        NSURL *targetUrl = [parsedUrl targetURL];
        [[[UIAlertView alloc] initWithTitle:@"Received link:"
                                    message:[targetUrl absoluteString]
                                   delegate:nil
                          cancelButtonTitle:@"OK"
                          otherButtonTitles:nil] show];
    }
    
    BOOL flag = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                          openURL:url
                                                sourceApplication:sourceApplication
                                                       annotation:annotation];
    NSLog(@"%s %d", __FUNCTION__, flag);
    return flag;
}

+ (UIImage *)imageFromColor:(UIColor *)color forSize:(CGSize)size withCornerRadius:(CGFloat)radius
{
    CGRect rect = CGRectMake(0, 0, size.width, size.height);
    UIGraphicsBeginImageContext(rect.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Begin a new image that will be the new image with the rounded corners
    // (here with the size of an UIImageView)
    UIGraphicsBeginImageContext(size);
    
    // Add a clip before drawing anything, in the shape of an rounded rect
    [[UIBezierPath bezierPathWithRoundedRect:rect cornerRadius:radius] addClip];
    // Draw your image
    [image drawInRect:rect];
    
    // Get the image, here setting the UIImageView image
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    // Lets forget about that we were drawing
    UIGraphicsEndImageContext();
    
    return image;
}

@end
