//
//  main.m
//  uLift
//
//  Created by Mayank Verma on 6/26/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
