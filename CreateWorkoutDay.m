//
//  CreateWorkoutDay.m
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "CreateWorkoutDay.h"
#import "CreateWorkout.h"
#import "CreateWorkoutNew.h"

@interface CreateWorkoutDay () {
    NSArray *dayArray, *muscleArray;
    NSArray *workoutDays;
    UISegmentedControl *segmentedControl;
    NSMutableArray *workoutDayName, *unique;
    NSString *selectedDay;
   // int selectedWorkoutNumber;
    NSArray *workoutDaysCount;
    NYSegmentedControl *foursquareSegmentedControl;
    NSMutableArray *dataArr, *heightArr;
    float lblHeight ;
}
@end

@implementation CreateWorkoutDay
@synthesize tableView, routineName, coachMarksView, collectionView;

- (void)viewDidLoad {
    [super viewDidLoad];
    lblHeight = 20;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.text = routineName;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    subTitleLabel.text = @"Workouts";
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;

    
    dayArray = [[NSArray alloc] initWithObjects:@"M", @"T", @"W", @"Th", @"F", @"Sa", @"Su", @"Any", nil];
    muscleArray = [Utilities getMuscleList];
    if ([muscleArray count] == 0) {
        [Utilities syncAllExercises:self];
    }

    unique  = [NSMutableArray array];
    workoutDayName = [[NSMutableArray alloc] init];
    
    // show button to all users but dont perform action if it is free version
    UIBarButtonItem *exportBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createNewWorkoutDay)];
    
    self.navigationItem.rightBarButtonItem = exportBtn;
    
    [self getWorkoutsDay];
    
    foursquareSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"LIST", @"ANALYZE"]];
    foursquareSegmentedControl.titleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    foursquareSegmentedControl.backgroundColor = FlatWhiteDark;
    foursquareSegmentedControl.borderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorInset = 2.0f;
    foursquareSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    [foursquareSegmentedControl sizeToFit];
    foursquareSegmentedControl.cornerRadius = 5.0f;
    foursquareSegmentedControl.selectedSegmentIndex = 0;
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];

    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0
    foursquareSegmentedControl.usesSpringAnimations = YES;
#endif
    [foursquareSegmentedControl addTarget:self action:@selector(switchView:) forControlEvents:UIControlEventValueChanged];
    
    foursquareSegmentedControl.frame = CGRectMake(9, 5, CGRectGetWidth(self.view.frame) - 18, 35);
    
    //    foursquareSegmentedControl.layer.shadowOffset = CGSizeMake(-8, 8);
    //    foursquareSegmentedControl.layer.shadowRadius = 5;
    //    foursquareSegmentedControl.layer.shadowOpacity = 0.5;
    
    // Add the control to your view
    [self.view addSubview:foursquareSegmentedControl];
    
    dataArr = [[NSMutableArray alloc] init];
    heightArr = [[NSMutableArray alloc] init];

}

#pragma switchViews
-(IBAction) switchView:(id)sender {
    
    switch (foursquareSegmentedControl.selectedSegmentIndex) {
        case 0: {
            [collectionView removeFromSuperview];
            tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, CGRectGetMaxY(foursquareSegmentedControl.frame) + 5, self.view.frame.size.width, CGRectGetHeight(self.view.frame) - CGRectGetMaxY(foursquareSegmentedControl.frame)) style:UITableViewStylePlain];
            
            tableView.rowHeight = 45;
            tableView.sectionFooterHeight = 22;
            tableView.sectionHeaderHeight = 22;
            tableView.scrollEnabled = YES;
            tableView.showsVerticalScrollIndicator = YES;
            tableView.userInteractionEnabled = YES;
            tableView.bounces = YES;
            
            tableView.delegate = self;
            tableView.dataSource = self;
            tableView.emptyDataSetSource = self;
            tableView.emptyDataSetDelegate = self;

            [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
            [self.tableView setBackgroundColor:[UIColor whiteColor]];
            [self.tableView registerClass:[MGSwipeTableCell class] forCellReuseIdentifier:@"Cell"];
            [self.view addSubview:tableView];

        }
            break;
        case 1: {
            [tableView removeFromSuperview];
            UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
            collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(foursquareSegmentedControl.frame) + 5, self.view.frame.size.width, CGRectGetHeight(self.view.frame) - CGRectGetMaxY(foursquareSegmentedControl.frame)) collectionViewLayout:layout];
            collectionView.backgroundColor = [UIColor whiteColor];
            [collectionView setDataSource:self];
            [collectionView setDelegate:self];
            
            [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
            [self.view addSubview:collectionView];
        }
            break;
            
        default:
            break;
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidDisappear:(BOOL)animated {
}

-(void) viewDidAppear:(BOOL)animated {
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];
    [self switchView:foursquareSegmentedControl];

    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
    workoutDaysCount = [UserWorkout MR_findAllSortedBy:@"createdDate" ascending:NO withPredicate:predicate inContext:localContext];

    if ([workoutDaysCount count] == 0) {
        [self createNewWorkoutDay];
    } else {
        [self showCoachMarks];
        [Utilities syncWorkoutInfo:self];
    }
    [self getAllData];
    //[tableView reloadData];
}
-(void) getWorkoutsDay {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
    workoutDays = [UserWorkout MR_findAllSortedBy:@"createdDate" ascending:NO withPredicate:predicate inContext:localContext];
    
//    NSLog(@"unumber of workouts days availabe are %lu", (long) [workoutDays count]);
    NSMutableSet * processed = [NSMutableSet set];
    
    for (UserWorkout *data in workoutDays) {
        NSString *string = data.workoutName;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
            [workoutDayName addObject:string];
        }
    }
//    [tableView reloadData];
}


-(void) createNewWorkoutDay {
    bool showFlag = false;
    NSString *tempRBid = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
    NSLog(@"workout bundle is %@, %@", tempRBid, _routineBundleId);
    // check if it is a downloaded workout and using free version. No edits allowed on those...
    if (![_routineBundleId containsString:tempRBid]) {
        if ([Utilities showPowerRoutinePackage]) {
            [self showPurchasePopUp:@"Adding workouts to downloaded routine is restricted in free version"];
        } else {
            showFlag = true;
        }
    } else {
        showFlag = true;
    }
    
    if (showFlag) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        alert.shouldDismissOnTapOutside = true;
        UITextField *workoutNameTF = [alert addTextField:@"Like Chest Day, Legs Day etc."];
        [workoutNameTF becomeFirstResponder];
        
        [alert addButton:@"Done" actionBlock:^{
            // workout name has been added to list..
            if ([workoutNameTF.text length] == 0) {
                SCLAlertView *alert = [[SCLAlertView alloc] init];
                [alert showError:self title:@"Name Missing" subTitle:@"Workout name is required." closeButtonTitle:@"Ok" duration:0.0f];
            } else {
                if ([workoutNameTF.text length] == 0) {
                    SCLAlertView *alert = [[SCLAlertView alloc] init];
                    [alert showError:self title:@"Name Missing" subTitle:@"Day name is required." closeButtonTitle:@"Ok" duration:0.0f];
                } else {
                    [workoutDayName addObject:workoutNameTF.text];
                    selectedDay = workoutNameTF.text;
                    //selectedWorkoutNumber = (int)[workoutDaysCount count];
                    [self performSegueWithIdentifier:@"addExerciseToWorkoutDaySegue" sender:self];
                    
                }
            }
        }];
        
        [alert showNotice:self.parentViewController title:@"Create New Workout." subTitle:@"Please provide a workout name." closeButtonTitle:nil duration:0.0f];
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"number of days %lu", (unsigned long)[workoutDayName count]);
    return [workoutDayName count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *workoutDName = [workoutDayName objectAtIndex:indexPath.row];
    NSArray *exerciseInDay = [Utilities getWorkoutsDayExercises:routineName workoutN:workoutDName];
    
    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    // adding the dash line...
    
    float cellWidth = CGRectGetWidth(cell.frame);
    float leftColumn = cellWidth/2;
    float rightColumn = leftColumn/3;
    
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else {
            //            NSLog(@"catn remove label");
        }
    }
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]] && lbl.tag == 501) {
            [lbl removeFromSuperview];
        } else {
            //            NSLog(@"catn remove label");
        }
    }
    
    
    UILabel *dayName = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, cellWidth, 30)];
    dayName.text = workoutDName;
    dayName.tag = 100;
    dayName.textAlignment = NSTextAlignmentCenter;
    [dayName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    dayName.textColor = [UIColor whiteColor];
    
    [cell.contentView addSubview:dayName];
    if ([exerciseInDay count] == 0) {
        UILabel *noExercise = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(dayName.frame), cellWidth - 10, 30)];
        noExercise.text = @"No exercise added.";
        noExercise.textAlignment = NSTextAlignmentCenter;
        [noExercise setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        noExercise.textColor = [UIColor whiteColor];
        
        dayName.backgroundColor = _routineColor;
        cell.backgroundColor = _routineColor;

        [cell.contentView addSubview:noExercise];
    } else {
        UILabel *setLbl = [[UILabel alloc] initWithFrame:CGRectMake(leftColumn, CGRectGetMaxY(dayName.frame), rightColumn - 20, 20)];
        UILabel *repLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLbl.frame), CGRectGetMaxY(dayName.frame), rightColumn + 10, 20)];
        UILabel *restTimerLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repLbl.frame), CGRectGetMaxY(dayName.frame), rightColumn + 10, 20)];
        
        setLbl.text = @"Sets";
        repLbl.text = @"Reps";
        restTimerLbl.text = @"Time (s)";
        
        setLbl.textAlignment = NSTextAlignmentLeft;
        repLbl.textAlignment = NSTextAlignmentLeft;
        restTimerLbl.textAlignment = NSTextAlignmentLeft;
        
        setLbl.textColor = [UIColor whiteColor];
        repLbl.textColor = [UIColor whiteColor];
        restTimerLbl.textColor = [UIColor whiteColor];

        
        [setLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        [repLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        [restTimerLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        
        [cell.contentView addSubview:setLbl];
        [cell.contentView addSubview:repLbl];
        [cell.contentView addSubview:restTimerLbl];
        
        
        NSMutableArray *exerciseGroupInWorkout = [[NSMutableArray alloc] init];
        NSMutableArray *exerciseInWorkout = [[NSMutableArray alloc] init];
        
        NSMutableArray * uniqueGrp = [NSMutableArray array];
        NSMutableSet * processedGroup = [NSMutableSet set];
        float maxY = CGRectGetMaxY(setLbl.frame);
        float newY = maxY;
        //float lblHeight = 21;
        
        for (UserWorkout *data in exerciseInDay) {
            [exerciseInWorkout addObject:data];
                NSNumber *groupString = data.exerciseGroup;
            
            if ([processedGroup containsObject:groupString] == NO) {
                [uniqueGrp addObject:groupString];
                [processedGroup addObject:data.exerciseGroup];
            }
        }
        NSLog(@"Number of groups are %lu %@", (unsigned long)[processedGroup count], processedGroup);
        for (int i = 0; i < [processedGroup count]; i++) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %d", i];
            
            NSArray *allExInWorkout = [exerciseInWorkout filteredArrayUsingPredicate:predicate];
            NSLog(@"AllExInWorkout %lu", (unsigned long)[allExInWorkout count]);
            NSMutableArray *ex = [[NSMutableArray alloc] init];
            for (UserWorkout *item in allExInWorkout) {
                [ex addObject:item];
            }
            [exerciseGroupInWorkout addObject:ex];
            NSLog(@"exGroup %d, exNuminGroup %lu", i, (unsigned long)[[exerciseGroupInWorkout objectAtIndex:i] count]);
        }
        
        int exNumberCount = 0;
        for (int j = 0; j < [exerciseGroupInWorkout count]; j++) {
            int exInGroupCountTotal = (int)[[exerciseGroupInWorkout objectAtIndex:j] count];
            float rowGap = 21;
            maxY = newY;
            
            for  (int i = 0 ; i < exInGroupCountTotal; i++) {
                UserWorkout *workoutInfo = [exerciseInDay objectAtIndex:exNumberCount];
                UILabel *exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(5, maxY + i * rowGap, leftColumn, lblHeight)];
                UILabel *setLblVal = [[UILabel alloc] initWithFrame:CGRectMake(leftColumn, maxY + i * rowGap, rightColumn - 20, lblHeight)];
                UILabel *repLblVal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLblVal.frame), maxY + i * rowGap, rightColumn + 10, lblHeight)];
                UILabel *restTimerLblVal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repLblVal.frame), maxY + i * rowGap, rightColumn - 5 + 10, lblHeight)];
                
                exerciseName.textColor = FlatWhiteDark;
                
                exerciseName.text = [NSString stringWithFormat:@" %@", workoutInfo.exerciseName];
                setLblVal.text = [NSString stringWithFormat:@"%d", [workoutInfo.sets intValue]];
                repLblVal.text = [NSString stringWithFormat:@"%@", workoutInfo.repsPerSet];//[workoutInfo.reps intValue]];
                restTimerLblVal.text = [NSString stringWithFormat:@"%@", workoutInfo.restPerSet];//[workoutInfo.restTimer intValue]];
                
                [exerciseName setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [setLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [repLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [restTimerLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                
                setLblVal.textAlignment = NSTextAlignmentLeft;
                repLblVal.textAlignment = NSTextAlignmentLeft;
                restTimerLblVal.textAlignment = NSTextAlignmentLeft;
                
                exerciseName.textColor = [UIColor whiteColor];
                setLblVal.textColor = [UIColor whiteColor];
                repLblVal.textColor = [UIColor whiteColor];
                restTimerLblVal.textColor = [UIColor whiteColor];
                
                if (exInGroupCountTotal > 1) {
                    if ([_routineColor isEqual:FlatOrange])
                        exerciseName.backgroundColor = setLblVal.backgroundColor = repLblVal.backgroundColor = restTimerLblVal.backgroundColor = FlatOrangeDark;
                    else
                        exerciseName.backgroundColor = setLblVal.backgroundColor = repLblVal.backgroundColor = restTimerLblVal.backgroundColor = [Utilities getDarkerColorFor:_routineColor];
                }
                
                dayName.backgroundColor = _routineColor;//[Utilities getDarkerColorFor:_routineColor];
                cell.backgroundColor = _routineColor;
                
                [cell.contentView addSubview:exerciseName];
                [cell.contentView addSubview:setLblVal];
                [cell.contentView addSubview:repLblVal];
                [cell.contentView addSubview:restTimerLblVal];
                exNumberCount++;
                newY = CGRectGetMaxY(restTimerLblVal.frame);
            }
            if (exInGroupCountTotal > 1)
                newY += 5;
        }

    }
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    lineView.tag = 501;
    [cell.contentView addSubview:lineView];

    
    //configure right buttons
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.delegate = self;

    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *exerciseCount = [Utilities getWorkoutsDayExercises:routineName workoutN:[workoutDayName objectAtIndex:indexPath.row]];
    if ([exerciseCount count] == 0)
        return 60;
    else
        return 60 + 22 * ([exerciseCount count]);
                      
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
    selectedDay = [workoutDayName objectAtIndex:indexPath.row];
    [self performSegueWithIdentifier:@"addExerciseToWorkoutDaySegue" sender:self];
}


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    bool delWk = false;
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    NSIndexPath * path = [self.tableView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSLog(@"delete pressed");
        
        NSString *tempRBid = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
        
        // check if it is a downloaded workout and using free version. No edits allowed on those...
        if (![_routineBundleId containsString:tempRBid]) {
            if ([Utilities showPowerRoutinePackage]) {
                [self showPurchasePopUp:@"Deleting workouts from downloaded routine is restricted in free version"];
            } else {
                delWk = true;
            }
        } else {
            delWk = true;
        }
        
        if (delWk) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            
            [alert addButton:@"Yes" actionBlock:^{
                NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
                //TODO: we need to check if user has performed this workout. We have to delete all that or dont allow user to delete this
                NSString *day = [workoutDayName objectAtIndex:path.row];
                NSArray *exerciseInDay = [Utilities getWorkoutsDayExercises:routineName workoutN:day];
                
                [workoutDayName removeObject:day];
                for (UserWorkout *wk in exerciseInDay) {
                    
                    [Utilities markWorkoutAsInActive:wk.routineName workoutName:wk.workoutName];
                    
                    [wk MR_deleteEntityInContext:localContext];
                    [localContext MR_saveToPersistentStoreAndWait];
                }
                [tableView reloadData];
            }];
            [alert showWarning:self title:@"Delete Workout Day?" subTitle:@"Are you sure you want to delete day. All exercise from this workout will be deleted as well?" closeButtonTitle:@"No" duration:0.0f];
        }
        return NO; //Don't autohide to improve delete expansion animation
    }

    return YES;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No workout created yet."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Please click + to create a new Workout.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}



// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addExerciseToWorkoutDaySegue"]) {
//        CreateWorkout *destVC = segue.destinationViewController;
        CreateWorkoutNew *destVC = segue.destinationViewController;
        destVC.routineName = routineName;
        destVC.workoutName = selectedDay;
        destVC.routineBundleId = _routineBundleId;
        NSLog(@"%@ %@ %@", routineName, selectedDay, _routineBundleId);
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ROUTINE PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ROUTINE_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@. Please upgrade to Premium or Power Routine Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

-(void) showCoachMarks {
    
    bool showHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownWorkoutListHint"];
    
    if ([Utilities isHintEnabled] || showHint == false) {
//        float width = self.view.frame.size.width;
        
        //        CGRect coachmark1 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame) + 10, 40, 38);
//        CGRect coachmark2 = CGRectMake(width - 100, 0, 100, 40);
//        CGRect coachmark3 = CGRectMake(0, -30,  0, 0);
        CGRect coachmark4 = CGRectMake(0, CGRectGetHeight(self.view.frame)/2, 0, 0);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                //                                @{
                                //                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                //                                    @"caption": @"Click detail button for workout summary.",
                                //                                    },
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark2],
//                                    @"caption": @"Move group up and down",
//                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
//                                    @"showArrow":[NSNumber numberWithBool:YES]
//                                    },
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark3],
//                                    @"caption": @"\n\n\n\n\n\n\n\n\nClick 'EDIT' to combine exercises into supersets, tri-sets, giant sets or circuits.",
//                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
//                                    @"showArrow":[NSNumber numberWithBool:NO]
//                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark4],
                                    @"caption": @"Slide LEFT on Workout to DELETE.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    }
                                
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownWorkoutListHint"]);
    //    aleadyShowingHint = false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownWorkoutListHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


-(void) getAllData {
    [heightArr removeAllObjects];
    [dataArr removeAllObjects];
    
    NSMutableDictionary *temp;
    NSNumber *height = nil;
    NSArray *orderArray = nil;
    float heightChart = 170;
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    // NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@",routineName];
    NSArray *workout = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    
    
    temp = [DBInterface getRoutineOverallTotalStats:workout];    
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];

    NSMutableDictionary *workoutsList =  [DBInterface getRoutineWorkouts:workout];
    for (id key in workoutsList) {
        NSPredicate *workoutPredicate = [NSPredicate predicateWithFormat:@"workoutName == %@", key];
        NSArray *subWorkouts = [workout filteredArrayUsingPredicate:workoutPredicate];
        temp = [DBInterface getRoutinePerWorkoutsEst:subWorkouts];
        orderArray = temp[AT_ORDER];
        if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
            height = [NSNumber numberWithInt:heightChart];
        } else {
            height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
        }
        [heightArr addObject:height];
        [dataArr addObject:temp];
    }

    temp = [DBInterface getMuscleStatsEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    
    temp = [DBInterface getMajorMuscleDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    
    temp = [DBInterface getMuscleDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getFrontToBackDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getUpperToBottomDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getExerciseStatsEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getExerciseEquipmentDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getExerciseMechanicsDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
        
    temp = [DBInterface getExerciseTypeDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    // this is special
    /*
    for (id key in workoutNames) {
        if ([key isEqualToString:PIE_CHART] || [key isEqualToString:LIST] || [key isEqualToString:AT_ORDER ] || [key isEqualToString:DATA_TYPE] || [key isEqualToString:AT_NAME_KEY] || [key isEqualToString:CHART_COUNT] || [key isEqualToString:LINE_COUNT] || [key isEqualToString:LIST_AND_LINE_CHART])
            continue;
        
        NSLog(@"key is %@", key);
        
        NSPredicate *workoutPredicate = [NSPredicate predicateWithFormat:@"muscle == %@", key];
        NSArray *workoutSets = [userSetsRoutine filteredArrayUsingPredicate:workoutPredicate];
        temp = [DBInterface getPerMuscleStats:workoutSets];
        
        if ([temp[DATA_TYPE] isEqualToString:LIST_AND_LINE_CHART]) {
            height = [NSNumber numberWithInt:lblHeight * [temp[LINE_COUNT] intValue] + heightChart * [temp[CHART_COUNT] intValue]];
        }
        
        if  ([temp[AT_NAME_KEY] isEqualToString:@"None"])
            [temp setObject:key forKey:AT_NAME_KEY];
        
        [heightArr addObject:height];
        [dataArr addObject:temp];
        
    }
     */
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArr count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)myCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[myCollectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    // cell.backgroundColor=[UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:cell.frame andColors:@[FlatGreen, FlatGreenDark]];
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = _routineColor;//[Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-8, 8);
    cell.layer.shadowRadius = 5;
    cell.layer.shadowOpacity = 0.5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[PieChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[LineChartView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    NSMutableDictionary *item = [dataArr objectAtIndex:indexPath.row];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(cell.frame) - 10, 30)];
    title.text = item[AT_NAME_KEY];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor whiteColor];
    [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
    
    int iterate = 0;
    if ([item[DATA_TYPE] isEqualToString:PIE_CHART]) {
        _pieChartView = [[PieChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 170)];
        [self setupPieChart];
        NSLog(@"item list is %@", item);
        [self setPieChartDataValues:item];
        [cell.contentView addSubview:_pieChartView];
    } else if ([item[DATA_TYPE] isEqualToString:LIST]) {
        NSArray *orderDisp = item[AT_ORDER];
        for (NSString *key in orderDisp) {
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            left.text = key;
            right.text = [NSString stringWithFormat:@"%@", item[key]];
            if ([right.text isEqualToString:@"-1"]) {
                [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                right.text = @"";
            } else {
                left.textAlignment = NSTextAlignmentLeft;
                right.textAlignment = NSTextAlignmentRight;
                [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
            }
            left.textColor = [UIColor whiteColor];
            right.textColor = [UIColor whiteColor];
            
            [cell.contentView addSubview:left];
            [cell.contentView addSubview:right];
            iterate++;
        }
    } else {
        float maxY = CGRectGetMaxY(title.frame);
        NSArray *orderDisp = item[AT_ORDER];
        for (NSString *key in orderDisp) {
            if ([key isEqualToString:SETS_PROGRESS]) {
                NSLog(@"ST order is %@",key);
                
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: SETS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatRed forKey:@"Dots"];
                [colorDict setObject:FlatRedDark forKey:@"Cover"];
                [self setLineChartDataValues:item[SETS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else if ([key isEqualToString:REPS_PROGRESS]) {
                NSLog(@"RE order is %@",key);
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: REPS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatYellow forKey:@"Dots"];
                [colorDict setObject:FlatYellowDark forKey:@"Cover"];
                [self setLineChartDataValues:item[REPS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
                
            } else if ([key isEqualToString:VOL_PROGRESS]) {
                NSLog(@"PO order is %@",key);
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY  + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: VOL_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatMint forKey:@"Dots"];
                [colorDict setObject:FlatMintDark forKey:@"Cover"];
                [self setLineChartDataValues:item[VOL_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else {
                NSLog(@"LIST order is %@",key);
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                if ([right.text isEqualToString:@"-1"]) {
                    [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                    right.text = @"";
                } else {
                    left.textAlignment = NSTextAlignmentLeft;
                    right.textAlignment = NSTextAlignmentRight;
                    [left setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                    [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                }
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                iterate++;
                maxY = CGRectGetMaxY(left.frame);
                
            }
        }
    }
    [cell.contentView addSubview:title];
    //[cell.contentView addSubview:subTitle];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame) - 20, [[heightArr objectAtIndex:indexPath.row] floatValue] + 35);
}

-(void) setupLineChartView: (NSString *) name {
    _lineChartView.delegate = self;
    
    _lineChartView.descriptionText = name;
    _lineChartView.noDataText = @"No data.";
    
    _lineChartView.dragEnabled = YES;
    [_lineChartView setScaleEnabled:YES];
    _lineChartView.pinchZoomEnabled = YES;
    _lineChartView.drawGridBackgroundEnabled = NO;
    _lineChartView.backgroundColor = [Utilities getAppColor];
    _lineChartView.layer.cornerRadius = 4;
    
    
    ChartXAxis *xAxis = _lineChartView.xAxis;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    
    
    ChartYAxis *leftAxis = _lineChartView.leftAxis;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.drawZeroLineEnabled = YES;
    leftAxis.enabled = NO;
    
    ChartYAxis *rightAxis = _lineChartView.rightAxis;
    rightAxis.drawAxisLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    
    _lineChartView.legend.enabled = false;
    
    _lineChartView.rightAxis.enabled = NO;
    _lineChartView.xAxis.axisLineColor = FlatWhite;
    _lineChartView.xAxis.labelTextColor = FlatWhite;
    _lineChartView.leftAxis.axisLineColor = FlatWhite;
    _lineChartView.leftAxis.labelTextColor = FlatWhite;
    
    [_lineChartView.viewPortHandler setMaximumScaleY: 2.f];
    [_lineChartView.viewPortHandler setMaximumScaleX: 2.f];
    
    //    ChartMarker *marker = [[ChartMarker alloc] initWithColor:[UIColor colorWithWhite:180/255. alpha:1.0] font:[UIFont systemFontOfSize:12.0] insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    //    marker.minimumSize = CGSizeMake(80.f, 40.f);
    //    _chartView.marker = marker;
    
    //    _chartView.legend.form = ChartLegendFormLine;
    
    [_lineChartView animateWithXAxisDuration:2.5 easingOption:ChartEasingOptionEaseInOutQuart];
    
    
}

-(void) setLineChartDataValues: (NSMutableDictionary *) chartData colorDict:(NSMutableDictionary *) colorDict {
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    NSArray *items = chartData[@"xvals"];
    for (int i = 0; i < [items count]; i++)
    {
        [xVals addObject:[items objectAtIndex:i]];
    }
    
    NSArray *yvalItems = chartData[@"yvals"];
    for (int i = 0; i < [yvalItems count]; i++)
    {
        int value = [[yvalItems objectAtIndex:i] intValue] ;
        [yVals addObject:[[ChartDataEntry alloc] initWithX:i y:value]];
    }
    
    LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals label:@""];
    
    set1.lineDashLengths = @[@5.f, @2.5f];
    set1.highlightLineDashLengths = @[@5.f, @2.5f];
    [set1 setColor:colorDict[@"Dots"]];
    [set1 setCircleColor:colorDict[@"Cover"]];
    set1.lineWidth = 1.5;
    set1.circleRadius = 5.0;
    set1.drawCircleHoleEnabled = NO;
    set1.valueFont = [UIFont systemFontOfSize:9.f];
    set1.valueTextColor = FlatWhite;
    set1.fillAlpha = 65/255.0;
    set1.fillColor = colorDict[@"Dots"];
    set1.fillAlpha = 1.f;
    set1.drawFilledEnabled = YES;
    
    set1.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    
    LineChartData *data = [[LineChartData alloc] initWithDataSet:set1];
    [data setValueFont:[UIFont fontWithName:@HAL_REG_FONT size:8.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _lineChartView.data = data;
    [_lineChartView highlightValues:nil];
    
}


-(void) setupPieChart {
    _pieChartView.usePercentValuesEnabled = YES;
    _pieChartView.holeColor = [UIColor clearColor];
    _pieChartView.noDataText = @"No data.";
    _pieChartView.holeRadiusPercent = 0.40;
    _pieChartView.transparentCircleRadiusPercent = 0.21;
    _pieChartView.descriptionText = @"";
    _pieChartView.drawCenterTextEnabled = YES;
    _pieChartView.drawHoleEnabled = YES;
    _pieChartView.rotationAngle = 0.0;
    _pieChartView.rotationEnabled = YES;
    _pieChartView.legend.enabled = YES;
    _pieChartView.drawSliceTextEnabled = NO;
    
    //        ChartLegend *l = _pieChartView.legend;
    //        l.position = ChartLegendPositionPiechartCenter;
    //        l.xEntrySpace = 7.0;
    //        l.yEntrySpace = 0.0;
    //        l.yOffset = 0.0;
    
    
    _pieChartView.legend.form = ChartLegendFormSquare;
    _pieChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _pieChartView.legend.textColor = UIColor.whiteColor;
    _pieChartView.legend.position = ChartLegendPositionLeftOfChart;
    
    [_pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
    
}


-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    bool colorFlag = false, isMajor = false, isMinor = false;

    if (chartData[AT_NAME_MUSCLE] != nil) {
        colorFlag = true;
        isMinor = true;
    }
    
    if (chartData[AT_NAME_MAJOR_MUSCLE] != nil) {
        colorFlag = true;
        isMajor = true;
    }
    
    for (id key in chartData)
    {        
        if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE] || [key isEqualToString:AT_NAME_MUSCLE] || [key isEqualToString:AT_NAME_MAJOR_MUSCLE]) {
            continue;
        }
        if (colorFlag == true) {
            if (isMinor == true) {
                [colors addObject:[Utilities getExerciseColor:key]];
            }
            else if (isMajor == true) {
                [colors addObject:[Utilities getMajorMuscleColor:key]];
            }
        }
        [xVals addObject:key];
        [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:[chartData[key] floatValue] label:key]];
    }
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:yVals1 label:@""];
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    
    if (colorFlag == false) {
        [colors addObject:FlatRedDark];
        [colors addObject:FlatYellowDark];
        [colors addObject:FlatBlueDark];
        [colors addObject:FlatMagentaDark];
        [colors addObject:FlatGreenDark];
        [colors addObject:FlatPinkDark];
        [colors addObject:FlatOrangeDark];
        [colors addObject:FlatBlackDark];
        [colors addObject:FlatTealDark];
    }
    
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _pieChartView.data = data;
    [_pieChartView highlightValues:nil];
    
}
#pragma mark - pieChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    if ([chartView isKindOfClass:[LineChartView class]]) {
        NSLog(@"Line chart");
    } else if ([chartView isKindOfClass:[PieChartView class]]) {
        NSLog(@"Pie chart");
    }
    
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}
@end
