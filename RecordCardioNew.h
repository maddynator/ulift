//
//  RecordCardioNew.h
//  gyminutes
//
//  Created by Mayank Verma on 6/19/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
@interface RecordCardioNew : UIViewController <UITableViewDelegate, UITableViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UIPickerView *picker;

@end
