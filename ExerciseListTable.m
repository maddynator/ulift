//
//  ExerciseListTable.m
//  uLift
//
//  Created by Mayank Verma on 6/28/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
/*
 THIS FILE IS DEPRECATED. CHECK ExerciseSelectFotWorkout
 */
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************



#import "ExerciseListTable.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>
#import "AddExerciseVC.h"


@interface ExerciseListTable () {
    NSMutableArray * unique;
    NSMutableArray *exerciseGrouped;
    NSMutableSet* _collapsedSections;
    NSArray *exerciseArray, *searchResults, *exercisesPerformed;
}
@property (nonatomic, strong) NSMutableArray* headers;

@end

@implementation ExerciseListTable

- (void)viewDidLoad {
    [super viewDidLoad];

    [self.navigationController.navigationBar setTranslucent:YES];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    _collapsedSections = [NSMutableSet new];

    self.title = _muscleGroup;
    NSLog(@"muscle grop %@", _muscleGroup);

    NSMutableArray *exerciseArrayAll = [[NSMutableArray alloc] init];
    
    exerciseArray = [Utilities getAllExerciseForMuscle:_muscleGroup];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exercisePerformed == true AND muscle == %@", _muscleGroup];
    NSArray *exerciseRecent = [exerciseArray filteredArrayUsingPredicate:predicate];
    

    for( ExerciseList *ex in exerciseRecent) {
        // we are intentially setting context to nil otherwise, the exercises keeps on reloading...
        ExerciseList *newData = [ExerciseList MR_createEntityInContext:nil];
        newData.exerciseName = ex.exerciseName;
        newData.muscle = ex.muscle;
        newData.equipment = @"Recent";
        [exerciseArrayAll addObject:newData];
    }
    
    [exerciseArrayAll addObjectsFromArray:exerciseArray];
    exerciseArray = (NSArray *) exerciseArrayAll;

    [self filterAndStuff];
    [self.tableView reloadData];

    

    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(addExercise)];
    self.navigationController.navigationBar.translucent = YES;
    // Here's where we create our UISearchController
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(self.searchController.searchBar.frame));
    
    [self.searchController.searchBar sizeToFit];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
    self.tableView.tableHeaderView = self.searchController.searchBar;

}

-(void) addExercise {
    
    [self performSegueWithIdentifier:@"addExerciseSegue" sender:self];
}
-(void) viewDidAppear:(BOOL)animated {
    [self filterAndStuff];
    [self.tableView reloadData];

}
-(void)viewWillDisappear:(BOOL)animated {
    [self.searchController removeFromParentViewController];
}


-(void) filterAndStuff {
    unique = nil;
    unique = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseList *data in exerciseArray) {
        NSString *string = data.equipment;
        
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    
    exerciseGrouped = nil;
    exerciseGrouped = [[NSMutableArray alloc] init];
    
    for (NSString *equipment in unique) {
        //NSArray *exercGrp = [Utilities getAllExerciseForMuscleAndEquipment:_muscleGroup equip:equipment];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND equipment == %@", _muscleGroup, equipment];
        NSArray *exerGrp;
        exerGrp =  [exerciseArray filteredArrayUsingPredicate:predicate];
        
        [exerciseGrouped addObject:exerGrp];
    }
    
    self.headers = nil;
    self.headers = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < [unique count] ; i++)
    {
        UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 40)];
        //[header setBackgroundColor:FlatGreen];
        [header setBackgroundColor:[Utilities getExerciseColor:_muscleGroup]];
        
        NSString *headerTxt = [NSString stringWithFormat:@"%@ (%lu)", [unique objectAtIndex:i], (unsigned long)[[exerciseGrouped objectAtIndex:i] count]];
        UILabel *hdrLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(header.frame), CGRectGetMinY(header.frame), CGRectGetWidth(header.frame), CGRectGetHeight(header.frame))];
        hdrLabel.text = headerTxt;
        hdrLabel.textAlignment = NSTextAlignmentCenter;
        
        [header addSubview:hdrLabel];
        [self.headers addObject:header];
    }

}
- (void)openSection:(NSUInteger)sectionIndex animated:(BOOL)animated {
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        // Original table view
        return [unique count];
    }
    else
    {
        // search results table view
        return 1;
    }

}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    NSString *header = [NSString stringWithFormat:@"%@ (%lu)", [unique objectAtIndex:section], (unsigned long)[[exerciseGrouped objectAtIndex:section] count]];
//    return header;//[unique objectAtIndex:section];
//}
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        // Original table view
        return [[exerciseGrouped objectAtIndex:section] count];//[exerciseArray count];
    }
    else
    {
        // search results table view
        return [searchResults count];
    }

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    UILabel *exerciseName = (UILabel *)[cell viewWithTag:100];
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        ExerciseList *exercise = [[exerciseGrouped objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        exerciseName.text = exercise.exerciseName;
    } else {
        ExerciseList *exercise = [searchResults objectAtIndex:indexPath.row];
        exerciseName.text = [NSString stringWithFormat:@"%@ (%@)", exercise.exerciseName, exercise.equipment];
    }

    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        return 40.0f;
    } else {
        return 0.1f;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseList *data;
    
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        data = [[exerciseGrouped objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    } else {
        data = [searchResults objectAtIndex:indexPath.row];
    }
    
    // toast with duration, title, and position
    [self.view makeToast:data.exerciseName
                duration:0.5
                position:CSToastPositionCenter
                   title:@"Exercise Added"];
    
    
//    [Utilities createWorkoutListForExerciseOnDate:data date:_date];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    //if (tableView == self.tableView)
    if (self.searchController.active == false)
    {
        return [self.headers objectAtIndex:section];//[self.headers objectAtIndex:section];
    } else {
        return nil;
    }
}

/*
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND exerciseName contains[c] %@", _muscleGroup, searchText];
    
    searchResults = [ExerciseList MR_findAllSortedBy:@"equipment" ascending:YES withPredicate:predicate inContext:localContext];

}


-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];

    return YES;
}
*/
// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");
    
    NSString *searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchString isEqualToString:@""]) {
        //[searchResults removeAllObjects];
        searchResults = nil;
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND exerciseName contains[c] %@", _muscleGroup, searchString];
        
        searchResults = [ExerciseList MR_findAllSortedBy:@"equipment" ascending:YES withPredicate:predicate inContext:localContext];

    }
    
    [self.tableView reloadData];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addExerciseSegue"]) {
        AddExerciseVC *destVC = segue.destinationViewController;
        //PFObject *data = [muscleArray objectAtIndex:indexSelected];
        destVC.majorMuscle = _muscleGroup;
    }
}


@end
