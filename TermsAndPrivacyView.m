//
//  TermsAndPrivacyView.m
//  uLift
//
//  Created by Mayank Verma on 10/23/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "TermsAndPrivacyView.h"
#import "commons.h"
#import <UIKit/UIWebView.h>

@interface TermsAndPrivacyView ()<UIWebViewDelegate>

@end

@implementation TermsAndPrivacyView
@synthesize webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void) viewDidAppear:(BOOL)animated {
    webView.backgroundColor = FlatOrangeDark;
    // Do any additional setup after loading the view.
    NSURL *url;// = [NSURL URLWithString:@"http://gyminutesapp.com/terms.html"];
    switch (_whichWebPage) {
        case TERMS_WEBVIEW: {
            self.title = @"Terms of Service";
            url = [NSURL URLWithString:@"http://gyminutesapp.com/terms.html"];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"TOSViewed" withParameters:articleParams];

        }
            break;
        case PRIVACY_WEBVIEW: {
            self.title = @"Privacy Policy";
            url = [NSURL URLWithString:@"http://gyminutesapp.com/privacy.html"];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"PPViewed" withParameters:articleParams];

        }
            break;
        case  INSTAGRAM_WEBVIEW:
            {
                self.title =@"Follow us";
                url = [NSURL URLWithString:@"instagram://user?username=gyminutes"];
                if ([[UIApplication sharedApplication] canOpenURL:url]) {
                    NSLog(@"opening the app..");
                    break;
                } else {
                    NSLog(@"opening in webview");
                    url = [NSURL URLWithString:@"http://instagram.com/gyminutes/"];
                }
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"instagramFollow" withParameters:articleParams];

            }
            break;
        case FAQS_WEBVIEW: {
            self.title = @"FAQs";
            url = [NSURL URLWithString:@"http://gyminutesapp.com/gyminutesFAQs.html"];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"faqViewed" withParameters:articleParams];
        }
            break;
        case TUTORIALS_WEBVIEW: {
            self.title = @"Tutorials";
            url = [NSURL URLWithString:@"http://gyminutesapp.com/tutorials.html"];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"tutorialViewed" withParameters:articleParams];

        }
            break;
        default:
            break;
    }
    NSLog(@"loading url %@", url);
    self.webView.delegate = self;
    
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webView setScalesPageToFit:YES];
    [self.webView loadRequest:request];
    NSLog(@"hsould oload bynow");
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    NSLog(@"page is loading");
    [KVNProgress show];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView {
    NSLog(@"finished loading");
        [KVNProgress dismiss];
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    NSLog(@"Error : %@",error);
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
