//
//  CreateRoutineVC.m
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "CreateRoutineVC.h"
#import "CreateRoutineForm.h"

@interface CreateRoutineVC () {

}

@end
@implementation CreateRoutineVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.definesPresentationContext = YES;
    self.title = @"Create Routine";
    CreateRoutineForm *form = [[CreateRoutineForm alloc] init];
    
    if (_routine != nil) {
        form.name = _routine.routineName;
        form.summary = _routine.routineSummary;
        form.desc = _routine.routineDescription;
        form.goal = _routine.routingGoal;
        if ([_routine.routineDuration intValue] != 0)
            form.duration = _routine.routineDuration;
        
        form.color = [self convertColorStringToNum:_routine.routineColor];
        form.type = [self convertTypeStringToEnum:_routine.routineType];
        form.level = [self convertLevelStringToEnum: _routine.routineLevel];
        form.instructions = _routine.routineInstructions;
        form.faqs = _routine.routineFaqs;
        form.discalimer = _routine.routineDisclosure;
    }
    self.formController.form = form;
    UIBarButtonItem *saveRoutine = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemSave target:self action:@selector(barBtnSave)];
    self.navigationItem.rightBarButtonItem = saveRoutine;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(Type) convertTypeStringToEnum: (NSString *) type {
    /*
    Strength = 0,
    Bodybuilding,
    Sports
     */
    if ([type isEqualToString:@"Strength/PowerLifting"])
        return Strength;
    if ([type isEqualToString:@"Bodybuilding"])
        return Bodybuilding;
    if ([type isEqualToString:@"Sports"])
        return Sports;

    return Bodybuilding;
}

-(Level) convertLevelStringToEnum: (NSString *) level {
    /*
    Beginner = 0,
    Intermediate,
    Advance,
    Monster
     @"Beginners(0-1 Year)", @"Intermediate (1-3 Year)", @"Advance (3-5 Years)", @"Monster (5+ years)"]
     */
    
    if ([level isEqualToString:@"Beginners(0-1 Year)"])
        return Beginner;
    if ([level isEqualToString:@"Intermediate (1-3 Year)"])
        return Intermediate;
    if ([level isEqualToString:@"Advance (3-5 Years)"])
        return Advance;
    if ([level isEqualToString:@"Monster (5+ years)"])
        return Monster;
    
    return Beginner;
}
-(Color ) convertColorStringToNum: (NSString*) colorStr {
    /*
     Red = 0,
     Blue,
     Orange,
     Pink,
     Magenta,
     Brown,
     Coffee,
     Maroon,
     Mint
     */
    if ([colorStr isEqualToString:@"Red"])
        return Red;
    if ([colorStr isEqualToString:@"Blue"])
        return Blue;
//    if ([colorStr isEqualToString:@"Orange"])
//        return Orange;
    if ([colorStr isEqualToString:@"Pink"])
        return Pink;
    if ([colorStr isEqualToString:@"Magenta"])
        return Magenta;
    if ([colorStr isEqualToString:@"Brown"])
        return Brown;
    if ([colorStr isEqualToString:@"Coffee"])
        return Coffee;
    if ([colorStr isEqualToString:@"Maroon"])
        return Maroon;
    if ([colorStr isEqualToString:@"Mint"])
        return Mint;
    
    return Red;

}
-(NSString *) convertTypeEnumToString: (Type) type {
    /*
     Strength = 0,
     Bodybuilding,
     Sports
     */
    if (type == 0)
        return @"Strength/PowerLifting";
    if (type  == 1)
        return @"Bodybuilding";
    if (type == 2)
         return @"Sports";
    if (type == 3)
        return @"Fullbody";
    
    return @"Bodybuilding";
}

-(NSString *) convertLevelEnumToString: (Level) level {
    /*
     Beginner = 0,
     Intermediate,
     Advance,
     Monster
     @"Beginners(0-1 Year)", @"Intermediate (1-3 Year)", @"Advance (3-5 Years)", @"Monster (5+ years)"]
     */
    
    if (level == 0) return @"Beginners(0-1 Year)";
    if (level == 1) return @"Intermediate (1-3 Year)";
    if (level == 2) return @"Advance (3-5 Years)";
    if (level == 3) return @"Monster (5+ years)";
    
    return @"Beginners(0-1 Year)";
}
-(NSString *) convertColorEnumToString: (Color) color {
    /*
     Red = 0,
     Blue,
     Orange,
     Pink,
     Magenta,
     Brown,
     Coffee,
     Maroon,
     Mint
     */
    NSLog(@"color value is %ld", (long)color);
    if (color == 0) return @"Red";
    if (color == 1) return @"Blue";
    if (color == 2) return @"Pink";
    if (color == 3) return @"Magenta";
    if (color == 4) return @"Brown";
    if (color == 5) return @"Coffee";
    if (color == 6) return @"Maroon";
    if (color == 7) return @"Mint";
    
    return @"Red";
}

- (IBAction)barBtnSave {

    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    CreateRoutineForm *form = self.formController.form;
    [self.view endEditing:YES];
    
    if ([form.name length] == 0) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Missing" subTitle:@"Please enter routine name" closeButtonTitle:@"Ok" duration:0.0f];
        return;
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", form.name];
    NSArray *tempRoutines = [RoutineMetaInfo MR_findAllWithPredicate:predicate inContext:localContext];
    
    if ([tempRoutines count] > 0 && _routine == nil) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Duplicate" subTitle:@"Please enter a unique routine name" closeButtonTitle:@"Ok" duration:0.0f];
        return;
    }
    
    RoutineMetaInfo *newRoutine;
    NSString *bundleId = @"None";
    int newFlag = false;
    if (_routine == nil) {
        newRoutine = [RoutineMetaInfo MR_createEntityInContext:localContext];
        newFlag = true;
    } else {
        newRoutine = _routine;
        if (![form.name isEqualToString:newRoutine.routineName]) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showError:self title:@"Error" subTitle:@"Routine name cannot be changed." closeButtonTitle:@"Ok" duration:0.0f];
            return;
        }
    }
    newRoutine.routineName = form.name;
    newRoutine.routineSummary = ([form.summary length] == 0) ? @"": form.summary;
    newRoutine.routineDescription = ([form.desc length]  == 0) ? @"": form.desc;
    newRoutine.routingGoal = ([form.goal  length]  == 0) ? @"": form.goal;
    newRoutine.routineDuration = (form.duration == nil) ? [NSNumber numberWithInt:90]: form.duration;
    newRoutine.routineColor  = [self convertColorEnumToString:form.color];
    newRoutine.routineType = [self convertTypeEnumToString:form.type];
    newRoutine.routineLevel = [self convertLevelEnumToString:form.level];
    newRoutine.routineCreatedDate = [Utilities getCurrentDate];
    newRoutine.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
    newRoutine.routineUserCreated = [NSNumber numberWithBool:true];
    newRoutine.routineInstructions =  ([ form.instructions length]  == 0) ? @"": form.instructions;
    newRoutine.routineFaqs = ([form.faqs  length]  == 0) ? @"": form.faqs;
    newRoutine.routineDisclosure = ([form.discalimer length]  == 0) ? @"": form.discalimer;
    newRoutine.routineAuthor = @"self";
    if (newFlag) {
        bundleId = [NSString stringWithFormat:@"com.gyminutes.%@.%@", [PFUser currentUser].objectId, form.name];
        bundleId = [bundleId stringByReplacingOccurrencesOfString:@" " withString:@""];
        newRoutine.routineBundleId = bundleId;
    } else {
        NSLog(@"existing workout.. so we cant modify the routine bundle id Disclouure is %@, %@", newRoutine.routineDisclosure, form.discalimer);
    }

    [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
        if (!error) {
            NSLog(@"routine saved...");
        }
    }];
    [Utilities syncRoutineInfo:self];
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    if (_routine == nil) {
        [alert showSuccess:self title:@"Success" subTitle:@"Routine created Successfully." closeButtonTitle:@"Ok" duration:0.0f];
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RtCreate", [NSString stringWithFormat:@"%@",form.name],
                                       nil];
        [Flurry logEvent:@"NewRoutineCreated" withParameters:articleParams];
    }
    else {
        [alert showSuccess:self title:@"Success" subTitle:@"Routine updated Successfully." closeButtonTitle:@"Ok" duration:0.0f];
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RtUpdate", [NSString stringWithFormat:@"%@",form.name],
                                       nil];
        [Flurry logEvent:@"RoutineUpdated" withParameters:articleParams];

    }
}

//- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    return 0.000000000000000001f;
//}
@end