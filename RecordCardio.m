//
//  RecordCardio.m
//  gyminutes
//
//  Created by Mayank Verma on 6/22/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "RecordCardio.h"
#import "CardioStats+CoreDataClass.h"

@interface RecordCardio () {
    NSArray *cardioFields, *cardioFieldType;
    NSMutableArray *cardioPlaceholders;
    NSMutableArray *_pickerData;
    int selectedIndex, selectedRow;
    UIToolbar* addOnKeyboard;

}

@end

@implementation RecordCardio
@synthesize date;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    
    self.title = @"Cardio";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    NSString *dateField = [NSString stringWithFormat:@"Start Time (%@)", date];
    NSString *distanceFields = [NSString stringWithFormat:@"Distance (Miles)"];
    if ([Utilities isKgs]) {
        distanceFields = [NSString stringWithFormat:@"Distance (Kms)"];
    }

    cardioFields = @[@"Cardio Type", @"Training Type",  @"Duration (Mins)", distanceFields, @"Empty Stomach", @"Calories Burned", @"Elevation", dateField];

    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *cardioPredicate = [NSPredicate predicateWithFormat:@"date == %@", [formatter dateFromString:date]];
    CardioStats *existing = [CardioStats MR_findFirstWithPredicate:cardioPredicate inContext:localContext];
    
    if (existing == nil)
        cardioPlaceholders = [[NSMutableArray alloc] initWithArray:@[@"RUNNING", @"HIGH INTENSITY", @"25", @"1", @"YES", @"350", @"0", [dateFormatter stringFromDate:[NSDate date]]]];
    else
        cardioPlaceholders = [[NSMutableArray alloc] initWithArray:@[existing.cardioType, existing.trainingType, [NSString stringWithFormat:@"%@", existing.duration], [NSString stringWithFormat:@"%@", existing.distance], ([existing.emptyStomach intValue] == 0) ? @"YES" : @"NO" , [NSString stringWithFormat:@"%@", existing.caloriesBurned], [NSString stringWithFormat:@"%@", existing.elevation], [dateFormatter stringFromDate:existing.startTime]]];

    
    // textfield = 0, spinner = 1, button = 2
    cardioFieldType = @[@1, @1, @0, @0, @1, @0, @0, @1];
    
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [_collectionView setBackgroundColor:FlatWhite];
    
    [self.view addSubview:_collectionView];
    
    
    _pickerData = [[NSMutableArray alloc] init];

    // Do any additional setup after loading the view, typically from a nib.
    addOnKeyboard = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 35)];
    
    addOnKeyboard.items = [NSArray arrayWithObjects:
                           //  [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"DONE" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           //                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           nil];
    
    
    addOnKeyboard.tintColor = [Utilities getAppColor];

}

-(void) updateData {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    timeFormatter.dateFormat = @"HH:mm:ss";
    [timeFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *cardioPredicate = [NSPredicate predicateWithFormat:@"date == %@", [formatter dateFromString:date]];
    CardioStats *item = [CardioStats MR_findFirstWithPredicate:cardioPredicate inContext:localContext];
    
    
    //cardioFields = @[@"Cardio Type", @"Training Type",  @"Duration (Mins)", distanceFields, @"Empty Stomach", @"Calories Burned", @"Elevation", dateField];
    
    if (item == nil) {
        NSLog(@"creating new...");
        item = [CardioStats MR_createEntityInContext:localContext];
    } else {
        NSLog(@"updating existing");
    }
    
    for (int i = 0; i < [cardioPlaceholders count]; i++) {
        switch (i) {
            case 0:
            {
                item.cardioType = [cardioPlaceholders objectAtIndex:i];
            }
                break;
            case 1:
            {
                item.trainingType = [cardioPlaceholders objectAtIndex:i];
            }
                break;
            case 2:
            {
                item.duration = [NSNumber numberWithFloat:[[cardioPlaceholders objectAtIndex:i] floatValue]];
            }
                break;
            case 3:
            {
                item.distance = [NSNumber numberWithFloat:[[cardioPlaceholders objectAtIndex:i] floatValue]];
            }
                break;
            case 4:
            {
                item.emptyStomach = [NSNumber numberWithFloat:[[cardioPlaceholders objectAtIndex:i] floatValue]];
            }
                break;
            case 5:
            {
                item.caloriesBurned = [NSNumber numberWithFloat:[[cardioPlaceholders objectAtIndex:i] floatValue]];
                
            }
                break;
            case 6:
            {
                item.elevation = [NSNumber numberWithFloat:[[cardioPlaceholders objectAtIndex:i] floatValue]];
                
            }
                break;
            case 7:
            {
                item.startTime = [timeFormatter dateFromString:[cardioPlaceholders objectAtIndex:i]];
                
            }
                break;
                
            default:
                break;
        }
    }
    item.syncedState = [NSNumber numberWithBool:false];
    NSLog(@"item is %@", item);
    item.date = [formatter dateFromString:date];
    [localContext MR_saveToPersistentStoreAndWait];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [cardioFields count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UITextView class]]) {
            [lbl removeFromSuperview];
        }
    }

    
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, cell.frame.size.width, 20)];
    header.text = [cardioFields objectAtIndex:indexPath.row];
    [header setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    header.backgroundColor = [UIColor whiteColor];
    header.textColor = FlatGrayDark;
    if (indexPath.row %2)
        cell.backgroundColor = FlatRed;
    else
        cell.backgroundColor = FlatMint;
    
    switch ([[cardioFieldType objectAtIndex:indexPath.row] intValue]) {
        case 0: {
            UITextView *textField = [[UITextView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(header.frame), cell.frame.size.width, CGRectGetHeight(cell.frame) - 20)];
            textField.keyboardType = UIKeyboardTypeDecimalPad;
            textField.delegate = self;
            textField.tag = indexPath.row;
            textField.backgroundColor = [UIColor whiteColor];
            [textField setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
            textField.textAlignment = NSTextAlignmentCenter;
            textField.text = [cardioPlaceholders objectAtIndex:indexPath.row];
            textField.inputAccessoryView = addOnKeyboard;
            [cell.contentView addSubview:textField];
        }
            break;
        case 1: {
            UILabel *labelField = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(header.frame), cell.frame.size.width, CGRectGetHeight(cell.frame) - 20)];
            labelField.tag = indexPath.row;
            labelField.textAlignment = NSTextAlignmentCenter;
            labelField.backgroundColor = [UIColor whiteColor];
            labelField.text = [cardioPlaceholders objectAtIndex:indexPath.row];
            [labelField setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
            [cell.contentView addSubview:labelField];
            
        }
            break;
        case 2: {
            
        }
            break;
            
    }
    
    
    [cell.contentView addSubview:header];

    return cell;
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([[cardioFieldType objectAtIndex:indexPath.row] intValue] == 1) {
        [self.view endEditing:YES];
        [self pickerClicked:(int)indexPath.row];
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView
                  layout:(UICollectionViewLayout *)collectionViewLayout
  sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    float cellWidth = screenWidth / 2.0; //Replace the divisor with the column count requirement. Make sure to have it in float.
    CGSize size = CGSizeMake(cellWidth -5, 60);
    
    return size;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
        return 2;
}

//- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
//{
//        return UIEdgeInsetsMake(5, 2, 0, 2);
//}

-(void)pickerClicked:(int) tag {
    [_pickerData removeAllObjects];
    selectedIndex = tag;
    NSLog(@"selected index is %d", selectedIndex);
    switch (tag) {
        case 0: {
            [_pickerData addObject:@"RUNNING"];
            [_pickerData addObject:@"CYCLING"];
            [_pickerData addObject:@"SWIMMING"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"RUNNING"])
                selectedRow = 0;
            else if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"CYCLING"])
                selectedRow = 1;
            else
                selectedRow = 2;
            
        }
            break;
        case 1: {
            [_pickerData addObject:@"HIGH INTENSITY"];
            [_pickerData addObject:@"STEADY STATE"];
            [_pickerData addObject:@"LOW INTENSITY"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"HIGH INTENSITY"])
                selectedRow = 0;
            else if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"STEADY STATE"])
                selectedRow = 1;
            else
                selectedRow = 2;
        }
            break;
        case 2: {
            for (int i = 10; i <= 90; i++) {
                NSString *time = [NSString stringWithFormat:@"%d Mins", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i-10;
            }
            
        }
            break;
        case 3: {
            for (float i = 1; i <= 1000; i++) {
                NSString *time = [NSString stringWithFormat:@"%.2f", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i - 50;
            }
        }
            break;
        case 4: {
            
            [_pickerData addObject:@"YES"];
            [_pickerData addObject:@"NO"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"YES"])
                selectedRow = 0;
            else
                selectedRow = 1;
        }
            break;
        case 5: {
            for (int i = 50; i <= 1000; i++) {
                NSString *time = [NSString stringWithFormat:@"%d", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i - 50;
            }
            
        }
            break;
        case 6: {
            for (float i = 0 ; i <= 15; i +=.5) {
                NSString *time = [NSString stringWithFormat:@"%.1f", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i*2;
            }
            
        }
            break;
        case 7: {
        }
            
        default:
            break;
    }
    
    [self showPicker: [cardioFields objectAtIndex:tag]];
    [self.picker selectRow:selectedRow inComponent:0 animated:YES];
}


#pragma UIPickerView - Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [_pickerData count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_pickerData objectAtIndex:row];
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"row selected is %@", _pickerData[row]);
    [cardioPlaceholders replaceObjectAtIndex:selectedIndex withObject:_pickerData[row]];
    [self.popupController dismissPopupControllerAnimated:YES];
    [self updateData];
    [self.collectionView reloadData];
}

- (void)showPicker: (NSString *) titleButton {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:titleButton attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    // Connect data
    self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 250, 200)];
    self.picker.dataSource = self;
    self.picker.delegate = self;
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, self.picker]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    //self.popupController.theme.popupContentInsets = UIEdgeInsetsZero;
    
    [self.popupController presentPopupControllerAnimated:YES];
}

#pragma textView delegate
-(void)textViewDidEndEditing:(UITextView *)textView {
    NSLog(@"Editng done.. %@", textView.text);
    [cardioPlaceholders replaceObjectAtIndex:textView.tag withObject:textView.text];
    [self updateData];
}
#pragma mark barbellcalculator
-(void)doneWithNumberPad{
    [self updateData];
    [self.view endEditing:YES];
}



@end
