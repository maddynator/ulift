//
//  TodayWorkoutItems.m
//  gyminutes
//
//  Created by Mayank Verma on 5/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "TodayWorkoutItems.h"
#import "commons.h"

@implementation TodayWorkoutItems

+(NSMutableDictionary *) getDailyStats:(NSString *)date {
    
    NSMutableArray *order = [[NSMutableArray alloc] init];
    [order addObject:@"Weight"];
    [order addObject:@"Body Fat (%)"];
    [order addObject:@"BMI"];

    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:@"Body Stats" forKey:AT_NAME_KEY];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *startDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[formatter dateFromString:date]];
    [startDayComponents setHour:00];
    [startDayComponents setMinute:00];
    [startDayComponents setSecond:00];
    
    NSDateComponents *endDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[formatter dateFromString:date]];
    [endDayComponents setHour:23];
    [endDayComponents setMinute:59];
    [endDayComponents setSecond:59];
    
    NSDate *startDate = [gregorian dateFromComponents:startDayComponents];
    NSDate *endDate = [gregorian dateFromComponents:endDayComponents];

    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startDate, endDate];
    DailyStats *todayItem = [DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];
    //todayItem = [allItems objectAtIndex:0];

    if (todayItem == nil) {
        [cal_data setObject:@"N/A" forKey:@"Weight"];
        [cal_data setObject:@"N/A" forKey:@"Body Fat (%)"];
        [cal_data setObject:@"N/A" forKey:@"BMI"];
    } else {
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", todayItem.weight, [[Utilities getUnits] lowercaseString]] forKey:@"Weight"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %%", todayItem.bodyFat] forKey:@"Body Fat (%)"];
        [cal_data setObject:todayItem.bodyMassIndex forKey:@"BMI"];
    }
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}
+(NSMutableDictionary *) getStartWorkout: (NSArray *) data {
    NSMutableArray *order = [[NSMutableArray alloc] init];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    //[order addObject:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:@"Workout Info" forKey:AT_NAME_KEY];
    [cal_data setObject:@"segue" forKey:@"Segue"];
    
    if ([data count] == 0) {
        // do nothing
    } else {
        [cal_data setObject:@"Sets [Reps]" forKey:@"Exercise"];
        [order addObject:@"Exercise"];
        for (WorkoutList *workout in data) {
            int mulExCount = 1;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", workout.exerciseGroup];
            NSArray *multipleEx = [data filteredArrayUsingPredicate:predicate];
            if ([multipleEx count] > 1)
                mulExCount = (int)[multipleEx count];
            
            [cal_data setObject:[NSString stringWithFormat:@"%@ [%@]", workout.setsSuggested, workout.repsPerSet] forKey:workout.exerciseName];
            [cal_data setObject:[NSNumber numberWithInt:mulExCount] forKey:[NSString stringWithFormat:@"%@=MULTISET", workout.exerciseName]];
            [cal_data setObject:workout.exerciseNumInGroup forKey:[NSString stringWithFormat:@"%@=MULTISET-COUNT", workout.exerciseName]];
            
            [order addObject:workout.exerciseName];
        }
    }
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}


+(NSMutableDictionary *) getPerExerciseInformation:(WorkoutList *) data workoutList:(NSArray *) workout {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    NSMutableArray *order = [[NSMutableArray alloc] initWithArray:@[@"Type", @"Personal Record", @"Sets", @"Status", @"Goal"]];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    [cal_data setObject:data.exerciseName forKey:AT_NAME_KEY];
    
    NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", data.exerciseGroup];
    NSArray *exInGroup = [workout filteredArrayUsingPredicate:groupPredicate];
    
    switch ([exInGroup count]) {
        case 0:
            [cal_data setObject:@"Single" forKey:@"Type"];
            break;
        case 1:
            [cal_data setObject:@"Single" forKey:@"Type"];
            break;
        case 2:
            [cal_data setObject:@"Super Set" forKey:@"Type"];
            break;
        case 3:
            [cal_data setObject:@"Tri Set" forKey:@"Type"];
            break;
        case 4:
            [cal_data setObject:@"Giant Set" forKey:@"Type"];
            break;
        default:
            break;
    }
    if ([exInGroup count] == [workout count])
        [cal_data setObject:@"Circuit" forKey:@"Type"];
    
    NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", data.exerciseName];
    ExerciseMetaInfo *metaInfoArr = [ExerciseMetaInfo MR_findFirstWithPredicate:exPredicate inContext:localContext];
    if (metaInfoArr == nil) {
        [cal_data setObject:@"Not Set" forKey:@"Personal Record"];
    } else {
        [cal_data setObject:[NSString stringWithFormat:@"%@ x %@ %@ (%@)", metaInfoArr.maxRep, metaInfoArr.maxWeight, [[Utilities getUnits] lowercaseString], (metaInfoArr.maxDate == nil) ? @"N/A": metaInfoArr.maxDate] forKey:@"Personal Record"];
    }
    NSPredicate *setsPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@ AND date == %@", data.exerciseName, data.date];
    NSArray *exerciseSets = [ExerciseSet MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:setsPredicate inContext:localContext];
    if ([exerciseSets count] == 0) {
        [cal_data setObject:[NSString stringWithFormat:@"0/%@", data.setsSuggested] forKey:@"Sets"];
        [cal_data setObject:@"Not Performed" forKey:@"Status"];
    } else {
        if ([exerciseSets count] < [data.setsSuggested intValue])
            [cal_data setObject:@"In Progress" forKey:@"Status"];
        else
            [cal_data setObject:@"Completed" forKey:@"Status"];
        
        [cal_data setObject:[NSString stringWithFormat:@"%lu/%@", [exerciseSets count], data.setsSuggested] forKey:@"Sets"];
    }
    
    NSPredicate *isTemporary = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@ AND exerciseName == %@", data.routiineName, data.workoutName, data.exerciseName];
    UserWorkout *exercisePresent = [UserWorkout MR_findFirstWithPredicate:isTemporary inContext:localContext];
    if (exercisePresent == nil) {
        [order addObject:@"Temporary"];
        [cal_data setObject:@"Yes" forKey:@"Temporary"];
    }
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"MM/dd/yyyy"];
    
    if (metaInfoArr.goalWeightOrRep == nil)
        [cal_data setObject:@"Set Weight Goal" forKey:@"Goal"];
    else {
        if ([metaInfoArr.goalWeightOrRep floatValue] == 0)
            [cal_data setObject:@"Set Weight Goal" forKey:@"Goal"];
        else
            [cal_data setObject:[NSString stringWithFormat:@"%@x%.1f %@ (%@)", @1, [metaInfoArr.goalWeightOrRep floatValue], [Utilities getUnits], (metaInfoArr.goalAchieveDate == nil) ? @"Set Date":[dateFormat stringFromDate:metaInfoArr.goalAchieveDate]] forKey:@"Goal"];
    }
    
    return cal_data;
}

+(NSMutableDictionary *) getWorkoutSummary: (NSArray *) data {
    NSArray *order = @[@"Name", @"Reps", @"Sets", @"Volume", @"Duration"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:BOX forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    
    if ([data count] == 0) {
        [cal_data setObject:@"Workout Summary" forKey:AT_NAME_KEY];
        [cal_data setObject:@"Name" forKey:@"None"];
        [cal_data setObject:@"N/A" forKey:@"Date"];
        [cal_data setObject:@"0 sec" forKey:@"Duration"];
        [cal_data setObject:@"0" forKey:@"Reps"];
        [cal_data setObject:@"0" forKey:@"Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@",[[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
        
    } else {
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm:ss";
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

        ExerciseSet *end =[data objectAtIndex:0];
        ExerciseSet *start =[data objectAtIndex:[data count] - 1];
        NSDate *endTime = [dateFormatter dateFromString:end.timeStamp];
        NSDate *startTime = [dateFormatter dateFromString:start.timeStamp];
        NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
        float totalReps = 0, totalSets = 0, totalWeight = 0;
        
        totalSets = [data count];
        
        for (ExerciseSet *set in data) {
            totalReps += [set.rep intValue];
            totalWeight += [set.rep intValue] * [set.weight floatValue];
        }
        
        
        int seconds = (int) diff % 60;
        int minutes = ((int) diff / 60) % 60;
        int hours = (int) diff / 3600;
        NSString *time = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
        
        NSLog(@"==>%d %d %d %d %@",(int) diff ,hours, minutes, seconds, time);
        
        [cal_data setObject:@"Workout Summary" forKey:AT_NAME_KEY];
        [cal_data setObject:start.workoutName forKey:@"Name"];
        [cal_data setObject:start.date forKey:@"Date"];
        [cal_data setObject:[NSString stringWithFormat:@"%@", time] forKey:@"Duration"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
        [cal_data setObject:[NSNumber numberWithFloat:totalSets] forKey:@"Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@",[numberformatter stringFromNumber:[NSNumber  numberWithFloat:totalWeight]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    }
    return cal_data;

    
}
+(NSMutableDictionary *) getPersonalRecords: (NSString *) date {
    NSMutableArray *order = [[NSMutableArray alloc] init];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:IMAGE_TYPE forKey:DATA_TYPE];
    [cal_data setObject:@"Personal Records" forKey:AT_NAME_KEY];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *prPredicate = [NSPredicate predicateWithFormat:@"maxDate == %@", date];
    NSArray *exMetaInfoForPR = [ExerciseMetaInfo MR_findAllWithPredicate:prPredicate inContext:localContext];
    if ([exMetaInfoForPR count] == 0) {
        [cal_data setObject:@"None" forKey:@"New Records"];
        [order addObject:@"New Records"];
    } else {
        for (ExerciseMetaInfo *extemp in exMetaInfoForPR) {
            if ([extemp.maxWeight floatValue] > 0) {
                [cal_data setObject:[NSString stringWithFormat:@"%@ x %@ %@", extemp.maxRep, extemp.maxWeight, [[Utilities getUnits] lowercaseString]] forKey:extemp.exerciseName];
                [order addObject:extemp.exerciseName];
            }
        }        
    }
    [cal_data setObject:order forKey:AT_ORDER];
    NSLog(@"Peronal Record Date is %@ %@", date, cal_data);

    return cal_data;

}
+(NSMutableDictionary *) getWorkoutTimeLine:(NSArray *) data {
    NSMutableArray *order = [[NSMutableArray alloc] init];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:order forKey:AT_ORDER];
    [cal_data setObject:BAR_CHART forKey:DATA_TYPE];
    [cal_data setObject:@"Workout Timeline" forKey:AT_NAME_KEY];
    if ([data count] == 0) {
//        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"xVals"];
//        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"yVals"];
        NSLog(@"count is 0");
    } else {
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"h:mm a";
        
        NSDateFormatter *dateFormatterPre = [[NSDateFormatter alloc] init];
        dateFormatterPre.dateFormat = @"HH:mm:ss";
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        NSArray *reversedSets = [[data reverseObjectEnumerator] allObjects];
        
        ExerciseSet *firstSet = [reversedSets objectAtIndex:0];
        ExerciseSet *lastSet = [reversedSets objectAtIndex:[reversedSets count] - 1];
        NSDate *startTime = [dateFormatterPre dateFromString:firstSet.timeStamp];
        NSDate *lastTime = [dateFormatterPre dateFromString:lastSet.timeStamp];
        
        NSLog(@"start time %@ end Time %@", firstSet.timeStamp, lastSet.timeStamp);
        NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *startOffsetComponents = [[NSDateComponents alloc] init];
        NSDateComponents *lastOffsetComponents = [[NSDateComponents alloc] init];
        [startOffsetComponents setMinute:-5]; // note that I'm setting it to -1
        [lastOffsetComponents setMinute:5]; // note that I'm setting it to -1
        
        NSDate *startFinalTime = [gregorian dateByAddingComponents:startOffsetComponents toDate:startTime options:0];
        NSDate *lastFinalTime = [gregorian dateByAddingComponents:lastOffsetComponents toDate:lastTime options:0];
        
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yVals = [[NSMutableArray alloc] init];
        [yVals addObject:[NSNumber numberWithInt:0]];
        [xVals addObject:[dateFormatter stringFromDate:startFinalTime]];
    
        for (ExerciseSet *set in reversedSets) {
//            NSLog(@"Time is %@",[dateFormatter stringFromDate:[dateFormatterPre dateFromString:set.timeStamp]]);
            [xVals addObject:[dateFormatter stringFromDate:[dateFormatterPre dateFromString:set.timeStamp]]];
            [yVals addObject:[NSNumber numberWithFloat:[set.rep intValue] * [set.weight floatValue]] ];
        }
        [xVals addObject:[dateFormatter stringFromDate:lastFinalTime]];
        [yVals addObject:[NSNumber numberWithInt:0]];
        [cal_data setObject:xVals forKey:@"xvals"];
        [cal_data setObject:yVals forKey:@"yvals"];
    }
    return cal_data;
}
+(NSMutableDictionary *) getWorkoutStats:(NSArray *) userSets {
   
    NSArray *order = @[@"Name", @"SETS", @"Total Sets", @"Avg. Sets", SETS_PROGRESS, @"REPS", @"Total Reps", @"Avg. Reps", REPS_PROGRESS, @"VOLUME", @"Total Volume", @"Avg. Volume", VOL_PROGRESS];
    NSString *units = [Utilities getUnits];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST_AND_LINE_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:@"None" forKey:@"Name"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:@"0"  forKey:@"Total Sets"];
        [cal_data setObject:@"0"  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:13] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    } else {
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        NSMutableArray *uniqDates = [[NSMutableArray alloc] init];
        NSMutableDictionary *uniqSubWorkouts = [[NSMutableDictionary alloc] init];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[userSets count];
        for (ExerciseSet *set in userSets) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            if ([uniqSubWorkouts objectForKey:set.date] == nil) {
                [uniqSubWorkouts setObject:[NSNumber numberWithInt:1] forKey:set.date];
                [uniqDates addObject:set.date];
            }
        }
        
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        NSArray *reversedDates =[[uniqDates reverseObjectEnumerator] allObjects];
        
        for (NSString *key in reversedDates) {
            //NSLog(@"dates are %@", key);
            NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", key];
            NSArray *subItems = [userSets filteredArrayUsingPredicate:datePredicate];
            float totalRp = 0, totalSt = (int)[subItems count], totalWt = 0;
            for (ExerciseSet *set in subItems) {
                totalRp += [set.rep intValue];
                totalWt += [set.rep intValue] * [set.weight floatValue];
            }
            [xVals addObject:key];
            [yValsReps addObject:[NSNumber numberWithInt:totalRp]];
            [yValsSets addObject:[NSNumber numberWithInt:totalSt]];
            [yValsVol addObject:[NSNumber numberWithFloat:totalWt]];
        }
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        float totalWorkouts = (float)[[uniqSubWorkouts allKeys] count];
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        ExerciseSet *lastSet = [userSets objectAtIndex:0];
        
        [cal_data setObject:firstSet.workoutName forKey:AT_NAME_KEY];
        [cal_data setObject:lastSet.workoutName forKey:@"Name"];
        
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets]   forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalSets/totalWorkouts]  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalWorkouts]  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber  numberWithFloat:totalWt]], [units lowercaseString]]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber  numberWithFloat:totalWt/totalWorkouts]], [units lowercaseString]]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:11] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    }
    return cal_data;

}
+(NSMutableDictionary *) getMuscleStats:(NSArray *) data  {
    
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        if (set.muscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.muscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.muscle];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Muscle Distribution" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    return cal_data;
}


+(NSMutableDictionary *) getExerciseStats:(NSArray *) userSets {
    NSArray *order = @[@"REPS", @"Total Reps", @"Avg. Reps/Set", REPS_PROGRESS, @"VOLUME", @"Total Volume", @"Avg. Volume/Set", VOL_PROGRESS];
    NSString *units = [[Utilities getUnits] lowercaseString];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST_AND_LINE_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
                
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:@"None" forKey:@"Name"];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps/Set"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume/Set"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:4] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:2] forKey:CHART_COUNT];
        
    } else {
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[userSets count];
        
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        int setCount = 0;
        NSArray *reversedSets = [[userSets reverseObjectEnumerator] allObjects];
        
        for (ExerciseSet *set in reversedSets) {
            setCount++;
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            [xVals addObject:[NSNumber numberWithInt:setCount]];
            [yValsReps addObject:set.rep];
            [yValsVol addObject:set.weight];
        }
        
        NSLog(@"Reps: %.0f, Sets:%.0f, Volume: %.0f", totalReps, totalSets, totalWt);
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        [cal_data setObject:firstSet.exerciseName forKey:AT_NAME_KEY];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalSets]  forKey:@"Avg. Reps/Set"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f %@", totalWt, units]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f %@", totalWt/totalSets, units]  forKey:@"Avg. Volume/Set"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:6] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:2] forKey:CHART_COUNT];
        
    }
    return cal_data;
}

+(NSMutableDictionary *) rateUsMenu {
    NSArray *strings = @[@"Would you mind rating GYMINUTES?", @"Enjoying \"GYMINUTES\" ?", @"Do \"GYMINUTES\" deserve a 5 Star rating?"];
    NSArray *order = @[@"Desc", @"YES", @"NO"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:BUTTON forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];

    int random = rand() % [strings count];
    [cal_data setObject:[strings objectAtIndex:random] forKey:@"Desc"];
    switch (random) {
        case 0: {
            [cal_data setObject:@"Sure, I'll rate" forKey:@"YES"];
            [cal_data setObject:@"No, thanks" forKey:@"NO"];
            [cal_data setObject:[NSNumber numberWithBool:false] forKey:@"AskFurther"];
        }
            break;
        case 1: {
            [cal_data setObject:@"Yes." forKey:@"YES"];
            [cal_data setObject:@"Not really" forKey:@"NO"];
            [cal_data setObject:[NSNumber numberWithBool:true] forKey:@"AskFurther"];
        }
            break;
        case 2: {
            [cal_data setObject:@"Yes, you do" forKey:@"YES"];
            [cal_data setObject:@"Not yet" forKey:@"NO"];
            [cal_data setObject:[NSNumber numberWithBool:false] forKey:@"AskFurther"];
        }
            break;
        default:
            break;
    }

    return cal_data;
}

+(NSMutableDictionary *) featureRequest {
    NSArray *strings = @[@"Have a feature in mind?", @"Suggest a feature.", @"Are we missing a feature?"];
    NSArray *order = @[@"Desc", @"YES"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:SINGLE_BUTTON forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    int random = rand() % [strings count];
    [cal_data setObject:[strings objectAtIndex:random] forKey:@"Desc"];
    [cal_data setObject:@"Suggest" forKey:@"YES"];
    
    return cal_data;
}

+(NSMutableDictionary *) setExerciseGoals:(NSArray *)data {
    NSMutableArray *order = [[NSMutableArray alloc] init];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:RIGHT_SIDE_BUTTON forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    [cal_data setObject:@"Goals" forKey:AT_NAME_KEY];

    for (WorkoutList *key in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", key.exerciseName];
        ExerciseMetaInfo *exerciseMeta = [ExerciseMetaInfo MR_findFirstWithPredicate:exPredicate];
        ExerciseList *exList = [ExerciseList MR_findFirstWithPredicate:exPredicate];
        
        if (exerciseMeta == nil)
            continue;
        
        if ([exList.equipment isEqualToString:@"Bodyweight"]) {
            if (exerciseMeta.goalWeightOrRep == nil || [exerciseMeta.goalWeightOrRep floatValue] == 0)
                [cal_data setObject:@"Set Rep Goal" forKey:exerciseMeta.exerciseName];
        } else {
            if (exerciseMeta.goalWeightOrRep == nil || [exerciseMeta.goalWeightOrRep floatValue] == 0)
                [cal_data setObject:@"Set Weight Goal" forKey:exerciseMeta.exerciseName];
        }
        [order addObject:key.exerciseName];
    }
    return cal_data;
}
+(NSMutableDictionary *) setNotificationForWorkout {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"EEEE"];

    NSDateFormatter *timeFormatter = [[NSDateFormatter alloc] init];
    [timeFormatter setDateFormat:@"H:mm a"];

    
    NSMutableArray *order = [[NSMutableArray alloc] init];
    [order addObject:@"Desc"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:SINGLE_BUTTON forKey:DATA_TYPE];
    [cal_data setObject:@"Workout Reminder" forKey:@"Desc"];
    
    UIApplication* objApp = [UIApplication sharedApplication];
    NSArray *oldNotifications = [objApp scheduledLocalNotifications];
    
    if ([oldNotifications count] == 0) {
        [cal_data setObject:@"Would you like to set workout reminder?" forKey:@"Detail"];
        [cal_data setObject:@"Set Reminder" forKey:@"YES"];
    } else {
//        [cal_data setObject:@"Time" forKey:@"Days"];
//        [order addObject:@"Days"];
        for (UILocalNotification *notify in oldNotifications) {
            //NSLog(@"notifications are : %@", notify.fireDate);
            [order addObject:[formatter stringFromDate:notify.fireDate]];
            [cal_data setObject:[timeFormatter stringFromDate:notify.fireDate] forKey:[formatter stringFromDate:notify.fireDate]];
        }
        [cal_data setObject:@"Change Reminder" forKey:@"YES"];
    }
    [order addObject:@"YES"];
    
    [cal_data setObject:order forKey:AT_ORDER];
    
    return cal_data;
}
@end
