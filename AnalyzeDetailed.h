//
//  AnalyzeDetailed.h
//  uLift
//
//  Created by Mayank Verma on 7/23/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface AnalyzeDetailed : UIViewController

@property (nonatomic, retain) NSString *muscle, *routineName, *workout;
@property (nonatomic) int muscleOrWorkout; // muscle =0, workout = 1
@property (nonatomic, retain) NSArray *allData;
@property (nonatomic, retain) IBOutlet UITableView *myTableView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSegment;

@end
