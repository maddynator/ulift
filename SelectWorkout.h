//
//  SelectWorkout.h
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "MPCoachMarks.h"

@interface SelectWorkout : UIViewController <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, CNPPopupControllerDelegate, MPCoachMarksViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) UIColor *routineColor;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;


@end
