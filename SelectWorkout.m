//
//  SelectWorkout.m
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "SelectWorkout.h"
#import "TodayWorkout.h"
#import "RoutineDetailsTVC.h"

@interface SelectWorkout () {
    NSArray *dayArray, *muscleArray;
    NSArray *workoutDays;
    UISegmentedControl *segmentedControl;
    NSMutableArray *workoutDayName, *unique;
    NSString *selectedDay;
}
@end

@implementation SelectWorkout

@synthesize tableView, routineName, coachMarksView;

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.text = routineName;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont systemFontOfSize:12];
    subTitleLabel.text = @"Select Workout";
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;
    
    
    dayArray = [[NSArray alloc] initWithObjects:@"M", @"T", @"W", @"Th", @"F", @"Sa", @"Su", @"Any", nil];
    muscleArray = [Utilities getMuscleList];
    
    unique  = [NSMutableArray array];
    workoutDayName = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:FlatWhite];
    [self getWorkoutsDay];
    UIBarButtonItem *infoBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconInfo"] style:UIBarButtonItemStyleDone target:self action:@selector(routineInfo)];
    
    self.navigationItem.rightBarButtonItems = @[infoBtn];
}

-(void) routineInfo {
    [self performSegueWithIdentifier:@"routineDetailSegue" sender:self];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) getWorkoutsDay {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
    workoutDays = [UserWorkout MR_findAllSortedBy:@"createdDate" ascending:NO withPredicate:predicate inContext:localContext];
    
    NSLog(@"unumber of workouts days availabe are %lu", (long) [workoutDays count]);
    NSMutableSet * processed = [NSMutableSet set];
    
    for (UserWorkout *data in workoutDays) {
        NSString *string = data.workoutName;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
            [workoutDayName addObject:string];
        }
    }
    [tableView reloadData];
    
    if ([workoutDayName count] > 0)
        [self showCoachMarks];
    

}

- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle workoutDay: (NSString *) workoutDName {
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:workoutDName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:_date attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *doneButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 35)];
    [doneButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    doneButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [doneButton setTitle:@"Start Workout" forState:UIControlStateNormal];
    doneButton.backgroundColor = FlatOrangeDark;
    doneButton.layer.cornerRadius = 4;
    
    UIScrollView *scroll = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 250, 35)];
    scroll.contentSize = CGSizeMake(320, 700);
    scroll.showsHorizontalScrollIndicator = YES;
    
//    segmentedControl = [[UISegmentedControl alloc] initWithItems:muscleArray];
//    segmentedControl.frame = CGRectMake(0, 0, 250, 30);
//    [segmentedControl addTarget:self action:nil forControlEvents: UIControlEventValueChanged];
//    [scroll addSubview:segmentedControl];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    doneButton.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", routineName, workoutDName];

        NSArray *exercisesInWorkout = [UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
        
        for  (UserWorkout *exercise in exercisesInWorkout) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise.exerciseName];
            ExerciseList *ex = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];                    
            [Utilities createWorkoutListForExerciseOnDate:ex date:_date workoutInfo:exercise];
            NSLog(@"exercise name = %@, %@", ex.exerciseName, exercise.exerciseNumber);
        }
        [self.navigationController popToRootViewControllerAnimated:YES];
    };
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel,lineOneLabel,doneButton]];//scroll,
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}

#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [workoutDayName count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *workoutDName = [workoutDayName objectAtIndex:indexPath.row];
    NSArray *exerciseInDay = [Utilities getWorkoutsDayExercises:routineName workoutN:workoutDName];
    
    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    float cellWidth = CGRectGetWidth(cell.frame);
    float leftColumn = cellWidth/2;
    float rightColumn = leftColumn/3;
    
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else {
            //            NSLog(@"catn remove label");
        }
    }
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]] && lbl.tag == 501) {
            [lbl removeFromSuperview];
        } else {
            //            NSLog(@"catn remove label");
        }
    }
    
    UILabel *dayName = [[UILabel alloc] initWithFrame:CGRectMake(0, 1, cellWidth, 30)];
    dayName.tag = 100;
    dayName.textAlignment = NSTextAlignmentCenter;
    [dayName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    dayName.textColor = _routineColor;//[UIColor whiteColor];
    dayName.text = workoutDName;

    UILabel *lastDate = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(dayName.frame), cellWidth, 15)];
    lastDate.tag = 101;
    lastDate.textAlignment = NSTextAlignmentCenter;
    [lastDate setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:12]];
    lastDate.textColor = _routineColor;//[UIColor whiteColor];

    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ && workoutName == %@", routineName, workoutDName];
    NSArray *workoutExecutedTimes = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO withPredicate:predicate inContext:localContext];
    NSMutableArray *dateArray = [[NSMutableArray alloc] init];
    for(ExerciseSet *data in workoutExecutedTimes) {
        [dateArray addObject:data.date];
    }
    NSSet *uniqueWorkouts = [NSSet setWithArray:dateArray];
    
    if ([workoutExecutedTimes count] == 0) {
        // wokrout has not been performed yet..
        lastDate.text = @"Not done yet.";
    } else {
        ExerciseSet *lastSet = [workoutExecutedTimes objectAtIndex:0];
        lastDate.text = [NSString stringWithFormat:@"Last performed: %@, Total Times: %lu", lastSet.date, (long)[uniqueWorkouts count]];
    }

    [cell.contentView addSubview:dayName];
    [cell.contentView addSubview:lastDate];
    if ([exerciseInDay count] == 0) {
        UILabel *noExercise = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(lastDate.frame), cellWidth - 10, 30)];
        noExercise.text = @"No exercise added.";
        noExercise.textAlignment = NSTextAlignmentCenter;
        [noExercise setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
        noExercise.textColor = _routineColor;//[UIColor whiteColor];
        
//        dayName.backgroundColor = _routineColor;
//        cell.backgroundColor = _routineColor;
//        
        [cell.contentView addSubview:noExercise];
    } else {

        UILabel *setLbl = [[UILabel alloc] initWithFrame:CGRectMake(leftColumn, CGRectGetMaxY(lastDate.frame), rightColumn - 20, 20)];
        UILabel *repLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLbl.frame), CGRectGetMaxY(lastDate.frame), rightColumn + 10, 20)];
        UILabel *restTimerLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repLbl.frame), CGRectGetMaxY(lastDate.frame), rightColumn + 10, 20)];
        
        setLbl.text = @"Sets";
        repLbl.text = @"Reps";
        restTimerLbl.text = @"Time (s)";
        
        setLbl.textAlignment = NSTextAlignmentCenter;
        repLbl.textAlignment = NSTextAlignmentCenter;
        restTimerLbl.textAlignment = NSTextAlignmentCenter;
        
        setLbl.textColor = _routineColor;
        repLbl.textColor = _routineColor;
        restTimerLbl.textColor = _routineColor;
        
        
        [setLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        [repLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        [restTimerLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        
        [cell.contentView addSubview:setLbl];
        [cell.contentView addSubview:repLbl];
        [cell.contentView addSubview:restTimerLbl];
        
        
        NSMutableArray *exerciseGroupInWorkout = [[NSMutableArray alloc] init];
        NSMutableArray *exerciseInWorkout = [[NSMutableArray alloc] init];
        
        NSMutableArray * uniqueGrp = [NSMutableArray array];
        NSMutableSet * processedGroup = [NSMutableSet set];
        float maxY = CGRectGetMaxY(setLbl.frame);
        float newY = maxY;
        float lblHeight = 21;
        
        for (UserWorkout *data in exerciseInDay) {
            [exerciseInWorkout addObject:data];
            NSNumber *groupString = data.exerciseGroup;
            
            if ([processedGroup containsObject:groupString] == NO) {
                [uniqueGrp addObject:groupString];
                [processedGroup addObject:data.exerciseGroup];
            }
        }
      //  NSLog(@"Number of groups are %lu %@", (unsigned long)[processedGroup count], processedGroup);
        for (int i = 0; i < [processedGroup count]; i++) {
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %d", i];
            
            NSArray *allExInWorkout = [exerciseInWorkout filteredArrayUsingPredicate:predicate];
     //       NSLog(@"AllExInWorkout %lu", (unsigned long)[allExInWorkout count]);
            NSMutableArray *ex = [[NSMutableArray alloc] init];
            for (UserWorkout *item in allExInWorkout) {
                [ex addObject:item];
            }
            [exerciseGroupInWorkout addObject:ex];
      //      NSLog(@"exGroup %d, exNuminGroup %lu", i, (unsigned long)[[exerciseGroupInWorkout objectAtIndex:i] count]);
        }
        
        int exNumberCount = 0;
        for (int j = 0; j < [exerciseGroupInWorkout count]; j++) {
            int exInGroupCountTotal = (int)[[exerciseGroupInWorkout objectAtIndex:j] count];
            float rowGap = 21;
            maxY = newY;
            
            for  (int i = 0 ; i < exInGroupCountTotal; i++) {
                UserWorkout *workoutInfo = [exerciseInDay objectAtIndex:exNumberCount];
                UILabel *exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(5, maxY + i * rowGap, leftColumn, lblHeight)];
                UILabel *setLblVal = [[UILabel alloc] initWithFrame:CGRectMake(leftColumn, maxY + i * rowGap, rightColumn - 20, lblHeight)];
                UILabel *repLblVal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLblVal.frame), maxY + i * rowGap, rightColumn + 10, lblHeight)];
                UILabel *restTimerLblVal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repLblVal.frame), maxY + i * rowGap, rightColumn - 5 + 10, lblHeight)];
                
                exerciseName.textColor = _routineColor;//FlatWhiteDark;
                
                exerciseName.text = [NSString stringWithFormat:@" %@", workoutInfo.exerciseName];
                setLblVal.text = [NSString stringWithFormat:@"%d", [workoutInfo.sets intValue]];
                repLblVal.text = [NSString stringWithFormat:@"%@",  workoutInfo.repsPerSet];//[workoutInfo.reps intValue]];
                restTimerLblVal.text = [NSString stringWithFormat:@"%@", workoutInfo.restPerSet];//[workoutInfo.restTimer intValue]];
                
                [exerciseName setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [setLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [repLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                [restTimerLblVal setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                
                setLblVal.textAlignment = NSTextAlignmentCenter;
                repLblVal.textAlignment = NSTextAlignmentCenter;
                restTimerLblVal.textAlignment = NSTextAlignmentCenter;
                
                exerciseName.textColor = _routineColor;//[UIColor whiteColor];
                setLblVal.textColor = _routineColor;//[UIColor whiteColor];
                repLblVal.textColor = _routineColor;//[UIColor whiteColor];
                restTimerLblVal.textColor = _routineColor;//[UIColor whiteColor];
                
                if (exInGroupCountTotal > 1) {
                    if ([_routineColor isEqual:FlatOrange])
                        exerciseName.backgroundColor = setLblVal.backgroundColor = repLblVal.backgroundColor = restTimerLblVal.backgroundColor = FlatWhite;//FlatOrangeDark;
                    else
                        exerciseName.backgroundColor = setLblVal.backgroundColor = repLblVal.backgroundColor = restTimerLblVal.backgroundColor = FlatWhite;//[Utilities getDarkerColorFor:_routineColor];
                }
                
                //dayName.backgroundColor = _routineColor;//[Utilities getDarkerColorFor:_routineColor];
                //cell.backgroundColor = _routineColor;
                
                [cell.contentView addSubview:exerciseName];
                [cell.contentView addSubview:setLblVal];
                [cell.contentView addSubview:repLblVal];
                [cell.contentView addSubview:restTimerLblVal];
                exNumberCount++;
                newY = CGRectGetMaxY(restTimerLblVal.frame);
            }
            if (exInGroupCountTotal > 1)
                newY += 5;
        }

    }
    
    //configure right buttons
//    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
//    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    lineView.tag = 501;
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    
    [cell.contentView addSubview:lineView];
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray *exerciseCount = [Utilities getWorkoutsDayExercises:routineName workoutN:[workoutDayName objectAtIndex:indexPath.row]];
    if ([exerciseCount count] == 0) {
        return 60;
    } else {
        return 75 + 22 * ([exerciseCount count]);
    }
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *workoutDName = [workoutDayName objectAtIndex:indexPath.row];
    NSString *workoutStr = [NSString stringWithFormat:@"Do you want to start (%@) workout", workoutDName];

    NSArray *currWorkoutList = [Utilities getAllExerciseForDate:_date];
    NSLog(@"total exercise for today is %lu", (long) [currWorkoutList count]);

    if ( [currWorkoutList count] == 0) {

        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert addButton:@"Yes" actionBlock:^{
            [self addExercisesToTodayList:routineName workoutName:workoutDName];
        }];
        //[alert showInfo:self title:@"START WORKOUT" subTitle:workoutStr closeButtonTitle:@"No" duration:0.0f];
        [alert showCustom:self image:[Utilities getDetailedImage] color:FlatMintDark   title:@"START WORKOUT" subTitle:workoutStr closeButtonTitle:@"No" duration:0.0f]; // Custom

    } else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert addButton:@"Replace" actionBlock:^{
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                for(WorkoutList *ex in currWorkoutList) {
                    [ex MR_deleteEntityInContext:localContext];
                }
            } completion:^(BOOL contextDidSave, NSError *error) {
                [self addExercisesToTodayList:routineName workoutName:workoutDName];
            }];
        }];
        
        [alert addButton:@"Add Workout" actionBlock:^{
            int count = (int)[currWorkoutList count];
            NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", routineName, workoutDName];
            NSArray *exercisesInWorkout = [UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
            
            NSArray *currentWorkout = [Utilities getAllExerciseForDate:_date];
            int maxExNumber = 0;
            for (WorkoutList *exercise in currentWorkout) {
                NSLog(@"Ex group number %d maxExNum %d", [exercise.exerciseGroup intValue], maxExNumber);
                if ([exercise.exerciseGroup intValue] > maxExNumber) {
                    maxExNumber = [exercise.exerciseGroup intValue];
                }
            }
            NSLog(@"maximum group number is %d", maxExNumber);
            maxExNumber +=1;
            
            for  (UserWorkout *exercise in exercisesInWorkout) {
                NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise.exerciseName];
                ExerciseList *ex = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
                [Utilities addWorkoutListForExerciseOnDate:ex date:_date workoutInfo:exercise exerciseNumber:count maxExerciseGroup:maxExNumber + [exercise.exerciseGroup intValue]];
                NSLog(@"exercise name = %@, %d, group number is %d", ex.exerciseName, count, maxExNumber + [exercise.exerciseGroup intValue]);
                count++;
            }            
            self.tabBarController.selectedIndex = 1;
        }];
        [alert showWarning:self title:@"Workout in Progress" subTitle:@"Another workout in progress. Do you want to replace current workout or add this workout to today workout ?" closeButtonTitle:@"CANCEL" duration:0.0f];
    }

    
//    [self performSegueWithIdentifier:@"startWorkoutSegue" sender:self];
//
//    [self showPopupWithStyle:CNPPopupStyleCentered workoutDay:workoutDName];
}

-(void) addExercisesToTodayList: (NSString *) rName workoutName:(NSString *) workoutDName {

    //step:1 get all exercise from Workout that has to be added to today workout.
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", rName, workoutDName];
    NSArray *exercisesInWorkout = [UserWorkout MR_findAllSortedBy:@"exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
    
    // Step 2: get all exercises that have been added to today workout..
    NSArray *todayExercises = [Utilities getAllExerciseForDate:_date];
    
    
    // Step 3: Cycle through list of exercise from the workout that has to be added and check if exercise already present.. if present we skip it otherwise we add that to our exercersieName array....
    NSMutableArray *exToAdd = [[NSMutableArray alloc] init];
    if ([todayExercises count] > 0) {
        for  (UserWorkout *exercise in exercisesInWorkout) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise.exerciseName];
            NSArray *existingEx = [todayExercises filteredArrayUsingPredicate:exPredicate];
            if ([existingEx count] > 0) {
                NSLog(@"Exercise (%@) already present for date (%@)", exercise.exerciseName, _date);
            } else {
                [exToAdd addObject:exercise.exerciseName];
            }
        }
    } else {
        NSLog(@"no exercise is part of this.. So just add all");
        for  (UserWorkout *exercise in exercisesInWorkout) {
            [exToAdd addObject:exercise.exerciseName];
        }
    }
    
    
    NSArray *exerciseArray = [ExerciseList MR_findAllInContext:localContext];
    
    //NSLog(@"exercise that will be added are %@", exToAdd);
    // step 4: We iterate of exercises that can be added and add them to today workout..
    
    NSDate *now = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        /*
        for (NSString *exerciseName in exToAdd) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
            NSArray *exArr = [exerciseArray filteredArrayUsingPredicate:exPredicate];
            
            if ([exArr count] == 0) {
                NSLog(@"Exercise present in workout was not present in our exercise list. So we continue to next one.");
                continue;
            }
            ExerciseList *ex = [exArr objectAtIndex:0];
            NSArray *workoutArr = [exercisesInWorkout filteredArrayUsingPredicate:exPredicate];
            
            UserWorkout *workoutInfo;
            if ([workoutArr count] > 0) {
                workoutInfo = [workoutArr objectAtIndex:0];
                WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
                workout.exerciseName = ex.exerciseName;
                workout.muscle = ex.muscle;
                workout.majorMuscle = ex.majorMuscle;
                workout.date = _date;
                workout.timeStamp = [dateFormatter stringFromDate:now];
                workout.exerciseNumber = workoutInfo.exerciseNumber;
                workout.routiineName = workoutInfo.routineName;
                workout.workoutName = workoutInfo.workoutName;
                workout.restSuggested = workoutInfo.restTimer;
                workout.repsSuggested = workoutInfo.reps;
                workout.setsSuggested = workoutInfo.sets;
                workout.exerciseNumInGroup = workoutInfo.exerciseNumInGroup;
                workout.exerciseGroup = workoutInfo.exerciseGroup;
                ex.exercisePerformed = [NSNumber numberWithBool:true];
                NSLog(@"exercise name = %@, %@", ex.exerciseName, workoutInfo.exerciseNumber);
            }
            else
                NSLog(@" we will never be here ever because we create exToAdd from exerciseInWorkout.");
        }
        */
        NSMutableDictionary *exerciseGroup = [[NSMutableDictionary alloc] init];
        NSMutableArray *exerciseGroupOrder = [[NSMutableArray alloc] init];
        NSMutableArray *repairedUserWorkout = [[NSMutableArray alloc] init];
        
        bool isGroupRepairRequired = false;
        bool isExerciseInGroupRepairRequired = false;
        
        // this logic is to make sure that if for some reason, the workout exercise group or exercise in group or exercise number is messed up and not got saved, we dont want the todayWorkout to crash. So we need to make sure that everything is in order. Otherwise this will spoil user experience.
        for (UserWorkout *workout in exercisesInWorkout) {
            if ([exerciseGroup objectForKey:workout.exerciseGroup] == nil) {
                NSMutableDictionary *exerciseNumInGroupDict = [[NSMutableDictionary alloc] init];
                [exerciseNumInGroupDict setObject:[NSNumber numberWithInt:1] forKey:workout.exerciseNumInGroup];
                [exerciseGroup setObject:exerciseNumInGroupDict forKey:workout.exerciseGroup];
                [exerciseGroupOrder addObject:workout.exerciseGroup];
            } else {
                NSMutableDictionary *exerciseNumInGroupDict = [exerciseGroup objectForKey:workout.exerciseGroup];
                if ([exerciseNumInGroupDict objectForKey:workout.exerciseNumInGroup] != nil) {
                    // this means that for a given exercise group, there are 2 exercises with same number
                    isExerciseInGroupRepairRequired = true;
                } else {
                    [exerciseNumInGroupDict setObject:[NSNumber numberWithInt:1] forKey:workout.exerciseNumInGroup];
                }
            }
        }
        //NSLog(@"dict array %@", exerciseGroup);
        NSSortDescriptor *lowestToHighestGroup = [NSSortDescriptor sortDescriptorWithKey:@"self" ascending:YES];
        [exerciseGroupOrder sortUsingDescriptors:[NSArray arrayWithObject:lowestToHighestGroup]];
        
        //NSLog(@"sorted array is %@", exerciseGroupOrder);
        int groupRepairCheck = 0;
        for (int i = 0; i < [exerciseGroupOrder count]; i++) {
            if ([[exerciseGroupOrder objectAtIndex:i] intValue] != groupRepairCheck) {
                isGroupRepairRequired = true;
            }
            groupRepairCheck++;
        }
        
        if (isGroupRepairRequired == true) {
            NSLog(@"***** REPAIRING *****");
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           @"RoutineWorkoutName", [NSString stringWithFormat:@"%@,%@",rName, workoutDName],
                                           nil];
            [Flurry logEvent:@"WorkoutRepairing" withParameters:articleParams];
            
            // we first fix UserWorkout and then copy them here.
            int runningGroup = 0, totalGroupCount = 0;
            int runningExerciseNumber = 0;
            
            // the idea is to start with group number 0 and add repair exercise for that workout. For some reason, if that group does not exist, move onto next group number. We do this until the number of groups for which we have repaired exercises does not equal the total number of groups we calculated above.
            while (1) {
                NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", [NSNumber numberWithInt:runningGroup]];
                NSArray *exInGroup = [exercisesInWorkout filteredArrayUsingPredicate:groupPredicate];
                if ([exInGroup count] > 0) {
                    int runningExerciseNumberInGroup = 0;
                    for (UserWorkout *repairWorkout in exInGroup) {
                        //NSLog(@"runningGroup %d, runningExerciseNumberInGroup: %d, runningExerciseNumber:%d",runningGroup, runningExerciseNumberInGroup, runningExerciseNumber);
                        repairWorkout.exerciseGroup = [NSNumber numberWithInt:totalGroupCount];
                        repairWorkout.exerciseNumInGroup = [NSNumber numberWithInt:runningExerciseNumberInGroup];
                        repairWorkout.exerciseNumber = [NSNumber numberWithInt:runningExerciseNumber];
                        runningExerciseNumber++;
                        runningExerciseNumberInGroup++;
                        //repairWorkout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                        [repairedUserWorkout addObject:repairWorkout];
                    }
                    totalGroupCount++;
                }
                runningGroup++;
                if (totalGroupCount == [exerciseGroup count])
                    break;

                // fail safe
                if (runningGroup > 100)
                    break;
            }
        } else {
            for (UserWorkout *workout in exercisesInWorkout) {
                [repairedUserWorkout addObject:workout];
            }
        }
        
        for (UserWorkout *workoutInfo in repairedUserWorkout) {
            WorkoutList *workout = [WorkoutList MR_createEntityInContext:localContext];
            workout.exerciseName = workoutInfo.exerciseName;
            workout.muscle = workoutInfo.muscle;
            workout.majorMuscle = workoutInfo.majorMuscle;
            workout.date = _date;
            workout.timeStamp = [dateFormatter stringFromDate:now];
            workout.exerciseNumber = workoutInfo.exerciseNumber;
            workout.routiineName = workoutInfo.routineName;
            workout.workoutName = workoutInfo.workoutName;
            workout.restSuggested = workoutInfo.restTimer;
            workout.repsSuggested = workoutInfo.reps;
            workout.setsSuggested = workoutInfo.sets;
            workout.exerciseNumInGroup = workoutInfo.exerciseNumInGroup;
            workout.exerciseGroup = workoutInfo.exerciseGroup;
            workout.repsPerSet = workoutInfo.repsPerSet;
            workout.restPerSet = workoutInfo.restPerSet;
            
            // we need to mark that an exercise is performed here
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", workoutInfo.exerciseName];
            NSArray *exArr = [exerciseArray filteredArrayUsingPredicate:exPredicate];
            if ([exArr count] == 0) {
                NSLog(@"Exercise present in workout was not present in our exercise list. So we continue to next one.");
            } else {
                ExerciseList *ex = [exArr objectAtIndex:0];
                ex.exercisePerformed = [NSNumber numberWithBool:true];
            }
        }
     

/*
        // Now we have added all exercises.. let reshuffle them.
        NSArray *todayExercisesUpdated = [Utilities getAllExerciseForDate:_date];
        
        NSMutableArray * uniqueUpd = [NSMutableArray array];
        NSMutableSet * processedGroupUpt = [NSMutableSet set];
        
        for (WorkoutList *data in todayExercisesUpdated) {
            NSNumber *groupString = data.exerciseGroup;
            if ([processedGroupUpt containsObject:groupString] == NO) {
                [uniqueUpd addObject:groupString];
                [processedGroupUpt addObject:data.exerciseGroup];
            }
        }
        
        //NSLog(@"unique is %@", unique);
        int exerciseCount = 0, exerciseGroupRunning = 0;
        int exGroupCounter = 0;
        
        for (int i = 0; i < [unique count]; i++) {
            NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", [unique objectAtIndex:i]];
            NSArray *exerciseInThisGroup = [todayExercisesUpdated filteredArrayUsingPredicate:groupPredicate];
            
            if ([exerciseInThisGroup count] == 0) {
                //this means that intermediate ex got deleted... so we need to be careful.
                NSLog(@"all ex of group got deleted.. lets move on to next one and re-adjust everyone %d", i);
            } else {
                for (int j = 0; j < [exerciseInThisGroup count]; j++) {
                    WorkoutList *data = [exerciseInThisGroup objectAtIndex:j];
                    data.exerciseGroup = [NSNumber numberWithInt:exerciseGroupRunning];
                    data.exerciseNumInGroup = [NSNumber numberWithInt:j];
                    data.exerciseNumber = [NSNumber numberWithInt:exerciseCount];
                    NSLog(@"exercise group running is %d", exerciseGroupRunning);
                    exerciseCount++;
                }
                exerciseGroupRunning++;
            }
            NSLog(@"exgroup conter is %d", exGroupCounter);
            exGroupCounter++;
        }
        
        */
    } completion:^(BOOL contextDidSave, NSError *error) {
        // it is possible that few exercises were deleted and then new added. So we will just re-order the numbers.
        
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RtWtInfo", [NSString stringWithFormat:@"%@,%@",rName, workoutDName],
                                       nil];
        [Flurry logEvent:@"WorkoutStarted" withParameters:articleParams];

        self.tabBarController.selectedIndex = 1;
    }];
    //[Utilities createWorkoutListForExerciseOnDate:ex date:_date workoutInfo:workoutInfo];
 
}
/*
-(NSString *) temp_createRepsToSetStr:(int) numSets rep:(int) repVal {
    NSString *repsPerSet = @"";
    for (int i = 0; i < numSets; i++) {
        if ([repsPerSet isEqualToString:@""]) {
            repsPerSet = [NSString stringWithFormat:@"%d", repVal];
        } else {
            repsPerSet = [NSString stringWithFormat:@"%@,%d", repsPerSet, repVal];
        }
    }
    return repsPerSet;
}

-(NSString *) temp_createRestToSetStr:(int) numSets rest:(int) restVal {
    NSString *repsPerSet = @"";
    for (int i = 0; i < numSets; i++) {
        if ([repsPerSet isEqualToString:@""]) {
            repsPerSet = [NSString stringWithFormat:@"%d", restVal];
        } else {
            repsPerSet = [NSString stringWithFormat:@"%@,%d", repsPerSet, restVal];
        }
    }
    return repsPerSet;
}
*/
-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return NO;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    return YES;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No workout present for (%@) routine.", routineName];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = [NSString stringWithFormat:@"Please select \"HOME\" (Bottom Left)\n followed by \"CREATE ROUTINE\" and add workouts to (%@) routine.", routineName];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"routineDetailSegue"]) {
        RoutineDetailsTVC *destVC = segue.destinationViewController;
        destVC.routineNameLocal = routineName;
        destVC.isLocalStored = true;
    }
}


-(void) showCoachMarks {
    
    bool showSuperSetHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownStartWorkoutHint"];
    NSLog(@"show %d", showSuperSetHint);
    if ([Utilities isHintEnabled] || showSuperSetHint == false) {
        float width = self.view.frame.size.width;
        
        NSLog(@"in here....");
        CGRect coachmark2 = CGRectMake(0, 0, width, 200);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Select a workout to start.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    }
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownStartWorkoutHint"]);
    //    aleadyShowingHint = false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownStartWorkoutHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


@end
