//
//  Created by Dmitry Ivanenko on 14.04.14.
//  Copyright (c) 2014 Dmitry Ivanenko. All rights reserved.
//

#import "DIDatepicker.h"
#import "DIDatepickerDateView.h"
#import "Utilities.h"

const CGFloat kDIDatepickerHeight = 60.;
const CGFloat kDIDatepickerSpaceBetweenItems = 15.;
NSString * const kDIDatepickerCellIndentifier = @"kDIDatepickerCellIndentifier";

@interface DIDatepicker (){
    NSIndexPath *selectedIndexPath;
}

@property (strong, nonatomic) UICollectionView *datesCollectionView;
@property (strong, nonatomic, readwrite) NSDate *selectedDate;

@end


@implementation DIDatepicker

- (void)awakeFromNib
{
    [self setupViews];
}

- (id)initWithFrame:(CGRect)frame
{
    if(self = [super initWithFrame:frame]){
        [self setupViews];
    }
    
    return self;
}

- (void)setupViews
{
    self.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    self.backgroundColor = [UIColor whiteColor];
    self.bottomLineColor = [UIColor colorWithWhite:0.816 alpha:1.000];
    self.selectedDateBottomLineColor = self.tintColor;
}

#pragma mark Setters | Getters

- (void)setDates:(NSArray *)dates
{
    _dates = dates;
    
    [self.datesCollectionView reloadData];
    
    self.selectedDate = nil;
}

- (void)setSelectedDate:(NSDate *)selectedDate
{
    _selectedDate = selectedDate;
    
    NSIndexPath *selectedCellIndexPath = [NSIndexPath indexPathForItem:[self.dates indexOfObject:selectedDate] inSection:0];
    [self.datesCollectionView deselectItemAtIndexPath:selectedIndexPath animated:YES];

    [self.datesCollectionView selectItemAtIndexPath:selectedCellIndexPath animated:YES scrollPosition:UICollectionViewScrollPositionCenteredHorizontally];
    selectedIndexPath = selectedCellIndexPath;
    
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}

- (UICollectionView *)datesCollectionView
{
    if (!_datesCollectionView) {
        UICollectionViewFlowLayout *collectionViewLayout = [[UICollectionViewFlowLayout alloc] init];
        [collectionViewLayout setItemSize:CGSizeMake(kDIDatepickerItemWidth, CGRectGetHeight(self.bounds))];
        [collectionViewLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        [collectionViewLayout setSectionInset:UIEdgeInsetsMake(0, 100, 0, kDIDatepickerSpaceBetweenItems)];
        [collectionViewLayout setMinimumLineSpacing:kDIDatepickerSpaceBetweenItems];
        
        _datesCollectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:collectionViewLayout];
        
        NSString *deviceModel = [Utilities getIphoneName];
        NSLog(@"device model is %@", deviceModel);
        if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"])  {
            NSLog(@"iphone 4 or 4s");
                    [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/20, 0, CGRectGetWidth(self.bounds)/2)];

        } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
            NSLog(@"iphone 5 or 5s or SE or ipod");
                [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/20, 0, CGRectGetWidth(self.bounds)/2)];
        } else if ([deviceModel containsString:@"Plus"]) {
            
            NSLog(@"iphone 6s");
            [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/6, 0, CGRectGetWidth(self.bounds)/2)];
        } else if ([deviceModel containsString:@"6"]) {
            
            NSLog(@"iphone 6");
            [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/8, 0, CGRectGetWidth(self.bounds)/2)];
        } else if ([deviceModel containsString:@"iPad"]) {
            [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/20, 0, CGRectGetWidth(self.bounds)/2)];
        } else if ([deviceModel containsString:@"Simulator"]) {
            [_datesCollectionView setContentInset:UIEdgeInsetsMake(0, CGRectGetWidth(self.bounds)/8, 0, CGRectGetWidth(self.bounds)/2)];
        }



        [_datesCollectionView registerClass:[DIDatepickerCell class] forCellWithReuseIdentifier:kDIDatepickerCellIndentifier];
        [_datesCollectionView setBackgroundColor:[UIColor clearColor]];
        [_datesCollectionView setShowsHorizontalScrollIndicator:NO];
        [_datesCollectionView setAllowsMultipleSelection:NO];
        _datesCollectionView.dataSource = self;
        _datesCollectionView.delegate = self;
        [self addSubview:_datesCollectionView];
    }
    return _datesCollectionView;
}

- (void)setSelectedDateBottomLineColor:(UIColor *)selectedDateBottomLineColor
{
    _selectedDateBottomLineColor = selectedDateBottomLineColor;
    
    [self.datesCollectionView.indexPathsForSelectedItems enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        DIDatepickerCell *selectedCell = (DIDatepickerCell *)[self.datesCollectionView cellForItemAtIndexPath:obj];
        selectedCell.itemSelectionColor = _selectedDateBottomLineColor;
    }];
}

#pragma mark Public methods

- (void)selectDate:(NSDate *)date
{
    [[NSCalendar currentCalendar] rangeOfUnit:NSCalendarUnitDay startDate:&date interval:NULL forDate:date];
    
    NSAssert([self.dates indexOfObject:date] != NSNotFound, @"Date not found in dates array");
    
    self.selectedDate = date;
}

- (void)selectDateAtIndex:(NSUInteger)index
{
    NSAssert(index < self.dates.count, @"Index too big");
    
    self.selectedDate = self.dates[index];
}

// -

- (void)fillDatesFromDate:(NSDate *)fromDate toDate:(NSDate *)toDate
{
    NSAssert([fromDate compare:toDate] == NSOrderedAscending, @"toDate must be after fromDate");
    
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSDateComponents *days = [[NSDateComponents alloc] init];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSInteger dayCount = 0;
    while(YES){
        [days setDay:dayCount++];
        NSDate *date = [calendar dateByAddingComponents:days toDate:fromDate options:0];
        
        if([date compare:toDate] == NSOrderedDescending) break;
        [dates addObject:date];
    }
    
    self.dates = dates;
}

- (void)fillDatesFromDate:(NSDate *)fromDate numberOfDays:(NSInteger)numberOfDays
{
    NSDateComponents *days = [[NSDateComponents alloc] init];
    [days setDay:numberOfDays];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    [self fillDatesFromDate:fromDate toDate:[calendar dateByAddingComponents:days toDate:fromDate options:0]];
}

- (void)fillCurrentWeek
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *weekdayComponents = [calendar components:NSCalendarUnitWeekday fromDate:today];
    
    NSDateComponents *componentsToSubtract = [[NSDateComponents alloc] init];
    [componentsToSubtract setDay: - ((([weekdayComponents weekday] - [calendar firstWeekday]) + 7 ) % 7)];
    NSDate *beginningOfWeek = [calendar dateByAddingComponents:componentsToSubtract toDate:today options:0];
    
    NSDateComponents *componentsToAdd = [[NSDateComponents alloc] init];
    [componentsToAdd setDay:6];
    NSDate *endOfWeek = [calendar dateByAddingComponents:componentsToAdd toDate:beginningOfWeek options:0];
    
    [self fillDatesFromDate:beginningOfWeek toDate:endOfWeek];
}

- (void)fillCurrentMonth
{
    [self fillDatesWithCalendarUnit:NSCalendarUnitMonth];
}

- (void)fillCurrentYear
{
    [self fillDatesWithCalendarUnit:NSCalendarUnitYear];
}

#pragma mark Private methods

- (void)fillDatesWithCalendarUnit:(NSCalendarUnit)unit
{
    NSDate *today = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDate *beginning;
    NSTimeInterval length;
    [calendar rangeOfUnit:unit startDate:&beginning interval:&length forDate:today];
    NSDate *end = [beginning dateByAddingTimeInterval:length-1];
    
    [self fillDatesFromDate:beginning toDate:end];
}

- (void)drawRect:(CGRect)rect
{
    [super drawRect:rect];
    
    // draw bottom line
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetStrokeColorWithColor(context, self.bottomLineColor.CGColor);
    CGContextSetLineWidth(context, .5);
    CGContextMoveToPoint(context, 0, rect.size.height - .5);
    CGContextAddLineToPoint(context, rect.size.width, rect.size.height - .5);
    CGContextStrokePath(context);
}

#pragma mark - UICollectionView Delegate

-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  [self.dates count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    DIDatepickerCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:kDIDatepickerCellIndentifier forIndexPath:indexPath];
    
    
    cell.date = [self.dates objectAtIndex:indexPath.item];
    cell.itemSelectionColor = _selectedDateBottomLineColor;
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *cellDateString = [dateFormat stringFromDate:cell.date];
    
    
    // we need all exercises performed for the date on cell
    NSArray *exerciseList = [Utilities getAllExerciseForDate:cellDateString];
    if ([exerciseList count] == 0) {
        for (UIView *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UIView class]]) {
                if (lbl.tag == 100) {
                    [lbl removeFromSuperview];
                }
            }
        }
    } else {
        NSMutableArray * unique = [NSMutableArray array];
        NSMutableSet * processed = [NSMutableSet set];
        for (WorkoutList *data in exerciseList) {
            NSString *string = data.majorMuscle;
            
            if ([processed containsObject:string] == NO) {
                [unique addObject:string];
                [processed addObject:string];
            }
        }
        
        for (int i = 0 ; i < [unique count]; i++) {
            UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake(0, i * 5 , CGRectGetWidth(cell.frame), 5)];
            circleView.alpha = 0.5;
            circleView.layer.cornerRadius = 1;
            circleView.tag = 100;
            circleView.backgroundColor = [Utilities getMajorMuscleColor:[unique objectAtIndex:i]];
            [cell.contentView addSubview:circleView];
        }
        
    }
    if ([cellDateString isEqualToString:[Utilities getCurrentDate]]) {
        NSLog(@"CellDATE %@, %@", cellDateString, [Utilities getCurrentDate]);
        UIView *circleView = [[UIView alloc] initWithFrame:CGRectMake(-3, CGRectGetHeight(cell.frame) - 7 , CGRectGetWidth(cell.frame)+6, 5)];
        circleView.tag = 101;
        circleView.backgroundColor = [UIColor redColor];
        cell.tintColor = [UIColor whiteColor];
        [cell.contentView addSubview:circleView];
    } else {
        for (UIView *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UIView class]]) {
                if (lbl.tag == 101) {
                    [lbl removeFromSuperview];
                }
            }
        }
    }

    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return ![indexPath isEqual:selectedIndexPath];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.datesCollectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    _selectedDate = [self.dates objectAtIndex:indexPath.item];
    
    [collectionView deselectItemAtIndexPath:selectedIndexPath animated:YES];
    selectedIndexPath = indexPath;
    [self sendActionsForControlEvents:UIControlEventValueChanged];
}


@end
