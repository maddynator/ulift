//
//  uLiftIAPHelper.h
//  uLift
//
//  Created by Mayank Verma on 9/10/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "IAPHelper.h"

@interface uLiftIAPHelper : IAPHelper
+ (uLiftIAPHelper *)sharedInstance;

@end
