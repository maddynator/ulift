//
//  ExerciseListTable.h
//  uLift
//
//  Created by Mayank Verma on 6/28/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface ExerciseListTable : UITableViewController <UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>

@property (nonatomic, retain) NSString *muscleGroup;
@property (nonatomic, retain) NSString *date;
//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) UISearchController * searchController;
@end
