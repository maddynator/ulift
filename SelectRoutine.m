//
//  SelectRoutine.m
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "SelectRoutine.h"
#import "SelectWorkout.h"
#import "RoutineDetailsTVC.h"

@interface SelectRoutine () {
    NSString *routineName;
    NSArray *routineArray;
    RoutineMetaInfo *selectedRoutine;
    NSString *routineColor;
}
@end

@implementation SelectRoutine
@synthesize tableView, coachMarksView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    routineArray = [[NSArray alloc] init];
    self.title = @"Start Routine";
    UIBarButtonItem *storeBtn = [[UIBarButtonItem alloc] initWithTitle:@"Store" style:UIBarButtonItemStylePlain target:self action:@selector(openAppStore)];
    
    self.navigationItem.rightBarButtonItems = @[storeBtn];

    
    [self getWorkouts];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:FlatWhite];
}

-(void) openAppStore {
    self.tabBarController.selectedIndex = 3;
    //[self performSegueWithIdentifier:@"inAppStoreSegue" sender:self];
}

-(void) viewDidAppear:(BOOL)animated {
    routineName = @"";
    routineColor = @"";
    if ([routineArray count] > 0)
        [self showCoachMarks];
    
    [self.tableView reloadData];
}
-(void) getWorkouts {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    routineArray = [RoutineMetaInfo MR_findAllSortedBy:@"routineCreatedDate" ascending:NO inContext:localContext];
    
    NSLog(@"unumber of workouts availabe are %lu", (long) [routineArray count]);
    [tableView reloadData];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) createNewWorkout {
    [self performSegueWithIdentifier:@"createRoutineSegue" sender:self];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [routineArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
    UILabel *routineNm = (UILabel *)[cell viewWithTag:100];
//    UILabel *routineSummary = (UILabel *)[cell viewWithTag:101];
//    [routineSummary removeFromSuperview];
    
    RoutineMetaInfo *routine = [routineArray objectAtIndex:indexPath.row];
    routineNm.text = [routine.routineName uppercaseString];
    NSLog(@"[%@]", routine.routineName);
    //routineSummary.text = routine.routineSummary;
    
    [routineNm setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
    //[routineSummary setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    
    routineNm.textColor = [self colorForString:routine.routineColor];//[UIColor whiteColor];
    //routineSummary.textColor = [self colorForString:routine.routineColor];//[UIColor whiteColor];
    
    //cell.backgroundColor = FlatWhite;//[self colorForString:routine.routineColor];
    
    //configure right buttons
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]],
                          [MGSwipeButton buttonWithTitle:@"Edit" backgroundColor:[UIColor grayColor]]];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.delegate = self;
    
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
//    cell.accessoryView.backgroundColor = [Utilities getAppColor];
//    cell.tintColor = [Utilities getAppColor];//[UIColor whiteColor];
    
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];

    [cell.contentView addSubview:lineView];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    return cell;
}

//- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
//    NSLog(@"Accessory clicked... %lu", (long)indexPath.row);
//    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
//    RoutineMetaInfo *temp = [routineArray objectAtIndex:indexPath.row];
//    routineName = temp.routineName;
//    routineColor = temp.routineColor;
//
//    [self performSegueWithIdentifier:@"routineDetailSegue" sender:self];
//}


-(UIColor *) colorForString:(NSString *) colorStr {
    if ([colorStr isEqualToString:@"Red"])
        return FlatRed;
    if ([colorStr isEqualToString:@"Blue"])
        return FlatBlue;
    if ([colorStr isEqualToString:@"Orange"])
        return FlatOrange;
    if ([colorStr isEqualToString:@"Pink"])
        return FlatPink;
    if ([colorStr isEqualToString:@"Magenta"])
        return FlatMagenta;
    if ([colorStr isEqualToString:@"Brown"])
        return FlatBrown;
    if ([colorStr isEqualToString:@"Maroon"])
        return FlatMaroon;
    if ([colorStr isEqualToString:@"Mint"])
        return FlatMint;
    
    return FlatRed;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
    RoutineMetaInfo *temp = [routineArray objectAtIndex:indexPath.row];
    routineName = temp.routineName;
    routineColor = temp.routineColor;
    [self performSegueWithIdentifier:@"selectWorkoutSegue" sender:self];
}


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return NO;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    return YES;
}


-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Remove seperator inset
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    // Prevent the cell from inheriting the Table View's margin settings
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        [cell setPreservesSuperviewLayoutMargins:NO];
    }
    
    // Explictly set your cell's layout margins
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No routines created yet."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Please create a routine. Select \"HOME\" (Bottom Left) and click \"CREATE ROUINTE\"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}


// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"selectWorkoutSegue"]) {
        SelectWorkout *destVC = segue.destinationViewController;
        destVC.routineName = routineName;
        destVC.date = _date;
        destVC.routineColor = [Utilities colorForString:routineColor];
    } else if ([segue.identifier isEqualToString:@"routineDetailSegue"]) {
        RoutineDetailsTVC *destVC = segue.destinationViewController;
        destVC.routineNameLocal = routineName;
        destVC.isLocalStored = true;
    }
}


-(void) showCoachMarks {
    
    bool showSuperSetHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownStartRoutineHint"];
    
    if ([Utilities isHintEnabled] || showSuperSetHint == false) {
        float width = self.view.frame.size.width;
        
        //        CGRect coachmark1 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame) + 10, 40, 38);
        CGRect coachmark2 = CGRectMake(0, 0, width, 60);
        CGRect coachmark3 = CGRectMake(0, -30,  0, 0);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                //                                @{
                                //                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                //                                    @"caption": @"Click detail button for workout summary.",
                                //                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Select a routine to start.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark3],
                                    @"caption": @"\n\n\n\n\n\n\n\n\nClick 'STORE' to download more routines.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    }
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownCreateWorkoutHint"]);
    //    aleadyShowingHint = false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownStartRoutineHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}



@end
