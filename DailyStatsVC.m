//
//  DailyStatsVC.m
//  gyminutes
//
//  Created by Mayank Verma on 6/27/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "DailyStatsVC.h"
#import "DailyStats+CoreDataClass.h"

#define WEIGHT_BITMAP 1
#define BODYFAT_BITMAP 2
#define BMI_BITMAP 4


@interface DailyStatsVC () <SHMultipleSelectDelegate, CNPPopupControllerDelegate> {
    UIView *topView;
    DailyStats *todayItem;
    NSArray *allItems;
    int recordingItem;//0 is weight and bf, 1 is energy level
    JVFloatLabeledTextField *wtTf, *bfTv, *bmiTv;
    UISegmentedControl *elMor, *elPre, *elPost;
    NSMutableArray *graphElements;
    int selectedMap;
    int oldTimerIndex;
    NSDate *selectedDate;
    NSArray *reverseFilteredArray;
    UILabel *selectedLabel;
    NSDate *loadDate;
}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation DailyStatsVC
@synthesize tableView, calendarView;

-(void) createDummyData {
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    [DailyStats MR_truncateAllInContext:localContext];
    [localContext MR_saveToPersistentStoreAndWait];
    /*
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:1];
    [comps setMonth:6];
    [comps setYear:2016];
    NSDate *startDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    for (int i = 0; i < 30; i++) {
        int daysToAdd = i + 1;
        NSDate *newDate1 = [startDate dateByAddingTimeInterval:60*60*24*daysToAdd];
        
        DailyStats *item = [DailyStats MR_createEntityInContext:localContext];
        item.date = newDate1;
        item.bodyFat = [NSNumber numberWithFloat:rand()%25];
        item.bodyMassIndex = [NSNumber numberWithFloat:rand()%30];
        item.caloriesBurned = @0;
        item.elMorning = @0;
        item.elPreWorkout = @0;
        item.elPostWorkout = @0;
        item.steps = @0;
        item.syncedState = [NSNumber numberWithBool:false];
        item.weight = [NSNumber numberWithFloat:rand()%200];
        [localContext MR_saveToPersistentStoreAndWait];
    }
     */
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    selectedMap = WEIGHT_BITMAP;
    graphElements = [[NSMutableArray alloc] initWithObjects:@"Weight", @"Body Fat", @"Body Mass Index", nil];

    UIBarButtonItem *filterBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconFilter"] style:UIBarButtonItemStyleDone target:self action:@selector(loadFilterMenu)];
    self.navigationItem.rightBarButtonItems = @[filterBtn];
    
    loadDate = [NSDate date];
    self.title = @"Body Stats";
    recordingItem = -1;
    float topHeight = CGRectGetHeight(self.view.frame)/2;
    topView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, topHeight)];

    
    selectedLabel = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(topView.frame) - 10, 30)];
    selectedLabel.textColor = [UIColor whiteColor];
    selectedLabel.textAlignment = NSTextAlignmentCenter;
    selectedLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    
    _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, CGRectGetMaxY(selectedLabel.frame), CGRectGetWidth(topView.frame) - 10, CGRectGetHeight(topView.frame) - 100)];
    [self setupLineChartView: @""];
    NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
    [colorDict setObject:FlatMint forKey:@"Dots"];
    [colorDict setObject:FlatMintDark forKey:@"Cover"];
    //[self setLineChartDataValues:item[SETS_PROGRESS] colorDict:colorDict];
    
    _timeSegment = [[UISegmentedControl alloc] initWithItems:@[@"1W", @"2W", @"1M", @"3M", @"6M", @"All"]];
    _timeSegment.selectedSegmentIndex = 1;
    _timeSegment.frame = CGRectMake(5, CGRectGetMaxY(_lineChartView.frame) + 20, CGRectGetWidth(self.view.frame) - 10, 30);
    _timeSegment.tintColor = [UIColor whiteColor];
    [_timeSegment addTarget:self action:@selector(timeSegmentClicked:) forControlEvents:UIControlEventValueChanged];

    [self timeSegmentClicked:_timeSegment];
    oldTimerIndex = 1;

    
    topView.backgroundColor = FlatBlack;
    
    UIButton *recordStats = [[UIButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(topView.frame) + 5, CGRectGetWidth(self.view.frame) - 10, 30)];
    [recordStats setTitle:@"Record Body Stats" forState:UIControlStateNormal];
    [recordStats addTarget:self action:@selector(recWtAndBf:) forControlEvents:UIControlEventTouchUpInside];
    [recordStats.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    recordStats.layer.cornerRadius = 15;
    recordStats.backgroundColor = [Utilities getAppColor];
    [recordStats setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
//    UIButton *recordEL = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/2 + 5, CGRectGetMaxY(topView.frame) + 5, CGRectGetWidth(self.view.frame)/2 - 10, 30)];
//    [recordEL setTitle:@"Energy Level" forState:UIControlStateNormal];
//    [recordEL addTarget:self action:@selector(recEnergyLvL:) forControlEvents:UIControlEventTouchUpInside];
//    recordEL.layer.cornerRadius = 15;
//    recordEL.backgroundColor = [Utilities getAppColor];
//    [recordEL setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    [recordEL.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    
    
    // init table view
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(recordStats.frame) + 5, self.view.frame.size.width, CGRectGetHeight(self.view.frame) - topHeight - CGRectGetHeight(recordStats.frame)) style:UITableViewStyleGrouped];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    tableView.delegate = self;
    tableView.dataSource = self;
    
    tableView.backgroundColor = [UIColor clearColor];
    //self.view.backgroundColor = FlatRed;
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *startDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    [startDayComponents setHour:00];
    [startDayComponents setMinute:00];
    [startDayComponents setSecond:00];

    NSDateComponents *endDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[NSDate date]];
    [endDayComponents setHour:23];
    [endDayComponents setMinute:59];
    [endDayComponents setSecond:59];

    
    NSDate *startDate = [gregorian dateFromComponents:startDayComponents];
    NSDate *endDate = [gregorian dateFromComponents:endDayComponents];

    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startDate, endDate];
    allItems  = [DailyStats MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    todayItem = [DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];

    [self reloadData];

    [topView addSubview:_timeSegment];
    [topView addSubview:selectedLabel];
    [topView addSubview:_lineChartView];
    [self.view addSubview:topView];
    [self.view addSubview:recordStats];
    //[self.view addSubview:recordEL];
    [self.view addSubview:tableView];

}

-(void) reloadData {
    allItems = nil;
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    allItems  = [DailyStats MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    [self.tableView reloadData];
    [self timeSegmentClicked:_timeSegment];
}

-(void) viewDidAppear:(BOOL)animated {
    [self timeSegmentClicked:_timeSegment];
    //[self checkUserAndCreateWorkout];
}

-(void)viewWillDisappear:(BOOL)animated {
    [self syncDailyStatsToParse];
}
-(void) syncDailyStatsToParse{
    NSLog(@"Sync all daily stats to parse...");
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    
    //    [DailyStats MR_truncateAllInContext:localContext];
    //    [localContext MR_saveToPersistentStoreAndWait];
    
    NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"syncedState = %@", [NSNumber numberWithBool:SYNC_NOT_DONE]];
    NSArray *unsyncedItems  = [DailyStats MR_findAllWithPredicate:dailyPredicate inContext:localContext];
    for (DailyStats *item in unsyncedItems) {
        PFQuery *query = [PFQuery queryWithClassName:@P_DAILY_STATS];
        [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
        [query whereKey:@"Date" equalTo:item.date];
        [query setLimit: 1];
        [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
            if (task.error) {
                NSLog(@"Error: %@", task.error);
                return task;
            }
            NSLog(@"number of objects found are %lu", (long) [task.result count]);
            PFObject *rObj;
            if ([task.result count] == 0) {
                NSLog(@"Did not find a stats");
                rObj = [PFObject objectWithClassName:@P_DAILY_STATS];
                
                rObj[@"Date"] = item.date;
                rObj[@"BodyFat"] = item.bodyFat;
                rObj[@"BodyMassIndex"] = item.bodyMassIndex;
                rObj[@"CaloriesBurned"] = item.caloriesBurned;
//                rObj[@"ElMorning"] = item.elMorning;
//                rObj[@"ElPreWorkout"] = item.elPreWorkout;
//                rObj[@"ElPostWorkout"] = item.elPostWorkout;
                rObj[@"Steps"] = item.steps;
                rObj[@"Weight"] = item.weight;
                rObj[@"UserId"] = [[PFUser currentUser] objectId];
                [rObj saveEventually];
            } else {
                NSLog(@"stats found.. updating.. %lu", [task.result count]);
                rObj = [task.result objectAtIndex:0];
                rObj[@"Date"] = item.date;
                rObj[@"BodyFat"] = item.bodyFat;
                rObj[@"BodyMassIndex"] = item.bodyMassIndex;
                rObj[@"CaloriesBurned"] = item.caloriesBurned;
//                rObj[@"ElMorning"] = item.elMorning;
//                rObj[@"ElPreWorkout"] = item.elPreWorkout;
//                rObj[@"ElPostWorkout"] = item.elPostWorkout;
                rObj[@"Steps"] = item.steps;
                rObj[@"Weight"] = item.weight;
                rObj[@"UserId"] = [[PFUser currentUser] objectId];
                [rObj saveEventually];
            }
            return task;
        }];
        
        NSLog(@"routine sync state updated....");
        item.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        [localContext MR_saveToPersistentStoreAndWait];
    }
}
#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [allItems count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    // Similar to UITableViewCell, but
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[JVFloatLabeledTextField class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        }
    }

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd yyyy"];
    
    DailyStats *item = [allItems objectAtIndex:indexPath.row];
    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(self.view.frame)/2, 30)];
    date.text = [formatter stringFromDate:item.date];
    date.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
    
    UILabel *data = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(date.frame), 5, CGRectGetWidth(self.view.frame)/2 - 15, 25)];
    data.text = [NSString stringWithFormat:@"%@ %@ / %@%% / %@", item.weight, [[Utilities getUnits] lowercaseString], item.bodyFat, item.bodyMassIndex];
    data.font = [UIFont fontWithName:@HAL_BOLD_FONT size:14];
    data.textAlignment = NSTextAlignmentCenter;
    data.backgroundColor = [Utilities getAppColor];
    data.textColor = [UIColor whiteColor];
    data.layer.cornerRadius = 13;
    data.clipsToBounds = YES;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    [cell.contentView addSubview:date];
    [cell.contentView addSubview:data];
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    
//    DailyStats *selected = [allItems objectAtIndex:indexPath.row];
//    loadDate = selected.date;
//    [self recWtAndBf:self];
//    userModified = true;
//    selectedLabel.text = @"";
//
//    if (selectedMap & WEIGHT_BITMAP)
//        selectedLabel.text = [NSString stringWithFormat:@"WT: %@ %@", selected.weight, [[Utilities getUnits] lowercaseString]];
//    
//    if (selectedMap & BODYFAT_BITMAP)
//        selectedLabel.text = [NSString stringWithFormat:@"%@ BF: %@ %%", selectedLabel.text, selected.bodyFat];
//    
//    if (selectedMap & BMI_BITMAP)
//        selectedLabel.text = [NSString stringWithFormat:@"%@ BMI: %@", selectedLabel.text, selected.bodyMassIndex];
    
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.1f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 35;
}
-(IBAction)recWtAndBf:(id)sender {
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"BODY STATS" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];

    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];
    
    calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(0, 0, 250, 120)];
    [self dailyCalendarViewDidSelect:loadDate];
    [calendarView setSelectedDate:loadDate];
    [calendarView redrawToDate:loadDate];
    calendarView.delegate = self;
    calendarView.backgroundColor = [Utilities getAppColor];
    
    NSLog(@"date user **** %@ %@", loadDate, calendarView.selectedDate);
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 150)];
    customView.backgroundColor = [Utilities getAppColor];
    
    wtTf = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, 10, 230, 35)];
    wtTf.backgroundColor = [UIColor whiteColor];
    wtTf.textAlignment = NSTextAlignmentCenter;
    wtTf.keyboardType = UIKeyboardTypeDecimalPad;
    wtTf.text = [todayItem.weight stringValue];
    wtTf.layer.cornerRadius = 5;
    
    wtTf.placeholder = [NSString stringWithFormat:@"Weight (%@)", [[Utilities getUnits] lowercaseString]];
    [customView addSubview:wtTf];
    
    bfTv = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(wtTf.frame)+ 5, 230, 35)];
    bfTv.backgroundColor = [UIColor whiteColor];
    bfTv.placeholder = @"BodyFat (%)";
    bfTv.textAlignment = NSTextAlignmentCenter;
    bfTv.keyboardType = UIKeyboardTypeDecimalPad;
    bfTv.text = [todayItem.bodyFat stringValue];
    bfTv.layer.cornerRadius = 5;
    [customView addSubview:bfTv];

    bmiTv = [[JVFloatLabeledTextField alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(bfTv.frame) + 5, 230, 35)];
    bmiTv.backgroundColor = [UIColor whiteColor];
    bmiTv.placeholder = @"Body Mass Index";
    bmiTv.textAlignment = NSTextAlignmentCenter;
    bmiTv.text = [todayItem.bodyMassIndex stringValue];
    bmiTv.keyboardType = UIKeyboardTypeDecimalPad;
    bmiTv.layer.cornerRadius = 5;
    [customView addSubview:bmiTv];

    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Save" forState:UIControlStateNormal];
    button.backgroundColor = [Utilities getAppColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        
        NSCalendar *gregorian = [[NSCalendar alloc]
                                 initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *startDayComponents =
        [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:calendarView.selectedDate];
        [startDayComponents setHour:00];
        [startDayComponents setMinute:00];
        [startDayComponents setSecond:00];
        
        NSDateComponents *endDayComponents =
        [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:calendarView.selectedDate];
        [endDayComponents setHour:23];
        [endDayComponents setMinute:59];
        [endDayComponents setSecond:59];
        
        
        NSDate *startDate = [gregorian dateFromComponents:startDayComponents];
        NSDate *endDate = [gregorian dateFromComponents:endDayComponents];
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startDate, endDate];

        DailyStats *item  =[DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];
        
        if (item == nil) {
            NSLog(@"DS: New entry for date %@", calendarView.selectedDate);
            item = [DailyStats MR_createEntityInContext:localContext];
            item.date = calendarView.selectedDate;
            item.bodyFat = ([bfTv.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[bfTv.text floatValue]];
            item.bodyMassIndex = ([bmiTv.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[bmiTv.text floatValue]];
            item.caloriesBurned = @0;
            item.elMorning = @0;
            item.elPreWorkout = @0;
            item.elPostWorkout = @0;
            item.steps = @0;
            item.syncedState = [NSNumber numberWithBool:false];
            item.weight = ([wtTf.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[wtTf.text floatValue]];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           nil];
            [Flurry logEvent:@"DailyStatsRecorded" withParameters:articleParams];
            
//            Mixpanel *mixpanel = [Mixpanel sharedInstance];
//            [mixpanel track:@"DailyStatsRecorded" properties:articleParams];
        } else {
            NSLog(@"DS: updated entry for date %@", calendarView.selectedDate);
            item.bodyFat = ([bfTv.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[bfTv.text floatValue]];
            item.bodyMassIndex = ([bmiTv.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[bmiTv.text floatValue]];
            item.syncedState = [NSNumber numberWithBool:false];
            item.weight = ([wtTf.text floatValue] == 0) ? @0 : [NSNumber numberWithFloat:[wtTf.text floatValue]];
        }
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [self reloadData];
            [self.tableView reloadData];
            wtTf = nil;
            bfTv = nil;
            bmiTv = nil;
            loadDate = [NSDate date];
        }];
        
    };

    recordingItem = 0;
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, calendarView, customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.theme.backgroundColor = [Utilities getAppColor];
    self.popupController.theme.contentVerticalPadding = 0;
    [self.popupController presentPopupControllerAnimated:YES];
}

/*
-(IBAction)recEnergyLvL:(id)sender {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];

    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"ENERGY LEVEL" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];
    
    calendarView = [[CLWeeklyCalendarView alloc] initWithFrame:CGRectMake(0, 0, 250, 120)];
    calendarView.delegate = self;
    calendarView.backgroundColor = [Utilities getAppColor];
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 170)];
    customView.backgroundColor = [Utilities getAppColor];
    
    UILabel *elMorning, *elPreWorkout, *elPostWorkout;
    elMorning = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 20)];
    elMorning.textAlignment = NSTextAlignmentCenter;
    elMorning.text = @"Energy (Morning)";
    elMorning.textColor = [UIColor whiteColor];
    elMorning.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
    [customView addSubview:elMorning];

    elMor = [[UISegmentedControl alloc] initWithItems:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"]];
    elMor.frame = CGRectMake(10, CGRectGetMaxY(elMorning.frame), 230, 30);
    elMor.tintColor = [UIColor whiteColor];
    elMor.tag = 1;
    [customView addSubview:elMor];

    elPreWorkout = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(elMor.frame) + 5, 230, 20)];
    elPreWorkout.text = @"Energy (PreWorkout)";
    elPreWorkout.textAlignment = NSTextAlignmentCenter;
    elPreWorkout.textColor = [UIColor whiteColor];
    elPreWorkout.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
    [customView addSubview:elPreWorkout];
    
    elPre = [[UISegmentedControl alloc] initWithItems:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"]];
    elPre.frame = CGRectMake(10, CGRectGetMaxY(elPreWorkout.frame), 230, 30);
    elPre.tintColor = [UIColor whiteColor];
    elPre.tag = 2;
    [customView addSubview:elPre];

    elPostWorkout = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(elPre.frame) + 5, 230, 20)];
    elPostWorkout.text = @"Energy (PreWorkout)";
    elPostWorkout.textColor = [UIColor whiteColor];
    elPostWorkout.textColor = [UIColor whiteColor];
    elPostWorkout.textAlignment = NSTextAlignmentCenter;
    elPostWorkout.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
    [customView addSubview:elPostWorkout];
    
    elPost = [[UISegmentedControl alloc] initWithItems:@[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10"]];
    elPost.frame = CGRectMake(10, CGRectGetMaxY(elPostWorkout.frame), 230, 30);
    elPost.tintColor = [UIColor whiteColor];
    elPost.tag = 3;
    [customView addSubview:elPost];
    
    if (todayItem != nil) {
        elMor.selectedSegmentIndex = [todayItem.elMorning intValue] + 1;
        elPre.selectedSegmentIndex = [todayItem.elPreWorkout intValue] + 1;
        elPost.selectedSegmentIndex = [todayItem.elPostWorkout intValue] + 1;
    }
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Save" forState:UIControlStateNormal];
    button.backgroundColor = [Utilities getAppColor];
    button.layer.cornerRadius = 0;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date == %@", calendarView.selectedDate];
        DailyStats *item  =[DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];
        
        if (item == nil) {
            NSLog(@"EL: New entry for date %@", calendarView.selectedDate);
            item = [DailyStats MR_createEntityInContext:localContext];
            item.date = calendarView.selectedDate;
            item.bodyFat = @0;
            item.bodyMassIndex = @0;
            item.caloriesBurned = @0;
            item.elMorning = [NSNumber numberWithInt:(int)elMor.selectedSegmentIndex];
            item.elPreWorkout = [NSNumber numberWithInt:(int)elPre.selectedSegmentIndex];
            item.elPostWorkout = [NSNumber numberWithInt:(int)elPost.selectedSegmentIndex];
            item.steps = @0;
            item.syncedState = [NSNumber numberWithBool:false];
            item.weight = @0;
        } else {
            NSLog(@"EL: updated entry for date %@", calendarView.selectedDate);
            item.elMorning = @0;
            item.elPreWorkout = @0;
            item.elPostWorkout = @0;
            item.syncedState = [NSNumber numberWithBool:false];
        }
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError * _Nullable error) {
            [self getDailyStatsInfo];
            [self.tableView reloadData];
        }];
    };
    
    recordingItem = 1;
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, calendarView, customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.theme.contentVerticalPadding = 0;
    self.popupController.theme.backgroundColor = [Utilities getAppColor];
    [self.popupController presentPopupControllerAnimated:YES];


}
*/

#pragma mark - CLWeeklyCalendarViewDelegate
-(NSDictionary *)CLCalendarBehaviorAttributes
{
    return @{
                          CLCalendarWeekStartDay : @1,                 //Start Day of the week, from 1-7 Mon-Sun -- default 1
                          CLCalendarDayTitleTextColor : [UIColor whiteColor],
                          CLCalendarSelectedDatePrintColor : [UIColor whiteColor],
                          CLCalendarBackgroundImageColor:   [Utilities getAppColor]
            };
}


-(void)dailyCalendarViewDidSelect:(NSDate *)date
{
    NSLog(@"date selected by user is %@", date);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *startDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    [startDayComponents setHour:00];
    [startDayComponents setMinute:00];
    [startDayComponents setSecond:00];
    
    NSDateComponents *endDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    [endDayComponents setHour:23];
    [endDayComponents setMinute:59];
    [endDayComponents setSecond:59];
    
    
    NSDate *startDate = [gregorian dateFromComponents:startDayComponents];
    NSDate *endDate = [gregorian dateFromComponents:endDayComponents];
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startDate, endDate];
    todayItem = [DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];
    
    switch (recordingItem) {
        case 0: {
            if (todayItem != nil) {
                wtTf.text = [todayItem.weight stringValue];
                bfTv.text = [todayItem.bodyFat stringValue];
                bmiTv.text = [todayItem.bodyMassIndex stringValue];
            } else {
                wtTf.text = @"";
                bfTv.text = nil;
                bmiTv.text = nil;
            }
        }
            break;
            
        case 1: {
            if (todayItem != nil) {
                elMor.selectedSegmentIndex = [todayItem.elMorning intValue] + 1;
                elPre.selectedSegmentIndex = [todayItem.elPreWorkout intValue] + 1;
                elPost.selectedSegmentIndex = [todayItem.elPostWorkout intValue] + 1;
            } else {
                elMor.selectedSegmentIndex = -1;
                elPre.selectedSegmentIndex = -1;
                elPost.selectedSegmentIndex = -1;
            }
        }
            break;
        default:
            break;
    }

}

-(void) setupLineChartView: (NSString *) name {
    _lineChartView.delegate = self;
    
    _lineChartView.descriptionText = name;
    _lineChartView.noDataText = @"No data.";

    
    _lineChartView.dragEnabled = YES;
    [_lineChartView setScaleEnabled:YES];
    _lineChartView.pinchZoomEnabled = YES;
    _lineChartView.drawGridBackgroundEnabled = NO;
    _lineChartView.backgroundColor = [UIColor clearColor];//[Utilities getAppColorLight];
    _lineChartView.layer.cornerRadius = 4;
    
    
    ChartXAxis *xAxis = _lineChartView.xAxis;
    xAxis.drawAxisLineEnabled = YES;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    
    
    ChartYAxis *leftAxis = _lineChartView.leftAxis;
    leftAxis.drawAxisLineEnabled = YES;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.drawZeroLineEnabled = NO;
    leftAxis.enabled = YES;
    
    ChartYAxis *rightAxis = _lineChartView.rightAxis;
    rightAxis.drawAxisLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    
    _lineChartView.legend.enabled = false;
    
    _lineChartView.rightAxis.enabled = NO;
    _lineChartView.xAxis.axisLineColor = FlatWhite;
    _lineChartView.xAxis.labelTextColor = FlatWhite;
    _lineChartView.leftAxis.axisLineColor = FlatWhite;
    _lineChartView.leftAxis.labelTextColor = FlatWhite;
    
    [_lineChartView.viewPortHandler setMaximumScaleY: 2.f];
    [_lineChartView.viewPortHandler setMaximumScaleX: 2.f];
    
    //    ChartMarker *marker = [[ChartMarker alloc] initWithColor:[UIColor colorWithWhite:180/255. alpha:1.0] font:[UIFont systemFontOfSize:12.0] insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    //    marker.minimumSize = CGSizeMake(80.f, 40.f);
    //    _chartView.marker = marker;
    
    //    _chartView.legend.form = ChartLegendFormLine;
    
    [_lineChartView animateWithXAxisDuration:2.5 easingOption:ChartEasingOptionEaseInOutQuart];
    
    
}
-(void) setLineChartDataValues: (NSArray *) dataItems {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d"];
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals2 = [[NSMutableArray alloc] init];
    NSMutableArray *yVals3 = [[NSMutableArray alloc] init];
    
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [dataItems count]; i++)
    {
        DailyStats *item = [dataItems objectAtIndex:i];
        [xVals addObject:[formatter stringFromDate:item.date]];
    }
    
    for (int i = 0; i < [dataItems count]; i++)
    {
        DailyStats *item = [dataItems objectAtIndex:i];
//        if ([item.weight floatValue] == 0)
//            continue;
        [yVals1 addObject:[[ChartDataEntry alloc] initWithX:i y:[item.weight floatValue]]];
        
    }
    
    for (int i = 0; i < [dataItems count]; i++)
    {
        DailyStats *item = [dataItems objectAtIndex:i];
//        if ([item.bodyFat floatValue] == 0)
//            continue;
        [yVals2 addObject:[[ChartDataEntry alloc]  initWithX:i y:[item.bodyFat floatValue]]];
    }

    for (int i = 0; i < [dataItems count]; i++)
    {
        DailyStats *item = [dataItems objectAtIndex:i];
//        if ([item.bodyMassIndex floatValue] == 0)
//            continue;
        [yVals3 addObject:[[ChartDataEntry alloc] initWithX:i y:[item.bodyMassIndex floatValue]]];
    }

    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];


    if (selectedMap & WEIGHT_BITMAP) {
        LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals1 label:@""];
        [set1 setColor:[UIColor whiteColor]];
        set1.lineWidth = 2.5;
        set1.circleRadius = 0;
        set1.drawCircleHoleEnabled = YES;
        set1.drawValuesEnabled = NO;
        set1.valueFont = [UIFont systemFontOfSize:9.f];
        set1.valueTextColor = FlatWhite;
        set1.fillAlpha = 65/255.0;
        set1.fillColor = FlatYellow;
        set1.fillAlpha = 1.f;
        set1.drawFilledEnabled = NO;
        set1.highlightColor = FlatWhite;
        [dataSets addObject:set1];
    }


    if (selectedMap & BODYFAT_BITMAP) {
        LineChartDataSet *set2 = [[LineChartDataSet alloc] initWithValues:yVals2 label:@""];
        [set2 setColor:FlatRedDark];
        set2.lineWidth = 2.5;
        set2.circleRadius = 0;
        set2.drawCircleHoleEnabled = YES;
        set2.valueFont = [UIFont systemFontOfSize:9.f];
        set2.valueTextColor = FlatRedDark;
        set2.drawValuesEnabled = NO;//remove text from chart values
        set2.fillAlpha = 65/255.0;
        set2.fillColor = FlatYellow;
        set2.fillAlpha = 1.f;
        set2.drawFilledEnabled = NO;
        set2.highlightColor = FlatRedDark;
        [dataSets addObject:set2];

    }

    if (selectedMap & BMI_BITMAP) {
        LineChartDataSet *set3 = [[LineChartDataSet alloc] initWithValues:yVals3 label:@""];
        [set3 setColor:FlatGreenDark];
        set3.lineWidth = 2.5;
        set3.circleRadius = 0;
        set3.drawCircleHoleEnabled = YES;
        set3.drawValuesEnabled = NO;
        set3.valueFont = [UIFont systemFontOfSize:9.f];
        set3.valueTextColor = FlatGreenDark;
        set3.fillAlpha = 65/255.0;
        set3.fillColor = FlatYellow;
        set3.fillAlpha = 1.f;
        set3.drawFilledEnabled = NO;
        set3.highlightColor = FlatGreenDark;
        [dataSets addObject:set3];
        
    }
    
    
    LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@HAL_REG_FONT size:8.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _lineChartView.data = data;
    [_lineChartView highlightValues:nil];
    
}

-(void)chartValueSelected:(ChartViewBase *)chartView entry:(ChartDataEntry *)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight *)highlight {
    
    DailyStats *selected = [reverseFilteredArray objectAtIndex:highlight.x];
    NSLog(@"%ld %@", (long)dataSetIndex, selected.weight);
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM d"];

    selectedLabel.text = [formatter stringFromDate:selected.date];
    
    if (selectedMap & WEIGHT_BITMAP)
        selectedLabel.text = [NSString stringWithFormat:@"%@ WT: %@ %@", selectedLabel.text, selected.weight, [[Utilities getUnits] lowercaseString]];
    
    if (selectedMap & BODYFAT_BITMAP)
        selectedLabel.text = [NSString stringWithFormat:@"%@ BF: %@ %%", selectedLabel.text, selected.bodyFat];
    
    if (selectedMap & BMI_BITMAP)
        selectedLabel.text = [NSString stringWithFormat:@"%@ BMI: %@", selectedLabel.text, selected.bodyMassIndex];

}
-(void) loadFilterMenu {
    SHMultipleSelect *multipleSelect = [[SHMultipleSelect alloc] init];
    multipleSelect.delegate = self;
    multipleSelect.tintColor = [Utilities getAppColor];
    multipleSelect.rowsCount = graphElements.count;
    [multipleSelect show];
}

#pragma mark - SHMultipleSelectDelegate

- (void)multipleSelectView:(SHMultipleSelect *)multipleSelectView clickedBtnAtIndex:(NSInteger)clickedBtnIndex withSelectedIndexPaths:(NSArray *)selectedIndexPaths {
    if (clickedBtnIndex == 1) { // Done btn
        selectedMap  = 0;
        for (NSIndexPath *indexPath in selectedIndexPaths) {
            NSLog(@"selected index is %ld", (long)indexPath.row);
            switch (indexPath.row) {
                case 0:
                    selectedMap |= WEIGHT_BITMAP;
                    break;
                case 1:
                    selectedMap |= BODYFAT_BITMAP;
                    break;
                case 2:
                    selectedMap |= BMI_BITMAP;
                    break;
                default:
                    break;
            }
        }
    }
    
    NSLog(@"selected map %d", selectedMap);
    [self timeSegmentClicked:_timeSegment];
}

- (NSString *)multipleSelectView:(SHMultipleSelect *)multipleSelectView titleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return graphElements[indexPath.row];
}

- (BOOL)multipleSelectView:(SHMultipleSelect *)multipleSelectView setSelectedForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL canSelect = NO;
    
    if (selectedMap & WEIGHT_BITMAP && indexPath.row == 0)
        canSelect = YES;
    
    if (selectedMap & BODYFAT_BITMAP && indexPath.row == 1)
        canSelect = YES;
    
    if (selectedMap & BMI_BITMAP && indexPath.row == 2)
        canSelect = YES;
    
    return canSelect;
}

-(void)timeSegmentClicked:(UISegmentedControl *)segmentedControl {
    
    bool showAnalysis = false;
    
    if (segmentedControl.selectedSegmentIndex > 1) {
        if ([Utilities showPowerRoutinePackage]) {
            [self showPurchasePopUp:@"This is a paid feature."];
            _timeSegment.selectedSegmentIndex = oldTimerIndex;
        } else
            showAnalysis = true;
    } else
        showAnalysis = true;
    
    if (showAnalysis == true) {
        oldTimerIndex = (int) segmentedControl.selectedSegmentIndex;
        
        NSString *dateStringForHeaders = @"";
        NSMutableArray *filteredArrayByDate = [[NSMutableArray alloc] init];
        NSLog(@"segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *today = [NSDate date];
        
        switch (segmentedControl.selectedSegmentIndex) {
            case 0:
                selectedDate = [today dateBySubtractingWeeks:1];
                dateStringForHeaders = @"last week.";
                break;
            case 1:
                selectedDate = [today dateBySubtractingWeeks:2];
                dateStringForHeaders = @"last 2 weeks.";
                break;
            case 2:
                selectedDate = [today dateBySubtractingMonths:1];
                dateStringForHeaders = @"last 1 month.";
                break;
            case 3:
                selectedDate = [today dateBySubtractingMonths:3];
                dateStringForHeaders = @"last 3 months.";
                break;
            case 4:
                selectedDate = [today dateBySubtractingMonths:6];
                dateStringForHeaders = @"last 6 months.";
                break;
            case 5:
                selectedDate = [today dateBySubtractingYears:1];
                dateStringForHeaders = @"last 1 year.";
                break;
            default:
                break;
        }
        
        for (DailyStats *ex in allItems) {
            NSDate *date = ex.date;
            if ([date isLaterThanOrEqualTo:selectedDate]) {
                [filteredArrayByDate addObject:ex];
            } else {
            }
            
        }
        reverseFilteredArray = [[filteredArrayByDate reverseObjectEnumerator] allObjects];
        
        [self setLineChartDataValues:reverseFilteredArray];
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ROUTINE PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ROUTINE_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium or Power Routine Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}


-(void) checkUserAndCreateWorkout {
    if ([[PFUser currentUser].email isEqualToString:@"mayankworkoutcreator@gmail.com"]) {
        [self syncIAPWorkoutInfo: @"FOCUSSED FOUR" bundleId:@IAP_ROUTINE_FOCUSSED_FOUR syncNow:true];
        //[self syncIAPWorkoutInfo: @"FOCUSSED FOUR" bundleId:@IAP_ROUTINE_FOCUSSED_FOUR syncNow:true];
        //[self syncIAPWorkoutInfo: @"STRONG SIX" bundleId:@IAP_ROUTINE_6DAYSPLIT syncNow:true];
        //[self syncIAPWorkoutInfo: @"3 DAY DUMBBELL DOMINATION " bundleId:@IAP_ROUTINE_3DAYDUMBBELLDOMINATION syncNow:true];
        //[self syncIAPWorkoutInfo: @"GERMAN VOLUME TRAINING II" bundleId:@IAP_ROUTINE_GVT_II syncNow:true];
        //[self syncIAPWorkoutInfo: @"GERMAN VOLUME TRAINING" bundleId:@IAP_ROUTINE_GVT_I syncNow:true];
        //[self syncIAPWorkoutInfo: @"PENTAGRAM POWER" bundleId:@IAP_ROUTINE_5DAYSPLIT syncNow:false];
        //[self syncIAPWorkoutInfo: @"4 DAY MUSCLE MASS" bundleId:@IAP_ROUTINE_4DAYSPLIT syncNow:false];
        //[self syncIAPWorkoutInfo: @"2 DAY SPLIT: PUSH/PULL" bundleId:@"com.gymessential.gyminutes.2daysplitpushpull" syncNow:false];
        //[self syncIAPWorkoutInfo: @"3 DAY LEAN MUSCLE" bundleId:@IAP_ROUTINE_3DAYSPLIT syncNow:false];
    } else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self.parentViewController title:@"ACCESS DENIED" subTitle:@"YOU ARE NOT AUTHORIZED." closeButtonTitle:@"OK" duration:0.0];

    }
}

-(void) syncIAPWorkoutInfo: (NSString *) workoutName bundleId: (NSString *) bundleId syncNow:(BOOL) syncNow {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", workoutName];
    NSArray *workouts = [UserWorkout MR_findAllSortedBy:@"workoutName,exerciseNumber" ascending:YES withPredicate:predicate inContext:localContext];
    NSLog(@"will sync workout info now... %lu", (long) [workouts count]);
    
    
    for (UserWorkout *workoutObj in workouts) {
        
        NSLog(@"%@, %@, %@, %@", workoutObj.routineName, workoutObj.workoutName, workoutObj.exerciseName, workoutObj.exerciseNumber);


        if (syncNow == true) {
         PFQuery *query = [PFQuery queryWithClassName:@P_IAP_WORKOUTS];
         [query whereKey:@"RoutineName" equalTo:workoutObj.routineName];
         [query whereKey:@"WorkoutName" equalTo:workoutObj.workoutName];
         [query whereKey:@"ExerciseName" equalTo:workoutObj.exerciseName];
         [query setLimit: 1];
         [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
         if (task.error) {
         NSLog(@"Error: %@", task.error);
         return task;
         }
         PFObject *rObj;
         NSLog(@"number of elements found %lu", (long)[task.result count]);
         if ([task.result count] == 0) {
         NSLog(@"Creating a new workout..");
         rObj = [PFObject objectWithClassName:@P_IAP_WORKOUTS];
         rObj[@"CreatedDate"] = workoutObj.createdDate;
         rObj[@"ExerciseName"] = workoutObj.exerciseName;
         rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
         rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
         rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
         rObj[@"IsActive"] = @YES;
         rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
         rObj[@"Muscle"] = workoutObj.muscle;
         rObj[@"Reps"]  = workoutObj.reps;
         rObj[@"RestTimer"]  = workoutObj.restTimer;
         rObj[@"RoutineName"] = workoutObj.routineName;
         rObj[@"Sets"] = workoutObj.sets;
         rObj[@"SuperSetWith"] = workoutObj.superSetWith;
         rObj[@"WorkoutName"] = workoutObj.workoutName;
         rObj[@"RoutineBundleId"] = bundleId;
         rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
         rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
         [rObj saveEventually];
         } else {
         NSLog(@"updating old workout");
         rObj = [task.result objectAtIndex:0];
         rObj[@"CreatedDate"] = workoutObj.createdDate;
         rObj[@"ExerciseName"] = workoutObj.exerciseName;
         rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
         rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
         rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
         rObj[@"IsActive"] = @YES;
         rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
         rObj[@"Muscle"] = workoutObj.muscle;
         rObj[@"Reps"]  = workoutObj.reps;
         rObj[@"RestTimer"]  = workoutObj.restTimer;
         rObj[@"RoutineName"] = workoutObj.routineName;
         rObj[@"Sets"] = workoutObj.sets;
         rObj[@"SuperSetWith"] = workoutObj.superSetWith;
         rObj[@"WorkoutName"] = workoutObj.workoutName;
         rObj[@"RoutineBundleId"] = bundleId;
         rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
         rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
         [rObj saveEventually];
         
         }
         NSLog(@"Workout Synced.. %@", workoutObj.workoutName);
         return task.result;
         }];
        }
    }
    
}
@end
