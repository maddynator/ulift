//
//  WorkoutListView.m
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "RoutineList.h"
#import "CreateWorkoutDay.h"
#import "CreateRoutineVC.h"
#import "RoutineDetailsTVC.h"

#define MODIFY_MSG  "Modifying downloaded workout routines with free version is restricted. Please upgrade to Premium or Power Routine Package to modify workouts routines."
#define CREATE_MSG  "Creating more than one workout routines with free version is restricted. Please upgrade to Premium or Power Routine Package to modify workouts routines."

@interface RoutineList () {
    NSString *routineName, *routineColor;
    NSArray *routineArray;
    RoutineMetaInfo *selectedRoutine;
    
}

@end

@implementation RoutineList
@synthesize tableView, coachMarksView;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    routineArray = [[NSArray alloc] init];
    self.title = @"Create Routines";
//    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconMenu"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    self.navigationController.navigationBar.translucent = YES;
    
    UIBarButtonItem *exportBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(createNewWorkout)];
    UIBarButtonItem *storeBtn = [[UIBarButtonItem alloc] initWithTitle:@"Store" style:UIBarButtonItemStylePlain target:self action:@selector(openAppStore)];
    
    self.navigationItem.rightBarButtonItems = @[exportBtn, storeBtn];

    [self getWorkouts];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
   
    
}

-(void) viewDidAppear:(BOOL)animated {
    [self getWorkouts];
    selectedRoutine = nil;
    [Utilities syncWorkoutInfo:self];
}
-(void) getWorkouts {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    routineArray = [RoutineMetaInfo MR_findAllSortedBy:@"routineCreatedDate" ascending:NO inContext:localContext];
    
    NSLog(@"unumber of workouts availabe are %lu", (long) [routineArray count]);
    [tableView reloadData];
    if ([routineArray count] > 0) {
        [self showCoachMarks];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/* Description:
 * Before we create a new workout, we check how many workouts user has locally. Then we iterate over each workout bundle id to figure out if it is a user custom workout or downloaded workout. If there are more than 1 custom workout, then we check for premium package.
*/
-(void) createNewWorkout {
    int count = 0;
    NSString *bundleString = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
    for (RoutineMetaInfo *routine in routineArray) {
        NSLog(@"bundle id %@ %@", bundleString, routine.routineBundleId);
        if ([routine.routineBundleId containsString:bundleString]) {
            count++;
        }
    }
    NSLog(@"how many workouts are there %d", count);
    if (count > 0) {
        if ([Utilities showPowerRoutinePackage]) {
            [self showPurchasePopUp:@CREATE_MSG];
        } else {
            [self performSegueWithIdentifier:@"createRoutineSegue" sender:self];
        }
    } else {
        [self performSegueWithIdentifier:@"createRoutineSegue" sender:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [routineArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];

    UILabel *routineNm = (UILabel *)[cell viewWithTag:100];
    //UILabel *routineSummary = (UILabel *)[cell viewWithTag:101];
    
    RoutineMetaInfo *routine = [routineArray objectAtIndex:indexPath.row];
    routineNm.text = [routine.routineName uppercaseString];
  //  routineSummary.text = routine.routineSummary;
    //NSLog(@"routine bundle id is %@", routine.routineBundleId);
    
    [routineNm setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
   // [routineSummary setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    
    routineNm.textColor = [Utilities colorForString:routine.routineColor];//[UIColor whiteColor];
    cell.tintColor = [Utilities colorForString:routine.routineColor];// routineSummary.textColor =[UIColor whiteColor];

    cell.backgroundColor = [UIColor whiteColor];//[Utilities colorForString:routine.routineColor];
    
    //configure right buttons
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]],
                          [MGSwipeButton buttonWithTitle:@"Edit" backgroundColor:[UIColor grayColor]]];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.delegate = self;
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    [cell.contentView addSubview:lineView];

    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"Accessory clicked... %lu", (long)indexPath.row);
    RoutineMetaInfo *temp = [routineArray objectAtIndex:indexPath.row];
    routineName = temp.routineName;
    routineColor = temp.routineColor;

    [self performSegueWithIdentifier:@"routineDetailSegue" sender:self];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
    RoutineMetaInfo *temp = [routineArray objectAtIndex:indexPath.row];
    routineName = temp.routineName;
    routineColor = temp.routineColor;
    selectedRoutine = temp;
    [self performSegueWithIdentifier:@"workoutEditSegue" sender:self];
    
}


-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    NSIndexPath * path = [self.tableView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSLog(@"delete pressed");
        
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert addButton:@"Yes" actionBlock:^{
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
            //TODO: we need to check if user has performed this workout. We have to delete all that or dont allow user to delete this
            // we need to mark this routine as deleted on parse too..
            RoutineMetaInfo *data = [routineArray objectAtIndex:path.row];
            
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", data.routineName];
            NSArray *workoutArray = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
            
            NSMutableArray * unique  = [NSMutableArray array];
            NSMutableSet * processed = [NSMutableSet set];
            // we have a routine with multiple workout days. but they way we store is that each exercise in workout day is stored independently.. so when we are deleteing all exercises in workout, we get all entries based on routine name, workout name and userId. So dont call each one separately.
            for (UserWorkout *data in workoutArray) {
                NSString *string = data.workoutName;
                if ([data.syncedState boolValue] == true) {
                    if ([processed containsObject:string] == NO) {
                        [unique addObject:string];
                        [processed addObject:string];
                        [Utilities markWorkoutAsInActive:data.routineName workoutName:data.workoutName];
                    }
                }
            }
            
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                for (UserWorkout *tempData in workoutArray) {
                    [tempData MR_deleteEntityInContext:localContext];
                }
            }];
            if ([data.syncedState boolValue] == true) {
                NSLog(@"we have synced this routkne.. so we have to mark it as inactive on parse.");
                [Utilities markRoutineAsInActive:data.routineName];
            }

            [data MR_deleteEntityInContext:localContext];
            [localContext MR_saveToPersistentStoreAndWait];
            
            [self getWorkouts];
        }];
        [alert showWarning:self title:@"Delete routine?" subTitle:@"Are you sure you want to delete this routine? All workouts and exercises will be deleted." closeButtonTitle:@"No" duration:0.0f];
        return NO; //Don't autohide to improve delete expansion animation
    } else if (direction == MGSwipeDirectionRightToLeft && index == 1) {
        RoutineMetaInfo *temp = [routineArray objectAtIndex:path.row];
        NSString *tempRBid = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
        NSLog(@"ROUTINE IS %@, %@", temp.routineBundleId, tempRBid);
        // check if it is a downloaded workout and using free version. No edits allowed on those...        
        if (![temp.routineBundleId containsString:tempRBid]) {
            if ([Utilities showPowerRoutinePackage]) {
                [self showPurchasePopUp:@MODIFY_MSG];
            } else {
                selectedRoutine = temp;
                [self performSegueWithIdentifier:@"createRoutineSegue" sender:self];
            }
        } else {
            selectedRoutine = temp;
            [self performSegueWithIdentifier:@"createRoutineSegue" sender:self];
        }
//        if ([Utilities isEditingAllowed:temp.routineName] == true) {
//        } else {
//            SCLAlertView *alert = [[SCLAlertView alloc] init];
//            [alert showError:self title:@"Edits Not Allowed." subTitle:@"Downloaded workouts cannot be edited" closeButtonTitle:@"Ok" duration:0.0f];
//        }
        
        return NO; //Don't autohide to improve delete expansion animation

    }
    
    return YES;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No routines available yet."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Please click + (Top Right) to create a new routine or download a routine from our STORE.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

-(void) openAppStore {
    self.tabBarController.selectedIndex = 3;
    //[self performSegueWithIdentifier:@"inAppStoreSegue" sender:self];
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"workoutEditSegue"]) {
        CreateWorkoutDay *destVC = segue.destinationViewController;
        destVC.routineName = routineName;
        destVC.routineBundleId = selectedRoutine.routineBundleId;
        destVC.routineColor = [Utilities colorForString:routineColor];
    } else if ([segue.identifier isEqualToString:@"createRoutineSegue"]) {
        CreateRoutineVC *destVC = segue.destinationViewController;
        destVC.routine = selectedRoutine;
    } else if ([segue.identifier isEqualToString:@"routineDetailSegue"]) {
        RoutineDetailsTVC *dest = segue.destinationViewController;
        dest.isLocalStored = true;
        dest.routineNameLocal = routineName;
    }
}

-(void) showPurchasePopUp: (NSString *) subtitle {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ROUTINE PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ROUTINE_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:subtitle closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

-(void) showCoachMarks {
    
    bool showHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownRoutineListHint"];
    
    if ([Utilities isHintEnabled] || showHint == false) {
        float width = self.view.frame.size.width;
        
        //        CGRect coachmark1 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame) + 10, 40, 38);
//        CGRect coachmark2 = CGRectMake(width - 100, 0, 100, 40);
//        CGRect coachmark3 = CGRectMake(0, -30,  0, 0);
        CGRect coachmark4 = CGRectMake(0, 0, width, 60);
        
        // Setup coach marks
        NSArray *coachMarks = @[
                                //                                @{
                                //                                    @"rect": [NSValue valueWithCGRect:coachmark1],
                                //                                    @"caption": @"Click detail button for workout summary.",
                                //                                    },
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark2],
//                                    @"caption": @"Move group up and down",
//                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
//                                    @"showArrow":[NSNumber numberWithBool:YES]
//                                    },
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark3],
//                                    @"caption": @"\n\n\n\n\n\n\n\n\nClick 'EDIT' to combine exercises into supersets, tri-sets, giant sets or circuits.",
//                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_LEFT],
//                                    @"showArrow":[NSNumber numberWithBool:NO]
//                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark4],
                                    @"caption": @"Slide LEFT on routine to EDIT or DELETE.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    }
                                
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownRoutineListHint"]);
    //    aleadyShowingHint = false;
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownRoutineListHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
