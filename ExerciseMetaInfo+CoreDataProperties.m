//
//  ExerciseMetaInfo+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseMetaInfo+CoreDataProperties.h"

@implementation ExerciseMetaInfo (CoreDataProperties)

+ (NSFetchRequest<ExerciseMetaInfo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExerciseMetaInfo"];
}

@dynamic comment;
@dynamic exerciseName;
@dynamic goalAchieveDate;
@dynamic goalWeightOrRep;
@dynamic handAngle;
@dynamic maxDate;
@dynamic maxRep;
@dynamic maxWeight;
@dynamic muscle;
@dynamic seatHeight;
@dynamic syncedState;

@end
