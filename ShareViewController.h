//
//  ShareViewController.h
//  gyminutes
//
//  Created by Mayank Verma on 6/24/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
@interface ShareViewController : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, retain)  IBOutlet UICollectionView *_collectionView;
@property (nonatomic, retain) NSString *date;
@end
