//
//  ExerciseList+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExerciseList (CoreDataProperties)

+ (NSFetchRequest<ExerciseList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *equipment;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSNumber *exercisePerformed;
@property (nullable, nonatomic, copy) NSString *expLevel;
@property (nullable, nonatomic, copy) NSNumber *isUserCreated;
@property (nullable, nonatomic, copy) NSString *majorMuscle;
@property (nullable, nonatomic, copy) NSString *mechanics;
@property (nullable, nonatomic, copy) NSString *muscle;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSString *type;

@end

NS_ASSUME_NONNULL_END
