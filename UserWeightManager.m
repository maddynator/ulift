//
//  UserWeightManager.m
//  gyminutes
//
//  Created by Mayank Verma on 7/6/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "UserWeightManager.h"
#import "commons.h"

float userWeight = -1;

@implementation UserWeightManager

+(UserWeightManager *)sharedInstance {
    
    static dispatch_once_t pred = 0;
    static UserWeightManager *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[UserWeightManager alloc] init];
    });
    return instance;
    
}

+(float) getLastUserWeight {
    if (userWeight > 0)
        return userWeight;
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    DailyStats *weight = [DailyStats MR_findFirstOrderedByAttribute:@"date" ascending:YES inContext:localContext];
    if (weight == nil) {
        UserProfile *profileWt = [UserProfile MR_findFirstInContext:localContext];
        userWeight = [profileWt.weight floatValue];
    } else {
        userWeight = [weight.weight floatValue];
    }
    
    return userWeight;
}
@end
