//
//  RecordWorkout.m
//  EBCardCollectionViewLayout
//
//  Created by Mayank Verma on 3/3/16.
//  Copyright © 2016 Ezequiel A Becerra. All rights reserved.
//

#import "RecWorkout.h"
#import "ExerciseHistoryTVC.h"
#import "ExerciseSummary.h"


#define WIDTH_FACTOR    40

@interface RecWorkout () {
    UICollectionViewFlowLayout *flowLayout;
    UIView *historyView;
    int selectedExercise, totalEx, selectedIndex, groupNumber;
    BOOL scrollBack;
    NSMutableArray *isFlipped, *histView;
    NSMutableArray *exerciseGroups, *setsArray;
    NSArray *superSetExercises;
    UILabel *footer;
    
    UIToolbar* addOnKeyboard;
    NSArray *weightsLbs, *weightsKgs, *weightLbVal, *weightKgVal, *barbelWtLbs, *barbellWtKgs;
    NSMutableArray *weightLbsArray, *weightKgsArray;
    float totalWeight;
    float barbellWeight;
    TNRadioButtonGroup *barbellGroup;
    float lbORkg;
    UITableView *myBarbelTableView;
    UILabel *calculatedWeightLbl;
    int seatHeight, handAngle, timerValueToCountDown;
    BOOL warmupFlag, isDropSetVisible, updateField;
    NSArray *exerciseList;
    ExerciseMetaInfo *exMetaInfo;
    NSManagedObjectContext *localContext;
    float maxWeight;
    int maxRep;
    NSString *historyStr;
    NSArray *previousWorkoutSets;
    NSArray *exerciseSets;
    UIButton *unMarkDropSet, *markDropSet;
    UIView *additionOptionView;
    UILabel *setsInfo;
    MZTimerLabel *timerBWEx, *timerBWSts;
    SMPageControl *horizontalPageControl, *verticalPageControl;
    UIButton *seatBtn, *handBtn, *timerBtn, *commentBtn;
    NSString *userComment;
    bool isPersonalRecord;
    NSArray *headerStrings;
    
    // warmupset stuff
    UIView *customSetsListView;
    float lastWorkoutFinalWeight, warmupWeight;
    int warmupRep, warmupSets;
    JVFloatLabeledTextView *workingWeightTextFied, *warmupMaxRepTV, *warmupMaxSetTV;
    int userCustomRepsForSet;
}
@end

@implementation RecWorkout
@synthesize weightText, repText, saveBtn, timer, repPlus, repMinus, weightMinus, weightPlus, cllearBtn, prevExBtn, nextExBtn, exerciseObj, segmentControl, coachMarksView, exerciseGroup, exerciseNumInGroup, repsPerSet;

- (void)viewDidLoad
{
    [super viewDidLoad];

    selectedExercise = -1;
    groupNumber = -1;
    
    // these are for warmups sets related
    lastWorkoutFinalWeight = 0;
    warmupRep = 0;
    warmupSets = 0;
    warmupWeight = 0;
    
    scrollBack = false;
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    UIButton *back = [[UIButton alloc] initWithFrame:CGRectMake(15, 30, 30, 30)];
    [back addTarget:self action:@selector(goBackButton:) forControlEvents:UIControlEventTouchUpInside];
    [back setImage:[UIImage imageNamed:@"IconEnd"] forState:UIControlStateNormal];
    [back setImageEdgeInsets:UIEdgeInsetsMake(7,7,7,7)];
    [back setContentMode:UIViewContentModeScaleAspectFit];
    
    
    [self.view addSubview:back];
    
    timer = [[MZTimerLabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/2 - 35, 20, 70, 50)];
    timer.delegate = self;
    // this is set so that the timer starts with 0.
    timerValueToCountDown = 0;
    [timer setCountDownTime:timerValueToCountDown];
    timer.resetTimerAfterFinish = NO; //IMPORTANT, if you needs custom text with finished, please do not set resetTimerAfterFinish to YES.
    timer.tag = 100;
    timer.timeFormat = @"mm:ss";//@"mm:ss SS";
    timer.timeLabel.backgroundColor = [UIColor clearColor];
    timer.timeLabel.textColor = [UIColor whiteColor];
    timer.timeLabel.textAlignment = NSTextAlignmentCenter;
    timer.timerType = MZTimerLabelTypeTimer;
    timer.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
    
    [self.view addSubview:timer];
    
    UIButton *addOptions = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame) - 50, 30, 30, 30)];
    [addOptions setImage:[UIImage imageNamed:@"IconMoreOptions"] forState:UIControlStateNormal];
    [addOptions addTarget:self action:@selector(showMoreOptionMenu) forControlEvents:UIControlEventTouchUpInside];
    [addOptions setImageEdgeInsets:UIEdgeInsetsMake(5,5,5,5)];
    [addOptions setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.view addSubview:addOptions];
    
    UIButton *warmupSetsOptions = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMinX(addOptions.frame) - 50, 30, 30, 30)];
    //[warmupRepsOptions setImage:[UIImage imageNamed:@"IconMoreOptions"] forState:UIControlStateNormal];
    [warmupSetsOptions setTitle:@"W" forState:UIControlStateNormal];
    [warmupSetsOptions setBackgroundColor:[UIColor whiteColor]];
    [warmupSetsOptions setTitleColor:FlatBlack forState:UIControlStateNormal];
    [warmupSetsOptions addTarget:self action:@selector(warmupSetsOptionMenu) forControlEvents:UIControlEventTouchUpInside];
    [warmupSetsOptions setImageEdgeInsets:UIEdgeInsetsMake(5,5,5,5)];
    [warmupSetsOptions.layer setCornerRadius:15];
    [warmupSetsOptions setContentMode:UIViewContentModeScaleAspectFit];
    
    [self.view addSubview:warmupSetsOptions];
    

    flowLayout = [[UICollectionViewFlowLayout alloc] init];
    [flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [_collectionView setPagingEnabled:YES];
    [_collectionView setCollectionViewLayout:flowLayout];
    
    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timer.frame) + 10, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetMaxY(timer.frame) - 40) collectionViewLayout:flowLayout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.pagingEnabled = YES;
    
    // the prefetching was causing the issue of skipping cells. We need to make sure we dont prefetch as this causes the
    NSLog(@"Current system version is %@", [[UIDevice currentDevice] systemVersion]);
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))
        _collectionView.prefetchingEnabled = NO;
    else
        NSLog(@"Skipping prefetching as old version");
    
    [self.view addSubview:_collectionView];
    self.view.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:self.view.frame andColors:@[[Utilities getAppColor], FlatYellow, [Utilities getAppColor]]];//@[FlatOrange, FlatYellow, FlatOrange]
    
    // [UIColor colorWithGradientStyle:UIGradientStyleRadial withFrame:self.view.frame andColors:@[FlatYellow, FlatOrange]];
    _collectionView.backgroundColor = [UIColor clearColor];
    
    setsArray = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view, typically from a nib.
    addOnKeyboard = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    addOnKeyboard.items = [NSArray arrayWithObjects:
                           //  [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Plate Calculator" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           //                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           nil];
    
    
    addOnKeyboard.tintColor = FlatOrangeDark;
    
    headerStrings = @[@"THATS AWESOME", @"YOU ROCK", @"LIGHT WEIGHT BABY", @"HERCULES IS HERE", @"THATS IMPRESSIVE", @"YEEEHAAA!!", @"FANTASTIC", @"HEY SUPER-HUMAN", @"OH MY....", @"YOU DID IT", @"NAILED IT", @"PERFECT", @"MAKING GAINS", @"YOU'RE A BEAST", @"CONGRATS CHAMP", @"KABOOOM", @"NICE WORK", @"MARVELOUS", @"PERFECT SET", @"YOU MOTIVATE ME", @"THATS PROGRESS", @"HURRAY", @"I'M PROUD", @"I'M IMPRESSED", @"TIME TO FLEX", @"TAKE A BOW!", @"WONDERFUL", @"YOU'RE A CHAMP", @"UNBELIEVABLE", @"YOU'VE OUTDONE YOURSELF", @"AMAZING EFFORT", @"THAT'S A CLASS ACT", @"YOU'RE TOPS!", @"5 STAR PERFORMANCE", @"MAGNIFICENT SET", @"SUPERB SET", @"TWO THUMBS UP!", @"TERRIFIC SET", @"BRAVO!", @"WOW!", @"NICE GOING"];
    
    weightsLbs = [[NSArray alloc] initWithObjects: @"45 Lbs", @"35 Lbs", @"25 Lbs", @"10 Lbs", @"5 Lbs", @"2.5 Lbs", nil];
    weightsKgs = [[NSArray alloc] initWithObjects:@"25 Kgs", @"20 Kgs", @"15 Kgs", @"10 Kgs", @"5 Kgs", @"2 Kgs", @"1.5 Kgs", @"1.0 Kgs", @"0.5 Kgs", nil];
    
    weightLbVal = [[NSArray alloc] initWithObjects:
                   [NSNumber numberWithFloat:45],
                   [NSNumber numberWithFloat:35],
                   [NSNumber numberWithFloat:25],
                   [NSNumber numberWithFloat:10],
                   [NSNumber numberWithFloat:5.0],
                   [NSNumber numberWithFloat:2.5],
                   nil];
    
    weightKgVal = [[NSArray alloc] initWithObjects:
                   [NSNumber numberWithFloat:25],
                   [NSNumber numberWithFloat:20],
                   [NSNumber numberWithFloat:15],
                   [NSNumber numberWithFloat:10],
                   [NSNumber numberWithFloat:5],
                   [NSNumber numberWithFloat:2],
                   [NSNumber numberWithFloat:1.5],
                   [NSNumber numberWithFloat:1.0],
                   [NSNumber numberWithFloat:.5],
                   nil];
    
    
    weightKgsArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0],[NSNumber numberWithFloat:0],[NSNumber numberWithFloat:0], nil];
    
    barbelWtLbs = [[NSArray alloc] initWithObjects:[NSNumber numberWithFloat:25], [NSNumber numberWithFloat:35], [NSNumber numberWithFloat:45], nil];
    barbellWtKgs = [[NSArray alloc] initWithObjects:[NSNumber numberWithFloat:10], [NSNumber numberWithFloat:15], [NSNumber numberWithFloat:20], nil];
    totalWeight = 0;
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tap];
    
    footer  = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_collectionView.frame), CGRectGetWidth(self.view.frame), 30)];
    footer.textAlignment = NSTextAlignmentCenter;
    footer.textColor = [UIColor whiteColor];
    footer.font = [UIFont fontWithName:@HAL_THIN_FONT size:20];
    [self.view addSubview:footer];
    
    self.navigationController.navigationBarHidden = YES;
    exerciseGroups = [[NSMutableArray alloc] init];

    horizontalPageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timer.frame) - 15, CGRectGetWidth(self.view.frame), 30)];
    horizontalPageControl.alignment = SMPageControlAlignmentCenter;
    horizontalPageControl.hidesForSinglePage = YES;
    horizontalPageControl.pageIndicatorTintColor = FlatBlack;
    horizontalPageControl.tapBehavior = SMPageControlTapBehaviorStep;
    horizontalPageControl.enabled = false;
    [self.view addSubview:horizontalPageControl];

}

-(void) viewDidAppear:(BOOL)animated {
    // we need to do this in viewDidAppear because user can change from LB to KG in between and when we come back, we need to change text from lb to kg and or vice versa
    isPersonalRecord = false;
    [self showCoachMarks];
    
    [exerciseGroups removeAllObjects];
    self.navigationController.navigationBarHidden = YES;

    lbORkg = [Utilities isKgs];
    
    exerciseList = [Utilities getAllExerciseForDate:_date];
    
    totalEx = (int)[exerciseList count];
    
    NSArray *allExerciseInWorkout = exerciseList;
    NSLog(@"COUNT IS %lu", (unsigned long)[allExerciseInWorkout count]);
    NSMutableArray * unique = [NSMutableArray array];
    NSMutableSet * processedGroup = [NSMutableSet set];
    
    
    for (WorkoutList *data in allExerciseInWorkout) {
        NSNumber *groupString = data.exerciseGroup;
        NSLog(@"** GROUP STE %@", groupString);
        if ([processedGroup containsObject:groupString] == NO) {
            [unique addObject:groupString];
            [processedGroup addObject:data.exerciseGroup];
            NSLog(@"Processed Group number %@", groupString);
        }
    }
    
    for (int i = 0; i < [processedGroup count]; i++) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %d", i];
        NSArray *allExInWorkout = [exerciseList filteredArrayUsingPredicate:predicate];
        NSLog(@"AllExInWorkout %lu", (unsigned long)[allExInWorkout count]);
        NSMutableArray *ex = [[NSMutableArray alloc] init];
        for (WorkoutList *item in allExInWorkout) {
            [ex addObject:item];
        }
        [exerciseGroups addObject:ex];
        NSLog(@"exGroup %d, exNuminGroup count:%lu", i, (unsigned long)[[exerciseGroups objectAtIndex:i] count]);
    }
    
    
    // NSLog(@"total ex %d %@", totalEx, exerciseGroups);
    
    
    //    timerValueToCountDown = [exerciseObj.restSuggested intValue];
    //    NSLog(@"timerValueToCountDown: %d", timerValueToCountDown);
    //    NSString *deviceModel = [Utilities getIphoneName];
    //
    //    [self reloadAllData];
    //    warmupFlag = false;
    //
    //    [self reloadHeaderButtons];
    
    localContext = [NSManagedObjectContext MR_defaultContext];
    [_collectionView reloadData];
    horizontalPageControl.numberOfPages = [exerciseGroups count];
    
    
    // this is to make sure everythings proper exercise card is selected when user clicks the exercise.
    NSIndexPath *groupIndexPath = [NSIndexPath indexPathForRow:0 inSection:[exerciseGroup intValue]];
    [_collectionView scrollToItemAtIndexPath:groupIndexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
    //isPersonalRecord = true;
    //[self showRestTimerBetweenSets:CNPPopupStyleActionSheet];
    //[self showNextExerciseMenuWithCountDownTimer: CNPPopupStyleCentered];
}

-(IBAction) quickAddExercise:(id)sender {
    SCLAlertView *addExAlert = [[SCLAlertView alloc] init];
    
    [addExAlert addButton:@"Add To Today Workout" actionBlock:^{
        if ([Utilities showWorkoutPackage]) {
            [self showPurchasePopUp:@"Addition exercises cannot be added to a workout in Free Version."];
        } else {
            NSLog(@"moving now...");
            [self performSegueWithIdentifier:@"addTempExerciseSegue" sender:self];
        }
    }];
    
    [addExAlert addButton:@"Close" actionBlock:^{
    }];
    [addExAlert showInfo:self title:@"Additional Exercise" subTitle:@"Additional exercise will be added to today workout only and not the routine." closeButtonTitle:nil duration:0.0f];
    addExAlert.shouldDismissOnTapOutside = false;
    addExAlert.backgroundType = Blur;
    
}
-(IBAction)viewHistory:(id)sender {
    [self performSegueWithIdentifier:@"exerciseHistory" sender:self];
}


-(IBAction)goBackButton:(id)sender {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"PAUSE" actionBlock:^{
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    
    [alert addButton:@"WORKOUT COMPLETE" actionBlock:^{
        [timer pause];
        [timer reset];
        [Utilities deleteLocalNotificationWithBody:@REST_TIMER_OVER_TEXT];
        [self dismissViewControllerAnimated:YES completion:nil];

    }];
    
    [alert showWarning:self title:@"FINISH WORKOUT?" subTitle:@"Your workout is not complete. Do you still want to exit?" closeButtonTitle:@"CONTINUE" duration:0.0f];

}

-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (collectionView == _collectionView)
        return [exerciseGroups count];
    else
        return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _collectionView)
        return 1;
    else {
        NSIndexPath *indexPathSuper = nil;
        for (UICollectionViewCell *cell in [_collectionView visibleCells]) {
            indexPathSuper = [_collectionView indexPathForCell:cell];
            NSLog(@"%lu, %lu",indexPathSuper.section, indexPathSuper.row);
        }
        NSLog(@"section number is %d %lu", groupNumber, (unsigned long)[[exerciseGroups objectAtIndex:groupNumber] count]);
        return [[exerciseGroups objectAtIndex:groupNumber] count];
    }
}


// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    lastWorkoutFinalWeight = 0;
    if (collectionView == _collectionView) {
        UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        cell.backgroundColor=[UIColor blackColor];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        exerciseCards = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame) - 40, CGRectGetHeight(cell.frame)) collectionViewLayout:layout];
        [layout setScrollDirection:UICollectionViewScrollDirectionVertical];
        [exerciseCards setPagingEnabled:YES];
        [exerciseCards setCollectionViewLayout:layout];
        exerciseCards.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:exerciseCards.frame andColors:@[[Utilities getAppColor], FlatYellow, [Utilities getAppColor]]];
        [exerciseCards setDataSource:self];
        [exerciseCards setDelegate:self];
        [exerciseCards registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"exCardsIdentifier"];
        exerciseCards.pagingEnabled = YES;
        exerciseCards.showsVerticalScrollIndicator = NO;
        // the prefetching was causing the issue of skipping cells. We need to make sure we dont prefetch as this causes the
        // the prefetching was causing the issue of skipping cells. We need to make sure we dont prefetch as this causes the
        NSLog(@"Current system version is %@", [[UIDevice currentDevice] systemVersion]);
        if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"10.0"))
            exerciseCards.prefetchingEnabled = NO;
        else
            NSLog(@"Skipping prefetching as old version");


        [cell.contentView addSubview:exerciseCards];
        NSLog(@"section is %lu", (long)indexPath.section);
        groupNumber = (int) indexPath.section;
        cell.layer.cornerRadius = 8;
        exerciseCards.layer.cornerRadius = 8;
        superSetExercises = [exerciseGroups objectAtIndex:indexPath.section];
        horizontalPageControl.currentPage = indexPath.section;
        
        if ([superSetExercises count] > 1) {
            verticalPageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(-110, CGRectGetHeight(_collectionView.frame)/2, 200, 20)];
            verticalPageControl.numberOfPages = [superSetExercises count];
            verticalPageControl.alignment = SMPageControlAlignmentCenter;
            verticalPageControl.hidesForSinglePage = YES;
            verticalPageControl.pageIndicatorTintColor = FlatBlack;
//            verticalPageControl.currentPageIndicatorTintColor = ;
//            verticalPageControl.backgroundColor = [UIColor blackColor];
            verticalPageControl.transform = CGAffineTransformMakeRotation(M_PI_2);
            [cell.contentView addSubview:verticalPageControl];
//            [self showCoachMarks];
        }
        
        // we need to do this because now we are loading the specific section when user clicks the exercise. So we need to update this otherwise if user adds new exercise, it will move to old index that was passed to us
        exerciseGroup = [NSNumber numberWithInt:(int)indexPath.section];
        
        return cell;
    } else {
        UICollectionViewCell *cell = (UICollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"exCardsIdentifier" forIndexPath:indexPath];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        [self createCellView:cell.contentView exGroup:groupNumber exNumInGroup:(int)indexPath.row];
        UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                       initWithTarget:self
                                       action:@selector(dismissKeyboard)];
        tap.cancelsTouchesInView = NO;
        [cell.contentView addGestureRecognizer:tap];
        if ([superSetExercises count] > 1)
            verticalPageControl.currentPage = indexPath.row;
        //cell.backgroundColor = FlatWatermelonDark;
//        cell.layer.cornerRadius = 20;
        NSLog(@"Exercise Card created succesffuly %d %d", groupNumber, (int)indexPath.row);
        return cell;
        
    }

}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (collectionView == _collectionView)
        return CGSizeMake(CGRectGetWidth(collectionView.frame) - WIDTH_FACTOR, CGRectGetHeight(collectionView.frame));
    else
        return CGSizeMake(CGRectGetWidth(exerciseCards.frame) , CGRectGetHeight(exerciseCards.frame));
}

- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == _collectionView)
        return WIDTH_FACTOR;
    else
        return 0;
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    // we need to check that because sometimes user come back and dont scroll but then want to add more sets. So we reload the view once scroll is complete.
    
    /* This code finds out the visible cell in view and return the indexPath of it. We dont need it now but may need to enable it.
//    CGRect visibleRect = (CGRect){.origin = _collectionView.contentOffset, .size = _collectionView.bounds.size};
//    CGPoint visiblePoint = CGPointMake(CGRectGetMidX(visibleRect), CGRectGetMidY(visibleRect));
//    NSIndexPath *visibleIndexPath = [_collectionView indexPathForItemAtPoint:visiblePoint];
//    NSLog(@"Index path: %ld %ld",(long)visibleIndexPath.section, (long)visibleIndexPath.row);
//    horizontalPageControl.currentPage = visibleIndexPath.section;
//    verticalPageControl.currentPage = visibleIndexPath.row;
    */
    if (scrollView == _collectionView) {
        [_collectionView reloadData];
    }
    else {
        [exerciseCards reloadData];
    }
}
-(BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (collectionView == _collectionView) {
        NSInteger numberOfCells = self.view.frame.size.width / (CGRectGetWidth(self.view.frame) - WIDTH_FACTOR);
        NSInteger edgeInsets = (self.view.frame.size.width - (numberOfCells * (CGRectGetWidth(self.view.frame) - WIDTH_FACTOR))) / (numberOfCells + 1);
        return UIEdgeInsetsMake(0, edgeInsets, 0, edgeInsets);
        
    } else {
        return UIEdgeInsetsZero;
    }
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"do nothing ");
    [self dismissKeyboard];
}

-(void) collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //    if (collectionView == _collectionView) {
    //            NSLog(@"did end displaying main");
    //        [cellSetsView reloadData];
    //    } else {
    //            NSLog(@"did end displaying child");
    //    }
}

-(void)LoadNextExerciseCell {
    NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
    NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
    if ([_exerciseNumber intValue] ==[exerciseList count] - 1) {
        NSLog(@"NOW WE SHOW WORKOUT SUMMARY");
        return;
    }
    NSLog(@"current index %ld %d", (long)currentItem.section, (int)[exerciseList count]);
    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:currentItem.item + 1 inSection:currentItem.section];
    [_collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}

-(void) LoadPrevExerciseCell {
    NSArray *visibleItems = [_collectionView indexPathsForVisibleItems];
    NSIndexPath *currentItem = [visibleItems objectAtIndex:0];
    
    NSIndexPath *prevItem = [NSIndexPath indexPathForItem:currentItem.item - 1 inSection:currentItem.section];
    [_collectionView scrollToItemAtIndexPath:prevItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
}

-(void) createCellView: (UIView *) view exGroup:(int) exGrp exNumInGroup:(int) exNumInGrp {
        view.backgroundColor = FlatWhite;
        //        view.layer.cornerRadius = 8;
        float maxY = 0;
    
    
        UILabel *exName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(view.frame) - 50, 30)];
//        exerciseObj = [superSetExercises objectAtIndex:index];
        exerciseObj = [[exerciseGroups objectAtIndex:groupNumber] objectAtIndex:exNumInGrp];
        _routineName = exerciseObj.routiineName;
        _workoutName = exerciseObj.workoutName;
        _exerciseNumber = exerciseObj.exerciseNumber;
    
        exName.text = [NSString stringWithFormat:@"  %@", exerciseObj.exerciseName];
        exName.backgroundColor = [UIColor whiteColor];
        exName.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
        exName.textColor = FlatBlueDark;
        // we will be adding to this view soon
        UIView *exExtra = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(exName.frame), CGRectGetWidth(view.frame) - 50, 30)];
        exExtra.backgroundColor = [UIColor whiteColor];
        UILabel *exType = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, CGRectGetWidth(exExtra.frame)/2, 26)];
        //        exType.layer.cornerRadius = 13;
        //        exType.layer.borderColor = FlatMintDark.CGColor;
        //        exType.layer.borderWidth = 2;
        exType.font = [UIFont fontWithName:@HAL_BOLD_FONT size:14];
        exType.textColor = FlatGreenDark;
        
    
        int exGroupNum = [exerciseObj.exerciseGroup intValue];
        int exCount = (int)[[exerciseGroups objectAtIndex:exGroupNum] count];
        if (exCount == 2) {
            exType.text = [NSString stringWithFormat:@"SUPER SET %d/%d", [exerciseObj.exerciseNumInGroup intValue]+ 1, exCount];
            [exExtra addSubview:exType];
        } else if (exCount == 3) {
            exType.text = [NSString stringWithFormat:@"TRI SET %d/%d", [exerciseObj.exerciseNumInGroup intValue]+ 1, exCount];
            [exExtra addSubview:exType];
        } else if (exCount == 2) {
            exType.text = [NSString stringWithFormat:@"GIANT SET %d/%d", [exerciseObj.exerciseNumInGroup intValue]+ 1, exCount];
            [exExtra addSubview:exType];
        }
        
        if ([exerciseGroups count] == 1) {
            exType.text = [NSString stringWithFormat:@"CIRCUIT %d/%d", [exerciseObj.exerciseNumInGroup intValue]+ 1, exCount];
            [exExtra addSubview:exType];
        }
        
        exerciseSets = nil;
        exerciseSets = [Utilities getSetForExerciseOnDate:exerciseObj.exerciseName date:_date];
        // as this is for the first set.
    NSLog(@"Mayank: createCell is called");
        NSArray *restPerSet = [exerciseObj.restPerSet componentsSeparatedByString:@","];
        if (restPerSet.count > 0 && restPerSet.count > exerciseSets.count)
            timerValueToCountDown = [[restPerSet objectAtIndex:exerciseSets.count] intValue];
        else
            timerValueToCountDown = [exerciseObj.restSuggested intValue];

    
        setsInfo = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(exName.frame), 0, 50, 60)];
        setsInfo.numberOfLines = 2;
        setsInfo.text = [NSString stringWithFormat:@"SETS\n%lu/%@", (unsigned long)[exerciseSets count], exerciseObj.setsSuggested];
        setsInfo.font = [UIFont systemFontOfSize:12];
        setsInfo.textAlignment = NSTextAlignmentCenter;
        setsInfo.backgroundColor = [UIColor whiteColor];
    
        maxY = CGRectGetMaxY(setsInfo.frame);
        
        //        UIView *line = [[UIView alloc] initWithFrame:CGRectMake(10, maxY, CGRectGetWidth(cell.frame) - 20, 2)];
        //        line.backgroundColor = [UIColor grayColor];
        //        [cell.contentView addSubview:line];
        //        maxY = CGRectGetMaxY(line.frame);
        
        
        
        maxY = [self createTraditionalView:view maxY:maxY exerciseItem:exerciseObj];
        float width = CGRectGetWidth(view.frame)/10;
        
        _history = [[UILabel alloc] initWithFrame:CGRectMake(0, maxY + 20, width*10, 40)];
        
        NSString *historyTitle = @"";
        historyStr = @"";
        
        NSArray *previousWorkout = [Utilities getSetForExerciseOnPreviosDate:exerciseObj.exerciseName date:_date rName:exerciseObj.routiineName wName:exerciseObj.workoutName];
        previousWorkoutSets = previousWorkout;
        
        //   NSLog(@"cont %lu, %@", [previousWorkout count], _date);
        if ([previousWorkout count] > 0) {
            
            ExerciseSet *firstSet = [previousWorkout objectAtIndex:0];
            historyTitle = [NSString stringWithFormat:@"Last Workout (%@)", firstSet.date];
            
            if ([Utilities showWorkoutPackage]) {
                NSString *temp = @"";
                for (int i = 0; i < 1; i++) {
                    ExerciseSet *data = [previousWorkout objectAtIndex:i];
                    //NSLog(@"ex number is : %@", data.exerciseNumber);
                    if ([data.isDropSet boolValue] == true) {
                        temp = [NSString stringWithFormat:@" %@(DS-%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                    } else {
                        temp = [NSString stringWithFormat:@" %@(%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                    }
                    // this way we will always get maximum weight from last workout.
                    if (lastWorkoutFinalWeight < [data.weight floatValue])
                        lastWorkoutFinalWeight = [data.weight floatValue];
                }
                historyStr = [NSString stringWithFormat:@"%@ UPGRADE TO PREMIUM", temp];
                [_history setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:12]];
                _history.userInteractionEnabled = YES;
                UITapGestureRecognizer *tapGesture =
                [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPurchasePopUpHistory)];
                [_history addGestureRecognizer:tapGesture];
            } else {
                
                for (int i = 0; i < [previousWorkout count]; i++) {
                    
                    ExerciseSet *data = [previousWorkout objectAtIndex:i];
                    //NSLog(@"ex number is : %@", data.exerciseNumber);
                    if ([data.isDropSet boolValue] == true) {
                        historyStr = [NSString stringWithFormat:@" %@(DS-%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                    } else {
                        
                        historyStr = [NSString stringWithFormat:@" %@(%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                    }
                    if (lastWorkoutFinalWeight < [data.weight floatValue])
                        lastWorkoutFinalWeight = [data.weight floatValue];
                }
                [_history setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
            }
        } else {
            historyTitle = [NSString stringWithFormat:@"Last Workout"];
            historyStr = @"No Workout History";
            [_history setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        }
        
//        NSLog(@"HISTORY *** %@", historyStr);
        _history.text = historyStr;
        _history.numberOfLines = 2;
        _history.textAlignment = NSTextAlignmentCenter;
        _history.backgroundColor = [Utilities getMajorMuscleColorSelected:exerciseObj.majorMuscle];
        _history.textColor = [UIColor whiteColor];
        
        float buttonHeight = 30;
        // change bottomBtn to 2 if you want to add prev and next exercise button at bottom
        int bottomBtn = 1;
        
        exerciseSetsView=[[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_history.frame), CGRectGetWidth(view.frame), CGRectGetHeight(view.frame) - CGRectGetMaxY(_history.frame) - buttonHeight * bottomBtn) style:UITableViewStyleGrouped];
        [exerciseSetsView registerClass:[MGSwipeTableCell class] forCellReuseIdentifier:@"setsIdentifier"];
        [exerciseSetsView setEmptyDataSetDelegate:self];
        [exerciseSetsView setEmptyDataSetSource:self];
        [exerciseSetsView setDataSource:self];
        [exerciseSetsView setDelegate:self];
        exerciseSetsView.allowsMultipleSelectionDuringEditing = NO;
        exerciseSetsView.allowsMultipleSelection = YES;
    
    
        NSLog(@"rep text is hidden %d", repText.hidden);
        
        segmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[historyTitle, @"Personal Record"]];
        segmentControl.frame = CGRectMake(0, CGRectGetMinY(exerciseSetsView.frame) - 53, CGRectGetWidth(view.frame), 15);
        segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
        segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
        segmentControl.selectionIndicatorColor = FlatOrangeDark;
        segmentControl.verticalDividerEnabled = NO;
        //segmentControl.verticalDividerColor = [UIColor grayColor];
        segmentControl.verticalDividerWidth = 1.0f;
        segmentControl.selectionIndicatorHeight = 4.0f;
        segmentControl.backgroundColor = [UIColor clearColor];
        segmentControl.titleTextAttributes = @{NSForegroundColorAttributeName : FlatBlack, NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:12]};
        [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    
        seatBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(view.frame) - bottomBtn * buttonHeight, CGRectGetWidth(view.frame)/4, buttonHeight)];
        
        UIImage *seatImage = [UIImage imageNamed:@"IconExSeatHeight"];
        [seatBtn setImage:seatImage forState:UIControlStateNormal];
        seatBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [seatBtn setTitle:@"" forState:UIControlStateNormal];
        seatBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 2, 0);
        seatBtn.titleLabel.font = [UIFont fontWithName:@HAL_REG_FONT size:16];
        [seatBtn addTarget:self action:@selector(seatSetting:) forControlEvents:UIControlEventTouchUpInside];
        
        handBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(seatBtn.frame),CGRectGetHeight(view.frame) - bottomBtn * buttonHeight, CGRectGetWidth(view.frame)/4, buttonHeight)];
        UIImage *handImage = [UIImage imageNamed:@"IconExHandAngle"];
        [handBtn setImage:handImage forState:UIControlStateNormal];
        handBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [handBtn setTitle:@"" forState:UIControlStateNormal];
        handBtn.titleLabel.font = [UIFont fontWithName:@HAL_REG_FONT size:16];
        handBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 2, 0);
        [handBtn addTarget:self action:@selector(handSetting:) forControlEvents:UIControlEventTouchUpInside];
        
        timerBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(handBtn.frame), CGRectGetHeight(view.frame) - bottomBtn * buttonHeight, CGRectGetWidth(view.frame)/4, buttonHeight)];
        UIImage *timerImage = [UIImage imageNamed:@"IconExTimer"];
        [timerBtn setImage:timerImage forState:UIControlStateNormal];
        timerBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [timerBtn setTitle:@"+10s" forState:UIControlStateNormal];
        timerBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 2, 0);
        timerBtn.titleLabel.font = [UIFont fontWithName:@HAL_REG_FONT size:16];
        [timerBtn addTarget:self action:@selector(timerValue:) forControlEvents:UIControlEventTouchUpInside];
        
        commentBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerBtn.frame), CGRectGetHeight(view.frame) - bottomBtn * buttonHeight,  CGRectGetWidth(view.frame)/4, buttonHeight)];
        UIImage *commentImage = [UIImage imageNamed:@"IconExComment"];
        [commentBtn setImage:commentImage forState:UIControlStateNormal];
        commentBtn.imageEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 10);
        [commentBtn setTitle:@"!" forState:UIControlStateNormal];
        commentBtn.titleEdgeInsets = UIEdgeInsetsMake(0, 10, 2, 0);
        commentBtn.titleLabel.font = [UIFont fontWithName:@HAL_REG_FONT size:16];
        [commentBtn addTarget:self action:@selector(commentSetting:) forControlEvents:UIControlEventTouchUpInside];
    
        seatBtn.backgroundColor = FlatTeal;
        handBtn.backgroundColor = FlatTealDark;
        timerBtn.backgroundColor =  FlatTeal;
        commentBtn.backgroundColor = FlatTealDark;
    
        NSPredicate *exNamePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseObj.exerciseName];
        ExerciseList *exTypeBodyweight = [ExerciseList MR_findFirstWithPredicate:exNamePredicate];
        if ([exTypeBodyweight.equipment isEqualToString:@"Bodyweight"]) {
//            [self.view makeToast:@"For bodyweight exercises, the default weight is set to 1."
//                    duration:2
//                    position:CSToastPositionTop
//                       title:@"Info"];
            [EHPlainAlert showAlertWithTitle:@"Infomation" message:@"For bodyweight exercises, the default weight is set to 1." type:ViewAlertInfo];
            [EHPlainAlert updateNumberOfAlerts:1];
            weightText.text = @"1";
        
        }
        [self loadExerciseMetaInfo];

    
    float cornerRadius = 8;
    UIBezierPath *topLeftPath = [UIBezierPath bezierPathWithRoundedRect:exName.bounds byRoundingCorners:(UIRectCornerTopLeft) cornerRadii:CGSizeMake(cornerRadius,cornerRadius)];
    
    UIBezierPath *topRightPath = [UIBezierPath bezierPathWithRoundedRect:setsInfo.bounds byRoundingCorners:(UIRectCornerTopRight) cornerRadii:CGSizeMake(cornerRadius,cornerRadius)];
    
    UIBezierPath *bottomLeftPath = [UIBezierPath bezierPathWithRoundedRect:seatBtn.bounds byRoundingCorners:(UIRectCornerBottomLeft) cornerRadii:CGSizeMake(cornerRadius,cornerRadius)];
    
    UIBezierPath *bottomRightPath = [UIBezierPath bezierPathWithRoundedRect:commentBtn.bounds byRoundingCorners:(UIRectCornerBottomRight) cornerRadii:CGSizeMake(cornerRadius,cornerRadius)];
    
    CAShapeLayer *topLeftmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *topRightmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *bottomLeftmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *bottomRightmaskLayer = [[CAShapeLayer alloc] init];
    

        bottomLeftmaskLayer.frame = seatBtn.bounds;
        bottomLeftmaskLayer.path  = bottomLeftPath.CGPath;
        bottomRightmaskLayer.frame = commentBtn.bounds;
        bottomRightmaskLayer.path  = bottomRightPath.CGPath;
        topLeftmaskLayer.frame = exName.bounds;
        topLeftmaskLayer.path  = topLeftPath.CGPath;
        exName.layer.mask = topLeftmaskLayer;
        topRightmaskLayer.frame = setsInfo.bounds;
        topRightmaskLayer.path  = topRightPath.CGPath;
        setsInfo.layer.mask = topRightmaskLayer;
        seatBtn.layer.mask = bottomLeftmaskLayer;
        commentBtn.layer.mask = bottomRightmaskLayer;
    
        [self setFooterView:[exerciseObj.exerciseNumber intValue] + 1];
        view.layer.cornerRadius = cornerRadius;
    
        [view addSubview:exName];
        [view addSubview:exExtra];
        [view addSubview:setsInfo];
        [view addSubview:_history];
        [view addSubview:exerciseSetsView];
        [view addSubview:segmentControl];
        [view addSubview:seatBtn];
        [view addSubview:handBtn];
        [view addSubview:timerBtn];
        [view addSubview:commentBtn];
        [exerciseSetsView reloadData];
        [self checkSaveBtn];
}


-(float) createTraditionalView: (UIView *) view maxY: (float) maxY exerciseItem:(WorkoutList *) currentEx {
    float width = CGRectGetWidth(view.frame)/10;
    float itemheight = 0;
    NSString *deviceType = [Utilities getIphoneName];
   // NSLog(@"device model is %@", deviceType);
    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        itemheight = 30;
    } else if ([deviceType containsString:@"5"] || [deviceType containsString:@"SE"] || [deviceType containsString:@"iPod"]) {
        itemheight = 40;
    } else {
        itemheight = 50;
    }
    
    weightMinus = [[UIButton alloc] initWithFrame:CGRectMake(width*2.5, maxY + 15, width, itemheight)];
    [weightMinus setTitle:@"-" forState:UIControlStateNormal];
    weightMinus.backgroundColor = FlatMintDark;
    [weightMinus addTarget:self action:@selector(weightDec:) forControlEvents:UIControlEventTouchUpInside];
    
    
    weightText = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightMinus.frame), maxY + 15, width * 3, itemheight)];
    weightText.font = [UIFont fontWithName:@HAL_REG_FONT size:20];
    weightText.textAlignment = NSTextAlignmentCenter;
    weightText.keyboardType = UIKeyboardTypeDecimalPad;
    weightText.inputAccessoryView = addOnKeyboard;
    weightText.textContainer.maximumNumberOfLines = 1;
    weightText.scrollEnabled = false;
    weightText.backgroundColor = [UIColor whiteColor];
    weightText.delegate = self;
//    [weightText addTarget:self
//                  action:@selector(checkSaveBtn)
//        forControlEvents:UIControlEventEditingChanged];
    
    weightText.tag = [exerciseObj.exerciseNumber intValue];
    
    [weightText setReturnKeyType:UIReturnKeyDone];
    
    weightPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightText.frame), maxY + 15, width, itemheight)];
    [weightPlus setTitle:@"+" forState:UIControlStateNormal];
    weightPlus.backgroundColor = FlatMintDark;
    
    [weightPlus addTarget:self action:@selector(weightInc:) forControlEvents:UIControlEventTouchUpInside];
    
    maxY = CGRectGetMaxY(weightPlus.frame);
    
    repMinus = [[UIButton alloc] initWithFrame:CGRectMake(width*2.5,maxY + 10, width, itemheight)];
    [repMinus setTitle:@"-" forState:UIControlStateNormal];
    repMinus.backgroundColor = FlatMintDark;
    [repMinus addTarget:self action:@selector(repDec:) forControlEvents:UIControlEventTouchUpInside];
    
    
    repText = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinus.frame), maxY + 10, width * 3, itemheight)];
    repText.font = [UIFont fontWithName:@HAL_REG_FONT size:20];
    repText.textAlignment = NSTextAlignmentCenter;
    repText.keyboardType = UIKeyboardTypeNumberPad;
    repText.backgroundColor = [UIColor whiteColor];
    repText.textContainer.maximumNumberOfLines = 1;
    repText.scrollEnabled = false;
    repText.delegate = self;
//    [repText addTarget:self
//                   action:@selector(checkSaveBtn)
//         forControlEvents:UIControlEventEditingChanged];

    [repText setReturnKeyType:UIReturnKeyDone];
    
    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        weightText.floatingLabelYPadding = -15;
        weightText.placeholderYPadding = -20;
        repText.floatingLabelYPadding = -15;
        repText.placeholderYPadding = -20;

    } else if ([deviceType containsString:@"5"] || [deviceType containsString:@"SE"] || [deviceType containsString:@"iPod"]) {
        weightText.floatingLabelYPadding = -5;
        weightText.placeholderYPadding = -10;
        repText.floatingLabelYPadding = -5;
        repText.placeholderYPadding = -10;
    } else {
        weightText.floatingLabelYPadding = 5;
        weightText.placeholderYPadding = 1;
        repText.floatingLabelYPadding = 5;
        repText.placeholderYPadding = 1;
    }

    /*
     repText  = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinus.frame), maxY + 10, width * 3, itemheight)];
     repText.placeholder = NSLocalizedString(@"REPS", @"");
     repText.font = [UIFont fontWithName:@HAL_REG_FONT size:20];
     repText.textAlignment = NSTextAlignmentCenter;
     repText.keyboardType = UIKeyboardTypeNumberPad;
     repText.floatingLabelYPadding = 0;
     repText.floatingLabelFont = [UIFont systemFontOfSize:12];
     repText.translatesAutoresizingMaskIntoConstraints = NO;
     repText.textContainer.maximumNumberOfLines = 1;
     repText.scrollEnabled = false;
     repText.delegate = self;
     [repText setReturnKeyType:UIReturnKeyDone];
     */
    
    repPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repText.frame), maxY + 10, width, itemheight)];
    [repPlus setTitle:@"+" forState:UIControlStateNormal];
    repPlus.backgroundColor = FlatMintDark;
    [repPlus addTarget:self action:@selector(repInc:) forControlEvents:UIControlEventTouchUpInside];
    
    repsPerSet = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repPlus.frame) + 5, maxY + 10, CGRectGetWidth(_collectionView.frame) - CGRectGetMaxX(repPlus.frame), itemheight)];
    repsPerSet.text = @"";
    repsPerSet.textColor = FlatGray;
    
    [self buttonsBeautification];
    
    WorkoutList *item = currentEx;//[exerciseList objectAtIndex:0];
    NSArray *repArray = [item.repsPerSet componentsSeparatedByString:@","];
    if (repArray.count != 0) {
        if ([[repArray objectAtIndex:0] isEqualToString:@"MAX"]) {
            repText.text = [NSString stringWithFormat:@"20"];
            repsPerSet.text = @"MAX(20)";
            userCustomRepsForSet = 20;
        } else {
            repText.text = [repArray objectAtIndex:0];
            userCustomRepsForSet = [[repArray objectAtIndex:0] intValue];
            //repsPerSet.text = item.repsPerSet;
        }
    }
    else
        repText.text = [NSString stringWithFormat:@"%@", item.repsSuggested];
    
    
    weightText.placeholder = NSLocalizedString(@"WEIGHT", @"");
    repText.placeholder = NSLocalizedString(@"REPS", @"");
//    weightText.placeholderLabel.textAlignment = NSTextAlignmentCenter;
//    repText.placeholderLabel.textAlignment = NSTextAlignmentRight;
    weightText.floatingLabel.textAlignment = NSTextAlignmentCenter;
    repText.floatingLabel.textAlignment = NSTextAlignmentCenter;

    
    maxY = CGRectGetMaxY(repPlus.frame);
    saveBtn = [[UIButton alloc] initWithFrame:CGRectMake(width, maxY + 15, width * 3, 35)];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    saveBtn.backgroundColor = FlatMintDark;
    saveBtn.layer.cornerRadius = 3;
    [saveBtn addTarget:self action:@selector(saveWorkout:) forControlEvents:UIControlEventTouchUpInside];
    
    cllearBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(saveBtn.frame) + width*2, maxY + 15, width * 3, 35)];
    [cllearBtn setTitle:@"Cancel" forState:UIControlStateNormal];
    cllearBtn.layer.cornerRadius = 3;
    cllearBtn.backgroundColor = FlatSkyBlueDark;
    [cllearBtn addTarget:self action:@selector(clearWorkout:) forControlEvents:UIControlEventTouchUpInside];
    
    [view addSubview:weightMinus];
    [view addSubview:weightText];
    [view addSubview:weightPlus];
    [view addSubview:repMinus];
    [view addSubview:repText];
    [view addSubview:repPlus];
    [view addSubview:repsPerSet];
    [view addSubview:saveBtn];
    [view addSubview:cllearBtn];

    // when we scroll the windown
    [self updateRepText];
    return CGRectGetMaxY(cllearBtn.frame) + 20;
}

-(IBAction)saveWorkout:(id)sender {
    bool isNewPREntry = false;
    if (updateField == false) {
        NSDate *now = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm:ss";
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        
        ExerciseSet *set = [ExerciseSet MR_createEntityInContext:localContext];
        set.exerciseName  = exerciseObj.exerciseName;
        set.muscle = exerciseObj.muscle;
        set.majorMuscle = exerciseObj.majorMuscle;
        set.date = _date;
        set.rep = [NSNumber numberWithInteger:[repText.text integerValue]];
        set.weight = [NSNumber numberWithFloat:[weightText.text floatValue]];
        set.timeStamp =[dateFormatter stringFromDate:now];
        set.isDropSet = [NSNumber numberWithBool:false];
        set.isPyramidSet = [NSNumber numberWithBool:false];
        set.isSuperSet = [NSNumber numberWithBool:false];
        set.routineName = _routineName;
        set.workoutName = _workoutName;
        set.exerciseNumber = _exerciseNumber;
        set.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
        
        if ([set.weight floatValue] > maxWeight) {
            exMetaInfo.maxWeight = set.weight;
            exMetaInfo.maxRep = set.rep;
            exMetaInfo.maxDate = _date;
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            maxWeight = [set.weight floatValue];
            maxRep = [set.rep intValue];
            isPersonalRecord = true;
            isNewPREntry = true;
        } else if ([set.weight floatValue] == maxWeight) {
            if ([set.rep intValue] > maxRep) { // removed =
                exMetaInfo.maxWeight = set.weight;
                exMetaInfo.maxRep = set.rep;
                exMetaInfo.maxDate = _date;
                exMetaInfo.syncedState = [NSNumber numberWithBool:false];
                maxWeight = [set.weight floatValue];
                maxRep = [set.rep intValue];
                isPersonalRecord = true;
                isNewPREntry = true;
            }
        }
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                NSLog(@"set saved... %d", isPersonalRecord);
                [Utilities addExerciseSetToParse:set];
                if (isNewPREntry == true) {
                    [Utilities addExercisePersonalRecordEntry:exMetaInfo setInfo:set setInfo:exerciseObj];
                }
            }
        }];
        
        //Crash in 1.4: #12: saveWorkout crash. Sometimes the exerciseGroup is nil for weird reason. So we are cleaning it up and making sure we dont access nil object.
        int numExInGrpCount = 0;
        if ([exerciseGroups count] == 0) {
            numExInGrpCount = 1;
            NSLog(@"Mayank: exercise GROUP count is 0");
        }
        else
            numExInGrpCount = (int)[[exerciseGroups objectAtIndex:[exerciseObj.exerciseGroup intValue]] count];
        
        // Crash in 1.4: #22 setting the timer to what user has set in workout.
        timerValueToCountDown = ([exerciseObj.restSuggested intValue] == 0) ? 45: [exerciseObj.restSuggested intValue];

        if (numExInGrpCount > 1) {
            int exNumInGrp = [exerciseObj.exerciseNumInGroup intValue] + 1;
            if (exNumInGrp < numExInGrpCount) {
                NSLog(@"scrolling to next exercise");
//                [self reloadExercise];
//                [exerciseCard scrollToItemAtIndex:[exerciseObj.exerciseNumInGroup intValue] + 1 animated:YES];
                NSIndexPath *nextItem = [NSIndexPath indexPathForItem:[exerciseObj.exerciseNumInGroup intValue] + 1 inSection:0];
                [exerciseCards scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
            } else if (exNumInGrp == numExInGrpCount) {
                int exCountLocal = (int)[exerciseSets count] + 1;
                NSLog(@"scrolling to 1st exercise %d, %d", exCountLocal , [exerciseObj.setsSuggested intValue]);
                if (exCountLocal == [exerciseObj.setsSuggested intValue]) {
                    [self reloadExercise];
                    [timer pause];
                    [timer reset];
                    [self showNextExerciseMenuWithCountDownTimer:CNPPopupStyleCentered];
                    [self scheduleNotificationAfterSec:[Utilities getDefaultRestTimerValueBetweenExercises]];
                } else {
                    [timer setCountDownTime:timerValueToCountDown];
                    [timer pause];
                    [timer reset];
                    [timer start];
//                    [exerciseCard scrollToItemAtIndex:0 animated:YES];
                    NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:0];
                    [exerciseCards scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredVertically animated:YES];
                    [self scheduleNotificationAfterSec:timerValueToCountDown];
                    [self showRestTimerBetweenSets:CNPPopupStyleActionSheet];
                }
            }
        } else {
            // only 1 exercise in group.. Start counter.
            int exCountLocal = (int)[exerciseSets count] + 1;
            if (exCountLocal == [exerciseObj.setsSuggested intValue]) {
                [self showNextExerciseMenuWithCountDownTimer:CNPPopupStyleCentered];
            } else {
                [self showRestTimerBetweenSets:CNPPopupStyleActionSheet];
                [timer setCountDownTime:timerValueToCountDown];
                [timer pause];
                [timer reset];
                [timer start];
            }
            [self reloadExercise];

        }
        
        //once we save everything.. updateField should be false when we exit from here.
        updateField = false;
        
    } else {
        NSLog(@"updating ***** %d", selectedIndex);
        ExerciseSet *set = [exerciseSets objectAtIndex:selectedIndex];
        set.rep = [NSNumber numberWithInteger:[repText.text integerValue]];
        set.weight = [NSNumber numberWithFloat:[weightText.text floatValue]];
        
        
        if ([set.weight floatValue] > maxWeight) {
            exMetaInfo.maxWeight = set.weight;
            exMetaInfo.maxRep = set.rep;
            exMetaInfo.maxDate = _date;
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            maxWeight = [set.weight floatValue];
            maxRep = [set.rep intValue];
            isNewPREntry = true;
        } else if ([set.weight floatValue] == maxWeight) {
            if ([set.rep intValue] >= maxRep) {
                exMetaInfo.maxWeight = set.weight;
                exMetaInfo.maxRep = set.rep;
                exMetaInfo.maxDate = _date;
                exMetaInfo.syncedState = [NSNumber numberWithBool:false];
                maxWeight = [set.weight floatValue];
                maxRep = [set.rep intValue];
                isNewPREntry = true;
            }
        }
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                NSLog(@"set updated...");
                // there seems to be a crash here. #26 on fabric seems like when we are deleting, the update button is still highlighted and causing the set to be passed for storing 
                if (set != nil)
                    [Utilities updateExerciseSetToParse:set];
                if (isNewPREntry == true) {
                    [Utilities addExercisePersonalRecordEntry:exMetaInfo setInfo:set setInfo:exerciseObj];
                }
            }
        }];
        
        [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
        saveBtn.titleLabel.textColor = [UIColor whiteColor];
        //    selectedIndex = -1;
        NSIndexPath *path = [NSIndexPath indexPathWithIndex:selectedIndex];
        [exerciseSetsView deselectRowAtIndexPath:path animated:YES];
        
        
        [self reloadExercise];
        
        if ([exerciseSets count] == [exerciseObj.setsSuggested intValue]) {
            [timer pause];
            [timer reset];
            [self showNextExerciseMenuWithCountDownTimer:CNPPopupStyleCentered];
            [self scheduleNotificationAfterSec:[Utilities getDefaultRestTimerValueBetweenExercises]];
        } else {
            if (updateField == false) {
                [self scheduleNotificationAfterSec:timerValueToCountDown];
                [self showRestTimerBetweenSets:CNPPopupStyleActionSheet];
            }
        }
        
        //once we save everything.. updateField should be false when we exit from here.
        updateField = false;
    }
    [Utilities syncExerciseMetaInfoToParse];
    [Utilities setTodayWidgetData];
}
-(void) scheduleNotificationAfterSec:(int) seconds {
    
    // we need to do this to make sure all previous ones are cleared.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    // before we create any new ones, cancel all existing notifications
    //[[UIApplication sharedApplication]cancelAllLocalNotifications];
    [Utilities deleteLocalNotificationWithBody:@REST_TIMER_OVER_TEXT];
    
    if ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone) {
        NSLog(@"***** notification scheduled.. **** ");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        localNotification.fireDate = [[NSDate date] dateByAddingSeconds:timerValueToCountDown];
        localNotification.alertAction = NSLocalizedString(@"RestTimer", @"RestTimer");
        localNotification.alertBody = @REST_TIMER_OVER_TEXT;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;
        
        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        // this will fire the notification right away, it will still also fire at the date we set
        //        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
    }
}

-(void) reloadExercise {
    exerciseSets = nil;
    //  NSLog(@"%s", __FUNCTION__);
    
    exerciseSets = [Utilities getSetForExerciseOnDate:exerciseObj.exerciseName date:_date];
    setsInfo.text = [NSString stringWithFormat:@"Sets:\n%lu/%@", (unsigned long)[exerciseSets count], exerciseObj.setsSuggested];
    [exerciseSetsView reloadData];
    [self updateRepText];
}

-(void) updateRepText {
    NSArray *repsInSet = [exerciseObj.repsPerSet componentsSeparatedByString:@","];
    NSLog(@"updating rep text now... %d %d", (int)exerciseSets.count, (int) repsInSet.count);
    if (exerciseSets.count < repsInSet.count) {
        if ([[repsInSet objectAtIndex:exerciseSets.count] isEqualToString:@"MAX"]) {
            repText.text = [NSString stringWithFormat:@"20"];
        } else {
            repText.text = [NSString stringWithFormat:@"%d", [[repsInSet objectAtIndex:exerciseSets.count] intValue]];
        }
    }
}
- (void)showNextExerciseMenuWithCountDownTimer:(CNPPopupStyle)popupStyle {
    
    int exNum = [_exerciseNumber intValue];
    exNum++;
    UIColor *appColor = [Utilities getAppColor];
    
    if (exNum == [exerciseList count]) {
        NSLog(@"exnum is %d, total: %lu", exNum, (long)[exerciseList count]);
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"WORKOUT COMPLETE" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:18], NSParagraphStyleAttributeName : paragraphStyle}];
        
        
        UIView *customViewComplete = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 265, 180)];
        
        NSString *imageName = [NSString stringWithFormat:@"workoutFinish%d", rand()%5];
        UIImage *image =[UIImage imageNamed:imageName];
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 265, 150)];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        [imageView setImage:image];
        
        UILabel *lineOneLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(imageView.frame), 265, 30)];
        lineOneLabel.numberOfLines = 2;
        lineOneLabel.attributedText = lineOne;
        lineOneLabel.textColor = [UIColor whiteColor];

        customViewComplete.backgroundColor = appColor;
        customViewComplete.layer.cornerRadius = 5;
        [customViewComplete addSubview:imageView];
        [customViewComplete addSubview:lineOneLabel];
        
        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
        [button setTitleColor:appColor forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        [button setTitle:@"RECORD MORE SETS" forState:UIControlStateNormal];
        button.backgroundColor = [UIColor whiteColor];
        button.layer.cornerRadius = 4;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
        };
        
        CNPPopupButton *nextButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
        [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        nextButton.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        [nextButton setTitle:@"FINISH WORKOUT" forState:UIControlStateNormal];
        nextButton.backgroundColor = appColor;
        nextButton.layer.cornerRadius = 4;
        nextButton.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
//            [self LoadNextExerciseCell];
//            [self performSegueWithIdentifier:@"showSummarySegue" sender:self];
            [self dismissViewControllerAnimated:YES completion:nil];
            
            // Bug: also pause the timer.
            [timer pause];
            [timer reset];
            // Bug: no need to set the notification.
            [Utilities deleteLocalNotificationWithBody:@REST_TIMER_OVER_TEXT];

        };
        
        
        if (isPersonalRecord) {
            UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 265, 135)];
            customView.backgroundColor = [Utilities getAppColor];
            customView.layer.cornerRadius = 5;
            
            
            NSString *imageName = [NSString stringWithFormat:@"motivation%d", rand()%41];
            //UIImage *image= [self imageWithImage:[UIImage imageNamed:imageName] scaledToSize:CGSizeMake(50, 50)];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(customView.frame)/3, 100)];
            [imageView setImage:[UIImage imageNamed:imageName]];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            NSString *personalRecStr = [NSString stringWithFormat:@"%@\n%d x %.1f %@\nPERSONAL RECORD", exerciseObj.exerciseName, maxRep, maxWeight, [[Utilities getUnits] lowercaseString]];
            
            UILabel *header = [[UILabel alloc] init];
            header.text = [headerStrings objectAtIndex:rand()%[headerStrings count]];
            header.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            header.textColor = [UIColor whiteColor];
            header.textAlignment = NSTextAlignmentCenter;
            // we are setting frame later because sizetofit sets it to left align
            [header sizeToFit];
            header.frame =CGRectMake(CGRectGetMaxX(imageView.frame), 0, CGRectGetWidth(customView.frame)/3*2, 30);
            
            UILabel *lineTwoLbl = [[UILabel alloc] init];
            lineTwoLbl.numberOfLines = 0;
            lineTwoLbl.textAlignment = NSTextAlignmentCenter;
            lineTwoLbl.text = personalRecStr;
            lineTwoLbl.textColor = [UIColor whiteColor];
            lineTwoLbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
            [lineTwoLbl sizeToFit];
            lineTwoLbl.frame = CGRectMake(CGRectGetMaxX(imageView.frame), CGRectGetMaxY(header.frame), CGRectGetWidth(customView.frame)/3*2, 70);
            
            CNPPopupButton *shareBtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(lineTwoLbl.frame), CGRectGetWidth(customView.frame)- 10, 30)];
            [shareBtn setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            shareBtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            [shareBtn setTitle:@"Share" forState:UIControlStateNormal];
            shareBtn.backgroundColor = [UIColor whiteColor];
            shareBtn.layer.cornerRadius = 5;
            shareBtn.selectionHandler = ^(CNPPopupButton *button){
                [self.popupController dismissPopupControllerAnimated:YES];
                [self performSegueWithIdentifier:@"shareSegue" sender:self];
            };

            [customView addSubview:imageView];
            [customView addSubview:header];
            [customView addSubview:lineTwoLbl];
            [customView addSubview:shareBtn];
            self.popupController = [[CNPPopupController alloc] initWithContents:@[customView, customViewComplete, button, nextButton]];
            isPersonalRecord = false;
        }
        else
            self.popupController = [[CNPPopupController alloc] initWithContents:@[customViewComplete, button, nextButton]];
    } else {
        
        //int exNumber = [NSNumber numberWithInt:exNum];
        WorkoutList *nextExName = [exerciseList objectAtIndex:exNum];
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", nextExName.exerciseName];
        ExerciseMetaInfo *exMetaInfoNext = [ExerciseMetaInfo MR_findFirstWithPredicate:exPredicate inContext:localContext];
        
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"NEXT EXERCISE" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:20], NSParagraphStyleAttributeName : paragraphStyle}];
        
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:nextExName.exerciseName attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        
        NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"SEAT: %@, HAND: %@", (exMetaInfoNext.seatHeight == nil) ? @0: exMetaInfoNext.seatHeight, (exMetaInfoNext.handAngle == nil) ? @0: exMetaInfoNext.handAngle] attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        titleLabel.textColor = appColor;
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 2;
        lineOneLabel.attributedText = lineOne;
        lineOneLabel.textColor = [Utilities getMajorMuscleColor:exerciseObj.majorMuscle];
        lineOneLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];

        UILabel *lineTwoLabel = [[UILabel alloc] init];
        lineTwoLabel.numberOfLines = 1;
        lineTwoLabel.attributedText = lineTwo;
        lineTwoLabel.textColor = [Utilities getMajorMuscleColor:exerciseObj.majorMuscle];
        lineTwoLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];

        /*
        timerBWEx = [[MZTimerLabel alloc] initWithFrame:CGRectMake(0, 0, 250,50)];
        timerBWEx.delegate = self;
        [timerBWEx setCountDownTime:[Utilities getDefaultRestTimerValueBetweenExercises]];
        timerBWEx.resetTimerAfterFinish = NO; //IMPORTANT, if you needs custom text with finished, please do not set resetTimerAfterFinish to YES.
        timerBWEx.timeFormat = @"m:ss";
        timerBWEx.tag = 101;
        timerBWEx.timeLabel.backgroundColor = [UIColor clearColor];
        timerBWEx.timeLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:40];
        timerBWEx.timeLabel.textColor = [UIColor blackColor];
        timerBWEx.timeLabel.textAlignment = NSTextAlignmentCenter;
        timerBWEx.timerType = MZTimerLabelTypeTimer;
        [timerBWEx start];
        */
        
        if (isPersonalRecord) {
            _timeCircle = [[CircleTimer alloc] initWithFrame:CGRectMake(0, 0, 150, 150)];
            self.timeCircle.font = [UIFont systemFontOfSize:40 weight:30];
        }
        else {
            _timeCircle = [[CircleTimer alloc] initWithFrame:CGRectMake(0, 0, 250, 250)];
            self.timeCircle.font = [UIFont systemFontOfSize:60 weight:30];
        }
        self.timeCircle.totalTime = [Utilities getDefaultRestTimerValueBetweenExercises];
        self.timeCircle.elapsedTime = 0;
        self.timeCircle.thickness = 15;
        self.timeCircle.isBackwards = YES;
        
        self.timeCircle.active = YES;
        
        [self.timeCircle start];

        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
        [button setTitleColor:appColor forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button setTitle:@"RECORD MORE SETS" forState:UIControlStateNormal];
        button.backgroundColor = [UIColor whiteColor];
        button.layer.cornerRadius = 4;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
        };
        
        CNPPopupButton *nextButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
        [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [nextButton setTitle:@"LET'S GO" forState:UIControlStateNormal];
        nextButton.backgroundColor = appColor;
        nextButton.layer.cornerRadius = 4;
        
        nextButton.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
//            [timerBWEx pause];
//            [timerBWEx reset];
            [timer pause];
            //resetting in case someone recorded multiple sets with last set and moved on to next exercise.
            [timer setCountDownTime:0];
            [_timeCircle stop];
            
            NSIndexPath *nextItem = [NSIndexPath indexPathForItem:0 inSection:groupNumber + 1];
            [_collectionView scrollToItemAtIndexPath:nextItem atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];

            //            [Utilities syncExerciseSetToParse:_date];
            //            [self loadNextExercise:self];
//            [self LoadNextExerciseCell];
        };
        
        if (isPersonalRecord) {
            UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 135)];
            customView.backgroundColor = [Utilities getAppColor];
            customView.layer.cornerRadius = 5;
            
            
            NSString *imageName = [NSString stringWithFormat:@"motivation%d", rand()%41];
            //UIImage *image= [self imageWithImage:[UIImage imageNamed:imageName] scaledToSize:CGSizeMake(50, 50)];
            
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(customView.frame)/3, 100)];
            [imageView setImage:[UIImage imageNamed:imageName]];
            imageView.contentMode = UIViewContentModeScaleAspectFit;
            
            NSString *personalRecStr = [NSString stringWithFormat:@"%@\n%d x %.1f %@\nPERSONAL RECORD", exerciseObj.exerciseName, maxRep, maxWeight, [[Utilities getUnits] lowercaseString]];
            UILabel *header = [[UILabel alloc] init];
            header.text = [headerStrings objectAtIndex:rand()%[headerStrings count]];
            header.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            header.textColor = [UIColor whiteColor];
            header.textAlignment = NSTextAlignmentCenter;
            // we are setting frame later because sizetofit sets it to left align
            [header sizeToFit];
            header.frame =CGRectMake(CGRectGetMaxX(imageView.frame), 0, CGRectGetWidth(customView.frame)/3*2, 30);
            
            UILabel *lineTwoLbl = [[UILabel alloc] init];
            lineTwoLbl.numberOfLines = 0;
            lineTwoLbl.textAlignment = NSTextAlignmentCenter;
            lineTwoLbl.text = personalRecStr;
            lineTwoLbl.textColor = [UIColor whiteColor];
            lineTwoLbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
            [lineTwoLbl sizeToFit];
            lineTwoLbl.frame = CGRectMake(CGRectGetMaxX(imageView.frame), CGRectGetMaxY(header.frame), CGRectGetWidth(customView.frame)/3*2, 70);
            
            CNPPopupButton *shareBtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(lineTwoLbl.frame), CGRectGetWidth(customView.frame)- 10, 30)];
            [shareBtn setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            shareBtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            [shareBtn setTitle:@"Share" forState:UIControlStateNormal];
            shareBtn.backgroundColor = [UIColor whiteColor];
            shareBtn.layer.cornerRadius = 5;
            shareBtn.selectionHandler = ^(CNPPopupButton *button){
                [self.popupController dismissPopupControllerAnimated:YES];
                [self performSegueWithIdentifier:@"shareSegue" sender:self];
            };
            
            [customView addSubview:imageView];
            [customView addSubview:header];
            [customView addSubview:lineTwoLbl];
            [customView addSubview:shareBtn];
            self.popupController = [[CNPPopupController alloc] initWithContents:@[customView, titleLabel, lineOneLabel, lineTwoLabel, _timeCircle, button, nextButton]];
            isPersonalRecord = false;
        } else
            self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, lineTwoLabel, _timeCircle, button, nextButton]];
    }
    //    self.popupController.theme.backgroundColor = FlatOrange;
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromRight;
    self.popupController.theme.dismissesOppositeDirection = YES;
    self.popupController.theme.contentVerticalPadding = 8;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}

#pragma POPUP Delegate
- (void)popupControllerDidDismiss:(CNPPopupController *)controller {
    if (controller == self.popupController) {
        _timeCircle = nil;
    }
        
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (void)showRestTimerBetweenSets:(CNPPopupStyle)popupStyle {
    
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"REST" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:30], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 2;
    lineOneLabel.attributedText = lineOne;
    lineOneLabel.textColor = FlatGreenDark;
    
    NSArray *restPerSet = [exerciseObj.restPerSet componentsSeparatedByString:@","];
    if (((int) restPerSet.count > 0) && ((int)restPerSet.count > (int)exerciseSets.count)) {
        timerValueToCountDown = [[restPerSet objectAtIndex:exerciseSets.count] intValue];
    }
    else
        timerValueToCountDown = [exerciseObj.restSuggested intValue];
    
    _timeCircle = [[CircleTimer alloc] initWithFrame:CGRectMake(0, 0, 250, 250)];
    self.timeCircle.totalTime = (timerValueToCountDown == 0) ? [Utilities getDefaultRestTimerValue]: timerValueToCountDown;
    self.timeCircle.elapsedTime = 0;
    self.timeCircle.thickness = 15;
    self.timeCircle.isBackwards = YES;
    self.timeCircle.font = [UIFont systemFontOfSize:60 weight:30];
    self.timeCircle.active = YES;
    [self.timeCircle start];
    [timer start];
    
    if (isPersonalRecord) {
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 340, 135)];
        customView.backgroundColor = [Utilities getAppColor];
        customView.layer.cornerRadius = 5;
        
       
        NSString *imageName = [NSString stringWithFormat:@"motivation%d", rand()%41];
        //UIImage *image= [self imageWithImage:[UIImage imageNamed:imageName] scaledToSize:CGSizeMake(50, 50)];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(customView.frame)/3, 100)];
        [imageView setImage:[UIImage imageNamed:imageName]];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        
        NSString *personalRecStr = [NSString stringWithFormat:@"%@\n%d x %.1f %@\nPERSONAL RECORD", exerciseObj.exerciseName, maxRep, maxWeight, [[Utilities getUnits] lowercaseString]];
        UILabel *header = [[UILabel alloc] init];
        header.text = [headerStrings objectAtIndex:rand()%[headerStrings count]];
        header.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        header.textColor = [UIColor whiteColor];
        header.textAlignment = NSTextAlignmentCenter;
        // we are setting frame later because sizetofit sets it to left align
        [header sizeToFit];
        header.frame =CGRectMake(CGRectGetMaxX(imageView.frame), 0, CGRectGetWidth(customView.frame)/3*2, 30);
        
        UILabel *lineTwoLbl = [[UILabel alloc] init];
        lineTwoLbl.numberOfLines = 0;
        lineTwoLbl.textAlignment = NSTextAlignmentCenter;
        lineTwoLbl.text = personalRecStr;
        lineTwoLbl.textColor = [UIColor whiteColor];
        lineTwoLbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
        [lineTwoLbl sizeToFit];
        lineTwoLbl.frame = CGRectMake(CGRectGetMaxX(imageView.frame), CGRectGetMaxY(header.frame), CGRectGetWidth(customView.frame)/3*2, 70);
        
        CNPPopupButton *shareBtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(lineTwoLbl.frame), CGRectGetWidth(customView.frame)- 10, 30)];
        [shareBtn setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
        shareBtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        [shareBtn setTitle:@"Share" forState:UIControlStateNormal];
        shareBtn.backgroundColor = [UIColor whiteColor];
        shareBtn.layer.cornerRadius = 5;
        shareBtn.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
            [self performSegueWithIdentifier:@"shareSegue" sender:self];
        };
        
        
        [customView addSubview:imageView];
        [customView addSubview:header];
        [customView addSubview:lineTwoLbl];
        [customView addSubview:shareBtn];
        
        
        self.popupController = [[CNPPopupController alloc] initWithContents:@[customView, lineOneLabel,  self.timeCircle]];
        isPersonalRecord = false;
    }
    else
        self.popupController = [[CNPPopupController alloc] initWithContents:@[lineOneLabel, self.timeCircle]];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.shouldDismissOnBackgroundTouch = true;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromBottom;
    self.popupController.theme.dismissesOppositeDirection = YES;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)weightInc:(id)sender {
    NSLog(@"%s", __func__);
    if ([Utilities isIncBy25Enabled])
        weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] + 2.5];
    else
        weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] + 5];
    
    [self checkSaveBtn];
}
-(IBAction)weightDec:(id)sender {
    int newValue = 0;
    NSLog(@"%s", __func__);
    if ([Utilities isIncBy25Enabled]) {
        newValue = [weightText.text intValue] - 2.5;
        if (newValue >= 0)
            weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] - 2.5];
    }
    else {
        newValue = [weightText.text intValue] - 5;
        if (newValue >= 0)
            weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] - 5];
    }
    
    
    [self checkSaveBtn];
}
-(IBAction)repInc:(id)sender {
    repText.text =  [NSString stringWithFormat:@"%d", [repText.text intValue] + 1];
    if ([repText.text intValue] != userCustomRepsForSet && exerciseSets.count < [exerciseObj.setsSuggested intValue]) {
        repsPerSet.text = [NSString stringWithFormat:@"/%d", userCustomRepsForSet];
    } else {
        repsPerSet.text = @"";
    }
    [self checkSaveBtn];
}
-(IBAction)repDec:(id)sender {
    int newValue = [repText.text intValue] - 1;
    if (newValue >= 0) {
        repText.text =  [NSString stringWithFormat:@"%d", [repText.text intValue] - 1];
        if ([repText.text intValue] != userCustomRepsForSet && exerciseSets.count < [exerciseObj.setsSuggested intValue]) {
            repsPerSet.text = [NSString stringWithFormat:@"/%d", userCustomRepsForSet];
        } else {
            repsPerSet.text = @"";
        }
    }
    
    [self checkSaveBtn];
}

-(IBAction)clearWorkout:(id)sender {
    
    [timer pause];
    [timer reset];
    weightText.text = @"";
    repText.text = @"";
    if (isDropSetVisible == true) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            
        } completion:^(BOOL finished) {
            isDropSetVisible = false;
        }];
    }
    NSLog(@"check and see if update is active %d", updateField);
    if (updateField == true) {
        updateField = false;
        [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    }
    [exerciseSetsView reloadData];
    [self checkSaveBtn];
}

#pragma mark - UITableViewDataSource


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == myBarbelTableView) {
        if (lbORkg == 0)
            return [weightsLbs count];
        else
            return [weightsKgs count];
    } else {
        NSLog(@"exercise sets count is %lu", (long) [exerciseSets count]);
        return [exerciseSets count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    if (tableView == myBarbelTableView) {
        static NSString *CellIdentifier = @"newFriendCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        for (UIView *lbl in cell.contentView.subviews) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            }
        }
        
        UILabel *weightName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(tableView.frame)/3, 30)];
        UILabel *val = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightName.frame), 5, CGRectGetWidth(tableView.frame)/3, 30)];
        UIStepper *stepper = [[UIStepper alloc] initWithFrame:CGRectMake(CGRectGetMaxX(val.frame), 5, CGRectGetWidth(tableView.frame)/3, 30)];
        [stepper addTarget:self action:@selector(stepperValueDidChanged:) forControlEvents:UIControlEventValueChanged];
        
        if (lbORkg == 0) {
            weightName.text = [weightsLbs objectAtIndex:indexPath.row];
            val.text = [NSString stringWithFormat:@"%@", [weightLbsArray objectAtIndex:indexPath.row]];
        }
        else {
            weightName.text = [weightsKgs objectAtIndex:indexPath.row];
            val.text = [NSString stringWithFormat:@"%@", [weightKgsArray objectAtIndex:indexPath.row]];
        }
        
        weightName.tag = 2;
        val.tag = 1;
        stepper.value = 0;
        weightName.textAlignment =     val.textAlignment = NSTextAlignmentCenter;
        
        [stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateNormal];
        //[stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateHighlighted];
        [stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateDisabled];
        [stepper setDividerImage:self.emptyImageForSteper forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal];
        stepper.tintColor = [UIColor blackColor];
        cell.backgroundColor = FlatWhiteDark;
        
        [cell.contentView addSubview:weightName];
        [cell.contentView addSubview:val];
        [cell.contentView addSubview:stepper];
        return cell;
        
    } else {
        MGSwipeTableCell *cell = [exerciseSetsView dequeueReusableCellWithIdentifier:@"setsIdentifier" forIndexPath:indexPath];
        
        float cellWidth = CGRectGetWidth(cell.frame);
        float smallCellWidth = ((cellWidth * 4)/6 - 50)/3;
        float bigCellWidth = (cellWidth - smallCellWidth*3)/2;
        
        UILabel *count = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, smallCellWidth, cell.frame.size.height)];
        UILabel *weight = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(count.frame), 0, bigCellWidth, cell.frame.size.height)];
        UILabel *weightLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weight.frame), 0, smallCellWidth, cell.frame.size.height)];
        UILabel *rep = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightLbl.frame), 0, bigCellWidth, cell.frame.size.height)];
        UILabel *repLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rep.frame), 0, smallCellWidth, cell.frame.size.height)];
        
        for (UILabel *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            } else {
                //            NSLog(@"catn remove label");
            }
        }
        
        count.textAlignment = NSTextAlignmentCenter;
        weight.textAlignment = NSTextAlignmentRight;
        weightLbl.textAlignment = NSTextAlignmentLeft;
        rep.textAlignment = NSTextAlignmentRight;
        repLbl.textAlignment = NSTextAlignmentLeft;
        
        weightLbl.textColor = FlatGray;
        repLbl.textColor = FlatGray;
        
        ExerciseSet *data = [exerciseSets objectAtIndex:indexPath.row];
        count.text = [NSString stringWithFormat:@"%ld", [exerciseSets count] - (long)indexPath.row ];
        weight.text = [data.weight stringValue];
        weightLbl.text = [NSString stringWithFormat:@" %@",[Utilities getUnits]];
        rep.text = [data.rep stringValue];
        repLbl.text = @" Reps";
        
        NSArray *repPerSetArray = [exerciseObj.repsPerSet componentsSeparatedByString:@","];
        if (repPerSetArray != nil && ((indexPath.row) + 1 < repPerSetArray.count)) {
            if ([[repPerSetArray objectAtIndex:(indexPath.row + 1)] isEqualToString:@"MAX"])
                userCustomRepsForSet = 20;
            else
                userCustomRepsForSet = [[repPerSetArray objectAtIndex:(indexPath.row + 1)] intValue];
        }
        
        repsPerSet.text = @"";
        
        NSLog(@"is this a drop set %d", [data.isDropSet boolValue]);
        if ([data.isDropSet boolValue]) {
            cell.backgroundColor = FlatMintDark;
            weightLbl.textColor = [UIColor whiteColor];
            repLbl.textColor = [UIColor whiteColor];
            //        cell.leftButtons = @[[MGSwipeButton buttonWithTitle:@"Not DropSet" backgroundColor:FlatMint]];
            //        cell.leftSwipeSettings.transition = MGSwipeTransition3D;
            
        }
        else
            cell.backgroundColor = [UIColor whiteColor];
        //NSLog(@"timestamp %@ %.0f", data.timeStamp, [data.weight floatValue]);
        
        
        //configure right buttons
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate = self;
        
        
        [cell.contentView addSubview:count];
        [cell.contentView addSubview:weight];
        [cell.contentView addSubview:weightLbl];
        [cell.contentView addSubview:rep];
        [cell.contentView addSubview:repLbl];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == myBarbelTableView)
    {
        return 33.0f;
    } else {
        
        NSString *deviceType = [Utilities getIphoneName];
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            return 25;
        }
        return 35;
    }
}


-(IBAction)seatSetting:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Seat height recording is not available in Free Version."];
    } else {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Seat Height"];
        [dialog setMessage:@" "];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 1;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
    }
}
-(IBAction) handSetting:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Hand angle recording is not available in Free Version."];
    } else {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Hand Angle"];
        [dialog setMessage:@" "];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 2;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
    }
}
-(IBAction) timerValue:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Modifying rest timer is not available in Free Version."];
    } else {
        /*
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Additional Rest (sec)"];
        [dialog setMessage:@" "];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 3;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
         */
        [timer addTimeCountedByTime:10];
    }
}

-(IBAction)commentSetting:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Modifying rest timer is not available in Free Version."];
    } else {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Exercise Comment"];
        [dialog setMessage:userComment];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 4;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeAlphabet;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
    }
    
}

#pragma ALERTVIEW - DELEGATE
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *test = [[alertView textFieldAtIndex:0] text];
    if ([test isEqualToString:@""]) {
        // do nothing..
        NSLog(@"did not enter anythign..");
    } else
        if (alertView.tag == 1) {
            NSLog(@"seat height is %@", test);
            seatHeight = [test intValue];
            exMetaInfo.seatHeight = [NSNumber numberWithInt:seatHeight];
            
            //making sure null is not set..
            if (handAngle == 0)
                exMetaInfo.handAngle = [NSNumber numberWithInt:0];
            
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        } else if (alertView.tag == 2) {
            NSLog(@"Hand Height us %@", test);
            handAngle = [test intValue];
            
            //making sure null is not set..
            if (seatHeight == 0)
                exMetaInfo.seatHeight = [NSNumber numberWithInt:0];
            
            exMetaInfo.handAngle = [NSNumber numberWithInt:handAngle];
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        } else if (alertView.tag == 3) {
            // updating settings bundle with new timer...
            timerValueToCountDown = [test intValue];
        } else if (alertView.tag == 4) {
            userComment = test;
            exMetaInfo.comment = userComment;
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveOnlySelfAndWait];
        }
    [self loadExerciseMetaInfo];
    
}

-(void) loadExerciseMetaInfo {
    exMetaInfo = [Utilities getExerciseMetaInfo:exerciseObj.exerciseName];

    if (exMetaInfo == nil) {
        NSLog(@"meta info absent");
        exMetaInfo = [ExerciseMetaInfo MR_createEntityInContext:localContext];
        exMetaInfo.exerciseName = exerciseObj.exerciseName;
        exMetaInfo.muscle = exerciseObj.muscle;
        exMetaInfo.seatHeight = [NSNumber numberWithInt:0];
        exMetaInfo.handAngle= [NSNumber numberWithInt:0];
        exMetaInfo.maxWeight = [NSNumber numberWithFloat:0];
        exMetaInfo.maxRep = [NSNumber numberWithInt:0];
        exMetaInfo.maxDate = _date;
        exMetaInfo.syncedState = [NSNumber numberWithBool:false];
        exMetaInfo.comment = @"";
        [localContext MR_saveToPersistentStoreAndWait];
        seatHeight = 0;
        handAngle = 0;
    } else {
        seatHeight = [exMetaInfo.seatHeight intValue];
        handAngle = [exMetaInfo.handAngle intValue];
        userComment = exMetaInfo.comment;
       // NSLog(@"meta info present %d %d", seatHeight, handAngle);
    }
    maxWeight = [exMetaInfo.maxWeight floatValue];
    maxRep = [exMetaInfo.maxRep intValue];
    [self loadMetaInfo];
}

-(void) loadMetaInfo {
    // seat btn text
    if ( exMetaInfo != nil) {
        [seatBtn setTitle:[NSString stringWithFormat:@"%d", seatHeight] forState:UIControlStateNormal];
    } else
        [seatBtn setTitle:@"0" forState:UIControlStateNormal];
    
    
    if (exMetaInfo != nil)
        [handBtn setTitle:[NSString stringWithFormat:@"%d", handAngle] forState:UIControlStateNormal];
    else
        [seatBtn setTitle:@"0" forState:UIControlStateNormal];
    
    //[timerBtn setTitle:[NSString stringWithFormat:@"%d", timerValueToCountDown] forState:UIControlStateNormal];

}

-(void)dismissKeyboard {
    NSLog(@"trying to dismiss keyboard");
    [weightText resignFirstResponder];
    [repText resignFirstResponder];
}

-(void) showPurchasePopUp: (NSString *) feature {
    [self dismissKeyboard];
    
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY WORKOUT PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_WORKOUT_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium Package or Workout Package.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

#pragma mark barbellcalculator
-(void)doneWithNumberPad{
    NSLog(@"show additional keyboard based on option..");
    [weightText resignFirstResponder];
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Plate calculator is not available in Free verion."];
    } else {
        [self showBarbelPopup];
    }
}

-(void) showBarbelPopup {
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"PlateCalculator" withParameters:articleParams];
    
    weightLbsArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      nil];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title;
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Select plates on one side only." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12], NSParagraphStyleAttributeName : paragraphStyle}];
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    if (lbORkg == 0)
        title = [[NSAttributedString alloc] initWithString:@"Barbell Weight (Lbs)" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    else
        title = [[NSAttributedString alloc] initWithString:@"Barbell Weight (Kgs)" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.attributedText = title;
    
    NSString *deviceType = [Utilities getIphoneName];
    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        myBarbelTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, 300, 100) style:UITableViewStylePlain];
    } else {
        myBarbelTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, 350, 150) style:UITableViewStylePlain];
    }
    myBarbelTableView.delegate = self;
    myBarbelTableView.dataSource = self;
    myBarbelTableView.tag = 501;
    myBarbelTableView.backgroundColor = FlatWhiteDark;
    
    TNRectangularRadioButtonData *bbZ = [[TNRectangularRadioButtonData alloc] init];
    bbZ.selected = NO;
    bbZ.borderColor = [UIColor blackColor];
    bbZ.rectangleColor = [UIColor blackColor];
    bbZ.borderWidth = bbZ.borderHeight = 12;
    bbZ.rectangleWidth = bbZ.rectangleHeight = 5;
    
    
    TNRectangularRadioButtonData *bbSm = [[TNRectangularRadioButtonData alloc] init];
    bbSm.selected = NO;
    bbSm.borderColor = [UIColor blackColor];
    bbSm.rectangleColor = [UIColor blackColor];
    bbSm.borderWidth = bbSm.borderHeight = 12;
    bbSm.rectangleWidth = bbSm.rectangleHeight = 5;
    
    
    TNRectangularRadioButtonData *bbMd = [[TNRectangularRadioButtonData alloc] init];
    bbMd.selected = NO;
    bbMd.borderColor = [UIColor blackColor];
    bbMd.rectangleColor = [UIColor blackColor];
    bbMd.borderWidth = bbMd.borderHeight = 12;
    bbMd.rectangleWidth = bbMd.rectangleHeight = 5;
    
    TNRectangularRadioButtonData *bbLg = [[TNRectangularRadioButtonData alloc] init];
    bbLg.selected = YES;
    bbLg.borderColor = [UIColor blackColor];
    bbLg.rectangleColor = [UIColor blackColor];
    bbLg.borderWidth = bbLg.borderHeight = 12;
    bbLg.rectangleWidth = bbLg.rectangleHeight = 5;
    
    if (lbORkg == 0) {
        bbZ.labelText = @"0 (BW)";
        bbZ.identifier = @"0";
        
        bbSm.labelText = @"25";
        bbSm.identifier = @"25";
        
        bbMd.labelText = @"35";
        bbMd.identifier = @"35";
        
        bbLg.labelText = @"45";
        bbLg.identifier = @"45";
        
        totalWeight = barbellWeight = 45;
    } else {
        bbZ.labelText = @"0 (BW)";
        bbZ.identifier = @"0";
        
        bbSm.labelText = @"10";
        bbSm.identifier = @"10";
        
        bbMd.labelText = @"15";
        bbMd.identifier = @"15";
        
        bbLg.labelText = @"20";
        bbLg.identifier = @"20";
        
        totalWeight = barbellWeight = 20;
        
    }
    
    barbellGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[bbZ, bbSm, bbMd, bbLg] layout:TNRadioButtonGroupLayoutHorizontal];
    barbellGroup.identifier = @"Barbell";
    barbellGroup.position = CGPointMake(CGRectGetMidX(self.view.frame), 0);
    
    [barbellGroup create];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(babellGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:barbellGroup];
    
    UIView *topView;
    
    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 30)];
    } else {
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 350, 30)];
    }
    //topView.backgroundColor = FlatWhite;
    
    calculatedWeightLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(topView.frame)/2, 30)];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", barbellWeight];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(calculatedWeightLbl.frame), 0, CGRectGetWidth(topView.frame)/2, 30)];
    [button setTitleColor:FlatBlueDark forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
    
    [button setTitle:@"DONE" forState:UIControlStateNormal];
    button.selectionHandler = ^(CNPPopupButton *button){
        [self checkSaveBtn];
        [self.barbelPopupController dismissPopupControllerAnimated:YES];
    };
    
    [topView addSubview:calculatedWeightLbl];
    [topView addSubview:button];
    
    self.barbelPopupController = [[CNPPopupController alloc] initWithContents:@[topView, titleLabel, lineOneLabel, barbellGroup, myBarbelTableView]];//, lineOneLabel, imageView, lineTwoLabel, customView, button]];
    self.barbelPopupController.theme = [CNPPopupTheme defaultTheme];
    self.barbelPopupController.theme.popupStyle = CNPPopupStyleActionSheet;
    self.barbelPopupController.theme.maskType = CNPPopupMaskTypeClear;
    self.barbelPopupController.theme.popupContentInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    self.barbelPopupController.theme.contentVerticalPadding = 10;
    self.barbelPopupController.theme.backgroundColor = FlatWhiteDark;
    [self.barbelPopupController presentPopupControllerAnimated:YES];
}

-(void) textViewDidChange:(UITextView *)textView {
    if (textView.tag == 501 || textView.tag == 502 || textView.tag == 503) {
        // warmup stuff
    } else {
        NSLog(@"text view changed..");
        [self checkSaveBtn];
    }
}


-(void) checkSaveBtn {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSLog(@"WT: %@, RT: %@, %@, %ld SVBTN: %d",weightText.text, repText.text, [f numberFromString:weightText.text], (long)weightText.tag, [saveBtn isEnabled]);

    if ([weightText.text isEqualToString:@""] ||
        [weightText.text isEqualToString:@"0.0"] ||
        [weightText.text isEqualToString:@"0"] ||
        ([weightText.text floatValue] > 1000) ||
        [repText.text isEqualToString:@""] ||
        [repText.text isEqualToString:@"0"] ||
        [repText.text isEqualToString:@"0.0"] ||
        [repText.text floatValue] > 100) {
        
        saveBtn.backgroundColor = FlatGray;
        saveBtn.enabled = false;
        NSLog(@"somethign is 0");
    } else if ([f numberFromString:weightText.text] < 0 || [f numberFromString:repText.text] <= 0) {
        saveBtn.backgroundColor = FlatGray;
        saveBtn.enabled = false;
        NSLog(@"something is < 0");
    } else {
        saveBtn.backgroundColor = FlatMint;
        saveBtn.enabled = true;
        NSLog(@"everything is dandy");
    }
    NSLog(@"SaveBen %d", [saveBtn isEnabled]);
}

-(void)timerLabel:(MZTimerLabel*)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime{
    //time is up, what should I do master?
    if (timerLabel.tag == 101) {
        //        timerBWEx.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        //        timerBWEx.text = @"Times Up. Lets LIFT.";
        //        [timerBWEx reset];
        //        [self loadNextExercise:self];
    } else if (timerLabel.tag == 100) {
        NSLog(@"in here...");
        AVAudioSession *audioSession = [AVAudioSession sharedInstance];
        
        NSError *setCategoryError = nil;
        [audioSession setCategory:AVAudioSessionCategoryPlayback
                      withOptions:AVAudioSessionCategoryOptionMixWithOthers error:&setCategoryError];
        
        NSError *activationError = nil;
        [audioSession setActive:YES error:&activationError];

        /*
        AVSpeechUtterance *utterance = [AVSpeechUtterance speechUtteranceWithString:speak];
        utterance.rate = 0.4f;
        utterance.voice = [AVSpeechSynthesisVoice voiceWithLanguage:[AVSpeechSynthesisVoice currentLanguageCode]];
        
        AVSpeechSynthesizer *synth = [[AVSpeechSynthesizer alloc] init];
        [synth speakUtterance:utterance];
        */
        AudioServicesPlaySystemSound(1005);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);

        [self.popupController dismissPopupControllerAnimated:YES];
        // timer.hidden = YES;
        [timer pause];
        [timer reset];
    }
    
}

-(void) buttonsBeautification {
    
    // hand btn text
    // timer btn test
    saveBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    saveBtn.layer.cornerRadius = 5;
    
    cllearBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    cllearBtn.layer.cornerRadius = 5;
    
    UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *RPmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *RMmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *RPmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *RMmaskLayer = [[CAShapeLayer alloc] init];
    
    WPmaskLayer.frame = self.view.bounds;
    WPmaskLayer.path  = WPmaskPath.CGPath;
    WMmaskLayer.frame = self.view.bounds;
    WMmaskLayer.path  = WMmaskPath.CGPath;
    RPmaskLayer.frame = self.view.bounds;
    RPmaskLayer.path  = RPmaskPath.CGPath;
    RMmaskLayer.frame = self.view.bounds;
    RMmaskLayer.path  = RMmaskPath.CGPath;
    
    
    weightPlus.layer.mask = WPmaskLayer;
    weightMinus.layer.mask = WMmaskLayer;
    repPlus.layer.mask = RPmaskLayer;
    repMinus.layer.mask = RMmaskLayer;
    
    weightPlus.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
    weightMinus.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
    repPlus.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
    repMinus.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
}


-(void) setFooterView: (int) exNumber {
    NSString *redText = [NSString stringWithFormat:@"%d", exNumber];
    NSString *greenText = @"of";
    NSString *purpleBoldText = [NSString stringWithFormat:@"%d", totalEx];
    
    NSString *text = [NSString stringWithFormat:@"%@ %@ %@",
                      redText,
                      greenText,
                      purpleBoldText];
    
    // If attributed text is supported (iOS6+)
    if ([footer respondsToSelector:@selector(setAttributedText:)]) {
        UIFont *boldFont = [UIFont fontWithName:@HAL_BOLD_FONT size:footer.font.pointSize];
        UIFont *italicFont = [UIFont italicSystemFontOfSize:footer.font.pointSize];
        // Define general attributes for the entire text
        NSDictionary *attribs = @{
                                  NSForegroundColorAttributeName: footer.textColor,
                                  NSFontAttributeName: footer.font
                                  };
        NSMutableAttributedString *attributedText =
        [[NSMutableAttributedString alloc] initWithString:text
                                               attributes:attribs];
        
        // Red text attributes
        UIColor *redColor = [UIColor whiteColor];
        NSRange redTextRange = [text rangeOfString:redText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:redColor,
                                        NSFontAttributeName:boldFont}
                                range:redTextRange];
        
        // Green text attributes
        UIColor *greenColor = FlatWhite;
        NSRange greenTextRange = [text rangeOfString:greenText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:greenColor,
                                        NSFontAttributeName:italicFont}
                                range:greenTextRange];
        
        // Purple and bold text attributes
        UIColor *purpleColor = [UIColor whiteColor];
        NSRange purpleBoldTextRange = [text rangeOfString:purpleBoldText];// * Notice that usage of rangeOfString in this case may cause some bugs - I use it here only for demonstration
        [attributedText setAttributes:@{NSForegroundColorAttributeName:purpleColor,
                                        NSFontAttributeName:boldFont}
                                range:purpleBoldTextRange];
        
        footer.attributedText = attributedText;
    }
    // If attributed text is NOT supported (iOS5-)
    else {
        footer.text = text;
    }
}


-(void) showPurchasePopUpHistory {
    [self dismissKeyboard];
    
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY WORKOUT PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_WORKOUT_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"Last workout information if not available in Free version. Please upgrade to Premium Package or Workout Package."];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
    
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    if (segmentedControl.selectedSegmentIndex == 0)
        _history.text = historyStr;
    else {
        if (exMetaInfo != nil) {
            if ([exMetaInfo.maxRep intValue] != 0 && [exMetaInfo.maxWeight floatValue] != 0) {
                if ([Utilities showWorkoutPackage]) {
                    _history.text = [NSString stringWithFormat:@"Set PR of %d x %.1f on UPGRADE TO PREMIUM", [exMetaInfo.maxRep intValue], [exMetaInfo.maxWeight floatValue]];
                } else {
                    _history.text = [NSString stringWithFormat:@"Set PR of %d x %.1f on %@", [exMetaInfo.maxRep intValue], [exMetaInfo.maxWeight floatValue], exMetaInfo.maxDate];
                }
            }
            else
                _history.text = @"No PR Yet.";
        } else
            _history.text = @"No PR Yet";
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == myBarbelTableView) {
        // do nothing
        [self checkSaveBtn];
    } else {
        int rowSelected = (int)[[exerciseSetsView indexPathsForSelectedRows] count];
        NSLog(@"rows selected %d, Dropset %d", rowSelected, isDropSetVisible);
        if (rowSelected == 1) {
            ExerciseSet *data = [exerciseSets objectAtIndex:indexPath.row];
            weightText.text = [data.weight stringValue];
            repText.text = [data.rep stringValue];
            [saveBtn setTitle:@"Update" forState:UIControlStateNormal];
            updateField = true;
            selectedIndex = (int) indexPath.row;
        } else if (isDropSetVisible == false) {
            additionOptionView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44)];
            
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                additionOptionView.frame = CGRectMake(0, CGRectGetHeight(self.view.frame) - 44, CGRectGetWidth(self.view.frame), 44);
                //self.headerView.frame  = CGRectMake(0, 5, 320,0);
                
            } completion:^(BOOL finished) {
                
            }];
            
            additionOptionView.backgroundColor = FlatMintDark;
            
            
            unMarkDropSet = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(self.view.frame)/2 - 20, 35)];
            unMarkDropSet.backgroundColor = [UIColor whiteColor];
            unMarkDropSet.layer.cornerRadius = 5.0f;
            [unMarkDropSet setTitle:@"Not Drop Set" forState:UIControlStateNormal];
            [unMarkDropSet setTitleColor:FlatMintDark forState:UIControlStateNormal];
            [unMarkDropSet addTarget:self action:@selector(unMarkDropSet:) forControlEvents:UIControlEventTouchUpInside];
            
            markDropSet = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(self.view.frame)/2 + 10, 5, CGRectGetWidth(self.view.frame)/2 - 20, 35)];
            markDropSet.backgroundColor = [UIColor whiteColor];
            markDropSet.layer.cornerRadius = 5.0f;
            [markDropSet setTitle:@"Drop Set" forState:UIControlStateNormal];
            [markDropSet setTitleColor:FlatMintDark forState:UIControlStateNormal];
            [markDropSet addTarget:self action:@selector(markDropSet:) forControlEvents:UIControlEventTouchUpInside];
            
            [additionOptionView addSubview:unMarkDropSet];
            [additionOptionView addSubview:markDropSet];
            
            [self.view addSubview:additionOptionView];
            
            NSLog(@"may be a dropset...");
            weightText.text = @"";
            repText.text = @"";
            [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
            updateField = false;
            isDropSetVisible = true;
            
        }
    }
}
-(IBAction)markDropSet:(id)sender {
    
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Drop set support is not available in Free Version."];
    } else {
        
        NSLog(@"selected rows are mark drop set are : %@",[exerciseSetsView indexPathsForSelectedRows]);
        //[additionOptionView removeFromSuperview];
        
        for (NSIndexPath *index in [exerciseSetsView indexPathsForSelectedRows]) {
            NSLog(@"index is %ld", (long)index.row);
            ExerciseSet *set = [exerciseSets objectAtIndex:index.row];
            set.isDropSet = [NSNumber numberWithBool:true];
            set.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            isDropSetVisible = false;
        } completion:^(BOOL finished) {
            [exerciseSetsView reloadData];
            [Utilities syncDropSetToParse:_date];
        }];
    }
}

-(IBAction)unMarkDropSet:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Drop set support is not available in Free Version."];
    } else {
        NSLog(@"selected rows for unmark drop set are: %@",[exerciseSetsView indexPathsForSelectedRows]);
        //[additionOptionView removeFromSuperview];
        
        for (NSIndexPath *index in [exerciseSetsView indexPathsForSelectedRows]) {
            NSLog(@"index is %ld", (long)index.row);
            ExerciseSet *set = [exerciseSets objectAtIndex:index.row];
            set.isDropSet = [NSNumber numberWithBool:false];
            set.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            isDropSetVisible = false;
        } completion:^(BOOL finished) {
            [exerciseSetsView reloadData];
            [Utilities syncDropSetToParse:_date];
        }];
    }
    
}

- (IBAction)stepperValueDidChanged:(UIStepper *)sender {
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    // assuming your view controller is a subclass of UITableViewController, for example.
    NSIndexPath *indexPath = [myBarbelTableView indexPathForCell:cell];
    
    [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:@""];
    
    totalWeight = 0;
    
    float weightIndexLbVal;
    if (lbORkg == 0) {
        weightIndexLbVal = [[weightLbsArray objectAtIndex:indexPath.row] floatValue];
        if ((int)[sender value] > weightIndexLbVal) {
            // increment clicked..
            NSLog(@"increment clicked");
            [weightLbsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal + 1]];
        } else if ((int)[sender value] < weightIndexLbVal) {
            // decrement clicked
            NSLog(@"Decrement clicked");
            [weightLbsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal - 1]];
        }
        
        
        [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:[NSString stringWithFormat:@"%d", (int) [sender value]]];
        
        for (int i = 0; i < [weightLbsArray count]; i++) {
            totalWeight += [[weightLbVal objectAtIndex:i] floatValue] * [[weightLbsArray objectAtIndex:i] floatValue] * 2;
        }
    } else {
        weightIndexLbVal = [[weightKgsArray objectAtIndex:indexPath.row] floatValue];
        if ((int)[sender value] > weightIndexLbVal) {
            // increment clicked..
            NSLog(@"increment clicked");
            [weightKgsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal + 1]];
        } else if ((int)[sender value] < weightIndexLbVal) {
            // decrement clicked
            NSLog(@"Decrement clicked");
            [weightKgsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal - 1]];
        }
        
        
        [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:[NSString stringWithFormat:@"%d", (int) [sender value]]];
        
        for (int i = 0; i < [weightKgsArray count]; i++) {
            totalWeight += [[weightKgVal objectAtIndex:i] floatValue] * [[weightKgsArray objectAtIndex:i] floatValue] * 2;
        }
    }
    
    totalWeight += barbellWeight;
    weightText.text = [NSString stringWithFormat:@"%.1f", totalWeight];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", totalWeight];
    [self checkSaveBtn];
}

- (void)babellGroupUpdated:(NSNotification *)notification {
    NSLog(@"[MainView] barbell group updated to %@", barbellGroup.selectedRadioButton.data.identifier);
    totalWeight -= barbellWeight;
    
    if (lbORkg == 0) {
        if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"0"])
            barbellWeight = 0;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"25"])
            barbellWeight = 25;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"35"])
            barbellWeight = 35;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"45"])
            barbellWeight = 45;
    } else {
        if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"0"])
            barbellWeight = 0;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"10"])
            barbellWeight = 10;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"15"])
            barbellWeight = 15;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"20"])
            barbellWeight = 20;
    }
    
    totalWeight += barbellWeight;
    weightText.text = [NSString stringWithFormat:@"%.1f", totalWeight];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", totalWeight];
    
}
- (UIImage *)emptyImageForSteper
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 1));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No set performed on %@.", _date];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Record a set by entering \nweight, reps and then clicking \"save\" ";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    
    return [Utilities getMuscleImage:exerciseObj.majorMuscle];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

#pragma mark Swipe Delegate

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    NSIndexPath * path = [exerciseSetsView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSLog(@"delete pressed");
        
        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert addButton:@"Yes" actionBlock:^{
            ExerciseSet *data = [exerciseSets objectAtIndex:path.row];
            [Utilities deleteExerciseSetToParse:data.date timeStamp:data.timeStamp];
            [data MR_deleteEntityInContext:localContext];
            [localContext MR_saveToPersistentStoreAndWait];
            
            [self reloadExercise];
            // if user selected an index and then deleted the highlighted index and then updated the field and clicked. The crash happens. So when we are deleting an entry, we need to make sure everything is reset along with saveBtn
            [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
            saveBtn.titleLabel.textColor = [UIColor whiteColor];
            updateField = false;
            weightText.text = @"";
            selectedIndex = 0;
            [self checkSaveBtn];
            
        }];
        [alert showWarning:self title:@"Delete entry?" subTitle:@"Are you sure you want to delete this set?" closeButtonTitle:@"No" duration:0.0f];
        return NO; //Don't autohide to improve delete expansion animation
    } else if (direction == MGSwipeDirectionLeftToRight && index == 0) {
        /* // this is not called anymore...
         NSLog(@"not a drop set pressed");
         ExerciseSet *data = [exerciseSets objectAtIndex:path.row];
         data.isDropSet = [NSNumber numberWithBool:false];
         data.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
         [localContext MR_saveToPersistentStoreAndWait];
         [self reloadExercise];
         [Utilities updateExerciseSetToParse:data];
         return NO;
         */
    }
    
    return YES;
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSLog(@"preparing %@", segue.identifier);
    
    if ([segue.identifier isEqualToString:@"exerciseHistory"]) {
        ExerciseHistoryTVC *destVC = segue.destinationViewController;
        destVC.exerciseObj = exerciseObj.exerciseName;
    } else if ([segue.identifier isEqualToString:@"showSummarySegue"]) {
        ExerciseSummary *destVC = segue.destinationViewController;
        destVC.date = _date;
        destVC.routineName = _routineName;
        destVC.workoutName = _workoutName;
    } else if ([segue.identifier isEqualToString:@"addTempExerciseSegue"]) {
        MuscleListTVC *dest = segue.destinationViewController;
        dest.date = _date;
        dest.routineName = _routineName;
        dest.workoutName = _workoutName;
        dest.exerciseNumInGroup = [NSNumber numberWithInt:0];
        NSLog(@"grounp number is %@", [NSNumber numberWithInt:(int)[exerciseGroups count]]);
        dest.exerciseGroup = [NSNumber numberWithInt:(int)[exerciseGroups count]];
        dest.isTempEx = true;
    } else if ([segue.identifier isEqualToString:@"shareSegue"]) {
        ShareViewController *destVC = segue.destinationViewController;
        destVC.date = _date;
        self.title = @"";
    }
    

    
}

-(void) showCoachMarks {
    
    bool showHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownRecWorkoutHint"];
    
    if (showHint == false) {
    float width = self.view.frame.size.width;
   // float bottomHeight =  (self.view.frame.size.height/2)/3;
    
    CGRect coachmark1 = CGRectMake(0, CGRectGetHeight(self.view.frame)/2, width, 0);
    CGRect coachmark2 = CGRectMake(0, CGRectGetMaxY(coachmark1), width, 0);
    CGRect coachmark3 = CGRectMake(20, CGRectGetMaxY(_collectionView.frame) - 30, width - 40, 30);
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Swipe LEFT/RIGHT to view next and previous exercise.",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"showArrow":[NSNumber numberWithBool:NO]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"Swipe UP/DOWN to view multi-set exercises (if present).",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                @"showArrow":[NSNumber numberWithBool:NO]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"Record Seat height, hand angle, additional rest and exercise comments.",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                }
//                            @{
//                                @"rect": [NSValue valueWithCGRect:coachmark4],
//                                @"caption": @"To analyze your workout, click here.",
//                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
//                                @"showArrow":[NSNumber numberWithBool:YES]
//                                }
                            
                            ];
    
    coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        coachMarksView.delegate = self;
        coachMarksView.enableContinueLabel = false;
        coachMarksView.enableSkipButton =false;
    [self.view addSubview:coachMarksView];
    [coachMarksView start];
    }
}

#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks");
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownRecWorkoutHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark timer
-(void)timerLabel:(MZTimerLabel *)timerLabel countingTo:(NSTimeInterval)time timertype:(MZTimerLabelType)timerType {
    if (timerLabel == timer) {
        if ((int)time %2 == 0) {
            timer.textColor = [UIColor whiteColor];
        } else {
            timer.textColor = [UIColor blackColor];
        }
    }
}


- (void)showMoreOptionMenu {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"ADDITIONAL OPTIONS" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:18], NSParagraphStyleAttributeName : paragraphStyle}];

    
    CNPPopupButton *addExercisebtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [addExercisebtn setTitleColor:FlatBlueDark forState:UIControlStateNormal];
    addExercisebtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    [addExercisebtn setTitle:@"ADD EXERCISE" forState:UIControlStateNormal];
    addExercisebtn.backgroundColor = [UIColor whiteColor];
    addExercisebtn.layer.cornerRadius = 4;
    addExercisebtn.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self quickAddExercise:self];
    };
    
    CNPPopupButton *historyBtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [historyBtn setTitleColor:FlatCoffeeDark forState:UIControlStateNormal];
    historyBtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    [historyBtn setTitle:@"HISTORY" forState:UIControlStateNormal];
    historyBtn.backgroundColor = [UIColor whiteColor];
    historyBtn.layer.cornerRadius = 4;
    historyBtn.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [self viewHistory:self];
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatOrangeDark;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, addExercisebtn, historyBtn]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    [self.popupController presentPopupControllerAnimated:YES];
}

- (void)warmupSetsOptionMenu {
    
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Warm-up Set calculator is not available in Free verion."];
        return;
    }
    
    NSPredicate *exNamePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseObj.exerciseName];
    ExerciseList *exTypeBodyweight = [ExerciseList MR_findFirstWithPredicate:exNamePredicate];
    if (![exTypeBodyweight.equipment isEqualToString:@"Barbell"]) {
        //            [self.view makeToast:@"For bodyweight exercises, the default weight is set to 1."
        //                    duration:2
        //                    position:CSToastPositionTop
        //                       title:@"Info"];
        [EHPlainAlert showAlertWithTitle:@"Infomation" message:@"Warmup Calculator is build for exercises with Barbell. " type:ViewAlertUnknown];
        [EHPlainAlert updateHidingDelay:5];
    }

    float itemHeight = 30;
    float popupWidth = 300;//CGRectGetWidth(self.view.frame) - 10
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"WARMUP CALCULATOR" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UIView *weightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, popupWidth, itemHeight)];
    
    UILabel *wwLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, popupWidth/2, itemHeight)];
    wwLbl.text = [NSString stringWithFormat:@"Working Weight (%@)", [Utilities getUnits]];
    wwLbl.numberOfLines = 0;
    [wwLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
    
    UIButton *weightMinusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(wwLbl.frame), 0, 30, itemHeight)];
    [weightMinusWarmup setTitle:@"-" forState:UIControlStateNormal];
    weightMinusWarmup.backgroundColor = FlatMintDark;
    [weightMinusWarmup addTarget:self action:@selector(weightDecWarmUp:) forControlEvents:UIControlEventTouchUpInside];
    
    workingWeightTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightMinusWarmup.frame), 0, popupWidth/4, itemHeight)];
//    workingWeightTextFied.placeholder = [NSString stringWithFormat:@"Working Weight (%@)", [Utilities getUnits]];
    workingWeightTextFied.textAlignment = NSTextAlignmentCenter;
    workingWeightTextFied.keyboardType = UIKeyboardTypeDecimalPad;
    workingWeightTextFied.tag = 101;
    workingWeightTextFied.delegate = self;
    workingWeightTextFied.textContainer.maximumNumberOfLines = 1;
    workingWeightTextFied.scrollEnabled = false;
//    workingWeightTextFied.floatingLabelYPadding = 0;
    workingWeightTextFied.placeholderYPadding = -16;
    workingWeightTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
    workingWeightTextFied.text = [NSString stringWithFormat:@"%.1f", lastWorkoutFinalWeight];

    UIButton *weightPlusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(workingWeightTextFied.frame), 0, 30, itemHeight)];
    [weightPlusWarmup setTitle:@"+" forState:UIControlStateNormal];
    weightPlusWarmup.backgroundColor = FlatMintDark;
    [weightPlusWarmup addTarget:self action:@selector(weightIncWarmUp:) forControlEvents:UIControlEventTouchUpInside];

//    UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightPlusWarmup.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];    
//    UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightMinusWarmup.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
//    
//    CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
//    CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];
//    
//    WPmaskLayer.frame = self.view.bounds;
//    WPmaskLayer.path  = WPmaskPath.CGPath;
//    WMmaskLayer.frame = self.view.bounds;
//    WMmaskLayer.path  = WMmaskPath.CGPath;
//
//    weightPlusWarmup.layer.mask = WPmaskLayer;
//    weightMinusWarmup.layer.mask = WMmaskLayer;
    
    [weightView addSubview:wwLbl];
    [weightView addSubview:weightMinusWarmup];
    [weightView addSubview:workingWeightTextFied];
    [weightView addSubview:weightPlusWarmup];
    
    
    UIView *repView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, popupWidth, itemHeight)];
    UILabel *repLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, popupWidth/2, itemHeight)];
    repLbl.text = [NSString stringWithFormat:@"Max Reps in Warmup"];
    repLbl.numberOfLines = 0;
    [repLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];

    UIButton *maxRepMinusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repLbl.frame), 0, 30, itemHeight)];
    [maxRepMinusWarmup setTitle:@"-" forState:UIControlStateNormal];
    maxRepMinusWarmup.backgroundColor = FlatMintDark;
    [maxRepMinusWarmup addTarget:self action:@selector(repDecWarmUp:) forControlEvents:UIControlEventTouchUpInside];
    
    warmupMaxRepTV = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightMinusWarmup.frame), 0, popupWidth/4, itemHeight)];
    //warmupMaxRepTV.placeholder = [NSString stringWithFormat:@"Max Rep"];
    warmupMaxRepTV.textAlignment = NSTextAlignmentCenter;
    warmupMaxRepTV.keyboardType = UIKeyboardTypeDecimalPad;
    warmupMaxRepTV.tag = 502;
    warmupMaxRepTV.delegate = self;
    warmupMaxRepTV.textContainer.maximumNumberOfLines = 1;
    warmupMaxRepTV.scrollEnabled = false;
//    warmupMaxRepTV.floatingLabelYPadding = 0;
    warmupMaxRepTV.placeholderYPadding = -16;
    warmupMaxRepTV.floatingLabel.textAlignment = NSTextAlignmentCenter;
    if (warmupRep == 0)
        warmupRep = 10;
    
    warmupMaxRepTV.text = [NSString stringWithFormat:@"%d", warmupRep];
    
    UIButton *maxRepPlusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(warmupMaxRepTV.frame), 0, 30, itemHeight)];
    [maxRepPlusWarmup setTitle:@"+" forState:UIControlStateNormal];
    maxRepPlusWarmup.backgroundColor = FlatMintDark;
    [maxRepPlusWarmup addTarget:self action:@selector(repIncWarmUp:) forControlEvents:UIControlEventTouchUpInside];
    [repView addSubview:repLbl];
    [repView addSubview:maxRepMinusWarmup];
    [repView addSubview:warmupMaxRepTV];
    [repView addSubview:maxRepPlusWarmup ];
    
    UIView *setView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, popupWidth, itemHeight)];
    UILabel *setLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, popupWidth/2, 30)];
    setLbl.text = [NSString stringWithFormat:@"Max Sets in Warmup"];
    setLbl.numberOfLines = 0;
    [setLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];

    
    UIButton *maxSetMinusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLbl.frame), 0, 30, itemHeight)];
    [maxSetMinusWarmup setTitle:@"-" forState:UIControlStateNormal];
    maxSetMinusWarmup.backgroundColor = FlatMintDark;
    [maxSetMinusWarmup addTarget:self action:@selector(setDecWarmUp:) forControlEvents:UIControlEventTouchUpInside];
    
    warmupMaxSetTV = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(maxSetMinusWarmup.frame), 0, popupWidth/4, itemHeight)];
    //warmupMaxSetTV.placeholder = [NSString stringWithFormat:@"Max Set"];
    warmupMaxSetTV.textAlignment = NSTextAlignmentCenter;
    warmupMaxSetTV.keyboardType = UIKeyboardTypeDecimalPad;
    warmupMaxSetTV.tag = 503;
    warmupMaxSetTV.delegate = self;
    warmupMaxSetTV.textContainer.maximumNumberOfLines = 1;
    warmupMaxSetTV.scrollEnabled = false;
//    warmupMaxSetTV.floatingLabelYPadding = 0;
    warmupMaxSetTV.placeholderYPadding = -16;
    warmupMaxSetTV.floatingLabel.textAlignment = NSTextAlignmentCenter;
    if (warmupSets == 0)
        warmupSets = 4;
    warmupMaxSetTV.text = [NSString stringWithFormat:@"%d", warmupSets];
    
    UIButton *maxSetPlusWarmup = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(warmupMaxSetTV.frame), 0, 30, itemHeight)];
    [maxSetPlusWarmup setTitle:@"+" forState:UIControlStateNormal];
    maxSetPlusWarmup.backgroundColor = FlatMintDark;
    [maxSetPlusWarmup addTarget:self action:@selector(setIncWarmUp:) forControlEvents:UIControlEventTouchUpInside];
    [setView addSubview:setLbl];
    [setView addSubview:maxSetMinusWarmup];
    [setView addSubview:warmupMaxSetTV];
    [setView addSubview:maxSetPlusWarmup ];
    customSetsListView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 180)];
    
    CNPPopupButton *closeBtn = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, itemHeight)];
    [closeBtn setTitleColor:FlatCoffeeDark forState:UIControlStateNormal];
    closeBtn.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    [closeBtn setTitle:@"CLOSE" forState:UIControlStateNormal];
    closeBtn.backgroundColor = [UIColor whiteColor];
    closeBtn.layer.cornerRadius = 4;
    closeBtn.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatOrangeDark;
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, weightView, repView, setView, customSetsListView, closeBtn]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    //self.popupController.theme.popupContentInsets = UIEdgeInsetsMake(16, 10, 16, 10);
    [self.popupController presentPopupControllerAnimated:YES];
    [self stepperValueChanged];
}
-(IBAction)weightDecWarmUp:(id)sender {
    float currentValue = [workingWeightTextFied.text floatValue];
    currentValue -= 5;
    workingWeightTextFied.text = [NSString stringWithFormat:@"%.1f", currentValue];
    [self stepperValueChanged];
}

-(IBAction)weightIncWarmUp:(id)sender {
    float currentValue = [workingWeightTextFied.text floatValue];
    currentValue += 5;
    workingWeightTextFied.text = [NSString stringWithFormat:@"%.1f", currentValue];
    [self stepperValueChanged];
}

-(IBAction)repDecWarmUp:(id)sender {
    int currentValue = [warmupMaxRepTV.text floatValue];
    currentValue -= 1;
    if (currentValue < 1) {
        [EHPlainAlert showAlertWithTitle:@"Error" message:@"Minimum Reps cannot be less than 1." type:ViewAlertError];
    } else {
        warmupMaxRepTV.text = [NSString stringWithFormat:@"%d", currentValue];
        [self stepperValueChanged];
    }
}

-(IBAction)repIncWarmUp:(id)sender {
    int currentValue = [warmupMaxRepTV.text floatValue];
    currentValue += 1;
    if (currentValue > 10) {
        [EHPlainAlert showAlertWithTitle:@"Error" message:@"Maximum Reps cannot be more than 10." type:ViewAlertError];
    } else {
        warmupMaxRepTV.text = [NSString stringWithFormat:@"%d", currentValue];
        [self stepperValueChanged];
    }
}
-(IBAction)setDecWarmUp:(id)sender {
    int currentValue = [warmupMaxSetTV.text floatValue];
    currentValue -= 1;
    if (currentValue < 2) {
        [EHPlainAlert showAlertWithTitle:@"Error" message:@"Minimum Sets cannot be less than 2." type:ViewAlertError];
    } else {
        warmupMaxSetTV.text = [NSString stringWithFormat:@"%d", currentValue];
        [self stepperValueChanged];
    }
}

-(IBAction)setIncWarmUp:(id)sender {
    int currentValue = [warmupMaxSetTV.text floatValue];
    currentValue += 1;
    if (currentValue > 8) {
        [EHPlainAlert showAlertWithTitle:@"Error" message:@"Maximum Sets cannot be more than 8." type:ViewAlertError];
    } else {
        warmupMaxSetTV.text = [NSString stringWithFormat:@"%d", currentValue];
        [self stepperValueChanged];
    }
}

- (void)stepperValueChanged {
    
    NSDictionary *repsLoadRation = @{@2:@{@"Reps": @[@100, @50],
                                            @"Load": @[@40, @70]},
                                     
                                     @3:@{@"Reps": @[@100, @75, @50],
                                            @"Load": @[@40, @60, @80]},
                                     
                                     @4:@{@"Reps": @[@100, @80, @60, @40],
                                            @"Load": @[@30, @50, @70, @80]},
                                     
                                     @5:@{@"Reps": @[@100, @80, @60, @40, @20],
                                            @"Load": @[@40, @50, @60, @70, @80]},
                                     
                                     @6:@{@"Reps": @[@100, @80, @60, @40, @20, @20],
                                            @"Load": @[@40, @50, @60, @70, @80, @90]},
                                     
                                     @7:@{@"Reps": @[@100, @80, @60, @50, @40, @30, @20],
                                            @"Load": @[@30, @40, @50, @60, @70, @80, @90]},
                                     
                                     @8:@{@"Reps": @[@100, @90, @80, @70, @60, @50, @40, @20],
                                            @"Load": @[@20, @30, @40, @50, @60, @70, @80, @90]}
                                     };
    
    warmupRep = [warmupMaxRepTV.text intValue];
    warmupSets = [warmupMaxSetTV.text intValue];
    lastWorkoutFinalWeight = [workingWeightTextFied.text floatValue];
    
    float maxY = 0;
    //removing all subviews
    [[customSetsListView subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];

    NSArray *reps = [[repsLoadRation objectForKey:[NSNumber numberWithInt:warmupSets]] objectForKey:@"Reps"];
    NSArray *load = [[repsLoadRation objectForKey:[NSNumber numberWithInt:warmupSets]] objectForKey:@"Load"];
    float workingWeight = [workingWeightTextFied.text floatValue];
    
    UILabel *sr = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 30)];
    sr.text = @"SxR";
    sr.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
    
    UILabel *loadLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sr.frame), 0, 50, 30)];
    loadLbl.text = @"Weight";
    [loadLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    
    UILabel *plateLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(loadLbl.frame), 0, 300 - 100, 30)]; //300 = popupwidth
    plateLbl.text = @"Plates (Per Side)";
    [plateLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    
    [customSetsListView addSubview:sr];
    [customSetsListView addSubview:loadLbl];
    [customSetsListView addSubview:plateLbl];
    maxY = CGRectGetMaxY(plateLbl.frame);
    
    for (int i = 0 ; i < warmupSets; i++) {
        UILabel *setNum  = [[UILabel alloc] initWithFrame:CGRectMake(0, maxY, 50, 20)];
        UILabel *setLoad  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), maxY, 50, 20)];
        UILabel *setPlate  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setLoad.frame), maxY, 300-100, 20)];
        
        float weight = [self RoundTo:(workingWeight * [[load objectAtIndex:i] intValue])/100 to:5];

        setNum.text = [NSString stringWithFormat:@"1x%.0f", ceil((warmupRep * [[reps objectAtIndex:i] intValue])/100)];
        setLoad.text = [NSString stringWithFormat:@"%.1f", weight];
        setPlate.text = [self platesOnSide:weight];
    
        [setNum setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        [setLoad setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        [setPlate setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        
        setNum.textAlignment = NSTextAlignmentLeft;
        setLoad.textAlignment = NSTextAlignmentLeft;
        setPlate.textAlignment = NSTextAlignmentLeft;
        
        [customSetsListView addSubview:setNum];
        [customSetsListView addSubview:setLoad];
        [customSetsListView addSubview:setPlate];
        maxY = CGRectGetMaxY(setNum.frame);
    }
}
-(float) RoundTo: (float) number to:(float)to
{
    if (number >= 0) {
        return to * floorf(number / to + 0.5f);
    }
    else {
        return to * ceilf(number / to - 0.5f);
    }
}
-(NSString  *) platesOnSide:(float) weight {
    NSString *wtonBar= @"";
    
    if (lbORkg == 0) {
        weight -= 45;// bar weight
        weight /= 2;
        
        NSMutableArray *plateArray = [[NSMutableArray alloc] initWithCapacity:weightLbVal.count];
        for (int i = 0 ; i < weightLbVal.count; i++)
            [plateArray addObject:@0];
        if (weight <= 0)
            wtonBar = @"Bar";
        while (weight > 0) {
            if (weight >= [[weightLbVal objectAtIndex:0] floatValue]) {
                [plateArray replaceObjectAtIndex:0 withObject:@([[plateArray objectAtIndex:0] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:0] floatValue];
                continue;
            } else if (weight >= [[weightLbVal objectAtIndex:1] floatValue]) {
                [plateArray replaceObjectAtIndex:1 withObject:@([[plateArray objectAtIndex:1] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:1] floatValue];
                continue;
            } else if (weight >= [[weightLbVal objectAtIndex:2] floatValue]) {
                [plateArray replaceObjectAtIndex:2 withObject:@([[plateArray objectAtIndex:2] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:2] floatValue];
                continue;
            } else if (weight >= [[weightLbVal objectAtIndex:3] floatValue]) {
                [plateArray replaceObjectAtIndex:3 withObject:@([[plateArray objectAtIndex:3] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:3] floatValue];
                continue;
            } else if (weight >= [[weightLbVal objectAtIndex:4] floatValue]) {
                [plateArray replaceObjectAtIndex:4 withObject:@([[plateArray objectAtIndex:4] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:4] floatValue];
                continue;
            } else if (weight >= [[weightLbVal objectAtIndex:5] floatValue]) {
                [plateArray replaceObjectAtIndex:5 withObject:@([[plateArray objectAtIndex:5] floatValue] + 1)];
                weight -= [[weightLbVal objectAtIndex:5] floatValue];
                continue;
            }
        }
        for (int i = 0 ; i < plateArray.count ; i++) {
            if ([[plateArray objectAtIndex:i] floatValue] != 0) {
                if ([wtonBar isEqualToString: @""])
                    wtonBar = [NSString stringWithFormat:@"%@x%@", [weightLbVal objectAtIndex:i],  [plateArray objectAtIndex:i]];
                else
                    wtonBar = [NSString stringWithFormat:@"%@, %@x%@", wtonBar, [weightLbVal objectAtIndex:i],  [plateArray objectAtIndex:i]];
            }
        }
    } else {
        //KGs
        weight -= 20;// bar weight
        weight /= 2;

        NSMutableArray *plateArray = [[NSMutableArray alloc] initWithCapacity:weightKgVal.count];
        for (int i = 0 ; i < weightKgVal.count; i++)
            [plateArray addObject:@0];
        
        if (weight <= 0)
            wtonBar = @"Bar";
        while (weight > 0) {
            if (weight >= [[weightKgVal objectAtIndex:0] floatValue]) {
                [plateArray replaceObjectAtIndex:0 withObject:@([[plateArray objectAtIndex:0] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:0] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:1] floatValue]) {
                [plateArray replaceObjectAtIndex:1 withObject:@([[plateArray objectAtIndex:1] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:1] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:2] floatValue]) {
                [plateArray replaceObjectAtIndex:2 withObject:@([[plateArray objectAtIndex:2] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:2] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:3] floatValue]) {
                [plateArray replaceObjectAtIndex:3 withObject:@([[plateArray objectAtIndex:3] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:3] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:4] floatValue]) {
                [plateArray replaceObjectAtIndex:4 withObject:@([[plateArray objectAtIndex:4] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:4] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:5] floatValue]) {
                [plateArray replaceObjectAtIndex:5 withObject:@([[plateArray objectAtIndex:5] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:5] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:6] floatValue]) {
                [plateArray replaceObjectAtIndex:6 withObject:@([[plateArray objectAtIndex:6] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:6] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:7] floatValue]) {
                [plateArray replaceObjectAtIndex:7 withObject:@([[plateArray objectAtIndex:7] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:7] floatValue];
                continue;
            } else if (weight >= [[weightKgVal objectAtIndex:8] floatValue]) {
                [plateArray replaceObjectAtIndex:8 withObject:@([[plateArray objectAtIndex:8] floatValue] + 1)];
                weight -= [[weightKgVal objectAtIndex:8] floatValue];
                continue;
            }
        }
        for (int i = 0 ; i < plateArray.count ; i++) {
            if ([[plateArray objectAtIndex:i] floatValue] != 0) {
                if ([wtonBar isEqualToString: @""]) {
                    wtonBar = [NSString stringWithFormat:@"%@x%@", [weightKgVal objectAtIndex:i],  [plateArray objectAtIndex:i]];
                }
                else
                    wtonBar = [NSString stringWithFormat:@"%@, %@x%@", wtonBar, [weightKgVal objectAtIndex:i],  [plateArray objectAtIndex:i]];
            }
        }

    }
    
    return  wtonBar;
}
@end
