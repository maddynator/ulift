//
//  MoreOptionsTVC.m
//  uLift
//
//  Created by Mayank Verma on 10/22/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "MoreOptionsTVC.h"
#import "commons.h"
#import <MessageUI/MessageUI.h>
#import "TermsAndPrivacyView.h"
#import "LinkAccounts.h"

@interface MoreOptionsTVC () <MFMailComposeViewControllerDelegate, SKStoreProductViewControllerDelegate, MFMessageComposeViewControllerDelegate >{
    NSArray *sectionHeaders;
    NSArray *segueValues;
    NSArray *itemValues;
    int webViewSelected, socialSelected;
}

@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation MoreOptionsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Settings";
    
    sectionHeaders = [[NSArray alloc] initWithObjects:@"Information",
                      @"Settings",
                      @"Feedback",//@"Social/Feedback",
                      @"Tools",
                      @"Miscellaneous", nil];
    
    itemValues = [[NSArray alloc] initWithObjects:
                  [[NSArray alloc] initWithObjects: @"Tutorials", nil],//, @"Workout Store/Restore Purchase", nil], // 0@"Our Other Apps",s
                  [[NSArray alloc] initWithObjects: @"Profile", @"App Settings", @"Link with Facebook", @"Link with Twitter", @"Change Theme",nil], // 1
                 // [[NSArray alloc] initWithObjects: @"Like us on Facebook", @"Follow us on Twitter",@"Follow us on Instagram", @"Contact Us", nil], // 2
                  [[NSArray alloc] initWithObjects: @"Contact Us", nil], // 2
                  [[NSArray alloc] initWithObjects: @"Export Data", @"Rate us on App Store", nil], // 3@"Invite a friend",
                  [[NSArray alloc] initWithObjects: @"FAQs", @"Terms of Service", @"Privacy Policy", nil], // 4
                  nil];
    
    segueValues = [[NSArray alloc] initWithObjects:
                   [[NSArray alloc] initWithObjects:@"webViewSegue", nil],//]@"workoutStoreSegue", nil], // 0@"",
                   [[NSArray alloc] initWithObjects:@"userProfileSegue", @"settingsSegue", @"linkSocialSegue", @"linkSocialSegue", @"appThemeSegue", nil], // 1
                   //[[NSArray alloc] initWithObjects:@"linkSocialSegue", @"linkSocialSegue", @"webViewSegue", @"", nil], // 2
                   [[NSArray alloc] initWithObjects:@"", nil], // 2
                   [[NSArray alloc] initWithObjects:@"", @"", nil], // 3@"",
                   [[NSArray alloc] initWithObjects:@"webViewSegue", @"webViewSegue",  @"webViewSegue", nil], // 4
                   nil];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionHeaders count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[itemValues objectAtIndex:section] count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    cell.textLabel.text = [[itemValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    cell.textLabel.textColor = [Utilities getAppColor];
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    [cell.contentView addSubview:lineView];

    return cell;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
            switch (indexPath.row) {
//                case 0:
//                    [self showOtherAppView:CNPPopupStyleCentered];
//                    break;
                case 0:
                    webViewSelected = TUTORIALS_WEBVIEW;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                case 1:
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                default:
                    break;
            }
        }
            break;
        case 1: {//settings
            switch (indexPath.row) {
                case 0:
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                case 1:
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                case 2:
                    socialSelected = FACEBOOK_CONNECT;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                case 3:
                    socialSelected = TWITTER_CONNECT;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    break;
                case 4:
                    [self performSegueWithIdentifier:@"appThemeSegue" sender:self];
                    break;
                default:
                    break;
            }
        }

            break;
        case 2: {//feedback
            switch (indexPath.row) {
                    /*
                case 0: {
                    socialSelected = FACEBOOK_LIKE;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                    
                }
                    break;
                case 1:{
                    
                    socialSelected = TWITTER_FOLLOW;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                }
                    break;
                case 2:{
                    
                    webViewSelected = INSTAGRAM_WEBVIEW;
                    [self performSegueWithIdentifier:[[segueValues objectAtIndex:indexPath.section] objectAtIndex:indexPath.row] sender:self];
                }
                    break;

                case 3: {
                */
                case 0: {
                    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
                    
                    if (mailClass != nil) {
                        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                        mc.mailComposeDelegate = self;
                        [mc setSubject:@"Feedback: Gyminutes"];
                        [mc setMessageBody:@"" isHTML:NO];
                        [mc setToRecipients:@[@"gyminutes@gmail.com"]];
                        // Present mail view controller on screen
                        if([mailClass canSendMail]) {
                            [self presentViewController:mc animated:YES completion:^{
                                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                               @"UserID", [PFUser currentUser].objectId,
                                                               nil];
                                [Flurry logEvent:@"EmailedFeedback" withParameters:articleParams];

                            }];
                        }
                    }
                    
                }
                    break;
                default:
                    break;
            }
        }

            break;
        case 3:  { //tools
            switch (indexPath.row) {
                case 0: {
                    if ([Utilities showDataPackage]) {
                        SCLAlertView *error = [[SCLAlertView alloc] init];
                        [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
                            SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
                            payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
                            [[SKPaymentQueue defaultQueue] addPayment:payment];
                        }];

                        [error addButton:@"BUY DATA PACK" actionBlock:^{
                            SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
                            payment.productIdentifier = @IAP_DATA_PACKAGE_ID;
                            [[SKPaymentQueue defaultQueue] addPayment:payment];
                        }];
                        
                        [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:@"Please upgrade to Premium Package or Data Package." closeButtonTitle:nil duration:0.0f]; // Custom
                        
                        error.shouldDismissOnTapOutside = YES;
                    } else {
                        [self exportData];
                    }
                }
                    break;
                case 1: {
//                    [Appirater setDebug:YES];
//                    [Appirater tryToShowPrompt];
                    #define YOUR_APP_STORE_ID 1059671198 //Change this one to your ID
                    NSString * appId = @"1059671198";
                    NSString * theUrl = [NSString  stringWithFormat:@"itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=%@&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software",appId];
                    if ([[UIDevice currentDevice].systemVersion integerValue] > 6)
                        theUrl = [NSString stringWithFormat:@"itms-apps://itunes.apple.com/app/id%@",appId];
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:theUrl]];
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   @"UserID", [PFUser currentUser].objectId,
                                                   nil];
                    [Flurry logEvent:@"RateAppClicked" withParameters:articleParams];

                    }
                    break;
              
                    /*
                case 2: {
                    SCLAlertView *alert = [[SCLAlertView alloc] init];
                    
                    [alert addButton:@"Email" actionBlock:^{
                        NSString *emailTitle = @"Checkout \"gyminutes\" for IOS.";
                        // Email Content
                        NSString *messageBody = @"Hey,\n\nCheckout GYMINUTES. Its the best workout tracking app.\n\n https://itunes.apple.com/us/app/gyminutes/id1059671198?ls=1&mt=8";
                        // To address
                        //    NSArray *toRecipents = [NSArray arrayWithObject:@[currentUser.email]];
                        Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
                        
                        if (mailClass != nil) {
                            MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
                            mc.mailComposeDelegate = self;
                            [mc setSubject:emailTitle];
                            [mc setMessageBody:messageBody isHTML:YES];
                        
                        
                            NSLog(@"readches here..");
                            // Present mail view controller on screen
                            if([mailClass canSendMail]) {
                                NSLog(@"why i am crashing here...?");
                                [self presentViewController:mc animated:YES completion:^{
                                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                                   @"Date", [NSDate date],
                                                                   @"UserID", [PFUser currentUser].objectId,
                                                                   nil];
                                    [Flurry logEvent:@"sharedAppLinkViaEmail" withParameters:articleParams];

                                }];
                            }
                        }

                    }];
                    
                    [alert addButton:@"Message" actionBlock:^{
                        if(![MFMessageComposeViewController canSendText]) {
                            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                            [warningAlert show];
                            return;
                        }
                        
                        NSString *message = [NSString stringWithFormat:@"Hey, checkout gyminutes. This is a great workout tracking app. https://itunes.apple.com/us/app/gyminutes/id1059671198?ls=1&mt=8"];
                        
                        MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
                        messageController.messageComposeDelegate = self;
                        [messageController setBody:message];

                        
                        // Present message view controller on screen
                        [self presentViewController:messageController animated:YES completion:^{
                            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                           @"Date", [NSDate date],
                                                           @"UserID", [PFUser currentUser].objectId,
                                                           nil];
                            [Flurry logEvent:@"sharedAppLinkViaMessage" withParameters:articleParams];
                        }];

                    }];
                    alert.shouldDismissOnTapOutside = YES;
                    //[alert showNotice:self.parentViewController title:nil subTitle:@"Share GYMINUTES with friends" closeButtonTitle:nil duration:0.0f];
                    [alert showCustom:self.parentViewController image:[UIImage imageNamed:@"IconFriends"] color:FlatMintDark title:nil subTitle:@"Share \"gyminutes\" with friends" closeButtonTitle:nil duration:0.0f];

                }
                default:
                    break;
                     */
            }
                     
        }
                
            break;
        case 4: { //misc
            switch (indexPath.row) {
                case 0: {
                    webViewSelected = FAQS_WEBVIEW;
                    [self performSegueWithIdentifier:@"webViewSegue" sender:self];
                    
                }
                    break;
                case 1: {
                    webViewSelected = TERMS_WEBVIEW;
                    [self performSegueWithIdentifier:@"webViewSegue" sender:self];

                }
                    break;
                case 2: {
                    webViewSelected = PRIVACY_WEBVIEW;

                    [self performSegueWithIdentifier:@"webViewSegue" sender:self];
                }
                    break;
                default:
                    break;
            }
        }

            break;
            
        default:
            break;
    }
    
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [sectionHeaders objectAtIndex:section];
}

-(void) exportData {
    NSString *filePath = [Utilities exportAllDataForRoutine:@"All"];
    // Email Subject
    NSString *emailTitle = @"Gyminutes Workout History";
    // Email Content
    NSString *messageBody = @"Hi There,\nHere is your workout history provided by gyminutes.\nYour Meeting Minutes From the GYM.\n\n";
    // To address
    //    NSArray *toRecipents = [NSArray arrayWithObject:@[currentUser.email]];
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    NSData *noteData = [NSData dataWithContentsOfFile:filePath];
    
    if (mailClass != nil) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:NO];
        //        [mc setToRecipients:toRecipents];
        
        [mc addAttachmentData:noteData mimeType:@"text/plain" fileName:@"gyminutes-WorkoutHistory.csv"];
        // Present mail view controller on screen
        if([mailClass canSendMail]) {
            [self presentViewController:mc animated:YES completion:^{
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"dataExportClicked" withParameters:articleParams];

            }];
        }
    }
}


- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed: {
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [Flurry logError:@"emailFailure" message:[NSString stringWithFormat:@"Date:%@,UserID:%@", [NSDate date], [PFUser currentUser].objectId] error:error];
        }
            break;
        default:
            break;
    }
    
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)productViewControllerDidFinish:(SKStoreProductViewController *)viewController {
    [viewController dismissViewControllerAnimated: YES completion: nil];
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"webViewSegue"]) {
        TermsAndPrivacyView *destVC = segue.destinationViewController;
        NSLog(@"web view selected %d", webViewSelected);
        destVC.whichWebPage = webViewSelected;
        
    } else if ([segue.identifier isEqualToString:@"linkSocialSegue"]) {
        LinkAccounts *destVC = segue.destinationViewController;
        destVC.socialMedia = socialSelected;        
    }
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            [Flurry logError:@"msgFailure" message:[NSString stringWithFormat:@"Date:%@,UserID:%@", [NSDate date], [PFUser currentUser].objectId] error:nil];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)showOtherAppView:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"7 MIN FIT" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Scientifically proven 7 Minute workouts." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Equiped with new High Intensity workouts that just last 7 Minute each. Excellent for days when you are short on time." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSForegroundColorAttributeName : [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"DOWNLOAD FOR FREE!" forState:UIControlStateNormal];
    button.backgroundColor = FlatMint;
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];

        NSString *iTunesLink = @"itms://itunes.apple.com/us/app/7-min-fit/id1082175631?mt=8";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:iTunesLink]];
        
//        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
//                                       @"Date", [NSDate date],
//                                       @"UserID", [PFUser currentUser].objectId,
//                                       nil];
//        [Flurry logEvent:@"SVNMinAppClicked" withParameters:articleParams];
        

    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [UIColor whiteColor];
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    lineOneLabel.textColor = [UIColor whiteColor];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(50, 0, 100, 100)];
    imageView.contentMode  = UIViewContentModeScaleAspectFill;
    [imageView setImage:[UIImage imageNamed:@"SvnMin"]];
    imageView.layer.cornerRadius = 2;
    imageView.layer.borderWidth = 4;
    imageView.layer.borderColor = FlatWhiteDark.CGColor;
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    lineTwoLabel.textColor = [UIColor whiteColor];
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 55)];
    customView.backgroundColor = [UIColor lightGrayColor];
    
    UITextField *textFied = [[UITextField alloc] initWithFrame:CGRectMake(10, 10, 230, 35)];
    textFied.borderStyle = UITextBorderStyleRoundedRect;
    textFied.placeholder = @"Custom view!";
    [customView addSubview:textFied];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[imageView, titleLabel, lineOneLabel, lineTwoLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.backgroundColor = FlatBlack;
    //    self.popupController.theme.popupContentInsets = UIEdgeInsetsMake(15, 5, 5, 15);
    [self.popupController presentPopupControllerAnimated:YES];
}


@end
