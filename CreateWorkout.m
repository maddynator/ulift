//
//  CreateWorkout.m
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
/*
            THIS FILE IS DEPRECATED. CHECK CREATEWORKOUTNEW.M/H
 */
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************
// *******************************************************************************************


#import "CreateWorkout.h"

@interface CreateWorkout () {
    NSMutableArray *exerciseInWorkout;
    NSIndexPath *selectedIndex;
    BOOL changesMade;
    NSManagedObjectContext *localContext;
    UIToolbar* numberToolbar;
    UIBarButtonItem *editButton;
}

@end

@implementation CreateWorkout
@synthesize routineName, workoutName;

- (void)viewDidLoad {
    [super viewDidLoad];
    localContext = [NSManagedObjectContext MR_defaultContext];
    changesMade = false;
    self.title = workoutName;
    UIBarButtonItem *addBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd target:self action:@selector(addExercise)];
    editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStyleDone target:self action:@selector(EditTable:)];
    
    
    self.navigationItem.rightBarButtonItems = @[addBtn, editButton];//, saveBtn];
    [self getExercises];
    
    numberToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    
    numberToolbar.items = [NSArray arrayWithObjects:
//                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
                           nil];

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
    
    exerciseInWorkout = [[NSMutableArray alloc] init];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
}

- (IBAction) EditTable:(id)sender{
    NSLog(@"editing valu is %d", (int)self.editing);
    if(self.editing)
    {
        [super setEditing:NO animated:NO];
        [self.tableView setEditing:NO animated:YES];
        [self.tableView reloadData];
        editButton.title = @"Edit";
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
            int count = 0;
            for(UserWorkout *entry in exerciseInWorkout) {
                entry.exerciseNumber = [NSNumber numberWithInt:count];
                entry.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                count++;
            }
        }];
    }
    else
    {
        [super setEditing:YES animated:YES];
        editButton.title = @"Done";
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
        [self.tableView setEditing:YES animated:YES];
        [self.tableView reloadData];
        
    }
}

- (void)keyboardWillShow:(NSNotification *)sender
{
    CGSize kbSize = [[[sender userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsMake(0, 0, kbSize.height, 0);
        [self.tableView setContentInset:edgeInsets];
        [self.tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

- (void)keyboardWillHide:(NSNotification *)sender
{
    NSTimeInterval duration = [[[sender userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    
    [UIView animateWithDuration:duration animations:^{
        UIEdgeInsets edgeInsets = UIEdgeInsetsZero;
        [self.tableView setContentInset:edgeInsets];
        [self.tableView setScrollIndicatorInsets:edgeInsets];
    }];
}

-(void) dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
-(void)cancelNumberPad{
}

-(void)doneWithNumberPad{
    
    [self saveExerciseChanges];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {
    [self getExercises];
}
-(void) viewDidDisappear:(BOOL)animated {
    [self saveExerciseChanges];
}
-(void) getExercises {
    [exerciseInWorkout removeAllObjects];
    for (UserWorkout *data in [Utilities getWorkoutsDayExercises:routineName workoutN:workoutName]) {
        [exerciseInWorkout addObject:data];
    }
    NSLog(@"exercise in workouts are %lu", (long)[exerciseInWorkout count]);
    [self.tableView reloadData];
}

-(void) addExercise {
    bool addEx = false;
    NSString *tempRBid = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
    // check if it is a downloaded workout and using free version. No edits allowed on those...
    if (![_routineBundleId containsString:tempRBid]) {
        if ([Utilities showPowerRoutinePackage]) {
            [self showPurchasePopUp:@"Adding exercises to a downloaded routine is restricted in free version"];
        } else {
            addEx = true;
        }
    } else {
        
        addEx = true;
    }
    
    if (addEx) {
        [self performSegueWithIdentifier:@"addExerciseSegue" sender:self];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [exerciseInWorkout count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UserWorkout *workoutInfo = [exerciseInWorkout objectAtIndex:indexPath.row];

    MGSwipeTableCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    for (JVFloatLabeledTextField *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[JVFloatLabeledTextField class]]) {
            [lbl removeFromSuperview];
        } else {
        }
    }
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else {
            //            NSLog(@"catn remove label");
        }
    }

    
    float cellWidth = CGRectGetWidth(cell.frame);
    float smallCellWidth = cellWidth/3 - 30;
    float gapWidth = (cellWidth - smallCellWidth * 3)/6;
    
    
    UILabel *exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, cellWidth, 30)];
    UIColor *floatingLabelColor = [UIColor whiteColor];

    UILabel *setsLbl = [[UILabel alloc] initWithFrame:CGRectMake(gapWidth*2, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
    UILabel *repsLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsLbl.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
    UILabel *restTimeLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repsLbl.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame), smallCellWidth, 10)];
    
    setsLbl.text = @"Sets";
    repsLbl.text = @"Reps";
    restTimeLbl.text = @"Rest Timer(s)";
    
    setsLbl.textColor = floatingLabelColor;
    repsLbl.textColor = floatingLabelColor;
    restTimeLbl.textColor = floatingLabelColor;
    [setsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
    [repsLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
    [restTimeLbl setFont:[UIFont fontWithName:@HAL_REG_FONT size:10]];
    setsLbl.textAlignment = repsLbl.textAlignment = restTimeLbl.textAlignment = NSTextAlignmentCenter;
    
    [cell.contentView addSubview:setsLbl];
    [cell.contentView addSubview:repsLbl];
    [cell.contentView addSubview:restTimeLbl];
    
    UITextField *sets = [[UITextField alloc] initWithFrame:CGRectMake(gapWidth*2, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
    UITextField *reps = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(sets.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame)+ 15, smallCellWidth, 30)];
    UITextField *restTime = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(reps.frame)+ gapWidth, CGRectGetMaxY(exerciseName.frame) + 15, smallCellWidth, 30)];
    
    restTime.keyboardType = reps.keyboardType = sets.keyboardType = UIKeyboardTypeNumberPad;
    
    sets.delegate = reps.delegate = restTime.delegate = self;
    sets.textAlignment = reps.textAlignment = restTime.textAlignment = NSTextAlignmentCenter;
    exerciseName.textAlignment = NSTextAlignmentCenter;

    [sets setBorderStyle:UITextBorderStyleRoundedRect];
    [reps setBorderStyle:UITextBorderStyleRoundedRect];
    [restTime setBorderStyle:UITextBorderStyleRoundedRect];
    
    exerciseName.tag = 100;
    sets.tag = 101;
    reps.tag = 102;
    restTime.tag = 103;
    
    cell.backgroundColor = [Utilities getMajorMuscleColor:workoutInfo.majorMuscle];
    restTime.backgroundColor = reps.backgroundColor = sets.backgroundColor = [Utilities getMajorMuscleColorSelected:workoutInfo.majorMuscle];
    

//    sets.placeholder =  @"     Sets";
//    reps.placeholder = @"      Reps";
//    restTime.placeholder = @"      Rest Time(s)";
    
    exerciseName.textColor = [UIColor whiteColor];
    sets.textColor = [UIColor whiteColor];
    reps.textColor = [UIColor whiteColor];
    restTime.textColor = [UIColor whiteColor];
    
    [exerciseName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
    [sets setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
    [reps setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
    [restTime setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
    
    NSLog(@"exercise name %@, %d", workoutInfo.exerciseName, [workoutInfo.exerciseNumber intValue]);
    exerciseName.text = workoutInfo.exerciseName;
    sets.text = [NSString stringWithFormat:@"%d", [workoutInfo.sets intValue]];
    reps.text = [NSString stringWithFormat:@"%d", [workoutInfo.reps intValue]];
    restTime.text = [NSString stringWithFormat:@"%d", [workoutInfo.restTimer intValue]];
    
    [cell.contentView addSubview:exerciseName];
    [cell.contentView addSubview:sets];
    [cell.contentView addSubview:reps];
    [cell.contentView addSubview:restTime];

    //configure right buttons
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
    cell.rightSwipeSettings.transition = MGSwipeTransition3D;
    cell.delegate = self;

    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 1.0, self.tableView.frame.size.width, 1)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
    [cell.contentView addSubview:lineView];

    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
        return 0.1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 90;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    //int rowSelected = (int)[[self.tableView indexPathsForSelectedRows] count];
//    [self performSegueWithIdentifier:@"workoutEditSegue" sender:self];
    NSLog(@"something edited...");
    selectedIndex = indexPath;
//    [tableView endEditing:YES];
}

#pragma mark Row reordering
-(BOOL) tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
-(BOOL) tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    NSLog(@"can move rows.... ");
    return YES;
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableview shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath {
    return NO;
}

-(void) tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)sourceIndexPath toIndexPath:(NSIndexPath *)destinationIndexPath {
        ExerciseList *temp = [exerciseInWorkout objectAtIndex:sourceIndexPath.row];
        [exerciseInWorkout removeObjectAtIndex:sourceIndexPath.row];
        [exerciseInWorkout insertObject:temp atIndex:destinationIndexPath.row];
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    bool delEx = false;
    
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
    NSIndexPath * path = [self.tableView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSString *tempRBid = [NSString stringWithFormat:@"com.gyminutes.%@", [PFUser currentUser].objectId];
        // check if it is a downloaded workout and using free version. No edits allowed on those...
        if (![_routineBundleId containsString:tempRBid]) {
            if ([Utilities showPowerRoutinePackage]) {
                [self showPurchasePopUp:@"Deleting exercises from a downloaded routine is restricted in free version"];
            } else {
                delEx = true;
            }
        } else {
            delEx = true;
        }
        
        if (delEx) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            
            [alert addButton:@"Yes" actionBlock:^{
                //TODO: we need to check if user has performed this workout. We have to delete all that or dont allow user to delete this
                UserWorkout *data = [exerciseInWorkout objectAtIndex:path.row];
                [Utilities markExerciseAsInActive:data.routineName workoutName:data.workoutName exerciseName:data.exerciseName];
                
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContextNew)  {
                    [data MR_deleteEntityInContext:localContextNew];
                    [exerciseInWorkout removeObjectAtIndex:index];
                } completion:^(BOOL contextDidSave, NSError *error) {
                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContextNew)  {
                        int count = 0;
                        NSLog(@"entity deleted.. and now getting more ");
                        
                        for(UserWorkout *entry in [Utilities getWorkoutsDayExercises:routineName workoutN:workoutName]) {
                            entry.exerciseNumber = [NSNumber numberWithInt:count];
                            entry.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                            count++;
                        }
                    } completion:^(BOOL contextDidSave, NSError *error) {
                        [self getExercises];
                        [self.tableView reloadData];
                        
                    }];
                }];
                
                //            [localContext MR_saveToPersistentStoreAndWait];
                // this may not be necessary but we are still doing it for now..
            }];
            [alert showWarning:self title:@"Delete exercise?" subTitle:@"Are you sure you want to delete this exercise from workout?" closeButtonTitle:@"No" duration:0.0f];
        }
        return NO; //Don't autohide to improve delete expansion animation
    }
    return YES;
}


- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    
    changesMade = true;
    NSLog(@"user clicked on it.. may have made some change... lets save it...");
    textField.inputAccessoryView = numberToolbar;

//    CGPoint pointInTable = [textField.superview convertPoint:textField.frame.origin toView:self.tableView];
//    CGPoint contentOffset = self.tableView.contentOffset;
//    
//    contentOffset.y = (pointInTable.y - textField.inputAccessoryView.frame.size.height);
//    
//    NSLog(@"contentOffset is: %@, %f", NSStringFromCGPoint(contentOffset), textField.inputAccessoryView.frame.size.height);
//    [self.tableView setContentOffset:contentOffset animated:YES];
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
//    if ([textField.superview.superview isKindOfClass:[UITableViewCell class]])
//    {
//        CGPoint buttonPosition = [textField convertPoint:CGPointZero
//                                                  toView: self.tableView];
//        NSIndexPath *indexPath = [self.tableView indexPathForRowAtPoint:buttonPosition];
//        
//        [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:TRUE];
//    }
    
    return YES;
}



-(void) saveExerciseChanges {
    [self.tableView endEditing:YES];
    
    for (int i = 0; i < [exerciseInWorkout count]; i++) {
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow: i inSection: 0];
        
        UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        UserWorkout *wkExercise = [exerciseInWorkout objectAtIndex:i];
        NSLog(@"exName %@, set %d rep %d rest %d", wkExercise.exerciseName, [wkExercise.sets intValue], [wkExercise.reps intValue], [wkExercise.restTimer intValue]);
        for (UIView *view in  cell.contentView.subviews){
            if ([view isKindOfClass:[UITextField class]]){
                int sets = 0, reps = 0, restTimer = 0;
                UITextField* txtField = (UITextField *)view;
                
                if (txtField.tag == 100) {
                    NSLog(@"TextField.tag:%ld and Data %@", (long)
                          txtField.tag, txtField.text);
                }
                if (txtField.tag == 101) {
                    NSLog(@"TextField.tag:%ld and Data %@ set %@", (long)txtField.tag, txtField.text, wkExercise.sets);
                    sets = [txtField.text intValue];
                    if (wkExercise.sets != [NSNumber numberWithInt:sets]) {
                        wkExercise.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                        wkExercise.sets = [NSNumber numberWithInt:sets];
                    }

                }
                if (txtField.tag == 102) {
                    NSLog(@"TextField.tag:%ld and Data %@, %@", (long)txtField.tag, txtField.text, wkExercise.reps);
                    reps = [txtField.text intValue];
                    if (wkExercise.reps != [NSNumber numberWithInt:reps]) {
                        wkExercise.reps = [NSNumber numberWithInt:reps];
                        wkExercise.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                    }
                }
                if (txtField.tag == 103) {
                    NSLog(@"TextField.tag:%ld and Data %@, %@", (long)txtField.tag, txtField.text, wkExercise.restTimer);
                    restTimer = [txtField.text intValue];
                    if (wkExercise.restTimer != [NSNumber numberWithInt:restTimer]) {
                        wkExercise.restTimer = [NSNumber numberWithInt:restTimer];
                        wkExercise.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                    }
                }
            } // End of Cell Sub View
        }// Counter
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                NSLog(@"set saved...");
            }
        }];
        

    }

}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No exercise added yet."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Please click + to add exercise.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

-(void) searchExercises {
    [self performSegueWithIdentifier:@"allExerciseSegue" sender:self];
}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addExerciseSegue"]) {
        MuscleListTVC *destVC = segue.destinationViewController;
        destVC.date = [Utilities getCurrentDate];
        destVC.workoutName = workoutName;
        destVC.routineName = routineName;
        destVC.isTempEx = false;
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ROUTINE PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ROUTINE_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@. Please upgrade to Premium or Power Routine Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

#pragma scroll view

-(void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    NSLog(@"will save data now...");
    [self saveExerciseChanges];
}

@end
