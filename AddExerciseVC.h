//
//  AddExerciseVC.h
//  uLift
//
//  Created by Mayank Verma on 7/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <ParseUI/ParseUI.h>
#import "commons.h"

@interface AddExerciseVC : FXFormViewController
@property (nonatomic, retain) NSString *majorMuscle;
@end
