//
//  RoutineWebView.m
//  uLift
//
//  Created by Mayank Verma on 10/28/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "RoutineWebView.h"
#import "commons.h"

@interface RoutineWebView ()

@end

@implementation RoutineWebView
@synthesize webView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = _routineName;
    NSLog(@"routine name %@", _routineName);
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:_urlToLoad]];
    [self.webView setScalesPageToFit:YES];
    [self.webView loadRequest:request];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineName", _routineName,
                                   nil];
    [Flurry logEvent:@"RoutineWebViewLoaded" withParameters:articleParams];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
