//
//  WorkoutList+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "WorkoutList+CoreDataProperties.h"

@implementation WorkoutList (CoreDataProperties)

+ (NSFetchRequest<WorkoutList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"WorkoutList"];
}

@dynamic date;
@dynamic dayName;
@dynamic exerciseGroup;
@dynamic exerciseName;
@dynamic exerciseNumber;
@dynamic exerciseNumInGroup;
@dynamic isTempExercise;
@dynamic majorMuscle;
@dynamic muscle;
@dynamic repsSuggested;
@dynamic restSuggested;
@dynamic routiineName;
@dynamic setsSuggested;
@dynamic timeStamp;
@dynamic workoutName;
@dynamic repsPerSet;
@dynamic restPerSet;

@end
