//
//  IAPViewController.h
//  uLift
//
//  Created by Mayank Verma on 9/10/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "uLiftIAPHelper.h"
#import <StoreKit/StoreKit.h>
#import "commons.h"

@interface IAPViewController : UITableViewController <DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>

@property (nonatomic, retain) UIRefreshControl *refreshControl;

@end
