//
//  TodayViewController.m
//  gymin
//
//  Created by Mayank Verma on 6/5/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "TodayViewController.h"
#import <NotificationCenter/NotificationCenter.h>

@interface TodayViewController () <NCWidgetProviding> {
    NSArray *workutArray, *motivationQuotes;

}

@end

@implementation TodayViewController
@synthesize wrkThisWeekLbl, lastWrkLbl, wrkThisWeekBtn, lastWrkBtn, motivationalLbl;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSUserDefaults *tdefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.gymessential.GyminitesSharingDefault"];
    
    NSLog(@"last week %@",[tdefaults objectForKey:@"NumWorkoutsThisWeek"]);
    NSLog(@"last workout %@",[tdefaults objectForKey:@"LastWorkoutDate"]);
    
    self.preferredContentSize = CGSizeMake(320, 100);
    motivationQuotes = @[@"Sweat is just fat crying.",
                         @"Sore. The most satisfying pain.",
                         @"Making excuses burns zero calories.",
                         @"FIT is not a destination. Its a way of LIFE.",
                         @"Better sore than sorry",
                         @"Its actually pretty simple. Either you DO IT, or you don't.",
                         @"Strive for progress, not perfection.",
                         @"You want me to do something... tell me I can't do it. -Maya Angelou",
                         @"You miss 100% of the shots you don't take. -Wayne Gretzky",
                         @"Motivation will almost always beat mere talent. -Norman R. Augustine",
                         @"Energy and persistence conquer all things. -Benjamin Franklin",
                         @"Motivation is what gets you started. Habit is what keeps you going. -Jim Ryan",
                         @"The finish line is just the beginning of a whole new race.",
                         @"Just do it.™ -Nike",
                         @"The secret of getting ahead is getting started. -Mark Twain",
                         @"It's never too late to become what you might have been. -George Eliot",
                         @"Consistency is the key."];
    // Do any additional setup after loading the view from its nib.
    
    wrkThisWeekLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, CGRectGetWidth(self.view.frame)/2 - 20, 30)];
    wrkThisWeekBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(wrkThisWeekLbl.frame), 5, CGRectGetWidth(self.view.frame)/2 - 50, 30)];
    
    lastWrkLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(wrkThisWeekLbl.frame), CGRectGetWidth(self.view.frame)/2 - 20, 30)];
    lastWrkBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lastWrkLbl.frame), CGRectGetMaxY(wrkThisWeekLbl.frame), CGRectGetWidth(self.view.frame)/2 - 50, 30)];
    
    motivationalLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(lastWrkBtn.frame) + 2, CGRectGetWidth(self.view.frame) - 70, 30)];
    
    wrkThisWeekLbl.textColor = [UIColor whiteColor];
    lastWrkLbl.textColor = [UIColor whiteColor];
    motivationalLbl.textColor = [UIColor whiteColor];
    
    wrkThisWeekBtn.contentHorizontalAlignment = NSTextAlignmentRight;
    lastWrkBtn.contentHorizontalAlignment = NSTextAlignmentRight;
    [wrkThisWeekLbl setFont:[UIFont systemFontOfSize:14]];
    [lastWrkLbl setFont:[UIFont systemFontOfSize:14]];
    [wrkThisWeekBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    [lastWrkBtn.titleLabel setFont:[UIFont systemFontOfSize:14]];
    
    wrkThisWeekBtn.layer.cornerRadius = 5;
    lastWrkBtn.layer.cornerRadius = 5;
    wrkThisWeekLbl.text = @"This week";
    lastWrkLbl.text = @"Last Workout";
    
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.gymessential.GyminitesSharingDefault"];
    
    if ([defaults objectForKey:@"NumWorkoutsThisWeek"] != nil) {
        [wrkThisWeekBtn setTitle:[defaults objectForKey:@"NumWorkoutsThisWeek"]  forState:UIControlStateNormal];
        [lastWrkBtn setTitle:[defaults objectForKey:@"LastWorkoutDate"]  forState:UIControlStateNormal];
    } else {
        [wrkThisWeekBtn setTitle:@"0 workouts" forState:UIControlStateNormal];
        [lastWrkBtn setTitle:@"Not Performed" forState:UIControlStateNormal];
    }
    
    UIFontDescriptor * fontD = [motivationalLbl.font.fontDescriptor
                                fontDescriptorWithSymbolicTraits:UIFontDescriptorTraitItalic];
    motivationalLbl.font = [UIFont fontWithDescriptor:fontD size:10];
    
    long rand = random();
    int index = (int) rand % [motivationQuotes count];
    motivationalLbl.text = [motivationQuotes objectAtIndex:index];
    motivationalLbl.numberOfLines = 2;
    motivationalLbl.lineBreakMode=NSLineBreakByWordWrapping;
    motivationalLbl.adjustsFontSizeToFitWidth=YES;
    motivationalLbl.minimumScaleFactor=0.1;
    
    [self.view addSubview:wrkThisWeekLbl];
    [self.view addSubview:wrkThisWeekBtn];
    [self.view addSubview:lastWrkLbl];
    [self.view addSubview:lastWrkBtn];
    [self.view addSubview:motivationalLbl];
    
    
    [wrkThisWeekBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 30)];
    [lastWrkBtn setTitleEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 30)];
    
    //    [wrkThisWeekBtn setBackgroundColor:[UIColor darkGrayColor]];
    //    [lastWrkBtn setBackgroundColor:[UIColor darkGrayColor]];
    
    [wrkThisWeekBtn addTarget:self action:@selector(launchHostingApp:) forControlEvents:UIControlEventTouchUpInside];
    [lastWrkBtn addTarget:self action:@selector(launchHostingApp:) forControlEvents:UIControlEventTouchUpInside];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(userDefaultsDidChange:)
                                                 name:NSUserDefaultsDidChangeNotification
                                               object:nil];
}
- (IBAction)launchHostingApp:(id)sender
{
    NSURL *pjURL = [NSURL URLWithString:@"gyminutesapp://"];
    NSLog(@"url is %@", pjURL);
    [self.extensionContext openURL:pjURL completionHandler:nil];
}

- (void)userDefaultsDidChange:(NSNotification *)notification {
    NSLog(@"values changed...");
    [self updateNumberLabelText];
}

- (void)updateNumberLabelText {
    NSUserDefaults *defaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.gymessential.GyminitesSharingDefault"];
    [wrkThisWeekBtn setTitle:[defaults objectForKey:@"NumWorkoutsThisWeek"]  forState:UIControlStateNormal];
    [lastWrkBtn setTitle:[defaults objectForKey:@"LastWorkoutDate"]  forState:UIControlStateNormal];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)widgetPerformUpdateWithCompletionHandler:(void (^)(NCUpdateResult))completionHandler {
    // Perform any setup necessary in order to update the view.
    
    // If an error is encountered, use NCUpdateResultFailed
    // If there's no update required, use NCUpdateResultNoData
    // If there's an update, use NCUpdateResultNewData
    NSLog(@"herre now");
    
    completionHandler(NCUpdateResultNewData);
}

@end
