//
//  TodayViewController.h
//  gymin
//
//  Created by Mayank Verma on 6/5/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TodayViewController : UIViewController
@property (nonatomic, retain) IBOutlet UILabel *wrkThisWeekLbl, *lastWrkLbl, *motivationalLbl;
@property (nonatomic, retain) IBOutlet UIButton *wrkThisWeekBtn, *lastWrkBtn;

@end
