//

//  TodayWorkout.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//



//
#import "TodayWorkout.h"
#import "MuscleListTVC.h"
#import "RecordExercise.h"
#import "RecordExerciseMenu.h"
#import "NavMenu.h"
#import "ExerciseSummary.h"
#import <Crashlytics/Crashlytics.h>
#import "RecWorkout.h"

@interface TodayWorkout () {
    NSArray *summaryItems, *allExerciseForCalendar;
    NSMutableArray *exerciseList;
    NSMutableDictionary *calendarColorDict;
    WorkoutList *exerciseSelected;
    ExerciseMetaInfo *exMetaInfo;
    NSString *date;
    NSString *currentDate;
    int counter;
    RBMenu *menu;
    UILabel *lblLeft;
    UILabel *lblCenter;
    UILabel *lblRight;
    UIImageView *statusImg;
    UIFont *leftFont;
    UIFont *centerFont;
    UIFont *RightFont;
    UIRefreshControl *refreshControl;
    NSArray *allExerciseList, *searchResults;
    UISearchBar *searchBar;
   // UISearchController *searchDisplayController;
    UIView *additionOptionView;
    BOOL showWorkoutCompleteBtn, calendarFlag;
    UIButton *summary;
}
@property (nonatomic, strong) CNPPopupController *popupController;
@end

@implementation TodayWorkout

@synthesize collectionView, recordDate, myTableView, coachMarksView;
-(void) viewDidLoad {
    [super viewDidLoad];

    
    calendarFlag = false;
    
    summaryItems = [[NSArray alloc] initWithObjects: @"Duration", @"Exercises", @"Sets", @"Wt. Mvd", @"Muscles", @"PR Set", nil];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    collectionView.emptyDataSetDelegate = self;
    collectionView.emptyDataSetSource = self;
    
    showWorkoutCompleteBtn = false;
    
    date = [Utilities getCurrentDate];
    NSLog(@"got date is %@", date);
    currentDate = [Utilities getCurrentDate];
    
    counter = 0;
//    [self.navigationItem setHidesBackButton:YES];
//    UIBarButtonItem *menuBtn =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconMenu"] style:UIBarButtonItemStylePlain target:self.navigationController action:@selector(showMenu)];
    
    UIBarButtonItem *calendatBtn =[[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconCalendar"] style:UIBarButtonItemStylePlain target:self action:@selector(showCalender)];
    
    //self.navigationItem.leftBarButtonItems = @[calendatBtn];
    self.tabBarController.navigationController.navigationBar.translucent = YES;

    UIBarButtonItem *summaryBtn = [[UIBarButtonItem alloc]  initWithImage:[UIImage imageNamed:@"IconSocial"] style:UIBarButtonItemStylePlain target:self action:@selector(shareit:)];
//    
//    UIBarButtonItem *addMuscle = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(addExercise:)];
    
    self.navigationItem.rightBarButtonItems = @[calendatBtn,summaryBtn];
    self.collectionView.alwaysBounceVertical = YES;
    
   // [self.collectionView addSubview:refreshControl];
    
    allExerciseList = [ExerciseList MR_findAllSortedBy:@"exerciseName" ascending:YES];
    [self setupDate:date];
    [self getCalendarData];
    summary = [[UIButton alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(_datepicker.frame) + 5, CGRectGetWidth(self.view.frame) - 20, 40)];
    [summary setTitle:@"WORKOUT SUMMARY" forState:UIControlStateNormal];
    summary.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
    summary.layer.cornerRadius = 4;
    summary.backgroundColor = [UIColor whiteColor];
    [summary setTitleColor:FlatOrangeDark forState:UIControlStateNormal];
    [summary addTarget:self action:@selector(loadDetailedSummaryView) forControlEvents:UIControlEventTouchUpInside];
    summary.hidden = YES;
    [self.view addSubview:summary];

    exerciseList = [[NSMutableArray alloc] init];

}

-(void) viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];
    
    self.title = @"WORKOUT";
    self.navigationController.navigationBarHidden = NO;
    lblLeft.text = lblCenter.text = lblRight.text = @"";
    [self reloadData];

    // we are not using this. so commenting it out..
    //showWorkoutCompleteBtn = [Utilities showWorkoutFinishButton:date];
    
//    collectionView.frame = CGRectMake(0, CGRectGetMaxY(summary.frame) + 5, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetMaxY(_datepicker.frame) - 5);

//    NSLog(@"*** MULTI SET SUPPORT %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"MultipleSetSupport"] );
}


-(void) showCoachMarks {
    bool shownTodayWorkoutHint = [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownTodayWorkoutHint"];

    if ([Utilities isHintEnabled] || shownTodayWorkoutHint == false) {
        float width = self.view.frame.size.width;
        
//        CGRect coachmark1 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame) + 10, 40, 38);
        CGRect coachmark2 = CGRectMake(0, CGRectGetMinY(self.collectionView.frame)+ 270 + 15, width, 182);
        CGRect coachmark3 = CGRectMake(width - 50, CGRectGetMinY(self.collectionView.frame)+ 270 + 20, 38, 38);

        //adding margin of 5 on all sides
        CGRect coachmark4 = CGRectMake(5, CGRectGetMinY(self.collectionView.frame)+5, width - 10, 280);
        
        // Setup coach marks
        NSArray *coachMarks = @[
//                                @{
//                                    @"rect": [NSValue valueWithCGRect:coachmark1],
//                                    @"caption": @"Click detail button for workout summary.",
//                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark2],
                                    @"caption": @"Per Exercise information.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark3],
                                    @"maxLblWidth": [NSNumber numberWithFloat:CGRectGetWidth(self.view.frame)],
                                    @"caption": @"MARKS DENOTES\nEdit = Not started\n ! = In-Complete\n\u2713 = Complete.",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                    @"showArrow":[NSNumber numberWithBool:NO]
                                    },
                                @{
                                    @"rect": [NSValue valueWithCGRect:coachmark4],
                                    @"caption": @"Click to START WORKOUT",
                                    @"position":[NSNumber numberWithInteger:LABEL_POSITION_BOTTOM],
                                    @"showArrow":[NSNumber numberWithBool:YES]
                                    }
                                
                                ];
        
        coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
        [self.view addSubview:coachMarksView];
        coachMarksView.delegate = self;
        [coachMarksView start];
    }
    
}
#pragma CoachMarks delegate
-(void) coachMarksViewDidCleanup:(MPCoachMarks *)coachMarksView {
    NSLog(@"in here for cleanup of coachmarks %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ShownTodayWorkoutHint"]);

    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ShownTodayWorkoutHint"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}
-(void) showCalender {
    if (calendarFlag == true) {
        for (RSDFDatePickerView *lbl in [self.view subviews]) {
            if ([lbl isKindOfClass:[RSDFDatePickerView class]]) {
                [lbl removeFromSuperview];
            }
        }
        calendarFlag = false;
        return;
    } else {
    
        RSDFDatePickerView *datePickerView = [[RSDFDatePickerView alloc] initWithFrame:self.view.bounds];
        datePickerView.delegate = self;
        datePickerView.dataSource = self;
        datePickerView.pagingEnabled =  YES;
        datePickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
        
        [self.view addSubview:datePickerView];
        calendarFlag = true;
    }
}
-(void) setupDate: (NSString *) newDate {
    
    [self.datepicker addTarget:self action:@selector(updateSelectedDate) forControlEvents:UIControlEventValueChanged];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MM-dd"];

    
    [self.datepicker fillDatesFromDate:[NSDate dateWithYear:2015 month:01 day:01] toDate:[NSDate dateWithYear:2020 month:12 day:31]];
    [self.datepicker selectDate:[dateFormat dateFromString:newDate]];
    NSLog(@"=====> date is %@, %@", newDate, self.datepicker.selectedDate);

    
}
- (void)updateSelectedDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

//    formatter.dateFormat = [NSDateFormatter dateFormatFromTemplate:@"EEEEddMMMM" options:0 locale:nil];
    
    //self.selectedDateLabel.text = [formatter stringFromDate:self.datepicker.selectedDate];
    NSLog(@"selected date is... %@",[formatter stringFromDate:self.datepicker.selectedDate]);
    date = [formatter stringFromDate:self.datepicker.selectedDate];
    [self reloadData];
}





-(void) getCalendarData {
    dispatch_async(dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
        //Background Thread
        allExerciseForCalendar = [ExerciseSet MR_findAllSortedBy:@"date,exerciseNumber" ascending:YES];
        calendarColorDict = [[NSMutableDictionary alloc] init];
        
        NSMutableArray * unique  = [NSMutableArray array];
        NSMutableSet * processed = [NSMutableSet set];
        for (ExerciseSet *set in allExerciseForCalendar) {
            
            NSString *string = set.date;
            
            // Crash #15: nil string was causing the crash. Making sure such entires are skipped.
            if (string == nil) {
                NSLog(@"string is null");
                continue;
            }
            
            if ([processed containsObject:string] == NO) {
                [unique addObject:string];
                [processed addObject:string];
                [calendarColorDict setObject:set.majorMuscle forKey:string];
            }
        }
    });
}

-(void) viewDidDisappear:(BOOL)animated {
    [additionOptionView removeFromSuperview];
}


-(IBAction)shareit:(id)sender {
    
    if ([exerciseList count] != 0) {
        
        CGRect cropRect = CGRectMake(CGRectGetMinX(self.collectionView.frame), CGRectGetMinX(self.collectionView.frame)  + 60, CGRectGetWidth(self.view.frame), 290);
        UIImageView *viewImage = [self rp_screenshotImageViewWithCroppingRect:cropRect];
        
        //    UIImageWriteToSavedPhotosAlbum(viewImage.image, nil, nil, nil);
        NSString *text = [NSString stringWithFormat:@"Here is my workout summary for %@. \n Start tracking your workouts with gyminutes.", date];
        NSURL *url = [NSURL URLWithString:@"http://gyminutesapp.com"];
        
        
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[text, url, viewImage.image]
         applicationActivities:nil];
        
        [self presentViewController:controller animated:YES completion:^{
            WorkoutList *ex = [exerciseList objectAtIndex:0];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           @"RtInfo", [NSString stringWithFormat:@"%@,%@", ex.routiineName, ex.workoutName],
                                           nil];
            [Flurry logEvent:@"RtSummary" withParameters:articleParams];
        }];

//        dispatch_group_t group = dispatch_group_create();
//        
//        dispatch_group_async(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
//            CGFloat compensateHeight = -(self.navigationController.navigationBar.bounds.size.height+[UIApplication sharedApplication].statusBarFrame.size.height) + 60;
//            [self.collectionView setContentOffset:CGPointMake(0, compensateHeight) animated:YES];
////            [NSThread sleepForTimeInterval:0.5];
//        });
//        
//        dispatch_group_notify(group,dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^ {
//            //   [self showPopupWithStyle:CNPPopupStyleActionSheet];
//            // start x = navigationbar height + 60 for date picker...
//            
//        });
    } else {
        NSLog(@"no exercise present to get the summary view...");
    }
    
 }

-(void) reloadData {
//    exerciseList = [Utilities getAllExerciseForDate:date];
    [exerciseList removeAllObjects];
    for (WorkoutList *workout in [Utilities getAllExerciseForDate:date]) {
        [exerciseList addObject:workout];
    }
//    exerciseList = [Utilities getAllExerciseForDateByExGroup:date];
    
    for (UserWorkout *wrk in exerciseList) {
        NSLog(@"%@", wrk.exerciseGroup);
    }
    NSLog(@"getting data for date %@ %lu", date, [exerciseList count]);

    if ([exerciseList count] == 0) {
        summary.hidden = YES;
        self.view.backgroundColor = [UIColor whiteColor];
    } else {
        summary.hidden = NO;
        self.view.backgroundColor = [UIColor groupTableViewBackgroundColor];
    }
    
    
    if ([exerciseList count] > 0) {
        [self showCoachMarks];
        [Utilities syncExerciseMetaInfoToParse];
    }
    
    [self.collectionView reloadData];
}


-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];  
}

//-(IBAction)addExercise:(id)sender {
//    [self performSegueWithIdentifier:@"selectRoutineSegue" sender:self];
//}

-(IBAction) yesterdayDate: (id) sender {

    date = [Utilities getYesterdayDate:date];
    recordDate.text = date;
    counter = counter - 1;
    [self reloadData];
}
-(IBAction) tomorrowDate: (id) sender {
    
    counter = counter + 1;
    if ([date isEqualToString:currentDate]) {
        counter = 0;
    } else {
        if (counter != 0) {
            date = [Utilities getTomorrowDate:date];
            recordDate.text = date;
        }
    }
    [self reloadData];
    
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    // if user have not added any workout today, show them nothing.. else show them 1 extra cell with summary info in it..
    NSLog(@"exercise list count is %lu", (long) [exerciseList count]);
    if ([exerciseList count] == 0) {
        return 0;
    } else {
        return [exerciseList count] + 1;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"Cell";
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            if (lbl.tag == 100) {
            } else if (lbl.tag == 101 || lbl.tag == 102) {
            } else {
                [lbl removeFromSuperview];
            }
        } else {
        }
    }
    
    for (UIImageView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }

    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]]) {
            if (lbl.tag == 301 || lbl.tag == 302 || lbl.tag == 303)
                [lbl removeFromSuperview];
        }
    }

    
    if (indexPath.row == 0) {
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
        
        NSArray *allExerciseSet = [Utilities getSetsForAllExerciseForDateSortedByTime:date];
        int exercises = ([exerciseList count] != 0) ? (int)[exerciseList count] : 0;
        float weightMvd = 0;
        int setCount =([allExerciseSet count] != 0) ? (int)[allExerciseSet count] : 0;
        
        NSMutableArray *timeSpentDate = [[NSMutableArray alloc] init];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"HH:mm:ss"];
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        
        for (ExerciseSet *set in allExerciseSet) {
            weightMvd += [set.weight floatValue] * [set.rep intValue];
            [timeSpentDate addObject:[dateFormatter dateFromString:set.timeStamp]];
        }
        
        // this is to get all the muscle worked...
        NSMutableArray * unique  = [NSMutableArray array];
        NSMutableSet * processed = [NSMutableSet set];
        
        // for muscle we just look at exercise list as user may not have recorded any data yet..
        for (ExerciseSet *data in exerciseList) {
            NSString *string = data.majorMuscle;
            if ([processed containsObject:string] == NO) {
                [unique addObject:string];
                [processed addObject:string];
            }
        }
        
        // this is imporant - we set our input date format to match our input string
        // if format doesn't match you'll get nil from your string, so be careful
        NSDate *date2, *date1;
        if (setCount == 0) {
            date2 = date1 = [NSDate date];
        } else {
            date2 = [timeSpentDate objectAtIndex:0];
            date1 = [timeSpentDate objectAtIndex:[timeSpentDate count]- 1];
        }
        
        NSTimeInterval secs = [date1 timeIntervalSinceDate:date2];
//        NSLog(@"Workout Duration: %ld:%ld:%ld %@", (long) secs/3600, (long)(secs/60)%60, (long) secs %60, date);
        
        NSPredicate *prPredicate = [NSPredicate predicateWithFormat:@"maxDate == %@", date];
        NSArray *exMetaInfoForPR = [ExerciseMetaInfo MR_findAllWithPredicate:prPredicate inContext:localContext];
        
        WorkoutList *data = [exerciseList objectAtIndex:indexPath.row];
        cell.layer.cornerRadius = 5;
        cell.backgroundColor = [Utilities getMajorMuscleColor:data.majorMuscle];
        
        UILabel *routineName = (UILabel *)[cell.contentView viewWithTag:100];
        UILabel *workoutName = (UILabel *)[cell.contentView viewWithTag:101];
        UIColor *whiteColor = [UIColor whiteColor];
        
        routineName.text = ([data.routiineName length]  == 0) ? @"Routine Summary" : [NSString stringWithFormat:@"%@ - Summary", data.routiineName];
        NSString *workoutNameStr = [NSString stringWithFormat:@"%@ (%@)", ([data.workoutName length] == 0) ? @"Workout" :data.workoutName, date];
        workoutName.text = workoutNameStr;
        workoutName.backgroundColor = FlatOrange;
        routineName.textAlignment = NSTextAlignmentLeft;
        routineName.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
        
       // NSLog(@"muscle array %@", unique);
        UIView *customViewMuscle = [[UIView alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(workoutName.frame) + 10, CGRectGetMaxX(self.view.frame) - 25, 30)];
        customViewMuscle.tag = 301;
        
        float viewWidthMuscle = CGRectGetWidth(customViewMuscle.frame)/[unique count];
        for (int i = 0 ; i < [unique count]; i++) {
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(i * viewWidthMuscle, 0, viewWidthMuscle - 5, 30)];
            lbl.text = [unique objectAtIndex:i];
            [lbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
            lbl.layer.cornerRadius = 15.0f;
            lbl.clipsToBounds=YES;
            lbl.textAlignment = NSTextAlignmentCenter;
            lbl.backgroundColor = whiteColor;
            lbl.textColor = [Utilities getMajorMuscleColor:[unique objectAtIndex:i]];
            [customViewMuscle addSubview:lbl];
        }
        // this is to get all sets, exercise and wt moved..
        UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customViewMuscle.frame) + 10, CGRectGetMaxX(self.view.frame)- 20, 70)];
        // customView.backgroundColor = [UIColor lightGrayColor];
        customView.tag = 302;
        
        // NSLog(@"frame %f, cv %f", CGRectGetWidth(self.view.frame), CGRectGetWidth(customView.frame));
        float viewWidth = CGRectGetWidth(customView.frame)/3;
        for (int i = 0 ; i < 3; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(i * viewWidth, 0, viewWidth, 70)];
            
            UILabel *subViewData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 40)];
            subViewData.textAlignment = NSTextAlignmentCenter;
            subViewData.numberOfLines = 2;
            
            UILabel *subViewtitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(subViewData.frame), viewWidth, 30)];
            subViewtitle.textAlignment = NSTextAlignmentCenter;
            subViewtitle.textColor = whiteColor;
            //    NSLog(@"x %f, wd: %f, lb %f, %f", i * viewWidth, viewWidth, CGRectGetMinX(view.frame), CGRectGetWidth(view.frame));
            [subViewtitle setFont:[UIFont fontWithName: @"HelveticaNeue" size:14]];
            [subViewData setFont:[UIFont fontWithName: @HAL_BOLD_FONT size:30]];
            
            switch (i) {
                case 0:
                    subViewtitle.text = @"Exercises";
                    subViewData.text = [NSString stringWithFormat:@"%d", exercises];
                    subViewData.textColor = whiteColor;
                    break;
                case 1:
                    subViewtitle.text = @"Sets";
                    subViewData.text = [NSString stringWithFormat:@"%d", setCount];
                    subViewData.textColor = whiteColor;
                    break;
                case 2:
                    subViewtitle.text = [NSString stringWithFormat:@"Wt. Mvd (%@)", [Utilities getUnits]];
                    subViewData.text = [NSString stringWithFormat:@"%d", (int) weightMvd];
                    subViewData.textColor = whiteColor;

                    break;
                default:
                    break;
            }
            [view addSubview:subViewtitle];
            [view addSubview:subViewData];
            [customView addSubview:view];
        }
        
        // this is to get all sets, exercise and wt moved..
        UIView *customView2nd = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(customView.frame) + 10, CGRectGetMaxX(self.view.frame)- 20, 70)];
        // customView.backgroundColor = [UIColor lightGrayColor];
        customView2nd.tag = 303;
        
        // NSLog(@"frame %f, cv %f", CGRectGetWidth(self.view.frame), CGRectGetWidth(customView.frame));
        float viewWidth2nd = CGRectGetWidth(customView.frame)/2;
        for (int i = 0 ; i < 2; i++) {
            UIView *view = [[UIView alloc] initWithFrame:CGRectMake(i * viewWidth2nd, 0, viewWidth2nd, 70)];
            
            UILabel *subViewData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth2nd, 40)];
            subViewData.textAlignment = NSTextAlignmentCenter;
            subViewData.numberOfLines = 2;
            
            UILabel *subViewtitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(subViewData.frame), viewWidth2nd, 30)];
            subViewtitle.textAlignment = NSTextAlignmentCenter;
            subViewtitle.textColor = whiteColor;
            //    NSLog(@"x %f, wd: %f, lb %f, %f", i * viewWidth, viewWidth, CGRectGetMinX(view.frame), CGRectGetWidth(view.frame));
            [subViewtitle setFont:[UIFont fontWithName: @"HelveticaNeue" size:14]];
            [subViewData setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:30]];
            
            switch (i) {
                case 0:
                    subViewtitle.text = @"Workout Duration";
                    subViewData.text = [NSString stringWithFormat:@"%ld:%ld:%ld", (long) secs/3600, (long)(secs/60)%60, (long) secs %60];
                    subViewData.textColor = whiteColor;
                    break;
                case 1: {
                    int prCount = 0;
                    for (ExerciseMetaInfo *extemp in exMetaInfoForPR) {
                        if ([extemp.maxWeight floatValue] > 0)
                            prCount++;
                    }
                    subViewtitle.text = @"Personal Record (PR)";
                    subViewData.text =  [NSString stringWithFormat:@"%d", prCount];
                    subViewData.textColor = whiteColor;
                }
                    break;
                default:
                    break;
            }
            [view addSubview:subViewtitle];
            [view addSubview:subViewData];
            [customView2nd addSubview:view];
        }

        [cell.contentView addSubview:customViewMuscle];
        [cell.contentView addSubview:customView];
        [cell.contentView addSubview:customView2nd];
    
//        statusImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cell.frame)- 35, CGRectGetMinY(routineName.frame) + 10, 15, 15)];
//        statusImg.tag = 103;
//        statusImg.image = [Utilities getDetailedImage];

//        UIButton *detailButton = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cell.frame)- 35, CGRectGetMinY(routineName.frame) + 5, 20, 20)];
//        [detailButton setImage:[UIImage imageNamed:@"IconDetailButton"] forState:UIControlStateNormal];
//        [detailButton addTarget:self action:@selector(loadDetailedSummaryView) forControlEvents:UIControlEventTouchUpInside];
//        
//        [cell.contentView addSubview:detailButton];
//        [cell.contentView addSubview:statusImg];


    } else {
        
       // NSLog(@"index is %ld ***", (long)indexPath.row);
        for (UIButton *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UIButton class]]) {
                    [lbl removeFromSuperview];
            }
        }


    WorkoutList *data = [exerciseList objectAtIndex:indexPath.row - 1];
    UILabel *exerciseName = (UILabel *)[cell.contentView viewWithTag:100];
    UILabel *pr = (UILabel *)[cell.contentView viewWithTag:101];

        exerciseName.textColor = [UIColor whiteColor];
    exerciseName.textAlignment = NSTextAlignmentLeft;
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getMajorMuscleColor:data.majorMuscle];
    
    exMetaInfo = [Utilities getExerciseMetaInfo:data.exerciseName];
    
        
//    UIButton *trashView =  (UIButton *)[cell viewWithTag:103];
//    [trashView addTarget:self action:@selector(deleteItemsAtIndexPaths:) forControlEvents:UIControlEventTouchDragInside];
//    trashView.hidden = NO;

    
    exerciseName.text =  data.exerciseName;
    if (exMetaInfo != nil) {
        if ( [exMetaInfo.maxRep intValue] == 0 || [exMetaInfo.maxWeight floatValue] == 0)
            pr.text = @"No Personal Record Set";
        else
            pr.text = [NSString stringWithFormat:@"PR: %@ (%d x %.1f %@)", exMetaInfo.maxDate, [exMetaInfo.maxRep intValue], [exMetaInfo.maxWeight floatValue], [Utilities getUnits]];
    } else
        pr.text = @"No Personal Record Set";
    
    pr.textAlignment = NSTextAlignmentCenter;
    pr.backgroundColor = FlatOrange;

    
    statusImg = [[UIImageView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cell.frame)- 35, CGRectGetMinY(exerciseName.frame) + 10, 15, 15)];
    statusImg.tag = 103;
    [cell.contentView addSubview:statusImg];
    
    NSArray *previousWorkout = [Utilities getSetForExerciseOnPreviosDate:data.exerciseName date:date rName:data.routiineName wName:data.workoutName];
    NSArray *todayWorkout = [Utilities getSetForExerciseOnDate:data.exerciseName date:date];

    int previousMaxIndex = -1, todayMaxIndex = -1;
    NSString *previousSetCount = @"", *todaySetCount = @"";
    NSString *previousMax = @"", *todayMax = @"";
    NSString *previousWtMvd = @"", *todayWtMvd = @"";
    NSString *previousDate = @"";
    
    if ([previousWorkout count] == 0) {
        previousSetCount = @"NA";
        previousMax = @"NA";
        previousWtMvd = @"NA";
        previousDate = @"Previous";
    } else {
        previousSetCount = [NSString stringWithFormat:@"%lu", (unsigned long)[previousWorkout count]];
        previousMaxIndex = [Utilities getMaxIndexForExerciseSetArray:previousWorkout];
        ExerciseSet *previousMaxSet =[previousWorkout objectAtIndex:previousMaxIndex];
        previousMax = [NSString stringWithFormat:@"%@ x %@", previousMaxSet.rep, previousMaxSet.weight];
//        previousWtMvd = [NSString stringWithFormat:@"%.1f", [previousMaxSet.rep intValue]* [previousMaxSet.weight floatValue]];
        float preWtMvd = 0;
        for (int i = 0; i < [previousWorkout count]; i++) {
            ExerciseSet *set = [previousWorkout objectAtIndex:i];
            preWtMvd += [set.rep intValue] * [set.weight floatValue];
        }
        previousWtMvd = [NSString stringWithFormat:@"%.1f", preWtMvd];
        previousDate = [NSString stringWithFormat:@"(%@) Previous", previousMaxSet.date];
    }
    
    if ([todayWorkout count] == 0) {
        todaySetCount = @"0";
        todayMax = @"NA";
        todayWtMvd = @"NA";
       // NSLog(@"setting image as count == 0");
        statusImg.image = [Utilities getStartMark];
    } else {
        todaySetCount = [NSString stringWithFormat:@"%lu", (unsigned long)[todayWorkout count]];
        todayMaxIndex = [Utilities getMaxIndexForExerciseSetArray:todayWorkout];
        ExerciseSet *todayMaxSet =[todayWorkout objectAtIndex:todayMaxIndex];
        todayMax = [NSString stringWithFormat:@"%@ x %@", todayMaxSet.rep, todayMaxSet.weight];
        //todayWtMvd = [NSString stringWithFormat:@"%.1f", [todayMaxSet.rep intValue]* [todayMaxSet.weight floatValue]];
        float todWtMvd = 0;
        for (int i = 0; i < [todayWorkout count]; i++) {
            ExerciseSet *set = [todayWorkout objectAtIndex:i];
            todWtMvd += [set.rep intValue] * [set.weight floatValue];
        }
        todayWtMvd = [NSString stringWithFormat:@"%.1f", todWtMvd];
        if ([todayWorkout count] >= [data.setsSuggested intValue]) {
            statusImg.image = [Utilities getCompleteMark];
            NSLog(@"suggested sets %d", [data.setsSuggested intValue]);
        }
        else {
            statusImg.image = [Utilities getIncompleteMark];
            statusImg.tintColor = [UIColor whiteColor];
            NSLog(@"not zrto but not greated either... suggested sets %d", [data.setsSuggested intValue]);
        }
    }
    
   
    for (int i = 0 ; i < 4; i++) {
//        NSLog(@"%ld %d %@ %@ %@ %@ %@ %@", (long)indexPath.row, i, previousSetCount, todaySetCount, previousMax, todayMax, previousWtMvd, todayWtMvd);
        
        if (i == 0) {
            lblLeft = [[UILabel alloc] initWithFrame:CGRectMake(5,  CGRectGetMaxY(pr.frame) + 10 + i * 25, 0, 21)];
            lblCenter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblLeft.frame), CGRectGetMaxY(pr.frame) + 10 + i * 25, cell.frame.size.width/3 * 2 - 5, 21)];
            centerFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f];
            RightFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f];
        }
        else {
            lblLeft = [[UILabel alloc] initWithFrame:CGRectMake(5,  CGRectGetMaxY(pr.frame) + 10 + i * 25, cell.frame.size.width/3  - 5, 21)];
            lblCenter = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblLeft.frame), CGRectGetMaxY(pr.frame) + 10 + i * 25, cell.frame.size.width/3, 21)];
            leftFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:14.0f];
            centerFont = [UIFont fontWithName:@"HelveticaNeue-Thin" size:15.0f];
            RightFont = [UIFont fontWithName:@"HelveticaNeue-Bold" size:15.0f];
        }
        
        lblRight = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(lblCenter.frame), CGRectGetMaxY(pr.frame)+ 10 + i * 25, cell.frame.size.width/3 - 5, 21)];

        lblLeft.textAlignment = NSTextAlignmentLeft;
        lblCenter.textAlignment = NSTextAlignmentRight;
        lblRight.textAlignment = NSTextAlignmentRight;

        lblLeft.textColor = [UIColor whiteColor];
        lblCenter.textColor = [UIColor whiteColor];
        lblRight.textColor = [UIColor whiteColor];
        
//        lblLeft.backgroundColor = FlatRed;
//        lblCenter.backgroundColor = FlatSandDark;
//        lblRight.backgroundColor = FlatRedDark;
        lblLeft.text = @"";
        lblCenter.text = @"";
        lblRight.text = @"";

        
        lblLeft.font = leftFont;
        lblCenter.font = centerFont;
        lblRight.font = RightFont;
        
        switch (i) {
            case 0:
                lblLeft.text = @"";
                lblCenter.text = previousDate;
                lblRight.text = @"Today";
                break;
            case 1:
                lblLeft.text = @"Sets";
                lblCenter.text = previousSetCount;
                lblRight.text = todaySetCount;
                break;
            case 2:
                lblLeft.text = [NSString stringWithFormat:@"Max (%@)", [Utilities getUnits]];
                lblCenter.text = previousMax;
                lblRight.text = todayMax;
                break;
            case 3:
                lblLeft.text = [NSString stringWithFormat:@"Wt Moved (%@)",  [Utilities getUnits]];
                lblCenter.text = previousWtMvd;
                lblRight.text = todayWtMvd;
                break;
                
            default:
                break;
        }
        [cell.contentView addSubview:lblLeft];
        [cell.contentView addSubview:lblCenter];
        [cell.contentView addSubview:lblRight];
    
    }
    

    UISwipeGestureRecognizer* gestureRecognizer = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(userDidSwipe:)];
    [gestureRecognizer setDirection:UISwipeGestureRecognizerDirectionLeft];
    [cell addGestureRecognizer:gestureRecognizer];
    }
    return cell;
}
- (void)userDidSwipe:(UIGestureRecognizer *)gestureRecognizer {
    if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        //handle the gesture appropriately
//        NSLog(@"gesture delete detected");

        CGPoint tapLocation = [gestureRecognizer locationInView:collectionView];
        NSIndexPath *indexPath = [collectionView indexPathForItemAtPoint:tapLocation];

        if (indexPath.row == 0) {

            SCLAlertView *alert  = [[SCLAlertView alloc] init];
            [alert showInfo:self title:@"Not allowed." subTitle:@"Summary view cannot be deleted." closeButtonTitle:@"Ok" duration:0.0f];
        } else {
            
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert addButton:@"Yes" actionBlock:^{
                NSLog(@"***** %lu, %ld", (long) [exerciseList count], (long)indexPath.row - 1);
                WorkoutList *ex = [exerciseList objectAtIndex:indexPath.row - 1];
                NSLog(@"deleting... %ld,,, %@", (long)indexPath.row, ex.exerciseName);
                [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                    [ex MR_deleteEntityInContext:localContext];
                    
                    if ([exerciseList count] == 1) {
                        exerciseList = nil;
                    }
                    [exerciseList removeObjectAtIndex:indexPath.row - 1];
                    
                    NSMutableArray * unique = [NSMutableArray array];
                    NSMutableSet * processedGroup = [NSMutableSet set];
                    
                    for (WorkoutList *data in exerciseList) {
                        NSNumber *groupString = data.exerciseGroup;
                        if ([processedGroup containsObject:groupString] == NO) {
                            [unique addObject:groupString];
                            [processedGroup addObject:data.exerciseGroup];
                        }
                    }
                    
                    //NSLog(@"unique is %@", unique);
                    int exerciseCount = 0, exerciseGroupRunning = 0;
                    int exGroupCounter = 0;

                    for (int i = 0; i < [unique count]; i++) {
                        NSPredicate *groupPredicate = [NSPredicate predicateWithFormat:@"exerciseGroup == %@", [unique objectAtIndex:i]];
                        NSArray *exerciseInThisGroup = [exerciseList filteredArrayUsingPredicate:groupPredicate];
                        
                        if ([exerciseInThisGroup count] == 0) {
                            //this means that intermediate ex got deleted... so we need to be careful.
                            NSLog(@"all ex of group got deleted.. lets move on to next one and re-adjust everyone %d", i);
                        } else {
                            for (int j = 0; j < [exerciseInThisGroup count]; j++) {
                                WorkoutList *data = [exerciseInThisGroup objectAtIndex:j];
                                data.exerciseGroup = [NSNumber numberWithInt:exerciseGroupRunning];
                                data.exerciseNumInGroup = [NSNumber numberWithInt:j];
                                data.exerciseNumber = [NSNumber numberWithInt:exerciseCount];
                                NSLog(@"exercise group running is %d", exerciseGroupRunning);
                                exerciseCount++;
                            }
                            exerciseGroupRunning++;
                        }
                        NSLog(@"exgroup conter is %d", exGroupCounter);
                        exGroupCounter++;
                    }
                } completion:^(BOOL contextDidSave, NSError *error) {
                    NSLog(@"entry deleted successfully.. reload collectionview...");
                    
                    [self reloadData];
                }];
            }];
            [alert showWarning:self title:@"Delete Exercise" subTitle:@"Are you sure you want to delete this exercise for today's workout?" closeButtonTitle:@"No" duration:0.0f];
        }
    }
}
-(void) loadDetailedSummaryView {
    exerciseSelected = [exerciseList objectAtIndex:0];
    [self performSegueWithIdentifier:@"showSummarySegue" sender:self];
    
}
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        exerciseSelected = [exerciseList objectAtIndex:indexPath.row];
    } else {
        exerciseSelected = [exerciseList objectAtIndex:indexPath.row - 1];
    }
   // [self performSegueWithIdentifier:@"trackSegueNew" sender:self];
    [self performSegueWithIdentifier:@"recordWorkoutSegue" sender:self];
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    float cellWidth = (self.collectionView.frame.size.width) - 20;
    float cellHeight = 175.0f;//(self.view.frame.size.width)/2;
    if (indexPath.row == 0)
        cellHeight = 270;
    return CGSizeMake(cellWidth, cellHeight);
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    if ([currentDate isEqualToString:date])
        text = @"No workout performed TODAY.";
    else
        text = [NSString stringWithFormat:@"No workout performed on %@.", date];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"To start a workout, select \"HOME\" (Bottom Left) and click \"START WORKOUT\"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"Icon_Home"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}


 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"selectRoutineSegue"]) {
//        SelectRoutine *destVC = segue.destinationViewController;
//        destVC.date = date;
    } else if ([segue.identifier isEqualToString:@"trackSegueNew"]) {
        //ExerciseTrackingVC *destVC = segue.destinationViewController;
        RecordExercise *destVC = segue.destinationViewController;
        NSLog(@"selected exercise is %@", exerciseSelected.exerciseName);
        destVC.exerciseObj = exerciseSelected;
        destVC.date = date;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.exerciseNumber = exerciseSelected.exerciseNumber;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"showSummarySegue"]) {
        ExerciseSummary *destVC = segue.destinationViewController;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.date = date;
    } else if ([segue.identifier isEqualToString:@"recordWorkoutSegue"]) {
        //ExerciseTrackingVC *destVC = segue.destinationViewController;
//        RecordWorkout *destVC = segue.destinationViewController;
        RecWorkout *destVC = segue.destinationViewController;
        NSLog(@"selected exercise is %@", exerciseSelected.exerciseName);
        destVC.exerciseObj = exerciseSelected;
        destVC.date = date;
        destVC.routineName = exerciseSelected.routiineName;
        destVC.workoutName = exerciseSelected.workoutName;
        destVC.exerciseNumber = exerciseSelected.exerciseNumber;
        destVC.hidesBottomBarWhenPushed = TRUE;
    }    //self.title = @"";
}

- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {

    NSMutableParagraphStyle *headingStyle = NSMutableParagraphStyle.new;
    headingStyle.lineBreakMode = NSLineBreakByWordWrapping;
    headingStyle.alignment = NSTextAlignmentCenter;

    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentLeft;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Workout Summary!" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];
    
    NSAttributedString *subTitle = [[NSAttributedString alloc] initWithString:date attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:16], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatSkyBlueDark}];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;

    UILabel *subTitleLabel = [[UILabel alloc] init];
    subTitleLabel.numberOfLines = 0;
    subTitleLabel.attributedText = subTitle;
   
    
/*
    // this is to get time spent during workout... START
    NSArray *timeSpent = [Utilities getSetsForAllExerciseForDateSortedByTime:date];
    NSMutableArray *timeSpentDate = [[NSMutableArray alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    // voila!
    for (ExerciseSet *set in timeSpent) {
        [timeSpentDate addObject:[dateFormatter dateFromString:set.timeStamp]];
        NSLog(@"exercise name is %@ at timestamp %@", set.majorMuscle, set.timeStamp);
//        weightMvd += [set.weight floatValue] * [set.rep intValue];
    }

    NSDate* date2 = [timeSpentDate objectAtIndex:0];
    NSDate* date1 = [timeSpentDate objectAtIndex:[timeSpentDate count]- 1];
    NSTimeInterval secs = [date1 timeIntervalSinceDate:date2];
    NSLog(@"Workout Duration: %ld:%ld:%ld", (long) secs/3600, (long)(secs/60)%60, (long) secs %60);
    // this is to get time spent during workout... END
*/    
    // get all sets performed today.
    NSArray *allExerciseSet = [Utilities getSetsForAllExerciseForDate:date];
    
    int exercises = ([exerciseList count] != 0) ? (int)[exerciseList count] : 0;
    float weightMvd = 0;
    
    int setCount =([allExerciseSet count] != 0) ? (int)[allExerciseSet count] : 0;
    
    for (ExerciseSet *set in allExerciseSet) {
        //        NSLog(@"exercise name is %@ at timestamp %@", set.majorMuscle, set.timeStamp);
        weightMvd += [set.weight floatValue] * [set.rep intValue];
    }

    // this is to get all the muscle worked...
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in allExerciseSet) {
        NSString *string = data.majorMuscle;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
  //  NSLog(@"muscle array %@", unique);
    UIView *customViewMuscle = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(self.view.frame)- 30, 30)];
    float viewWidthMuscle = CGRectGetWidth(customViewMuscle.frame)/[unique count];
    for (int i = 0 ; i < [unique count]; i++) {
        UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(i * viewWidthMuscle, 0, viewWidthMuscle - 5, 30)];
        lbl.text = [unique objectAtIndex:i];
        [lbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
        lbl.layer.cornerRadius = 15.0f;
        lbl.clipsToBounds=YES;
        lbl.textAlignment = NSTextAlignmentCenter;
        lbl.backgroundColor = [Utilities getMajorMuscleColor:[unique objectAtIndex:i]];
        lbl.textColor = [UIColor whiteColor];
        [customViewMuscle addSubview:lbl];
    }
    // this is to get all sets, exercise and wt moved..
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetMaxX(self.view.frame)- 30, 70)];
   // customView.backgroundColor = [UIColor lightGrayColor];
    
   // NSLog(@"frame %f, cv %f", CGRectGetWidth(self.view.frame), CGRectGetWidth(customView.frame));
    float viewWidth = CGRectGetWidth(customView.frame)/3;
    for (int i = 0 ; i < 3; i++) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(i * viewWidth, 0, viewWidth, 70)];
        
        UILabel *subViewData = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, viewWidth, 40)];
        subViewData.textAlignment = NSTextAlignmentCenter;
        subViewData.numberOfLines = 2;

        UILabel *subViewtitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(subViewData.frame), viewWidth, 30)];
        subViewtitle.textAlignment = NSTextAlignmentCenter;

    //    NSLog(@"x %f, wd: %f, lb %f, %f", i * viewWidth, viewWidth, CGRectGetMinX(view.frame), CGRectGetWidth(view.frame));
        [subViewtitle setFont:[UIFont fontWithName: @"HelveticaNeue-Thin" size:14]];
        [subViewData setFont:[UIFont fontWithName: @"HelveticaNeue" size:30]];
        
        switch (i) {
            case 0:
                subViewtitle.text = @"Exercises";
                subViewData.text = [NSString stringWithFormat:@"%d", exercises];
                subViewData.textColor = FlatMintDark;
                break;
            case 1:
                subViewtitle.text = [NSString stringWithFormat:@"Wt. Mvd (%@)", [Utilities getUnits]];
                subViewData.text = [NSString stringWithFormat:@"%d", (int) weightMvd];
                subViewData.textColor = FlatRedDark;
                break;
            case 2:
                subViewtitle.text = @"Sets";
                subViewData.text = [NSString stringWithFormat:@"%d", setCount];
                subViewData.textColor = FlatOrangeDark;
                break;
            default:
                break;
        }
        [view addSubview:subViewtitle];
        [view addSubview:subViewData];
        [customView addSubview:view];
    }

//    NSAttributedString *muscle = [[NSAttributedString alloc] initWithString:@"Workout Summary!" attributes:@{NSFontAttributeName : [UIFont fontWithName: @"HelveticaNeue-Bold" size:20], NSParagraphStyleAttributeName : headingStyle,  NSForegroundColorAttributeName : FlatBlueDark}];

    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"Share" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
//        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
   
    if ([unique count] > 0) {
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, subTitleLabel, customViewMuscle, customView]];

    } else
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, subTitleLabel, customView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
    
    
}

#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    NSLog(@"Dismissed with button title: %@", title);
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}


-(IBAction)saveToParse:(id)sender {
    
    [Utilities logout];
}

-(IBAction) startRefresh:(id)sender {
    NSLog(@"refresh clicked...");
    [refreshControl endRefreshing];
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Add Exercise" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatWhite;
    
    myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, 250, CGRectGetHeight(self.view.frame)/3*2)];
    myTableView.delegate = self;
    myTableView.dataSource = self;

    searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(myTableView.frame), 44)];
    searchBar.autocorrectionType = UITextAutocorrectionTypeNo;
    searchBar.placeholder = @"search";
    //searchBar.delegate = self;
    searchBar.searchBarStyle = UISearchBarStyleMinimal;
    UITextField *searchField = [searchBar valueForKey:@"_searchField"];
    searchField.textColor = [UIColor redColor];
    
    [searchBar becomeFirstResponder];
    
    
    //searchDisplayController.delegate = self;
    // searchDisplayController.searchResultsDataSource = self;
    
    [myTableView setTableHeaderView:searchBar];
    myTableView.backgroundColor = FlatOrange;
    myTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, myTableView]];
    //self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.delegate = self;
    self.popupController.theme.backgroundColor = FlatOrange;
    [self.popupController presentPopupControllerAnimated:YES];
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.myTableView)
    {
        return [allExerciseList count];
    } else
        return [searchResults count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
//    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
  
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    ExerciseList *data;
    if (tableView == self.myTableView)
    {
         data = [allExerciseList objectAtIndex:indexPath.row];
    } else {
        data = [searchResults objectAtIndex:indexPath.row];
    }
    cell.backgroundColor = FlatOrange;

    cell.textLabel.font = [UIFont systemFontOfSize:12.0];
    cell.textLabel.textColor = FlatWhite;
    cell.textLabel.text = data.exerciseName;
    
    cell.detailTextLabel.font = [UIFont systemFontOfSize:10.0];
    cell.detailTextLabel.textAlignment = NSTextAlignmentRight;
    cell.detailTextLabel.textColor = FlatWhiteDark;
    cell.detailTextLabel.text = data.muscle;
    return cell;
    
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseList *data;
    
    if (tableView == self.myTableView)
    {
        data = [[allExerciseList objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
    } else {
        data = [searchResults objectAtIndex:indexPath.row];
    }
    
    // toast with duration, title, and position
    [self.view makeToast:data.exerciseName
                duration:0.5
                position:CSToastPositionCenter
                   title:@"Exercise Added"];
    //[Utilities createWorkoutListForExerciseOnDate:data date:date];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
//    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName contains[c] %@", searchText];
    
    searchResults = [allExerciseList filteredArrayUsingPredicate:predicate];
    
}

//workout complete button

-(void) workoutComplete {
    if (showWorkoutCompleteBtn == true) {
        additionOptionView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44)];
        
        [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame) - 44, CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            
        } completion:^(BOOL finished) {
            
        }];
        NSLog(@"reloading the end workout...");
        additionOptionView.backgroundColor = FlatOrange;
        
        UIButton *dropSet = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(self.view.frame)/2 , 35)];
        dropSet.backgroundColor = [UIColor whiteColor];
        dropSet.layer.cornerRadius = 5.0f;
        [dropSet setTitle:@"Workout Complete!" forState:UIControlStateNormal];
        [dropSet setTitleColor:FlatOrange forState:UIControlStateNormal];
        [dropSet addTarget:self action:@selector(workoutCompleteSync:) forControlEvents:UIControlEventTouchUpInside];
        
        dropSet.center = CGPointMake(additionOptionView.frame.size.width  / 2,
                                     additionOptionView.frame.size.height / 2);
        
        [additionOptionView addSubview:dropSet];
        [self.view addSubview:additionOptionView];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
    } completion:^(BOOL finished) {
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.collectionView.frame) - 44, CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
        
    } completion:^(BOOL finished) {
        
    }];
    
}

-(IBAction)workoutCompleteSync:(id)sender {
    NSLog(@"workout completed. Syncing now.... %@", date);
    [Utilities syncExerciseSetToParse:date];
    
    [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
        
        additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
        //self.headerView.frame  = CGRectMake(0, 5, 320,0);
    } completion:^(BOOL finished) {
    }];
}

/**
 Takes a screenshot of a UIView at a specific point and size, as denoted by
 the provided croppingRect parameter. Returns a UIImageView of this cropped
 region.
 
 CREDIT: This is based on @escrafford's answer at http://stackoverflow.com/a/15304222/535054
 */
- (UIImageView *)rp_screenshotImageViewWithCroppingRect:(CGRect)croppingRect {
    // For dealing with Retina displays as well as non-Retina, we need to check
    // the scale factor, if it is available. Note that we use the size of teh cropping Rect
    // passed in, and not the size of the view we are taking a screenshot of.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(croppingRect.size, YES, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(croppingRect.size);
    }
    
    // Create a graphics context and translate it the view we want to crop so
    // that even in grabbing (0,0), that origin point now represents the actual
    // cropping origin desired:
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -croppingRect.origin.x, -croppingRect.origin.y);
    [self.view.layer renderInContext:ctx];
    
    // Retrieve a UIImage from the current image context:
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the image in a UIImageView:
    return [[UIImageView alloc] initWithImage:snapshotImage];
}

// popoup calendar delete and data source...

// Returns YES if the date should be highlighted or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldHighlightDate:(NSDate *)date
{
    return YES;
}

// Returns YES if the date should be selected or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldSelectDate:(NSDate *)myDate
{
    return YES;
}

// Prints out the selected date.
- (void)datePickerView:(RSDFDatePickerView *)view didSelectDate:(NSDate *)calDate
{
   
    [view removeFromSuperview];
    NSLog(@"%@", [calDate description]);

    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSDate *temp = [formatter dateFromString:[calDate formattedDateWithFormat:@"yyyy-MM-dd"]];
    
    [self.datepicker selectDate:calDate];
    
    NSLog(@"selected date is... %@, temp %@",[formatter stringFromDate:self.datepicker.selectedDate], temp);

    date = [formatter stringFromDate:calDate];
    [self reloadData];
    calendarFlag = false;
    

}

// calendar data source
// Returns YES if the date should be marked or NO if it should not.
- (BOOL)datePickerView:(RSDFDatePickerView *)view shouldMarkDate:(NSDate *)mydate
{
    // The date is an `NSDate` object without time components.
    // So, we need to use dates without time components.
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];
    
    if ([calendarColorDict objectForKey:[formatter stringFromDate:mydate]]) {

        return YES;
    }
    
    
    if ([[formatter stringFromDate:today] isEqualToString:[formatter stringFromDate:mydate]])
        return YES;

    
    return NO;
}

// Returns the color of the default mark image for the specified date.
- (UIColor *)datePickerView:(RSDFDatePickerView *)view markImageColorForDate:(NSDate *)myDate
{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    NSDate *today = [NSDate date];

    if ([[formatter stringFromDate:today] isEqualToString:[formatter stringFromDate:myDate]])
        return FlatRed;

    
    //NSLog(@"date %@ myDate %@, color %@", today, myDate, [calendarColorDict objectForKey:myDate]);

    
    return [Utilities getMajorMuscleColor:[calendarColorDict objectForKey:[formatter stringFromDate:myDate]]];
}

//// Returns the mark image for the specified date.
//- (UIImage *)datePickerView:(RSDFDatePickerView *)view markImageForDate:(NSDate *)date
//{
//    NSLog(@"=========>>>>> %s",__func__);
//    if (arc4random() % 2 == 0) {
//        return [UIImage imageNamed:@"IconComplete"];
//    } else {
//        return [UIImage imageNamed:@"IconIncomplete"];
//    }
//}



#pragma mark PremiumPackage

-(void)showPurchaseMenu:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"UPGRADE TO PREMIUM" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Trial expired. Please upgrade to premium package." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"To continue using the GYMINUTES, please upgrade to premium package." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    button.enabled = false;
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitle:@"TRIAL EXPIRED" forState:UIControlStateNormal];
    [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
    button.layer.cornerRadius = 4;
    button.layer.borderWidth = 2;
    button.layer.borderColor = FlatGrayDark.CGColor;
    button.tintColor = FlatOrangeDark;
    button.backgroundColor = FlatGrayDark;
    
    NSNumberFormatter * _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            NSArray *prod = products;
            
            button.enabled = true;
            button.layer.borderColor = FlatOrangeDark.CGColor;
            button.backgroundColor = FlatOrangeDark;
            
            for (SKProduct * product in prod) {
                if ([product.productIdentifier isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
                    [_priceFormatter setLocale:product.priceLocale];
                    [button setTitle:[NSString stringWithFormat:@"BUY %@", [_priceFormatter stringFromNumber:product.price]] forState:UIControlStateNormal];
                    break;
                }
            }
        }
    }];
    
    
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
        
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatOrangeDark;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    //    self.popupController.theme.backgroundColor = [UIColor colorWithRed:(160/255.0) green:(97/255.0) blue:(5/255.0) alpha:1];
    //    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}
@end
