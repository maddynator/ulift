//
//  DailyStats+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "DailyStats+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface DailyStats (CoreDataProperties)

+ (NSFetchRequest<DailyStats *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *bodyFat;
@property (nullable, nonatomic, copy) NSNumber *bodyMassIndex;
@property (nullable, nonatomic, copy) NSNumber *caloriesBurned;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSNumber *elMorning;
@property (nullable, nonatomic, copy) NSNumber *elPostWorkout;
@property (nullable, nonatomic, copy) NSNumber *elPreWorkout;
@property (nullable, nonatomic, copy) NSNumber *steps;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSNumber *weight;

@end

NS_ASSUME_NONNULL_END
