//
//  CreateRoutineForm.h
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXForms.h"
#import "commons.h"



typedef NS_ENUM(NSInteger, Color)
{
    Red = 0,
    Blue,
    Pink,
    Magenta,
    Brown,
    Coffee,
    Maroon,
    Mint
};

typedef NS_ENUM(NSInteger, Level)
{
    Beginner = 0,
    Intermediate,
    Advance,
    Monster
};

typedef NS_ENUM(NSInteger, Type)
{
    Strength = 0,
    Bodybuilding,
    Sports
};

@interface CreateRoutineForm : NSObject <FXForm>


@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *summary;
@property (nonatomic, copy) NSString *desc;
@property (nonatomic, copy) NSString *goal;
@property (nonatomic, copy) NSNumber  *duration;
@property (nonatomic, copy) NSString *instructions;
@property (nonatomic, copy) NSString *faqs;
@property (nonatomic, strong) NSString *discalimer;

@property (nonatomic, assign) Color color;
@property (nonatomic, assign) Type type;
@property (nonatomic, assign) Level level;
//@property (nonatomic, retain) NSString * routineImage;

@end
