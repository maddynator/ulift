//
//  IAPHelper.m
//  uLift
//
//  Created by Mayank Verma on 9/10/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "IAPHelper.h"
#import <StoreKit/StoreKit.h>
#import "commons.h"
#import "UpdateHandler.h"

// Add to top of file
NSString *const IAPHelperProductPurchasedNotification = @"IAPHelperProductPurchasedNotification";


@interface IAPHelper () <SKProductsRequestDelegate, SKPaymentTransactionObserver>
@end

@implementation IAPHelper {
    // 3
    SKProductsRequest * _productsRequest;
    
    // 4
    RequestProductsCompletionHandler _completionHandler;
    NSSet * _productIdentifiers;
    NSMutableSet * _purchasedProductIdentifiers;
}

- (id)initWithProductIdentifiers:(NSSet *)productIdentifiers {
    
    if ((self = [super init])) {
        
        // Store product identifiers
        _productIdentifiers = productIdentifiers;
        
        // Check for previously purchased products
        _purchasedProductIdentifiers = [NSMutableSet set];
        for (NSString * productIdentifier in _productIdentifiers) {
            BOOL productPurchased = [[NSUserDefaults standardUserDefaults] boolForKey:productIdentifier];
            if (productPurchased) {
                [_purchasedProductIdentifiers addObject:productIdentifier];
                NSLog(@"Previously purchased: %@", productIdentifier);
            } else {
                NSLog(@"Not purchased: %@", productIdentifier);
            }
        }
        [[SKPaymentQueue defaultQueue] addTransactionObserver:self];
    }

    return self;
}

- (void)requestProductsWithCompletionHandler:(RequestProductsCompletionHandler)completionHandler {
    
    // 1
    _completionHandler = [completionHandler copy];
    
    // 2
    _productsRequest = [[SKProductsRequest alloc] initWithProductIdentifiers:_productIdentifiers];
    _productsRequest.delegate = self;
    [_productsRequest start];
    
}

#pragma mark - SKProductsRequestDelegate

- (void)productsRequest:(SKProductsRequest *)request didReceiveResponse:(SKProductsResponse *)response {
    
    NSLog(@"Loaded list of products... %lu",(long)[response.products count]);
    _productsRequest = nil;
    
    NSArray * skProducts = response.products;
//    for (SKProduct * skProduct in skProducts) {
//        NSLog(@"Found product: %@ %@ %0.2f",
//              skProduct.productIdentifier,
//              skProduct.localizedTitle,
//              skProduct.price.floatValue);
//    }
    
    if (_completionHandler) {
        _completionHandler(YES, skProducts);
        _completionHandler = nil;
    }
    
    if ([KVNProgress isVisible]){
        [KVNProgress dismiss];
    }
}

- (void)request:(SKRequest *)request didFailWithError:(NSError *)error {
    
    NSLog(@"Failed to load list of products.");
    _productsRequest = nil;    
    _completionHandler(NO, nil);
    _completionHandler = nil;
    
}

- (BOOL)productPurchased:(NSString *)productIdentifier {
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
    return [_purchasedProductIdentifiers containsObject:productIdentifier];
}

- (void)buyProduct:(SKProduct *)product {
    
    NSLog(@"Buying %@...", product.productIdentifier);

    SKPayment * payment = [SKPayment paymentWithProduct:product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)paymentQueue:(SKPaymentQueue *)queue updatedTransactions:(NSArray *)transactions
{
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }

    for (SKPaymentTransaction * transaction in transactions) {
        switch (transaction.transactionState)
        {
            case SKPaymentTransactionStatePurchased:
                [self completeTransaction:transaction];
                break;
            case SKPaymentTransactionStateFailed:
                [self failedTransaction:transaction];
                break;
            case SKPaymentTransactionStateRestored:
                [self restoreTransaction:transaction];
            default:
                break;
        }
    };
}

- (void)completeTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"completeTransaction...");
    
    [self provideContentForProductIdentifier:transaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    
    NSLog(@"purchase complete...");
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
    
    if ([self isIAPPackage:transaction.payment.productIdentifier]) {
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"PackageId", transaction.payment.productIdentifier,
                                       nil];
        [Flurry logEvent:@"PackageBought" withParameters:articleParams];
    } else {
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RoutineId", transaction.payment.productIdentifier,
                                       nil];
        [Flurry logEvent:@"RoutineBought" withParameters:articleParams];
    }
}

- (void)restoreTransaction:(SKPaymentTransaction *)transaction {
    NSLog(@"restoreTransaction... %@", transaction.originalTransaction.payment.productIdentifier);
    
    [self provideContentForProductIdentifier:transaction.originalTransaction.payment.productIdentifier];
    [[SKPaymentQueue defaultQueue] finishTransaction:transaction];
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
}

- (void)failedTransaction:(SKPaymentTransaction *)transaction {
    
    NSLog(@"failedTransaction...");
    if (transaction.error.code != SKErrorPaymentCancelled)
    {
        NSLog(@"Transaction error: %@", transaction.error.localizedDescription);
    }
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
    [[SKPaymentQueue defaultQueue] finishTransaction: transaction];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineId", transaction.payment.productIdentifier,
                                   @"Error", transaction.error.localizedDescription,
                                   nil];
    NSLog(@"%@", articleParams);    
    [Flurry logEvent:@"RoutineTransactionFailed" withParameters:articleParams];

}

// Add new method
- (void)provideContentForProductIdentifier:(NSString *)productIdentifier {
    
    [_purchasedProductIdentifiers addObject:productIdentifier];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:productIdentifier];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    if ([self isIAPPackage:productIdentifier]) {
            [self markPurchaseOfProduct:productIdentifier];
    } else {
        [Utilities downloadIAPRoutineFromStore:productIdentifier view:self];
        NSLog(@"downloading content for id [%@]", productIdentifier);
    }
    
    // we still have to post notification as we need to update the tableview.
    //[[NSNotificationCenter defaultCenter] postNotificationName:IAPHelperProductPurchasedNotification object:productIdentifier userInfo:nil];

}
- (void)paymentQueue:(SKPaymentQueue *)queue restoreCompletedTransactionsFailedWithError:(NSError *)error {
    NSLog(@"restore transcation failed %@", error);
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
    
    [Flurry logError:@"RestoreFailed" message:[PFUser currentUser].objectId error:error];

}
- (void)restoreCompletedTransactions {
    NSLog(@"inside complete... dequeuing now...");
    [[SKPaymentQueue defaultQueue] restoreCompletedTransactions];
}

- (void)paymentQueueRestoreCompletedTransactionsFinished:(SKPaymentQueue *)queue {
    NSLog(@"Completed Transactions Finished");
    if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
}

-(void) markPurchaseOfProduct:(NSString *) productId {
    if ([productId isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
        NSLog(@"premium is purchase...");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PurchasedPremium"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:@IAP_ROUTINE_PACKAGE_ID]) {
        NSLog(@"routine pack is purchase....");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PurchasedPowerRoutinePackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:@IAP_DATA_PACKAGE_ID]) {
        NSLog(@"data pack is purchase....");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PurchasedDataPackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:@IAP_WORKOUT_PACKAGE_ID]) {
        NSLog(@"workout pack is purchase....");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PurchasedWorkoutPackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    } else if ([productId isEqualToString:@IAP_ANALYTICS_PACKAGE_ID]) {
        NSLog(@"workout pack is purchase....");
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"PurchasedAnalyticsPackage"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    [UpdateHandler updateUserProfileWithMemberShipInfo];
}

-(BOOL) isIAPPackage:(NSString *) productId {
    if ([productId isEqualToString:@IAP_PREMIUM_PACKAGE_ID] ||
        [productId isEqualToString:@IAP_WORKOUT_PACKAGE_ID] ||
        [productId isEqualToString:@IAP_ROUTINE_PACKAGE_ID] ||
        [productId isEqualToString:@IAP_ANALYTICS_PACKAGE_ID] ||
        [productId isEqualToString:@IAP_DATA_PACKAGE_ID]) {
        return true;
    }
    return false;
}
@end
