//
//  UserWeightManager.h
//  gyminutes
//
//  Created by Mayank Verma on 7/6/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserWeightManager : NSObject
+ (UserWeightManager *)sharedInstance;
+(float) getLastUserWeight;

@end
