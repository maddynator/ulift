//
//  AppTheme.m
//  gyminutes
//
//  Created by Mayank Verma on 5/24/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "AppTheme.h"
#import "commons.h"

@interface AppTheme () {
    NSArray *colorArray, *paid;
}
@end

@implementation AppTheme
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];

    
    self.title = @"Theme";
    //colorArray = @[FlatYellow, FlatYellowDark, FlatWatermelon, FlatWatermelonDark, FlatOrange, FlatOrangeDark, FlatRed, FlatRedDark, FlatMaroon, FlatMaroonDark, FlatPink, FlatPinkDark, FlatMagenta, FlatMagentaDark, FlatPurple, FlatPurpleDark, FlatPlum, FlatPlumDark, FlatPowderBlue, FlatPowderBlueDark, FlatBlue, FlatBlueDark, FlatSkyBlue, FlatSkyBlueDark, FlatTeal, FlatTealDark, FlatNavyBlue, FlatNavyBlueDark, FlatMint, FlatMintDark, FlatGreen, FlatGreenDark, FlatLime, FlatLimeDark, FlatForestGreen, FlatForestGreenDark, FlatGray, FlatGrayDark, FlatBrownDark, FlatBlack, FlatBlackDark];
    colorArray = @[FlatWatermelon,
                   FlatOrange,
                   FlatSkyBlue,
//                   FlatMaroonDark,
                   FlatPink,
                   
                 //  FlatPinkDark,
                   FlatMagenta,
                   FlatPurple,
                   //FlatPurpleDark,
                   FlatPlum,
                   
                   //FlatPlumDark,
//                   FlatPowderBlue,
//                   FlatPowderBlueDark,
                   FlatBlue,
                   FlatMaroon,
                   
//                   FlatSkyBlueDark,
//                   FlatTeal,
//                   FlatTealDark,
//                   FlatNavyBlue,
                   FlatNavyBlueDark,
                   
//                   FlatGreen,
//                   FlatLime,
//                   FlatLimeDark,
                   FlatForestGreen,
//                   FlatForestGreenDark,
                   
//                   FlatGray,
//                   FlatGrayDark, 
//                   FlatBrownDark,
//                   FlatBlack,
                   FlatBlackDark
                   ];
    
    paid = @[[NSNumber numberWithBool:false],
             [NSNumber numberWithBool:false],
             [NSNumber numberWithBool:false],
             //[NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             
            // [NSNumber numberWithBool:false],
             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             //[NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             
             //[NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
             
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],            
//             [NSNumber numberWithBool:true],
//             [NSNumber numberWithBool:true],
             [NSNumber numberWithBool:true],
             ];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];

    _collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.navigationController.tabBarController.tabBar.frame) - 50) collectionViewLayout:layout];
    [_collectionView setDataSource:self];
    [_collectionView setDelegate:self];
    
    [_collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    _collectionView.backgroundColor = [UIColor whiteColor];
    [self.view addSubview:_collectionView];
    

}

-(void) viewWillDisappear:(BOOL)animated {
//    NSLog(@"view disappearing");
//    UIColor *color = [Utilities getAppColor];
//    self.navigationController.navigationBar.barTintColor = color;
//    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:color size:CGSizeMake(320, 49)];
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [colorArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    float cellWidth = CGRectGetWidth(cell.frame);
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]] && lbl.tag == 100) {
            [lbl removeFromSuperview];
        }
        if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    cell.backgroundColor= [UIColor whiteColor];
    
    UIView *circularView = [[UIView alloc] initWithFrame:CGRectMake(5, 15, cellWidth - 10, cellWidth - 10)];
    circularView.layer.cornerRadius = (cellWidth - 10)/2;
    circularView.backgroundColor =[colorArray objectAtIndex:indexPath.row];
    circularView.tag = 100;
    [cell.contentView addSubview:circularView];
    
    if ([Utilities showPremiumPackage]) {
        if ([[paid objectAtIndex:indexPath.row] boolValue]) {
            UIButton *image = [[UIButton alloc] initWithFrame:CGRectMake(5, 15, cellWidth - 10, cellWidth - 10)];
            [image setImage:[UIImage imageNamed:@"IconLocked"] forState:UIControlStateNormal];
            image.imageEdgeInsets = UIEdgeInsetsMake(30, 30, 30, 30);
            [cell.contentView addSubview:image];
            [image addTarget:self action:@selector(imageBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
            image.tag = indexPath.row;
        }
    }
    return cell;
}

-(IBAction)imageBtnPressed:(UIButton *)sender {
    //Acccess the cell
    UICollectionViewCell *cell = (UICollectionViewCell*)[[sender superview] superview];
    NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
    
    if ([[paid objectAtIndex:indexPath.row] boolValue]) {
        [self showPurchasePopUp:@"This theme is not availabe in Free Version."];
    } else {
        [self setColorForIndexPath:indexPath];
    }
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((CGRectGetWidth(self.view.frame) - 20)/3, (CGRectGetWidth(self.view.frame) - 20)/3);
}

-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {

    if ([Utilities showPremiumPackage]) {
        if ([[paid objectAtIndex:indexPath.row] boolValue]) {
            [self showPurchasePopUp:@"This theme is not availabe in Free Version."];
        } else {
            [self setColorForIndexPath:indexPath];
            NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                           @"UserID", [PFUser currentUser].objectId,
                                           @"ColorIndex", [NSString stringWithFormat:@"%ld", (long)indexPath.row],
                                           nil];
            [Flurry logEvent:@"ColorChanged" withParameters:articleParams];            
        }
    } else {
        NSLog(@"color state is %d", [[paid objectAtIndex:indexPath.row] boolValue]);
        [self setColorForIndexPath:indexPath];
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"ColorIndex", [NSString stringWithFormat:@"%ld", (long)indexPath.row],
                                       nil];
        [Flurry logEvent:@"ColorChanged" withParameters:articleParams];
        
//        if ([[colorArray objectAtIndex:indexPath.row] isEqual:FlatBlackDark]) {
//            NSLog(@"color is flat black now.. seting red..");
//            [[UITabBar appearance] setTintColor:FlatRedDark];
//            [[UITabBar appearance] setBackgroundColor:FlatRedDark];
//            
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatWhite }
//                                                     forState:UIControlStateNormal];
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatRedDark }
//                                                     forState:UIControlStateSelected];
//        } else {
//            [[UITabBar appearance] setTintColor:FlatBlack];
//            [[UITabBar appearance] setBackgroundColor:FlatBlack];
//            
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatWhite }
//                                                     forState:UIControlStateNormal];
//            [[UITabBarItem appearance] setTitleTextAttributes:@{ NSForegroundColorAttributeName : FlatBlack }
//                                                     forState:UIControlStateSelected];
//        }
    }
}

-(void) setColorForIndexPath:(NSIndexPath *) indexPath {
    NSLog(@"color changed");
    UIColor *color = [colorArray objectAtIndex:indexPath.row];
    //UIColor *lightColor = [lightColorArray objectAtIndex:indexPath.row];
    self.navigationController.navigationBar.barTintColor = color;
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:color size:CGSizeMake(320, 49)];
    //[[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:color size:CGSizeMake(320, 49)]];
    UITableViewCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    cell.textLabel.text = @"Current Color";
    [self.tableView reloadData];
    
    NSData *colorData = [NSKeyedArchiver archivedDataWithRootObject:color];
    //NSData *colorDataLight = [NSKeyedArchiver archivedDataWithRootObject:lightColor];
    
    
    [[NSUserDefaults standardUserDefaults] setObject:colorData forKey:@"AppColor"];
    //[[NSUserDefaults standardUserDefaults] setObject:colorDataLight forKey:@"AppColorLight"];
}

-(void) showPurchasePopUp: (NSString *) feature {

    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium Package.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

@end
