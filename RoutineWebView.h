//
//  RoutineWebView.h
//  uLift
//
//  Created by Mayank Verma on 10/28/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RoutineWebView : UIViewController
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *urlToLoad, *routineName;
@end
