//
//  PackageDetailCVC.m
//  gyminutes
//
//  Created by Mayank Verma on 1/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "PackageDetailCVC.h"
#import "commons.h"
@interface PackageDetailCVC () {
    NSArray *_premiumPackHeader, *_dataPackHeader, *_workoutPackHeader, *_powerRoutinePackHeader, *_analyticsHeader;
    NSArray *_premiumPackDesc, *_dataPackDesc, *_workoutPackDesc, *_powerRoutinePackDesc, *_analyticsDesc;
    NSArray *_premiumPackImg, *_dataPackImg, *_workoutPackImg, *_powerRoutinePackImg, *_analyticsImg;
    int routineType;   //     DATA = 0,        WORKOUT = 1,        ROUTINE = 2,        PREMIUM = 3
}

@end

@implementation PackageDetailCVC

static NSString * const reuseIdentifier = @"Cell";

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = _product.localizedTitle;
    
    _powerRoutinePackImg = @[@"IconRIAP3", @"IconRIAP4", @"IconRIAP2", @"IconRIAP1"];
    _powerRoutinePackHeader = @[@"CREATE UNLIMITED ROUTINES", @"CREATE SUPER-SET WORKOUT", @"EDIT PREMIUM ROUTINES", @"ANALYZE ROUTINE OVER 1 MONTH"];
    _powerRoutinePackDesc = @[@"Create unlimited workout routines as you progress in you get strong.",
                              @"Create Super-set, tri-set, giant-set and circuis.",
                              @"Modify downloaded routines to fit your workout style and time requirement.",
                              @"Only way to know if a workout is right for you is to analyze your progress. Buy this pack to unlock analysis for over 1 month."];
    
    _dataPackImg = @[@"IconDIAP2", @"IconDIAP1"];
    _dataPackHeader = @[@"UNLIMITED CLOUD SYNC", @"DATA EXPORT VIA EMAIL"];
    _dataPackDesc = @[@"FREE for 1 Year. Store your workout data in secure cloud. Never lose it when you change phone or lose it.",
                      @"Export and analyze your data as you like. Export all your workouts and get crazy with charts."];
    
    _workoutPackImg = @[@"IconWIAP6", @"IconWIAP3", @"IconWIAP2", @"IconWIAP1", @"IconWIAP4", @"IconWIAP5"];
    _workoutPackHeader = @[@"WARMUP CALCULATOR", @"PLATE CALCULATOR", @"DROP SET MARKER", @"LAST WORKOUT INFO", @"ADD TEMPORARY EXERCISE", @"RECORD SEAT AND HAND INFO"];
    _workoutPackDesc = @[@"Calculate all the warmup sets for each exercise.",
                         @"Math may be fun but NOT during workout. Calculate weight on barbel using super simple plate calculator.",
                         @"Looking for that pump, do a drop set. Marking sets as drop sets is now intuitive.",
                         @"Only way to make progress is to know what you did in last workout. Get last workout sets right on workout screen.",
                         @"Add a quick Exercise equipment is busy or don't feel like doing a workout exercise",
                         @"Record seat height, hand angle and additional timer for each exercise for quick exercise setup."];
    
    _premiumPackImg = @[@"IconPIAP1", @"IconWIAP6",  @"IconWIAP1", @"IconWIAP2", @"IconWIAP3", @"IconWIAP4", @"IconWIAP5", @"IconRIAP3", @"IconRIAP2", @"IconRIAP1", @"IconRIAP4", @"IconDIAP1", @"IconDIAP2"];
    _premiumPackHeader = @[@"PRIORITY TECHNICAL SUPPORT", @"WARMUP CALCULATOR", @"PLATE CALCULATOR", @"DROP SET MARKER", @"LAST WORKOUT INFO", @"ADD TEMPORARY EXERCISE", @"RECORD SEAT AND HAND INFO", @"CREATE UNLIMITED ROUTINES", @"EDIT PREMIUM ROUTINES",  @"ANALYZE ROUTINE OVER 2 WEEKS", @"CREATE SUPER-SET", @"UNLIMITED CLOUD SYNC", @"DATA EXPORT VIA EMAIL"];
    _premiumPackDesc = @[@"Exclusive priority support for premium members.",
                         @"Calculate all the warmup sets for each exercise.",
                         @"Math may be fun but NOT during workout. Calculate weight on barbel using super simple plate calculator.",
                         @"Looking for that pump, do a drop set. Marking sets as drop sets is now intuitive.",
                         @"Only way to make progress is to know what you did in last workout. Get last workout sets right on workout screen.",
                         @"Add a quick Exercise equipment is busy or don't feel like doing a workout exercise",
                         @"Record seat height, hand angle and additional timer for each exercise for quick exercise setup.",
                         @"Create unlimited workout routines as you progress in you get strong.",
                         @"Modify downloaded routines to fit your workout style and time requirement.",
                         @"Only way to know if a workout is right for you is to analyze your progress. Buy this pack to unlock analysis for over 2 Weeks.",
                         @"Create Super-set, tri-set, giant-set and circuis.",                         
                         @"FREE for 1 Year. Store your workout data in secure cloud. Never lose it when you change phone or lose it.",
                         @"Export and analyze your data as you like. Export all your workouts and get crazy with charts."
                         ];

    _analyticsImg = @[@"IconRIAP1"];
    
    _analyticsHeader = @[@"ANALYZE ROUTINE OVER 2 WEEKS"];
    _analyticsDesc = @[
                         @"Only way to know if a workout is right for you is to analyze your progress. Buy this pack to unlock analysis for over 2 weeks."
                         ];

    
    NSLog(@"ROUTINE BUMDLE ID %@", _routineBundleId);
    if ([_routineBundleId isEqualToString:@IAP_PREMIUM_PACKAGE_ID])
        routineType = 3;
    if ([_routineBundleId isEqualToString:@IAP_ANALYTICS_PACKAGE_ID])
        routineType = 4;
    else if ([_routineBundleId isEqualToString:@IAP_ROUTINE_PACKAGE_ID])
        routineType = 2;
    else if ([_routineBundleId isEqualToString:@IAP_DATA_PACKAGE_ID])
        routineType = 0;
    else if ([_routineBundleId isEqualToString:@IAP_WORKOUT_PACKAGE_ID])
        routineType = 1;
    
    NSNumberFormatter *_priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    NSString *buyString = [NSString stringWithFormat:@"Buy (%@)", [_priceFormatter stringFromNumber:_product.price]];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:buyString style:UIBarButtonItemStylePlain target:self action:@selector(buyThisProduct)];

    NSLog(@"routine type is %d", routineType);
    self.collectionView.backgroundColor = FlatWhite;
    
}

- (void)buyThisProduct{
    BOOL isEarlyBird = [Utilities isEarlyBird];
    
    if (isEarlyBird == true) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showNotice:self.parentViewController title:@"PREMIUM MEMEBER" subTitle:@"You are already a premium member and have access to all features. We can't take your money again. You don't need to upgrade." closeButtonTitle:@"OK" duration:0.0f];
        return;
    }

    NSLog(@"Buying %@...", _product.productIdentifier);    
    SKPayment * payment = [SKPayment paymentWithProduct:_product];
    [[SKPaymentQueue defaultQueue] addPayment:payment];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark <UICollectionViewDataSource>

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    switch (routineType) {
        case 0:
            return [_dataPackHeader count];
        case 1:
            return [_workoutPackHeader count];
        case 2:            
            return [_powerRoutinePackHeader count];
        case 3:
            return [_premiumPackHeader count];
        case 4:
            return [_analyticsHeader count];
            
        default:
            break;
    }
    
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseIdentifier forIndexPath:indexPath];
    

    for (UIView *view in [cell.contentView subviews]) {
        if ([view isKindOfClass:[UILabel class]] || [view isKindOfClass:[UIImageView class]]) {
            [view removeFromSuperview];
        }
    }
    UILabel *header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(collectionView.frame), 30)];
    
    UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(header.frame) + 5, CGRectGetWidth(collectionView.frame), 150)];
    
    UILabel *description = [[UILabel alloc] initWithFrame:CGRectMake(10, CGRectGetMaxY(image.frame), CGRectGetWidth(collectionView.frame) - 20, 90)];
    description.numberOfLines = 3;
    [description setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
    
    description.textAlignment =    header.textAlignment = NSTextAlignmentCenter;
    image.contentMode = UIViewContentModeScaleAspectFit;
    description.textColor = header.textColor = [UIColor whiteColor];
    [header setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];

    switch (routineType) {
        case 0: {
            header.text = [_dataPackHeader objectAtIndex:indexPath.row];
            [image setImage:[UIImage imageNamed:[_dataPackImg objectAtIndex:indexPath.row]]];
            description.text = [_dataPackDesc objectAtIndex:indexPath.row];
        }
            break;
        case 1: {
            header.text = [_workoutPackHeader objectAtIndex:indexPath.row];
            [image setImage:[UIImage imageNamed:[_workoutPackImg objectAtIndex:indexPath.row]]];
            description.text = [_workoutPackDesc objectAtIndex:indexPath.row];
            
        }
            break;
        case 2: {
            header.text = [_powerRoutinePackHeader objectAtIndex:indexPath.row];
            [image setImage:[UIImage imageNamed:[_powerRoutinePackImg objectAtIndex:indexPath.row]]];
            description.text = [_powerRoutinePackDesc objectAtIndex:indexPath.row];
            
        }
            break;
        case 3: {
            header.text = [_premiumPackHeader objectAtIndex:indexPath.row];
            [image setImage:[UIImage imageNamed:[_premiumPackImg objectAtIndex:indexPath.row]]];
            description.text = [_premiumPackDesc objectAtIndex:indexPath.row];
            
        }
            break;
        case 4: {
            header.text = [_analyticsHeader objectAtIndex:indexPath.row];
            [image setImage:[UIImage imageNamed:[_analyticsImg objectAtIndex:indexPath.row]]];
            description.text = [_analyticsDesc objectAtIndex:indexPath.row];
            
        }
            break;
            
        default:
            break;
    }
    
    if (indexPath.row %2 == 0)
        cell.backgroundColor = FlatOrangeDark;
    else
        cell.backgroundColor = FlatOrange;
    
    cell.layer.cornerRadius = 10;
    [cell.contentView addSubview:header];
    [cell.contentView addSubview:image];
    [cell.contentView addSubview:description];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(CGRectGetWidth(self.view.bounds), 280);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return kItemSpace;
}


#pragma mark <UICollectionViewDelegate>

/*
// Uncomment this method to specify if the specified item should be highlighted during tracking
- (BOOL)collectionView:(UICollectionView *)collectionView shouldHighlightItemAtIndexPath:(NSIndexPath *)indexPath {
	return YES;
}
*/

/*
// Uncomment this method to specify if the specified item should be selected
- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}
*/

/*
// Uncomment these methods to specify if an action menu should be displayed for the specified item, and react to actions performed on the item
- (BOOL)collectionView:(UICollectionView *)collectionView shouldShowMenuForItemAtIndexPath:(NSIndexPath *)indexPath {
	return NO;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	return NO;
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender {
	
}
*/

@end
