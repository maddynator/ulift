//
//  WorkoutUpdaterToCloud.h
//  gyminutes
//
//  Created by Mayank Verma on 2/28/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WorkoutUpdaterToCloud : NSObject

+(void) downloadAllIAPWorkoutsAndUpdateCustomSets:(NSString *) routineBundleId;
@end
