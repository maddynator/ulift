//
//  AnalyzeDetailed.m
//  uLift
//
//  Created by Mayank Verma on 7/23/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AnalyzeDetailed.h"
#import "ExerciseHistoryTVC.h"
#define SETS_BITMAP 1
#define MAX_WT_BITMAP 2
#define MAX_REP_BITMAP 4
#define MAX_WT_MVD_BITMAP 8
#define ORM_BITMAP  16

@interface AnalyzeDetailed () <SHMultipleSelectDelegate, ChartViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    LineChartDataSet *maxWeightDS, *wtMvdDS, *maxRepDS, *estmtORMDS, *setsDS;
    NSString *selectedExercise;
    NSMutableArray *graphElements;
    NSMutableArray *exercises;
    int selectedMap;
    NSDate *selectedDate;
    int oldTimerIndex;
}
@property (nonatomic, strong) IBOutlet LineChartView *chartView;
@end

@implementation AnalyzeDetailed
@synthesize myTableView, timeSegment, muscleOrWorkout;

- (void)viewDidLoad {
    [super viewDidLoad];
    selectedMap = MAX_WT_BITMAP | ORM_BITMAP;
    selectedExercise = @"";
    
    NSLog(@"selected map is  %d", selectedMap);
    // Do any additional setup after loading the view.
    graphElements = [[NSMutableArray alloc] initWithObjects:@"Sets", @"Max Weight", @"Max Reps", @"Weight Moved", @"Estimated One Rep Max", nil];
    
    UIBarButtonItem *filterBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconFilter"] style:UIBarButtonItemStyleDone target:self action:@selector(loadFilterMenu)];

    UIBarButtonItem *shareBtn = [[UIBarButtonItem alloc]  initWithImage:[UIImage imageNamed:@"IconSocial"] style:UIBarButtonItemStylePlain target:self action:@selector(shareit:)];

    self.navigationItem.rightBarButtonItems = @[filterBtn, shareBtn];
    
    NSPredicate *predicte;
    
    if ([_routineName isEqualToString:@"All"])
        if (muscleOrWorkout == 0) {
            predicte = [NSPredicate predicateWithFormat:@"majorMuscle == %@", _muscle];
            self.title = _muscle;
        }
        else {
            predicte = [NSPredicate predicateWithFormat:@"workoutName == %@", _workout];
            self.title = _workout;
        }

    else {
        if (muscleOrWorkout == 0) {
            self.title = _muscle;
            predicte = [NSPredicate predicateWithFormat:@"routineName == %@ AND majorMuscle == %@", _routineName, _muscle];
        }
        else {
            predicte = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@", _routineName, _workout];
            self.title = _workout;
        }
    }
    NSArray *muscleExercises = [_allData filteredArrayUsingPredicate:predicte];
    NSMutableSet * processed = [NSMutableSet set];
    exercises = [[NSMutableArray alloc] init];
    
    for (ExerciseSet *data in muscleExercises) {
        NSString *string = data.exerciseName;
        if ([processed containsObject:string] == NO) {
            [exercises addObject:data];
            [processed addObject:string];

        }
    }
    

    NSArray *itemArray = [NSArray arrayWithObjects: @"2W", @"1M", @"3M", @"6M", @"1Y", @"All", nil];

    _chartView = [[LineChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(self.navigationController.navigationBar.frame) + 5, CGRectGetWidth(self.view.frame) - 10, CGRectGetHeight(self.view.frame)/2)];
    timeSegment = [[UISegmentedControl alloc] initWithItems:itemArray];
    timeSegment.frame = CGRectMake(5, CGRectGetMaxY(_chartView.frame), CGRectGetWidth(self.view.frame) - 10, 30);
    
    myTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timeSegment.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetMaxY(self.navigationController.navigationBar.frame) -CGRectGetHeight(_chartView.frame) - CGRectGetHeight(timeSegment.frame) - TABBAR_HEIGHT) style:UITableViewStyleGrouped];
    
    [self.view addSubview:_chartView];
    [self.view addSubview:timeSegment];
    [self.view addSubview:myTableView];

    self.myTableView.dataSource = self;
    self.myTableView.delegate = self;

    timeSegment.selectedSegmentIndex = 1;
    oldTimerIndex = 1;
    
    [timeSegment addTarget:self action:@selector(timeSegmentClicked:) forControlEvents:UIControlEventValueChanged];
    
   // [self timeSegmentClicked:timeSegment];
    [self.myTableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    [self.myTableView setBackgroundColor:[UIColor whiteColor]];

    [self setupChartView];
    if (_workout == nil) {
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineName", _routineName,
                                   @"Muscle", _muscle,
                                   nil];
        [Flurry logEvent:@"AnalyzeDetailed" withParameters:articleParams];
    } else {
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RoutineName", _routineName,
                                       @"WorkoutName", _workout,
                                       nil];
    [Flurry logEvent:@"AnalyzeDetailed" withParameters:articleParams];
    }


}

-(void) viewDidDisappear:(BOOL)animated {
    [Flurry endTimedEvent:@"AnalyzeDetailed" withParameters:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(IBAction)shareit:(id)sender {
    
        CGRect cropRect = CGRectMake(CGRectGetMinX(self.chartView.frame), CGRectGetMinX(self.chartView.frame), CGRectGetWidth(self.view.frame), CGRectGetHeight(self.chartView.frame) + 60);
        UIImageView *viewImage = [self rp_screenshotImageViewWithCroppingRect:cropRect];
        
        //    UIImageWriteToSavedPhotosAlbum(viewImage.image, nil, nil, nil);
        NSString *text = [NSString stringWithFormat:@"Workout Analysis. To track and analyze your workouts, get GYMINUTES today."];
        NSURL *url = [NSURL URLWithString:@"http://www.gyminutesapp.com"];
        
        
        UIActivityViewController *controller =
        [[UIActivityViewController alloc]
         initWithActivityItems:@[text, url, viewImage.image]
         applicationActivities:nil];
        
        [self presentViewController:controller animated:YES completion:^{
            ExerciseSet *exShr = [exercises objectAtIndex:[self.myTableView indexPathForSelectedRow].row];
            if (_workout == nil) {
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               @"RoutineName", _routineName,
                                               @"Muscle", _muscle,
                                               @"Exercise", exShr.exerciseName,
                                               nil];
                [Flurry logEvent:@"AnalyzeDetailedShr" withParameters:articleParams];
            } else {
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               @"RoutineName", _routineName,
                                               @"WorkoutName", _workout,
                                               @"Exercise", exShr.exerciseName,
                                               nil];
                [Flurry logEvent:@"AnalyzeDetailedShr" withParameters:articleParams];
            }
        }];
        
  
}

-(void) setupChartView {
    _chartView.delegate = self;
    
    _chartView.descriptionText = @"";
    _chartView.noDataText = @"No exercise recoded yet.";
    
//    _chartView.highlighted = YES;
    _chartView.dragEnabled = YES;
    [_chartView setScaleEnabled:YES];
    _chartView.drawGridBackgroundEnabled = NO;
    _chartView.pinchZoomEnabled = YES;
    
    _chartView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithWhite:204/255.f alpha:1.f];
    
    _chartView.legend.form = ChartLegendFormLine;
    _chartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _chartView.legend.textColor = UIColor.blackColor;
    _chartView.legend.position = ChartLegendPositionBelowChartLeft;
    
    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelFont = [UIFont systemFontOfSize:12.f];
    xAxis.labelTextColor = UIColor.blackColor;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.spaceMin = 1.0;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelTextColor = FlatBlackDark;//[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
//    leftAxis.customAxisMax = 200.0;
    leftAxis.drawGridLinesEnabled = NO;

    ChartYAxis *rightAxis = _chartView.rightAxis;
    rightAxis.labelTextColor = UIColor.redColor;
    rightAxis.drawZeroLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    
    [_chartView animateWithXAxisDuration:2.5];
    
}

-(void) viewDidAppear:(BOOL)animated {
    
    if ([exercises count] != 0) {
        NSIndexPath *indexPath=[NSIndexPath indexPathForRow:0 inSection:0];
        [myTableView selectRowAtIndexPath:indexPath animated:YES  scrollPosition:UITableViewScrollPositionTop];
        [self tableView:myTableView didSelectRowAtIndexPath:indexPath];
    }

}
-(void) loadFilterMenu {
    SHMultipleSelect *multipleSelect = [[SHMultipleSelect alloc] init];
    multipleSelect.delegate = self;
    multipleSelect.tintColor = FlatOrange;
    multipleSelect.rowsCount = graphElements.count;
    [multipleSelect show];
}

- (void)setDataCount: (NSArray *) filteredData exName: (NSString *) exerciseName
{
 
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
    
    NSMutableSet * processed = [NSMutableSet set];
    NSMutableArray *unique = [[NSMutableArray alloc] init];
    
    // getting x axis
    for (ExerciseSet *data in filteredData) {
        NSString *string = [NSString stringWithFormat:@"%@,%@", data.exerciseName, data.date];
        if ([processed containsObject:string] == NO) {
            [unique addObject:data];
            [processed addObject:string];
        }
    }
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    for (int i = 0; i < [unique count]; i++)
    {
        ExerciseSet *set = [unique objectAtIndex:i];
        [xVals addObject:set.date];
    }

    // setting max rep
    NSArray *maxRepArr = [ExerciseSet MR_findAllSortedBy:@"date,rep" ascending:NO withPredicate:predicate inContext:localContext];
    
    NSMutableSet * processedMaxRep = [NSMutableSet set];
    NSMutableArray *uniqueMaxRep = [[NSMutableArray alloc] init];
    
    for (ExerciseSet *data in maxRepArr) {
        NSString *string = [NSString stringWithFormat:@"%@,%@", data.exerciseName, data.date];
        NSDate *date = [formatter dateFromString:data.date];
        if ([processedMaxRep containsObject:string] == NO && [date isLaterThanOrEqualTo:selectedDate]) {
            [uniqueMaxRep addObject:data];
            [processedMaxRep addObject:string];
        }
    }
    //rever the array
    NSArray* reversedArrayRep = [[uniqueMaxRep reverseObjectEnumerator] allObjects];
    NSMutableArray *yValsRep = [[NSMutableArray alloc] init];
    for (int i = 0; i < [reversedArrayRep count]; i++)
    {
        ExerciseSet *set = [reversedArrayRep objectAtIndex:i];
//        NSDate *date = [formatter dateFromString:set.date];
//        NSLog(@"==> Max Rep == date and rep, %@, %@ %@", set.date, set.rep, date);
        [yValsRep addObject:[[ChartDataEntry alloc] initWithX:i y:[set.rep intValue]]];
    }
    
    maxRepDS = [[LineChartDataSet alloc] initWithValues:yValsRep label:@"MaxRep"];
    maxRepDS.axisDependency = AxisDependencyLeft;
    [maxRepDS setColor:FlatMagentaDark];
    [maxRepDS setCircleColor:UIColor.blackColor];
    maxRepDS.lineWidth = 2.0;
    maxRepDS.circleRadius = 3.0;
    maxRepDS.fillAlpha = 65/255.0;
    maxRepDS.fillColor = [UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    maxRepDS.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    maxRepDS.drawCircleHoleEnabled = NO;
 
    
    // setting max weight
    NSArray *maxWeightArr = [ExerciseSet MR_findAllSortedBy:@"date,weight" ascending:NO withPredicate:predicate inContext:localContext];
    
    NSMutableSet * processedMaxWeight = [NSMutableSet set];
    NSMutableArray *uniqueMaxWeight = [[NSMutableArray alloc] init];
    for (ExerciseSet *data in maxWeightArr) {
        NSString *string = [NSString stringWithFormat:@"%@,%@", data.exerciseName, data.date];
        NSDate *date = [formatter dateFromString:data.date];
        if ([processedMaxWeight containsObject:string] == NO && [date isLaterThanOrEqualTo:selectedDate]) {
            [uniqueMaxWeight addObject:data];
            [processedMaxWeight addObject:string];
        }
    }
    
    NSArray* reversedArrayWeight = [[uniqueMaxWeight reverseObjectEnumerator] allObjects];
    NSMutableArray *yValsWeight = [[NSMutableArray alloc] init];
    for (int i = 0; i < [reversedArrayWeight count]; i++)
    {
        ExerciseSet *set = [reversedArrayWeight objectAtIndex:i];
//        NSLog(@"==> Max Wt date %.1f", [set.weight floatValue]);
        [yValsWeight addObject:[[ChartDataEntry alloc] initWithX:i y:[set.weight floatValue]]];
    }
    
    maxWeightDS = [[LineChartDataSet alloc] initWithValues:yValsWeight label:@"MaxWeight"];
    maxWeightDS.axisDependency = AxisDependencyLeft;
    [maxWeightDS setColor:FlatOrangeDark];
    [maxWeightDS setCircleColor:UIColor.blackColor];
    maxWeightDS.lineWidth = 2.0;
    maxWeightDS.circleRadius = 3.0;
    maxWeightDS.fillAlpha = 65/255.0;
    maxWeightDS.fillColor = [UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    maxWeightDS.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    maxWeightDS.drawCircleHoleEnabled = NO;
    
    
    // setting max ORM
    NSMutableArray *yValsORM = [[NSMutableArray alloc] init];
    for (int i = 0; i < [reversedArrayWeight count]; i++)
    {
        ExerciseSet *set = [reversedArrayWeight objectAtIndex:i];
//        NSDate *date = [formatter dateFromString:set.date];
        float value = [set.weight floatValue] * (36/(37 - [set.rep floatValue]));
        [yValsORM addObject:[[ChartDataEntry alloc] initWithX:i y:value]];
//        NSLog(@"==> ORM max weithgt %@ %f", date,  value);

    }
    
    estmtORMDS = [[LineChartDataSet alloc] initWithValues:yValsORM label:@"ORM"];
    estmtORMDS.axisDependency = AxisDependencyLeft;
    [estmtORMDS setColor:FlatBlue];
    [estmtORMDS setCircleColor:UIColor.blackColor];
    estmtORMDS.lineWidth = 2.0;
    estmtORMDS.circleRadius = 3.0;
    estmtORMDS.fillAlpha = 65/255.0;
    estmtORMDS.fillColor = [UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    estmtORMDS.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    estmtORMDS.drawCircleHoleEnabled = NO;
    
    // setting sets
    NSMutableSet * processedSets = [NSMutableSet set];
    NSMutableArray *uniqueSets = [[NSMutableArray alloc] init];
    for (ExerciseSet *data in maxWeightArr) {
        NSString *string = data.date;
        NSDate *date = [formatter dateFromString:data.date];
        if ([processedSets containsObject:string] == NO && [date isLaterThanOrEqualTo:selectedDate]) {
            [uniqueSets addObject:string];
            [processedSets addObject:string];
        }
    }
    
    NSArray* reversedArraySets = [[uniqueSets reverseObjectEnumerator] allObjects];
    NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
    NSMutableArray *yValsWtMvd = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < [reversedArraySets count]; i++)
    {
        ExerciseSet *set = [reversedArrayWeight objectAtIndex:i];
        NSPredicate *predicateDate = [NSPredicate predicateWithFormat:@"date == %@", set.date];
//        NSLog(@"==> sets date %@", date);
        // using max weight arr as we dont need to pull it again..
        NSArray *setArray = [maxWeightArr filteredArrayUsingPredicate:predicateDate];
        
        int sets = (int) [setArray count];
        float totalWt = 0;
        for (int j = 0; j < [setArray count]; j++) {
            ExerciseSet *setsByDay = [setArray objectAtIndex:j];
            totalWt += [setsByDay.rep intValue] * [setsByDay.weight floatValue];
            
        }
        [yValsSets addObject:[[ChartDataEntry alloc] initWithX:i y:sets]];
        [yValsWtMvd addObject:[[ChartDataEntry alloc] initWithX:i y:totalWt]];
    }
    
    setsDS = [[LineChartDataSet alloc] initWithValues:yValsSets label:@"Sets"];
    setsDS.axisDependency = AxisDependencyLeft;
    [setsDS setColor:FlatLimeDark];
    [setsDS setCircleColor:UIColor.blackColor];
    setsDS.lineWidth = 2.0;
    setsDS.circleRadius = 3.0;
    setsDS.fillAlpha = 65/255.0;
    setsDS.fillColor = [UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    setsDS.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    setsDS.drawCircleHoleEnabled = NO;

    
    
    wtMvdDS = [[LineChartDataSet alloc] initWithValues:yValsWtMvd label:@"Volume"];
    wtMvdDS.axisDependency = AxisDependencyLeft;
    [wtMvdDS setColor:FlatRedDark];
    [wtMvdDS setCircleColor:UIColor.blackColor];
    wtMvdDS.lineWidth = 2.0;
    wtMvdDS.circleRadius = 3.0;
    wtMvdDS.fillAlpha = 65/255.0;
    wtMvdDS.fillColor = [UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    wtMvdDS.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    wtMvdDS.drawCircleHoleEnabled = NO;
    
    if (selectedMap & MAX_REP_BITMAP)
        [dataSets addObject:maxRepDS];
    
    if (selectedMap & MAX_WT_BITMAP)
        [dataSets addObject:maxWeightDS];
    
    if (selectedMap & SETS_BITMAP)
        [dataSets addObject:setsDS];
    
    if (selectedMap & ORM_BITMAP)
        [dataSets addObject:estmtORMDS];
    
    if (selectedMap & MAX_WT_MVD_BITMAP)
        [dataSets addObject:wtMvdDS];

    LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    [data setValueTextColor:UIColor.blackColor];
    [data setValueFont:[UIFont systemFontOfSize:9.f]];
    
    _chartView.data = data;
}


#pragma mark - ChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    NSLog(@"chartValueSelected");
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}


#pragma mark - SHMultipleSelectDelegate

- (void)multipleSelectView:(SHMultipleSelect *)multipleSelectView clickedBtnAtIndex:(NSInteger)clickedBtnIndex withSelectedIndexPaths:(NSArray *)selectedIndexPaths {
    if (clickedBtnIndex == 1) { // Done btn
        selectedMap  = 0;
        for (NSIndexPath *indexPath in selectedIndexPaths) {
            NSLog(@"selected index is %ld", (long)indexPath.row);
            switch (indexPath.row) {
                case 0:
                    selectedMap |= SETS_BITMAP;
                    break;
                case 1:
                    selectedMap |= MAX_WT_BITMAP;
                    break;
                case 2:
                    selectedMap |= MAX_REP_BITMAP;
                    break;
                case 3:
                    selectedMap |= MAX_WT_MVD_BITMAP;
                    break;
                case 4:
                    selectedMap |= ORM_BITMAP;
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    NSLog(@"selected map %d", selectedMap);
    [self timeSegmentClicked:timeSegment];
}

- (NSString *)multipleSelectView:(SHMultipleSelect *)multipleSelectView titleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return graphElements[indexPath.row];
}

- (BOOL)multipleSelectView:(SHMultipleSelect *)multipleSelectView setSelectedForRowAtIndexPath:(NSIndexPath *)indexPath {
    BOOL canSelect = NO;

    if (selectedMap & SETS_BITMAP && indexPath.row == 0)
        canSelect = YES;
    
    if (selectedMap & MAX_WT_BITMAP && indexPath.row == 1)
        canSelect = YES;

    if (selectedMap & MAX_REP_BITMAP && indexPath.row == 2)
        canSelect = YES;
    
    if (selectedMap & MAX_WT_MVD_BITMAP && indexPath.row == 3)
        canSelect = YES;
    
    if (selectedMap & ORM_BITMAP && indexPath.row == 4)
        canSelect = YES;
    
//
//    if (indexPath.row == 1)
//        canSelect = YES;
//    
//    if (indexPath.row == graphElements.count - 1) { // last object
//        canSelect = YES;
//    }
    return canSelect;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [exercises count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [self.myTableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
//    UILabel *exName = (UILabel *)[cell viewWithTag:100];
    ExerciseSet *ex = [exercises objectAtIndex:indexPath.row];
    NSLog(@"exercise name %@", ex.exerciseName);
 //   exName.text = ex.exerciseName;
    cell.textLabel.text = ex.exerciseName;
    cell.accessoryType = UITableViewCellAccessoryDetailButton;
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 25;
}
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseSet *selected = [exercises objectAtIndex:indexPath.row];
    selectedExercise = selected.exerciseName;
    [self timeSegmentClicked:timeSegment];
}

-(void) tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    ExerciseSet *selected = [exercises objectAtIndex:indexPath.row];
    selectedExercise = selected.exerciseName;
    [self performSegueWithIdentifier:@"exerciseHistorySegue" sender:self];
}

-(void)parentSegmentClicked:(UISegmentedControl *)segmentedControl {
//    [self setDataCount:filteredArrayByDate exName:selectedExercise];
}

-(void)timeSegmentClicked:(UISegmentedControl *)segmentedControl {
    bool showAnalysis = false;
    
    if (segmentedControl.selectedSegmentIndex > 1) {
        if ([Utilities showAnalyticsPackage]) {
            [self showPurchasePopUp:@"This is a paid feature."];
            timeSegment.selectedSegmentIndex = oldTimerIndex;
        } else
            showAnalysis = true;
    } else
        showAnalysis = true;
    
    if (showAnalysis == true) {
        NSPredicate *predicte = [NSPredicate predicateWithFormat:@"exerciseName == %@", selectedExercise];
        
        NSArray *filtered = [_allData filteredArrayUsingPredicate:predicte];
        NSArray* reversedArray = [[filtered reverseObjectEnumerator] allObjects];
        
        NSString *dateStringForHeaders = @"";
        NSMutableArray *filteredArrayByDate = [[NSMutableArray alloc] init];
        NSLog(@"segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *today = [NSDate date];
        
        switch (segmentedControl.selectedSegmentIndex) {
            case 0:
                selectedDate = [today dateBySubtractingWeeks:2];
                dateStringForHeaders = @"last 2 week.";
                break;
            case 1:
                selectedDate = [today dateBySubtractingMonths:1];
                dateStringForHeaders = @"last 1 month.";
                break;
            case 2:
                selectedDate = [today dateBySubtractingMonths:3];
                dateStringForHeaders = @"last 3 months.";
                break;
            case 3:
                selectedDate = [today dateBySubtractingMonths:6];
                dateStringForHeaders = @"last 6 months.";
                break;
            case 4:
                selectedDate = [today dateBySubtractingYears:1];
                dateStringForHeaders = @"last 1 year.";
                break;
            case 5:
                selectedDate = [today dateBySubtractingYears:5];
                dateStringForHeaders = @"last 5 year.";
                break;
            default:
                selectedDate = [today dateBySubtractingWeeks:2];
                break;
        }
        
        for (ExerciseSet *ex in reversedArray) {
            NSDate *date = [formatter dateFromString:ex.date];
            if ([date isLaterThanOrEqualTo:selectedDate]) {
                [filteredArrayByDate addObject:ex];
            } else {
            }
            
        }
        // NSLog(@"date string is %@, %@, %@", selectedDate, dateStringForHeaders, today);
        
        [self setDataCount:filteredArrayByDate exName:selectedExercise];
    }
}

- (UIImageView *)rp_screenshotImageViewWithCroppingRect:(CGRect)croppingRect {
    // For dealing with Retina displays as well as non-Retina, we need to check
    // the scale factor, if it is available. Note that we use the size of teh cropping Rect
    // passed in, and not the size of the view we are taking a screenshot of.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(croppingRect.size, YES, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(croppingRect.size);
    }
    
    // Create a graphics context and translate it the view we want to crop so
    // that even in grabbing (0,0), that origin point now represents the actual
    // cropping origin desired:
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -croppingRect.origin.x, -croppingRect.origin.y);
    [self.view.layer renderInContext:ctx];
    
    // Retrieve a UIImage from the current image context:
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the image in a UIImageView:
    return [[UIImageView alloc] initWithImage:snapshotImage];
}



#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"exerciseHistorySegue"]) {
        ExerciseHistoryTVC *destVC = segue.destinationViewController;
        destVC.exerciseObj = selectedExercise;
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ANALYTICS PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ANALYTICS_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium or Analytics Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

@end
