//
//  TodayWorkoutItems.h
//  gyminutes
//
//  Created by Mayank Verma on 5/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "commons.h"

@interface TodayWorkoutItems : NSObject

+(NSMutableDictionary *) getDailyStats: (NSString *) date;
+(NSMutableDictionary *) getStartWorkout: (NSArray *) data;
+(NSMutableDictionary *) getPerExerciseInformation:(WorkoutList *) data workoutList:(NSArray *) workout;
+(NSMutableDictionary *) getWorkoutSummary: (NSArray *) data;
+(NSMutableDictionary *) getPersonalRecords: (NSString *) date;
+(NSMutableDictionary *) getWorkoutTimeLine:(NSArray *) data;
+(NSMutableDictionary *) getWorkoutStats:(NSArray *) data;
+(NSMutableDictionary *) getMuscleStats:(NSArray *) data;
+(NSMutableDictionary *) getExerciseStats:(NSArray *)data;
+(NSMutableDictionary *) rateUsMenu;
+(NSMutableDictionary *) featureRequest;
+(NSMutableDictionary *) setExerciseGoals:(NSArray *) data;
+(NSMutableDictionary *) setNotificationForWorkout;

@end
