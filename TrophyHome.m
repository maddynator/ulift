//
//  TrophyHome.m
//  gyminutes
//
//  Created by Mayank Verma on 7/7/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "TrophyHome.h"
#import "MilestonesVC.h"
#import "PersonalRecordVC.h"

@implementation TrophyHome
-(void) viewDidLoad {
    [super viewDidLoad];
    self.title = @"Trophy";
    items = @[@"Milestones", @"Personal Records"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    NSLog(@"trophy home loaded");
    [self style];
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];

    
}

-(void) viewDidAppear:(BOOL)animated {
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];
    [self style];
    [carbonTabSwipeNavigation setIndicatorColor:[Utilities getAppColor]];

}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];//[UIColor colorWithRed:24.0/255 green:75.0/255 blue:152.0/255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    int width = CGRectGetWidth(self.view.frame)/[items count];
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [Utilities getAppColor];//[UIColor whiteColor];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:1];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:2];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:3];
//    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:4];
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont boldSystemFontOfSize:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0: {
            MilestonesVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MilestonesVC"];
            return destVC;
        }
        case 1: {
            PersonalRecordVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalRecordVC"];
            return destVC;
        }
//
//        case 2: {
//            WorkoutAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"workoutAnalysis"];
//            destVC.routineName = routineName;
//            return destVC;
//            
//        }
//        case 3: {
//            MuscleAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"muscleAnalysis"];
//            destVC.routineName = routineName;
//            return destVC;
//            
//        }
//        case 4: {
//            ExerciseAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"exerciseAnalysis"];
//            destVC.routineName = routineName;
//            return destVC;
//            
//        }
            
        default:
            //            return [self.storyboard instantiateViewControllerWithIdentifier:@"ViewControllerThree"];
            return nil;
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    //    switch(index) {
    //        case 0:
    //            self.title = @"Original";
    //            break;
    //        case 1:
    //            self.title = @"Alternate";
    //            break;
    //        case 2:
    //            self.title = @"Advanced";
    //            break;
    //        case 3:
    //            self.title = @"Core/Lower Body";
    //            break;
    //        default:
    //            self.title = items[index];
    //            break;
    //    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

@end
