//
//  RecordCardio.h
//  gyminutes
//
//  Created by Mayank Verma on 6/22/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
@interface RecordCardio : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, UIPickerViewDataSource, UIPickerViewDelegate, UITextViewDelegate>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (retain, nonatomic) IBOutlet UIPickerView *picker;
@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, retain) NSString *date;
@end
