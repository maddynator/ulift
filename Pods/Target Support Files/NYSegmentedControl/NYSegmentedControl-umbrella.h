#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "NYSegment.h"
#import "NYSegmentedControl.h"
#import "NYSegmentIndicator.h"
#import "NYSegmentLabel.h"

FOUNDATION_EXPORT double NYSegmentedControlVersionNumber;
FOUNDATION_EXPORT const unsigned char NYSegmentedControlVersionString[];

