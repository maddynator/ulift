#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "TACloseButton.h"
#import "TAITunesClient.h"
#import "TAPromotee.h"
#import "TAPromoteeApp.h"
#import "TAPromoteeViewController.h"

FOUNDATION_EXPORT double TAPromoteeVersionNumber;
FOUNDATION_EXPORT const unsigned char TAPromoteeVersionString[];

