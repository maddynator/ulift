#ifdef __OBJC__
#import <UIKit/UIKit.h>
#endif

#import "SHMultipleSelect.h"
#import "UIImage+SHAdditions.h"
#import "UIView+SHAdditions.h"

FOUNDATION_EXPORT double SHMultipleSelectVersionNumber;
FOUNDATION_EXPORT const unsigned char SHMultipleSelectVersionString[];

