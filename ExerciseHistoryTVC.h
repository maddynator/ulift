//
//  ExerciseHistoryTVC.h
//  uLift
//
//  Created by Mayank Verma on 7/6/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface ExerciseHistoryTVC : UITableViewController <DZNEmptyDataSetDelegate, DZNEmptyDataSetSource>
@property (nonatomic, retain) IBOutlet NSString *exerciseObj;

@end
