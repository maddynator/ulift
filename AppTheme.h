//
//  AppTheme.h
//  gyminutes
//
//  Created by Mayank Verma on 5/24/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppTheme : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, retain) UITableView *tableView;
@property (nonatomic, retain) UICollectionView *collectionView;
@end
