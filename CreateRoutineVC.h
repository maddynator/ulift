//
//  CreateRoutineVC.h
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "FXForms.h"
#import "commons.h"

@interface CreateRoutineVC : FXFormViewController <FXFormControllerDelegate>

@property (nonatomic, retain) RoutineMetaInfo *routine;
@end
