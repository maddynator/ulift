//
//  MuscleList+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "MuscleList+CoreDataProperties.h"

@implementation MuscleList (CoreDataProperties)

+ (NSFetchRequest<MuscleList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"MuscleList"];
}

@dynamic majorMuscle;
@dynamic muscleName;

@end
