//
//  ExerciseList+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseList+CoreDataProperties.h"

@implementation ExerciseList (CoreDataProperties)

+ (NSFetchRequest<ExerciseList *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExerciseList"];
}

@dynamic equipment;
@dynamic exerciseName;
@dynamic exercisePerformed;
@dynamic expLevel;
@dynamic isUserCreated;
@dynamic majorMuscle;
@dynamic mechanics;
@dynamic muscle;
@dynamic syncedState;
@dynamic type;

@end
