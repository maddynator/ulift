//
//  DailyStatsVC.h
//  gyminutes
//
//  Created by Mayank Verma on 6/27/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "CLWeeklyCalendarViewSourceCode/CLWeeklyCalendarView.h"

@interface DailyStatsVC : UIViewController <UITableViewDelegate, UITableViewDataSource, ChartViewDelegate, CLWeeklyCalendarViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CLWeeklyCalendarView* calendarView;
@property (nonatomic, strong) IBOutlet LineChartView *lineChartView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSegment;

@end
