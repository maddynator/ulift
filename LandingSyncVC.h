//
//  LandingSyncVC.h
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface LandingSyncVC : UIViewController <PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>
@property (nonatomic, retain) IBOutlet UIButton *logout;
-(IBAction)logout:(id)sender;

@end
