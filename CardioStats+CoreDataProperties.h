//
//  CardioStats+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "CardioStats+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface CardioStats (CoreDataProperties)

+ (NSFetchRequest<CardioStats *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *caloriesBurned;
@property (nullable, nonatomic, copy) NSString *cardioEquipment;
@property (nullable, nonatomic, copy) NSString *cardioType;
@property (nullable, nonatomic, copy) NSDate *date;
@property (nullable, nonatomic, copy) NSNumber *distance;
@property (nullable, nonatomic, copy) NSNumber *duration;
@property (nullable, nonatomic, copy) NSNumber *elevation;
@property (nullable, nonatomic, copy) NSNumber *emptyStomach;
@property (nullable, nonatomic, copy) NSNumber *pace;
@property (nullable, nonatomic, copy) NSDate *startTime;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSString *trainingType;

@end

NS_ASSUME_NONNULL_END
