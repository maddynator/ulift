//
//  MuscleListTVC.m
//  uLift
//
//  Created by Mayank Verma on 6/28/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "MuscleListTVC.h"
#import "ExerciseListTable.h"
#import "ExerciseSelectForWorkout.h"

NSMutableArray *muscleArray;
int indexSelected;

@interface MuscleListTVC () {
    NSArray *allExercises;
    NSArray *searchResults;
    NSString *searchString;
//    NSArray * displayedItems;
    JVFloatLabeledTextView *setsTextField, *repsTextField, *restTextField;
    int setCount;
    
    NSMutableArray *repArray,*amarpArray, *timerArray;
    UILabel *setsValue;


}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation MuscleListTVC
//@synthesize searchBar;
@synthesize customRepsPerSet;
- (void)viewDidLoad {
    [super viewDidLoad];
    
    indexSelected = -1;
    //muscleArray = [[NSArray alloc] initWithObjects: @"Abs1", @"Back", @"Biceps", @"Chest", @"Calfs", @"Legs", @"Shoulders", @"Triceps", nil];
    
    self.tableView.dataSource= self;
    self.tableView.delegate =self;
    self.navigationController.navigationBarHidden = NO;
    self.title = @"Muscle";
    self.tableView.scrollEnabled = false;
    allExercises = [Utilities getAllExercises];
    
    muscleArray = [[NSMutableArray alloc] init];
    [self loadMuscleGroup];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];    
    [self.tableView setBackgroundColor:[UIColor whiteColor]];
    
    // Here's where we create our UISearchController
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    
    [self.searchController.searchBar sizeToFit];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;
    
//    self.edgesForExtendedLayout = UIRectEdgeAll;
    self.extendedLayoutIncludesOpaqueBars = YES;

    self.tableView.tableHeaderView = self.searchController.searchBar;
    NSLog(@"view loaded again *****");
    NSLog(@"navigation bar state %d %d", self.navigationController.navigationBar.isHidden, self.searchController.isActive);
    [self.navigationController.navigationBar setTranslucent:YES];

}
-(void)dealloc {    
    // this needs to be done so we dont have a handing search controller...
    [self.searchController.view removeFromSuperview]; // It works!
}

-(void) viewDidAppear:(BOOL)animated {
    if (_isTempEx)
        _exerciseGroup = [NSNumber numberWithInt:[Utilities getNumberOfGroupsInCurrentWorkoutForTempExercise:_date]];
    else
        _exerciseGroup = [NSNumber numberWithInt:[Utilities getNumberOfGroupsInWorkoutRouting:_routineName workoutName:_workoutName]];
    

//    NSLog(@"navigation bar state %d %d", self.navigationController.navigationBar.isHidden, self.searchController.isActive);
//    if (self.searchController.active == false)
//        self.navigationController.navigationBar.hidden = false;
//    
}

-(void)viewWillDisappear:(BOOL)animated {
    //[self.searchController removeFromParentViewController];
//    self.searchController.hidesNavigationBarDuringPresentation = YES;
    [self.searchController setActive:false];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) loadMuscleGroup {
    //muscleName
    NSArray *temp = [MuscleList MR_findAllSortedBy:@"majorMuscle" ascending:YES];
    		
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (MuscleList *data in temp) {
        NSString *string = data.majorMuscle;
//        NSLog(@"major muscle is %@, %@", data.majorMuscle, data.muscleName);
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
            [muscleArray addObject:data];
        }
    }
//    muscleArray = [[NSArray alloc] initWithArray:unique];
//    NSLog(@"muscl count is %lu", (long) [unique count]);
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tableView) {
        NSLog(@"count %ld", (unsigned long)[muscleArray count]);
        if (self.searchController.active == false) {
            return [muscleArray count];
        } else {
            return [searchResults count];
        }
    } else {
        return setCount;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if ( cell == nil ) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]] && lbl.tag == 501) {
            [lbl removeFromSuperview];
        }
    }
    //if (tableView == self.tableView) {
    if (self.searchController.active == false) {
        MuscleList *data = [muscleArray objectAtIndex:indexPath.row];
        
        UILabel *name = (UILabel *)[cell viewWithTag:100];
        [name setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24.0f]];
        name.text = data.majorMuscle;// data.muscleName;
        
        cell.backgroundColor = [Utilities getMajorMuscleColor:data.majorMuscle];//[Utilities getExerciseColor:data.majorMuscle];
        //UIView *bgColorView = [[UIView alloc] init];
        //bgColorView.backgroundColor = [Utilities getMajorMuscleColorSelected:data.majorMuscle];
        //[cell setSelectedBackgroundView:bgColorView];
        cell.backgroundColor = [Utilities getMajorMuscleColorSelected:data.majorMuscle];
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, (CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.searchController.searchBar.frame))/[muscleArray count] - 1.0, self.tableView.frame.size.width, 1)];
        
        lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrangeDark]];
        lineView.tag = 501;
        [cell.contentView addSubview:lineView];
        
    } else {
        ExerciseList *data = [searchResults objectAtIndex:indexPath.row];
        cell.textLabel.text = [NSString stringWithFormat:@"%@ (%@)", data.exerciseName, data.muscle];
        cell.backgroundColor = [Utilities getExerciseColor:data.muscle];
        cell.textLabel.textColor = [UIColor whiteColor];
        [cell.textLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14.0f]];

    }

    return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepsCell"];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int itemHeight = 35;
        float popupWidth = CGRectGetWidth(cell.frame);
        UILabel *setNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 75, 30)];
        setNum.text = [NSString stringWithFormat:@"%d", (int)indexPath.row + 1];
        setNum.textAlignment = NSTextAlignmentCenter;
        //setNum.backgroundColor = FlatRed;
        
        UIButton *repMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), 5, 30, itemHeight)];
        [repMinus setTitle:@"-" forState:UIControlStateNormal];
        repMinus.backgroundColor = FlatMintDark;
        repMinus.tag = 1000 + indexPath.row;
        [repMinus addTarget:self action:@selector(repTableDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *repTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinus.frame), 5, popupWidth/4, itemHeight)];
        
        repTextFied.placeholder = @"Reps";
        repTextFied.textAlignment = NSTextAlignmentCenter;
        repTextFied.editable = false;
        repTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        repTextFied.tag = 2000 + indexPath.row;
        repTextFied.textContainer.maximumNumberOfLines = 1;
        repTextFied.scrollEnabled = false;
        //repTextFied.floatingLabelYPadding = 0;
        repTextFied.placeholderYPadding = -10;
        repTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        
        UIButton *repPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repTextFied.frame), 5, 30, itemHeight)];
        [repPlus setTitle:@"+" forState:UIControlStateNormal];
        repPlus.backgroundColor = FlatMintDark;
        repPlus.tag = 3000 + indexPath.row;
        [repPlus addTarget:self action:@selector(repTableInc:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == true) {
            repMinus.enabled = false;
            repPlus.enabled = false;
            repTextFied.backgroundColor = FlatGrayDark;
            repTextFied.text = @"MAX";
        } else {
            repMinus.enabled = true;
            repPlus.enabled = true;
            repTextFied.backgroundColor = [UIColor whiteColor];
            repTextFied.text = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:indexPath.row]];
        }
        
        UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];
        
        WPmaskLayer.frame = self.view.bounds;
        WPmaskLayer.path  = WPmaskPath.CGPath;
        WMmaskLayer.frame = self.view.bounds;
        WMmaskLayer.path  = WMmaskPath.CGPath;
        repPlus.layer.mask = WPmaskLayer;
        repMinus.layer.mask = WMmaskLayer;
        
        UISwitch *amarpSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.frame) - 75, 5, 75, 30)];
        amarpSwitch.tag = 4000 + indexPath.row;
        //amarpSwitch.tintColor = FlatRed;
        amarpSwitch.onTintColor = FlatGreen;
        [amarpSwitch setOn:[[amarpArray objectAtIndex:indexPath.row] boolValue]];
        [amarpSwitch addTarget:self action:@selector(amarpIsChanged:) forControlEvents:UIControlEventValueChanged];
        
        
        UIButton *timerMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerMinus setTitle:@"-" forState:UIControlStateNormal];
        timerMinus.backgroundColor = FlatMintDark;
        timerMinus.tag = 5000 + indexPath.row;
        [timerMinus addTarget:self action:@selector(timerTableDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *timerTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerMinus.frame), CGRectGetMaxY(repMinus.frame) + 5, popupWidth/4, itemHeight)];
        
        timerTextFied.placeholder = @"Rest";
        timerTextFied.textAlignment = NSTextAlignmentCenter;
        timerTextFied.editable = false;
        timerTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        timerTextFied.tag = 6000 + indexPath.row;
        timerTextFied.textContainer.maximumNumberOfLines = 1;
        timerTextFied.scrollEnabled = false;
        timerTextFied.placeholderYPadding = -10;
        timerTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        timerTextFied.text = [NSString stringWithFormat:@"%d", [[timerArray objectAtIndex:indexPath.row] intValue]];
        
        UIButton *timerPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerTextFied.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerPlus setTitle:@"+" forState:UIControlStateNormal];
        timerPlus.backgroundColor = FlatMintDark;
        timerPlus.tag = 7000 + indexPath.row;
        [timerPlus addTarget:self action:@selector(timerTableInc:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *TPmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *TMmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *TPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *TMmaskLayer = [[CAShapeLayer alloc] init];
        
        TPmaskLayer.frame = self.view.bounds;
        TPmaskLayer.path  = TPmaskPath.CGPath;
        TMmaskLayer.frame = self.view.bounds;
        TMmaskLayer.path  = TMmaskPath.CGPath;
        
        
        timerPlus.layer.mask = TPmaskLayer;
        timerMinus.layer.mask = TMmaskLayer;
        
        
        [cell.contentView addSubview:setNum];
        [cell.contentView addSubview:repMinus];
        [cell.contentView addSubview:repTextFied];
        [cell.contentView addSubview:repPlus];
        [cell.contentView addSubview:amarpSwitch];
        [cell.contentView addSubview:timerMinus];
        [cell.contentView addSubview:timerTextFied];
        [cell.contentView addSubview:timerPlus];
        
        return cell;
    }
    return nil;
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        [self.tableView setRowHeight:60.0];
        [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
        [self.tableView setSeparatorColor:[UIColor colorWithWhite:0.7 alpha:1]];
    }
    return self;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
//{
//    //TODO: remove this when adding search bar...
//    return 0.1f;
//}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tableView) {
        indexSelected = (int) indexPath.row;
        if (self.searchController.active == false)
            [self performSegueWithIdentifier:@"exerciseListSegue" sender:self];
        else {
            
            // we are setting this to false because otherwise a UIViewWrapperController that is transparent appears on top of view and navigation bar becomes inactive.
            self.searchController.active = false;
            searchString = @"";
            
            ExerciseList *data = [searchResults objectAtIndex:indexPath.row];
            NSLog(@"exercise selected %@ group number %@", data.exerciseName, _exerciseGroup);
            // toast with duration, title, and position
            [self customSetsMenu:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup];
            
            //        int cSets = [Utilities getDefaultSetsValue];
            //        int cReps = [Utilities getDefaultRepsValue];
            //        int cRest = [Utilities getDefaultRestTimerValue];
            //        if (_isTempEx) {
            //            if ([Utilities createTempWorkoutForDate:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == nil) {
            //                [self showNotificationMessage:@"Success" message:@"TEMPORARY EXERCISE ADDED" alertType:ViewAlertSuccess];
            //                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            //            } else {
            //                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            //            }
            //        } else {
            //            if ([Utilities addExerciseToWorkout:data date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup  customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == true) {
            //                NSLog(@"ADDING TO ROUTINE ****** %@", _exerciseGroup);
            //                [self showNotificationMessage:@"Success" message:@"EXERCISE ADDED" alertType:ViewAlertSuccess];
            //
            //                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            //            } else {
            //                //[TSMessage showNotificationInViewController:self title:@"EXERCISE ALREADY EXIST" subtitle:@"" type:TSMessageNotificationTypeWarning duration:notiDuration];
            //                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            //            }
            //        }
        }
    } else {
        
    }
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.tableView) {
        if (self.searchController.active == false)
            //return (CGRectGetHeight(self.view.frame) - CGRectGetHeight(searchBar.frame))/[muscleArray count];
            return (CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.searchController.searchBar.frame))/[muscleArray count];
        else
            return 30;
    } else {
        return 90;
    }
}

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");
    
    searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchString isEqualToString:@""]) {
        //[searchResults removeAllObjects];
        searchResults = nil;
//        for (NSString *str in alle) {
//            if ([searchString isEqualToString:@""] || [str localizedCaseInsensitiveContainsString:searchString] == YES) {
//                NSLog(@"str=%@", str);
//                [searchResults addObject:str];
//            }
//        }
        NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"exerciseName contains[c] %@", searchString];
        searchResults = [allExercises filteredArrayUsingPredicate:resultPredicate];
        self.tableView.scrollEnabled = true;
    } else
        self.tableView.scrollEnabled = false;
    [self.tableView reloadData];
}

/*
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"exerciseName contains[c] %@", searchText];
    searchResults = [allExercises filteredArrayUsingPredicate:resultPredicate];
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}
*/
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"exerciseListSegue"]) {
        //ExerciseListTable *destVC = segue.destinationViewController;
        ExerciseSelectForWorkout *destVC = segue.destinationViewController;
        destVC.date = _date;
        destVC.workoutName = _workoutName;
        destVC.routineName = _routineName;
        destVC.muscleGroup = ((MuscleList *)[muscleArray objectAtIndex:indexSelected]).majorMuscle;
        destVC.isTempExercise = _isTempEx;
        destVC.exerciseGroup = _exerciseGroup;
        destVC.exerciseNumInGroup = _exerciseNumInGroup;

        NSLog(@"date passed to exerciseSelectForWorkout is %@", destVC.date);
    }
}
-(void) showNotificationMessage: (NSString *) title message:(NSString *) message alertType: (ViewAlertType ) type{
    [EHPlainAlert showAlertWithTitle:title message:message type:type];
}

#pragma custom sets and reps
-(void) customSetsMenu:
(NSString *)date
           routineName:(NSString *)rName
           workoutName:(NSString *)wName
          exerciseList:(ExerciseList *) data
         exerciseGroup:(NSNumber *) exGroup
    exerciseNumInGroup:(NSNumber *) exNumInGrp {
    
    __block int cSets = [Utilities getDefaultSetsValue];
    __block int cReps = [Utilities getDefaultRepsValue];
    __block int cRest = [Utilities getDefaultRestTimerValue];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:data.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 140)];
    
    int itemHeight = 40;
    float popupWidth = 190;
    
    // sets
    UIButton *setsMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, itemHeight)];
    [setsMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    setsMinusBtn.backgroundColor = FlatMintDark;
    [setsMinusBtn addTarget:self action:@selector(setsDec:) forControlEvents:UIControlEventTouchUpInside];
    
    setsTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsMinusBtn.frame), 0, popupWidth, itemHeight)];
    setsTextField.placeholder = @"SETS";
    setsTextField.textAlignment = NSTextAlignmentCenter;
    setsTextField.keyboardType = UIKeyboardTypeDecimalPad;
    setsTextField.textContainer.maximumNumberOfLines = 1;
    setsTextField.scrollEnabled = false;
    setsTextField.floatingLabelYPadding = 0;
    setsTextField.placeholderYPadding = -8;
    setsTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    setsTextField.text = [NSString stringWithFormat:@"%d", cSets];
    
    UIButton *setsPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsTextField.frame), 0, 30, itemHeight)];
    [setsPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    setsPlusBtn.backgroundColor = FlatMintDark;
    [setsPlusBtn addTarget:self action:@selector(setsInc:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBezierPath *setPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:setsPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *setMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:setsMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *setPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *setMinusmaskLayer = [[CAShapeLayer alloc] init];
    setPlusmaskLayer.frame = self.view.bounds;
    setPlusmaskLayer.path  = setPlusmaskPath.CGPath;
    setMinusmaskLayer.frame = self.view.bounds;
    setMinusmaskLayer.path  = setMinusmaskPath.CGPath;
    setsPlusBtn.layer.mask = setPlusmaskLayer;
    setsMinusBtn.layer.mask = setMinusmaskLayer;
    
    // reps
    UIButton *repMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(setsPlusBtn.frame) + 5, 30, itemHeight)];
    [repMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    repMinusBtn.backgroundColor = FlatMintDark;
    [repMinusBtn addTarget:self action:@selector(repsDec:) forControlEvents:UIControlEventTouchUpInside];
    
    repsTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinusBtn.frame), CGRectGetMaxY(setsPlusBtn.frame) + 5, popupWidth, itemHeight)];
    repsTextField.placeholder = @"REPS";
    repsTextField.textAlignment = NSTextAlignmentCenter;
    repsTextField.keyboardType = UIKeyboardTypeDecimalPad;
    repsTextField.textContainer.maximumNumberOfLines = 1;
    repsTextField.scrollEnabled = false;
    repsTextField.floatingLabelYPadding = 0;
    repsTextField.placeholderYPadding = -8;
    repsTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    repsTextField.text = [NSString stringWithFormat:@"%d", cReps];
    
    UIButton *repPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repsTextField.frame), CGRectGetMaxY(setsPlusBtn.frame) + 5, 30, itemHeight)];
    [repPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    repPlusBtn.backgroundColor = FlatMintDark;
    [repPlusBtn addTarget:self action:@selector(repsInc:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBezierPath *repPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *repMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *repPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *repMinusmaskLayer = [[CAShapeLayer alloc] init];
    repPlusmaskLayer.frame = self.view.bounds;
    repPlusmaskLayer.path  = repPlusmaskPath.CGPath;
    repMinusmaskLayer.frame = self.view.bounds;
    repMinusmaskLayer.path  = repMinusmaskPath.CGPath;
    repPlusBtn.layer.mask = repPlusmaskLayer;
    repMinusBtn.layer.mask = repMinusmaskLayer;
    
    // rest
    UIButton *restMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(repPlusBtn.frame) + 5, 30, itemHeight)];
    [restMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    restMinusBtn.backgroundColor = FlatMintDark;
    [restMinusBtn addTarget:self action:@selector(restDec:) forControlEvents:UIControlEventTouchUpInside];
    
    restTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(restMinusBtn.frame), CGRectGetMaxY(repPlusBtn.frame) + 5, popupWidth, itemHeight)];
    restTextField.placeholder = @"REST";
    restTextField.textAlignment = NSTextAlignmentCenter;
    restTextField.keyboardType = UIKeyboardTypeDecimalPad;
    restTextField.textContainer.maximumNumberOfLines = 1;
    restTextField.scrollEnabled = false;
    restTextField.floatingLabelYPadding = 0;
    restTextField.placeholderYPadding = -8;
    restTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    restTextField.text = [NSString stringWithFormat:@"%d", cRest];
    
    UIButton *restPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(restTextField.frame), CGRectGetMaxY(repPlusBtn.frame) + 5, 30, itemHeight)];
    [restPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    restPlusBtn.backgroundColor = FlatMintDark;
    [restPlusBtn addTarget:self action:@selector(restInc:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBezierPath *restPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:restPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *restMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:restMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *restPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *restMinusmaskLayer = [[CAShapeLayer alloc] init];
    restPlusmaskLayer.frame = self.view.bounds;
    restPlusmaskLayer.path  = restPlusmaskPath.CGPath;
    restMinusmaskLayer.frame = self.view.bounds;
    restMinusmaskLayer.path  = restMinusmaskPath.CGPath;
    restPlusBtn.layer.mask = restPlusmaskLayer;
    restMinusBtn.layer.mask = restMinusmaskLayer;
    
    
    [customView addSubview:setsMinusBtn];
    [customView addSubview:setsTextField];
    [customView addSubview:setsPlusBtn];
    
    [customView addSubview:repMinusBtn];
    [customView addSubview:repsTextField];
    [customView addSubview:repPlusBtn];
    
    [customView addSubview:restMinusBtn];
    [customView addSubview:restTextField];
    [customView addSubview:restPlusBtn];
    
    UIView *customBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    
    CNPPopupButton *cancelButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.selectionHandler = ^(CNPPopupButton *button){
        [self showNotificationMessage:@"Info:" message:@"EXERCISE NOT ADDED" alertType:ViewAlertInfo];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *addButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cancelButton.frame) + 10, 0, 120, 40)];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    addButton.backgroundColor = [Utilities getAppColor];
    addButton.layer.cornerRadius = 5;
    addButton.selectionHandler = ^(CNPPopupButton *button){
        cSets = [setsTextField.text intValue];
        cReps = [repsTextField.text intValue];
        cRest = [restTextField.text intValue];
        if (_isTempEx) {
            if ([Utilities createTempWorkoutForDate:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == nil) {
                [self showNotificationMessage:@"Success" message:@"TEMPORARY EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        } else {
            if ([Utilities addExerciseToWorkout:data date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup  customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == true) {
                NSLog(@"ADDING TO ROUTINE ****** %@", _exerciseGroup);
                [self showNotificationMessage:@"Success" message:@"EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        }
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    [customBtnView addSubview:cancelButton];
    [customBtnView addSubview:addButton];
    
    CNPPopupButton *advance = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [advance setTitleColor:FlatSkyBlue forState:UIControlStateNormal];
    advance.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [advance setTitle:@"Advance" forState:UIControlStateNormal];
    advance.layer.cornerRadius = 5;
    advance.backgroundColor = [UIColor whiteColor];
    advance.selectionHandler = ^(CNPPopupButton *button){
        cSets = [setsTextField.text intValue];
        cReps = [repsTextField.text intValue];
        cRest = [restTextField.text intValue];
        [self.popupController dismissPopupControllerAnimated:YES];
        [self changeRepsForThisSet:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup sets:cSets reps:cReps rest:cRest];
    };

    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, customView , advance, customBtnView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)setsDec:(id)sender {
    int sets = [setsTextField.text intValue];
    
    if (sets <= 1) {
        return;
    }
    sets -= 1;
    setsTextField.text = [NSString stringWithFormat:@"%d", sets];
}
-(IBAction)setsInc:(id)sender {
    int sets = [setsTextField.text intValue];
    sets += 1;
    setsTextField.text = [NSString stringWithFormat:@"%d", sets];
}

-(IBAction)repsDec:(id)sender {
    int reps = [repsTextField.text intValue];
    
    if (reps <= 1) {
        return;
    }
    reps -= 1;
    repsTextField.text = [NSString stringWithFormat:@"%d", reps];
    
}
-(IBAction)repsInc:(id)sender {
    int reps = [repsTextField.text intValue];
    
    reps += 1;
    repsTextField.text = [NSString stringWithFormat:@"%d", reps];
    
}

-(IBAction)restDec:(id)sender {
    int rest = [restTextField.text intValue];
    
    if (rest <= 5) {
        return;
    }
    rest -= 5;
    restTextField.text = [NSString stringWithFormat:@"%d", rest];
}
-(IBAction)restInc:(id)sender {
    int rest = [restTextField.text intValue];
    rest += 5;
    restTextField.text = [NSString stringWithFormat:@"%d", rest];
}

- (void)changeRepsForThisSet:(NSString *)date
                 routineName:(NSString *)rName
                 workoutName:(NSString *)wName
                exerciseList:(ExerciseList *) data
               exerciseGroup:(NSNumber *) exGroup
          exerciseNumInGroup:(NSNumber *) exNumInGrp
                        sets:(int) sets
                        reps:(int) reps
                        rest:(int) rest {
    
    repArray = nil;
    repArray = [[NSMutableArray alloc] init];
    amarpArray = nil;
    amarpArray = [[NSMutableArray alloc] init];
    timerArray = nil;
    timerArray = [[NSMutableArray alloc] init];
    
    
    for (int i = 0; i < sets ; i++) {
        [repArray addObject:[NSNumber numberWithInt:reps]];
        [amarpArray addObject:@0];
        [timerArray addObject:[NSNumber numberWithInt:rest]];
    }
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:data.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UIView *setsHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    setsValue = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    
    UIStepper *setsStepper = [[UIStepper alloc] init];
    setsStepper.value = sets;
    setsStepper.minimumValue = 1;
    setsStepper.maximumValue = 20;
    setsStepper.frame = CGRectMake(CGRectGetMaxX(setsHeader.frame) - 100, 0, 100, 30);
    [setsStepper addTarget:self action:@selector(setsValueChanged:) forControlEvents:UIControlEventValueChanged];
    [setsHeader addSubview:setsValue];
    [setsHeader addSubview:setsStepper];
    
    UIView *customHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    UILabel *setN  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    setN.text = @"Set #";
    setN.textAlignment = NSTextAlignmentCenter;
    //setN.backgroundColor = FlatRedDark;
    
    
    UILabel *setsNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setN.frame), 0, CGRectGetWidth(customHeader.frame)/2 + 10, 30)];
    setsNum.text = @"Per Set";
    setsNum.textAlignment = NSTextAlignmentCenter;
    
    UILabel *maxRep  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsNum.frame), 0, CGRectGetWidth(customHeader.frame)/4, 30)];
    maxRep.text = @"AMRAP";
    maxRep.textAlignment = NSTextAlignmentCenter;
    
    
    [customHeader addSubview:setN];
    [customHeader addSubview:setsNum];
    [customHeader addSubview:maxRep];
    float itemHeight = 35, defaultSets = 5;
    
    customRepsPerSet = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(maxRep.frame), 0, 320, (itemHeight + 5) * defaultSets) style:UITableViewStylePlain];
    [customRepsPerSet registerClass:[UITableViewCell self] forCellReuseIdentifier:@"RepsCell"];
    customRepsPerSet.backgroundColor = [UIColor clearColor];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    self.customRepsPerSet.delegate = self;
    self.customRepsPerSet.dataSource = self;
    
    setCount = sets;
    
    UIView *customBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    
    CNPPopupButton *cancelButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.selectionHandler = ^(CNPPopupButton *button){
        [self showNotificationMessage:@"Info:" message:@"EXERCISE NOT ADDED" alertType:ViewAlertInfo];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *addButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cancelButton.frame) + 10, 0, 120, 40)];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    addButton.backgroundColor = [Utilities getAppColor];
    addButton.layer.cornerRadius = 5;
    addButton.selectionHandler = ^(CNPPopupButton *button){
        if (_isTempEx) {
            if ([Utilities createTempWorkoutForDate:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup customSet:setCount customRep:reps customRest:rest repsPerSet:[self createRepsPerSet] restPerSet:[self createRestPerSet]] == nil) {
                [self showNotificationMessage:@"Success" message:@"TEMPORARY EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        } else {
            if ([Utilities addExerciseToWorkout:data date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup  customSet:setCount customRep:reps customRest:rest repsPerSet:[self createRepsPerSet] restPerSet:[self createRestPerSet]] == true) {
                NSLog(@"ADDING TO ROUTINE ****** %@", _exerciseGroup);
                [self showNotificationMessage:@"Success" message:@"EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        }
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    
    [customBtnView addSubview:cancelButton];
    [customBtnView addSubview:addButton];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, setsHeader, customHeader, self.customRepsPerSet, customBtnView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)repTableDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"decreasing rep %ld", (long)indexPath.row);
        if ([[repArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] - 1)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)repTableInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rep %ld", (long)indexPath.row);
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] + 1)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)amarpIsChanged:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    NSLog(@"AMARP changed %ld", (long)indexPath.row);
    if (indexPath != nil)
    {
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == false) {
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@1];//true
        } else
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@0];
    }
    [self.customRepsPerSet reloadData];
}

-(IBAction)timerTableDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSLog(@"decreasing rest %ld", (long)indexPath.row);
        if ([[timerArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] - 5)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)timerTableInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rest %ld", (long)indexPath.row);
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] + 5)];
    }
    [self.customRepsPerSet reloadData];
}
-(NSString *) createRepsPerSet {
    NSString *repsPerSet = @"";
    
    for (int i = 0; i < repArray.count; i++) {
        if ([[amarpArray objectAtIndex:i] boolValue] == true) {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = @"MAX";//[NSString stringWithFormat:@"%d", [repArray objectAtIndex:i]];
            } else  {
                repsPerSet = [NSString stringWithFormat:@"%@,MAX", repsPerSet];
            }
        }
        else {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:i]];
            } else {
                repsPerSet = [NSString stringWithFormat:@"%@,%@", repsPerSet, [repArray objectAtIndex:i]];
            }
        }
        
    }
    return repsPerSet;
}

-(NSString *) createRestPerSet {
    NSString *restPerSet = @"";
    
    for (int i = 0; i < timerArray.count; i++) {
        if ([restPerSet isEqualToString:@""]) {
            restPerSet = [NSString stringWithFormat:@"%@", [timerArray objectAtIndex:i]];
        } else {
            restPerSet = [NSString stringWithFormat:@"%@,%@", restPerSet, [timerArray objectAtIndex:i]];
        }
    }
    return restPerSet;
}
-(IBAction)setsValueChanged:(id)sender {
    UIStepper *setsVal = (UIStepper *)sender;
    int sets = setsVal.value;
    
    if (sets < setCount) {
        // remove sets
        [repArray removeObjectAtIndex:setCount - 1];
        [amarpArray removeObjectAtIndex:setCount - 1];
        [timerArray removeObjectAtIndex:setCount - 1];
    } else if (sets > setCount){
        // add sets
        [repArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRepsValue]]];
        [amarpArray addObject:@0];
        [timerArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRestTimerValue]]];
    }
    setCount = sets;
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    [self.customRepsPerSet reloadData];
}
@end
