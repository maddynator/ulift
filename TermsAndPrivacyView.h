//
//  TermsAndPrivacyView.h
//  uLift
//
//  Created by Mayank Verma on 10/23/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TermsAndPrivacyView : UIViewController

@property (nonatomic) int  whichWebPage;
@property (nonatomic, strong) IBOutlet UIWebView *webView;
@end
