//
//  CardioStats+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "CardioStats+CoreDataProperties.h"

@implementation CardioStats (CoreDataProperties)

+ (NSFetchRequest<CardioStats *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"CardioStats"];
}

@dynamic caloriesBurned;
@dynamic cardioEquipment;
@dynamic cardioType;
@dynamic date;
@dynamic distance;
@dynamic duration;
@dynamic elevation;
@dynamic emptyStomach;
@dynamic pace;
@dynamic startTime;
@dynamic syncedState;
@dynamic trainingType;

@end
