//
//  CreateWorkout.h
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface CreateWorkout : UIViewController   <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, UITextFieldDelegate, UIScrollViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *routineBundleId;
@property (nonatomic, retain) NSString *workoutName;
@end
