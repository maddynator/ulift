//
//  UserWorkout+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "UserWorkout+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserWorkout (CoreDataProperties)

+ (NSFetchRequest<UserWorkout *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *createdDate;
@property (nullable, nonatomic, copy) NSNumber *exerciseGroup;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSNumber *exerciseNumber;
@property (nullable, nonatomic, copy) NSNumber *exerciseNumInGroup;
@property (nullable, nonatomic, copy) NSNumber *hasDropSet;
@property (nullable, nonatomic, copy) NSNumber *hasPyramidSet;
@property (nullable, nonatomic, copy) NSString *majorMuscle;
@property (nullable, nonatomic, copy) NSString *muscle;
@property (nullable, nonatomic, copy) NSNumber *reps;
@property (nullable, nonatomic, copy) NSNumber *restTimer;
@property (nullable, nonatomic, copy) NSString *routineBundleId;
@property (nullable, nonatomic, copy) NSString *routineName;
@property (nullable, nonatomic, copy) NSNumber *sets;
@property (nullable, nonatomic, copy) NSNumber *superSetWith;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSString *workoutName;
@property (nullable, nonatomic, copy) NSNumber *workoutNumber;
@property (nullable, nonatomic, copy) NSNumber *workoutUserCreated;
@property (nullable, nonatomic, copy) NSString *restPerSet;
@property (nullable, nonatomic, copy) NSString *repsPerSet;

@end

NS_ASSUME_NONNULL_END
