//
//  TodayWorkoutNew.h
//  gyminutes
//
//  Created by Mayank Verma on 5/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import <MessageUI/MessageUI.h>

@interface TodayWorkoutNew : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, CNPPopupControllerDelegate, UIScrollViewDelegate, RSDFDatePickerViewDelegate, RSDFDatePickerViewDataSource, MPCoachMarksViewDelegate, ChartViewDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) IBOutlet UILabel *recordDate;
@property (retain, nonatomic) IBOutlet DIDatepicker *datepicker;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;

-(IBAction)saveToParse:(id)sender;
@property (nonatomic, strong) IBOutlet LineChartView *lineChartView;
@property (nonatomic, strong) IBOutlet PieChartView *pieChartView;
@property (nonatomic, strong) IBOutlet BarChartView *barChartView;


@end
