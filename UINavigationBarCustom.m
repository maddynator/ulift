
//-------------------------
// UINavigationBarCustom.m
//-------------------------
#import "UINavigationBarCustom.h"

const CGFloat MyNavigationBarHeightIncrease = 38.f;

@implementation UINavigationBarCustom


- (id)initWithCoder:(NSCoder *)aDecoder {
    
    self = [super initWithCoder:aDecoder];
    
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (id)initWithFrame:(CGRect)frame {
    
    self = [super initWithFrame:frame];
    
    if (self) {
        [self initialize];
    }
    
    return self;
}

- (void)initialize {
    // Set tittle position for top
    [self setTitleVerticalPositionAdjustment:-(MyNavigationBarHeightIncrease) forBarMetrics:UIBarMetricsDefault];
}

- (CGSize)sizeThatFits:(CGSize)size {
    // Increase NavBar size
    CGSize amendedSize = [super sizeThatFits:size];
    amendedSize.height += MyNavigationBarHeightIncrease;
    
    return amendedSize;
}

- (void)layoutSubviews {
    // Set buttons position for top
    [super layoutSubviews];
    
    NSArray *classNamesToReposition = @[@"UINavigationButton"];
    
    for (UIView *view in [self subviews]) {
        
        if ([classNamesToReposition containsObject:NSStringFromClass([view class])]) {
            
            CGRect frame = [view frame];
            frame.origin.y -= MyNavigationBarHeightIncrease;
            
            [view setFrame:frame];
        }
    }
}

- (void)didAddSubview:(UIView *)subview
{
    // Set segmented position
    [super didAddSubview:subview];
    
    if ([subview isKindOfClass:[UISegmentedControl class]])
    {
        CGRect frame = subview.frame;
        frame.origin.y += MyNavigationBarHeightIncrease;
        subview.frame = frame;
    }
}

@end