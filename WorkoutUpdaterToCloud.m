//
//  WorkoutUpdaterToCloud.m
//  gyminutes
//
//  Created by Mayank Verma on 2/28/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "WorkoutUpdaterToCloud.h"
#import "commons.h"

@implementation WorkoutUpdaterToCloud


+(void) downloadAllIAPWorkoutsAndUpdateCustomSets:(NSString *) routineBundleId {
        PFQuery *queryUE = [PFQuery queryWithClassName:@P_IAP_WORKOUTS];
        [queryUE whereKey:@"RoutineBundleId" equalTo:routineBundleId];

        NSError *error;
        NSArray *workoutData = [queryUE findObjects:&error];
        
        if (!error) {
            NSLog(@"Routine bundle id is: %lu",(long)[workoutData count]);
            [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                
                //RoutineMetaInfo *isPresent = [RoutineMetaInfo MR_findFirstWithPredicate:routineCheck inContext:localContext];
                
                for (int i = 0; i < [workoutData count]; i++) {
                    PFObject *workoutEntry = [workoutData objectAtIndex:i];
                    NSPredicate *workoutCheck = [NSPredicate predicateWithFormat:@"routineName == %@ AND workoutName == %@ AND exerciseName == %@", workoutEntry[@"RoutineName"], workoutEntry[@"WorkoutName"],  workoutEntry[@"ExerciseName"]];
                    UserWorkout *workout = [UserWorkout MR_findFirstWithPredicate:workoutCheck inContext:localContext];
                    
                    if (workout != nil) {
                        NSLog(@"Workout already present and was downloaded before... %@ %@", workoutEntry[@"RoutineName"], workoutEntry[@"WorkoutName"]);
                        continue;
                    } else {
                        workout = [UserWorkout MR_createEntityInContext:localContext];
                        workout.createdDate = workoutEntry[@"CreatedDate"];
                        workout.routineName = workoutEntry[@"RoutineName"];
                        workout.workoutName = workoutEntry[@"WorkoutName"];
                        workout.exerciseName = workoutEntry[@"ExerciseName"];
                        workout.majorMuscle = workoutEntry[@"MajorMuscle"];
                        workout.muscle = workoutEntry[@"Muscle"];
                        workout.exerciseNumber = workoutEntry[@"ExerciseNumber"];
                        workout.sets = workoutEntry[@"Sets"];
                        workout.reps = workoutEntry[@"Reps"];
                        workout.restTimer = workoutEntry[@"RestTimer"];
                        workout.superSetWith = workoutEntry[@"SuperSetWith"];
                        workout.hasDropSet = workoutEntry[@"HasDropSet"];
                        workout.hasPyramidSet = workoutEntry[@"HasPyramidSet"];
                        workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
                        workout.routineBundleId = routineBundleId;
                        workout.workoutUserCreated = [NSNumber numberWithBool:false];
                        workout.exerciseNumInGroup = workoutEntry[@"ExerciseNumInGroup"];
                        workout.exerciseGroup = workoutEntry[@"ExerciseGroup"];
                        workout.repsPerSet = [Utilities createRepsPerSet:[workout.sets intValue] reps:[workout.reps intValue]];
                        workout.restPerSet = [Utilities createRestPerSet:[workout.sets intValue] rest:[workout.restTimer intValue]];
                        NSLog(@"downloaded workout %@", workout);
                }
                }
            } completion:^(BOOL contextDidSave, NSError *error) {
                // we do not want update muscle again when user exercises are synced...
                NSLog(@"**** USERWORKOUT SYNC DONE ****");
                [self syncWorkoutInfoToIAPWorkout];
            }];
        }
}

+(void) syncWorkoutInfoToIAPWorkout {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"syncedState == %@", [NSNumber numberWithBool:SYNC_NOT_DONE]];
    NSArray *workouts = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    NSLog(@"will sync workout info now... %lu", (long) [workouts count]);
    
    for (UserWorkout *workoutObj in workouts) {
        
        NSLog(@"sncing routine info %@ and workout info... %@, %@, %@", workoutObj.routineName, workoutObj.workoutName, workoutObj.exerciseName, [PFUser currentUser].objectId);
        NSLog(@"workout %@, %@, %@\n", workoutObj.syncedState, workoutObj.exerciseName, workoutObj.exerciseNumber);
        
        
        //        [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        workoutObj.syncedState = [NSNumber numberWithBool:SYNC_DONE];
        //            NSLog(@"sync state updated...");
        //        }];
        [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];
        
        
        PFQuery *query = [PFQuery queryWithClassName:@P_IAP_WORKOUTS];
        [query whereKey:@"RoutineName" equalTo:workoutObj.routineName];
        [query whereKey:@"WorkoutName" equalTo:workoutObj.workoutName];
        [query whereKey:@"ExerciseName" equalTo:workoutObj.exerciseName];
        [query setLimit: 1];
        [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
            if (task.error) {
                NSLog(@"Error: %@", task.error);
                return task;
            }
            PFObject *rObj;
            NSLog(@"number of elements found %lu", (long)[task.result count]);
            if ([task.result count] == 0) {
                NSLog(@"Creating a new workout..");
                rObj = [PFObject objectWithClassName:@P_IAP_WORKOUTS];
                rObj[@"CreatedDate"] = workoutObj.createdDate;
                rObj[@"RoutineName"] = workoutObj.routineName;
                rObj[@"WorkoutName"] = workoutObj.workoutName;
                rObj[@"ExerciseName"] = workoutObj.exerciseName;
                rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                rObj[@"Muscle"] = workoutObj.muscle;
                rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                rObj[@"Sets"] = workoutObj.sets;
                rObj[@"Reps"]  = workoutObj.reps;
                rObj[@"RestTimer"]  = workoutObj.restTimer;
                rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                rObj[@"RepsPerSet"] = workoutObj.repsPerSet;
                rObj[@"RestPerSet"] = workoutObj.restPerSet;
                rObj[@"IsActive"] = @YES;
                [rObj saveEventually];
            } else {
                NSLog(@"updating old workout");
                rObj = [task.result objectAtIndex:0];
                rObj[@"CreatedDate"] = workoutObj.createdDate;
                rObj[@"RoutineName"] = workoutObj.routineName;
                rObj[@"WorkoutName"] = workoutObj.workoutName;
                rObj[@"ExerciseName"] = workoutObj.exerciseName;
                rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                rObj[@"Muscle"] = workoutObj.muscle;
                rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                rObj[@"Sets"] = workoutObj.sets;
                rObj[@"Reps"]  = workoutObj.reps;
                rObj[@"RestTimer"]  = workoutObj.restTimer;
                rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                rObj[@"RepsPerSet"] = workoutObj.repsPerSet;
                rObj[@"RestPerSet"] = workoutObj.restPerSet;
                rObj[@"IsActive"] = @YES;
                [rObj saveEventually];
                
            }
            NSLog(@"Workout Synced.. %@", workoutObj.workoutName);
            return task.result;
        }];
        
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
