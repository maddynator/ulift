//
//  WorkoutStore.m
//  gyminutes
//
//  Created by Mayank Verma on 7/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "WorkoutStore.h"
#import "RoutineDetailsTVC.h"

@interface WorkoutStore(){
    NSMutableArray *_routineProduct;
    NSNumberFormatter * _priceFormatter;
    SKProduct *selectedProduct;
    BOOL isEarlyBird;

    NSArray  *sectionHeader;
    NSDictionary *fbWorkoutDict, *bbWorkoutDict, *stWorkoutDict;
}
#define BEGINNER    @"Beginner"
#define INTERMEDIATE    @"Intermediate"
#define ADVANCE     @"Advance"

@end
@implementation WorkoutStore

@synthesize collectionView;
@synthesize refreshControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
        
    sectionHeader = @[@"FULLBODY", @"BODYBUILDING", @"STRENGTH TRAINING"];
    
    fbWorkoutDict = @{
                          @"Workouts": @[
                                    @{@"Level": BEGINNER,
                                      @"Name": @"BODYWEIGHT MAX",
                                      @"Type": @"FullBody",
                                      @"Image": [UIImage imageNamed:@"IconWRBodyWeightMax"],
                                      @"ProductId": @IAP_ROUTINE_BODYWEIGHT_MAX,
                                      }
                                    ]
                         };
    
    bbWorkoutDict = @{
                      @"Workouts": @[
                              @{@"Level": INTERMEDIATE,
                                @"Name": @"HYPERTROPHY UNLIMITED",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWRHypertrophyUnlimited"],
                                @"ProductId": @IAP_ROUTINE_HYPERTROPHY_UNLIMITED,
                                },
                              @{@"Level": ADVANCE,
                                @"Name": @"2 DAY SPLIT: PUSH/PULL",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR2DaySplitPushPull"],
                                @"ProductId": @IAP_ROUTINE_2DAYSPLITPUSHPULL,
                                },
                              @{@"Level": BEGINNER,
                                @"Name": @"3 DAY LEAN MUSCLE",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR3DaySplit"],
                                @"ProductId": @IAP_ROUTINE_3DAYSPLIT,
                                },
                              @{@"Level": BEGINNER,
                                @"Name": @"4 DAY MUSCLE MASS",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR4DaySplit"],
                                @"ProductId": @IAP_ROUTINE_4DAYSPLIT,
                                },
                              @{@"Level": INTERMEDIATE,
                                @"Name": @"PENTAGRAM POWER",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR5DaySplit"],
                                @"ProductId": @IAP_ROUTINE_5DAYSPLIT,
                                },
                              @{@"Level": ADVANCE,
                                @"Name": @"STRONG SIX",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR6DaySplit"],
                                @"ProductId": @IAP_ROUTINE_6DAYSPLIT,
                                },
                              @{@"Level": BEGINNER,
                                @"Name": @"3 DAY DUMBBELL DOMINATION",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWR3DS"],
                                @"ProductId": @IAP_ROUTINE_3DAYDUMBBELLDOMINATION,
                                },
                              @{@"Level": INTERMEDIATE,
                                @"Name": @"FOCUSSED FOUR",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWRFocussedFour"],
                                @"ProductId": @IAP_ROUTINE_FOCUSSED_FOUR,
                                },
                              @{@"Level": BEGINNER,
                                @"Name": @"GERMAN VOLUME TRAINING I",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWRGvt1"],
                                @"ProductId": @IAP_ROUTINE_GVT_I,
                                },
                              @{@"Level": INTERMEDIATE,
                                @"Name": @"GERMAN VOLUME TRAINING II",
                                @"Type": @"BODYBUILDING",
                                @"Image": [UIImage imageNamed:@"IconWRGvt2"],
                                @"ProductId": @IAP_ROUTINE_GVT_II,
                                }
                              ]
                      };
    
    stWorkoutDict = @{
                      @"Workouts": @[
                              @{@"Level": BEGINNER,
                                @"Name": @"STRONGLIFT 5x5",
                                @"Type": @"STRENGTH",
                                @"Image": [UIImage imageNamed:@"IconWRStrongLift5x5"],
                                @"ProductId": @IAP_ROUTINE_STRONGLIFT5X5,
                                }
                              ]
                      };


    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(self.navigationController.navigationBar.frame), self.view.frame.size.width, CGRectGetHeight(self.view.frame) - 150) collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    
    [collectionView registerClass:[UICollectionReusableView class]
            forSupplementaryViewOfKind: UICollectionElementKindSectionHeader
                   withReuseIdentifier:@"HeaderView"];
    
    [self.view addSubview:collectionView];
    _routineProduct = [[NSMutableArray alloc] init];
    
    selectedProduct = nil;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    
    if ([Utilities connected]) {
        [self reload];
    }
    else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"No Connectivity" subTitle:@"Unable to connect. Please check your network connectivity." closeButtonTitle:@"Ok" duration:0.0f];
    }
    
    [self.refreshControl beginRefreshing];
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    
    // Do any additional setup after loading the view, typically from a nib.
}

-(void) viewDidAppear:(BOOL)animated {
    selectedProduct = nil;
    [collectionView reloadData];
}
-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return [sectionHeader count];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    switch (section) {
        case 0:
            return [[fbWorkoutDict objectForKey:@"Workouts"] count];
            break;
        case 1:
            return [[bbWorkoutDict objectForKey:@"Workouts"] count];
            break;
        case 2:
            return [[stWorkoutDict objectForKey:@"Workouts"] count];
            break;
            
        default:
            break;
    }
    return 0;
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)CollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[CollectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor = FlatBlack;
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.layer.shadowRadius = 2;
    cell.layer.shadowOpacity = 0.5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    UILabel *title = [[UILabel alloc] init];
    NSArray *itemArray = nil;
    
    switch (indexPath.section) {
        case 0:
            itemArray = [fbWorkoutDict objectForKey:@"Workouts"];
            break;
        case 1:
            itemArray = [bbWorkoutDict objectForKey:@"Workouts"];
            break;
        case 2:
            itemArray = [stWorkoutDict objectForKey:@"Workouts"];
            break;
        default:
            break;
    }
    
    
    NSDictionary *itemDict = [itemArray objectAtIndex:indexPath.row];
    title.text = [itemDict objectForKey:@"Name"];
//    title.layer.cornerRadius = 20;
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:18]];
    title.textColor = [UIColor whiteColor];
    title.backgroundColor = [FlatBlack colorWithAlphaComponent:0.5];
    
    title.textAlignment = NSTextAlignmentCenter;
    [title setClipsToBounds:YES];
//    title.layer.borderWidth= 5;
//    title.layer.borderColor = [UIColor whiteColor].CGColor;
    title.frame = CGRectMake(0, CGRectGetHeight(cell.frame) - 40, CGRectGetWidth(cell.frame), 40);
    
    UIImageView *bcImage  = [[UIImageView alloc] init];
    [bcImage setContentMode:UIViewContentModeScaleAspectFill];
    [bcImage setImage:[itemDict objectForKey:@"Image"]];
    cell.backgroundColor = [UIColor clearColor];
    bcImage.backgroundColor = FlatMint;
    [bcImage setClipsToBounds:YES];
    bcImage.layer.cornerRadius  = 5;
    bcImage.frame = CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame));
    
    [cell.contentView addSubview:bcImage];
    [cell.contentView addSubview:title];

    if ([[uLiftIAPHelper sharedInstance] productPurchased:[itemDict objectForKey:@"ProductId"]]) {
        UILabel *isPurchased = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.frame) - 100, 0, 100, 40)];
        isPurchased.text = @"PURCHASED";
        isPurchased.textAlignment = NSTextAlignmentCenter;
        [isPurchased setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:18]];

        isPurchased.textColor = [UIColor whiteColor];
        //isPurchased.backgroundColor = FlatMintDark;
        [cell.contentView addSubview:isPurchased];
        NSLog(@"in here with view...");
    }
    

    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *itemArray = nil;
    
    switch (indexPath.section) {
        case 0:
            itemArray = [fbWorkoutDict objectForKey:@"Workouts"];
            break;
        case 1:
            itemArray = [bbWorkoutDict objectForKey:@"Workouts"];
            break;
        case 2:
            itemArray = [stWorkoutDict objectForKey:@"Workouts"];
            break;
        default:
            break;
    }
    
    NSDictionary *itemDict = [itemArray objectAtIndex:indexPath.row];
    NSString *productID = [itemDict objectForKey:@"ProductId"];

    bool productFound = false;
    for (SKProduct *item in _routineProduct) {
        if ([item.productIdentifier isEqualToString:productID]) {
            selectedProduct = item;
            productFound = true;
            break;
        }
    }
    NSLog(@"product id %@", productID);
    if (productFound) {
        [self performSegueWithIdentifier:@"routineDetailSegue" sender:self];
    }
    
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width =CGRectGetWidth(self.view.frame) - 20;
    float height = width * 3/4;
    return CGSizeMake(width, height/2);
}

-(UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,5,10,5);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)CollectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath
{
    if (kind == UICollectionElementKindSectionHeader) {
        
        UICollectionReusableView *reusableview = [CollectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView" forIndexPath:indexPath];
        
        if (reusableview==nil) {
            reusableview=[[UICollectionReusableView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        }
        for (UIView *lbl in [reusableview subviews]) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            } else if ([lbl isKindOfClass:[UIButton class]]) {
                [lbl removeFromSuperview];
            } else if ([lbl isKindOfClass:[UIImageView class]]) {
                [lbl removeFromSuperview];
            }
        }
        UILabel *label=[[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 44)];
        label.text = [sectionHeader objectAtIndex:indexPath.section];
        label.textAlignment = NSTextAlignmentCenter;
        label.textColor = [UIColor whiteColor];
        [label setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
        
        [reusableview addSubview:label];
        reusableview.backgroundColor = [Utilities getAppColor];
        return reusableview;
    }
    return nil;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    CGSize headerSize = CGSizeMake(self.view.frame.size.width, 44);
    return headerSize;
    
}

// 4
- (void)reload {
    [_routineProduct removeAllObjects];
    
    [KVNProgress showWithStatus:@"Loading..."];
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            //            _products = products;
//            NSLog(@"Products %@", products);
            for (int i = 0; i < [products count]; i++) {
                SKProduct *prod = [products objectAtIndex:i];
                NSLog(@"product id is %@", prod.productIdentifier);
                if ([prod.productIdentifier isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
                    continue;
                } else if ([prod.productIdentifier isEqualToString:@IAP_WORKOUT_PACKAGE_ID]) {
                    continue;
                } else if ([prod.productIdentifier isEqualToString:@IAP_ROUTINE_PACKAGE_ID]) {
                    continue;
                } else if ([prod.productIdentifier isEqualToString:@IAP_DATA_PACKAGE_ID]) {
                    continue;
                } else if ([prod.productIdentifier isEqualToString:@IAP_ANALYTICS_PACKAGE_ID]) {
                    continue;
                } else {
                    NSLog(@"adding routine now...");
                    [_routineProduct addObject:prod];
                }
            }
            if ([KVNProgress isVisible]) {
                [KVNProgress dismiss];
            }
        }
        [self.refreshControl endRefreshing];
    }];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"routineDetailSegue"]) {
        RoutineDetailsTVC *destVC = segue.destinationViewController;
        destVC.product = selectedProduct;
        destVC.routineBundleId = selectedProduct.productIdentifier;
        destVC.isLocalStored = false;
    }
    self.title = @"";
}

@end
