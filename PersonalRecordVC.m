//
//  PersonalRecordVC.m
//  gyminutes
//
//  Created by Mayank Verma on 7/8/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "PersonalRecordVC.h"
#import "commons.h"
#import "ShareViewController.h"

@interface PersonalRecordVC () {
    NSArray *recent, *weight, *frequency;
    NSMutableArray *itemArr;
    NSString *selectedDate;
    
}
@end
@implementation PersonalRecordVC
@synthesize tableView;

-(void)viewDidLoad {
    [super viewDidLoad];
    foursquareSegmentedControl = [[NYSegmentedControl alloc] initWithItems:@[@"Weight", @"Recent"]];//, @"Frequent"
    foursquareSegmentedControl.titleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleTextColor = [UIColor whiteColor];
    foursquareSegmentedControl.selectedTitleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    foursquareSegmentedControl.backgroundColor = FlatWhiteDark;
    foursquareSegmentedControl.borderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorBorderWidth = 0.0f;
    foursquareSegmentedControl.segmentIndicatorInset = 2.0f;
    foursquareSegmentedControl.segmentIndicatorBorderColor = self.view.backgroundColor;
    [foursquareSegmentedControl sizeToFit];
    foursquareSegmentedControl.cornerRadius = 5.0f;
    foursquareSegmentedControl.segmentIndicatorBackgroundColor = [Utilities getAppColor];
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_7_0
    foursquareSegmentedControl.usesSpringAnimations = YES;
#endif
    [foursquareSegmentedControl addTarget:self action:@selector(sortPR:) forControlEvents:UIControlEventValueChanged];
    
    foursquareSegmentedControl.frame = CGRectMake(9, 6, CGRectGetWidth(self.view.frame) - 18, 35);
    foursquareSegmentedControl.selectedSegmentIndex = 0;
    
    tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(foursquareSegmentedControl.frame) + 5, self.view.frame.size.width, CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.navigationController.navigationBar.frame) - CGRectGetHeight(foursquareSegmentedControl.frame) - 100) style:UITableViewStylePlain];
    
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.emptyDataSetSource = self;
    tableView.emptyDataSetDelegate = self;
    
    [self.view addSubview:foursquareSegmentedControl];
    [self.view addSubview:tableView];
    itemArr = [[NSMutableArray alloc] init];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"PRViewed" withParameters:articleParams];

    
}

-(void) viewDidAppear:(BOOL)animated {
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    weight = [ExerciseMetaInfo MR_findAllSortedBy:@"maxWeight" ascending:NO inContext:localContext];
    recent = [ExerciseMetaInfo MR_findAllSortedBy:@"maxDate,maxWeight" ascending:NO inContext:localContext];
    [self sortPR:foursquareSegmentedControl];
}
#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [itemArr count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";

    UITableViewCell *cell = (UITableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    UILabel *exerciseName, *date, *pr;
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(self.view.frame) - 10, 25)];
        exerciseName.tag = 1;
        //exerciseName.backgroundColor = FlatBlue;
        [exerciseName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
        exerciseName.textColor = [Utilities getAppColor];
        
        date = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(exerciseName.frame), CGRectGetWidth(self.view.frame)/2, 10)];
        date.tag = 2;
        [date setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        date.textColor = FlatGray;
        
        pr = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(date.frame), CGRectGetMaxY(exerciseName.frame), CGRectGetWidth(self.view.frame)/2 - 10, 40)];
        pr.tag = 3;
        [pr setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:34]];
        pr.textAlignment = NSTextAlignmentRight;
        pr.textColor = [Utilities getAppColor];
        
        [cell.contentView addSubview:exerciseName];
        [cell.contentView addSubview:date];
        [cell.contentView addSubview:pr];

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 68, self.tableView.frame.size.width, 2)];
        
        lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrange]];
        [cell.contentView addSubview:lineView];

    }
    exerciseName = (UILabel *) [cell viewWithTag:1];
    date = (UILabel *) [cell viewWithTag:2];
    pr = (UILabel *) [cell viewWithTag:3];
    
    ExerciseMetaInfo *item = [itemArr objectAtIndex:indexPath.row];
    exerciseName.text = item.exerciseName;
    date.text = item.maxDate;
    pr.text = [NSString stringWithFormat:@"%@x%@ %@", item.maxRep, item.maxWeight, [[Utilities getUnits] lowercaseString]];

    //NSLog(@"%@", item.maxWeight);
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ExerciseMetaInfo *item = [itemArr objectAtIndex:indexPath.row];
    selectedDate = item.maxDate;
    [self performSegueWithIdentifier:@"shareSegue" sender:self];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}
-(IBAction)sortPR:(id)sender {
    [itemArr removeAllObjects];
    switch (foursquareSegmentedControl.selectedSegmentIndex) {
        case 0: {
            for (ExerciseMetaInfo *rec in weight) {
                if ([rec.maxWeight floatValue] == 0)
                    continue;
                
                [itemArr addObject:rec];
            }
        }
            break;
        case 1:
        {
            for (ExerciseMetaInfo *rec in recent) {
                if (rec.maxDate == nil || [rec.maxWeight floatValue] == 0) {
                    continue;
                }
                [itemArr addObject:rec];
            }
        }

            break;
        case 2:
        {
            for (ExerciseMetaInfo *rec in weight) {
                if ([rec.maxWeight floatValue] == 0)
                    continue;
                
                [itemArr addObject:rec];
            }
        }
            break;
        default:
            break;
    }
    
    [tableView reloadData];
}

#pragma mark - Navigation

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"shareSegue"]) {
        ShareViewController *destVC = segue.destinationViewController;
        destVC.date = selectedDate;
        destVC.hidesBottomBarWhenPushed = TRUE;

    }
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Welcome to PR. No workout recorded yet. Start recording weights to reach new milestones.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"To start a workout, select \"HOME\" (Bottom Left) and click \"START WORKOUT\"";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIImage imageNamed:@"Icon_Home"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}
@end
