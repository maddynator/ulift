//
//  ExerciseMetaInfo+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseMetaInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExerciseMetaInfo (CoreDataProperties)

+ (NSFetchRequest<ExerciseMetaInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *comment;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSDate *goalAchieveDate;
@property (nullable, nonatomic, copy) NSNumber *goalWeightOrRep;
@property (nullable, nonatomic, copy) NSNumber *handAngle;
@property (nullable, nonatomic, copy) NSString *maxDate;
@property (nullable, nonatomic, copy) NSNumber *maxRep;
@property (nullable, nonatomic, copy) NSNumber *maxWeight;
@property (nullable, nonatomic, copy) NSString *muscle;
@property (nullable, nonatomic, copy) NSNumber *seatHeight;
@property (nullable, nonatomic, copy) NSNumber *syncedState;

@end

NS_ASSUME_NONNULL_END
