//
//  UserProfileForm.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "UserProfileForm.h"

@implementation UserProfileForm

- (NSArray *)fields
{
    return @[
            // @{FXFormFieldTitle: @"Update", FXFormFieldHeader: @"", FXFormFieldAction: @"submitProfileForm:", @"textLabel.color": FlatGreenDark},
             
             
             @{FXFormFieldKey: @"name", FXFormFieldHeader: @"Your Information",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords)},
             
             @{FXFormFieldKey:@"email"},//,  @"textField.enabled": @(NO)
             @"age",
             @{FXFormFieldKey: @"gender", FXFormFieldOptions: @[@"Male", @"Female"]},
             
             @{FXFormFieldKey: @"units", FXFormFieldOptions: @[@"Lbs/cms", @"Kgs/cms"]},
             
             @{FXFormFieldKey: @"experience", FXFormFieldOptions: @[@"Beginners(0-1 Year)", @"Intermediate (1-3 Year)", @"Expert (3+ Years)"]},
             @{FXFormFieldKey: @"weight", @"textField.keyboardType": @(UIKeyboardTypeNumberPad)},
             @{FXFormFieldKey: @"height", @"textField.keyboardType": @(UIKeyboardTypeNumberPad)},
             @{FXFormFieldTitle: @"Logout", FXFormFieldHeader: @"", FXFormFieldAction: @"logoutForm:"},
             ];
}

@end
