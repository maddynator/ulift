//
//  Utilities.h
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "commons.h"
#import "ExerciseList+CoreDataClass.h"
#import "ExerciseMetaInfo+CoreDataClass.h"
#import "UserWorkout+CoreDataClass.h"
#import "SCLAlertViewStyleKit.h"
#import "WorkoutList+CoreDataClass.h"
#import "ExerciseSet+CoreDataClass.h"

@interface Utilities : NSObject
+(void) showSyncingHudFor;

// General Utilities
+(NSString *) getCurrentDate;
+(NSString *) getYesterdayDate: (NSString * ) todayDate;
+(NSString *) getTomorrowDate: (NSString * ) todayDate;
+(UIColor *) getExerciseColor: (NSString *) muscle;
+(UIColor *) getLighterColorFor:(UIColor *) color;
+(UIColor *) getDarkerColorFor:(UIColor *) color;
+(UIColor *) colorForString:(NSString *) colorStr;
+(UIImage *) getMuscleImage: (NSString *) muscle;
+(UIColor *) getAppColor;
+(UIColor *) getAppColorLight;

+(BOOL) askForRating;
+(void) setTodayWidgetData;
+(NSString *) getNumberOfWorkoutThisWeek;
+(void) crashAnalyticsUserData;
+(int) daysSinceAccount;
+(BOOL) showWorkoutPackage;
+(BOOL) showAnalyticsPackage;
+(BOOL) showDataPackage;
+(BOOL) showPowerRoutinePackage;
+(bool) showPremiumPackage;
+(BOOL) isEarlyBird;

+(NSString *) getUnits;
+(BOOL) isKgs;
+(int) getDefaultSetsValue;
+(int) getDefaultRepsValue;
+(int) getDefaultRestTimerValue;
+(int) getDefaultRestTimerValueBetweenExercises;
+(int) getAdditionalRestTimerValue;
+(int) getDetailedSummaryValue;
+(bool) isIncBy25Enabled;
+(BOOL) isHintEnabled;
//+(BOOL) isCustomKeyboardEnabled;

+(NSString *) getUserFullNameFromFacebookOrTwitter;
+(NSString *) getIphoneName;
+ (BOOL)connected;
+(NSArray *) getAllExercises;
// Methods for login/Signup
+(void) syncMuscleGroups;

//+(NSString *) createTempWorkoutForDate: (NSString *) date routineName:(NSString *) rName workoutName:(NSString *) wName exerciseList:(ExerciseList *) exercise;
+(NSString *) createTempWorkoutForDate:(NSString *)date routineName:(NSString *)rName workoutName:(NSString *)wName exerciseList:(ExerciseList *) exercise exerciseGroup:(NSNumber *) exGroup exerciseNumInGroup:(NSNumber *) exNumInGrp customSet:(int) cSets customRep:(int) cRep customRest:(int) cRest repsPerSet:(NSString *) repsPSet restPerSet:(NSString *) restPSet;

// Methods for login
+(void) syncAllExercises:(id)sender;
+(void) syncAllExerciseMetaInfo:(id)sender;
+(void) syncAllUserRoutines:(id)sender;
+(void) syncAllUserWorksouts:(id)sender;
+(void) syncUserDataOnLogin:(NSMutableArray *) allObjectsNew skip:(NSInteger ) skip limit:(NSInteger ) limit;
+(void) syncAllUserExercises:(id)sender;
+(void) syncUserProfileOnLogin:(id) sender;
+(void) syncRoutineInfo:(id)sender;
+(void) syncWorkoutInfo:(id)sender;
+(void) syncAllDailyStats:(id)sender;
+(void) syncExercisePersonalRecordEntry:(NSMutableArray *) allObjectsNew skip:(NSInteger ) skip limit:(NSInteger ) limit;
+(void) markRoutineAsInActive:(NSString *)routineName;
+(void) markWorkoutAsInActive:(NSString *)routineName workoutName:(NSString *) wkName;
+(void) markExerciseAsInActive:(NSString *) routineName workoutName:(NSString *) wkName exerciseName:(NSString *)exName;


// In App Purchase Store
+(BOOL) downloadIAPRoutineFromStore:(NSString *) routineBundleId view:(id) sender;
+(BOOL) isEditingAllowed:(NSString *) routineName;

//Methods for Signup
+(void) createUserProfileOnSignUp:(id) sender;


// Methods for syncing to the cloud
+(BOOL) showWorkoutFinishButton: (NSString *) date;
+(void) syncExerciseSetToParse: (NSString *) date;
+(void) addExerciseSetToParse: (ExerciseSet *) exerciseObj;
+(void) deleteExerciseSetToParse: (NSString *) date timeStamp:(NSString *) timestamp;
+(void) updateExerciseSetToParse: (ExerciseSet *) exerciseObj;
+(void) syncDropSetToParse: (NSString *) date;

+(NSString *) exportAllDataForRoutine:(NSString *) routineName;

// Cleanup
+(void) logout;

+(UIColor *) getMajorMuscleColor: (NSString *) muscle;
+(UIColor *) getMajorMuscleColorSelected: (NSString *) muscle;

+(NSArray *) getAllExerciseForMuscle:(NSString *) muscle;
+(NSArray *) getAllExerciseForMajorMuscle:(NSString *) muscle;

//+(void) addExerciseToWorkout:(ExerciseList *) exercise date:(NSString *) date routineN:(NSString *) routineName workoutN:(NSString *) workoutName;
+(BOOL) addExerciseToWorkout:(ExerciseList *) exercise date:(NSString *) date routineN:(NSString *)routineName workoutN:(NSString *)workoutName exerciseGroup:(NSNumber *) exGroup exerciseNumInGroup:(NSNumber *) exNumInGrp  customSet:(int) cSets customRep:(int) cRep customRest:(int) cRest repsPerSet:(NSString *) repsPSet restPerSet:(NSString *) restPSet;

+(NSArray *) getAllExerciseForMuscleAndEquipment: (NSString *) muscle equip:(NSString*) equipment;
+(void) createWorkoutListForExerciseOnDate:(ExerciseList *) exercise date:(NSString *) date workoutInfo:(UserWorkout *) wkInfo;
+(void) addWorkoutListForExerciseOnDate:(ExerciseList *) exercise date:(NSString *) date workoutInfo:(UserWorkout *) wkInfo exerciseNumber :(int) exNumber maxExerciseGroup:(int) maxExGroup;

+(NSArray *) getAllExerciseForDate:(NSString*) date;
+(NSArray *) getAllExerciseForDateByExGroup:(NSString*) date;

+(NSArray *) getSetForExerciseOnDate:(NSString *)exercise date:(NSString *) date;
+(NSArray *) getSetForExerciseOnPreviosDate:(NSString *)exercise date:(NSString *) date  rName:(NSString *) routineName wName:(NSString *) workoutName;
+(NSArray *) getSetsForAllExerciseForDateSortedByTime:(NSString*) date;

+(NSArray *) getSetsForAllExerciseForDate:(NSString*) date;
//+(NSArray *) getAllExerciseForMuscleUserPerformed:(NSString *) muscle;
+(int) getMaxIndexForExerciseSetArray: (NSArray *) exerciseSetArray;
+(NSArray *) getSetForExercise:(NSString *)exercise;
+(ExerciseMetaInfo *) getExerciseMetaInfo: (NSString *) exercise;
+(NSArray *) getWorkoutsDayExercises:(NSString *) routineName workoutN: (NSString *)workoutName;
+(NSMutableArray *) getMuscleList;
+(NSArray *) getSetsForAllExerciseForGivenWorkoutAndRoutine:(NSString*) rName workout:(NSString *) wName;
+(void) getWeightMovedForGivenRoutineAndWorkout:(NSString*) rName workout:(NSString *) wName dateArr:(NSMutableArray **)dateArr weightArray:(NSMutableArray **) weightArr;
+(void) deleteLocalNotificationWithBody: (NSString *) bodyText;
// get UI Icons made
+(UIImage *) getCompleteMark;
+(UIImage *) getIncompleteMark;
+(UIImage *) getStartMark;
+(UIImage *) getDetailedImage;
+(int) getNumberOfGroupsInWorkoutRouting:(NSString *) routineName workoutName: (NSString *) wkName;
+(int) getNumberOfGroupsInCurrentWorkoutForTempExercise:(NSString *) date;
+(void) syncExerciseMetaInfoToParse;
+(void) printAllExMetaInfo;
+(void) addExercisePersonalRecordEntry: (ExerciseMetaInfo *) exerciseObj setInfo: (ExerciseSet *) set setInfo:(WorkoutList *) exerciseSetInfo;
+(void) updateUserWorkoutToSetBasedSystem;
+(void) startWorkoutOnSignup:(NSString *) routineName workoutName:(NSString *) workoutName;

+(NSString *) createRestPerSet:(int) sets rest:(int) rest;
+(NSString *) createRepsPerSet:(int) sets reps:(int) reps;
@end

