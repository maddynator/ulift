//
//  PackageStore.m
//  gyminutes
//
//  Created by Mayank Verma on 7/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "PackageStore.h"
#import "PackageDetailCVC.h"

@interface PackageStore(){
    NSMutableArray *_packProduct;
    NSArray *_packProductDesc;
    NSNumberFormatter * _priceFormatter;
    SKProduct *selectedProduct;
    NSArray *headerArray, *headerArrayIds, *imageArray;
}

@end
@implementation PackageStore

@synthesize collectionView;
@synthesize refreshControl;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.view = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    headerArray = @[@"PREMIUM PACK", @"ANALYTICS PACK", @"WORKOUT PACK", @"ROUTINE PACK", @"DATA PACK"];
    headerArrayIds = @[@IAP_PREMIUM_PACKAGE_ID, @IAP_ANALYTICS_PACKAGE_ID, @IAP_WORKOUT_PACKAGE_ID, @IAP_ROUTINE_PACKAGE_ID, @IAP_DATA_PACKAGE_ID];
    imageArray = @[@"IconIAPPremiumPack", @"IconIAPAnalyticsPack", @"IconIAPWorkoutPack", @"IconIAPRoutinePack", @"IconIAPDataPack"];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:self.view.frame collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    
    [self.view addSubview:collectionView];
    self.title = @"Discover";
    
    _packProduct = [[NSMutableArray alloc] init];
    _packProductDesc = @[@"Unloack all features. No limitations, no ads.",
                         @"All workout analytics at your finger tips. Visualize all your workouts without any restrictions.",
                         @"For serious lifters only. Unlock additional features such as drop set support when recording your workout.",
                         @"Be your own coach. Create unlimited routines and even modify the downloaded routines to fit your needs.",
                         @"Unlimited workout data backup. Never lose your workout progress ever again."];
    selectedProduct = nil;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    
    if ([Utilities connected]) {
        [self reload];
    }
    else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"No Connectivity" subTitle:@"Unable to connect. Please check your network connectivity." closeButtonTitle:@"Ok" duration:0.0f];
    }
    
    [self.refreshControl beginRefreshing];
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];

    
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [headerArray count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)CollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[CollectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    cell.backgroundColor = FlatBlack;
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-5, 5);
    cell.layer.shadowRadius = 2;
    cell.layer.shadowOpacity = 0.5;

    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
//    UIView *containerView = [[UIView alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame) - 20)];
//    containerView.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    
    UILabel *title = [[UILabel alloc] init];
    title.text = [headerArray objectAtIndex:indexPath.row];
    [title setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:20]];
    title.textColor = [UIColor whiteColor];
    title.backgroundColor = [Utilities getAppColor];
    title.textAlignment = NSTextAlignmentCenter;
    [title setClipsToBounds:YES];
//    title.layer.borderWidth= 5;
//    title.layer.borderColor = [UIColor whiteColor].CGColor;
    title.frame = CGRectMake(15, 0, CGRectGetWidth(cell.frame) - 30, 40);
    
    UIImageView *bcImage  = [[UIImageView alloc] init];
    [bcImage setContentMode:UIViewContentModeScaleAspectFit];
    bcImage.backgroundColor = [UIColor clearColor];
    [bcImage setClipsToBounds:YES];
    
    bcImage.frame = CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 90);
    [bcImage setImage:[UIImage imageNamed:[imageArray objectAtIndex:indexPath.row]]];
    
    UILabel *description = [[UILabel alloc] init];
    description.text = [_packProductDesc objectAtIndex:indexPath.row];
    [description setFont:[UIFont fontWithName:@HAL_REG_FONT size:16]];
    description.textColor = [UIColor whiteColor];
    description.numberOfLines = 0;
    description.textAlignment = NSTextAlignmentCenter;
    description.frame = CGRectMake(10, CGRectGetMaxY(bcImage.frame) - 10, CGRectGetWidth(cell.frame) -20, 90);
    //description.backgroundColor = FlatBlue;
    
    UILabel *buyNow = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(description.frame), CGRectGetWidth(cell.frame), 30)];
    buyNow.text = @"Buy Now";
    buyNow.textAlignment = NSTextAlignmentRight;
    [buyNow setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
    buyNow.textColor = [UIColor whiteColor];
    buyNow.numberOfLines = 0;
    
//    containerView.backgroundColor = FlatMint;
//    [containerView addSubview:description];
//    [containerView addSubview:buyNow];
//    [cell.contentView addSubview:containerView];
    [cell.contentView addSubview:title];
    [cell.contentView addSubview:bcImage];
    [cell.contentView addSubview:description];
    
    
    if ([[uLiftIAPHelper sharedInstance] productPurchased:[headerArrayIds objectAtIndex:indexPath.row]]) {
        UILabel *isPurchased = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.frame) - 100, 0, 100, 40)];
        isPurchased.text = @"PURCHASED";
        isPurchased.textAlignment = NSTextAlignmentCenter;
        [isPurchased setFont:[UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:18]];
        
        isPurchased.textColor = FlatWhite;		
        //isPurchased.backgroundColor = FlatMintDark;
        [cell.contentView addSubview:isPurchased];
    }
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString *productID = [headerArrayIds objectAtIndex:indexPath.row];
    
    bool productFound = false;
    NSLog(@"selected item is %@", productID);
    for (SKProduct *item in _packProduct) {
        if ([item.productIdentifier isEqualToString:productID]) {
            selectedProduct = item;
            productFound = true;
            NSLog(@"product found %@", selectedProduct);
            break;
        }
    }
    NSLog(@"product id %@", productID);
    if (productFound) {
        [self performSegueWithIdentifier:@"packageDetailSegue" sender:self];
    }
    
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame) - 20, 200);
}

-(UIEdgeInsets) collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10,5,150,5);
}

// 4
- (void)reload {
    [_packProduct removeAllObjects];
    
    [KVNProgress showWithStatus:@"Loading..."];
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
            //            _products = products;
//            NSLog(@"Products %@", products);
            for (int i = 0; i < [products count]; i++) {
                SKProduct *prod = [products objectAtIndex:i];
                NSLog(@"product id is %@", prod.productIdentifier);
                if ([prod.productIdentifier isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
                    [_packProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_ANALYTICS_PACKAGE_ID]) {
                    [_packProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_WORKOUT_PACKAGE_ID]) {
                    [_packProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_ROUTINE_PACKAGE_ID]) {
                    [_packProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_DATA_PACKAGE_ID]) {
                    [_packProduct addObject:prod];
                } else {
                    NSLog(@"adding routine now...");
                    continue;
                }
            }
            if ([KVNProgress isVisible]) {
                [KVNProgress dismiss];
            }
        }
        [self.refreshControl endRefreshing];
    }];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"packageDetailSegue"]) {
        PackageDetailCVC *destVC = segue.destinationViewController;
        destVC.product = selectedProduct;
        destVC.routineBundleId = selectedProduct.productIdentifier;
    }
    //self.title = @"";
}

@end
