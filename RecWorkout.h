//
//  RecordWorkout.h
//  gyminutes
//
//  Created by Mayank Verma on 3/3/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface RecWorkout : UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, MZTimerLabelDelegate, UIAlertViewDelegate, UITextViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, CNPPopupControllerDelegate, MPCoachMarksViewDelegate, UITextFieldDelegate>
{
    UICollectionView *_collectionView, *exerciseCards;
    UITableView *exerciseSetsView;
}
@property (nonatomic, retain) IBOutlet JVFloatLabeledTextView *weightText;
@property (nonatomic, retain) IBOutlet JVFloatLabeledTextView *repText;
@property (nonatomic, strong) CNPPopupController *popupController, *barbelPopupController;

@property (nonatomic,retain) IBOutlet UIButton *weightPlus;
@property (nonatomic,retain) IBOutlet UIButton *weightMinus;
@property (nonatomic,retain) IBOutlet UIButton *repPlus;
@property (nonatomic,retain) IBOutlet UIButton *repMinus;
@property (nonatomic,retain) IBOutlet UIButton *saveBtn;
@property (nonatomic,retain) IBOutlet UIButton *cllearBtn;
@property (nonatomic,retain) IBOutlet UIButton *prevExBtn;
@property (nonatomic,retain) IBOutlet UIButton *nextExBtn;
@property (nonatomic, retain) IBOutlet HMSegmentedControl *segmentControl;
@property (nonatomic, retain) IBOutlet UILabel *history;

@property (nonatomic,retain) IBOutlet MZTimerLabel *timer;
@property (nonatomic, retain) IBOutlet WorkoutList *exerciseObj;
@property (nonatomic, retain) IBOutlet NSString *date;
@property (nonatomic, retain) IBOutlet NSString *workoutName;
@property (nonatomic, retain) IBOutlet NSString *routineName;
@property (nonatomic, retain) IBOutlet NSNumber *exerciseNumber;
@property (nonatomic, retain) IBOutlet NSNumber *exerciseGroup, *exerciseNumInGroup;
@property (strong, nonatomic) IBOutlet CircleTimer *timeCircle;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;
@property (retain, nonatomic) IBOutlet UILabel *repsPerSet;

@end
