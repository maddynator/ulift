//
//  DBInterface.m
//  gyminutes
//
//  Created by Mayank Verma on 5/10/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "DBInterface.h"
#import "commons.h"

@implementation DBInterface

+(NSString *) convertTime:(int) totalSeconds {
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}
+(NSArray *) sortMyDict:(NSMutableDictionary *) dict{
    NSArray *myArray;
    
    myArray = [dict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    return myArray;
}

+(NSArray *) sortMyDictIncreasing:(NSMutableDictionary *) dict{
    NSArray *myArray;
    
    myArray = [dict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    return myArray;
}

////////////////////////////////////////// ROUTINE //////////////////////////////////////////
+(NSMutableDictionary *) getRoutineDate:(NSArray *)data {
    
    NSArray *order = @[@"Start Date", @"Last Date", @"Duration"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Date" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    
    if ([data count] == 0) {
        [cal_data setObject:@"Not Performed" forKey:@"Start Date"];
        [cal_data setObject:@"Not Performed" forKey:@"Last Date"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Duration"];
    } else {
        
        NSDateFormatter *dateFormatterDate = [[NSDateFormatter alloc] init];
        [dateFormatterDate setDateFormat:@"yyyy-MM-dd"];
        
        ExerciseSet *set = [data objectAtIndex:0];
        [cal_data setObject:set.date forKey:@"Start Date"];
        NSDate *startDate = [dateFormatterDate dateFromString:set.date];
        
        set = [data objectAtIndex:[data count] - 1];
        [cal_data setObject:set.date forKey:@"Last Date"];
        NSDate *lastDate = [dateFormatterDate dateFromString:set.date];
        
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:startDate
                                                              toDate:lastDate
                                                             options:NSCalendarWrapComponents];

        [cal_data setObject:[NSString stringWithFormat:@"%ld Days", [components day]] forKey:@"Duration"];
    }
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}
+(NSMutableDictionary *) getRoutineFavorites:(NSArray *)data {
    
    //#define AT_ROUTINE_FAV_WORKOUT              "AT_ROUTINE_FAV_WORKOUT"
    //#define AT_ROUTINE_FAV_EXERCISE             "AT_ROUTINE_FAV_EXERCISE"
    //#define AT_ROUTINE_FAV_MUSCLE               "AT_ROUTINE_FAV_MUSCLE"
    //#define AT_ROUTINE_FAV_WORKOUT_DAY          "AT_ROUTINE_FAV_WORKOUT_DAY"
    //#define AT_ROUTINE_FAV_WORKOUT_TIME         "AT_ROUTINE_FAV_WORKOUT_TIME"
    //#define AT_ROUTINE_FAV_EQUIPEMENT_TYPE      "AT_ROUTINE_FAV_EQUIPEMENT_TYPE"
    NSArray *order = @[@"Weekday", @"Workout", @"Muscle", @"Exercise"];

    NSMutableDictionary *favWrkDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *favExDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *favMusDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *favWrkDayDict = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
    
    //    NSMutableDictionary *favWrkTimeDict = [[NSMutableDictionary alloc] init];
    //    NSMutableDictionary *favEqTyDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatterDay = [[NSDateFormatter alloc] init];
    [dateFormatterDay setDateFormat:@"EEEE"];
    
    NSDateFormatter *dateFormatterDate = [[NSDateFormatter alloc] init];
    [dateFormatterDate setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:@"Favorites" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        [cal_data setObject:[NSString stringWithFormat:@"None (0 Workouts)"] forKey:@"Weekday"];
        [cal_data setObject:[NSString stringWithFormat:@"0 (0 Workouts)"] forKey:@"Workout"];
        [cal_data setObject:[NSString stringWithFormat:@"0 (0 Sets)"] forKey:@"Muscle"];
        [cal_data setObject:[NSString stringWithFormat:@"0 (0 Sets)"] forKey:@"Exercise"];
        
    } else {
        for (ExerciseSet *workout in data) {
            //NSLog(@"time is %@,%@,%@", workout.exerciseName, workout.date, workout.timeStamp);
            if (workout.workoutName == nil)
                continue;
            
            NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
            ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
            if (temp == nil) {
                //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
                [uniqWorkoutsDict setObject:workout forKey:uniqString];
            }
            
            
            // fav ex
            NSNumber *favExCount = [favExDict objectForKey:workout.exerciseName];
            if (favExCount == nil) {
                favExCount = [NSNumber numberWithInt:1];
            } else {
                favExCount = [NSNumber numberWithInt:[favExCount intValue] + 1];
            }
            [favExDict setObject:favExCount forKey:workout.exerciseName];
            
            
            // fav musc
            NSNumber *favMuscleCount = [favMusDict objectForKey:workout.muscle];
            if (favMuscleCount == nil) {
                favMuscleCount = [NSNumber numberWithInt:1];
            } else {
                favMuscleCount = [NSNumber numberWithInt:[favMuscleCount intValue] + 1];
            }
            [favMusDict setObject:favMuscleCount forKey:workout.muscle];
            
        }
        
        for (id key in uniqWorkoutsDict) {
            ExerciseSet *workout = uniqWorkoutsDict[key];
            // fav workout
            NSNumber *favWrkCount = [favWrkDict objectForKey:workout.workoutName];
            if (favWrkCount == nil) {
                favWrkCount = [NSNumber numberWithInt:1];
            } else {
                favWrkCount = [NSNumber numberWithInt:[favWrkCount intValue] + 1];
            }
            [favWrkDict setObject:favWrkCount forKey:workout.workoutName];
            
            
            //fav workout day
            NSString *dayName = [dateFormatterDay stringFromDate:[dateFormatterDate dateFromString:workout.date]];
            NSNumber *favWrkDayCount = [favWrkDayDict objectForKey:dayName];
            if (favWrkDayCount == nil) {
                favWrkDayCount = [NSNumber numberWithInt:1];
            } else {
                favWrkDayCount = [NSNumber numberWithInt:[favWrkDayCount intValue] + 1];
            }
            [favWrkDayDict setObject:favWrkDayCount forKey:dayName];
            
        }
        
        NSArray *workoutArr = [self sortMyDict:favWrkDict];
        NSArray *exArr = [self sortMyDict:favExDict];
        NSArray *musArr = [self sortMyDict:favMusDict];
        NSArray *dayArr = [self sortMyDict:favWrkDayDict];
        
        
        if ([dayArr count] > 0)
            [cal_data setObject:[NSString stringWithFormat:@"%@ (%@ Workouts)", [dayArr objectAtIndex:0], [favWrkDayDict objectForKey:[dayArr objectAtIndex:0]]] forKey:@"Weekday"];
        
        NSLog(@"weekday are %@", favWrkDayDict);
        if ([workoutArr count] > 0)
            [cal_data setObject:[NSString stringWithFormat:@"%@ (%@ Workouts)", [workoutArr objectAtIndex:0], [favWrkDict objectForKey:[workoutArr objectAtIndex:0]]] forKey:@"Workout"];
        
        if ([musArr count] > 0)
            [cal_data setObject:[NSString stringWithFormat:@"%@ (%@ Sets)", [musArr objectAtIndex:0], [favMusDict objectForKey:[musArr objectAtIndex:0]]] forKey:@"Muscle"];
        
        if ([exArr count] > 0)
            [cal_data setObject:[NSString stringWithFormat:@"%@ (%@ Sets)", [exArr objectAtIndex:0], [favExDict objectForKey:[exArr objectAtIndex:0]]] forKey:@"Exercise"];
    }
    return cal_data;
}

+(NSMutableDictionary *) getRoutineWeekday:(NSArray *)data {
    NSArray *order = @[@"Sun", @"Mon", @"Tue", @"Wed", @"Thu", @"Fri", @"Sat"];
    
    NSMutableDictionary *favWrkDayDict = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
    
    //    NSMutableDictionary *favWrkTimeDict = [[NSMutableDictionary alloc] init];
    //    NSMutableDictionary *favEqTyDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatterDay = [[NSDateFormatter alloc] init];
    [dateFormatterDay setDateFormat:@"EE"];
    
    NSDateFormatter *dateFormatterDate = [[NSDateFormatter alloc] init];
    [dateFormatterDate setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:@"Favorite Weekdays" forKey:AT_NAME_KEY];
    [cal_data setObject:BAR_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        NSArray *weekDayCount = @[@0, @0, @0, @0, @0, @0, @0];
        [cal_data setObject:order forKey:@"xvals"];
        [cal_data setObject:weekDayCount forKey:@"yvals"];
    } else {
        for (ExerciseSet *workout in data) {
            //NSLog(@"time is %@,%@,%@", workout.exerciseName, workout.date, workout.timeStamp);
            if (workout.workoutName == nil)
                continue;
            
            NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
            ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
            if (temp == nil) {
                //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
                [uniqWorkoutsDict setObject:workout forKey:uniqString];
            }
        }
        
        for (id key in uniqWorkoutsDict) {
            ExerciseSet *workout = uniqWorkoutsDict[key];
            
            //fav workout day
            NSString *dayName = [dateFormatterDay stringFromDate:[dateFormatterDate dateFromString:workout.date]];
            NSNumber *favWrkDayCount = [favWrkDayDict objectForKey:dayName];
            if (favWrkDayCount == nil) {
                favWrkDayCount = [NSNumber numberWithInt:1];
            } else {
                favWrkDayCount = [NSNumber numberWithInt:[favWrkDayCount intValue] + 1];
            }
            [favWrkDayDict setObject:favWrkDayCount forKey:dayName];
            
        }
        NSMutableArray *weekDayCount = [[NSMutableArray alloc] init];
        
        for (NSString *weekDay in order) {
            if ([favWrkDayDict objectForKey:weekDay] == nil) {
                [weekDayCount addObject:@0];
            } else {
                [weekDayCount addObject:[favWrkDayDict objectForKey:weekDay]];
            }
        }
        
        [cal_data setObject:order forKey:@"xvals"];
        [cal_data setObject:weekDayCount forKey:@"yvals"];
        NSLog(@"weekday are %@", cal_data);
    }
    
    return cal_data;
}
+(NSMutableDictionary *) getRoutineTime:(NSArray *)data {
    NSMutableArray *order = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < 24; i++) {
        NSString *time = nil;
        if (i < 10)
            time = [NSString stringWithFormat:@"0%d", i];
        else
            time = [NSString stringWithFormat:@"%d", i];
        
        [order addObject:time];
    }
    NSMutableDictionary *favWrkTimeDict = [[NSMutableDictionary alloc] init];
    
    NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatterTime = [[NSDateFormatter alloc] init];
    [dateFormatterTime setDateFormat:@"HH:mm:ss"];
    
    NSDateFormatter *dateFormatterDate = [[NSDateFormatter alloc] init];
    [dateFormatterDate setDateFormat:@"yyyy-MM-dd"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:@"Favorite Workout Time" forKey:AT_NAME_KEY];
    [cal_data setObject:BAR_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        NSArray *hourCount = @[@0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0, @0];
        [cal_data setObject:order forKey:@"xvals"];
        [cal_data setObject:hourCount forKey:@"yvals"];
    } else {
        // we need to reverse this as every is sorted current to old. Even timestamp.
        NSArray *reverseSets = [[data reverseObjectEnumerator] allObjects];
        for (ExerciseSet *workout in reverseSets) {
            if (workout.workoutName == nil)
                continue;
            
            NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
            ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
            if (temp == nil) {
                [uniqWorkoutsDict setObject:workout forKey:uniqString];
            }
        }
        NSCalendar *calendar = [NSCalendar currentCalendar];
        for (id key in uniqWorkoutsDict) {
            ExerciseSet *workout = uniqWorkoutsDict[key];
            //fav workout day
            
            NSDateComponents *components = [calendar components:(NSCalendarUnitHour | NSCalendarUnitMinute) fromDate:[dateFormatterTime dateFromString:workout.timeStamp]];
            NSInteger hour = [components hour];
            
            NSString *workoutHour = [NSString stringWithFormat:@"%ld", (long)hour];

            NSNumber *favWrkDayCount = [favWrkTimeDict objectForKey:workoutHour];
            if (favWrkDayCount == nil) {
                favWrkDayCount = [NSNumber numberWithInt:1];
            } else {
                favWrkDayCount = [NSNumber numberWithInt:[favWrkDayCount intValue] + 1];
            }
            [favWrkTimeDict setObject:favWrkDayCount forKey:workoutHour];
            
        }
        NSMutableArray *hourCount = [[NSMutableArray alloc] init];
        for (NSString *hour in order) {
            if ([favWrkTimeDict objectForKey:hour] == nil) {
                [hourCount addObject:@0];
            } else {
                [hourCount addObject:[favWrkTimeDict objectForKey:hour]];
            }
        }

        [cal_data setObject:order forKey:@"xvals"];
        [cal_data setObject:hourCount forKey:@"yvals"];
        NSLog(@" hour is %@", cal_data);
    }
    
    return cal_data;

}
+(NSMutableDictionary *) getRoutineOverallStats:(NSArray *)data {
    float total_wt = 0;
    float total_reps = 0;

    NSArray *order = @[@"Performed Times", @"Total Reps", @"Total Sets", @"Total Volume", @"Total Lift Time", @"Total Rest Time", @"Total Workout Time"];

    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Overall Stats" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    NSString *units = [[Utilities getUnits] lowercaseString];
    if ([data count] == 0) {
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Performed Times"];
        
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSNumber numberWithFloat:0] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", @"sec"] forKey:@"Total Lift Time"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", @"sec"] forKey:@"Total Rest Time"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", @"sec"] forKey:@"Total Workout Time"];
        
    } else {
        NSMutableDictionary *temp = [[NSMutableDictionary alloc] init];
        for (ExerciseSet *set in data) {
            if ([temp objectForKey:set.date] == nil) {
                [temp setObject:[NSNumber numberWithInt:1] forKey:set.date];
            }
            total_wt += [set.rep intValue] * [set.weight floatValue];
            total_reps += [set.rep intValue];
        }
        
        NSMutableDictionary *workoutsDict = [[NSMutableDictionary alloc] init];
        int totalReps = 0;
        int totalSets = (int)[data count];
        int totalRest = 0;
        int totalWorkouts = 0;
        float totalWorkoutTime = 0;
        float totalWeight = 0;
        
        NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
        
        for (ExerciseSet *workout in data) {
            //NSLog(@"time is %@,%@,%@", workout.exerciseName, workout.date, workout.timeStamp);
            
            totalReps += [workout.rep intValue];
            totalWeight += [workout.rep intValue] * [workout.weight floatValue];
            NSNumber *workoutNameCount = [workoutsDict objectForKey:workout.workoutName];
            if (workoutNameCount == nil) {
                workoutNameCount = [NSNumber numberWithInt:1];
            } else {
                workoutNameCount = [NSNumber numberWithInt:[workoutNameCount intValue] + 1];
            }
            
            [workoutsDict setObject:workoutNameCount forKey:workout.workoutName];
            NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
            ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
            if (temp == nil) {
                //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
                [uniqWorkoutsDict setObject:workout forKey:uniqString];
            }
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm:ss";
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        for (id key in uniqWorkoutsDict) {
            ExerciseSet *temp = uniqWorkoutsDict[key];
            if (temp == nil)
                continue;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", temp.date, temp.workoutName];
            NSArray *workoutForEachDay = [data filteredArrayUsingPredicate:predicate];
            
            int workoutLength =  (int)[workoutForEachDay count];
            if (workoutLength > 0) {
                ExerciseSet *end =[workoutForEachDay objectAtIndex:0];
                ExerciseSet *start =[workoutForEachDay objectAtIndex:workoutLength - 1];
                NSDate *endTime = [dateFormatter dateFromString:end.timeStamp];
                NSDate *startTime = [dateFormatter dateFromString:start.timeStamp];
                NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
                //NSLog(@"Time interval for workot %@ is %.1f", key, diff/60);
                totalWorkoutTime += diff;
            } else {
                
            }
            
        }
        
        totalWorkouts = (int)[[uniqWorkoutsDict allKeys] count];
        float totalLiftTime = totalSets * 45;
        totalRest = (totalWorkoutTime - totalLiftTime);
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];
        // Assuming each set is roughly around 45 seconds.

        [cal_data setObject:[NSNumber numberWithInteger:[[temp allKeys] count]] forKey:@"Performed Times"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Total Sets"];
        [cal_data setObject:[self convertTime:totalLiftTime] forKey:@"Total Lift Time"];
        [cal_data setObject:[self convertTime:totalRest] forKey:@"Total Rest Time"];
        [cal_data setObject:[self convertTime:totalWorkoutTime] forKey:@"Total Workout Time"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWeight]], [[Utilities getUnits] lowercaseString]] forKey:@"Total Volume"];
    }
    
    return cal_data;
}

+(NSMutableDictionary *) getRoutineOverallTotalStats:(NSArray *) data {
    float total_sets = 0;
    float total_reps = 0;
    
    NSArray *order = @[@"Total Workouts", @"Total Reps", @"Total Sets", @"Avg. Rep/Workout", @"Avg. Set/Workout"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Routine Stats" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Workouts"];
        [cal_data setObject:[NSNumber numberWithFloat:0] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Sets"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Avg. Rep/Workout"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Avg. Set/Workout"];
    } else {
        NSMutableDictionary *uniqWokouts = [[NSMutableDictionary alloc] init];
        for (UserWorkout *set in data) {
            if ([uniqWokouts objectForKey:set.workoutName] == nil) {
                [uniqWokouts setObject:[NSNumber numberWithInt:1] forKey:set.workoutName];
            }
            NSArray *repsInSet = [set.repsPerSet componentsSeparatedByString:@","];
            for (int i = 0 ; i < repsInSet.count; i++) {
                if ([[repsInSet objectAtIndex:i] isEqualToString:@"MAX"])
                    total_reps += 20;
                else
                    total_reps += [[repsInSet objectAtIndex:i] intValue];
            }
            //total_reps *= [set.sets intValue];
            total_sets += [set.sets intValue];
        }
        int totalWorkouts = (int)[[uniqWokouts allKeys] count];
        [cal_data setObject:[NSNumber numberWithInt:totalWorkouts] forKey:@"Total Workouts"];
        [cal_data setObject:[NSNumber numberWithFloat:total_reps] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:total_sets] forKey:@"Total Sets"];
        [cal_data setObject:[NSNumber numberWithFloat:total_reps/totalWorkouts] forKey:@"Avg. Rep/Workout"];
        [cal_data setObject:[NSNumber numberWithFloat:total_sets/totalWorkouts] forKey:@"Avg. Set/Workout"];
    }
    
    return cal_data;
}

+(NSMutableDictionary *) getRoutineWorkouts:(NSArray *) data {    
    NSMutableDictionary *uniqWokouts = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        if ([uniqWokouts objectForKey:set.workoutName] == nil) {
            [uniqWokouts setObject:[NSNumber numberWithInt:1] forKey:set.workoutName];
        }
    }
    return uniqWokouts;
}
+(NSMutableDictionary *) getRoutinePerWorkoutsEst:(NSArray *) data {
    float total_sets = 0;
    float total_reps = 0;
    
    NSArray *order = @[@"Total Exercises", @"Total Reps", @"Total Sets", @"Workout Time (Est.)"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Exercises"];
        [cal_data setObject:[NSNumber numberWithFloat:0] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Sets"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Workout Time (Est.)"];
    } else {
        float workoutTime = 0;
        for (UserWorkout *set in data) {
            NSArray *repsInSet = [set.repsPerSet componentsSeparatedByString:@","];
            for (int i = 0 ; i < repsInSet.count; i++) {
                if ([[repsInSet objectAtIndex:i] isEqualToString:@"MAX"])
                    total_reps += 20;
                else
                    total_reps += [[repsInSet objectAtIndex:i] intValue];
            }
            //total_reps += [set.reps intValue]  * [set.sets intValue];;
            total_sets += [set.sets intValue];
            workoutTime += [set.restTimer intValue] * [set.sets intValue];
            [cal_data setObject:set.workoutName forKey:AT_NAME_KEY];
        }
        int totalExercise = (int)[data count];
        workoutTime += totalExercise * [Utilities getDefaultRestTimerValueBetweenExercises];
        workoutTime += total_sets * 45;
        
        [cal_data setObject:[NSNumber numberWithInt:totalExercise] forKey:@"Total Exercises"];
        [cal_data setObject:[NSNumber numberWithFloat:total_reps] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:total_sets] forKey:@"Total Sets"];
        [cal_data setObject:[self convertTime:workoutTime] forKey:@"Workout Time (Est.)"];
    }
    
    return cal_data;

}
////////////////////////////////////////// WORKOUT //////////////////////////////////////////
+(NSMutableDictionary *) getWorkoutFavorites:(NSArray *) userSets {
    /*
     
     Name
     First Performed Date
     Last Performed Date
     Times Performed
     Frequency of workout (Weekly/BiWeekly/Monthly)
     Sets
     Total Sets
     Avg Sets
     Reps
     Total Reps
     Avg. Reps
     Volume
     Total Volume
     Avg. Volume
     
     */
    
    NSArray *order = @[@"Name", @"First Completed Date", @"Last Completed Date", @"Times Completed", @"SETS", @"Total Sets", @"Avg. Sets", @"REPS", @"Total Reps", @"Avg. Reps", @"VOLUME", @"Total Volume", @"Avg. Volume"];
    NSString *units = [[Utilities getUnits] lowercaseString];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Favorite" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        [cal_data setObject:@"None" forKey:@"Name"];
        [cal_data setObject:@"N/A"  forKey:@"First Completed Date"];
        [cal_data setObject:@"N/A"  forKey:@"Last Completed Date"];
        [cal_data setObject:@"0"  forKey:@"Times Completed"];
        //[cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:@"0"  forKey:@"Total Sets"];
        [cal_data setObject:@"0"  forKey:@"Avg. Sets"];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps"];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume"];
        
    } else {
        NSMutableDictionary *uniqWorkouts = [[NSMutableDictionary alloc] init];
        
        for (ExerciseSet *set in userSets) {
            if (set.workoutName == nil)
                continue;
            
            NSNumber *value = [uniqWorkouts objectForKey:set.workoutName];
            if (value  == nil) {
                // insert object with value 1
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [uniqWorkouts setObject:value forKey:set.workoutName];
        }
        
        // lets sort them in decreasing order.
        NSArray *myArray = [self sortMyDict:uniqWorkouts];
        NSLog(@"fav workout array %@", myArray);
        NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"workoutName == %@", [myArray objectAtIndex:0]];
        NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
        
        NSMutableDictionary *uniqSubWorkouts = [[NSMutableDictionary alloc] init];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[subArray count];
        for (ExerciseSet *set in subArray) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            if ([uniqSubWorkouts objectForKey:set.date] == nil) {
                [uniqSubWorkouts setObject:[NSNumber numberWithInt:1] forKey:set.date];
            }
        }
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        float totalWorkouts = (float)[[uniqSubWorkouts allKeys] count];
        ExerciseSet *firstSet = [subArray objectAtIndex:[subArray count] - 1];
        ExerciseSet *lastSet = [subArray objectAtIndex:0];
        
        [cal_data setObject:firstSet.workoutName forKey:@"Name"];
        [cal_data setObject:firstSet.date  forKey:@"First Completed Date"];
        [cal_data setObject:lastSet.date  forKey:@"Last Completed Date"];
        [cal_data setObject:[NSString stringWithFormat:@"%.0f times", totalWorkouts]  forKey:@"Times Completed"];
//        [cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets]   forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalSets/totalWorkouts]  forKey:@"Avg. Sets"];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalWorkouts]  forKey:@"Avg. Reps"];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [units lowercaseString]]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt/totalWorkouts]], [units lowercaseString]]  forKey:@"Avg. Volume"];
//        [cal_data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Sets"];
//        [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
//        [cal_data setObject:[NSNumber numberWithFloat:totalWt] forKey:@"Wt. Mvd"];
    }
    
    NSLog(@"workout Favs %@", cal_data);
    return cal_data;

}

+(NSMutableDictionary *) getWorkoutPerWeek: (NSArray *) userSets {
    
    NSArray *order = @[@"Avg. Workout Per Week", @"Max. Workout in a week"];//, @"Max. Workout Week"];
    NSMutableDictionary *weekDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    
    for (ExerciseSet *set in userSets) {
        NSDate *date = [dateFormatter dateFromString:set.date];
        NSInteger workoutYear = [date year];
        if (workoutYear != currentYear)
            continue;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierISO8601];
        NSNumber *weekNumber = [NSNumber numberWithLong:[[calendar components: NSCalendarUnitWeekOfYear fromDate:date] weekOfYear]];
        //NSLog(@"Date: %@ week: %@", set.date, weekNumber);
        
        NSMutableDictionary *value = [weekDict objectForKey:weekNumber];
        NSNumber *workoutPerWeek;
        if (value  == nil) {
            // insert object with value 1
            value = [[NSMutableDictionary alloc] init];
            workoutPerWeek = [NSNumber numberWithInt:1];
        } else {
            
            //value = [NSNumber numberWithInt:[value intValue] + 1];
            workoutPerWeek = [value objectForKey:set.workoutName];
            if (workoutPerWeek == nil) {
                workoutPerWeek = [NSNumber numberWithInt:1];
            } else {
                workoutPerWeek = [NSNumber numberWithInt:[workoutPerWeek intValue] + 1];
            }
            [value setObject:workoutPerWeek forKey:set.workoutName];
        }
        [weekDict setObject:value forKey:weekNumber];
    }
    
    float avgWorkoutPerWeek = 0, maxWorkoutPerWeek = 0, totalWorkouts = 0;
    NSNumber *maxWorkoutWeek = nil;
    for (id key in weekDict) {
        NSMutableDictionary *temp = weekDict[key];
        int count = (int)[[temp allKeys] count];
        if (count >= maxWorkoutPerWeek) {
            maxWorkoutPerWeek = count;
            maxWorkoutWeek = key;
        }
        totalWorkouts += count;
    }
    avgWorkoutPerWeek = totalWorkouts/[[weekDict allKeys] count];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    
    [cal_data setObject:@"Workout in Week" forKey:AT_NAME_KEY];
    [cal_data setObject:order forKey:AT_ORDER];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:[NSString stringWithFormat:@"%.2f", avgWorkoutPerWeek] forKey:@"Avg. Workout Per Week"];
    [cal_data setObject:[NSString stringWithFormat:@"%.0f", maxWorkoutPerWeek] forKey:@"Max. Workout in a week"];
    //[cal_data setObject:maxWorkoutWeek forKey:@"Max. Workout week"];
    // NSLog(@"%@", cal_data);
    return cal_data;
    
}

+(NSMutableDictionary *) getWorkoutPerMonth:(NSArray *)userSets {
    NSArray *order = @[@"Avg. Workout Per Month", @"Max. Workout in a Month"];//, @"Max. Workout Month"];

    NSMutableDictionary *monthDict = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    NSInteger currentYear = [components year];
    
    for (ExerciseSet *set in userSets) {
        NSDate *date = [dateFormatter dateFromString:set.date];
        NSInteger workoutYear = [date year];
        if (workoutYear != currentYear)
            continue;
        
        NSCalendar *calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierISO8601];
        NSNumber *monthNumber = [NSNumber numberWithLong:[[calendar components: NSCalendarUnitMonth fromDate:date] month]];
        //NSLog(@"Date: %@ week: %@", set.date, monthNumber);
        
        NSMutableDictionary *value = [monthDict objectForKey:monthNumber];
        NSNumber *workoutPerWeek;
        if (value  == nil) {
            // insert object with value 1
            value = [[NSMutableDictionary alloc] init];
            workoutPerWeek = [NSNumber numberWithInt:1];
        } else {
            
            //value = [NSNumber numberWithInt:[value intValue] + 1];
            NSString *uniqWorkout = [NSString stringWithFormat:@"%@ %@", set.workoutName, set.date];
            workoutPerWeek = [value objectForKey:uniqWorkout];
            if (workoutPerWeek == nil) {
                workoutPerWeek = [NSNumber numberWithInt:1];
            } else {
                workoutPerWeek = [NSNumber numberWithInt:[workoutPerWeek intValue] + 1];
            }
            [value setObject:workoutPerWeek forKey:uniqWorkout];
        }
        [monthDict setObject:value forKey:monthNumber];
    }
    
    float avgWorkoutPerWeek = 0, maxWorkoutPerWeek = 0, totalWorkouts = 0;
    NSNumber *maxWorkoutWeek = nil;
    for (id key in monthDict) {
        NSMutableDictionary *temp = monthDict[key];
        int count = (int)[[temp allKeys] count];
        if (count >= maxWorkoutPerWeek) {
            maxWorkoutPerWeek = count;
            maxWorkoutWeek = key;
        }
        totalWorkouts += count;
    }
    avgWorkoutPerWeek = totalWorkouts/[[monthDict allKeys] count];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    
    [cal_data setObject:@"Workout in Month (YTD)" forKey:AT_NAME_KEY];
    [cal_data setObject:order forKey:AT_ORDER];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:[NSString stringWithFormat:@"%.2f", avgWorkoutPerWeek] forKey:@"Avg. Workout Per Month"];
    [cal_data setObject:[NSString stringWithFormat:@"%.0f", maxWorkoutPerWeek] forKey:@"Max. Workout in a Month"];
    //[cal_data setObject:maxWorkoutWeek forKey:@"Max. Workout Month"];
    // NSLog(@"%@", cal_data);
    return cal_data;
    
}

+(NSMutableDictionary *) getWorkoutDates: (NSArray *) userSets {
    NSArray *order = @[@"First Workout Name", @"First Workout Date", @"Last Workout Name", @"Last Workout Date"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Workout Dates" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];    
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        
        [cal_data setObject:@"N/A"  forKey:@"First Workout Name"];
        [cal_data setObject:@"N/A"  forKey:@"First Workout Date"];
        
        [cal_data setObject:@"N/A"  forKey:@"Last Workout Name"];
        [cal_data setObject:@"N/A"  forKey:@"Last Workout Date"];
    } else {
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        ExerciseSet *lastSet = [userSets objectAtIndex:0];
        
        [cal_data setObject:firstSet.workoutName  forKey:@"First Workout Name"];
        [cal_data setObject:firstSet.date  forKey:@"First Workout Date"];
        
        [cal_data setObject:lastSet.workoutName  forKey:@"Last Workout Name"];
        [cal_data setObject:lastSet.date  forKey:@"Last Workout Date"];
    }
    
    NSLog(@"workout Favs %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getShortestWorkout:(NSArray *)data {
    /*
     #define AT_SHORTEST_WORKOUT_NAME            "AT_SHORTEST_WORKOUT_NAME"
     #define AT_SHORTEST_WORKOUT_TIME            "AT_SHORTEST_WORKOUT_TIME"
     #define AT_SHORTEST_WORKOUT_DATE            "AT_SHORTEST_WORKOUT_DATE"
     #define AT_SHORTEST_WORKOUT_WEIGHT          "AT_SHORTEST_WORKOUT_WEIGHT"
     #define AT_SHORTEST_WORKOUT_REPS            "AT_SHORTEST_WORKOUT_REPS"
     #define AT_SHORTEST_WORKOUT_SETS            "AT_SHORTEST_WORKOUT_SETS"
     */
    NSArray *order = @[@"Date", @"Workout", @"Duration", @"Reps", @"Sets", @"Volume"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];

    
    if ([data count] == 0) {
        [cal_data setObject:@"Shortest Workout" forKey:AT_NAME_KEY];
        [cal_data setObject:@"N/A" forKey:@"Date"];
        [cal_data setObject:@"N/A" forKey:@"Workout"];
        [cal_data setObject:@"0 sec" forKey:@"Duration"];
        [cal_data setObject:@"0" forKey:@"Reps"];
        [cal_data setObject:@"0" forKey:@"Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@",[[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    } else {
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *uniqWorkoutsLength = [[NSMutableDictionary alloc] init];
    
    for (ExerciseSet *workout in data) {
        if (workout.workoutName == nil)
            continue;
        
        NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
        ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
        if (temp == nil) {
            //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
            [uniqWorkoutsDict setObject:workout forKey:uniqString];
        }
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    for (id key in uniqWorkoutsDict) {
        ExerciseSet *temp = uniqWorkoutsDict[key];
        if (temp == nil)
            continue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", temp.date, temp.workoutName];
        NSArray *workoutForEachDay = [data filteredArrayUsingPredicate:predicate];
        
        int workoutLength =  (int)[workoutForEachDay count];
        //        for (ExerciseSet *set in workoutForEachDay) {
        //            NSLog(@"name %@ Ex %@ time %@", set.workoutName, set.exerciseName, set.timeStamp);
        //        }
        if (workoutLength > 0) {
            ExerciseSet *end =[workoutForEachDay objectAtIndex:0];
            ExerciseSet *start =[workoutForEachDay objectAtIndex:workoutLength - 1];
            NSDate *endTime = [dateFormatter dateFromString:end.timeStamp];
            NSDate *startTime = [dateFormatter dateFromString:start.timeStamp];
            NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
            // NSLog(@"Time interval for workot start: %@ End:%@ %@ is %.1f", start.timeStamp, end.timeStamp, key, diff/60);
            [uniqWorkoutsLength setObject:[NSNumber numberWithFloat:diff] forKey:key];
            
        } else {
            
        }
        
    }
    
    NSArray *shortes = [self sortMyDict:uniqWorkoutsLength];
    
    
    ExerciseSet *shortestWorkout = [uniqWorkoutsDict objectForKey:[shortes objectAtIndex:[shortes count] - 1]];
    NSPredicate *exerPredicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", shortestWorkout.date, shortestWorkout.workoutName];
    NSArray *setList = [data filteredArrayUsingPredicate:exerPredicate];
    float totalReps = 0, totalSets = 0, totalWeight = 0;
    
    totalSets = [setList count];
    
    for (ExerciseSet *set in setList) {
        totalReps += [set.rep intValue];
        totalWeight += [set.rep intValue] * [set.weight floatValue];
    }
    
    int shortestSeconds = [[uniqWorkoutsLength objectForKey:[shortes objectAtIndex:[shortes count] - 1]] intValue];
    int seconds = shortestSeconds % 60;
    int minutes = (shortestSeconds / 60) % 60;
    int hours = shortestSeconds / 3600;
    NSString *time = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];

    [cal_data setObject:@"Shortest Workout" forKey:AT_NAME_KEY];
    [cal_data setObject:shortestWorkout.date forKey:@"Date"];
    [cal_data setObject:shortestWorkout.workoutName forKey:@"Workout"];
    [cal_data setObject:[NSString stringWithFormat:@"%@", time] forKey:@"Duration"];
    [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [cal_data setObject:[NSNumber numberWithFloat:totalSets] forKey:@"Sets"];
    [cal_data setObject:[NSString stringWithFormat:@"%@ %@",[numberformatter stringFromNumber:[NSNumber numberWithFloat: totalWeight]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    }

    return cal_data;
}

+(NSMutableDictionary *) getLongestWorkout:(NSArray *)data {
    /*
     #define AT_LONGEST_WORKOUT_NAME             "AT_LONGEST_WORKOUT_NAME"
     #define AT_LONGEST_WORKOUT_TIME             "AT_LONGEST_WORKOUT_TIME"
     #define AT_LONGEST_WORKOUT_DATE             "AT_LONGEST_WORKOUT_DATE"
     #define AT_LONGEST_WORKOUT_WEIGHT           "AT_LONGEST_WORKOUT_WEIGHT"
     #define AT_LONGEST_WORKOUT_REPS             "AT_LONGEST_WORKOUT_REPS"
     #define AT_LONGEST_WORKOUT_SETS             "AT_LONGEST_WORKOUT_SETS"
     */
    NSArray *order = @[@"Date", @"Workout", @"Duration", @"Reps", @"Sets", @"Volume"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];

    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    

    if ([data count] == 0) {
        [cal_data setObject:@"Longest Workout" forKey:AT_NAME_KEY];
        [cal_data setObject:@"N/A" forKey:@"Date"];
        [cal_data setObject:@"N/A" forKey:@"Workout"];
        [cal_data setObject:@"0 sec" forKey:@"Duration"];
        [cal_data setObject:@"0" forKey:@"Reps"];
        [cal_data setObject:@"0" forKey:@"Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@",[[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
        
    } else {

    NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
    NSMutableDictionary *uniqWorkoutsLength = [[NSMutableDictionary alloc] init];
    
    for (ExerciseSet *workout in data) {
        if (workout.workoutName == nil)
            continue;
        
        NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
        ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
        if (temp == nil) {
            //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
            [uniqWorkoutsDict setObject:workout forKey:uniqString];
        }
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    
    for (id key in uniqWorkoutsDict) {
        ExerciseSet *temp = uniqWorkoutsDict[key];
        if (temp == nil)
            continue;
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", temp.date, temp.workoutName];
        NSArray *workoutForEachDay = [data filteredArrayUsingPredicate:predicate];
        
        int workoutLength =  (int)[workoutForEachDay count];
        //        for (ExerciseSet *set in workoutForEachDay) {
        //            NSLog(@"name %@ Ex %@ time %@", set.workoutName, set.exerciseName, set.timeStamp);
        //        }
        if (workoutLength > 0) {
            ExerciseSet *end =[workoutForEachDay objectAtIndex:0];
            ExerciseSet *start =[workoutForEachDay objectAtIndex:workoutLength - 1];
            NSDate *endTime = [dateFormatter dateFromString:end.timeStamp];
            NSDate *startTime = [dateFormatter dateFromString:start.timeStamp];
            NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
            // NSLog(@"Time interval for workot start: %@ End:%@ %@ is %.1f", start.timeStamp, end.timeStamp, key, diff/60);
            [uniqWorkoutsLength setObject:[NSNumber numberWithFloat:diff] forKey:key];
            
        } else {
            
        }
        
    }
    
    NSArray *longest = [self sortMyDict:uniqWorkoutsLength];
    
    int longestSeconds = [[uniqWorkoutsLength objectForKey:[longest objectAtIndex:0]] intValue];
    
    ExerciseSet *longestWorkout = [uniqWorkoutsDict objectForKey:[longest objectAtIndex:0]];
    NSPredicate *exerPredicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", longestWorkout.date, longestWorkout.workoutName];
    NSArray *setList = [data filteredArrayUsingPredicate:exerPredicate];
    float totalReps = 0, totalSets = 0, totalWeight = 0;
    
    totalSets = [setList count];
    
    for (ExerciseSet *set in setList) {
        totalReps += [set.rep intValue];
        totalWeight += [set.rep intValue] * [set.weight floatValue];
    }
    
    
    int seconds = longestSeconds % 60;
    int minutes = (longestSeconds / 60) % 60;
    int hours = longestSeconds / 3600;
    NSString *time = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    
    NSLog(@"==>%d %d %d %d %@",longestSeconds ,hours, minutes, seconds, time);
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    [cal_data setObject:@"Longest Workout" forKey:AT_NAME_KEY];
    [cal_data setObject:longestWorkout.date forKey:@"Date"];
    [cal_data setObject:longestWorkout.workoutName forKey:@"Workout"];
    [cal_data setObject:[NSString stringWithFormat:@"%@", time] forKey:@"Duration"];
    [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [cal_data setObject:[NSNumber numberWithFloat:totalSets] forKey:@"Sets"];
    [cal_data setObject:[NSString stringWithFormat:@"%@ %@",[numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWeight]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    }
    return cal_data;
}

+(NSMutableDictionary *) getWorkoutStats:(NSArray *) userSets {
    NSArray *order = @[@"No Workouts"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Workout Stats" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];

    if ([userSets count] == 0) {
        [cal_data setObject:@"0"  forKey:@"No Workouts"];
    } else {
        
        NSMutableDictionary *uniqueWorkouts = [[NSMutableDictionary alloc] init];
        for (ExerciseSet *set in userSets) {
            if (set.workoutName == nil)
                continue;
            
            NSNumber *value = [uniqueWorkouts objectForKey:set.workoutName];
            if (value == nil) {
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [uniqueWorkouts setObject:value forKey:set.workoutName];
        }
        
        order = [self sortMyDict:uniqueWorkouts];
        for (id key in uniqueWorkouts) {
            NSMutableDictionary *uniqueWorkoutsDay = [[NSMutableDictionary alloc] init];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"workoutName == %@", key];
            NSArray *items = [userSets filteredArrayUsingPredicate:predicate];
            
            for (ExerciseSet *set in items) {
                NSNumber *num = [uniqueWorkoutsDay objectForKey:set.date];
                if (num == nil) {
                    num = [NSNumber numberWithInt:1];
                    [uniqueWorkoutsDay setObject:num forKey:set.date];
                }
            }
            [cal_data setObject:[NSString stringWithFormat:@"%lu times", (unsigned long)[[uniqueWorkoutsDay allKeys] count]] forKey:key];
        }
     }
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}


+(NSMutableDictionary *) getRoutineAvgWorkoutEst:(NSArray *) data {
    /*
     Returns average
     Reps
     Sets
     Lift Time
     Rest Time
     Workout Time
     */
    
    NSArray *order = @[@"Avg. Reps", @"Avg. Sets", @"Avg. Lift Time", @"Avg. Rest Time", @"Avg. Workout Time", @"Avg. Reps Per Set"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"All Workouts Stats (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:order forKey:AT_ORDER];

    if ([data count] == 0) {
        [cal_data setObject:@"0" forKey:@"Avg. Reps"];
        [cal_data setObject:@"0" forKey:@"Avg. Sets"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Lift Time"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Rest Time"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Workout Time"];
        [cal_data setObject:@"0" forKey:@"Avg. Reps Per Set"];
    } else {
        NSMutableDictionary *workoutsDict = [[NSMutableDictionary alloc] init];
        int totalReps = 0;
        int totalSets = 0;
        int totalRestBetweenSets = 0;
        int totalWorkouts = 0;
        
        NSMutableDictionary *exercise = [[NSMutableDictionary alloc] init];
        
        for (UserWorkout *workout in data) {
            NSNumber *value = [workoutsDict objectForKey:workout.workoutName];
            if (value == nil) {
                value = [NSNumber numberWithInt:[workout.reps intValue] * [workout.sets intValue]];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + [workout.reps intValue]  * [workout.sets intValue]];
            }
            [workoutsDict setObject:value forKey:workout.workoutName];
            
            NSArray *repsInSets = [workout.repsPerSet componentsSeparatedByString:@","];
            for (int i = 0; i < repsInSets.count; i++) {
                if ([[repsInSets objectAtIndex:i] isEqualToString:@"MAX"]) {
                    totalReps += 20;
                } else {
                    totalReps += [[repsInSets objectAtIndex:i] intValue];
                }
            }
            //totalReps += [workout.reps intValue]  * [workout.sets intValue];
            totalRestBetweenSets += [workout.restTimer intValue] * [workout.sets intValue];
            
            NSString *exNameAndWn = [NSString stringWithFormat:@"%@%@", workout.exerciseName, workout.workoutName];
            NSNumber *setsValue = [exercise objectForKey:exNameAndWn];
            if (setsValue == nil) {
                setsValue = [NSNumber numberWithInt:1];
            } else {
                setsValue = [NSNumber numberWithInt:[setsValue intValue] + 1];
            }
            totalSets += [workout.sets intValue];
            [exercise setObject:setsValue forKey:exNameAndWn];
        }
        
        totalWorkouts = (int)[[workoutsDict allKeys] count];
        NSLog(@"Reps er workout is %d, %d, %lu", totalReps, totalRestBetweenSets, (long)[[workoutsDict allKeys] count]);
        NSLog(@"%@", workoutsDict);
        // Assuming each set is roughly around 45 seconds.
        int bwExRestTimer = [Utilities getDefaultRestTimerValueBetweenExercises];
        //  totalRestBetweenSets /= totalWorkouts;
        
        NSNumber *avgReps = [NSNumber numberWithFloat:totalReps/totalWorkouts];
        NSNumber *avgSets = [NSNumber numberWithFloat:totalSets/totalWorkouts];
        NSNumber *avgLiftTime = [NSNumber numberWithFloat:([avgReps intValue] * 45)/totalWorkouts];
        
        // total rest between sets + total rest between exercerise * number of exercise
        NSNumber *avgRestTime = [NSNumber numberWithFloat:(totalRestBetweenSets + bwExRestTimer * (int)[[exercise allKeys] count])/totalWorkouts];
        NSNumber *avgWorkoutTime = [NSNumber numberWithFloat:([avgLiftTime floatValue] + [avgRestTime floatValue])];
        
        
        [cal_data setObject:avgReps forKey:@"Avg. Reps"];
        [cal_data setObject:avgSets forKey:@"Avg. Sets"];
        [cal_data setObject:[self convertTime:[avgLiftTime intValue]] forKey:@"Avg. Lift Time"];
        [cal_data setObject:[self convertTime:[avgRestTime intValue]] forKey:@"Avg. Rest Time"];
        [cal_data setObject:[self convertTime:[avgWorkoutTime intValue]] forKey:@"Avg. Workout Time"];
        [cal_data setObject:[NSNumber numberWithFloat:[avgReps intValue]/[avgSets intValue]] forKey:@"Avg. Reps Per Set"];
    }
    return cal_data;
}

+(NSMutableDictionary *) getRoutineAvgWorkoutAct:(NSArray *) data {
    /*
     Returns average
     Reps
     Sets
     Lift Time
     Rest Time
     Workout Time
     Average Weight Lifted
     */

    NSArray *order = @[@"Avg. Reps", @"Avg. Sets",  @"Avg. Volume", @"Avg. Lift Time", @"Avg. Rest Time", @"Avg. Workout Time", @"Avg. Reps Per Set"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    [cal_data setObject:@"All Workouts Stats (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:order forKey:AT_ORDER];

    if ([data count] == 0) {
        [cal_data setObject:@"0" forKey:@"Avg. Reps"];
        [cal_data setObject:@"0" forKey:@"Avg. Sets"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Lift Time"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Rest Time"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Workout Time"];
        [cal_data setObject:@"0 sec" forKey:@"Avg. Volume"];
        [cal_data setObject:@"0" forKey:@"Avg. Reps Per Set"];
    } else {
        NSMutableDictionary *workoutsDict = [[NSMutableDictionary alloc] init];
        int totalReps = 0;
        int totalSets = (int)[data count];
        int totalRest = 0;
        int totalWorkouts = 0;
        float totalWorkoutTime = 0;
        float totalWeight = 0;
        
        NSMutableDictionary *uniqWorkoutsDict = [[NSMutableDictionary alloc] init];
        
        for (ExerciseSet *workout in data) {
            //NSLog(@"time is %@,%@,%@", workout.exerciseName, workout.date, workout.timeStamp);
            
            totalReps += [workout.rep intValue];
            totalWeight += [workout.rep intValue] * [workout.weight floatValue];
            NSNumber *workoutNameCount = [workoutsDict objectForKey:workout.workoutName];
            if (workoutNameCount == nil) {
                workoutNameCount = [NSNumber numberWithInt:1];
            } else {
                workoutNameCount = [NSNumber numberWithInt:[workoutNameCount intValue] + 1];
            }
            
            [workoutsDict setObject:workoutNameCount forKey:workout.workoutName];
            NSString *uniqString = [NSString stringWithFormat:@"%@%@", workout.workoutName, workout.date];
            ExerciseSet *temp = [uniqWorkoutsDict objectForKey:uniqString];
            if (temp == nil) {
                //NSLog(@"adding workout %@ %@", workout.date, workout.workoutName);
                [uniqWorkoutsDict setObject:workout forKey:uniqString];
            }
        }
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm:ss";
        
        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        
        for (id key in uniqWorkoutsDict) {
            ExerciseSet *temp = uniqWorkoutsDict[key];
            if (temp == nil)
                continue;
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@ AND workoutName == %@", temp.date, temp.workoutName];
            NSArray *workoutForEachDay = [data filteredArrayUsingPredicate:predicate];
            
            int workoutLength =  (int)[workoutForEachDay count];
            if (workoutLength > 0) {
                ExerciseSet *end =[workoutForEachDay objectAtIndex:0];
                ExerciseSet *start =[workoutForEachDay objectAtIndex:workoutLength - 1];
                NSDate *endTime = [dateFormatter dateFromString:end.timeStamp];
                NSDate *startTime = [dateFormatter dateFromString:start.timeStamp];
                NSTimeInterval diff = [endTime timeIntervalSinceDate:startTime];
                //NSLog(@"Time interval for workot %@ is %.1f", key, diff/60);
                totalWorkoutTime += diff;
            } else {
                
            }
            
        }
        
        totalWorkouts = (int)[[uniqWorkoutsDict allKeys] count];
        float totalLiftTime = totalSets * 45;
        totalRest = (totalWorkoutTime - totalLiftTime);
        
//        NSLog(@"total workouts: %d UW: %d, R: %d, S: %d LT: %1.f, RT: %d, WT: %.1f WL: %.1f", totalWorkouts, (int)[[uniqWorkoutsDict allKeys] count], totalReps, totalSets, (totalWorkoutTime - totalReps), totalRest,totalWorkoutTime, totalWeight );
        // Assuming each set is roughly around 45 seconds.
        NSNumber *avgReps = [NSNumber numberWithFloat:totalReps/totalWorkouts];
        NSNumber *avgSets = [NSNumber numberWithFloat:totalSets/totalWorkouts];
        NSNumber *avgLiftTime = [NSNumber numberWithFloat:totalLiftTime/totalWorkouts];
        
        // total rest between sets + total rest between exercerise * number of exercise
        NSNumber *avgRestTime = [NSNumber numberWithFloat:(totalRest)/totalWorkouts];
        NSNumber *avgWorkoutTime = [NSNumber numberWithFloat:(totalWorkoutTime)/totalWorkouts];
        NSNumber *avgWeightLifted = [NSNumber numberWithFloat:totalWeight/totalWorkouts];
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:avgReps forKey:@"Avg. Reps"];
        [cal_data setObject:avgSets forKey:@"Avg. Sets"];
        [cal_data setObject:[self convertTime:[avgLiftTime intValue]] forKey:@"Avg. Lift Time"];
        [cal_data setObject:[self convertTime:[avgRestTime intValue]] forKey:@"Avg. Rest Time"];
        [cal_data setObject:[self convertTime:[avgWorkoutTime intValue]] forKey:@"Avg. Workout Time"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:avgWeightLifted], [[Utilities getUnits] lowercaseString]] forKey:@"Avg. Volume"];
        [cal_data setObject:[NSNumber numberWithFloat:[avgReps intValue]/[avgSets intValue]] forKey:@"Avg. Reps Per Set"];
    }
    return cal_data;
    
}


/*
 name
 fcd
 lcd
 sets
    totalsets
    maxets
    datatype: LIST_AND_LINE_CHART
 
    charts: {dates,totalsets,maxsets}
{
 "Name": "Workout Name"
 "First Completed Date" : "Date"
 "Last Completed Date"  : "Date"
 "Times Completed"      : "Times"
 "Sets": {
        "List Count"    : "number of lines",
        "chart count"   : "number of charts",
        "LIST"          : [
                            "Total Sets"    : total sets,
                            "Avg. Sets"     : avg. sets,
                        ]
        "CHART"         : [
                            xvals: nsarray,
                            yvals1: nsarray,
                            yvals2: nsarray
 
                            ]
        "Sets Progress" : {
                            "Total Sets": "Total set of workout",
                            "Max   Sets": "Maximum number of set is workout",
                            "Total Sets Per Workout" : [
                                {"Date" : "date", "set count" },
                                {"Date" : "date", "set count" },
                                {"Date" : "date", "set count" }
                                    ],
                            "Max Sets Per Workout" : [
                                {"Date" : "date", "Max set count" },
                                {"Date" : "date", "Max set count" },
                                {"Date" : "date", "Max set count" }
                                    ]
                            },
        "Reps Progress" : {
                            "Total Reps": "Total rep of workout",
                            "Max   Reps": "Maximum number of rep in workout",
                            "Total Reps Per Workout"" : [
                                {"Date" : "date", "Reps count" },
                                {"Date" : "date", "Reps count" },
                                {"Date" : "date", "Reps count" }
                                    ],
                            "Max Sets Per Workout"" :  [
                                {"Date" : "date", "Max Reps count" },
                                {"Date" : "date", "Max Reps count" },
                                {"Date" : "date", "Max Reps count" }
                                    ]
                            },
 
        "Volume Progress" : {
                            "Total Volume": "Total volume of workout",
                            "Total Volume Per Workout"" : [
                                {"Date" : "date", "Volume" },
                                {"Date" : "date", "Volume" },
                                {"Date" : "date", "Volume" }
                                    ]
                            },
        }
}
 
 */
+(NSMutableDictionary *) getPerWorkoutStats:(NSArray *) userSets {
    NSArray *order = @[@"Name", @"First Completed Date", @"Last Completed Date", @"Times Completed", @"SETS", @"Total Sets", @"Avg. Sets", SETS_PROGRESS, @"REPS", @"Total Reps", @"Avg. Reps", REPS_PROGRESS, @"VOLUME", @"Total Volume", @"Avg. Volume", VOL_PROGRESS];
    NSString *units = [[Utilities getUnits] lowercaseString];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST_AND_LINE_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];

        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:@"None" forKey:@"Name"];
        [cal_data setObject:@"N/A"  forKey:@"First Completed Date"];
        [cal_data setObject:@"N/A"  forKey:@"Last Completed Date"];
        [cal_data setObject:@"0"  forKey:@"Times Completed"];
        //[cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:@"0"  forKey:@"Total Sets"];
        [cal_data setObject:@"0"  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:13] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    } else {
//        NSMutableDictionary *uniqWorkouts = [[NSMutableDictionary alloc] init];
//        
//        for (ExerciseSet *set in userSets) {
//            if (set.workoutName == nil)
//                continue;
//            
//            NSNumber *value = [uniqWorkouts objectForKey:set.workoutName];
//            if (value  == nil) {
//                // insert object with value 1
//                value = [NSNumber numberWithInt:1];
//            } else {
//                value = [NSNumber numberWithInt:[value intValue] + 1];
//            }
//            [uniqWorkouts setObject:value forKey:set.workoutName];
//        }
//        
//        // lets sort them in decreasing order.
//        NSArray *myArray = [self sortMyDict:uniqWorkouts];
//        
//        NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"workoutName == %@", [myArray objectAtIndex:0]];
//        NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
        
        NSMutableArray *uniqDates = [[NSMutableArray alloc] init];
        NSMutableDictionary *uniqSubWorkouts = [[NSMutableDictionary alloc] init];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[userSets count];
        for (ExerciseSet *set in userSets) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            if ([uniqSubWorkouts objectForKey:set.date] == nil) {
                [uniqSubWorkouts setObject:[NSNumber numberWithInt:1] forKey:set.date];
                [uniqDates addObject:set.date];
            }
        }
        
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        NSArray *reversedDates =[[uniqDates reverseObjectEnumerator] allObjects];

        for (NSString *key in reversedDates) {
            //NSLog(@"dates are %@", key);
            NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", key];
            NSArray *subItems = [userSets filteredArrayUsingPredicate:datePredicate];
            float totalRp = 0, totalSt = (int)[subItems count], totalWt = 0;            
            for (ExerciseSet *set in subItems) {
                totalRp += [set.rep intValue];
                totalWt += [set.rep intValue] * [set.weight floatValue];
            }
            [xVals addObject:key];
            [yValsReps addObject:[NSNumber numberWithInt:totalRp]];
            [yValsSets addObject:[NSNumber numberWithInt:totalSt]];
            [yValsVol addObject:[NSNumber numberWithFloat:totalWt]];
        }

        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        float totalWorkouts = (float)[[uniqSubWorkouts allKeys] count];
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        ExerciseSet *lastSet = [userSets objectAtIndex:0];
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:firstSet.workoutName forKey:AT_NAME_KEY];
        [cal_data setObject:lastSet.workoutName forKey:@"Name"];
        [cal_data setObject:firstSet.date  forKey:@"First Completed Date"];
        [cal_data setObject:lastSet.date  forKey:@"Last Completed Date"];
        [cal_data setObject:[NSString stringWithFormat:@"%.0f times", totalWorkouts]  forKey:@"Times Completed"];
        //        [cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets]   forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalSets/totalWorkouts]  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalWorkouts]  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], units]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt/totalWorkouts]], [units lowercaseString]]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:11] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    }
    return cal_data;

}
////////////////////////////////////////// MUSCLE //////////////////////////////////////////
+(NSMutableDictionary *) getMuscleFavoriteForRoutine:(NSArray *) userSets {
    NSArray *order = @[@"Name", @"Total Reps",  @"Total Sets", @"Total Volume"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:order forKey:AT_ORDER];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:@"Favorite" forKey:AT_NAME_KEY];

    if ([userSets count] == 0) {
        [cal_data setObject:@"None" forKey:@"Name"];
        [cal_data setObject:@"0" forKey:@"Total Sets"];
        [cal_data setObject:@"0" forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@" 0 %@", [[Utilities getUnits] lowercaseString]] forKey:@"Total Volume"];
    } else {
        NSMutableDictionary *muscleDict = [[NSMutableDictionary alloc] init];
        for (ExerciseSet *set in userSets) {
            NSNumber *value = [muscleDict objectForKey:set.muscle];
            if (value  == nil) {
                // insert object with value 1
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [muscleDict setObject:value forKey:set.muscle];
            //        NSLog(@"Date %@", set.date);
        }
        
//        NSLog(@"Majot muscle Dict:");
//        for (id key in muscleDict) {
//            NSLog(@"Sets: %@ Muscle: %@", muscleDict[key], key);
//        }
        
        NSArray *myArray;
        
        myArray = [muscleDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
            
            if ([obj1 integerValue] < [obj2 integerValue]) {
                
                return (NSComparisonResult)NSOrderedDescending;
            }
            if ([obj1 integerValue] > [obj2 integerValue]) {
                
                return (NSComparisonResult)NSOrderedAscending;
            }
            
            return (NSComparisonResult)NSOrderedSame;
        }];
        
        NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"muscle == %@", [myArray objectAtIndex:0]];
        
        NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[subArray count];
        for (ExerciseSet *set in subArray) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
        }
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:[myArray objectAtIndex:0] forKey:@"Name"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Total Sets"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@",  [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits] lowercaseString]] forKey:@"Total Volume"];
    }
    return cal_data;

}


+(NSMutableDictionary *) getMuscleStatsEst:(NSArray *) data {
    NSArray *muscleGroup = @[@"Abs", @"Biceps", @"Triceps", @"Forearm", @"Back", @"Lats", @"Chest", @"Shoulders", @"Traps", @"Quads", @"Hamstring", @"Calves", @"Glutes"];
    NSArray *order = @[@"None"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Muscle Stats (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];

    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        if (set.muscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.muscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.muscle];
    }
    
    // check for missing muscle group
    for (NSString *muscle in muscleGroup) {
        if ([exerciseDict objectForKey:muscle] == nil) {
            [exerciseDict setObject:[NSNumber numberWithInt:0] forKey:muscle];
        }
    }
    NSArray *myArray = [self sortMyDict:exerciseDict];
    order = myArray;
    
    for (NSString *muscle in myArray) {
        [cal_data setObject:[NSString stringWithFormat:@"%@ Exercises", exerciseDict[muscle]] forKey:muscle];
    }
    
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}
+(NSMutableDictionary *) getMuscleStatsAct:(NSArray *) data {
    NSArray *muscleGroup = @[@"Abs", @"Biceps", @"Triceps", @"Forearm", @"Back", @"Lats", @"Chest", @"Shoulders", @"Traps", @"Quads", @"Hamstring", @"Calves", @"Glutes"];
    NSArray *order = @[@"None"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Muscle Stats (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        if (set.muscle == nil)
            continue;
        
        NSString *uniq = [NSString stringWithFormat:@"%@ %@", set.exerciseName, set.muscle];
        NSString *value = [exerciseDict objectForKey:uniq];
        if (value  == nil) {
            // insert object with value 1
            value = set.muscle;
            [exerciseDict setObject:value forKey:uniq];
        }
    }
    NSMutableDictionary *muscleDict = [[NSMutableDictionary alloc] init];
    for (id key in exerciseDict) {
        NSString *muscle = exerciseDict[key];
        NSNumber *value = [muscleDict objectForKey:muscle];
        if (value == nil) {
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [muscleDict setObject:value forKey:muscle];
    }
    
    for (NSString *muscle in muscleGroup) {
        if ([muscleDict objectForKey:muscle] == nil) {
            [muscleDict setObject:[NSNumber numberWithInt:0] forKey:muscle];
        }
    }

    NSArray *myArray = [self sortMyDict:muscleDict];
    order = myArray;
    NSLog(@"data %@", muscleDict);
    for (NSString *muscle in myArray) {
        [cal_data setObject:[NSString stringWithFormat:@"%@ Exercises", muscleDict[muscle]] forKey:muscle];
    }
    
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}
+(NSMutableDictionary *) getMajorMuscleDistributionOfRoutineEst:(NSArray *) data {
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        if (set.majorMuscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.majorMuscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.majorMuscle];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Major Muscle Distribution (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    [cal_data setObject:[NSNumber numberWithBool:true] forKey:AT_NAME_MAJOR_MUSCLE];
    for (NSString  *key in exerciseDict) {
        NSLog(@"exercise name is %@", key);
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    NSLog(@"exercise dict count is %lu", [exerciseDict count]);
    
    // NSLog(@"Major Muscle Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getMuscleDistributionOfRoutineEst:(NSArray *) data  {
    
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        if (set.muscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.muscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.muscle];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Muscle Distribution (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    [cal_data setObject:[NSNumber numberWithBool:true] forKey:AT_NAME_MUSCLE];
    // NSLog(@"Muscle Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getFrontToBackDistributionOfRoutineEst:(NSArray *) data  {
    NSString *front = @"front", *back = @"back";
    
    NSMutableDictionary *f2bMuscleGroup = [[NSMutableDictionary alloc] init];
    [f2bMuscleGroup setObject:front forKey:@"Abs"];
    [f2bMuscleGroup setObject:front forKey:@"Biceps"];
    [f2bMuscleGroup setObject:back forKey:@"Triceps"];
    [f2bMuscleGroup setObject:front forKey:@"Forearm"];
    [f2bMuscleGroup setObject:back forKey:@"Back"];
    [f2bMuscleGroup setObject:back forKey:@"Lats"];
    [f2bMuscleGroup setObject:front forKey:@"Chest"];
    [f2bMuscleGroup setObject:front forKey:@"Shoulders"];
    [f2bMuscleGroup setObject:back forKey:@"Traps"];
    [f2bMuscleGroup setObject:front forKey:@"Quads"];
    [f2bMuscleGroup setObject:back forKey:@"Hamstring"];
    [f2bMuscleGroup setObject:back forKey:@"Calves"];
    [f2bMuscleGroup setObject:back forKey:@"Glutes"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Front-to-Back Distribution (Est.)" forKey:AT_NAME_KEY];
    
    int total = (int) [data count];
    
    for (UserWorkout *workout in data) {
        NSString *muscleCategory = [f2bMuscleGroup objectForKey:workout.muscle];
        NSNumber *value = [cal_data objectForKey:muscleCategory];
        
        if (value == nil) {
            [cal_data setObject:[NSNumber numberWithInteger:1] forKey:muscleCategory];
        } else {
            if ([muscleCategory isEqualToString:front]) {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:front];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:back];
            }
        }
    }
    
    float front_total = [[cal_data objectForKey:front] floatValue];
    float back_total = [[cal_data objectForKey:back] floatValue];
    
    [cal_data setObject:[NSNumber numberWithFloat:(front_total/total * 100)] forKey:front];
    [cal_data setObject:[NSNumber numberWithFloat:(back_total/total * 100)] forKey:back];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];

    //    NSLog(@"front to back %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getUpperToBottomDistributionOfRoutineEst:(NSArray *) data
{
    NSString *upper = @"upper", *lower = @"lower";
    
    NSMutableDictionary *u2lMuscleGroup = [[NSMutableDictionary alloc] init];
    [u2lMuscleGroup setObject:upper forKey:@"Abs"];
    [u2lMuscleGroup setObject:upper forKey:@"Biceps"];
    [u2lMuscleGroup setObject:upper forKey:@"Triceps"];
    [u2lMuscleGroup setObject:upper forKey:@"Forearm"];
    [u2lMuscleGroup setObject:upper forKey:@"Back"];
    [u2lMuscleGroup setObject:upper forKey:@"Lats"];
    [u2lMuscleGroup setObject:upper forKey:@"Chest"];
    [u2lMuscleGroup setObject:upper forKey:@"Shoulders"];
    [u2lMuscleGroup setObject:upper forKey:@"Traps"];
    [u2lMuscleGroup setObject:lower forKey:@"Quads"];
    [u2lMuscleGroup setObject:lower forKey:@"Hamstring"];
    [u2lMuscleGroup setObject:lower forKey:@"Calves"];
    [u2lMuscleGroup setObject:lower forKey:@"Glutes"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Upper-to-Lower Distribution (Est.)" forKey:AT_NAME_KEY];
    
    int total = (int) [data count];
    
    for (UserWorkout *workout in data) {
        NSString *muscleCategory = [u2lMuscleGroup objectForKey:workout.muscle];
        NSNumber *value = [cal_data objectForKey:muscleCategory];
        
        if (value == nil) {
            [cal_data setObject:[NSNumber numberWithInteger:1] forKey:muscleCategory];
        } else {
            if ([muscleCategory isEqualToString:upper]) {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:upper];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:lower];
            }
        }
    }
    
    float upper_total = [[cal_data objectForKey:upper] floatValue];
    float lower_total = [[cal_data objectForKey:lower] floatValue];
    
    [cal_data setObject:[NSNumber numberWithFloat:(upper_total/total * 100)] forKey:upper];
    [cal_data setObject:[NSNumber numberWithFloat:(lower_total/total * 100)] forKey:lower];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];

    //  NSLog(@"upper to lower %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getMajorMuscleDistributionOfRoutineAct:(NSArray *) data {
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        if (set.majorMuscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.majorMuscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.majorMuscle];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Major Muscle Distribution (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    [cal_data setObject:[NSNumber numberWithBool:true] forKey:AT_NAME_MAJOR_MUSCLE];

    // NSLog(@"Major Muscle Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getMuscleDistributionOfRoutineAct:(NSArray *) data  {
    
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        if (set.muscle == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.muscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.muscle];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Muscle Distribution  (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    [cal_data setObject:[NSNumber numberWithBool:true] forKey:AT_NAME_MUSCLE];

    // NSLog(@"Muscle Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getFrontToBackDistributionOfRoutineAct:(NSArray *) data  {
    NSString *front = @"front", *back = @"back";
    
    NSMutableDictionary *f2bMuscleGroup = [[NSMutableDictionary alloc] init];
    [f2bMuscleGroup setObject:front forKey:@"Abs"];
    [f2bMuscleGroup setObject:front forKey:@"Biceps"];
    [f2bMuscleGroup setObject:back forKey:@"Triceps"];
    [f2bMuscleGroup setObject:front forKey:@"Forearm"];
    [f2bMuscleGroup setObject:back forKey:@"Back"];
    [f2bMuscleGroup setObject:back forKey:@"Lats"];
    [f2bMuscleGroup setObject:front forKey:@"Chest"];
    [f2bMuscleGroup setObject:front forKey:@"Shoulders"];
    [f2bMuscleGroup setObject:back forKey:@"Traps"];
    [f2bMuscleGroup setObject:front forKey:@"Quads"];
    [f2bMuscleGroup setObject:back forKey:@"Hamstring"];
    [f2bMuscleGroup setObject:back forKey:@"Calves"];
    [f2bMuscleGroup setObject:back forKey:@"Glutes"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Front-to-Back Distribution (Act.)" forKey:AT_NAME_KEY];
    
    int total = (int) [data count];
    
    for (ExerciseSet *workout in data) {
        NSString *muscleCategory = [f2bMuscleGroup objectForKey:workout.muscle];
        NSNumber *value = [cal_data objectForKey:muscleCategory];
        
        if (value == nil) {
            [cal_data setObject:[NSNumber numberWithInteger:1] forKey:muscleCategory];
        } else {
            if ([muscleCategory isEqualToString:front]) {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:front];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:back];
            }
        }
    }
    
    float front_total = [[cal_data objectForKey:front] floatValue];
    float back_total = [[cal_data objectForKey:back] floatValue];
    
    [cal_data setObject:[NSNumber numberWithFloat:(front_total/total * 100)] forKey:front];
    [cal_data setObject:[NSNumber numberWithFloat:(back_total/total * 100)] forKey:back];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];

    //    NSLog(@"front to back %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getUpperToBottomDistributionOfRoutineAct:(NSArray *) data
{
    NSString *upper = @"upper", *lower = @"lower";
    
    NSMutableDictionary *u2lMuscleGroup = [[NSMutableDictionary alloc] init];
    [u2lMuscleGroup setObject:upper forKey:@"Abs"];
    [u2lMuscleGroup setObject:upper forKey:@"Biceps"];
    [u2lMuscleGroup setObject:upper forKey:@"Triceps"];
    [u2lMuscleGroup setObject:upper forKey:@"Forearm"];
    [u2lMuscleGroup setObject:upper forKey:@"Back"];
    [u2lMuscleGroup setObject:upper forKey:@"Lats"];
    [u2lMuscleGroup setObject:upper forKey:@"Chest"];
    [u2lMuscleGroup setObject:upper forKey:@"Shoulders"];
    [u2lMuscleGroup setObject:upper forKey:@"Traps"];
    [u2lMuscleGroup setObject:lower forKey:@"Quads"];
    [u2lMuscleGroup setObject:lower forKey:@"Hamstring"];
    [u2lMuscleGroup setObject:lower forKey:@"Calves"];
    [u2lMuscleGroup setObject:lower forKey:@"Glutes"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Upper-to-Lower Distribution  (Act.)" forKey:AT_NAME_KEY];
    
    int total = (int) [data count];
    
    for (ExerciseSet *workout in data) {
        NSString *muscleCategory = [u2lMuscleGroup objectForKey:workout.muscle];
        NSNumber *value = [cal_data objectForKey:muscleCategory];
        
        if (value == nil) {
            [cal_data setObject:[NSNumber numberWithInteger:1] forKey:muscleCategory];
        } else {
            if ([muscleCategory isEqualToString:upper]) {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:upper];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
                [cal_data setObject:value forKey:lower];
            }
        }
    }
    
    float upper_total = [[cal_data objectForKey:upper] floatValue];
    float lower_total = [[cal_data objectForKey:lower] floatValue];
    
    [cal_data setObject:[NSNumber numberWithFloat:(upper_total/total * 100)] forKey:upper];
    [cal_data setObject:[NSNumber numberWithFloat:(lower_total/total * 100)] forKey:lower];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];

    //  NSLog(@"upper to lower %@", cal_data);
    return cal_data;
}

/////////////////////////////////////////  EXERCISE  /////////////////////////////////////////
+(NSMutableDictionary *) getExerciseFavoriteForRoutine:(NSArray *) data {
    NSArray *order = @[@"Exercise", @"Total Reps", @"Total Sets", @"Total Volume"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Favorite" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        [cal_data setObject:@"None" forKey:@"Exercise"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:0] forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", [[Utilities getUnits] lowercaseString]] forKey:@"Total Volume"];
        
    } else {
        NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
        for (ExerciseSet *set in data) {
            
            NSNumber *value = [exerciseDict objectForKey:set.exerciseName];
            if (value  == nil) {
                // insert object with value 1
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [exerciseDict setObject:value forKey:set.exerciseName];
        }
        NSArray *myArray = [self sortMyDict:exerciseDict];
        
        
        float totalReps = 0, totalSets = 0, totalWt = 0;
        if ([myArray count] > 0) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", [myArray objectAtIndex:0]];
            NSArray *subArray = [data filteredArrayUsingPredicate:exPredicate];
            totalSets = [subArray count];
            for (ExerciseSet *set in subArray) {
                totalReps += [set.rep intValue];
                totalWt += [set.rep intValue] * [set.weight floatValue];
            }
            NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
            [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:[myArray objectAtIndex:0] forKey:@"Exercise"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Total Reps"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits] lowercaseString]] forKey:@"Total Volume"];
        }
    }
    
    // NSLog(@"Exercise Equipment Distribution: %@", cal_data);
    return cal_data;

}
+(NSMutableDictionary *) getExerciseStatsEst:(NSArray *) data  {
    NSArray *order = @[@"None"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Stats (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    
    if ([data count] == 0) {
        [cal_data setObject:@"None" forKey:@"Exercise"];
    } else {
        NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
        for (UserWorkout *set in data) {
            
            NSNumber *value = [exerciseDict objectForKey:set.exerciseName];
            if (value  == nil) {
                // insert object with value 1
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [exerciseDict setObject:value forKey:set.exerciseName];
        }
        
        NSArray *myArray = [self sortMyDict:exerciseDict];
        order = myArray;
        
        for (NSString *exercise in myArray) {
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise];
            NSArray *subArray = [data filteredArrayUsingPredicate:exPredicate];
            float totalSets = 0;
            for (UserWorkout *set in subArray) {
                totalSets += [set.sets intValue];
            }
            [cal_data setObject:[NSString stringWithFormat:@"%.0f Sets", totalSets] forKey:exercise];
        }
    }
    [cal_data setObject:order forKey:AT_ORDER];

    return cal_data;
}

+(NSMutableDictionary *) getExerciseStatsAct:(NSArray *) data  {
    NSArray *order = @[@"None"];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Stats (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:LIST forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([data count] == 0) {
        [cal_data setObject:@"None" forKey:@"Exercise"];
    } else {
        NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
        for (ExerciseSet *set in data) {
            
            NSNumber *value = [exerciseDict objectForKey:set.exerciseName];
            if (value  == nil) {
                // insert object with value 1
                value = [NSNumber numberWithInt:1];
            } else {
                value = [NSNumber numberWithInt:[value intValue] + 1];
            }
            [exerciseDict setObject:value forKey:set.exerciseName];
        }
        NSArray *myArray = [self sortMyDict:exerciseDict];
        order = myArray;
        
        for (NSString *exercise in myArray) {
            [cal_data setObject:[NSString stringWithFormat:@"%@ Sets", exerciseDict[exercise]] forKey:exercise];
        }
    }
    [cal_data setObject:order forKey:AT_ORDER];
    return cal_data;
}

+(NSMutableDictionary *) getExerciseEquipmentDistributionOfRoutineEst:(NSArray *) data  {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    
    for (UserWorkout *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.equipment];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.equipment];
    }
    NSLog(@"Exercise Dict %@", exerciseDict);
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Equipement Distribution (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    // NSLog(@"Exercise Equipment Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getExerciseEquipmentDistributionOfRoutineAct:(NSArray *) data  {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    
    for (ExerciseSet *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.equipment];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.equipment];
    }
    NSLog(@"Exercise Dict %@", exerciseDict);
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Equipement Distribution (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    // NSLog(@"Exercise Equipment Distribution: %@", cal_data);
    return cal_data;
}


+(NSMutableDictionary *) getExerciseMechanicsDistributionOfRoutineEst:(NSArray *) data   {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.mechanics];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.mechanics];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Mechanics Distribution (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    //  NSLog(@"Exercise Mechanics Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getExerciseMechanicsDistributionOfRoutineAct:(NSArray *) data   {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.mechanics];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.mechanics];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Mechanics Distribution (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    //  NSLog(@"Exercise Mechanics Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getExerciseTypeDistributionOfRoutineEst:(NSArray *) data  {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (UserWorkout *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.type];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.type];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Type Distribution (Est.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    //  NSLog(@"Exercise Type Distribution: %@", cal_data);
    return cal_data;
}

+(NSMutableDictionary *) getExerciseTypeDistributionOfRoutineAct:(NSArray *) data  {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in data) {
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", set.exerciseName];
        ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:exPredicate inContext:localContext];
        if (exercise == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:exercise.type];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:exercise.type];
    }
    
    int total = (int) [data count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:@"Exercise Type Distribution (Act.)" forKey:AT_NAME_KEY];
    [cal_data setObject:PIE_CHART forKey:DATA_TYPE];
    
    for (NSString  *key in exerciseDict) {
        [cal_data setObject:[NSNumber numberWithFloat:(float)[exerciseDict[key] floatValue]/total * 100] forKey:key];
    }
    
    //  NSLog(@"Exercise Type Distribution: %@", cal_data);
    return cal_data;
}

/*
 name
 fcd
 lcd
 sets
 totalsets
 maxets
 datatype: LIST_AND_LINE_CHART
 
 charts: {dates,totalsets,maxsets}
 {
 "Name": "Workout Name"
 "First Completed Date" : "Date"
 "Last Completed Date"  : "Date"
 "Times Completed"      : "Times"
 "Sets": {
 "List Count"    : "number of lines",
 "chart count"   : "number of charts",
 "LIST"          : [
 "Total Sets"    : total sets,
 "Avg. Sets"     : avg. sets,
 ]
 "CHART"         : [
 xvals: nsarray,
 yvals1: nsarray,
 yvals2: nsarray
 
 ]
 "Sets Progress" : {
 "Total Sets": "Total set of workout",
 "Max   Sets": "Maximum number of set is workout",
 "Total Sets Per Workout" : [
 {"Date" : "date", "set count" },
 {"Date" : "date", "set count" },
 {"Date" : "date", "set count" }
 ],
 "Max Sets Per Workout" : [
 {"Date" : "date", "Max set count" },
 {"Date" : "date", "Max set count" },
 {"Date" : "date", "Max set count" }
 ]
 },
 "Reps Progress" : {
 "Total Reps": "Total rep of workout",
 "Max   Reps": "Maximum number of rep in workout",
 "Total Reps Per Workout"" : [
 {"Date" : "date", "Reps count" },
 {"Date" : "date", "Reps count" },
 {"Date" : "date", "Reps count" }
 ],
 "Max Sets Per Workout"" :  [
 {"Date" : "date", "Max Reps count" },
 {"Date" : "date", "Max Reps count" },
 {"Date" : "date", "Max Reps count" }
 ]
 },
 
 "Volume Progress" : {
 "Total Volume": "Total volume of workout",
 "Total Volume Per Workout"" : [
 {"Date" : "date", "Volume" },
 {"Date" : "date", "Volume" },
 {"Date" : "date", "Volume" }
 ]
 },
 }
 }
 
 */
+(NSMutableDictionary *) getPerMuscleStats:(NSArray *) userSets {
    NSArray *order = @[@"SETS", @"Total Sets", @"Avg. Sets", SETS_PROGRESS, @"REPS", @"Total Reps", @"Avg. Reps", REPS_PROGRESS, @"VOLUME", @"Total Volume", @"Avg. Volume", VOL_PROGRESS];
    NSString *units = [[Utilities getUnits] lowercaseString];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST_AND_LINE_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];

        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];

        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:@"None" forKey:@"Name"];
        //[cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:@"0"  forKey:@"Total Sets"];
        [cal_data setObject:@"0"  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:10] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    } else {
        NSMutableArray *uniqDates = [[NSMutableArray alloc] init];
        NSMutableDictionary *uniqSubWorkouts = [[NSMutableDictionary alloc] init];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[userSets count];
        for (ExerciseSet *set in userSets) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            if ([uniqSubWorkouts objectForKey:set.date] == nil) {
                [uniqSubWorkouts setObject:[NSNumber numberWithInt:1] forKey:set.date];
                [uniqDates addObject:set.date];
            }
        }
        
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        NSArray *reversedDates =[[uniqDates reverseObjectEnumerator] allObjects];
        
        for (NSString *key in reversedDates) {
            NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", key];
            NSArray *subItems = [userSets filteredArrayUsingPredicate:datePredicate];
            float totalRp = 0, totalSt = (int)[subItems count], totalWt = 0;
            for (ExerciseSet *set in subItems) {
                totalRp += [set.rep intValue];
                totalWt += [set.rep intValue] * [set.weight floatValue];
            }
            [xVals addObject:key];
            [yValsReps addObject:[NSNumber numberWithInt:totalRp]];
            [yValsSets addObject:[NSNumber numberWithInt:totalSt]];
            [yValsVol addObject:[NSNumber numberWithFloat:totalWt]];
        }
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        float totalWorkouts = (float)[[uniqSubWorkouts allKeys] count];
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:firstSet.muscle forKey:AT_NAME_KEY];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets]   forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalSets/totalWorkouts]  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalWorkouts]  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [units lowercaseString]]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt/totalWorkouts]], [units lowercaseString]]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:10] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    }
    return cal_data;
    
}

+(NSMutableDictionary *) getPerExerciseStats:(NSArray *) userSets {
    NSArray *order = @[@"SETS", @"Total Sets", @"Avg. Sets", SETS_PROGRESS, @"REPS", @"Total Reps", @"Avg. Reps", REPS_PROGRESS, @"VOLUME", @"Total Volume", @"Avg. Volume", VOL_PROGRESS];
    NSString *units = [[Utilities getUnits] lowercaseString];
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    [cal_data setObject:LIST_AND_LINE_CHART forKey:DATA_TYPE];
    [cal_data setObject:order forKey:AT_ORDER];
    
    if ([userSets count] == 0) {
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        [cal_data setObject:@"None" forKey:AT_NAME_KEY];
        [cal_data setObject:@"None" forKey:@"Name"];
        //[cal_data setObject:@"N/A"  forKey:@"Frequency of workout"];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:@"0"  forKey:@"Total Sets"];
        [cal_data setObject:@"0"  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:@"0"  forKey:@"Total Reps"];
        [cal_data setObject:@"0"  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units] forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"0 %@", units]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:10] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    } else {
        NSMutableArray *uniqDates = [[NSMutableArray alloc] init];
        NSMutableDictionary *uniqSubWorkouts = [[NSMutableDictionary alloc] init];
        float totalSets = 0, totalReps = 0, totalWt = 0;
        totalSets = (int)[userSets count];
        for (ExerciseSet *set in userSets) {
            totalReps += [set.rep intValue];
            totalWt += [set.weight floatValue] * [set.rep intValue];
            if ([uniqSubWorkouts objectForKey:set.date] == nil) {
                [uniqSubWorkouts setObject:[NSNumber numberWithInt:1] forKey:set.date];
                [uniqDates addObject:set.date];
            }
        }
        
        NSMutableDictionary *perWorkoutSets = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutReps = [[NSMutableDictionary alloc] init];
        NSMutableDictionary *perWorkoutWt = [[NSMutableDictionary alloc] init];
        NSMutableArray *xVals = [[NSMutableArray alloc] init];
        NSMutableArray *yValsSets = [[NSMutableArray alloc] init];
        NSMutableArray *yValsReps = [[NSMutableArray alloc] init];
        NSMutableArray *yValsVol = [[NSMutableArray alloc] init];
        
        NSArray *reversedDates =[[uniqDates reverseObjectEnumerator] allObjects];
        
        for (NSString *key in reversedDates) {
            NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", key];
            NSArray *subItems = [userSets filteredArrayUsingPredicate:datePredicate];
            float totalRp = 0, totalSt = (int)[subItems count], totalWt = 0;
            for (ExerciseSet *set in subItems) {
                totalRp += [set.rep intValue];
                totalWt += [set.rep intValue] * [set.weight floatValue];
            }
            [xVals addObject:key];
            [yValsReps addObject:[NSNumber numberWithInt:totalRp]];
            [yValsSets addObject:[NSNumber numberWithInt:totalSt]];
            [yValsVol addObject:[NSNumber numberWithFloat:totalWt]];
        }
        
        [perWorkoutReps setObject:xVals forKey:@"xvals"];
        [perWorkoutSets setObject:xVals forKey:@"xvals"];
        [perWorkoutWt setObject:xVals forKey:@"xvals"];
        
        [perWorkoutReps setObject:yValsReps forKey:@"yvals"];
        [perWorkoutSets setObject:yValsSets forKey:@"yvals"];
        [perWorkoutWt setObject:yValsVol forKey:@"yvals"];
        
        float totalWorkouts = (float)[[uniqSubWorkouts allKeys] count];
        ExerciseSet *firstSet = [userSets objectAtIndex:[userSets count] - 1];
        
        NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
        [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

        [cal_data setObject:firstSet.exerciseName forKey:AT_NAME_KEY];
        [cal_data setObject:@"-1"  forKey:@"SETS"];
        [cal_data setObject:[NSNumber numberWithInt:totalSets]   forKey:@"Total Sets"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalSets/totalWorkouts]  forKey:@"Avg. Sets"];
        [cal_data setObject:perWorkoutSets forKey:SETS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"REPS"];
        [cal_data setObject:[NSNumber numberWithInt:totalReps]  forKey:@"Total Reps"];
        [cal_data setObject:[NSString stringWithFormat:@"%.2f", totalReps/totalWorkouts]  forKey:@"Avg. Reps"];
        [cal_data setObject:perWorkoutReps forKey:REPS_PROGRESS];
        [cal_data setObject:@"-1"  forKey:@"VOLUME"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [units lowercaseString]]  forKey:@"Total Volume"];
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt/totalWorkouts]], [units lowercaseString]]  forKey:@"Avg. Volume"];
        [cal_data setObject:perWorkoutWt forKey:VOL_PROGRESS];
        [cal_data setObject:[NSNumber numberWithInt:10] forKey:LINE_COUNT];
        [cal_data setObject:[NSNumber numberWithInt:3] forKey:CHART_COUNT];
        
    }
    return cal_data;

}
+(NSMutableDictionary *) getMostWorkedOutMuscle: (NSArray *) userSets {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    if ([userSets count] == 0) {
        [data setObject:@"None" forKey:@"Name"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Sets"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Reps"];
        [data setObject:[NSString stringWithFormat:@"0 %@", [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    } else {
    // muscle group with most sets.
    NSLog(@"%lu", [userSets count]);
    NSMutableDictionary *muscleDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in userSets) {
        NSNumber *value = [muscleDict objectForKey:set.muscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [muscleDict setObject:value forKey:set.muscle];
        //        NSLog(@"Date %@", set.date);
    }
    
//    NSLog(@"Majot muscle Dict:");
//    for (id key in muscleDict) {
//        NSLog(@"Sets: %@ Muscle: %@", muscleDict[key], key);
//    }
    
    NSArray *myArray;
    
    myArray = [muscleDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] > [obj2 integerValue]) {
            
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"muscle == %@", [myArray objectAtIndex:0]];
    
    NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
    float totalSets = 0, totalReps = 0, totalWt = 0;
    totalSets = (int)[subArray count];
    for (ExerciseSet *set in subArray) {
        totalReps += [set.rep intValue];
        totalWt += [set.weight floatValue] * [set.rep intValue];
    }
    
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    [data setObject:[myArray objectAtIndex:0] forKey:@"Name"];
    [data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Sets"];
    [data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];

    }
    return data;
}


+(NSMutableDictionary *)  getMostWorkedOutMajorMuscle: (NSArray *) userSets {
    NSMutableDictionary *majorMuscleDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in userSets) {
        NSNumber *value = [majorMuscleDict objectForKey:set.majorMuscle];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [majorMuscleDict setObject:value forKey:set.majorMuscle];
    }
    
//    NSLog(@"Majot muscle Dict:");
//    for (id key in majorMuscleDict) {
//        NSLog(@"Sets: %@ Muscle: %@", majorMuscleDict[key], key);
//    }
    
    // lets sort them in decreasing order.
    NSArray *myArray;
    myArray = [majorMuscleDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    NSLog(@"%@", myArray);
    NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"majorMuscle == %@", [myArray objectAtIndex:0]];
    
    NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
    float totalSets = 0, totalReps = 0, totalWt = 0;
    totalSets = (int)[subArray count];
    for (ExerciseSet *set in subArray) {
        totalReps += [set.rep intValue];
        totalWt += [set.weight floatValue] * [set.rep intValue];
    }

    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:[myArray objectAtIndex:0] forKey:@"Name"];
    [data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Sets"];
    [data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    
    return data;
 }


+(NSMutableDictionary *) getMostWorkedOutExercise: (NSArray *) userSets {
    
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    if ([userSets count] == 0) {
        [data setObject:@"None" forKey:@"Name"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Sets"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Reps"];
        [data setObject:[NSString stringWithFormat:@"0 %@", [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    } else {
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in userSets) {
        NSNumber *value = [exerciseDict objectForKey:set.exerciseName];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.exerciseName];
    }
    
//    NSLog(@"Exercise:");
//    for (id key in exerciseDict) {
//        NSLog(@"Sets: %@ Ex: %@", exerciseDict[key], key);
//    }
    
    // lets sort them in decreasing order.
    NSArray *myArray;
    myArray = [exerciseDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    
    NSLog(@"%@", myArray);
    NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"exerciseName == %@", [myArray objectAtIndex:0]];
    
    NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
    float totalSets = 0, totalReps = 0, totalWt = 0;
    totalSets = (int)[subArray count];
    for (ExerciseSet *set in subArray) {
        totalReps += [set.rep intValue];
        totalWt += [set.weight floatValue] * [set.rep intValue];
    }
    
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    [data setObject:[myArray objectAtIndex:0] forKey:@"Name"];
    [data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Sets"];
    [data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
    }
    return data;
    
}

+(NSMutableDictionary *) getMostWorkedOutWorkout: (NSArray *) userSets {
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    
    if ([userSets count] == 0) {
        [data setObject:@"None" forKey:@"Name"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Sets"];
        [data setObject:[NSNumber numberWithInt:0] forKey:@"Reps"];
        [data setObject:[NSString stringWithFormat:@"0 %@", [[Utilities getUnits]lowercaseString] ] forKey:@"Volume"];
        
    } else {
    NSMutableDictionary *exerciseDict = [[NSMutableDictionary alloc] init];
    for (ExerciseSet *set in userSets) {
        if (set.workoutName == nil)
            continue;
        
        NSNumber *value = [exerciseDict objectForKey:set.workoutName];
        if (value  == nil) {
            // insert object with value 1
            value = [NSNumber numberWithInt:1];
        } else {
            value = [NSNumber numberWithInt:[value intValue] + 1];
        }
        [exerciseDict setObject:value forKey:set.workoutName];
    }
    
//    NSLog(@"Exercise:");
//    for (id key in exerciseDict) {
//        NSLog(@"Sets: %@ Workout: %@", exerciseDict[key], key);
//    }
    
    // lets sort them in decreasing order.
    NSArray *myArray;
    myArray = [exerciseDict keysSortedByValueUsingComparator: ^(id obj1, id obj2) {
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
    NSPredicate *muscleMost = [NSPredicate predicateWithFormat:@"workoutName == %@", [myArray objectAtIndex:0]];
    
    NSArray *subArray = [userSets filteredArrayUsingPredicate:muscleMost];
    float totalSets = 0, totalReps = 0, totalWt = 0;
    totalSets = (int)[subArray count];
    for (ExerciseSet *set in subArray) {
        totalReps += [set.rep intValue];
        totalWt += [set.weight floatValue] * [set.rep intValue];
    }
    
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    [data setObject:[myArray objectAtIndex:0] forKey:@"Name"];
    [data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Sets"];
    [data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Reps"];
    [data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt]], [[Utilities getUnits]lowercaseString] ] forKey:@"Volume"];
    
    }
    return data;
}

+(NSMutableDictionary *) getAverageRepRange: (NSArray *) userSets {
    
    int totalReps=0;
    NSString *type;
    
    for (ExerciseSet *set in userSets) {
        totalReps += [set.rep intValue];
    }
    float avg =totalReps/[userSets count];
    NSLog(@"Average Rep Range: %lu", (int)totalReps/[userSets count]);
    if (avg <=5) {
        type = @"Strength";
    } else if (avg > 6 && avg < 15) {
        type = @"Hypertrophy";
    } else {
        type = @"Endurance";
    }
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:[NSNumber numberWithFloat:avg] forKey:@"Reps Range"];
    [data setObject:type forKey:@"Muscle Growth"];
    
    return data;
}

+(NSMutableDictionary *) totalWorkoutTime: (NSArray *) userSets {
    int totalSeconds = (int)[userSets count]* 45;
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    NSLog(@"total calories; %d", totalSeconds/45 * 6);
    NSString *time = [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
    
    NSMutableDictionary *data = [[NSMutableDictionary alloc] init];
    [data setObject:time forKey:@"Workout Time"];
    return data;
}



// ROUTINE ANALYSIS

+(NSMutableDictionary*) getWorkoutStats:(NSArray *) data workoutName:(NSString *) workoutName {
    
    float totalSets = 0, totalReps = 0, totalWt = 0, totalWorkouts = 0;

    
    NSPredicate *wkPredicate = [NSPredicate predicateWithFormat:@"workoutName == %@", workoutName];
    NSArray *workoutSets = [data filteredArrayUsingPredicate:wkPredicate];
    
    totalSets = [workoutSets count];
    
    NSMutableDictionary *uniqueWorkouts = [[NSMutableDictionary alloc] init];
    
    for (ExerciseSet *set in workoutSets) {
        NSNumber *dates = [uniqueWorkouts objectForKey:set.date];
        if (dates == nil) {
            dates = [NSNumber numberWithInt:1];
        } else {
            dates = [NSNumber numberWithInt:[dates intValue] + 1];
        }
        [uniqueWorkouts setObject:dates forKey:set.date];
        totalReps += [set.rep intValue];
        totalWt += [set.rep intValue] * [set.weight floatValue];
    }
    totalWorkouts = (int)[[uniqueWorkouts allKeys] count];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    NSMutableArray *workoutDays = [[NSMutableArray alloc] init];
    
    NSNumberFormatter *numberformatter = [[NSNumberFormatter alloc] init];
    [numberformatter setNumberStyle:NSNumberFormatterDecimalStyle];

    [cal_data setObject:[NSNumber numberWithInt:totalWt] forKey:@"Total Wt. Moved"];
    [cal_data setObject:[NSNumber numberWithInt:totalReps] forKey:@"Total Reps"];
    [cal_data setObject:[NSNumber numberWithInt:totalSets] forKey:@"Total Sets"];
    [cal_data setObject:[NSNumber numberWithFloat:totalReps/totalWorkouts] forKey:@"Avg Reps/Workout"];
    [cal_data setObject:[NSNumber numberWithFloat:totalSets/totalWorkouts] forKey:@"Avg Sets/Workout"];
    [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithFloat:totalWt/totalWorkouts]], [[Utilities getUnits] lowercaseString]] forKey:@"Avg Volume/Workout"];
    [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithInt:totalWt/totalReps]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume/Rep"];
    [cal_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithInt:totalWt/totalSets]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume/Set"];
    
    
    NSLog(@"workout performed %@", uniqueWorkouts);
    for (id key in uniqueWorkouts) {
        NSMutableDictionary *wd_data = [[NSMutableDictionary alloc] init];
        NSPredicate *datePredicate = [NSPredicate predicateWithFormat:@"date == %@", key];
        NSArray *eachWorkoutDay = [data filteredArrayUsingPredicate:datePredicate];
        float ts = 0, tr = 0, tw = 0;
        ts = (int)[eachWorkoutDay count];
        for (ExerciseSet *set in eachWorkoutDay) {
            tr += [set.rep intValue];
            tw += [set.rep intValue] * [set.weight floatValue];
        }
        [wd_data setObject:[NSNumber numberWithInt:ts] forKey:@"Sets"];
        [wd_data setObject:[NSNumber numberWithInt:tr] forKey:@"Reps"];
        [wd_data setObject:[NSString stringWithFormat:@"%@ %@", [numberformatter stringFromNumber:[NSNumber numberWithInt:tw]], [[Utilities getUnits] lowercaseString]] forKey:@"Volume"];
        [wd_data setObject:key forKey:@"Date"];
        
        [workoutDays addObject:wd_data];
    }
    
    [cal_data setObject:workoutDays forKey:@"Workouts history"];
    
    // NSLog(@"%@", cal_data);
    
    return cal_data;
    
}

+(NSMutableDictionary *) getDailyStatsCard:(NSString *)date {
    
    NSMutableArray *order = [[NSMutableArray alloc] init];
    [order addObject:@"Weight"];
    [order addObject:@"Body Fat (%)"];
    [order addObject:@"BMI"];
    
    NSMutableDictionary *cal_data = [[NSMutableDictionary alloc] init];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    NSCalendar *gregorian = [[NSCalendar alloc]
                             initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *startDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[formatter dateFromString:date]];
    [startDayComponents setHour:00];
    [startDayComponents setMinute:00];
    [startDayComponents setSecond:00];
    
    NSDateComponents *endDayComponents =
    [gregorian components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:[formatter dateFromString:date]];
    [endDayComponents setHour:23];
    [endDayComponents setMinute:59];
    [endDayComponents setSecond:59];
    
    NSDate *startDate = [gregorian dateFromComponents:startDayComponents];
    NSDate *endDate = [gregorian dateFromComponents:endDayComponents];
    
    
    NSManagedObjectContext *localContext = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *dailyPredicate = [NSPredicate predicateWithFormat:@"date >= %@ AND date <= %@", startDate, endDate];
    DailyStats *todayItem = [DailyStats MR_findFirstWithPredicate:dailyPredicate inContext:localContext];
    NSDateFormatter *fancyFormatter = [[NSDateFormatter alloc] init];
    [fancyFormatter setDateFormat:@"MMMM dd yyyy"];

    
    if (todayItem == nil) {
        [cal_data setObject:@"--" forKey:@"Weight"];
        [cal_data setObject:@"--" forKey:@"Body Fat"];
        [cal_data setObject:@"--" forKey:@"BMI"];


    } else {
        [cal_data setObject:[NSString stringWithFormat:@"%@ %@", todayItem.weight, [[Utilities getUnits] lowercaseString]] forKey:@"Weight"];
        [cal_data setObject:[NSString stringWithFormat:@"%@%%", todayItem.bodyFat] forKey:@"Body Fat"];
        [cal_data setObject:todayItem.bodyMassIndex forKey:@"BMI"];
    }
        [cal_data setObject:[NSString stringWithFormat:@"%@", [fancyFormatter stringFromDate:[NSDate date]]] forKey:@"Name"];

    return cal_data;
}
@end
