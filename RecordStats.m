//
//  RecordStats.m
//  uLift
//
//  Created by Mayank Verma on 10/20/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "RecordStats.h"
#import "commons.h"
@interface RecordStats () {
    NSArray *statsTitle;
}
@end

@implementation RecordStats

- (void)viewDidLoad {
    [super viewDidLoad];
    statsTitle = [[NSArray alloc] initWithObjects:@"Weight", @"BMI", @"BF", nil];
    self.title = @"Record Stats";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [statsTitle count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(cell.frame)/2, 30)];
    title.text = [statsTitle objectAtIndex: indexPath.row];
    
    UITextField *value = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(title.frame), 5, CGRectGetWidth(cell.frame), 30)];
    
    value.adjustsFontSizeToFitWidth = YES;
    value.textColor = [UIColor blackColor];
    if ([indexPath row] == 0) {
        value.placeholder = @"lbs";
        value.keyboardType = UIKeyboardTypeEmailAddress;
        value.returnKeyType = UIReturnKeyNext;
    }
    else {
        value.placeholder = @"%";
        value.keyboardType = UIKeyboardTypeDefault;
        value.returnKeyType = UIReturnKeyDone;
    }
    value.backgroundColor = [UIColor whiteColor];
    value.autocorrectionType = UITextAutocorrectionTypeNo; // no auto correction support
    value.autocapitalizationType = UITextAutocapitalizationTypeNone; // no auto capitalization support
    value.tag = 0;
    //value.delegate = self;
    
    value.clearButtonMode = UITextFieldViewModeNever; // no clear 'x' button to the right
    [value setEnabled: YES];

    [cell.contentView addSubview:title];
    [cell.contentView addSubview:value];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 30.0f;
}

-(NSString *) tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    NSString *header = [NSString stringWithFormat: @"Record Body Stats for (%@)", [Utilities getCurrentDate]];
    return header;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
