//
//  AnalysisHome.h
//  gyminutes
//
//  Created by Mayank Verma on 5/15/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
@interface AnalysisHome : UIViewController <CarbonTabSwipeNavigationDelegate> {
    NSArray *items;
    CarbonTabSwipeNavigation *carbonTabSwipeNavigation;
}

@property (nonatomic, retain) NSString *routineName;
@end
