//
//  DailyStats+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "DailyStats+CoreDataProperties.h"

@implementation DailyStats (CoreDataProperties)

+ (NSFetchRequest<DailyStats *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"DailyStats"];
}

@dynamic bodyFat;
@dynamic bodyMassIndex;
@dynamic caloriesBurned;
@dynamic date;
@dynamic elMorning;
@dynamic elPostWorkout;
@dynamic elPreWorkout;
@dynamic steps;
@dynamic syncedState;
@dynamic weight;

@end
