//
//  AnalysisHome.m
//  gyminutes
//
//  Created by Mayank Verma on 5/15/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "AnalysisHome.h"
#import "RoutineAnalysusVC.h"
#import "WorkoutAnalysis.h"
#import "MuscleAnalysis.h"
#import "ExerciseAnalysis.h"
#import "AnalyzeVC.h"

@interface AnalysisHome()
@property (nonatomic, strong) CNPPopupController *popupController;
@end
@implementation AnalysisHome

@synthesize routineName;

-(void) viewDidLoad {
    [super viewDidLoad];
    self.title = routineName;
    items = @[@"STATS", @"ROUTINE", @"WORKOUT", @"MUSCLE", @"EXERCISES"];
    
    carbonTabSwipeNavigation = [[CarbonTabSwipeNavigation alloc] initWithItems:items delegate:self];
    [carbonTabSwipeNavigation insertIntoRootViewController:self];
    UIBarButtonItem *infoBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconInfo"] style:UIBarButtonItemStyleDone target:self action:@selector(showHelp:)];
    
    self.navigationItem.rightBarButtonItems = @[infoBtn];

    [self style];
}

-(IBAction)showHelp:(id)sender {
    [self showPopupWithStyle:CNPPopupStyleCentered];
}
-(void) viewDidAppear:(BOOL)animated {
    [carbonTabSwipeNavigation setIndicatorColor:[Utilities getAppColor]];
}
- (void)style {
    
    UIColor *color = [UIColor whiteColor];//[UIColor colorWithRed:24.0/255 green:75.0/255 blue:152.0/255 alpha:1];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    int width = CGRectGetWidth(self.view.frame)/4;
    carbonTabSwipeNavigation.toolbar.translucent = NO;
    carbonTabSwipeNavigation.toolbar.backgroundColor = [UIColor whiteColor];
    [carbonTabSwipeNavigation setTabExtraWidth:30];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:0];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:1];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:2];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:3];
    [carbonTabSwipeNavigation.carbonSegmentedControl setWidth:width forSegmentAtIndex:4];
    // Custimize segmented control
    [carbonTabSwipeNavigation setNormalColor:[color colorWithAlphaComponent:0.6]
                                        font:[UIFont boldSystemFontOfSize:14]];
    [carbonTabSwipeNavigation setSelectedColor:color
                                          font:[UIFont boldSystemFontOfSize:14]];
}

# pragma mark - CarbonTabSwipeNavigation Delegate
// required
- (nonnull UIViewController *)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbontTabSwipeNavigation
                                 viewControllerAtIndex:(NSUInteger)index {
    switch (index) {
        case 0: {
            AnalyzeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AnalyzeVC"];
            destVC.routineName = routineName;
            return destVC;
        }
        case 1: {
            RoutineAnalysusVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"routineAnalysis"];
            destVC.routineName = routineName;
            return destVC;
        }
        
        case 2: {
            WorkoutAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"workoutAnalysis"];
            destVC.routineName = routineName;
            return destVC;
            
        }
        case 3: {
            MuscleAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"muscleAnalysis"];
            destVC.routineName = routineName;
            return destVC;

        }
        case 4: {
            ExerciseAnalysis *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"exerciseAnalysis"];
            destVC.routineName = routineName;
            return destVC;

        }
            
        default:
            //            return [self.storyboard instantiateViewControllerWithIdentifier:@"ViewControllerThree"];
            return nil;
    }
}

// optional
- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                 willMoveAtIndex:(NSUInteger)index {
    //    switch(index) {
    //        case 0:
    //            self.title = @"Original";
    //            break;
    //        case 1:
    //            self.title = @"Alternate";
    //            break;
    //        case 2:
    //            self.title = @"Advanced";
    //            break;
    //        case 3:
    //            self.title = @"Core/Lower Body";
    //            break;
    //        default:
    //            self.title = items[index];
    //            break;
    //    }
}

- (void)carbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation
                  didMoveAtIndex:(NSUInteger)index {
    NSLog(@"Did move at index: %ld", (unsigned long)index);
}

- (UIBarPosition)barPositionForCarbonTabSwipeNavigation:(nonnull CarbonTabSwipeNavigation *)carbonTabSwipeNavigation {
    return UIBarPositionTop; // default UIBarPositionTop
}

- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;

    NSMutableParagraphStyle *paragraphStyleSub = NSMutableParagraphStyle.new;
    paragraphStyleSub.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyleSub.alignment = NSTextAlignmentJustified;

    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Help" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"ESTIMATE (Est.)\nThe statistics that have this tag are calculated based on the created workout." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16], NSParagraphStyleAttributeName : paragraphStyleSub}];

    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"ACTUAL (Act.)\nThe statistics that have this tag are calculated based on the actual workout performed by you." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:16], NSParagraphStyleAttributeName : paragraphStyleSub}];

    NSAttributedString *lineThree = [[NSAttributedString alloc] initWithString:@"Note :\nThe workout time is computed based on a 45 sec set. Scientific studies have shown that maximum muscle growth happens when muscle is under stress from 40 - 60 seconds.\n" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14], NSParagraphStyleAttributeName : paragraphStyleSub}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    lineOneLabel.textAlignment = NSTextAlignmentLeft;

    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    lineTwoLabel.textAlignment = NSTextAlignmentLeft;

    UILabel *lineThreeLabel = [[UILabel alloc] init];
    lineThreeLabel.numberOfLines = 0;
    lineThreeLabel.attributedText = lineThree;
    lineTwoLabel.textAlignment = NSTextAlignmentLeft;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, lineTwoLabel, lineThreeLabel]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    [self.popupController presentPopupControllerAnimated:YES];
}

@end
