//
//  MuscleList+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "MuscleList+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface MuscleList (CoreDataProperties)

+ (NSFetchRequest<MuscleList *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *majorMuscle;
@property (nullable, nonatomic, copy) NSString *muscleName;

@end

NS_ASSUME_NONNULL_END
