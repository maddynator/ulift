//
//  PackageDetailCVC.h
//  gyminutes
//
//  Created by Mayank Verma on 1/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"


@interface PackageDetailCVC : UICollectionViewController

@property (nonatomic, retain) NSString *routineBundleId;
@property (nonatomic, retain) SKProduct *product;

@end
