//
//  AchievementChecker.m
//  gyminutes
//
//  Created by Mayank Verma on 6/21/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "AchievementChecker.h"
#import "commons.h"
@implementation AchievementChecker

+(AchievementChecker *)sharedInstance {
    
    static dispatch_once_t pred = 0;
    static AchievementChecker *instance = nil;
    dispatch_once(&pred, ^{
        instance = [[AchievementChecker alloc] init];
    });
    return instance;

}

+(BOOL) checkBigThree {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *benchPressPredicate = [NSPredicate predicateWithFormat:@"exerciseName == Barbell Bench Press"];
    NSPredicate *squatPredicate = [NSPredicate predicateWithFormat:@"exerciseName == Squat"];
    NSPredicate *deadliftPressPredicate = [NSPredicate predicateWithFormat:@"exerciseName == Deadlift"];
    
    ExerciseMetaInfo *bpMax = [ExerciseMetaInfo MR_findFirstWithPredicate:benchPressPredicate inContext:localContext];
    ExerciseMetaInfo *sqMax = [ExerciseMetaInfo MR_findFirstWithPredicate:squatPredicate inContext:localContext];
    ExerciseMetaInfo *dlMax = [ExerciseMetaInfo MR_findFirstWithPredicate:deadliftPressPredicate inContext:localContext];
    
    
    if (bpMax != nil && sqMax != nil && dlMax != nil) {
        NSLog(@"BIG 3 INITIATED!!!");
        float totalWeight = [bpMax.maxWeight floatValue] + [sqMax.maxWeight floatValue] + [dlMax.maxWeight floatValue];
        if ([Utilities isKgs]) {
            totalWeight *= 2.2;
        }
        
        if (totalWeight >=250)
            NSLog(@"250 LB Club");

        if (totalWeight >=500)
            NSLog(@"500 LB Club");

        if (totalWeight >=750)
            NSLog(@"750 LB Club");

        if (totalWeight >=1000)
            NSLog(@"1000 LB Club");

        if (totalWeight >=1250)
            NSLog(@"1250 LB Club");

        if (totalWeight >=1500)
            NSLog(@"1500 LBΩClub");
    }

    return true;
}

/*
 .5 x bodyweight
 1 x bodyweight
 1.5 x bodyweight
 2x bodyweight
 2.5x bodyweight
 3x bodyweight
 */

+(BOOL) checkExerciseTimesWeight:(NSString *) exerciseName lastWeight:(float) userLastRecordedWeight {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *exercisePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
    ExerciseMetaInfo *exerciseMax = [ExerciseMetaInfo MR_findFirstWithPredicate:exercisePredicate inContext:localContext];
    
    if (exerciseMax != nil && userLastRecordedWeight != 0) {
        float maxWeight = [exerciseMax.maxWeight floatValue];
        float wtToBw = maxWeight/userLastRecordedWeight;
        if (wtToBw > 0.5)
            NSLog(@"Lifted 0.5x your body weight");
        
        if (wtToBw > 1)
            NSLog(@"Lifted 1.0 x your body weight");

        if (wtToBw > 2)
            NSLog(@"Lifted 2.0 x your body weight");

        if (wtToBw > 2.5)
            NSLog(@"Lifted 2.5x your body weight");

        if (wtToBw > 3)
            NSLog(@"Lifted 3.0 x your body weight");

        if (wtToBw > 2.5)
            NSLog(@"Lifted 3.5x your body weight");
        
        if (wtToBw > 3.5)
            NSLog(@"Lifted 4.0 x your body weight");

        if (wtToBw > 4.5)
            NSLog(@"Lifted 4.5 x your body weight");
        
        if (wtToBw > 5)
            NSLog(@"Lifted 5.0 x your body weight");
    }
    
    return true;
}
/*
    100% - improvement
	200%- improvement
	300%- improvement
	400%- improvement
	500%- improvement
*/

+(BOOL) checkExerciseImprovement:(NSString *) exerciseName {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *exercisePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
    ExerciseMetaInfo *exerciseMax = [ExerciseMetaInfo MR_findFirstWithPredicate:exercisePredicate inContext:localContext];
    NSArray *allSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO withPredicate:exercisePredicate inContext:localContext];
    
    if (exerciseMax != nil && [allSets count] > 0) {
        float maxWeight = [exerciseMax.maxWeight floatValue];
        ExerciseSet *firstSet = [allSets objectAtIndex:0];
        float firstWt = [firstSet.weight floatValue];
        float improvement = (maxWeight/firstWt)*100;
        
        if (improvement > 200)
            NSLog(@"Lifted 200 %% your body weight");
        
        if (improvement > 300)
            NSLog(@"Lifted 200 %% your body weight");
        
        if (improvement > 400)
            NSLog(@"Lifted 400 %% your body weight");
        
        if (improvement > 500)
            NSLog(@"Lifted 500 %% your body weight");
    }
    
    return true;
}

/*
 100 k lb club
	500 k lb club
	1M lb club
	5M lb club
	10M lb club

*/

+(BOOL) checkTotalWeight{
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *allSets = [ExerciseSet MR_findAllInContext:localContext];
    
    if ([allSets count] > 0) {
        float totalWeight = 0;
        for (ExerciseSet *set in allSets) {
            totalWeight += [set.rep intValue] * [set.weight floatValue];
        }

        if ([Utilities isKgs]) {
            totalWeight *= 2.2;
        }

        if (totalWeight > 50000)
            NSLog(@"Lifted 50K Total Weight");
        
        if (totalWeight > 100000)
            NSLog(@"Lifted 100 K Total Weight");
        
        if (totalWeight > 250000)
            NSLog(@"Lifted 250 K Total Weight");
        
        if (totalWeight > 500000)
            NSLog(@"Lifted 500 K Total Weight");
        
        if (totalWeight > 1000000)
            NSLog(@"Lifted 1 M Total Weight");

        if (totalWeight > 2500000)
            NSLog(@"Lifted 2.5 M Total Weight");

        if (totalWeight > 5000000)
            NSLog(@"Lifted 5 M Total Weight");

        if (totalWeight > 10000000)
            NSLog(@"Lifted 10 M Total Weight");


    }
    
    return true;
}

/*
 
 Per Routine
	100 k lb club
	500 k lb club
	1M lb club
	5M lb club

 */

+(BOOL) checkTotalWeightInRoutine: (NSString *) routineName{
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *routinePredicate = [NSPredicate predicateWithFormat:@"routineName == %@", routineName];
    NSArray *allSets = [ExerciseSet MR_findAllWithPredicate:routinePredicate inContext:localContext];
    
    if ([allSets count] > 0) {
        float totalWeight = 0;
        for (ExerciseSet *set in allSets) {
            totalWeight += [set.rep intValue] * [set.weight floatValue];
        }
        
        if ([Utilities isKgs]) {
            totalWeight *= 2.2;
        }
        
        if (totalWeight > 50000)
            NSLog(@"Lifted 50K Total Weight %@", routineName);
        
        if (totalWeight > 100000)
            NSLog(@"Lifted 100 K Total Weight %@", routineName);
        
        if (totalWeight > 250000)
            NSLog(@"Lifted 250 K Total Weight %@", routineName);
        
        if (totalWeight > 500000)
            NSLog(@"Lifted 500 K Total Weight %@", routineName);
        
        if (totalWeight > 1000000)
            NSLog(@"Lifted 1 M Total Weight %@", routineName);
        
        if (totalWeight > 2500000)
            NSLog(@"Lifted 2.5 M Total Weight %@", routineName);
        
        if (totalWeight > 5000000)
            NSLog(@"Lifted 5 M Total Weight %@", routineName);
        
        if (totalWeight > 10000000)
            NSLog(@"Lifted 10 M Total Weight %@", routineName);
    }
    
    return true;
}

/* 
 PR TO WARM UP Per exercise
 */
+(BOOL) checkExercisePrToWarnup:(NSString *) exerciseName {
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *exercisePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseName];
    ExerciseMetaInfo *exerciseMax = [ExerciseMetaInfo MR_findFirstWithPredicate:exercisePredicate inContext:localContext];
    NSArray *allSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO withPredicate:exercisePredicate inContext:localContext];
    
    if (exerciseMax != nil && [allSets count] > 0) {
        float maxWeight = [exerciseMax.maxWeight floatValue];
        ExerciseSet *firstSet = [allSets objectAtIndex:0];
        float firstWt = [firstSet.weight floatValue];
        float improvement = (maxWeight/firstWt)*100;
        
        if (improvement > 200)
            NSLog(@"Lifted 200 %% your body weight");
        
        if (improvement > 300)
            NSLog(@"Lifted 200 %% your body weight");
        
        if (improvement > 400)
            NSLog(@"Lifted 400 %% your body weight");
        
        if (improvement > 500)
            NSLog(@"Lifted 500 %% your body weight");
    }
    
    return true;
}

@end
