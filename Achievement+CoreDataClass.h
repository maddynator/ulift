//
//  Achievement+CoreDataClass.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

NS_ASSUME_NONNULL_BEGIN

@interface Achievement : NSManagedObject

@end

NS_ASSUME_NONNULL_END

#import "Achievement+CoreDataProperties.h"
