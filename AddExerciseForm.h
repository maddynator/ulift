#import <UIKit/UIKit.h>
#import "FXForms.h"
#import "commons.h"

typedef NS_ENUM(NSInteger, Equipment)
{
    Barbell = 0,
    BodyWeight,
    Cable,
    Dumbbell,
    ExerciseBall,
    Machine,
    kettlebell,
    Other
};

typedef NS_ENUM(NSInteger, Mechanics)
{
    Isolated = 0,
    Compound
};

typedef NS_ENUM(NSInteger, ExperienceLevel)
{
    Begineer = 0,
    Intermediate,
    Experienced
};

typedef NS_ENUM(NSInteger, Muscle)
{
    Abs = 0,
    Back,
    Biceps,
    Calves,
    Chest,
    Forearm,
    Glutes,
    Hamstring,
    Lats,
    Quads,
    Shoulders,
    Traps,
    Triceps
};

typedef NS_ENUM(NSInteger, MajorMuscle)
{
    MjAbs = 0,
    MjBack,
    MjArms,
    MjLegs,
    MJChest,
    MJShoulders
};

typedef NS_ENUM(NSInteger, Type)
{
    Strength = 0,
    Plyometrics,
    Powerlifting,
    Weightlifting
};

@interface AddExerciseForm : NSObject <FXForm>

@property (nonatomic, copy) NSString *name;
@property (nonatomic, assign) Equipment equipment;
@property (nonatomic, assign) Mechanics mechanics;
@property (nonatomic, assign) ExperienceLevel experience;
@property (nonatomic, assign) Muscle muscle;
@property (nonatomic, assign) MajorMuscle muscleGroup;
@property (nonatomic, assign) Type type;

@end
