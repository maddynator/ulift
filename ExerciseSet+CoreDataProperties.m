//
//  ExerciseSet+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseSet+CoreDataProperties.h"

@implementation ExerciseSet (CoreDataProperties)

+ (NSFetchRequest<ExerciseSet *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExerciseSet"];
}

@dynamic date;
@dynamic exerciseName;
@dynamic exerciseNumber;
@dynamic isDropSet;
@dynamic isPyramidSet;
@dynamic isSuperSet;
@dynamic majorMuscle;
@dynamic muscle;
@dynamic rep;
@dynamic routineName;
@dynamic setNumber;
@dynamic syncedState;
@dynamic timeStamp;
@dynamic weight;
@dynamic workoutName;

@end
