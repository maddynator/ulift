//
//  commons.h
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#ifndef uLift_commons_h
#define uLift_commons_h
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import <ParseTwitterUtils/PFTwitterUtils.h>
#import <Bolts/Bolts.h>
#import <KVNProgress/KVNProgress.h>
#import "FXForms.h"
#import "UIScrollView+EmptyDataSet.h"
//#import "TSMessage.h"
#import "UIView+Toast.h"
#import "Utilities.h"
#import "MZTimerLabel.h"
#import "Chameleon.h"
#import "CAPSPageMenu.h"
#import "STCollapseTableView.h"
#import "RBMenu.h"
#import "SCLAlertView.h"
#import "CustomLogInViewController.h"
#import "CustomSignUpViewController.h"
#import <DIDatepicker.h>
#import "HMSegmentedControl.h"
#import "UILabel+FormattedText.h"
#import "MGSwipeTableCell.h"
#import "MGSwipeButton.h"
#import "DateTools.h"
#import "SHMultipleSelect.h"
#import "JVFloatLabeledTextField.h"
#import "JVFloatLabeledTextView.h"
#import "RSDFDatePickerView.h"
#import "Flurry.h"
#import "CarbonKit.h"

@import Charts;
//#import "uLift-Swift.h"
//Logging
//#import "CocoaLumberjack.h"

#import "CNPPopupController.h"

// Coredata headers
#import <MagicalRecord/MagicalRecord.h>
#import <MagicalRecord/MagicalRecord+ShorthandMethods.h>
//#import <MagicalRecord/MagicalRecordShorthandMethodAliases.h>
#import "ExerciseList+CoreDataClass.h"
#import "MuscleList+CoreDataClass.h"
#import "ExerciseSet+CoreDataClass.h"
#import "WorkoutList+CoreDataClass.h"
#import "UserWorkout+CoreDataClass.h"
#import "RoutineMetaInfo+CoreDataClass.h"
#import "ExerciseMetaInfo+CoreDataClass.h"
#import "UserProfile+CoreDataClass.h"
#import "RoutineList.h"
#import "MuscleListTVC.h"
#import "SelectRoutine.h"
#import "SelectWorkout.h"
#import "UserWorkout+CoreDataClass.h"
#import "Appirater.h"
#import "REComposeViewController.h"
#import <sys/utsname.h>
#import "AFNetworking.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import "DMActivityInstagram.h"
#import "uLiftIAPHelper.h"
#import "MPCoachMarks.h"
#import "TNRadioButtonGroup.h"
#import <Crashlytics/Crashlytics.h>
#import "TAPromotee.h"
#import "iCarousel.h"
#import "SMPageControl.h"
#import "CircleTimer.h"
#import <EHPlainAlert/EHPlainAlert.h>
#import <Mixpanel/Mixpanel.h>
#import "DBInterface.h"
#import "UpdateHandler.h"
#import "NYSegmentedControl.h"
#import "ShareViewController.h"
#import "DailyStatsVC.h"
#import "CLWeeklyCalendarView.h"
#import "DailyStats+CoreDataClass.h"
#import "ExercisePersonalRecords+CoreDataClass.h"
#import "UserWeightManager.h"
#import "MultiSelectSegmentedControl.h"
#import "ParseErrorHandlingController.h"
@import AVFoundation;

#import "StickCollectionViewFlowLayout.h"
#import "UIImageView+AFNetworking.h"

#define P_MUSCLEGROUP_CLASS     "MuscleGroup"
#define P_EXERCISELIST_CLASS    "Exercises"
#define P_EXERCISEMETAINFO_CLASS    "ExerciseMetaInfo"
#define P_EXERCISESET_CLASS     "UserSet"
//#define P_WORKOUTLIST_CLASS     "Workout"
#define P_USERPROFILE_CLASS      "UserProfile"
#define P_USEREXERCISE_CLASS   "UserExercises"
#define P_ROUTINE_META_INF_CLASS    "RoutineMetaInfo"
#define P_WORKOUT_INFO_CLASS        "UserWorkout"
#define P_IAP_ROUTINES              "IAPRoutines"
#define P_IAP_WORKOUTS              "IAPWorkouts"
#define P_DAILY_STATS               "DailyStats"
#define P_EX_PERSONAL_REC           "ExPersonalRecord"

#define HAL_REG_FONT            "HelveticaNeue"
#define HAL_BOLD_FONT            "HelveticaNeue-Bold"
#define HAL_THIN_FONT            "HelveticaNeue-Thin"

#define TABBAR_HEIGHT           49
#define REST_TIMER_OVER_TEXT    "Rest time is over."
#define IAP_PREMIUM_PACKAGE_ID      "com.gymessential.gyminutes.PremiumPackage"
#define IAP_WORKOUT_PACKAGE_ID      "com.gymessential.gyminutes.WorkoutPackage"
#define IAP_DATA_PACKAGE_ID         "com.gymessential.gyminutes.DataPackage"
#define IAP_ROUTINE_PACKAGE_ID      "com.gymessential.gyminutes.RoutinePackage"
#define IAP_ANALYTICS_PACKAGE_ID      "com.gymessential.gyminutes.AnalyticsPackage"

// FREE ROUTINES
#define IAP_ROUTINE_BODYWEIGHT_MAX          "com.gymessential.gyminutes.BodyweightMax"
#define IAP_ROUTINE_HYPERTROPHY_UNLIMITED   "com.gymessential.gyminutes.HypertrophyUnlimited"
#define IAP_ROUTINE_STRONGLIFT5X5           "com.gymessential.gyminutes.StrongLift5x5"
#define IAP_ROUTINE_2DAYSPLITPUSHPULL       "com.gymessential.gyminutes.2daysplitpushpull"
#define IAP_ROUTINE_3DAYSPLIT               "com.gymessential.gyminutes.3daysplitbodybuilding"                                            
#define IAP_ROUTINE_4DAYSPLIT               "com.gymessential.gyminutes.4daysplitbodybuilding"
#define IAP_ROUTINE_5DAYSPLIT               "com.gymessential.gyminutes.5daysplitbodybuilding"
#define IAP_ROUTINE_6DAYSPLIT               "com.gymessential.gyminutes.6daysplitbodybuilding"
#define IAP_ROUTINE_GVT_I                   "com.gymessential.gyminutes.gvtone"
#define IAP_ROUTINE_GVT_II                  "com.gymessential.gyminutes.gvttwo"
#define IAP_ROUTINE_3DAYDUMBBELLDOMINATION  "com.gymessential.gyminutes.3daydumbbelldomination"
#define IAP_ROUTINE_FOCUSSED_FOUR           "com.gymessential.gyminutes.focussedfour"
//#define IAP_ROUTINE_BRICKHOUSE_BUILDER      "com.gymessential.gyminutes.focussedfour"

#define DATA_TYPE               @"DataType"
#define IMAGE_TYPE              @"Image"
#define PIE_CHART               @"PieChart"
#define LINE_CHART              @"LineChart"
#define BAR_CHART               @"BarChart"
#define LIST                    @"List"
#define BOX                     @"Box"
#define BUTTON                  @"Button"
#define RIGHT_SIDE_BUTTON       @"RightSideButton"
#define SINGLE_BUTTON           @"SingleButton"
#define LIST_AND_LINE_CHART     @"ListAndLineChart"
#define LIST_AND_PIE_CHART     @"ListAndPieChart"
#define SETS_PROGRESS           @"Sets Progress"
#define REPS_PROGRESS           @"Reps Progress"
#define VOL_PROGRESS            @"Volume Progress"
#define LINE_COUNT              @"Line Count"
#define CHART_COUNT             @"Chart count"
// FAVORITE ANALYTICS
#define AT_FAV_MUSCLE           @"FAVORITE MUSCLE"
#define AT_FAV_EXERCISE         @"FAVORITE EXERCISE"
#define AT_FAV_WORKOUT          @"FAVORITE WORKOUT"
#define AT_FAV_ROUTINE          @"FAVORITE ROUTINE"
#define AT_LAST_WORKOUT         @"LAST WORKOUT"
#define AT_RECORD_STATS         @"BODY STATS"
#define AT_TODAY_WORKOUT        @"TODAY WORKOUT"

// ROUTINE ANALYTICS
#define AT_NAME_KEY                                 @"name"
#define AT_NAME_MUSCLE                              @"Muscle"
#define AT_NAME_MAJOR_MUSCLE                        @"MajorMuscle"
#define AT_ORDER                                    @"Order"
#define AT_ROUTINE_MAJOR_MUSCLE_DISTRIBUTION        "AT_ROUTINE_MAJOR_MUSCLE_DISTRIBUTION"
#define AT_ROUTINE_MUSCLE_DISTRIBUTION              "AT_ROUTINE_MUSCLE_DISTRIBUTION"
#define AT_ROUTINE_FRONT_TO_BACK_DISTRIBUTION       "AT_ROUTINE_FRONT_TO_BACK_DISTRIBUTION"
#define AT_ROUTINE_UP_BOTTOM_DISTRIBUTION           "AT_ROUTINE_UP_BOTTOM_DISTRIBUTION"
#define AT_ROUTINE_EX_EQUIMENT_DISTRIBUTON          "AT_ROUTINE_EX_EQUIMENT_DISTRIBUTON"
#define AT_ROUTINE_EX_MECHANICS_DISTRIBUTON         "AT_ROUTINE_EX_MECHANICS_DISTRIBUTON"
#define AT_ROUTINE_EX_TYPE_DISTRIBUTON              "AT_ROUTINE_EX_TYPE_DISTRIBUTON"

#define AT_ROUTINE_START_DATE               "AT_ROUTINE_START_DATE"
#define AT_ROUTINE_PERFORMED_TIMES          "AT_ROUTINE_PERFORMED_TIMES"
#define AT_ROUTINE_TOTAL_WEIGHT_MOVED       "AT_ROUTINE_TOTAL_WEIGHT_MOVED"
#define AT_ROUTINE_TOTAL_REPS               "AT_ROUTINE_TOTAL_REPS"
#define AT_ROUTINE_TOTAL_SETS               "AT_ROUTINE_TOTAL_SETS"
#define AT_ROUTINE_TOTAL_LIFT_TIME          "AT_ROUTINE_TOTAL_LIFT_TIME"

//ROUTINE FAVOURITE
#define AT_ROUTINE_FAV_WORKOUT              "AT_ROUTINE_FAV"

// personal records set in this routine.
#define AT_ROUTINE_PR                       "AT_ROUTINE_PR"

#define AT_ROUTINE_AVG_WORKOUT_TIME_EST     "AT_ROUTINE_AVG_WORKOUT_EST"
#define AT_ROUTINE_AVG_WORKOUT_TIME_ACT     "AT_ROUTINE_AVG_WORKOUT_ACT"

// WORKOUT ANALYTICS
#define AT_SHORTEST_WORKOUT                 "AT_SHORTEST_WORKOUT"
#define AT_LONGEST_WORKOUT                  "AT_LONGEST_WORKOUT"

#define AT_WORKOUT_PER_WEEK             "AT_WORKOUT_PER_WEEK"
#define AT_WORKOUT_PER_MONTH            "AT_WORKOUT_PER_MONTH"

#define AT_BEST_WORKOUT_OF_THE_WEEK         "AT_BEST_WORKOUT_OF_THE_WEEK"
#define AT_BEST_WORKOUT_OF_THE_MONTH        "AT_BEST_WORKOUT_OF_THE_MONTH"
#define AT_BEST_WORKOUT_OF_THE_ROUTINE      "AT_BEST_WORKOUT_OF_THE_ROUTINE"

#define AT_AVG_REP_PER_WORKOUT              "AT_AVG_REP_PER_WORKOUT"
#define AT_AVG_SETS_PER_WORKOUT             "AT_AVG_SETS_PER_WORKOUT"
#define AT_AVG_REST_TIME_PER_WORKOUT        "AT_AVG_REST_TIME_PER_WORKOUT"
#define AT_AVG_LIFT_TIME_PER_WORKOUT        "AT_AVG_LIFT_TIME_PER_WORKOUT"
#define AT_AVG_WEIGHT_MOVED_PER_WORKOUT     "AT_AVG_WEIGHT_MOVED_PER_WORKOUT"
#define AT_AVG_WEIGHT_MOVED_PER_REP_PER_WORKOUT    "AT_AVG_WEIGHT_MOVED_PER_REP_PER_WORKOUT"

#define AT_INVITE_FRIENDS                   "AT_INVITE_FRIENDS"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]


typedef enum {
    SYNC_NOT_DONE = false,
    SYNC_DONE = true
} syncState;

enum {
    TERMS_WEBVIEW = 0,
    PRIVACY_WEBVIEW,
    INSTAGRAM_WEBVIEW,
    FAQS_WEBVIEW,
    TUTORIALS_WEBVIEW
};

enum {
    FACEBOOK_CONNECT = 0,
    TWITTER_CONNECT,
    FACEBOOK_LIKE,
    TWITTER_FOLLOW,
    
};


#define kCellHeight 90
#define kItemSpace -20

#define FREE_VERSION_DATE_LIMIT_MONTH   3
#define FREE_VERSION_DATE_LIMIT_DATE    16
#define FREE_VERSION_DATE_LIMIT_YEAR    2016
#define FREE_VERSION_TRIAL_PERIOD       15

#define AMRAP_VERSION_DATE_LIMIT_MONTH   4
#define AMRAP_VERSION_DATE_LIMIT_DATE    1
#define AMRAP_VERSION_DATE_LIMIT_YEAR    2017

#define MIXPANEL_TOKEN @"5e920c314e3b77eb5583d0d6f70c0171"
#define MIXPANEL_KEY   @"5ba6a763727eced4bba2cbac588520c6"
#define MIXPANEL_SECRET @"056618b2ac016ade6a67c947d18b3bd3"

#define WIDGET_GROUP    @"group.gymessential.GyminitesSharingDefault"

//#define NSLog(FORMAT, ...) printf("%s\n", [[NSString stringWithFormat:FORMAT, ##__VA_ARGS__] UTF8String]);

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define USER_DEFAULT_UPDATE_TO_AMRAP    @"UpdatedToAMRAP"

#endif
