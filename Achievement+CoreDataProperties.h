//
//  Achievement+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "Achievement+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface Achievement (CoreDataProperties)

+ (NSFetchRequest<Achievement *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSDate *dateAchieved;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSString *renewRate;
@property (nullable, nonatomic, copy) NSString *routineName;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSDate *timeAchieved;
@property (nullable, nonatomic, copy) NSString *title;
@property (nullable, nonatomic, copy) NSString *workoutName;

@end

NS_ASSUME_NONNULL_END
