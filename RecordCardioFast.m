//
//  RecordCardioFast.m
//  gyminutes
//
//  Created by Mayank Verma on 7/1/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "RecordCardioFast.h"

@interface RecordCardioFast () {
    NSMutableArray *allItems ;
    NSArray *cardioFields;
    int indexForCollection;
}

@end

@implementation RecordCardioFast
@synthesize tableView, collectionView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    
    self.title = @"Cardio";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    NSString *dateField = [NSString stringWithFormat:@"Start Time (%@)", [dateFormatter stringFromDate:[NSDate date]]];
    NSString *distanceFields = [NSString stringWithFormat:@"Distance (Miles)"];
    if ([Utilities isKgs]) {
        distanceFields = [NSString stringWithFormat:@"Distance (Kms)"];
    }
    
    cardioFields = @[@"Cardio Type", @"Training Type",  @"Duration (Mins)", distanceFields, @"Empty Stomach", @"Calories Burned", @"Elevation", dateField];

    allItems = [[NSMutableArray alloc] init];
    [allItems addObject:@[@"Running", @"Cycling", @"Swimming"]];
    [allItems addObject:@[@"High Intensity", @"Steady State", @"Low Intensity"]];
    
    NSMutableArray *durationArray = [[NSMutableArray alloc] init];
    for (int i = 1; i < 60; i++)
        [durationArray addObject:[NSNumber numberWithInt:i]];
    [allItems addObject:durationArray];
    
    NSMutableArray *distanceArray = [[NSMutableArray alloc] init];
    for (float i = 1; i < 10; i=i+.1)
        [distanceArray addObject:[NSNumber numberWithFloat:i]];
    [allItems addObject:distanceArray];
    
    [allItems addObject:@[@"YES", @"NO"]];

    NSMutableArray *caloriesArray = [[NSMutableArray alloc] init];
    for (int i = 1; i < 1000; i++)
        [caloriesArray addObject:[NSNumber numberWithInt:i]];
    [allItems addObject:caloriesArray];
    
    NSMutableArray *elevationArray = [[NSMutableArray alloc] init];
    for (float i = 1; i < 15; i = i+.1)
        [elevationArray addObject:[NSNumber numberWithInt:i]];
    [allItems addObject:elevationArray];

    NSMutableArray *dateArray = [[NSMutableArray alloc] init];
    for (int i = 1; i < 1000; i++)
        [dateArray addObject:[NSNumber numberWithInt:i]];
    [allItems addObject:dateArray];
    
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    tableView.delegate = self;
    tableView.dataSource = self;
    
    // add to canvas
    [self.view addSubview:tableView];
}

#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [allItems count];
}

// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    
    
    UITableViewCell *cell = (UITableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UICollectionView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        }
    }
    UILabel *header = [[UILabel alloc]  initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 20)];
    header.text = [cardioFields objectAtIndex:indexPath.row];
    header.font = [UIFont fontWithName:@HAL_REG_FONT size:14];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(5, 20, CGRectGetWidth(self.view.frame) - 10, 40) collectionViewLayout:layout];
    [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    collectionView.backgroundColor = [UIColor whiteColor];
    collectionView.tag  = indexPath.row;
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    
    [cell.contentView addSubview:header];
    [cell.contentView addSubview:collectionView];
    
    return cell;
}

#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}


//-(NSInteger)
//numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
//    
//}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [[allItems objectAtIndex:collectionView.tag] count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[self.collectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];

        }
    }
    cell.backgroundColor = FlatRed;
    cell.layer.cornerRadius = 5;

    NSArray *data = [allItems objectAtIndex:collectionView.tag];
    UILabel *cellLabel = [[UILabel alloc] init];
    cellLabel.text = [NSString stringWithFormat:@"%@", [data objectAtIndex:indexPath.row]];
    cellLabel.textAlignment = NSTextAlignmentCenter;
    cellLabel.textColor = [Utilities getAppColor];
    cellLabel.backgroundColor = [UIColor whiteColor];
    cellLabel.layer.borderColor = [Utilities getAppColor].CGColor;
    cellLabel.layer.borderWidth = 2;
    cellLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:12];
    [cellLabel sizeToFit];
    cellLabel.frame = CGRectMake(0, 0, cell.frame.size.width, 30);
    cellLabel.clipsToBounds = YES;

    [cell.contentView addSubview:cellLabel];
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    float width = 0;
    int items =[[allItems objectAtIndex:collectionView.tag] count];
        width = 80;
    
    return CGSizeMake(width, 30);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSArray *data = [allItems objectAtIndex:collectionView.tag];
    NSLog(@"item selected is %@ %@", [cardioFields objectAtIndex:collectionView.tag], [data objectAtIndex:indexPath.row]);
}
@end
