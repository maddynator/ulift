//
//  UserProfile+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "UserProfile+CoreDataProperties.h"

@implementation UserProfile (CoreDataProperties)

+ (NSFetchRequest<UserProfile *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"UserProfile"];
}

@dynamic age;
@dynamic deviceType;
@dynamic email;
@dynamic experienceLevel;
@dynamic height;
@dynamic membershipType;
@dynamic sex;
@dynamic units;
@dynamic userName;
@dynamic weight;

@end
