//
//  WorkoutAnalysis.h
//  gyminutes
//
//  Created by Mayank Verma on 5/15/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface WorkoutAnalysis : UIViewController <UICollectionViewDelegate, UICollectionViewDataSource, ChartViewDelegate, IChartAxisValueFormatter>

@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet LineChartView *lineChartView;
@property (nonatomic, strong) IBOutlet PieChartView *pieChartView;
@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSegment;


@end
