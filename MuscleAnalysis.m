//
//  MuscleAnalysis.m
//  gyminutes
//
//  Created by Mayank Verma on 5/15/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "MuscleAnalysis.h"
#import "commons.h"

@interface MuscleAnalysis () {
    NSMutableArray *dataArr, *heightArr, *xAxisLabelArr;
    float lblHeight ;
    int oldTimerIndex;
    NSDate *selectedDate;
}

@end
@implementation MuscleAnalysis

@synthesize collectionView, timeSegment;

-(void) viewDidLoad {
    [super viewDidLoad];
    lblHeight = 20;
    timeSegment = [[UISegmentedControl alloc] initWithItems:@[@"1W", @"2W", @"1M", @"3M", @"6M", @"1Y"]];
    timeSegment.frame = CGRectMake(10, 5, CGRectGetWidth(self.view.frame) - 20, 30);
    [self.view addSubview:timeSegment];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timeSegment.frame) + 5, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.navigationController.tabBarController.tabBar.frame) - 95) collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];
    
    timeSegment.selectedSegmentIndex = 1;
    oldTimerIndex = 1;
    selectedDate = [[NSDate date] dateBySubtractingWeeks:2];
    
    [timeSegment addTarget:self action:@selector(timeSegmentClicked:) forControlEvents:UIControlEventValueChanged];
    dataArr = [[NSMutableArray alloc] init];
    heightArr = [[NSMutableArray alloc] init];
    [self.view addSubview:collectionView];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineName", _routineName,
                                   nil];
    [Flurry logEvent:@"MuscleAnalysis" withParameters:articleParams];

}

-(void) viewDidAppear:(BOOL)animated {
    [self timeSegmentClicked:timeSegment];
    [self.collectionView reloadData];
    
}

-(void)timeSegmentClicked:(UISegmentedControl *)segmentedControl {
    bool showAnalysis = false;
    
    if (segmentedControl.selectedSegmentIndex > 1) {
        if ([Utilities showAnalyticsPackage]) {
            [self showPurchasePopUp:@"This is a paid feature."];
            timeSegment.selectedSegmentIndex = oldTimerIndex;
        } else
            showAnalysis = true;
    } else
        showAnalysis = true;
    
    if (showAnalysis == true) {
        oldTimerIndex = (int) segmentedControl.selectedSegmentIndex;
        
        NSString *dateStringForHeaders = @"";
        NSLog(@"segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *today = [NSDate date];
        
        switch (segmentedControl.selectedSegmentIndex) {
            case 0:
                selectedDate = [today dateBySubtractingWeeks:1];
                dateStringForHeaders = @"last week.";
                break;
            case 1:
                selectedDate = [today dateBySubtractingWeeks:2];
                dateStringForHeaders = @"last 2 weeks.";
                break;
            case 2:
                selectedDate = [today dateBySubtractingMonths:1];
                dateStringForHeaders = @"last 1 month.";
                break;
            case 3:
                selectedDate = [today dateBySubtractingMonths:3];
                dateStringForHeaders = @"last 3 months.";
                break;
            case 4:
                selectedDate = [today dateBySubtractingMonths:6];
                dateStringForHeaders = @"last 6 months.";
                break;
            case 5:
                selectedDate = [today dateBySubtractingYears:1];
                dateStringForHeaders = @"last 1 year.";
                break;
            default:
                break;
        }
        [self getAllData];
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ANALYTICS PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ANALYTICS_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium or Analytics Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

-(void) getAllData {
    [heightArr removeAllObjects];
    [dataArr removeAllObjects];

    NSMutableDictionary *temp;
    NSNumber *height = nil;
    NSArray *orderArray = nil;
    float heightChart = 180;
    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    // NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@",_routineName];
    NSArray *workout = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    
    NSArray *tempSetsRoutines = nil;
    NSMutableArray *userSetsRoutine = [[NSMutableArray alloc] init];
    tempSetsRoutines = [ExerciseSet MR_findAllSortedBy:@"date,workoutName,timeStamp" ascending:NO withPredicate:predicate inContext:localContext];
    for (ExerciseSet *ex in tempSetsRoutines) {
        NSDate *date = [formatter dateFromString:ex.date];
        if ([date isLaterThanOrEqualTo:selectedDate]) {
            [userSetsRoutine addObject:ex];
        }
    }
    
    temp = [DBInterface getMuscleFavoriteForRoutine:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getMuscleStatsEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];

    temp = [DBInterface getMuscleStatsAct:userSetsRoutine];
    NSMutableDictionary *workoutNames = temp;
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getMajorMuscleDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];

    temp = [DBInterface getMajorMuscleDistributionOfRoutineAct:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getMuscleDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
   
    temp = [DBInterface getMuscleDistributionOfRoutineAct:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getFrontToBackDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getFrontToBackDistributionOfRoutineAct:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    
    temp = [DBInterface getUpperToBottomDistributionOfRoutineEst:workout];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
 
    temp = [DBInterface getUpperToBottomDistributionOfRoutineAct:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    // this is special
    for (id key in workoutNames) {
        if ([key isEqualToString:PIE_CHART] || [key isEqualToString:LIST] || [key isEqualToString:AT_ORDER ] || [key isEqualToString:DATA_TYPE] || [key isEqualToString:AT_NAME_KEY] || [key isEqualToString:CHART_COUNT] || [key isEqualToString:LINE_COUNT] || [key isEqualToString:LIST_AND_LINE_CHART])
            continue;
        
        NSLog(@"key is %@", key);

        NSPredicate *workoutPredicate = [NSPredicate predicateWithFormat:@"muscle == %@", key];
        NSArray *workoutSets = [userSetsRoutine filteredArrayUsingPredicate:workoutPredicate];
        temp = [DBInterface getPerMuscleStats:workoutSets];
        
        if ([temp[DATA_TYPE] isEqualToString:LIST_AND_LINE_CHART]) {
            height = [NSNumber numberWithInt:lblHeight * [temp[LINE_COUNT] intValue] + heightChart * [temp[CHART_COUNT] intValue]];
        }
        
        if  ([temp[AT_NAME_KEY] isEqualToString:@"None"])
             [temp setObject:key forKey:AT_NAME_KEY];
        
        [heightArr addObject:height];
        [dataArr addObject:temp];
        
    }
    [self.collectionView reloadData];

    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArr count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)myCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[myCollectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
    // cell.backgroundColor=[UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:cell.frame andColors:@[FlatGreen, FlatGreenDark]];
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-8, 8);
    cell.layer.shadowRadius = 5;
    cell.layer.shadowOpacity = 0.5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[PieChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[LineChartView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    NSMutableDictionary *item = [dataArr objectAtIndex:indexPath.row];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(cell.frame) - 10, 30)];
    title.text = item[AT_NAME_KEY];
    title.textAlignment = NSTextAlignmentCenter;
    title.textColor = [UIColor whiteColor];
    [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
    
    int iterate = 0;
    if ([item[DATA_TYPE] isEqualToString:PIE_CHART]) {
        _pieChartView = [[PieChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 170)];
        [self setupPieChart];
        [self setPieChartDataValues:item];
        [cell.contentView addSubview:_pieChartView];
    } else if ([item[DATA_TYPE] isEqualToString:LIST]) {
        NSArray *orderDisp = item[AT_ORDER];
        for (NSString *key in orderDisp) {
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            left.text = key;
            right.text = [NSString stringWithFormat:@"%@", item[key]];
            if ([right.text isEqualToString:@"-1"]) {
                [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                right.text = @"";
            } else {
                left.textAlignment = NSTextAlignmentLeft;
                right.textAlignment = NSTextAlignmentRight;
                [left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
            }
            left.textColor = [UIColor whiteColor];
            right.textColor = [UIColor whiteColor];
            
            [cell.contentView addSubview:left];
            [cell.contentView addSubview:right];
            iterate++;
        }
    } else {
        float maxY = CGRectGetMaxY(title.frame);
        NSArray *orderDisp = item[AT_ORDER];
        for (NSString *key in orderDisp) {
            if ([key isEqualToString:SETS_PROGRESS]) {
                NSLog(@"ST order is %@",key);

                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: SETS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatRed forKey:@"Dots"];
                [colorDict setObject:FlatRedDark forKey:@"Cover"];
                [self setLineChartDataValues:item[SETS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else if ([key isEqualToString:REPS_PROGRESS]) {
                NSLog(@"RE order is %@",key);
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: REPS_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatYellow forKey:@"Dots"];
                [colorDict setObject:FlatYellowDark forKey:@"Cover"];
                [self setLineChartDataValues:item[REPS_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
                
            } else if ([key isEqualToString:VOL_PROGRESS]) {
                NSLog(@"PO order is %@",key);
                _lineChartView = [[LineChartView alloc] initWithFrame: CGRectMake(5, maxY  + 5, CGRectGetWidth(cell.frame) - 10, 170)];
                [self setupLineChartView: VOL_PROGRESS];
                NSMutableDictionary *colorDict = [[NSMutableDictionary alloc] init];
                [colorDict setObject:FlatMint forKey:@"Dots"];
                [colorDict setObject:FlatMintDark forKey:@"Cover"];
                [self setLineChartDataValues:item[VOL_PROGRESS] colorDict:colorDict];
                [cell.contentView addSubview:_lineChartView];
                maxY = CGRectGetMaxY(_lineChartView.frame) + 10;
            } else {
                NSLog(@"LIST order is %@",key);
                UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), maxY, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
                left.text = key;
                right.text = [NSString stringWithFormat:@"%@", item[key]];
                if ([right.text isEqualToString:@"-1"]) {
                    [left setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
                    right.text = @"";
                } else {
                    left.textAlignment = NSTextAlignmentLeft;
                    right.textAlignment = NSTextAlignmentRight;
                    [left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
                    [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
                }
                left.textColor = [UIColor whiteColor];
                right.textColor = [UIColor whiteColor];
                
                [cell.contentView addSubview:left];
                [cell.contentView addSubview:right];
                iterate++;
                maxY = CGRectGetMaxY(left.frame);

            }
        }
    }
    [cell.contentView addSubview:title];
    //[cell.contentView addSubview:subTitle];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame) - 20, [[heightArr objectAtIndex:indexPath.row] floatValue] + 35);
}

-(void) setupLineChartView: (NSString *) name {
    _lineChartView.delegate = self;
    
    _lineChartView.descriptionText = name;
    _lineChartView.noDataText = @"No data.";
    
    _lineChartView.dragEnabled = YES;
    [_lineChartView setScaleEnabled:YES];
    _lineChartView.pinchZoomEnabled = YES;
    _lineChartView.drawGridBackgroundEnabled = NO;
    _lineChartView.backgroundColor = [Utilities getAppColor];
    _lineChartView.layer.cornerRadius = 4;
    
    
    ChartXAxis *xAxis = _lineChartView.xAxis;
    xAxis.drawAxisLineEnabled = NO;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    xAxis.valueFormatter = self;
    xAxis.labelRotationAngle = -90;

    
    ChartYAxis *leftAxis = _lineChartView.leftAxis;
    leftAxis.drawAxisLineEnabled = NO;
    leftAxis.drawGridLinesEnabled = NO;
    leftAxis.drawZeroLineEnabled = YES;
    leftAxis.enabled = NO;
    
    ChartYAxis *rightAxis = _lineChartView.rightAxis;
    rightAxis.drawAxisLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    
    _lineChartView.legend.enabled = false;
    
    _lineChartView.rightAxis.enabled = NO;
    _lineChartView.xAxis.axisLineColor = FlatWhite;
    _lineChartView.xAxis.labelTextColor = FlatWhite;
    _lineChartView.leftAxis.axisLineColor = FlatWhite;
    _lineChartView.leftAxis.labelTextColor = FlatWhite;
    
    [_lineChartView.viewPortHandler setMaximumScaleY: 2.f];
    [_lineChartView.viewPortHandler setMaximumScaleX: 2.f];
    
    //    ChartMarker *marker = [[ChartMarker alloc] initWithColor:[UIColor colorWithWhite:180/255. alpha:1.0] font:[UIFont systemFontOfSize:12.0] insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
    //    marker.minimumSize = CGSizeMake(80.f, 40.f);
    //    _chartView.marker = marker;
    
    //    _chartView.legend.form = ChartLegendFormLine;
    
    [_lineChartView animateWithXAxisDuration:2.5 easingOption:ChartEasingOptionEaseInOutQuart];
    
    
}

-(void) setLineChartDataValues: (NSMutableDictionary *) chartData colorDict:(NSMutableDictionary *) colorDict {
    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
//    [xAxisLabelArr removeAllObjects];
    
    NSArray *items = chartData[@"xvals"];
    for (int i = 0; i < [items count]; i++)
    {
        [xVals addObject:[items objectAtIndex:i]];
    }
    
    NSArray *yvalItems = chartData[@"yvals"];
    for (int i = 0; i < [yvalItems count]; i++)
    {
        int value = [[yvalItems objectAtIndex:i] intValue] ;
       // [yVals addObject:[[ChartDataEntry alloc] initWithValue:value xIndex:i]];
        //        [yVals1 addObject:[[ChartDataEntry alloc] initWithValue:value/2 xIndex:i]];
        [yVals addObject:[[ChartDataEntry alloc] initWithX:i y:value]];
    }
    
    if ([xVals count] == 0 || [yVals count] == 0) {
        return;
    }
    xAxisLabelArr = xVals;
    
    LineChartDataSet *set1 = [[LineChartDataSet alloc] initWithValues:yVals label:@""];
    //[[LineChartDataSet alloc] initWithYVals:yVals label:@""];
    
    set1.lineDashLengths = @[@5.f, @2.5f];
    set1.highlightLineDashLengths = @[@5.f, @2.5f];
    [set1 setColor:colorDict[@"Dots"]];
    [set1 setCircleColor:colorDict[@"Cover"]];
    set1.lineWidth = 1.5;
    set1.circleRadius = 5.0;
    set1.drawCircleHoleEnabled = NO;
    set1.valueFont = [UIFont systemFontOfSize:9.f];
    set1.valueTextColor = FlatWhite;
    set1.fillAlpha = 65/255.0;
    set1.fillColor = colorDict[@"Dots"];
    set1.fillAlpha = 1.f;
    set1.drawFilledEnabled = YES;
    
    set1.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    
    
    //    LineChartDataSet *set2 = [[LineChartDataSet alloc] initWithYVals:yVals1 label:@"DataSet 2"];
    //    set2.axisDependency = AxisDependencyRight;
    //    [set2 setColor:UIColor.redColor];
    //    [set2 setCircleColor:UIColor.whiteColor];
    //    set2.lineWidth = 2.0;
    //    set2.circleRadius = 3.0;
    //    set2.fillAlpha = 65/255.0;
    //    set2.fillColor = UIColor.redColor;
    //    set2.highlightColor = [UIColor colorWithRed:244/255.f green:117/255.f blue:117/255.f alpha:1.f];
    //    set2.drawCircleHoleEnabled = NO;
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    //    [dataSets addObject:set2];
    
    LineChartData *data = [[LineChartData alloc] initWithDataSet:set1];
    //[[LineChartData alloc] initWithXVals:xVals dataSets:dataSets];
    [data setValueFont:[UIFont fontWithName:@HAL_REG_FONT size:8.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _lineChartView.data = data;
    [_lineChartView highlightValues:nil];
    
}


-(void) setupPieChart {
    _pieChartView.usePercentValuesEnabled = YES;
    _pieChartView.holeColor = [Utilities getAppColor];
    _pieChartView.noDataText = @"No data.";
    _pieChartView.holeRadiusPercent = 0.40;
    _pieChartView.transparentCircleRadiusPercent = 0.21;
    _pieChartView.descriptionText = @"";
    _pieChartView.drawCenterTextEnabled = YES;
    _pieChartView.drawHoleEnabled = YES;
    _pieChartView.rotationAngle = 0.0;
    _pieChartView.rotationEnabled = YES;
    _pieChartView.legend.enabled = YES;
    _pieChartView.drawSliceTextEnabled = NO;
    
    //        ChartLegend *l = _pieChartView.legend;
    //        l.position = ChartLegendPositionPiechartCenter;
    //        l.xEntrySpace = 7.0;
    //        l.yEntrySpace = 0.0;
    //        l.yOffset = 0.0;
    
    
    _pieChartView.legend.form = ChartLegendFormSquare;
    _pieChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _pieChartView.legend.textColor = UIColor.whiteColor;
    _pieChartView.legend.position = ChartLegendPositionLeftOfChart;
    
    [_pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
    
}

/*
-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    int i = 0;
    bool colorFlag = false;
    for (id key in chartData)
    {
        if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE]) {
            continue;
        }
        
        if ([key isEqualToString:AT_NAME_MUSCLE]) {
            colorFlag = true;
            continue;
        }
        [xVals addObject:key];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:[chartData[key] floatValue] xIndex:i]];
        if (colorFlag == true) {
            [colors addObject:[Utilities getMajorMuscleColor:key]];
        }
        
    }
    
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:@""];
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    if (colorFlag == false) {
        [colors addObject:FlatYellowDark];
        [colors addObject:FlatRedDark];
        [colors addObject:FlatGreenDark];
        [colors addObject:FlatBlueDark];
        [colors addObject:FlatPinkDark];
        [colors addObject:FlatOrangeDark];
    }
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:pFormatter];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _pieChartView.data = data;
    [_pieChartView highlightValues:nil];
    
}
 */-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
     
     NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
     NSMutableArray *xVals = [[NSMutableArray alloc] init];
     NSMutableArray *colors = [[NSMutableArray alloc] init];
     
     
     // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
     bool colorFlag = false, isMajor = false, isMinor = false;
     
     if (chartData[AT_NAME_MUSCLE] != nil) {
         colorFlag = true;
         isMinor = true;
     }
     
     if (chartData[AT_NAME_MAJOR_MUSCLE] != nil) {
         colorFlag = true;
         isMajor = true;
     }
     
     for (id key in chartData)
     {
         if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE] || [key isEqualToString:AT_NAME_MUSCLE] || [key isEqualToString:AT_NAME_MAJOR_MUSCLE]) {
             continue;
         }
         if (colorFlag == true) {
             if (isMinor == true) {
                 [colors addObject:[Utilities getExerciseColor:key]];
             }
             else if (isMajor == true) {
                 [colors addObject:[Utilities getMajorMuscleColor:key]];
             }
         }
         [xVals addObject:key];
         //[yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:[chartData[key] floatValue] xIndex:i]];
         [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:[chartData[key] floatValue] label:key]];

     }
     
     PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:yVals1 label:@""];
     //[[PieChartDataSet alloc] initWithYVals:yVals1 label:@""];
     dataSet.sliceSpace = 0.0;
     
     // add a lot of colors
     
     if (colorFlag == false) {
         [colors addObject:FlatRedDark];
         [colors addObject:FlatYellowDark];
         [colors addObject:FlatBlueDark];
         [colors addObject:FlatMagentaDark];
         [colors addObject:FlatGreenDark];
         [colors addObject:FlatPinkDark];
         [colors addObject:FlatOrangeDark];
         [colors addObject:FlatBlackDark];
         [colors addObject:FlatTealDark];
     }
     
     
     dataSet.colors = colors;
     
     PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
     //[[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
     
     NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
     pFormatter.numberStyle = NSNumberFormatterPercentStyle;
     pFormatter.maximumFractionDigits = 1;
     pFormatter.multiplier = @1.f;
     pFormatter.percentSymbol = @" %";
     [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
     [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
     [data setValueTextColor:[UIColor whiteColor]];
     
     _pieChartView.data = data;
     [_pieChartView highlightValues:nil];
     
 }

#pragma mark - pieChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    if ([chartView isKindOfClass:[LineChartView class]]) {
        NSLog(@"Line chart");
    } else if ([chartView isKindOfClass:[PieChartView class]]) {
        NSLog(@"Pie chart");
    }
    
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}

- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis
{
        return xAxisLabelArr[(int)value % xAxisLabelArr.count];
}

@end
