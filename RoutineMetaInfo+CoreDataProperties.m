//
//  RoutineMetaInfo+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "RoutineMetaInfo+CoreDataProperties.h"

@implementation RoutineMetaInfo (CoreDataProperties)

+ (NSFetchRequest<RoutineMetaInfo *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"RoutineMetaInfo"];
}

@dynamic routineAuthor;
@dynamic routineBundleId;
@dynamic routineColor;
@dynamic routineComplete;
@dynamic routineCreatedDate;
@dynamic routineDescription;
@dynamic routineDisclosure;
@dynamic routineDuration;
@dynamic routineFaqs;
@dynamic routineImage;
@dynamic routineInstructions;
@dynamic routineLevel;
@dynamic routineName;
@dynamic routineRoundNumber;
@dynamic routineSummary;
@dynamic routineType;
@dynamic routineUserCreated;
@dynamic routingGoal;
@dynamic syncedState;

@end
