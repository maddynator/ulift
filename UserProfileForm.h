//
//  UserProfileForm.h
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//


#import <UIKit/UIKit.h>
#import "FXForms.h"
#import "commons.h"

typedef NS_ENUM(NSInteger, Gender)
{
    Gendermale = 0,
    GenderFemale,
    GenderOther
};

typedef NS_ENUM(NSInteger, Units)
{
    LbsFeet = 0,
    KgsMtr
};

typedef NS_ENUM(NSInteger, ExperienceLevel)
{
    Begineer = 0,
    Intermediate,
    Experienced
};

@interface UserProfileForm : NSObject <FXForm>
@property (nonatomic, copy) NSString *name;
@property (nonatomic, copy) NSString *email;

@property (nonatomic, assign) NSUInteger age;
@property (nonatomic, assign) Gender gender;
@property (nonatomic, assign) ExperienceLevel experience;

@property (nonatomic, assign) Units units;
@property (nonatomic, assign) float weight;
@property (nonatomic, assign) float height;

@end
