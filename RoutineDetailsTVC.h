//
//  RoutineDetailsTVC.h
//  uLift
//
//  Created by Mayank Verma on 9/20/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "uLiftIAPHelper.h"
#import <StoreKit/StoreKit.h>

@interface RoutineDetailsTVC : UITableViewController

@property (nonatomic, retain) NSString *routineBundleId;
@property (nonatomic, retain) SKProduct *product;
@property (nonatomic) BOOL isLocalStored;
@property (nonatomic, retain) NSString *routineNameLocal;
@end
