//
//  RecordExerciseMenu.m
//  uLift
//
//  Created by Mayank Verma on 7/6/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "RecordExerciseMenu.h"
#import "RecordExercise.h"

@interface RecordExerciseMenu ()
@property (nonatomic) CAPSPageMenu *pageMenu;
@end


@implementation RecordExerciseMenu
- (void)viewDidLoad {
    [super viewDidLoad];
    RecordExercise *controller1 = [[RecordExercise alloc] initWithNibName:@"RecordExercise" bundle:nil];
    //controller1.exerciseObj = _exerciseObj;
    controller1.date = _date;
    
    NSArray *controllerArray = @[controller1];//, controller2, controller3, controller4];
    NSDictionary *parameters = @{
                                 CAPSPageMenuOptionScrollMenuBackgroundColor: [UIColor colorWithRed:30.0/255.0 green:30.0/255.0 blue:30.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionViewBackgroundColor: [UIColor colorWithRed:20.0/255.0 green:20.0/255.0 blue:20.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionSelectionIndicatorColor: [UIColor orangeColor],
                                 CAPSPageMenuOptionBottomMenuHairlineColor: [UIColor colorWithRed:70.0/255.0 green:70.0/255.0 blue:70.0/255.0 alpha:1.0],
                                 CAPSPageMenuOptionMenuItemFont: [UIFont fontWithName:@"HelveticaNeue" size:13.0],
                                 CAPSPageMenuOptionMenuHeight: @(40.0),
                                 CAPSPageMenuOptionMenuItemWidth: @(90.0),
                                 CAPSPageMenuOptionCenterMenuItems: @(YES)
                                 };
    
    _pageMenu = [[CAPSPageMenu alloc] initWithViewControllers:controllerArray frame:CGRectMake(0.0, 0.0, self.view.frame.size.width, self.view.frame.size.height) options:parameters];
    [self.view addSubview:_pageMenu.view];

}

@end
