//
//  ExerciseSelectForWorkout.m
//  uLift
//
//  Created by Mayank Verma on 7/30/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "ExerciseSelectForWorkout.h"
#import "AddExerciseVC.h"

@interface ExerciseSelectForWorkout () {
    NSMutableArray * unique, *uniqueMuscle, *uniqueWorkout;
    NSMutableArray *exerciseGrouped;
    UISegmentedControl *segmentedControl;
    NSMutableSet* _collapsedSections;
    NSArray *exerciseArray, *searchResults, *exercisesPerformed, *exerciseMajorMuscle;//, *previousWorkout;
    NSString *searchString;
    JVFloatLabeledTextView *setsTextField, *repsTextField, *restTextField;
    int setCount;
    
    NSMutableArray *repArray,*amarpArray, *timerArray;
    UILabel *setsValue;
    
}

@property (nonatomic, strong) CNPPopupController *popupController;
@property (nonatomic, strong) NSMutableArray* headers;
@end

@implementation ExerciseSelectForWorkout
@synthesize myTableView, myToolBar, mySearchBarView, customRepsPerSet;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController.navigationBar setTranslucent:YES];
    self.myTableView.delegate = self;
    self.myTableView.dataSource = self;

    _collapsedSections = [NSMutableSet new];
    setCount = 0;
    
    //self.tableView.contentOffset = CGPointMake(0, CGRectGetHeight(searchBar.frame));
    self.title = _muscleGroup;
    
    /*
     1. get all exercises for major muscle group.
     2. find all minor muscle group inside the major group
     3. create the segment control
     4. create a new array for the selected minor muscle
     */
    
//    exerciseArray
//    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", _muscleGroup];
//    previousWorkout = [ExerciseSet MR_findAllSortedBy:@"date,exerciseName" ascending:NO withPredicate:predicate];
//    NSLog(@"exercise performed for %@ are %lu", _muscleGroup, (unsigned long)[previousWorkout count]);
    exerciseMajorMuscle = [Utilities getAllExerciseForMajorMuscle:_muscleGroup];
    
    uniqueMuscle = [NSMutableArray array];
    NSMutableSet * processedMuscle = [NSMutableSet set];
    
    for (ExerciseList *data in exerciseMajorMuscle) {
        NSString *string = data.muscle;
        if ([processedMuscle containsObject:string] == NO) {
            [uniqueMuscle addObject:string];
            [processedMuscle addObject:string];
        }
    }

    UIBarButtonItem *addExerciseBtn = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemAdd  target:self action:@selector(addExercise)];
    
    // as we are now creating workouts, we dont need to copy it
    //UIBarButtonItem *copyExerciseBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconCopy"] style:UIBarButtonItemStylePlain target:self action:@selector(copyExercise)];

//    if ([previousWorkout count] > 0) {
//        self.navigationItem.rightBarButtonItems = @[ addExerciseBtn];//, copyExerciseBtn];
//    } else {
        self.navigationItem.rightBarButtonItems = @[ addExerciseBtn];
//    }

    segmentedControl = [[UISegmentedControl alloc] initWithItems:uniqueMuscle];
    segmentedControl.frame = CGRectMake(0, 0, self.myToolBar.frame.size.width, 30);
    segmentedControl.tintColor = FlatWhite;
    segmentedControl.selectedSegmentIndex = 0;
    [segmentedControl addTarget:self action:@selector(MySegmentControlAction:) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *segmentedControlButtonItem = [[UIBarButtonItem alloc] initWithCustomView:(UIView *)segmentedControl];
    UIBarButtonItem *flexibleSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    NSArray *barArray = [NSArray arrayWithObjects: flexibleSpace, segmentedControlButtonItem, flexibleSpace, nil];
    [self.myToolBar setItems:barArray];
    
    // Here's where we create our UISearchController
    
    self.searchController = [[UISearchController alloc] initWithSearchResultsController:nil];
    self.searchController.searchResultsUpdater = self;
    self.searchController.searchBar.delegate = self;
    
    [self.searchController.searchBar sizeToFit];
    self.searchController.hidesNavigationBarDuringPresentation = NO;
    self.definesPresentationContext = YES;
    self.searchController.dimsBackgroundDuringPresentation = NO;

//    NSArray *searchBarArray = [NSArray arrayWithObjects: flexibleSpace, self.searchController, flexibleSpace, nil];
//    [self.mySearchBarView setItems:searchBarArray];
    [self.mySearchBarView addSubview:self.searchController.searchBar];
    //self.mySearchBarView.backgroundColor = [Utilities getAppColor];
    
    self.navigationController.navigationBar.opaque = YES;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    self.extendedLayoutIncludesOpaqueBars = YES;
    self.myTableView.tableHeaderView.backgroundColor = [Utilities getAppColor];
    
    //self.extendedLayoutIncludesOpaqueBars = YES;
    //self.definesPresentationContext = YES;
    //self.edgesForExtendedLayout = UIRectEdgeNone;
    //self.myTableView.contentInset = UIEdgeInsetsMake(30, 0, 0, 0);

    //myTableView.frame = CGRectMake(0, CGRectGetMaxY(myToolBar.frame), self.view.frame.size.width, CGRectGetHeight(self.view.frame) - CGRectGetMaxY(myToolBar.frame));
    
    repArray = [[NSMutableArray alloc] init];
    amarpArray = [[NSMutableArray alloc] init];
    timerArray = [[NSMutableArray alloc] init];

}

-(void)dealloc {
    // this needs to be done so we dont have a handing search controller...
    [self.searchController.view removeFromSuperview]; // It works!
}

- (void)MySegmentControlAction:(UISegmentedControl *)segment
{
    NSLog(@"selected segment %ld", (long)segment.selectedSegmentIndex);
    self.searchController.active = false;
    [self filterExercisesBasedOnMinorMuscle:[uniqueMuscle objectAtIndex:segment.selectedSegmentIndex]];
}

-(void) filterExercisesBasedOnMinorMuscle: (NSString *) minorMuscle {
    NSMutableArray *exerciseArrayAll = [[NSMutableArray alloc] init];
    NSPredicate *predicateMuscle = [NSPredicate predicateWithFormat:@"muscle == %@", minorMuscle];
    exerciseArray = [exerciseMajorMuscle filteredArrayUsingPredicate:predicateMuscle];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exercisePerformed == true AND muscle == %@", minorMuscle];
    NSArray *exerciseRecent = [exerciseArray filteredArrayUsingPredicate:predicate];
    
    for( ExerciseList *ex in exerciseRecent) {
        // we are intentially setting context to nil otherwise, the exercises keeps on reloading...
        ExerciseList *newData = [ExerciseList MR_createEntityInContext:nil];
        newData.exerciseName = ex.exerciseName;
        newData.muscle = ex.muscle;
        newData.majorMuscle = ex.majorMuscle;
        newData.equipment = @"Recent";
        [exerciseArrayAll addObject:newData];
    }
    
    
  //  NSLog(@"muscle grop %@, %@, %lu %lu %lu", _muscleGroup, minorMuscle, (long) [exerciseArray count], (long) [exerciseArrayAll count], (long)[exerciseMajorMuscle count]);
    
    [exerciseArrayAll addObjectsFromArray:exerciseArray];
    exerciseArray = (NSArray *) exerciseArrayAll;
    [self filterAndStuff];
    [self.myTableView reloadData];
}
-(void) addExercise {
    [self performSegueWithIdentifier:@"addExerciseSegue" sender:self];
}


-(void) viewDidAppear:(BOOL)animated {
    [self filterExercisesBasedOnMinorMuscle:[uniqueMuscle objectAtIndex:segmentedControl.selectedSegmentIndex]];
    if(_isTempExercise)
        _exerciseGroup = [NSNumber numberWithInt:[Utilities getNumberOfGroupsInCurrentWorkoutForTempExercise:_date]];
    else
        _exerciseGroup = [NSNumber numberWithInt:[Utilities getNumberOfGroupsInWorkoutRouting:_routineName workoutName:_workoutName]];
    NSLog(@"inside adding exercise to group %@", _exerciseGroup);
    [self.navigationController.navigationBar setHidden:NO];
    self.searchController.active = false;
//    [self filterAndStuff];
//    [self.myTableView reloadData];
    
}
-(void) filterAndStuff {
    unique = nil;
    unique = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseList *data in exerciseArray) {
        NSString *string = data.equipment;
        
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    
    exerciseGrouped = nil;
    exerciseGrouped = [[NSMutableArray alloc] init];
    
    for (NSString *equipment in unique) {
        //NSArray *exercGrp = [Utilities getAllExerciseForMuscleAndEquipment:_muscleGroup equip:equipment];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND equipment == %@", [uniqueMuscle objectAtIndex:segmentedControl.selectedSegmentIndex], equipment];
        NSArray *exerGrp;
        exerGrp =  [exerciseArray filteredArrayUsingPredicate:predicate];
        
        [exerciseGrouped addObject:exerGrp];
    }
    
    self.headers = nil;
    self.headers = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < [unique count] ; i++)
    {
        UIView* header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.myTableView.frame.size.width, 40)];
        //[header setBackgroundColor:FlatGreen];
        [header setBackgroundColor:[Utilities getMajorMuscleColor:_muscleGroup]];
        
        NSString *headerTxt = [NSString stringWithFormat:@"%@ (%lu)", [unique objectAtIndex:i], (unsigned long)[[exerciseGrouped objectAtIndex:i] count]];
        UILabel *hdrLabel = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMinX(header.frame), CGRectGetMinY(header.frame), CGRectGetWidth(header.frame), CGRectGetHeight(header.frame))];
        hdrLabel.text = headerTxt;
        hdrLabel.textColor = FlatWhite;
        hdrLabel.textAlignment = NSTextAlignmentCenter;
        
        [header addSubview:hdrLabel];
        [self.headers addObject:header];
    }
    
}
- (void)openSection:(NSUInteger)sectionIndex animated:(BOOL)animated {
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.myTableView) {
        if (self.searchController.active == false)
        {
            // Original table view
            return [unique count];
        }
        else
        {
        // search results table view
            NSLog(@"number of sections are 1");
            return 1;
        }
    } else {
        return 1;
    }
    
}
//- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    NSString *header = [NSString stringWithFormat:@"%@ (%lu)", [unique objectAtIndex:section], (unsigned long)[[exerciseGrouped objectAtIndex:section] count]];
//    return header;//[unique objectAtIndex:section];
//}
//
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.myTableView) {
        if (self.searchController.active == false)
        {
            // Original table view
            return [[exerciseGrouped objectAtIndex:section] count];//[exerciseArray count];
        }
        else
        {
            // search results table view
            return [searchResults count];
        }
    } else {
        return setCount;
    }
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    if (tableView == self.myTableView) {
        cell = [self.myTableView dequeueReusableCellWithIdentifier:@"Cell"];
        if ( cell == nil ) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
        }
        
        UILabel *exerciseName = (UILabel *)[cell viewWithTag:100];
        //if (tableView == self.myTableView)
        if (self.searchController.active == false)
        {
            ExerciseList *exercise = [[exerciseGrouped objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
            exerciseName.text = exercise.exerciseName;
            
        } else {
            ExerciseList *exercise = [searchResults objectAtIndex:indexPath.row];
            exerciseName.text = [NSString stringWithFormat:@"%@ (%@)", exercise.exerciseName, exercise.equipment];
        }
        NSLog(@"exercise name %@", exerciseName.text);
        return cell;
    } else {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"RepsCell"];
        [[[cell contentView] subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        int itemHeight = 35;
        float popupWidth = CGRectGetWidth(cell.frame);
        UILabel *setNum = [[UILabel alloc] initWithFrame:CGRectMake(0, 5, 75, 30)];
        setNum.text = [NSString stringWithFormat:@"%d", (int)indexPath.row + 1];
        setNum.textAlignment = NSTextAlignmentCenter;
        //setNum.backgroundColor = FlatRed;
        
        UIButton *repMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), 5, 30, itemHeight)];
        [repMinus setTitle:@"-" forState:UIControlStateNormal];
        repMinus.backgroundColor = FlatMintDark;
        repMinus.tag = 1000 + indexPath.row;
        [repMinus addTarget:self action:@selector(repTableDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *repTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinus.frame), 5, popupWidth/4, itemHeight)];
        
        repTextFied.placeholder = @"Reps";
        repTextFied.textAlignment = NSTextAlignmentCenter;
        repTextFied.editable = false;
        repTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        repTextFied.tag = 2000 + indexPath.row;
        repTextFied.textContainer.maximumNumberOfLines = 1;
        repTextFied.scrollEnabled = false;
        //repTextFied.floatingLabelYPadding = 0;
        repTextFied.placeholderYPadding = -10;
        repTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        
        UIButton *repPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repTextFied.frame), 5, 30, itemHeight)];
        [repPlus setTitle:@"+" forState:UIControlStateNormal];
        repPlus.backgroundColor = FlatMintDark;
        repPlus.tag = 3000 + indexPath.row;
        [repPlus addTarget:self action:@selector(repTableInc:) forControlEvents:UIControlEventTouchUpInside];
        
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == true) {
            repMinus.enabled = false;
            repPlus.enabled = false;
            repTextFied.backgroundColor = FlatGrayDark;
            repTextFied.text = @"MAX";
        } else {
            repMinus.enabled = true;
            repPlus.enabled = true;
            repTextFied.backgroundColor = [UIColor whiteColor];
            repTextFied.text = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:indexPath.row]];
        }
        
        UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];
        
        WPmaskLayer.frame = self.view.bounds;
        WPmaskLayer.path  = WPmaskPath.CGPath;
        WMmaskLayer.frame = self.view.bounds;
        WMmaskLayer.path  = WMmaskPath.CGPath;
        repPlus.layer.mask = WPmaskLayer;
        repMinus.layer.mask = WMmaskLayer;
        
        UISwitch *amarpSwitch = [[UISwitch alloc] initWithFrame:CGRectMake(CGRectGetWidth(cell.frame) - 75, 5, 75, 30)];
        amarpSwitch.tag = 4000 + indexPath.row;
        //amarpSwitch.tintColor = FlatRed;
        amarpSwitch.onTintColor = FlatGreen;
        [amarpSwitch setOn:[[amarpArray objectAtIndex:indexPath.row] boolValue]];
        [amarpSwitch addTarget:self action:@selector(amarpIsChanged:) forControlEvents:UIControlEventValueChanged];
        
        
        UIButton *timerMinus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setNum.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerMinus setTitle:@"-" forState:UIControlStateNormal];
        timerMinus.backgroundColor = FlatMintDark;
        timerMinus.tag = 5000 + indexPath.row;
        [timerMinus addTarget:self action:@selector(timerTableDec:) forControlEvents:UIControlEventTouchUpInside];
        
        JVFloatLabeledTextView *timerTextFied = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerMinus.frame), CGRectGetMaxY(repMinus.frame) + 5, popupWidth/4, itemHeight)];
        
        timerTextFied.placeholder = @"Rest";
        timerTextFied.textAlignment = NSTextAlignmentCenter;
        timerTextFied.editable = false;
        timerTextFied.keyboardType = UIKeyboardTypeDecimalPad;
        timerTextFied.tag = 6000 + indexPath.row;
        timerTextFied.textContainer.maximumNumberOfLines = 1;
        timerTextFied.scrollEnabled = false;
        timerTextFied.placeholderYPadding = -10;
        timerTextFied.floatingLabel.textAlignment = NSTextAlignmentCenter;
        timerTextFied.text = [NSString stringWithFormat:@"%d", [[timerArray objectAtIndex:indexPath.row] intValue]];
        
        UIButton *timerPlus = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timerTextFied.frame), CGRectGetMaxY(repMinus.frame) + 5, 30, itemHeight)];
        [timerPlus setTitle:@"+" forState:UIControlStateNormal];
        timerPlus.backgroundColor = FlatMintDark;
        timerPlus.tag = 7000 + indexPath.row;
        [timerPlus addTarget:self action:@selector(timerTableInc:) forControlEvents:UIControlEventTouchUpInside];
        
        UIBezierPath *TPmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        UIBezierPath *TMmaskPath = [UIBezierPath bezierPathWithRoundedRect:timerMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
        
        
        CAShapeLayer *TPmaskLayer = [[CAShapeLayer alloc] init];
        CAShapeLayer *TMmaskLayer = [[CAShapeLayer alloc] init];
        
        TPmaskLayer.frame = self.view.bounds;
        TPmaskLayer.path  = TPmaskPath.CGPath;
        TMmaskLayer.frame = self.view.bounds;
        TMmaskLayer.path  = TMmaskPath.CGPath;
        
        
        timerPlus.layer.mask = TPmaskLayer;
        timerMinus.layer.mask = TMmaskLayer;
        
        
        [cell.contentView addSubview:setNum];
        [cell.contentView addSubview:repMinus];
        [cell.contentView addSubview:repTextFied];
        [cell.contentView addSubview:repPlus];
        [cell.contentView addSubview:amarpSwitch];
        [cell.contentView addSubview:timerMinus];
        [cell.contentView addSubview:timerTextFied];
        [cell.contentView addSubview:timerPlus];
        
        return cell;
    }

    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (tableView == self.myTableView) {
        if (self.searchController.active == false)
        {
            return 40.0f;
        } else {
            return 0.1f;
        }
    } else
        return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == self.customRepsPerSet)
        return 86;
    else //if (tableView == self.customRestTableView)
        return 40;
}



-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    ExerciseList *data;
    
    if (tableView == self.myTableView) {
        if (self.searchController.active == false)
        {
            data = [[exerciseGrouped objectAtIndex:indexPath.section]objectAtIndex:indexPath.row];
        } else {
            data = [searchResults objectAtIndex:indexPath.row];
        }
        NSLog(@"===> exercise selected %@ group number %@", data.exerciseName, _exerciseGroup);
        // we are setting this to false because otherwise a UIViewWrapperController that is transparent appears on top of view and navigation bar becomes inactive.
        self.searchController.active = false;
        searchString = @"";
        
        [self customSetsMenu:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup];

    } else {
        
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (tableView == self.myTableView) {
        if (self.searchController.active == false)
        {
            return [self.headers objectAtIndex:section];//[self.headers objectAtIndex:section];
        } else {
            return nil;
        }
    } else
        return nil;
}

// When the user types in the search bar, this method gets called.
- (void)updateSearchResultsForSearchController:(UISearchController *)aSearchController {
    NSLog(@"updateSearchResultsForSearchController");
    
    searchString = aSearchController.searchBar.text;
    NSLog(@"searchString=%@", searchString);
    
    // Check if the user cancelled or deleted the search term so we can display the full list instead.
    if (![searchString isEqualToString:@""]) {
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND exerciseName contains[c] %@", [uniqueMuscle objectAtIndex:segmentedControl.selectedSegmentIndex ], searchString];        
        searchResults = [ExerciseList MR_findAllSortedBy:@"equipment" ascending:YES withPredicate:predicate inContext:localContext];
        NSLog(@"Search count %lu", [searchResults count]);
        [myTableView openSection:0 animated:YES];        
    }
    
    [self.myTableView reloadData];
}


/*
- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"muscle == %@ AND exerciseName contains[c] %@", [uniqueMuscle objectAtIndex:segmentedControl.selectedSegmentIndex ], searchText];
    
    searchResults = [ExerciseList MR_findAllSortedBy:@"equipment" ascending:YES withPredicate:predicate inContext:localContext];
    
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
    [self filterContentForSearchText:searchString
                               scope:[[self.searchDisplayController.searchBar scopeButtonTitles]
                                      objectAtIndex:[self.searchDisplayController.searchBar
                                                     selectedScopeButtonIndex]]];
    
    return YES;
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchScope:(NSInteger)searchOption {
    // Tells the table data source to reload when scope bar selection changes
    [self filterContentForSearchText:self.searchDisplayController.searchBar.text scope:
     [[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:searchOption]];
    
    return YES;
}
*/
/*
#pragma mark <UICollectionViewDataSource>
-(void) copyExercise{
    
    for (NSString *date in uniqueWorkout) {
        NSLog(@"previous workout dates are %@", date);
    }
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Copy Previous Workout" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:20], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = [Utilities getMajorMuscleColor:_muscleGroup];
    
    //collectionView for previous workout
    uniqueWorkout = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in previousWorkout) {
        NSString *string = data.date;
        if ([processed containsObject:string] == NO) {
            [uniqueWorkout addObject:string];
            [processed addObject:string];
        }
    }
    
    int collectionViewHeight = 100;
    if ([uniqueWorkout count] >= 3) {
        collectionViewHeight = 320;
    } else {
        collectionViewHeight = 110 * (int)[uniqueWorkout count];
    }
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0,0, 250, collectionViewHeight) collectionViewLayout:layout];
    collectionView.backgroundColor = [UIColor clearColor];
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CellPrevious"];
    collectionView.delegate = self;
    collectionView.dataSource = self;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, collectionView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.delegate = self;

    [self.popupController presentPopupControllerAnimated:YES];
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    NSLog(@"num items in secion = %lu", (long) [uniqueWorkout count]);
    return [uniqueWorkout count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *identifier = @"CellPrevious";
    UICollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.layer.cornerRadius = 5;
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", [uniqueWorkout objectAtIndex:indexPath.row]];
    NSArray *exercisesForEachDate = [previousWorkout filteredArrayUsingPredicate:predicate];
    NSMutableArray *uniqueExercisePast = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in exercisesForEachDate) {
        NSString *string = data.exerciseName;
        if ([processed containsObject:string] == NO) {
            [uniqueExercisePast addObject:string];
            [processed addObject:string];
        }
    }
    
    UILabel *date = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 250, 30)];
    date.textAlignment = NSTextAlignmentCenter;
    date.text = [NSString stringWithFormat:@"%@ (%lu)", [uniqueWorkout objectAtIndex:indexPath.row], (unsigned long)[uniqueExercisePast count] ];
    date.textColor = FlatWhite;
    [date setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:15.0f]];
    cell.backgroundColor = [Utilities getMajorMuscleColorSelected:_muscleGroup];
    
    [cell.contentView addSubview:date];
    int count = 0;
    for (NSString *exerciseName in uniqueExercisePast) {
        if (count < 3) {
            UILabel *ex = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetHeight(date.frame) + 15 * count, 250, 15)];
            ex.textAlignment = NSTextAlignmentLeft;
            [ex setFont:[UIFont fontWithName:@HAL_REG_FONT size:13.0f]];
            ex.text = exerciseName;
            ex.textColor = FlatWhite;
            [cell.contentView addSubview:ex];
            count++;
        } else {
            UILabel *ex = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(date.frame) + 15 * count, 240, 15)];
            ex.textAlignment = NSTextAlignmentRight;
            [ex setFont:[UIFont fontWithName:@HAL_REG_FONT size:13.0f]];
            ex.text = @"... more ";
            ex.textColor = FlatWhite;
            [cell.contentView addSubview:ex];
            break;
        }

    }

    
    return cell;
}
-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.popupController dismissPopupControllerAnimated:NO];
    
    NSString *copyWorkoutSt = [NSString stringWithFormat:@"Copy workout from %@ to %@", [uniqueWorkout objectAtIndex:indexPath.row], _date];
    
    NSLog(@"did selected called...");
    
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert addButton:@"Yes" actionBlock:^{
        NSLog(@"will be adding following exercises to today...");
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", [uniqueWorkout objectAtIndex:indexPath.row]];
        NSArray *exercisesForEachDate = [previousWorkout filteredArrayUsingPredicate:predicate];
        NSMutableArray *uniqueExercisePast = [NSMutableArray array];
        NSMutableSet * processed = [NSMutableSet set];
        
        for (ExerciseSet *data in exercisesForEachDate) {
            NSString *string = data.exerciseName;
            if ([processed containsObject:string] == NO) {
                [uniqueExercisePast addObject:string];
                [processed addObject:string];
                NSPredicate *predicateExName = [NSPredicate predicateWithFormat:@"exerciseName == %@", string];
                ExerciseList *exercise = [ExerciseList MR_findFirstWithPredicate:predicateExName];
//                [Utilities createWorkoutListForExerciseOnDate:exercise date:_date];
                [Utilities addExerciseToWorkout:exercise date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup];
                NSLog(@"exercise to be added is %@", exercise.exerciseName);
            }
        }
  //      [[JLToast makeText:@"With delay, JLToast will be shown after delay." delay:1 duration:5] show];
    
    
    }];
    [alert showInfo:self title:@"Copy Workout" subTitle:copyWorkoutSt closeButtonTitle:@"No" duration:0.0f];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"date == %@", [uniqueWorkout objectAtIndex:indexPath.row]];
    NSArray *exercisesForEachDate = [previousWorkout filteredArrayUsingPredicate:predicate];
    NSMutableArray *uniqueExercisePast = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in exercisesForEachDate) {
        NSString *string = data.exerciseName;
        if ([processed containsObject:string] == NO) {
            [uniqueExercisePast addObject:string];
            [processed addObject:string];
        }
    }
    float height = 0;
    if ([uniqueExercisePast count] > 4) {
        height = 100;
    } else if ([uniqueExercisePast count] == 1) {
        height = 50;
    } else {
        height = 25 * [uniqueExercisePast count];
    }

    return CGSizeMake(250, height);
}
 */

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"addExerciseSegue"]) {
        AddExerciseVC *destVC = segue.destinationViewController;
        //PFObject *data = [muscleArray objectAtIndex:indexSelected];
        NSLog(@"%@ is muscle parent grp", _muscleGroup);
        destVC.majorMuscle = _muscleGroup;
    }
}

-(void) showNotificationMessage: (NSString *) title message:(NSString *) message alertType: (ViewAlertType ) type{
    [EHPlainAlert showAlertWithTitle:title message:message type:type];
}
                
#pragma custom sets and reps
-(void) customSetsMenu:
                (NSString *)date
                routineName:(NSString *)rName
                workoutName:(NSString *)wName
                exerciseList:(ExerciseList *) data
                exerciseGroup:(NSNumber *) exGroup
                exerciseNumInGroup:(NSNumber *) exNumInGrp {

    __block int cSets = [Utilities getDefaultSetsValue];
    __block int cReps = [Utilities getDefaultRepsValue];
    __block int cRest = [Utilities getDefaultRestTimerValue];
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:data.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    UIView *customView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 140)];

    int itemHeight = 40;
    float popupWidth = 190;
    
    // sets
    UIButton *setsMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, itemHeight)];
    [setsMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    setsMinusBtn.backgroundColor = FlatMintDark;
    [setsMinusBtn addTarget:self action:@selector(setsDec:) forControlEvents:UIControlEventTouchUpInside];
    
    setsTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsMinusBtn.frame), 0, popupWidth, itemHeight)];
    setsTextField.placeholder = @"SETS";
    setsTextField.textAlignment = NSTextAlignmentCenter;
    setsTextField.keyboardType = UIKeyboardTypeDecimalPad;
    setsTextField.textContainer.maximumNumberOfLines = 1;
    setsTextField.scrollEnabled = false;
    setsTextField.floatingLabelYPadding = 0;
    setsTextField.placeholderYPadding = -8;
    setsTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    setsTextField.text = [NSString stringWithFormat:@"%d", cSets];
    
    UIButton *setsPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsTextField.frame), 0, 30, itemHeight)];
    [setsPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    setsPlusBtn.backgroundColor = FlatMintDark;
    [setsPlusBtn addTarget:self action:@selector(setsInc:) forControlEvents:UIControlEventTouchUpInside];
    
    UIBezierPath *setPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:setsPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *setMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:setsMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *setPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *setMinusmaskLayer = [[CAShapeLayer alloc] init];
    setPlusmaskLayer.frame = self.view.bounds;
    setPlusmaskLayer.path  = setPlusmaskPath.CGPath;
    setMinusmaskLayer.frame = self.view.bounds;
    setMinusmaskLayer.path  = setMinusmaskPath.CGPath;
    setsPlusBtn.layer.mask = setPlusmaskLayer;
    setsMinusBtn.layer.mask = setMinusmaskLayer;
    
    // reps
    UIButton *repMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(setsPlusBtn.frame) + 5, 30, itemHeight)];
    [repMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    repMinusBtn.backgroundColor = FlatMintDark;
    [repMinusBtn addTarget:self action:@selector(repsDec:) forControlEvents:UIControlEventTouchUpInside];
    
    repsTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repMinusBtn.frame), CGRectGetMaxY(setsPlusBtn.frame) + 5, popupWidth, itemHeight)];
    repsTextField.placeholder = @"REPS";
    repsTextField.textAlignment = NSTextAlignmentCenter;
    repsTextField.keyboardType = UIKeyboardTypeDecimalPad;
    repsTextField.textContainer.maximumNumberOfLines = 1;
    repsTextField.scrollEnabled = false;
    repsTextField.floatingLabelYPadding = 0;
    repsTextField.placeholderYPadding = -8;
    repsTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    repsTextField.text = [NSString stringWithFormat:@"%d", cReps];
    
    UIButton *repPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(repsTextField.frame), CGRectGetMaxY(setsPlusBtn.frame) + 5, 30, itemHeight)];
    [repPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    repPlusBtn.backgroundColor = FlatMintDark;
    [repPlusBtn addTarget:self action:@selector(repsInc:) forControlEvents:UIControlEventTouchUpInside];
   
    UIBezierPath *repPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *repMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *repPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *repMinusmaskLayer = [[CAShapeLayer alloc] init];
    repPlusmaskLayer.frame = self.view.bounds;
    repPlusmaskLayer.path  = repPlusmaskPath.CGPath;
    repMinusmaskLayer.frame = self.view.bounds;
    repMinusmaskLayer.path  = repMinusmaskPath.CGPath;
    repPlusBtn.layer.mask = repPlusmaskLayer;
    repMinusBtn.layer.mask = repMinusmaskLayer;

    // rest
    UIButton *restMinusBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(repPlusBtn.frame) + 5, 30, itemHeight)];
    [restMinusBtn setTitle:@"-" forState:UIControlStateNormal];
    restMinusBtn.backgroundColor = FlatMintDark;
    [restMinusBtn addTarget:self action:@selector(restDec:) forControlEvents:UIControlEventTouchUpInside];
    
    restTextField = [[JVFloatLabeledTextView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(restMinusBtn.frame), CGRectGetMaxY(repPlusBtn.frame) + 5, popupWidth, itemHeight)];
    restTextField.placeholder = @"REST";
    restTextField.textAlignment = NSTextAlignmentCenter;
    restTextField.keyboardType = UIKeyboardTypeDecimalPad;
    restTextField.textContainer.maximumNumberOfLines = 1;
    restTextField.scrollEnabled = false;
    restTextField.floatingLabelYPadding = 0;
    restTextField.placeholderYPadding = -8;
    restTextField.floatingLabel.textAlignment = NSTextAlignmentCenter;
    restTextField.text = [NSString stringWithFormat:@"%d", cRest];
    
    UIButton *restPlusBtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(restTextField.frame), CGRectGetMaxY(repPlusBtn.frame) + 5, 30, itemHeight)];
    [restPlusBtn setTitle:@"+" forState:UIControlStateNormal];
    restPlusBtn.backgroundColor = FlatMintDark;
    [restPlusBtn addTarget:self action:@selector(restInc:) forControlEvents:UIControlEventTouchUpInside];
   
    UIBezierPath *restPlusmaskPath = [UIBezierPath bezierPathWithRoundedRect:restPlusBtn.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    UIBezierPath *restMinusmaskPath = [UIBezierPath bezierPathWithRoundedRect:restMinusBtn.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];
    
    CAShapeLayer *restPlusmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *restMinusmaskLayer = [[CAShapeLayer alloc] init];
    restPlusmaskLayer.frame = self.view.bounds;
    restPlusmaskLayer.path  = restPlusmaskPath.CGPath;
    restMinusmaskLayer.frame = self.view.bounds;
    restMinusmaskLayer.path  = restMinusmaskPath.CGPath;
    restPlusBtn.layer.mask = restPlusmaskLayer;
    restMinusBtn.layer.mask = restMinusmaskLayer;

    
    [customView addSubview:setsMinusBtn];
    [customView addSubview:setsTextField];
    [customView addSubview:setsPlusBtn];
    
    [customView addSubview:repMinusBtn];
    [customView addSubview:repsTextField];
    [customView addSubview:repPlusBtn];
    
    [customView addSubview:restMinusBtn];
    [customView addSubview:restTextField];
    [customView addSubview:restPlusBtn];
    
    UIView *customBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    
    CNPPopupButton *cancelButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.selectionHandler = ^(CNPPopupButton *button){
        [self showNotificationMessage:@"Info:" message:@"EXERCISE NOT ADDED" alertType:ViewAlertInfo];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *addButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cancelButton.frame) + 10, 0, 120, 40)];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    addButton.backgroundColor = [Utilities getAppColor];
    addButton.layer.cornerRadius = 5;
    addButton.selectionHandler = ^(CNPPopupButton *button){
        cSets = [setsTextField.text intValue];
        cReps = [repsTextField.text intValue];
        cRest = [restTextField.text intValue];
        if (_isTempExercise) {
            if ([Utilities createTempWorkoutForDate:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == nil) {
                [self showNotificationMessage:@"Success" message:@"TEMPORARY EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        } else {
            if ([Utilities addExerciseToWorkout:data date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup  customSet:cSets customRep:cReps customRest:cRest repsPerSet:[Utilities createRepsPerSet:cSets reps:cReps] restPerSet:[Utilities createRestPerSet:cSets rest:cRest]] == true) {
                NSLog(@"ADDING TO ROUTINE ****** %@", _exerciseGroup);
                [self showNotificationMessage:@"Success" message:@"EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        }
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *advance = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];
    [advance setTitleColor:FlatSkyBlue forState:UIControlStateNormal];
    advance.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [advance setTitle:@"Advance" forState:UIControlStateNormal];
    advance.layer.cornerRadius = 5;
    advance.backgroundColor = [UIColor whiteColor];
    advance.selectionHandler = ^(CNPPopupButton *button){
        cSets = [setsTextField.text intValue];
        cReps = [repsTextField.text intValue];
        cRest = [restTextField.text intValue];
        [self.popupController dismissPopupControllerAnimated:YES];
        [self changeRepsForThisSet:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup sets:cSets reps:cReps rest:cRest];
    };
    [customBtnView addSubview:cancelButton];
    [customBtnView addSubview:addButton];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, customView, advance,customBtnView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    self.popupController.theme.backgroundColor = FlatWhite;
    [self.popupController presentPopupControllerAnimated:YES];
}

-(IBAction)setsDec:(id)sender {
    int sets = [setsTextField.text intValue];
    
    if (sets <= 1) {
        return;
    }
    sets -= 1;
    setsTextField.text = [NSString stringWithFormat:@"%d", sets];
}
-(IBAction)setsInc:(id)sender {
    int sets = [setsTextField.text intValue];
    sets += 1;
    setsTextField.text = [NSString stringWithFormat:@"%d", sets];
}

-(IBAction)repsDec:(id)sender {
    int reps = [repsTextField.text intValue];
    
    if (reps <= 1) {
        return;
    }
    reps -= 1;
    repsTextField.text = [NSString stringWithFormat:@"%d", reps];
   
}
-(IBAction)repsInc:(id)sender {
    int reps = [repsTextField.text intValue];
    
    reps += 1;
    repsTextField.text = [NSString stringWithFormat:@"%d", reps];

}

-(IBAction)restDec:(id)sender {
    int rest = [restTextField.text intValue];
    
    if (rest <= 5) {
        return;
    }
    rest -= 5;
    restTextField.text = [NSString stringWithFormat:@"%d", rest];
}
-(IBAction)restInc:(id)sender {
    int rest = [restTextField.text intValue];
    rest += 5;
    restTextField.text = [NSString stringWithFormat:@"%d", rest];
}


- (void)changeRepsForThisSet:(NSString *)date
                 routineName:(NSString *)rName
                 workoutName:(NSString *)wName
                exerciseList:(ExerciseList *) data
               exerciseGroup:(NSNumber *) exGroup
          exerciseNumInGroup:(NSNumber *) exNumInGrp
                        sets:(int) sets
                        reps:(int) reps
                        rest:(int) rest {
    
    setCount = sets;
    [repArray removeAllObjects];
    [amarpArray removeAllObjects];
    [timerArray removeAllObjects];
    
    for (int i = 0; i < sets ; i++) {
        [repArray addObject:[NSNumber numberWithInt:reps]];
        [amarpArray addObject:@0];
        [timerArray addObject:[NSNumber numberWithInt:rest]];
    }
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:data.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UIView *setsHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    
    setsValue = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 150, 30)];
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    
    UIStepper *setsStepper = [[UIStepper alloc] init];
    setsStepper.value = sets;
    setsStepper.minimumValue = 1;
    setsStepper.maximumValue = 20;
    setsStepper.frame = CGRectMake(CGRectGetMaxX(setsHeader.frame) - 100, 0, 100, 30);
    [setsStepper addTarget:self action:@selector(setsValueChanged:) forControlEvents:UIControlEventValueChanged];
    [setsHeader addSubview:setsValue];
    [setsHeader addSubview:setsStepper];
    
    UIView *customHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 300, 30)];
    UILabel *setN  = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 55, 30)];
    setN.text = @"Set #";
    setN.textAlignment = NSTextAlignmentCenter;
    //setN.backgroundColor = FlatRedDark;
    
    
    UILabel *setsNum = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setN.frame), 0, CGRectGetWidth(customHeader.frame)/2 + 10, 30)];
    setsNum.text = @"Per Set";
    setsNum.textAlignment = NSTextAlignmentCenter;
    
    UILabel *maxRep  = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(setsNum.frame), 0, CGRectGetWidth(customHeader.frame)/4, 30)];
    maxRep.text = @"AMRAP";
    maxRep.textAlignment = NSTextAlignmentCenter;
    
    
    [customHeader addSubview:setN];
    [customHeader addSubview:setsNum];
    [customHeader addSubview:maxRep];
    float itemHeight = 35, defaultSets = 5;
    
    customRepsPerSet = [[UITableView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(maxRep.frame), 0, 320, (itemHeight + 5) * defaultSets) style:UITableViewStyleGrouped];
    [customRepsPerSet registerClass:[UITableViewCell self] forCellReuseIdentifier:@"RepsCell"];
    customRepsPerSet.backgroundColor = [UIColor clearColor];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    self.customRepsPerSet.delegate = self;
    self.customRepsPerSet.dataSource = self;
        
    UIView *customBtnView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 250, 40)];

    CNPPopupButton *cancelButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 120, 40)];
    [cancelButton setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    cancelButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [cancelButton setTitle:@"Cancel" forState:UIControlStateNormal];
    cancelButton.backgroundColor = [UIColor clearColor];
    cancelButton.selectionHandler = ^(CNPPopupButton *button){
        [self showNotificationMessage:@"Info:" message:@"EXERCISE NOT ADDED" alertType:ViewAlertInfo];
        [self.popupController dismissPopupControllerAnimated:YES];
    };
    
    CNPPopupButton *addButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(cancelButton.frame) + 10, 0, 120, 40)];
    [addButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    addButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [addButton setTitle:@"Add" forState:UIControlStateNormal];
    addButton.backgroundColor = [Utilities getAppColor];
    addButton.layer.cornerRadius = 5;
    addButton.selectionHandler = ^(CNPPopupButton *button){
        if (_isTempExercise) {
            if ([Utilities createTempWorkoutForDate:_date routineName:_routineName workoutName:_workoutName exerciseList:data exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup customSet:setCount customRep:reps customRest:rest repsPerSet:[self createRepsPerSet] restPerSet:[self createRestPerSet]] == nil) {
                [self showNotificationMessage:@"Success" message:@"TEMPORARY EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        } else {
            if ([Utilities addExerciseToWorkout:data date:_date routineN:_routineName workoutN:_workoutName exerciseGroup:_exerciseGroup exerciseNumInGroup:_exerciseNumInGroup  customSet:setCount customRep:reps customRest:rest repsPerSet:[self createRepsPerSet] restPerSet:[self createRestPerSet]] == true) {
                NSLog(@"ADDING TO ROUTINE ****** %@", _exerciseGroup);
                [self showNotificationMessage:@"Success" message:@"EXERCISE ADDED" alertType:ViewAlertSuccess];
                _exerciseGroup = [NSNumber numberWithInt:[_exerciseGroup intValue] + 1];
            } else {
                [self showNotificationMessage:@"Error" message:@"EXERCISE ALREADY EXIST" alertType:ViewAlertError];
            }
        }
        [self.popupController dismissPopupControllerAnimated:YES];
    };

    
    [customBtnView addSubview:cancelButton];
    [customBtnView addSubview:addButton];
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, setsHeader, customHeader, self.customRepsPerSet, customBtnView]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    [self.popupController presentPopupControllerAnimated:YES];
}
-(IBAction)setsValueChanged:(id)sender {
    UIStepper *setsVal = (UIStepper *)sender;
    int sets = setsVal.value;
    
    if (sets < setCount) {
        // remove sets
        [repArray removeObjectAtIndex:setCount - 1];
        [amarpArray removeObjectAtIndex:setCount - 1];
        [timerArray removeObjectAtIndex:setCount - 1];
    } else if (sets > setCount){
        // add sets
        [repArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRepsValue]]];
        [amarpArray addObject:@0];
        [timerArray addObject:[NSNumber numberWithInt:[Utilities getDefaultRestTimerValue]]];
    }
    setCount = sets;
    setsValue.text = [NSString stringWithFormat:@"%d SETS", sets];
    [self.customRepsPerSet reloadData];
}
-(IBAction)repTableDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"decreasing rep %ld", (long)indexPath.row);
        if ([[repArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] - 1)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)repTableInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rep %ld", (long)indexPath.row);
        [repArray replaceObjectAtIndex:indexPath.row withObject:@([[repArray objectAtIndex:indexPath.row] intValue] + 1)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)amarpIsChanged:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    NSLog(@"AMARP changed %ld", (long)indexPath.row);
    if (indexPath != nil)
    {
        if ([[amarpArray objectAtIndex:indexPath.row] boolValue] == false) {
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@1];//true
        } else
            [amarpArray replaceObjectAtIndex:indexPath.row withObject:@0];
    }
    [self.customRepsPerSet reloadData];
}

-(IBAction)timerTableDec:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    
    if (indexPath != nil)
    {
        NSLog(@"decreasing rest %ld", (long)indexPath.row);
        if ([[timerArray objectAtIndex:indexPath.row] intValue] == 0)
            return;
        
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] - 5)];
    }
    [self.customRepsPerSet reloadData];
}
-(IBAction)timerTableInc:(id)sender {
    CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:self.customRepsPerSet];
    NSIndexPath *indexPath = [self.customRepsPerSet indexPathForRowAtPoint:buttonPosition];
    if (indexPath != nil)
    {
        NSLog(@"increateong rest %ld", (long)indexPath.row);
        [timerArray replaceObjectAtIndex:indexPath.row withObject:@([[timerArray objectAtIndex:indexPath.row] intValue] + 5)];
    }
    [self.customRepsPerSet reloadData];
}
-(NSString *) createRepsPerSet {
    NSString *repsPerSet = @"";
    
    for (int i = 0; i < repArray.count; i++) {
        if ([[amarpArray objectAtIndex:i] boolValue] == true) {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = @"MAX";//[NSString stringWithFormat:@"%d", [repArray objectAtIndex:i]];
            } else  {
                repsPerSet = [NSString stringWithFormat:@"%@,MAX", repsPerSet];
            }
        }
        else {
            if ([repsPerSet isEqualToString:@""]) {
                repsPerSet = [NSString stringWithFormat:@"%@", [repArray objectAtIndex:i]];
            } else {
                repsPerSet = [NSString stringWithFormat:@"%@,%@", repsPerSet, [repArray objectAtIndex:i]];
            }
        }
        
    }
    return repsPerSet;
}

-(NSString *) createRestPerSet {
    NSString *restPerSet = @"";
    
    for (int i = 0; i < timerArray.count; i++) {
        if ([restPerSet isEqualToString:@""]) {
            restPerSet = [NSString stringWithFormat:@"%@", [timerArray objectAtIndex:i]];
        } else {
            restPerSet = [NSString stringWithFormat:@"%@,%@", restPerSet, [timerArray objectAtIndex:i]];
        }
    }
    return restPerSet;
}



@end
