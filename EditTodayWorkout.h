//
//  EditTodayWorkout.h
//  gyminutes
//
//  Created by Mayank Verma on 6/13/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface EditTodayWorkout : UIViewController   <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, UITextFieldDelegate, UIScrollViewDelegate, UIGestureRecognizerDelegate, MPCoachMarksViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView, *customRepsTableView;
@property (nonatomic, retain) NSString *date;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *workoutName;


@end
