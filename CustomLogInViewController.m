/*
 *  Copyright (c) 2014, Parse, LLC. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Parse.
 *
 *  As with any software that integrates with the Parse platform, your use of
 *  this software is subject to the Parse Terms of Service
 *  [https://www.parse.com/about/terms]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#import "CustomLogInViewController.h"
#import "Chameleon.h"
#import <QuartzCore/QuartzCore.h>
#import "commons.h"

@interface CustomLogInViewController ()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@property (nonatomic, strong)   UILabel *label, *sublabel;
@end

@implementation CustomLogInViewController
@synthesize fieldsBackground, label, sublabel;


- (void)viewDidLoad {
    [super viewDidLoad];

   // NSDictionary *underlineAttribute = @{NSUnderlineStyleAttributeName: @(NSUnderlineStyleSingle)};
    
    [self.logInView setBackgroundColor:[UIColor whiteColor]];
    [self.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconLogoNoBorder"]]];
    
    label = [[UILabel alloc] init];
    label.textColor = FlatOrangeDark;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:40.0f];
    label.textAlignment = NSTextAlignmentRight;
//    NSMutableAttributedString *string = [[NSMutableAttributedString alloc] initWithString:@"gym"];
//    [string addAttribute:NSUnderlineStyleAttributeName value:[NSNumber numberWithInt:1]  range:(NSRange){0,[string length]}];
//    label.attributedText = [string copy];
        label.text = @"GYM";
    
    sublabel = [[UILabel alloc] init];
//    sublabel.attributedText = [[NSAttributedString alloc] initWithString:@"inutes" attributes:underlineAttribute];
    sublabel.text = @"INUTES";
    sublabel.textColor = FlatOrangeDark;//[UIColor whiteColor];
    sublabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:40.0f];
    sublabel.textAlignment = NSTextAlignmentLeft;

    [self.logInView addSubview:label];
    [self.logInView addSubview:sublabel];
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.logInView.signUpButton setTitle:@"Signup" forState:UIControlStateNormal];
    // Set frame for elements
    int width  = CGRectGetWidth(self.view.frame);
    int startx = width/3;
    
    //logo
    [self.logInView.dismissButton setFrame:CGRectMake(10.0f, 10.0f, 87.5f, 45.5f)];
    NSString *deviceModel = [Utilities getIphoneName];
    NSLog(@"device model is %@", deviceModel);
    if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"]) {
        NSLog(@"iphone 4 or 4s");
        [self.logInView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"]) {
        NSLog(@"iphone 5 or 5s or SE");
        [self.logInView.logo setFrame:CGRectMake(startx, 50.0f, startx, startx)];
    } else if ([deviceModel containsString:@"Plus"]) {
        NSLog(@"iphone 6 plus");
        [self.logInView.logo setFrame:CGRectMake(startx, 100.0f, startx, startx)];
    } else if ([deviceModel containsString:@"6"] || [deviceModel containsString:@"7"]) {
        NSLog(@"iphone 6");
        [self.logInView.logo setFrame:CGRectMake(startx, 100.0f, startx, startx)];
    } else if ([deviceModel containsString:@"iPod"]) {
        [self.logInView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"iPad"]) {
        [self.logInView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"Simulator"]) {
        [self.logInView.logo setFrame:CGRectMake(startx, 50.0f, startx, startx)];
    }


    //logo titile
    [label setFrame:CGRectMake(0, CGRectGetMaxY(self.logInView.logo.frame), CGRectGetWidth(self.view.frame)/2 - 20, 50.0f)];
    [sublabel setFrame:CGRectMake(CGRectGetMaxX(label.frame), CGRectGetMaxY(self.logInView.logo.frame), CGRectGetWidth(self.view.frame), 50.0f)];
    
    //username and password field
    [self.logInView.usernameField setFrame:CGRectMake(0, CGRectGetMaxY(self.logInView.logo.frame) + 70, width, 50.0f)];
    [self.logInView.passwordField setFrame:CGRectMake(0, CGRectGetMaxY(self.logInView.usernameField.frame), width, 50.0f)];
    self.logInView.usernameField.backgroundColor= FlatOrangeDark;
    self.logInView.passwordField.backgroundColor = FlatOrangeDark;
    
    [self.logInView.logInButton setFrame:CGRectMake(0, CGRectGetMaxY(self.logInView.passwordField.frame), width, 50.0f)];
    [self.logInView.passwordForgottenButton setFrame:CGRectMake(0, CGRectGetMaxY(self.logInView.logInButton.frame), width, 50.0f)];
    self.logInView.passwordForgottenButton.titleLabel.textColor = [UIColor whiteColor];

//    [self.logInView.signUpButton setBackgroundColor:FlatNavyBlueDark];
//    [self.logInView.signUpButton setTitleColor:FlatNavyBlueDark forState:UIControlStateNormal];
//    [self.logInView.signUpButton setTitleShadowColor:FlatNavyBlueDark forState:UIControlStateHighlighted];
    
}

@end
