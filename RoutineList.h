//
//  WorkoutListView.h
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "MPCoachMarks.h"

@interface RoutineList : UIViewController <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, MPCoachMarksViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;

@end
