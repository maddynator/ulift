//
//  WorkoutStore.h
//  gyminutes
//
//  Created by Mayank Verma on 7/31/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "uLiftIAPHelper.h"
#import <StoreKit/StoreKit.h>

@interface WorkoutStore : UIViewController <UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) UIRefreshControl *refreshControl;



@end
