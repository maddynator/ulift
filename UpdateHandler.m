//
//  UpdateHandler.m
//  gyminutes
//
//  Created by Mayank Verma on 6/21/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "UpdateHandler.h"

@implementation UpdateHandler
+(void) v2_update_UserProfile_membershipType {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    UserProfile *profileData = [UserProfile MR_findFirstInContext:localContext];
    if (profileData.membershipType == nil) {
        [self updateUserProfileWithMemberShipInfo];
    }
}

+(void) updateUserProfileWithMemberShipInfo {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    UserProfile *profileData = [UserProfile MR_findFirstInContext:localContext];
    profileData.membershipType = [self getMembershipType];
    profileData.deviceType = [Utilities getIphoneName];
    [localContext MR_saveToPersistentStoreAndWait];
    
    PFQuery *query = [PFQuery queryWithClassName:@P_USERPROFILE_CLASS];
    [query whereKey:@"UserId" equalTo:[PFUser currentUser].objectId];
    [query setLimit:1];
    [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
        if (task.error) {
            NSLog(@"Error: %@", task.error);
            return task;
        }
        PFObject *data;
        data = [task.result objectAtIndex:0];
        data[@"UserId"] = [PFUser currentUser].objectId;
        data[@"Age"] = (profileData.age == 0) ? @18: profileData.age;
        data[@"Sex"] = ([profileData.sex boolValue] == false) ? @NO: @YES;
        data[@"Name"] = profileData.userName;
        data[@"Weight"] = profileData.weight;
        data[@"Height"] = profileData.height;
        data[@"ExperienceLevel"] = profileData.experienceLevel;
        data[@"Units"] = ([profileData.units boolValue] == false) ? @NO: @YES;
        data[@"MembershipType"] = [UpdateHandler getMembershipType];
        data[@"DeviceType"] = [Utilities getIphoneName];
        [data saveEventually];
        
        return task;
    }];
    NSLog(@"updated on parse....");

}

+(NSString *) getMembershipType {
    bool isPremium = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPremium"];
    bool isData = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedDataPackage"];
    bool isWorkout = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedWorkoutPackage"];
    bool isRoutine = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPowerRoutinePackage"];
    bool isAnalytics = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedAnalyticsPackage"];

    bool isEarlyBird = false;

    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
    [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
    [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];

    NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSDate *accountDate = [PFUser currentUser].createdAt;
    if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
        isEarlyBird = true;
    }

    NSString *memberShipStatus = @"FREE";
    if (isEarlyBird == true) {
        memberShipStatus = @"EARLY BIRD";
    } else {
        if (isPremium == false && isData == false && isWorkout == false && isRoutine == false)
            memberShipStatus = @"FREE";
        
        // 1 0 0 0
        if (isData && !isRoutine && !isWorkout && !isAnalytics) {
            memberShipStatus = @"DATA PACK";
            
        }
        
        // 0 1 0 0
        if (!isData && isRoutine && !isWorkout && !isAnalytics) {
            memberShipStatus = @"ROUTINE PACK";
            
        }
        
        // 0 0 1 0
        if (!isData && !isRoutine && isWorkout && !isAnalytics) {
            memberShipStatus = @"WORKOUT PACK";
            
        }
        
        // 0 0 0 1
        if (!isData && !isRoutine && !isWorkout && isAnalytics) {
            memberShipStatus = @"ANALYTICS PACK";
            
        }
        
        // 1100
        if (isData && isRoutine && !isWorkout && !isAnalytics) {
            memberShipStatus = @"DATA, ROUTINE PACK";
            
        }
        // 1010
        if (isData && !isRoutine && isWorkout && !isAnalytics) {
            memberShipStatus = @"DATA, WORKOUT PACK";
            
        }
        // 1001
        if (isData && !isRoutine && !isWorkout && isAnalytics) {
            memberShipStatus = @"DATA, ANALYTICS PACK";
            
        }
        
        // 0011
        if (!isData && !isRoutine && isWorkout && isAnalytics) {
            memberShipStatus = @"WORKOUT, ANALYTICS PACK";
            
        }
        // 0101
        if (!isData && isRoutine && !isWorkout && isAnalytics) {
            memberShipStatus = @"ROUTINE, ANALYTICS PACK";
        }
        // 0110
        if (!isData && isRoutine && isWorkout && !isAnalytics) {
            memberShipStatus = @"ROUTINE, WORKOUT PACK";
            
        }
        
        // 1110
        if (isData && isRoutine && isWorkout && !isAnalytics) {
            memberShipStatus = @"DATA, ROUTINE, WORKOUT PACK";
            
            
        }
        // 1101
        if (isData && isRoutine && !isWorkout && isAnalytics) {
            memberShipStatus = @"DATA, ROUTINE, ANALYTICS PACK";
            
        }
        // 1011
        if (isData && !isRoutine && isWorkout && isAnalytics) {
            memberShipStatus = @"DATA, WORKOUT, ANALYTICS PACK";
            
        }
        // 0111
        if (!isData && isRoutine && isWorkout && isAnalytics) {
            memberShipStatus = @"ROUTINE, WORKOUT, ANALYTICS PACK";
            
        }
        
        if (isData && isRoutine && isWorkout && isAnalytics) {
            memberShipStatus = @"PREMIUM PACK";
        }
        

        if (isPremium) {
            memberShipStatus = @"PREMIUM PACK";
        }
    }
    
    NSLog(@"MemberShip %@", memberShipStatus);
    return memberShipStatus;
}

+(void) v2_4_update_UserWorkout_to_AMRAP {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *workoutArray = [UserWorkout MR_findAllInContext:localContext];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (UserWorkout *workout in workoutArray) {
            // if we have already updated them, we dont do it again
            if (workout.restPerSet != nil && workout.repsPerSet != nil) {
                NSLog(@"AMRAP: No Need to sync");
                continue;
            }
            
            workout.restPerSet = [Utilities createRestPerSet:[workout.sets intValue] rest:[workout.restTimer intValue]];
            workout.repsPerSet = [Utilities createRepsPerSet:[workout.sets intValue] reps:[workout.reps intValue]];
            workout.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
        }
        
    } completion:^(BOOL contextDidSave, NSError *error) {
        //NSLog(@"count of set saved... %lu", (unsigned long)[ExerciseSet MR_countOfEntities]);
        NSLog(@"All user workout updated. We need to update to parse");
        [Utilities syncWorkoutInfo:self];
    }];
}
+(void) v2_4_update_WorkoutList_to_AMRAP {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *workoutArray = [WorkoutList MR_findAllInContext:localContext];
    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
        for (WorkoutList *workout in workoutArray) {
            // if we have already updated them, we dont do it again
//            if (workout.restPerSet != nil && workout.repsPerSet != nil) {
//                NSLog(@"skipping this not sure why %@", workout);
//                continue;
//            }

            workout.restPerSet = [Utilities createRestPerSet:[workout.setsSuggested intValue] rest:[workout.restSuggested intValue]];
            workout.repsPerSet = [Utilities createRepsPerSet:[workout.setsSuggested intValue] reps:[workout.repsSuggested intValue]];
        }
    } completion:^(BOOL contextDidSave, NSError *error) {
        NSLog(@"All workout list updated.");
    }];

}

@end
