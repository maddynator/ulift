//
//  uLiftIAPHelper.m
//  uLift
//
//  Created by Mayank Verma on 9/10/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "uLiftIAPHelper.h"
#import "commons.h"

@implementation uLiftIAPHelper

+(uLiftIAPHelper *)sharedInstance {

    static dispatch_once_t once;
    static uLiftIAPHelper * sharedInstance;

    dispatch_once(&once, ^{
        NSMutableSet *products = [[NSMutableSet alloc] init];
        
        // adding specific one for Premium Package.
        [products addObject:@IAP_PREMIUM_PACKAGE_ID];
        [products addObject:@IAP_WORKOUT_PACKAGE_ID];
        [products addObject:@IAP_DATA_PACKAGE_ID];
        [products addObject:@IAP_ROUTINE_PACKAGE_ID];
        [products addObject:@IAP_ANALYTICS_PACKAGE_ID];
        
        [products addObject:@IAP_ROUTINE_BODYWEIGHT_MAX];
        [products addObject:@IAP_ROUTINE_HYPERTROPHY_UNLIMITED];
        [products addObject:@IAP_ROUTINE_STRONGLIFT5X5];
        [products addObject:@IAP_ROUTINE_2DAYSPLITPUSHPULL];
        [products addObject:@IAP_ROUTINE_3DAYSPLIT];
        [products addObject:@IAP_ROUTINE_4DAYSPLIT];
        [products addObject:@IAP_ROUTINE_5DAYSPLIT];
        [products addObject:@IAP_ROUTINE_6DAYSPLIT];
        [products addObject:@IAP_ROUTINE_GVT_I];
        [products addObject:@IAP_ROUTINE_GVT_II];
        [products addObject:@IAP_ROUTINE_3DAYDUMBBELLDOMINATION];
        [products addObject:@IAP_ROUTINE_FOCUSSED_FOUR];


        
        /*
         UNCOMMENT THIS WHEN DYNAMIC WORKOUTS ARE ADDED.
        PFQuery *query = [PFQuery queryWithClassName:@P_IAP_ROUTINES];
        NSArray *bundleArray = [query findObjects];
        for (PFObject *data in bundleArray) {
            [products addObject:data[@"RoutineBundleID"]];
        }
         */
        
        NSSet *pId = [NSSet setWithSet:products];
        sharedInstance = [[self alloc] initWithProductIdentifiers:pId];        
    });
    return sharedInstance;
}
@end
