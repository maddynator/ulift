//
//  CreateWorkoutDay.h
//  uLift
//
//  Created by Mayank Verma on 8/19/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface CreateWorkoutDay : UIViewController <UITableViewDataSource, UITableViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, MPCoachMarksViewDelegate, UICollectionViewDelegate, UICollectionViewDataSource, ChartViewDelegate>

@property (nonatomic, retain) IBOutlet UITableView *tableView;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *routineBundleId;
@property (nonatomic, retain) UIColor *routineColor;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;
@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) IBOutlet LineChartView *lineChartView;
@property (nonatomic, strong) IBOutlet PieChartView *pieChartView;

@end
