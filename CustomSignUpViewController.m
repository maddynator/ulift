/*
 *  Copyright (c) 2014, Parse, LLC. All rights reserved.
 *
 *  You are hereby granted a non-exclusive, worldwide, royalty-free license to use,
 *  copy, modify, and distribute this software in source code or binary form for use
 *  in connection with the web services and APIs provided by Parse.
 *
 *  As with any software that integrates with the Parse platform, your use of
 *  this software is subject to the Parse Terms of Service
 *  [https://www.parse.com/about/terms]. This copyright notice shall be
 *  included in all copies or substantial portions of the software.
 *
 *  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 *  FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 *  COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 *  IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 *  CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#import "CustomSignUpViewController.h"
#import "commons.h"

NSString *disc = @"By signing up, you agree to our Terms of Services and Privacy Policy.";

@interface CustomSignUpViewController()
@property (nonatomic, strong) UIImageView *fieldsBackground;
@property (nonatomic, strong) UILabel *label, *disclosure, *sublabel;
@end
@implementation CustomSignUpViewController

@synthesize fieldsBackground, label, disclosure, sublabel;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.signUpView setBackgroundColor:[UIColor whiteColor]];
    [self.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconLogoNoBorder"]]];
    

    label = [[UILabel alloc] init];
    label.textColor = FlatOrangeDark;
    label.font = [UIFont fontWithName:@"HelveticaNeue-Bold" size:40.0f];
    label.text = @"GYM";
    label.textAlignment = NSTextAlignmentRight;

    sublabel = [[UILabel alloc] init];
    //    sublabel.attributedText = [[NSAttributedString alloc] initWithString:@"inutes" attributes:underlineAttribute];
    sublabel.text = @"INUTES";
    sublabel.textColor = FlatOrangeDark;
    sublabel.font = [UIFont fontWithName:@"HelveticaNeue-Thin" size:40.0f];
    sublabel.textAlignment = NSTextAlignmentLeft;

    
    disclosure = [[UILabel alloc] init];
    disclosure.textColor = FlatOrangeDark;
    disclosure.font = [UIFont fontWithName:@"HelveticaNeue" size:14.0f];
    disclosure.text = disc;
    disclosure.textAlignment = NSTextAlignmentCenter;
    disclosure.numberOfLines = 2;
    [self.signUpView addSubview:label];
    [self.signUpView addSubview:sublabel];
    [self.signUpView addSubview:disclosure];
}

-(void) viewDidAppear:(BOOL)animated {
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    [alert addButton:@"Terms of Services" actionBlock:^{
        NSString *url=@"http://gyminutesapp.com/terms.html";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];
    
    [alert addButton:@"Privacy Policy" actionBlock:^{
        NSString *url=@"http://gyminutesapp.com/privacy.html";
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }];

    [alert showInfo:self title:@"EULA" subTitle:disc closeButtonTitle:@"I Agree" duration:0.0f];
   
}
- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    [self.signUpView.signUpButton setTitle:@"Signup" forState:UIControlStateNormal];
    
    // Set frame for elements
    int width  = CGRectGetWidth(self.view.frame);
    int startx = width/3;
    
    //logo

    //[self.signUpView.logo setFrame:CGRectMake(startx, 100.0f, startx, startx)];
    NSString *deviceModel = [Utilities getIphoneName];
    NSLog(@"device model is %@", deviceModel);
    if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"]) {
        NSLog(@"iphone 4 or 4s");
        [self.signUpView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"]) {
        NSLog(@"iphone 5 or 5s or SE");
        [self.signUpView.logo setFrame:CGRectMake(startx, 50.0f, startx, startx)];
    } else if ([deviceModel containsString:@"Plus"]) {
        NSLog(@"iphone 6 plus");
        [self.signUpView.logo setFrame:CGRectMake(startx, 100.0f, startx, startx)];
    } else if ([deviceModel containsString:@"6"]) {
        NSLog(@"iphone 6");
        [self.signUpView.logo setFrame:CGRectMake(startx, 100.0f, startx, startx)];
    } else if ([deviceModel containsString:@"iPod"]) {
        [self.signUpView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"iPad"]) {
        [self.signUpView.logo setFrame:CGRectMake(startx, 50.0f, 0, 0)];
    } else if ([deviceModel containsString:@"Simulator"]) {
        [self.signUpView.logo setFrame:CGRectMake(startx, 50.0f, startx, startx)];
    }
    
    //logo titile
    [label setFrame:CGRectMake(0, CGRectGetMaxY(self.signUpView.logo.frame), CGRectGetWidth(self.view.frame)/2 - 20, 50.0f)];
    [sublabel setFrame:CGRectMake(CGRectGetMaxX(label.frame), CGRectGetMaxY(self.signUpView.logo.frame), CGRectGetWidth(self.view.frame), 50.0f)];
    
    //username and password field
    [self.signUpView.usernameField setFrame:CGRectMake(0, CGRectGetMaxY(self.signUpView.logo.frame) + 70, width, 50.0f)];
    [self.signUpView.passwordField setFrame:CGRectMake(0, CGRectGetMaxY(self.signUpView.usernameField.frame), width, 50.0f)];
    self.signUpView.usernameField.backgroundColor= FlatOrangeDark;
    self.signUpView.passwordField.backgroundColor = FlatOrangeDark;
    
    [self.signUpView.signUpButton setFrame:CGRectMake(0, CGRectGetMaxY(self.signUpView.passwordField.frame), width, 50.0f)];
    
    [disclosure setFrame:CGRectMake(5, CGRectGetHeight(self.view.frame) - 50, CGRectGetWidth(self.view.frame) - 10, 40.0f)];
}


@end
