//
//  ExerciseHistoryTVC.m
//  uLift
//
//  Created by Mayank Verma on 7/6/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "ExerciseHistoryTVC.h"
NSArray *exerciseHistory;

@interface ExerciseHistoryTVC ()
@property(retain) NSMutableArray* tableViewSections;
@property(retain) NSMutableDictionary* tableViewCells;
@property(retain) NSMutableDictionary* tableViewCellsListing;

@end

@implementation ExerciseHistoryTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    exerciseHistory = [Utilities getSetForExercise:_exerciseObj];
    [self setupDataSource:exerciseHistory];
    NSLog(@"count of objs are %ld", (unsigned long)[exerciseHistory count]);
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.navigationController.navigationBarHidden = NO;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.text = @"History";
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont systemFontOfSize:10];
    subTitleLabel.text = _exerciseObj;
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.tableViewSections.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    id key = [self.tableViewSections objectAtIndex:section];
    NSArray* tableViewCellsForSection = [self.tableViewCells objectForKey:key];
    NSLog(@"returning number of items: %lu", (unsigned long)tableViewCellsForSection.count);
    return tableViewCellsForSection.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    // Configure the cell...
    id key = [self.tableViewSections objectAtIndex:indexPath.section];
    NSArray* tableViewCellsForSection = [self.tableViewCells objectForKey:key];
    long newIndex = [tableViewCellsForSection count] -  indexPath.row;
    
    // we are using newIndex because the data is sorted in descending order by exerciseNumber. We cant get it ascending order because date gets messed up. So we are tricking it.
    
    ExerciseSet *data = [tableViewCellsForSection objectAtIndex:newIndex - 1];//indexPath.row];
    
    UILabel *counter = (UILabel *) [cell viewWithTag:201];
    UILabel *weight = (UILabel *) [cell viewWithTag:202];
    UILabel *rep = (UILabel *) [cell viewWithTag:203];
    
    
    counter.text = [NSString stringWithFormat:@"%ld", indexPath.row + 1];
    weight.text = [data.weight stringValue];
    rep.text = [data.rep stringValue];
    if ([data.isDropSet boolValue])
        cell.backgroundColor = FlatMintDark;
    
    return cell;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
//    NSLog(@"%@\n%@\n%@", self.tableViewSections, self.tableViewCells, self.tableViewCellsListing);
    return [self.tableViewSections objectAtIndex:section];
}

- (void) setupDataSource:(NSArray*)sortedDateArray
{
    self.tableViewSections = [NSMutableArray arrayWithCapacity:0];
    self.tableViewCells = [NSMutableDictionary dictionaryWithCapacity:0];
    self.tableViewCellsListing = [NSMutableDictionary dictionaryWithCapacity:0];
    
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.locale = [NSLocale currentLocale];
    dateFormatter.timeZone = calendar.timeZone;
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    
    NSUInteger dateComponents = NSCalendarUnitYear | NSCalendarUnitMonth  | NSCalendarUnitDay;
    NSInteger previousYear = -1;
    NSInteger previousMonth = -1;
    NSInteger previousDay = -1;
    NSMutableArray* tableViewCellsForSection = nil;
    for (ExerciseSet *data in sortedDateArray)
    {
        // Convert string to date object
        NSLog(@"Date is %@", data.date);
        NSDate *date = [dateFormatter dateFromString:data.date];
        NSDateComponents* components = [calendar components:dateComponents fromDate:date];
        NSInteger year = [components year];
        NSInteger month = [components month];
        NSInteger day = [components day];

        if (day != previousDay || year != previousYear || month != previousMonth)
        {
            NSString* sectionHeading = [[dateFormatter stringFromDate:date] uppercaseString];
            [self.tableViewSections addObject:sectionHeading];
            tableViewCellsForSection = [NSMutableArray arrayWithCapacity:0];
            [self.tableViewCells setObject:tableViewCellsForSection forKey:sectionHeading];
            //[self.tableViewCellsListing setObject:houseData forKey:sectionHeading];
            previousYear = year;
            previousMonth = month;
            previousDay = day;
        }
        [tableViewCellsForSection addObject:data];//date
    }
    
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"No exercise history availble.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Till date, no sets have been recorded for this exercises.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;//[UIImage imageNamed:@"Icon_Home"];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}


@end
