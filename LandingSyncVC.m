//
//  LandingSyncVC.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "LandingSyncVC.h"
#import <Parse/Parse.h>
#import <ParseUI/ParseUI.h>
#import <Bolts/Bolts.h>
//#import <PFFa>
#import "commons.h"
#import <ParseTwitterUtils/PF_Twitter.h>
#import "EAIntroView.h"
#import "SMPageControl.h"

@interface LandingSyncVC () <EAIntroDelegate> {
    EAIntroView *_intro;
}

@end

@implementation LandingSyncVC
@synthesize logout;

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

-(void) viewDidAppear:(BOOL)animated {
    NSLog(@"view appeared ... and user %@", [PFUser currentUser]);
    if (![PFUser currentUser] && ![PFFacebookUtils isLinkedWithUser:[PFUser currentUser]] && ![PFTwitterUtils isLinkedWithUser:[PFUser currentUser]]) {
        self.navigationController.navigationBar.hidden = false;
        NSLog(@"current user is null");
        NSArray *headerArray = @[@"THE SWISS ARMY KNIFE OF WORKOUT TRACKING",
                                 @"BUILD ROUTINE" ,
                                 @"TRACK WORKOUT",
                                 @"ANALYZE PROGRESS"];
        
        NSArray *dataArray = @[@"\nAll feature you will ever need for workout tracking in one place.",
                               @"Choose from 600+ exercises to create your own workout. Super Set, Tri Set, Drop Set and Circuit training support integrated in the build tool.",
                               @"Log workouts with ease. Workout information pre-populated to minimize distraction and achieve fitness goals.",
                               @"Get insight into your workout by analyzing 100+ data points extracted by our built in Analytics Engine."];
        
        NSString *deviceType = [Utilities getIphoneName];
        
        float titleIconPosY = self.view.bounds.size.height/4;
        float descPosY = self.view.bounds.size.height/3;
        float titlePosY = self.view.bounds.size.height/3 - 30;
        EAIntroPage *page1 = [EAIntroPage page];
        page1.title = [headerArray objectAtIndex:0];
        page1.desc = [dataArray objectAtIndex:0];
        page1.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconLogoNoBorder"]];
        page1.titleColor = [Utilities getAppColor];
        page1.descColor = [Utilities getAppColor];
        page1.descAlignment = NSTextAlignmentJustified;
        page1.bgColor = [UIColor whiteColor];
        page1.descPositionY = descPosY;
        page1.titlePositionY = titlePosY;
        page1.titleIconPositionY = titleIconPosY;
        
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            page1.titleIconView.frame = CGRectMake(80, 0, 100, 100);
            page1.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:22];
            page1.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        } else {
            page1.titleIconView.frame = CGRectMake(80, 100, 150, 150);
            page1.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:26];
            page1.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        }

        
        EAIntroPage *page2 = [EAIntroPage page];
        page2.title = [headerArray objectAtIndex:1];
        page2.desc = [dataArray objectAtIndex:1];
        page2.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconOnBoard1"]];
        page2.titleColor = [Utilities getAppColor];
        page2.descColor = [Utilities getAppColor];
        page2.descAlignment = NSTextAlignmentJustified;
        page2.bgColor = [UIColor whiteColor];
        page2.descPositionY = descPosY;
        page2.titlePositionY = titlePosY;
        page2.titleIconPositionY = titleIconPosY;
        
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            page2.titleIconView.frame = CGRectMake(80, 0, 100, 100);
            page2.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:22];
            page2.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        } else {
            page2.titleIconView.frame = CGRectMake(80, 0, 150, 150);
            page2.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:26];
            page2.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        }
        
        EAIntroPage *page3 = [EAIntroPage page];
        page3.title = [headerArray objectAtIndex:2];
        page3.desc = [dataArray objectAtIndex:2];
        page3.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconOnBoard2"]];
        page3.titleColor = [Utilities getAppColor];
        page3.descColor = [Utilities getAppColor];
        page3.descPositionY = descPosY;
        page3.titlePositionY = titlePosY;
        page3.titleIconPositionY = titleIconPosY;
        page3.descAlignment = NSTextAlignmentJustified;
        page3.bgColor = [UIColor whiteColor];
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            page3.titleIconView.frame = CGRectMake(80, 0, 100, 100);
            page3.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:22];
            page3.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        } else {
            page3.titleIconView.frame = CGRectMake(80, 0, 150, 150);
            page3.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:26];
            page3.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
        }


        EAIntroPage *page4 = [EAIntroPage page];
        page4.title = [headerArray objectAtIndex:3];
        page4.desc = [dataArray objectAtIndex:3];
        page4.titleIconView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconOnBoard3"]];
        page4.titleColor = [Utilities getAppColor];
        page4.descColor = [Utilities getAppColor];
        page4.descPositionY = descPosY;
        page4.titlePositionY = titlePosY;
        page4.titleIconPositionY = titleIconPosY;
        page4.descAlignment = NSTextAlignmentJustified;
        page4.bgColor = [UIColor whiteColor];
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            page4.titleIconView.frame = CGRectMake(80, 0, 100, 100);
            page4.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:22];
            page4.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:18];
        } else {
            page4.titleIconView.frame = CGRectMake(80, 0, 150, 150);
            page4.titleFont = [UIFont fontWithName:@HAL_BOLD_FONT size:26];
            page4.descFont = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            
        }
        
        EAIntroView *intro = [[EAIntroView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andPages:@[page1,page2,page3,page4]];
        [intro setDelegate:self];
        intro.skipButtonY = 10;
        [intro.skipButton setTitle:@"Skip" forState:UIControlStateNormal];
        [intro.skipButton setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
        
        intro.pageControlY = 25.f;
        
        [intro showInView:self.view animateDuration:0.3];
        [Utilities logout];
        
        } else {
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ParseSyncDone"] == false) {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ParseSyncDone"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            }
            
            [Utilities crashAnalyticsUserData];
            NSLog(@"====== > User already logged in... === > %@, %d", [PFUser currentUser].objectId, [[NSUserDefaults standardUserDefaults] boolForKey:@"MultipleSetSupport"]);
            if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MultipleSetSupport"] == false) {
                [ Utilities updateUserWorkoutToSetBasedSystem];
                NSLog(@"need to update sets info....");
            } else {
                NSLog(@"---> no need to update set info...");
            }

            [self performSegueWithIdentifier:@"loadMainSegue" sender:self];
            
        }
    
    
    // Do any additional setup after loading the view.
}

-(void) showLoginSignupView {
    PFLogInViewController *logInController = [[CustomLogInViewController alloc] init];
    logInController.fields = PFLogInFieldsUsernameAndPassword | PFLogInFieldsPasswordForgotten | PFLogInFieldsSignUpButton | PFLogInFieldsLogInButton | PFLogInFieldsFacebook;//| PFLogInFieldsTwitter;
    NSArray *permissionsArray = @[ @"user_about_me", @"public_profile", @"user_birthday", @"email"]; //we cannot ask read permission and publish permission together... so we will ask when user plan on publishing data.. @"publish_actions"
    
    logInController.facebookPermissions = permissionsArray;
    logInController.emailAsUsername = YES;
    logInController.delegate= self;
    
    
    PFSignUpViewController *signUpController = [[CustomSignUpViewController alloc] init];
    signUpController.fields = PFSignUpFieldsDefault;
    
    signUpController.emailAsUsername = YES;
    signUpController.delegate = self;
    signUpController.minPasswordLength = 8;
    
    logInController.signUpController = signUpController;
    
    [self presentViewController:logInController animated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(downloadAllMyStuffOnLogin) name:@"GyminutesDownloadLoginData" object:nil];
    [Utilities crashAnalyticsUserData];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)signUpViewController:(PFSignUpViewController *)signUpController didSignUpUser:(PFUser *)user {
    [Utilities syncAllExercises:self];
    [Utilities createUserProfileOnSignUp:self];
    [Utilities downloadIAPRoutineFromStore:@IAP_ROUTINE_BODYWEIGHT_MAX view:self];
    /*
    [PFCloud callFunctionInBackground:@"welcomeEmail" withParameters:@{@"templateName" : @"WelcomeSignup",
                                                                       @"toEmail" :[PFUser currentUser].email,
                                                                       @"toName": @"Hi"}
                                block:^(NSString *response, NSError *error) {
                                    if (!error) {
                                        NSLog(@"email successfully sent..");
                                    } else {
                                        NSLog(@"Error sending email: %@\n%@", error.description, error);
                                    }
                                }];
     */
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ParseSyncDone"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"SignupEmail" withParameters:articleParams];

    [self dismissViewControllerAnimated:YES completion:^{
        [self performSegueWithIdentifier:@"loadMainSegue" sender:self];
        // as this is a new signup, the new user will already be on multi Set Support.
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MultipleSetSupport"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        
        // as this a signup, we dont want to download login data o de-registering post notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];

    }];

}

- (void)signUpViewControllerDidCancelSignUp:(PFSignUpViewController *)signUpController {
    // Do nothing, as the view controller dismisses itself
        NSLog(@"signup cancelled");
    
    // Capture author info &  user status
    // Flurry Event
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"Date", [NSDate date],
                                   nil];
    [Flurry logEvent:@"SignUpCancelled" withParameters:articleParams];
    
}

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    NSLog(@"login in with facebook");

    if ([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]) {
        NSMutableDictionary* params = [NSMutableDictionary dictionary];
        [params setValue:@"id,name,email" forKey:@"fields"];

        FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                      initWithGraphPath:@"/me"
                                    parameters:params
                                      HTTPMethod:@"GET"];
        [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,
                                              id result,
                                              NSError *error) {
            // Handle the result
            NSLog(@"result is %@ email %@", result, [result objectForKey:@"email"]);
            
            if ([result objectForKey:@"email"] == nil) {
                NSLog(@"User did not give email... ");
                    NSLog(@"user atleast has a name and id %@", [result objectForKey:@"name"]);
                if ([result objectForKey:@"name"] != nil) {
                    [[PFUser currentUser] setUsername:[result objectForKey:@"name"]];
                    [[PFUser currentUser] saveInBackground];
                    [self facebookNewUserData:user name:[result objectForKey:@"name"] hasEmail:NO email:@""];
                } else {
                    [self facebookNewUserData:user name:@"" hasEmail:NO email:@""];
                }
            } else {
                PFQuery *checkEmail = [PFUser query];
                [checkEmail whereKey:@"email" equalTo:[result objectForKey:@"email"]];
                
                [checkEmail findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                    //      NSLog(@"**** got back here with objects %lu %ld", (long) [objects count], error.code);
                    if ([objects count] > 0 ) {
                        NSLog(@"found 1 user with this email address....");
                        [[PFUser currentUser] setEmail:[result objectForKey:@"email"]];
                        [[PFUser currentUser] saveInBackground];
                        // we are checking for > 0 but it should always return 1.
                        // Parse does not allow user to link to facebook unless they are usinged in using old account. So for us, a user will have to login using email and then if they decide, they can link their facebook account.. so next time when the come back, they should be able to login via facebook....
                    } else {
                        NSLog(@"Did not find user... setting name and email....");
                        // we will only land here if this is a new user that is using facebook to .
                        [[PFUser currentUser] setEmail:[result objectForKey:@"email"]];
                        [[PFUser currentUser] setUsername:[result objectForKey:@"email"]];
                        [[PFUser currentUser] saveInBackground];
                    }
                    
                    [self facebookNewUserData:user name:[result objectForKey:@"name"] hasEmail:YES email:[result objectForKey:@"email"]];
                    /*
                    // Store user full name as part of nsuserdefault data
                    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                    [defaults setObject:[result objectForKey:@"name"] forKey:@"userFullName"];
                    [defaults synchronize];
                    
                    if (user.isNew) {
                        [Utilities syncAllExercises:self];
                        [Utilities createUserProfileOnSignUp:self];
                        
                        [PFCloud callFunctionInBackground:@"sendTemplate" withParameters:@{@"templateName" : @"WelcomeSignup",
                                                                                           @"toEmail" :[result objectForKey:@"email"],
                                                                                           @"toName": [result objectForKey:@"name"]}
                                                    block:^(NSString *response, NSError *error) {
                                                        if (!error) {
                                                            NSLog(@"email successfully sent..");
                                                        }
                                                    }];
                        
                        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                       @"Date", [NSDate date],
                                                       @"UserID", [PFUser currentUser].objectId,
                                                       nil];
                        [Flurry logEvent:@"SignupFacebook" withParameters:articleParams];
                        // New user signing up using facebook will have multi set support.
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MultipleSetSupport"];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    } else {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"GyminutesDownloadLoginData"
                                                                            object:self
                                                                          userInfo:nil];
                    }
                     */
                }];
            }
        }];
    } else if ([PFTwitterUtils isLinkedWithUser:user]) {
        NSLog(@"Twitter: User new: %d emai:%@, %@", user.isNew, user.email, [PFTwitterUtils twitter]);
        

        NSString * twitterUserID = [PFTwitterUtils twitter].userId;
        NSString * twitterScreenName = [PFTwitterUtils twitter].screenName;

        NSString * urlString = @"https://api.twitter.com/1.1/users/show.json?";
        if (twitterUserID.length > 0) {
            urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"user_id=%@", twitterUserID]];
        } else if (twitterScreenName.length > 0) {
            urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"screen_name=%@", twitterScreenName]];
        } else {
            NSLog(@"There are no credentials for Twitter login. Something went really wrong !");
            return;
        }
        
        
        NSURL *verify = [NSURL URLWithString:urlString];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:verify];
        [[PFTwitterUtils twitter] signRequest:request];
        
        NSOperationQueue * queue = [[NSOperationQueue alloc] init];
        [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
            if ( connectionError == nil) {
                NSError * error = nil;
                NSDictionary* result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
                NSLog(@"twitter info result : %@",result);
                
//                NSString * profileImageURL = [result objectForKey:@"profile_image_url_https"];
//                if (profileImageURL.length > 0)
//                    [user setObject:profileImageURL forKey:@"picture"];
                
//                NSString * username = [result objectForKey:@"screen_name"];
//                if (username.length > 0)
//                    [user setUsername:[result objectForKey:@"screen_name"]];
                
//                NSString * names = [result objectForKey:@"name"];
//                if (names.length > 0) {
//                    NSMutableArray * array = [NSMutableArray arrayWithArray:[names componentsSeparatedByString:@" "]];
//                    if ( array.count > 1){
//                        [user setObject:[array lastObject]
//                                 forKey:@"last_name"];
//                        
//                        [array removeLastObject];
//                        [user setObject:[array componentsJoinedByString:@" " ]
//                                 forKey:@"first_name"];
//                    }
//                }
//
                if (user.isNew) {
                    [Utilities syncAllExercises:self];
                    [Utilities createUserProfileOnSignUp:self];
                    [Utilities downloadIAPRoutineFromStore:@IAP_ROUTINE_BODYWEIGHT_MAX view:self];
                    
                    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   @"UserID", [PFUser currentUser].objectId,
                                                   nil];
                    [Flurry logEvent:@"SignupTWitter" withParameters:articleParams];
                    // New user signing up using twitter will have multi set support.
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MultipleSetSupport"];
                    [[NSUserDefaults standardUserDefaults] synchronize];

                    // as this a signup, we dont want to download login data o de-registering post notification
                    [[NSNotificationCenter defaultCenter] removeObserver:self];

                } else  {
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"GyminutesDownloadLoginData"
                                                                        object:self
                                                                      userInfo:nil];
                }
                NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
                [defaults setObject:[result objectForKey:@"name"] forKey:@"userFullName"];
                [defaults synchronize];
                [user saveInBackground];
            }
        }];
        
    } else {
        //user loggedin via email....
        NSLog(@"user logged in via email.. lets download everthing mayank");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GyminutesDownloadLoginData"
                                                            object:self
                                                          userInfo:nil];
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       nil];
        [Flurry logEvent:@"Login" withParameters:articleParams];

    }
   
}

-(void) facebookNewUserData:(PFUser *) user name:(NSString *) username hasEmail:(BOOL) hasEmail email:(NSString *) emailForUser{
    // Store user full name as part of nsuserdefault data
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:username forKey:@"userFullName"];
    [defaults synchronize];
    NSLog(@"check if new user %d", user.isNew);
    if (user.isNew) {
        [Utilities syncAllExercises:self];
        [Utilities createUserProfileOnSignUp:self];
        // we are not downloading bodyweight max by default
        //[Utilities downloadIAPRoutineFromStore:@IAP_ROUTINE_BODYWEIGHT_MAX view:self];
        
        /*
        if (hasEmail) {
            NSLog(@"has email.");
        [PFCloud callFunctionInBackground:@"welcomeEmail" withParameters:@{@"templateName" : @"WelcomeSignup",
                                                                           @"toEmail" :emailForUser,
                                                                           @"toName": emailForUser}
                                    block:^(NSString *response, NSError *error) {
                                        if (!error) {
                                            NSLog(@"email successfully sent..");
                                        } else {
                                            NSLog(@"Error sending email: %@\n%@", error.description, error);
                                        }
                                    }];
            
        }
        */
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       nil];
        [Flurry logEvent:@"SignupFacebook" withParameters:articleParams];
        // New user signing up using facebook will have multi set support.
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"MultipleSetSupport"];
        [[NSUserDefaults standardUserDefaults] synchronize];

        // as this a signup, we dont want to download login data o de-registering post notification
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        [self dismissViewControllerAnimated:YES completion:^{
            NSLog(@"Mayank: Got UI.. Will perform segue now...");
            [self performSegueWithIdentifier:@"loadMainSegue" sender:self];
        }];
        
    } else {
        NSLog(@"WTF why in am in facebook mayank");
        [[NSNotificationCenter defaultCenter] postNotificationName:@"GyminutesDownloadLoginData"
                                                            object:self
                                                          userInfo:nil];
    }
}
-(void) downloadAllMyStuffOnLogin {
    [Utilities syncAllExercises:self];
    NSLog(@"Default exercise synced");
    
    [Utilities syncUserProfileOnLogin:self];
    NSLog(@"User profile Synced...");
    
    [Utilities syncAllUserRoutines:self];
    [Utilities syncAllUserWorksouts:self];
    [Utilities syncAllUserExercises:self];
    [Utilities syncAllDailyStats:self];
    
    NSMutableArray *allObjects = [[NSMutableArray alloc] init];
    [Utilities syncUserDataOnLogin:allObjects skip:0 limit:1000];

    NSMutableArray *allPR = [[NSMutableArray alloc] init];
    [Utilities syncExercisePersonalRecordEntry:allPR skip:0 limit:1000];
    
    NSLog(@"user routine/workout/exercise synced");
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        NSLog(@"dismissing now...");
        NSLog(@"deregistering notifications...");
        [[NSNotificationCenter defaultCenter] removeObserver:self];
        
        // this is a case when user had an app, deleted it and now reinstalled it and is logging in. Now this will get called but most probably will not change anything because when we download user Workouts, we anyway update exercise group and exercise number in group if empty
        
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"MultipleSetSupport"] == false) {
            NSLog(@"WIll be updating multiple set support");
            [ Utilities updateUserWorkoutToSetBasedSystem];
        }

        [self performSegueWithIdentifier:@"loadMainSegue" sender:self];

    }];
    
    
    
}
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    // Do nothing, as the view controller dismisses itself
    NSLog(@"login cancelled");
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"Date", [NSDate date],
                                   nil];
    [Flurry logEvent:@"LoginCancelled" withParameters:articleParams];

}

-(IBAction)logout:(id)sender {
      [Utilities logout];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - EAIntroView delegate

- (void)introDidFinish:(EAIntroView *)introView {
    NSLog(@"introDidFinish callback");
    self.navigationController.navigationBar.hidden = false;
    [self showLoginSignupView];

}

-(void) introDidFinish:(EAIntroView *)introView wasSkipped:(BOOL)wasSkipped {
    NSLog(@"introDidFinish and skipped");
    self.navigationController.navigationBar.hidden = false;
    [self showLoginSignupView];
    
}
@end
