//
//  ExerciseSet+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExerciseSet+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExerciseSet (CoreDataProperties)

+ (NSFetchRequest<ExerciseSet *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *date;
@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSNumber *exerciseNumber;
@property (nullable, nonatomic, copy) NSNumber *isDropSet;
@property (nullable, nonatomic, copy) NSNumber *isPyramidSet;
@property (nullable, nonatomic, copy) NSNumber *isSuperSet;
@property (nullable, nonatomic, copy) NSString *majorMuscle;
@property (nullable, nonatomic, copy) NSString *muscle;
@property (nullable, nonatomic, copy) NSNumber *rep;
@property (nullable, nonatomic, copy) NSString *routineName;
@property (nullable, nonatomic, copy) NSNumber *setNumber;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSString *timeStamp;
@property (nullable, nonatomic, copy) NSNumber *weight;
@property (nullable, nonatomic, copy) NSString *workoutName;

@end

NS_ASSUME_NONNULL_END
