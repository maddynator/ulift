//
//  IAPViewController.m
//  uLift
//
//  Created by Mayank Verma on 9/10/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "IAPViewController.h"
#import "commons.h"
#import "RoutineDetailsTVC.h"
#import "PackageDetailCVC.h"

@interface IAPViewController () {
//    NSArray *_products;
    NSMutableArray *_premiumProduct, *_indProduct, *_routineProduct;
    // Add new instance variable to class extension
    NSNumberFormatter * _priceFormatter;
    SKProduct *selectedProduct;
    NSArray *sectionHeaders;
    BOOL isEarlyBird;
}
@end

@implementation IAPViewController
@synthesize refreshControl;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    sectionHeaders = @[@"Premium Package", @"Individual Packages", @"Workout Routines"];
    
    _premiumProduct = [[NSMutableArray alloc] init];
    _indProduct  = [[NSMutableArray alloc] init];
    _routineProduct = [[NSMutableArray alloc] init];
    
    self.tableView.delegate = self;
    self.tableView.dataSource = self;

    selectedProduct = nil;
    
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(reload) forControlEvents:UIControlEventValueChanged];
    
    if ([Utilities connected]) {
        [self reload];
    }
    else {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"No Connectivity" subTitle:@"Unable to connect. Please check your network connectivity." closeButtonTitle:@"Ok" duration:0.0f];
    }
    
    [self.refreshControl beginRefreshing];
    
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Restore" style:UIBarButtonItemStylePlain target:self action:@selector(restoreTapped:)];
    
    
    _priceFormatter = [[NSNumberFormatter alloc] init];
    [_priceFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [_priceFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    
    self.tableView.emptyDataSetDelegate = self;
    self.tableView.emptyDataSetSource = self;
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"StoreOpened" withParameters:articleParams];
    isEarlyBird = [Utilities isEarlyBird];

}
-(void) viewDidAppear:(BOOL)animated {
    self.title = @"Gyminutes Store";
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// Add new method
- (void)restoreTapped:(id)sender {
    [KVNProgress showWithStatus:@"Restoring.."];
    NSLog(@"trying to restore...");

    [[uLiftIAPHelper sharedInstance] restoreCompletedTransactions];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"RestoreWorkouts" withParameters:articleParams];

   // [KVNProgress dismiss];
//    [self.refreshControl beginRefreshing];
//    [self reload];
}


// 4
- (void)reload {
//    _products = nil;
    [_premiumProduct removeAllObjects];
    [_indProduct removeAllObjects];
    [_routineProduct removeAllObjects];
    
    [KVNProgress showWithStatus:@"Loading..."];
    [self.tableView reloadData];
    [[uLiftIAPHelper sharedInstance] requestProductsWithCompletionHandler:^(BOOL success, NSArray *products) {
        if (success) {
//            _products = products;
//            NSLog(@"Products %@", products);
            for (int i = 0; i < [products count]; i++) {
                SKProduct *prod = [products objectAtIndex:i];
                NSLog(@"product id is %@", prod.productIdentifier);
                if ([prod.productIdentifier isEqualToString:@IAP_PREMIUM_PACKAGE_ID]) {
                    NSLog(@"adding premium now...");
                    [_premiumProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_WORKOUT_PACKAGE_ID]) {
                    NSLog(@"adding ind 1 now...");
                    [_indProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_ROUTINE_PACKAGE_ID]) {
                      NSLog(@"adding ind 2 now...");
                    [_indProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_DATA_PACKAGE_ID]) {
                    NSLog(@"adding ind 3 now...");
                    [_indProduct addObject:prod];
                } else if ([prod.productIdentifier isEqualToString:@IAP_ANALYTICS_PACKAGE_ID]) {
                    NSLog(@"adding ind 3 now...");
                    [_indProduct addObject:prod];
                } else {
                    NSLog(@"adding routine now...");                    
                    [_routineProduct addObject:prod];
                }
            }
            NSLog(@"count is %lu %lu %lu", (unsigned long)[_premiumProduct count], (unsigned long)[_indProduct count], (unsigned long)[_routineProduct count]);
            [self.tableView reloadData];
            if ([KVNProgress isVisible]) {
                [KVNProgress dismiss];
            }
        }
        [self.refreshControl endRefreshing];
    }];

}

#pragma mark - Table View

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [sectionHeaders objectAtIndex:section];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([Utilities connected]) {
        return [sectionHeaders count];
    } else {
        return 0;
    }
}

// 5
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section) {
        case 0:{
            NSLog(@"section %ld %lu", (long)section, (unsigned long)
                  [_premiumProduct count]);
            return [_premiumProduct count];
        }
        case 1:{
            NSLog(@"section %ld %lu",(long) section, (unsigned long)[_indProduct count]);
            return [_indProduct count];
        }
        case 2:{
            NSLog(@"section %ld %lu",(long) section, (unsigned long)[_routineProduct count]);
            return [_routineProduct count];
        }
        default:
            break;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
//    UILabel *price = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 72, 37)];
    
    cell.accessoryType = UITableViewCellAccessoryNone;
    
    SKProduct *product = nil;
    
    switch (indexPath.section) {
        case 0: {
                product = [_premiumProduct objectAtIndex:indexPath.row];
                }
            break;
        case 1: {
                product = [_indProduct objectAtIndex:indexPath.row];
                }
            break;
        case 2: {
                product = [_routineProduct objectAtIndex:indexPath.row];
                }
            break;
            
        default:
            break;
    }
    [_priceFormatter setLocale:product.priceLocale];
    cell.textLabel.text = product.localizedTitle;
    cell.detailTextLabel.text = product.localizedDescription;
    cell.detailTextLabel.numberOfLines = 3;
    cell.textLabel.textColor = [Utilities getAppColor];
    cell.detailTextLabel.textColor = [Utilities getAppColor];//light
    [cell.textLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
    [cell.detailTextLabel setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    
    if ([[uLiftIAPHelper sharedInstance] productPurchased:product.productIdentifier]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        cell.tintColor = [Utilities getAppColor];
        cell.accessoryView = nil;
    } else {
        UIButton *buyButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
        buyButton.frame = CGRectMake(0, 0, 72, 37);
        buyButton.tag = indexPath.row;
        buyButton.layer.borderWidth = 1;
        buyButton.layer.cornerRadius = 10;
        buyButton.layer.borderColor = [Utilities getAppColor].CGColor;
        buyButton.backgroundColor = [Utilities getAppColor];//light
        [buyButton addTarget:self action:@selector(buyButtonTapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.accessoryView = buyButton;
        
        [buyButton setTitle:[_priceFormatter stringFromNumber:product.price] forState:UIControlStateNormal];
        cell.accessoryType = UITableViewCellAccessoryNone;
        cell.accessoryView = buyButton;
    }
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    switch (indexPath.section) {
        case 0: {
            selectedProduct = [_premiumProduct objectAtIndex:indexPath.row];
            [self performSegueWithIdentifier:@"packageDetailSegue" sender:self];

        }
            break;
        case 1: {
            selectedProduct = [_indProduct objectAtIndex:indexPath.row];
            [self performSegueWithIdentifier:@"packageDetailSegue" sender:self];

        }
            break;
        case 2: {
            selectedProduct = [_routineProduct objectAtIndex:indexPath.row];
            [self performSegueWithIdentifier:@"routineDetailSegue" sender:self];

        }
            break;
                
        default:
            break;
    }

}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40.0f;
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (void)buyButtonTapped:(id)sender {
    

    UITableViewCell* cell = (UITableViewCell*)[sender superview];
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];

    // unless user is buying a routine, we dont need to check for early bird
    if (indexPath.section != 2 && isEarlyBird == true) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showNotice:self.parentViewController title:@"PREMIUM MEMEBER" subTitle:@"You are already a premium member and have access to all features. We can't take your money again. You don't need to upgrade." closeButtonTitle:@"OK" duration:0.0f];
        return;
    }
    [KVNProgress showWithStatus:@"Buying routine...."];
    SKProduct *product = nil;
    switch (indexPath.section) {
        case 0:
            product = [_premiumProduct objectAtIndex:indexPath.row];
            break;
        case 1:
            product = [_indProduct objectAtIndex:indexPath.row];
            break;
        case 2:
            product = [_routineProduct objectAtIndex:indexPath.row];
            break;
        default:
            break;
    }
    
    NSLog(@"Buying %@...", product.productIdentifier);
    [[uLiftIAPHelper sharedInstance] buyProduct:product];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)productPurchased:(NSNotification *)notification {
    
    NSString * productIdentifier = notification.object;
    [_premiumProduct enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
            *stop = YES;
        }
    }];
    [_indProduct enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:1]] withRowAnimation:UITableViewRowAnimationFade];
            *stop = YES;
        }
    }];
    [_routineProduct enumerateObjectsUsingBlock:^(SKProduct * product, NSUInteger idx, BOOL *stop) {
        if ([product.productIdentifier isEqualToString:productIdentifier]) {
            [self.tableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:idx inSection:2]] withRowAnimation:UITableViewRowAnimationFade];
            *stop = YES;
        }
    }];
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"routineDetailSegue"]) {
        RoutineDetailsTVC *destVC = segue.destinationViewController;
        destVC.product = selectedProduct;
        destVC.routineBundleId = selectedProduct.productIdentifier;
        destVC.isLocalStored = false;
    } else if ([segue.identifier isEqualToString:@"packageDetailSegue"]) {
        PackageDetailCVC *destVC = segue.destinationViewController;
        destVC.product = selectedProduct;
        destVC.routineBundleId = selectedProduct.productIdentifier;
        NSLog(@"in here... %@, %@", selectedProduct, selectedProduct.productIdentifier);
    }
    self.title = @"";
}

#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"Unable to connect to iTunes to get routines."];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"We are unable to get routines right now. Please check again in few minutes. Sorry for inconvenience.";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

@end
