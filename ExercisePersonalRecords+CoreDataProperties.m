//
//  ExercisePersonalRecords+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExercisePersonalRecords+CoreDataProperties.h"

@implementation ExercisePersonalRecords (CoreDataProperties)

+ (NSFetchRequest<ExercisePersonalRecords *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"ExercisePersonalRecords"];
}

@dynamic exerciseName;
@dynamic prDate;
@dynamic prExerciseNumber;
@dynamic prExerciseNumInGroup;
@dynamic prGroupNumber;
@dynamic prRep;
@dynamic prTime;
@dynamic prUserWeight;
@dynamic prWeight;
@dynamic roundNumber;
@dynamic routineName;
@dynamic syncedState;
@dynamic workoutName;

@end
