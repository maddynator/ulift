//
//  RoutineMetaInfo+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "RoutineMetaInfo+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface RoutineMetaInfo (CoreDataProperties)

+ (NSFetchRequest<RoutineMetaInfo *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *routineAuthor;
@property (nullable, nonatomic, copy) NSString *routineBundleId;
@property (nullable, nonatomic, copy) NSString *routineColor;
@property (nullable, nonatomic, copy) NSNumber *routineComplete;
@property (nullable, nonatomic, copy) NSString *routineCreatedDate;
@property (nullable, nonatomic, copy) NSString *routineDescription;
@property (nullable, nonatomic, copy) NSString *routineDisclosure;
@property (nullable, nonatomic, copy) NSNumber *routineDuration;
@property (nullable, nonatomic, copy) NSString *routineFaqs;
@property (nullable, nonatomic, copy) NSString *routineImage;
@property (nullable, nonatomic, copy) NSString *routineInstructions;
@property (nullable, nonatomic, copy) NSString *routineLevel;
@property (nullable, nonatomic, copy) NSString *routineName;
@property (nullable, nonatomic, copy) NSNumber *routineRoundNumber;
@property (nullable, nonatomic, copy) NSString *routineSummary;
@property (nullable, nonatomic, copy) NSString *routineType;
@property (nullable, nonatomic, copy) NSNumber *routineUserCreated;
@property (nullable, nonatomic, copy) NSString *routingGoal;
@property (nullable, nonatomic, copy) NSNumber *syncedState;

@end

NS_ASSUME_NONNULL_END
