//
//  RecordExercise.h
//  uLift
//
//  Created by Mayank Verma on 7/3/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "JVFloatLabeledTextView.h"

@interface RecordExercise : UIViewController <UITableViewDataSource, UITableViewDelegate, MZTimerLabelDelegate, UITextViewDelegate, UIAlertViewDelegate, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, MGSwipeTableCellDelegate, CNPPopupControllerDelegate, UICollectionViewDataSource, UICollectionViewDelegate>

@property (nonatomic,retain) IBOutlet UITextView *lastWorkout;
@property (nonatomic,retain) IBOutlet UITextView *todayWorkout;
@property (nonatomic, retain) IBOutlet UITableView *setsTableView;
@property (nonatomic, retain) IBOutlet JVFloatLabeledTextView *weightText;
@property (nonatomic, retain) IBOutlet JVFloatLabeledTextView *repText;
@property (nonatomic, retain) IBOutlet UILabel *history;;

@property (nonatomic,retain) IBOutlet UIButton *weightPlus;
@property (nonatomic,retain) IBOutlet UIButton *weightMinus;
@property (nonatomic,retain) IBOutlet UIButton *repPlus;
@property (nonatomic,retain) IBOutlet UIButton *repMinus;
@property (nonatomic,retain) IBOutlet UIButton *saveBtn;
@property (nonatomic,retain) IBOutlet UIButton *cllearBtn;
@property (nonatomic,retain) IBOutlet UILabel *dateForExercise;
@property (nonatomic,retain) IBOutlet MZTimerLabel *timer;
@property (nonatomic,retain) IBOutlet UIButton *prevExBtn;
@property (nonatomic,retain) IBOutlet UIButton *nextExBtn;
@property (nonatomic,retain) IBOutlet UILabel *setsLbl;
@property (nonatomic, retain) IBOutlet HMSegmentedControl *segmentControl;
@property (nonatomic, retain) IBOutlet WorkoutList *exerciseObj;
@property (nonatomic, retain) IBOutlet NSString *date;
@property (nonatomic, retain) IBOutlet NSString *workoutName;
@property (nonatomic, retain) IBOutlet NSString *routineName;
@property (nonatomic, retain) IBOutlet NSNumber *exerciseNumber;
@property (nonatomic, retain) IBOutlet UIBarButtonItem *seat, *head, *clockTimer;
@property (nonatomic, strong) CNPPopupController *popupController, *barbelPopupController;

-(IBAction)saveWorkout:(id)sender;
-(IBAction)weightInc:(id)sender;
-(IBAction)weightDec:(id)sender;
-(IBAction)repInc:(id)sender;
-(IBAction)repDec:(id)sender;
-(IBAction)clearWorkout:(id)sender;
-(IBAction)seatSetting:(id)sender;
-(IBAction)handSetting:(id)sender;
-(IBAction)timerValue:(id)sender;
-(IBAction) viewHistory:(id)sender;
-(IBAction) loadNextExercise:(id)sender;
-(IBAction) loadPreviousExercise:(id)sender;
-(IBAction) quickAddExercise:(id)sender;

@end
