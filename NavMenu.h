//
//  NavMenu.h
//  uLift
//
//  Created by Mayank Verma on 7/17/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavMenu : UINavigationController
-(void)showMenu;

@end
