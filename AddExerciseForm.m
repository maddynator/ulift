//
//  UserProfileForm.m
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AddExerciseForm.h"

@implementation AddExerciseForm

- (NSArray *)fields
{
    return @[
             /*
             @property (nonatomic, copy) NSString *name;
             @property (nonatomic, assign) Equipment equipment;
             @property (nonatomic, assign) Mechanics mechanics;
             @property (nonatomic, assign) ExperienceLevel experience;
             @property (nonatomic, assign) Muscle muscle;
             @property (nonatomic, assign) Type type;
             */
             
             @{FXFormFieldKey: @"name",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords)},
             
             @{FXFormFieldKey: @"equipment", FXFormFieldOptions: @[@"Barbell",
                                                                   @"Bodyweight",
                                                                   @"Cable",
                                                                   @"Dumbbell",
                                                                   @"Exercise Ball",
                                                                   @"Machine",
                                                                   @"Kettlebell",
                                                                   @"Other"]},
             
             @{FXFormFieldKey: @"mechanics", FXFormFieldOptions: @[@"Isolated",
                                                               @"Compound"]},
             
             @{FXFormFieldKey: @"experience", FXFormFieldOptions: @[@"Beginner",
                                                                     @"Intermediate",
                                                                     @"Experienced"]},
             @{FXFormFieldKey: @"muscle", FXFormFieldOptions: @[@"Abs",
                                                                @"Back",
                                                                @"Biceps",
                                                                @"Calves",
                                                                @"Chest",
                                                                @"Forearm",
                                                                @"Glutes",
                                                                @"Hamstring",
                                                                @"Lats",
                                                                @"Quads",
                                                                @"Shoulders",
                                                                @"Traps",
                                                                @"Triceps"]},
             
             @{FXFormFieldKey: @"muscleGroup", FXFormFieldOptions: @[@"Abs",
                                                                @"Arms",
                                                                @"Back",
                                                                @"Chest",
                                                                @"Legs",
                                                                @"Shoulders"
                                                                     ]},

             @{FXFormFieldKey: @"type", FXFormFieldOptions: @[@"Strength",
                                                                @"Plyometrics",
                                                                @"Powerlifting",
                                                                @"Weightlifting"]},

             @{FXFormFieldTitle: @"Create Exercise", FXFormFieldHeader: @"", FXFormFieldAction: @"submitExerciseForm:"},
             ];
}

@end
