//
//  RecordExerciseMenu.h
//  uLift
//
//  Created by Mayank Verma on 7/6/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface RecordExerciseMenu : UIViewController
@property (nonatomic, retain) IBOutlet PFObject *exerciseObj;
@property (nonatomic, retain) IBOutlet NSString *date;

@end
