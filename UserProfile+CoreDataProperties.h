//
//  UserProfile+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "UserProfile+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface UserProfile (CoreDataProperties)

+ (NSFetchRequest<UserProfile *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSNumber *age;
@property (nullable, nonatomic, copy) NSString *deviceType;
@property (nullable, nonatomic, copy) NSString *email;
@property (nullable, nonatomic, copy) NSNumber *experienceLevel;
@property (nullable, nonatomic, copy) NSNumber *height;
@property (nullable, nonatomic, copy) NSString *membershipType;
@property (nullable, nonatomic, copy) NSNumber *sex;
@property (nullable, nonatomic, copy) NSNumber *units;
@property (nullable, nonatomic, copy) NSString *userName;
@property (nullable, nonatomic, copy) NSNumber *weight;

@end

NS_ASSUME_NONNULL_END
