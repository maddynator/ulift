//
//  HomeTVC.h
//  uLift
//
//  Created by Mayank Verma on 11/7/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
#import "ReflectionView.h"
#import <MessageUI/MessageUI.h>

@interface HomeTVC : UITableViewController <iCarouselDataSource, iCarouselDelegate, FBSDKAppInviteDialogDelegate, MFMailComposeViewControllerDelegate,  MFMessageComposeViewControllerDelegate>

@property (nonatomic, strong) IBOutlet iCarousel *carousel;
@end
