//
//  TodayWorkout.h
//  uLift
//
//  Created by Mayank Verma on 7/2/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface TodayWorkout : UIViewController <UICollectionViewDataSource, UICollectionViewDelegate,DZNEmptyDataSetSource, DZNEmptyDataSetDelegate, CNPPopupControllerDelegate, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, RSDFDatePickerViewDelegate, RSDFDatePickerViewDataSource, MPCoachMarksViewDelegate>

@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
@property (nonatomic, retain) IBOutlet UITableView *myTableView;
@property (nonatomic, retain) IBOutlet UILabel *recordDate;
@property (retain, nonatomic) IBOutlet DIDatepicker *datepicker;
@property (retain, nonatomic) MPCoachMarks *coachMarksView;

-(IBAction) yesterdayDate: (id) sender;
-(IBAction) tomorrowDate: (id) sender;
//-(IBAction) addExercise: (id) sender;
-(IBAction)saveToParse:(id)sender;
@end
