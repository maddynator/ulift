//
//  ExerciseSummary.m
//  uLift
//
//  Created by Mayank Verma on 10/7/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "ExerciseSummary.h"
#import "commons.h"
#import <Social/Social.h>

@interface ExerciseSummary () <ChartViewDelegate> {
    NSArray *sectionHeader, *allExerciseSet;
    NSMutableArray *exerciseList, *workoutTime, *exerciseTBArray, *workoutTimeTBArray;
    float weightMvd;
    float prevWeightMoved;
    NSMutableArray *dateArray;
    int sectionTags;
    NSString *totalWorkoutTime;
}
@property (nonatomic, strong) IBOutlet BarChartView *chartView;
@property (nonatomic, retain) UIDocumentInteractionController *documentController;

@end

@implementation ExerciseSummary

- (void)viewDidLoad {
    [super viewDidLoad];
    
    sectionTags = 0;
    self.title = @"Summary";
    sectionHeader = [[NSArray alloc] initWithObjects:@"Wt. Mvd Comparison", @"Time Breakdown", @"Wt.Mvd/Ex(Last Wrkt)",  nil]; // @"Fun Fact",
    workoutTime = [[NSMutableArray alloc] init];
    dateArray = [[NSMutableArray  alloc] init];
    
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.navigationController.navigationBarHidden = NO;

    allExerciseSet = [Utilities getSetsForAllExerciseForDateSortedByTime:_date];
    
    int setCount =([allExerciseSet count] != 0) ? (int)[allExerciseSet count] : 0;
    weightMvd = prevWeightMoved = 0;
    
    
    NSMutableArray *timeSpentDate = [[NSMutableArray alloc] init];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
 
    
    for (ExerciseSet *set in allExerciseSet) {
        NSLog(@"exercise name is %@ at timestamp %@", set.exerciseName, [dateFormatter dateFromString:set.timeStamp]);
        weightMvd += [set.weight floatValue] * [set.rep intValue];
        [timeSpentDate addObject:[dateFormatter dateFromString:set.timeStamp]];
    }


    // this is imporant - we set our input date format to match our input string
    // if format doesn't match you'll get nil from your string, so be careful
    NSDate *date2, *date1;
    if (setCount == 0) {
        date2 = date1 = [NSDate date];
    } else {
        date2 = [timeSpentDate objectAtIndex:0];
        date1 = [timeSpentDate objectAtIndex:[timeSpentDate count]- 1];
    }
    
    NSTimeInterval secs = [date1 timeIntervalSinceDate:date2];
    totalWorkoutTime = [NSString stringWithFormat:@"%ld:%ld:%ld",(long) secs/3600, (long)(secs/60)%60, (long) secs %60];
    
    float liftTime = setCount * 45;
    float breakTime = secs - liftTime;
    
    
    if (secs < liftTime) {
        // this means that user just recorded all sets and did not recorded them per exercise during workout..
        liftTime = 0;
        breakTime = 0;
    }
    
    NSString *totalLiftTime = [NSString stringWithFormat:@"%ld:%ld:%ld", (long) liftTime/3600, (long)(liftTime/60)%60, (long) liftTime%60];
    NSString *totalRestTime = [NSString stringWithFormat:@"%ld:%ld:%ld", (long) breakTime/3600, (long)(breakTime/60)%60, (long) breakTime%60];
    
    NSLog(@"Workout Duration: %@ %@ %@", totalWorkoutTime, totalLiftTime, totalRestTime);

    // this is to get all the muscle worked...
    NSMutableArray * unique  = [NSMutableArray array];
    NSMutableSet * processed = [NSMutableSet set];
    
    for (ExerciseSet *data in allExerciseSet) {
        NSString *string = data.majorMuscle;
        if ([processed containsObject:string] == NO) {
            [unique addObject:string];
            [processed addObject:string];
        }
    }
    
    
    exerciseList = [[NSMutableArray alloc] initWithArray:[Utilities getAllExerciseForDate:_date]];
    workoutTime = [[NSMutableArray alloc] initWithObjects:totalWorkoutTime, totalLiftTime, totalRestTime, nil];
    
    exerciseTBArray = [[NSMutableArray alloc] initWithObjects:@"", @"", exerciseList, @"", nil];
    workoutTimeTBArray = [[NSMutableArray alloc] initWithObjects: @"", workoutTime, @"", @"", nil];
    
    _chartView = [[BarChartView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 300)];
    _chartView.delegate = self;
    
    _chartView.descriptionText = @"";
    _chartView.noDataText = @"No data.";
    
    _chartView.drawBarShadowEnabled = NO;
    _chartView.drawValueAboveBarEnabled = YES;
    
    _chartView.maxVisibleCount = 60;
    _chartView.pinchZoomEnabled = NO;
    _chartView.drawGridBackgroundEnabled = NO;
 

    ChartXAxis *xAxis = _chartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    
    ChartYAxis *leftAxis = _chartView.leftAxis;
    leftAxis.labelFont = [UIFont systemFontOfSize:10.f];
    leftAxis.labelCount = 8;
//    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
//    leftAxis.valueFormatter.maximumFractionDigits = 1;
//    leftAxis.valueFormatter.negativeSuffix = @"";
//    leftAxis.valueFormatter.positiveSuffix = @"";
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.drawGridLinesEnabled = NO;
    
    ChartYAxis *rightAxis = _chartView.rightAxis;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.labelFont = [UIFont systemFontOfSize:10.f];
    rightAxis.labelCount = 8;
    rightAxis.valueFormatter = leftAxis.valueFormatter;
    rightAxis.spaceTop = 0.15;
    rightAxis.enabled = NO;
    
    _chartView.legend.position = ChartLegendPositionBelowChartLeft;
    _chartView.legend.form = ChartLegendFormSquare;
    _chartView.legend.formSize = 9.0;
    _chartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _chartView.legend.xEntrySpace = 4.0;
    _chartView.legend.enabled = YES;

    [self setDataCount];

}

-(void) viewDidAppear:(BOOL)animated {
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setDataCount
{

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    NSMutableArray *dateArr = [[NSMutableArray alloc] init];
    NSMutableArray *weightArr = [[NSMutableArray alloc] init];
    
    [Utilities getWeightMovedForGivenRoutineAndWorkout:_routineName workout:_workoutName dateArr:&dateArr weightArray:&weightArr];
    
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    int count = 0;
    for(int i = 0; i < [dateArr count]; i++) {
        NSDate *temp = [dateFormatter dateFromString:[dateArr objectAtIndex:i]];
        if ([temp isLaterThan:[dateFormatter dateFromString:_date]]) {
            continue;
        }
        int value = [[weightArr objectAtIndex:i] intValue];
        [xVals addObject:[dateArr objectAtIndex:i]];
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:count y:value]];
        count++;
    }
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@"Volume"];
    
    BarChartData *data = [[BarChartData alloc] initWithDataSet:set1];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    
    _chartView.data = data;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [sectionHeader count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return 1;
        case 1:
            return [workoutTime count];
        case 2:
            return [exerciseList count];
        case 3:
            return 1;
        default:
            break;
    }
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIView class]]) {
            if (lbl.tag == 10)
                [lbl removeFromSuperview];
        }
    }
    
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    for (UIImageView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UIImageView class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    for (BarChartView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[BarChartView class]]) {
            [lbl removeFromSuperview];
        }
    }
    switch (indexPath.section) {
        case 0:     // show workout graphs..
        {
            
//            // Configure the cell...
//            for (UIView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIView class]]) {
//                    if (lbl.tag == 10)
//                        [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UILabel *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UILabel class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UIImageView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIImageView class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }
            
            [cell.contentView addSubview:_chartView];
        }
            break;
        case 1:
            // show duration summary
        {
           
            
            // Configure the cell...
            

            float cellWidth = CGRectGetWidth(cell.frame);
            
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 30, 30)];
            [image setImage:[UIImage imageNamed:@"IconTime"]];
            [cell.contentView addSubview:image];
            
            UILabel *timeName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(image.frame) + 10, 5, (cellWidth - 45)/2, 40)];
            switch (indexPath.row) {
                case 0:
                    timeName.text = @"Total Workout Duration";
                    break;
                case 1:
                    timeName.text = @"Lifting Duration (Estimated)";
                    break;
                case 2:
                    timeName.text = @"Rest Duration (Estimated)";
                    break;
                default:
                    break;
            }

            timeName.numberOfLines = 2;
            timeName.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
            
            UILabel *timeVal = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(timeName.frame), 5, cellWidth - CGRectGetWidth(image.frame) - 15 - CGRectGetWidth(timeName.frame) - 20 , 40)];
            timeVal.text = [[workoutTimeTBArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            timeVal.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            timeVal.textAlignment = NSTextAlignmentRight;
            
            [cell.contentView addSubview:image];
            [cell.contentView addSubview:timeName];
            [cell.contentView addSubview:timeVal];

        }

            break;
        case 2:
            // show each exercise summary
        {
//            
//            // Configure the cell...
//            for (UIView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIView class]]) {
//                    if (lbl.tag == 10)
//                        [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UILabel *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UILabel class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UIImageView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIImageView class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }

            float cellWidth = CGRectGetWidth(cell.frame);
            
            WorkoutList *exData = [[exerciseTBArray objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            NSArray *previousWorkout = [Utilities getSetForExerciseOnPreviosDate:exData.exerciseName date:_date rName:exData.routiineName wName:exData.workoutName];
            float lPrevWeightMoved = 0;
            
            for (ExerciseSet *setData in previousWorkout) {
                lPrevWeightMoved += [setData.rep intValue] * [setData.weight floatValue];
            }
            
            NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exData.exerciseName];
            NSArray *exerciseData = [allExerciseSet filteredArrayUsingPredicate:exPredicate];
            UIImageView *image = [[UIImageView alloc] initWithFrame:CGRectMake(15, 10, 30, 30)];
            
            [image setImage:[Utilities getMuscleImage:exData.majorMuscle]];
            [cell.contentView addSubview:image];

            
            float weightMoved = 0;
            
            for (ExerciseSet *setData in exerciseData) {
                weightMoved += [setData.rep intValue] * [setData.weight floatValue];
            }
            
            UILabel *exerciseName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(image.frame) + 10, 5, (cellWidth - 45)/2, 40)];
            exerciseName.text = exData.exerciseName;
            exerciseName.numberOfLines = 2;
            exerciseName.font = [UIFont fontWithName:@HAL_REG_FONT size:12];
            
            UILabel *exerciseLbs = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(exerciseName.frame), 5, cellWidth - CGRectGetWidth(image.frame) - 15 - CGRectGetWidth(exerciseName.frame) - 20 , 40)];
            exerciseLbs.text = [NSString stringWithFormat:@"%.f (%.f)", weightMoved, weightMoved - lPrevWeightMoved];
            
            exerciseLbs.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            exerciseLbs.textAlignment = NSTextAlignmentRight;
            if (lPrevWeightMoved > weightMoved) {
                exerciseLbs.textColor = FlatRedDark;
            } else {
                exerciseLbs.textColor = FlatGreenDark;
            }
//            NSLog(@"Previos %.f vs today %.f", prevWeightMoved, weightMoved);
            [cell.contentView addSubview:image];
            [cell.contentView addSubview:exerciseName];
            [cell.contentView addSubview:exerciseLbs];
        }
            break;
        case 3: {
            
//            
//            // Configure the cell...
//            for (UIView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIView class]]) {
//                    if (lbl.tag == 10)
//                        [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UILabel *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UILabel class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }
//            
//            for (UIImageView *lbl in [cell.contentView subviews]) {
//                if ([lbl isKindOfClass:[UIImageView class]]) {
//                    [lbl removeFromSuperview];
//                }
//            }
            
            // show image with puppy and graph
            int tbWdt = CGRectGetWidth(self.tableView.frame);
            UIView *pView = [[UIView alloc] initWithFrame:CGRectMake(10, 5, tbWdt - 20, 200)];
            pView.layer.cornerRadius = 5;
            pView.backgroundColor = FlatMint;
            pView.tag = 10;
            
            
            UIImageView *topImgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 20, (CGRectGetWidth(pView.frame)/5 * 2), 100)];
            [topImgView setContentMode:UIViewContentModeScaleAspectFit];
            topImgView.backgroundColor = [UIColor clearColor];
            [topImgView setImage:[UIImage imageNamed:@"IconAnimalShark"]];
            
            UILabel *multiPlier = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(topImgView.frame), 5, CGRectGetWidth(pView.frame)/5, 120)];
            multiPlier.text = [NSString stringWithFormat:@"x 10  = "];
            multiPlier.textAlignment = NSTextAlignmentCenter;
            multiPlier.textColor = [UIColor whiteColor];
            multiPlier.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            
            
            UILabel *wt = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(multiPlier.frame), 40, (CGRectGetWidth(pView.frame)/5 * 2) - 2, 40)];
            wt.text = [NSString stringWithFormat:@"%.f", weightMvd];
            wt.textAlignment = NSTextAlignmentCenter;
            wt.textColor = [UIColor whiteColor];
            //            wt.backgroundColor = FlatCoffeeDark;
            wt.font = [UIFont fontWithName:@HAL_BOLD_FONT size:30];
            
            
            UILabel *wtLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(multiPlier.frame), CGRectGetMaxY(wt.frame), (CGRectGetWidth(pView.frame)/5 * 2) - 10, 40)];
            wtLbl.text = @"Weight Moved (Lbs)";
            wtLbl.textAlignment = NSTextAlignmentCenter;
            wtLbl.textColor = [UIColor whiteColor];
            wtLbl.numberOfLines = 2;
            wtLbl.font = [UIFont fontWithName:@HAL_REG_FONT size:16];
            
            UILabel *textLbl = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(topImgView.frame) + 10, CGRectGetWidth(pView.frame) - 10, 60)];
            textLbl.text = @"Mayank just lifted an equivalent of 10 Sharks combined.";
            textLbl.textAlignment = NSTextAlignmentCenter;
            textLbl.textColor = [UIColor whiteColor];
            textLbl.backgroundColor = FlatMintDark;
            textLbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
            textLbl.numberOfLines = 2;
            
            [pView addSubview:topImgView];
            [pView addSubview:textLbl];
            [pView addSubview:multiPlier];
            [pView addSubview:wt];
            [pView addSubview:wtLbl];
            [cell.contentView addSubview:pView];
        }
            break;
        default:
            break;
    }
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    
//    static NSString *HeaderIdentifier = @"Header";
//
//    UITableViewHeaderFooterView *header = [tableView dequeueReusableHeaderFooterViewWithIdentifier:HeaderIdentifier];
//    if(!header) {
//        header = [[UITableViewHeaderFooterView alloc] initWithReuseIdentifier:HeaderIdentifier];
//
//    }
    
    UIView *header = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 50)];
    for (UILabel *lbl in [header subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        }
    }

    UILabel *headerLbl = headerLbl = [[UILabel alloc] initWithFrame:CGRectMake(15, 15, CGRectGetWidth(header.frame)/2, 30)];;
    headerLbl.text = [sectionHeader objectAtIndex:section];
    headerLbl.textColor = FlatSkyBlue;
    headerLbl.textAlignment = NSTextAlignmentLeft;
    headerLbl.font = [UIFont fontWithName:@HAL_BOLD_FONT size:16];
    
    if (section == 0) {
        NSArray *shareImg = [[NSArray alloc] initWithObjects: @"IconShareTwitter", @"IconShareInstagram", @"IconShareFacebook", nil];
        
        UIButton *twbtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(header.frame) - 110 + 0 * 40, 15, 25, 25)];
        [twbtn setImage:[UIImage imageNamed:[shareImg objectAtIndex:0]] forState:UIControlStateNormal];
        [[twbtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
        twbtn.tag = section;
        twbtn.layer.cornerRadius = 12;
        [header addSubview:twbtn];
        [twbtn addTarget:self action:@selector(twShareButtonHighlight:) forControlEvents:UIControlEventTouchDown];
        [twbtn addTarget:self action:@selector(twShareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *inbtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(header.frame) - 110 + 1 * 40, 15, 25, 25)];
        [inbtn setImage:[UIImage imageNamed:[shareImg objectAtIndex:1]] forState:UIControlStateNormal];
        [[inbtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
        inbtn.tag = section;
        inbtn.layer.cornerRadius = 12;
        [header addSubview:inbtn];
        [inbtn addTarget:self action:@selector(inShareButtonHighlight:) forControlEvents:UIControlEventTouchDown];
        [inbtn addTarget:self action:@selector(inShareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
        
        UIButton *fbbtn = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(header.frame) - 110 + 2 * 40, 15, 25, 25)];
        [fbbtn setImage:[UIImage imageNamed:[shareImg objectAtIndex:2]] forState:UIControlStateNormal];
        [[fbbtn imageView] setContentMode: UIViewContentModeScaleAspectFit];
        fbbtn.tag = section;
        fbbtn.layer.cornerRadius = 12;
        [header addSubview:fbbtn];
        [fbbtn addTarget:self action:@selector(fbShareButtonHighlight:) forControlEvents:UIControlEventTouchDown];
        [fbbtn addTarget:self action:@selector(fbShareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    [header addSubview:headerLbl];
    return header;
}


- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40.0f;
}
-(void) twShareButtonHighlight:(UIButton *) sender {
    sender.backgroundColor = FlatBlueDark;
}
-(void) inShareButtonHighlight:(UIButton *) sender {
    sender.backgroundColor = FlatBlueDark;
}
-(void) fbShareButtonHighlight:(UIButton *) sender {
    sender.backgroundColor = FlatBlueDark;
}

-(void)twShareBtnPressed:(UIButton *)sender {
    // so we get which section it was clicked.. and then we adjust the offset according to the table.
    CGRect temp =[self.tableView rectForSection:sender.tag];
    CGRect sectionRect = CGRectOffset(temp, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y);
    NSLog(@"tw share btn pressed with tag %ld %@ => %@", (long)sender.tag,  NSStringFromCGRect(sectionRect), NSStringFromCGRect(temp));
    sender.backgroundColor = [UIColor clearColor];
    
    CGRect sectionFrame = CGRectMake(0, temp.origin.y + 50, CGRectGetWidth(self.tableView.frame), [self sectionDataHeight:(int)sender.tag]);
    UIImageView *scGrab = [self rp_screenshotImageViewWithCroppingRect:sectionFrame];
    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
    [controller addURL:[NSURL URLWithString:@"http://www.gyminutesapp.com"]];
    [controller addImage:scGrab.image];
    [controller setInitialText:@"Here is my workout progress."];
    
//    switch (sender.tag) {
//        case 0:
//            [controller setInitialText:@"Here is my workout progress."];
//            break;
//        case 1:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//        case 2:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//        case 3:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//            
//        default:
//            break;
//    }
    [self presentViewController:controller animated:YES completion:^{
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RtInfo", [NSString stringWithFormat:@"%@,%@", _routineName, _workoutName],
                                       @"WtMvd", [NSString stringWithFormat:@"%.1f", weightMvd],
                                       @"TotalWrkTime", totalWorkoutTime,
                                       nil];
        [Flurry logEvent:@"RtDetSumTw" withParameters:articleParams];

    }];

    

}

-(void)inShareBtnPressed:(UIButton *)sender {
    // so we get which section it was clicked.. and then we adjust the offset according to the table.
    CGRect temp =[self.tableView rectForSection:sender.tag];
    CGRect sectionRect = CGRectOffset(temp, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y);
    NSLog(@"in share btn pressed with tag %ld %@ => %@", (long)sender.tag,  NSStringFromCGRect(sectionRect), NSStringFromCGRect(temp));
    sender.backgroundColor = [UIColor clearColor];
    
    CGRect sectionFrame = CGRectMake(0, temp.origin.y + 50, CGRectGetWidth(self.tableView.frame), [self sectionDataHeight:(int)sender.tag]);
    UIImageView *scGrab = [self rp_screenshotImageViewWithCroppingRect:sectionFrame];

    [self instaGramWallPost:scGrab.image];
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RtInfo", [NSString stringWithFormat:@"%@,%@", _routineName, _workoutName],
                                   @"WtMvd", [NSString stringWithFormat:@"%.1f", weightMvd],
                                   @"TotalWrkTime", totalWorkoutTime,
                                   nil];
    [Flurry logEvent:@"RtDetSumIns" withParameters:articleParams];
    
}

-(void)fbShareBtnPressed:(UIButton *)sender {
    // so we get which section it was clicked.. and then we adjust the offset according to the table.
    CGRect temp =[self.tableView rectForSection:sender.tag];
    CGRect sectionRect = CGRectOffset(temp, -self.tableView.contentOffset.x, -self.tableView.contentOffset.y);
    NSLog(@"fb share btn pressed with tag %ld %@ => %@", (long)sender.tag,  NSStringFromCGRect(sectionRect), NSStringFromCGRect(temp));
    sender.backgroundColor = [UIColor clearColor];
    
    CGRect sectionFrame = CGRectMake(0, temp.origin.y + 50, CGRectGetWidth(self.tableView.frame), [self sectionDataHeight:(int)sender.tag]);
    UIImageView *scGrab = [self rp_screenshotImageViewWithCroppingRect:sectionFrame];

    
    SLComposeViewController *controller = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
    [controller addURL:[NSURL URLWithString:@"http://www.gyminutesapp.com"]];
    [controller addImage:scGrab.image];
    [controller setInitialText:@"Here is my workout progress."];

//    switch (sender.tag) {
//        case 0:
//            [controller setInitialText:@"Here is my workout progress."];
//            break;
//        case 1:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//        case 2:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//        case 3:
//            [controller setInitialText:@"First post from my iPhone app"];
//            break;
//
//        default:
//            break;
//    }
    NSLog(@"data is %@ %@ %@ %f %@", [NSDate date], [PFUser currentUser].objectId,[NSString stringWithFormat:@"%@,%@", _routineName, _workoutName], weightMvd, totalWorkoutTime);
    [self presentViewController:controller animated:YES completion:^{
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       @"RtInfo", [NSString stringWithFormat:@"%@,%@", _routineName, _workoutName],
                                       @"WtMvd", [NSString stringWithFormat:@"%.1f", weightMvd],
                                       @"TotalWrkTime", totalWorkoutTime,
                                       nil];
        [Flurry logEvent:@"RtDetSumFb" withParameters:articleParams];
    }];
    

  }

/*
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{   NSArray *shareImg = [[NSArray alloc] initWithObjects: @"IconTwitter", @"IconInstagram", @"IconFB", nil];
    //    NSArray *shareColor = [[NSArray alloc] initWithObjects:FlatBlue, FlatSkyBlue,FlatCoffee, FlatBlueDark, nil];
    UIView *shareView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.tableView.frame), 30)];
    
    float width = CGRectGetWidth(self.tableView.frame) /3;
    for (int i = 0 ; i < 3; i++) {
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(i * width, 0, width, 30)];
//        btn.backgroundColor = FlatMint;//[shareColor objectAtIndex:i];
        [btn setImage:[UIImage imageNamed:[shareImg objectAtIndex:i]] forState:UIControlStateNormal];
        [[btn imageView] setContentMode: UIViewContentModeScaleAspectFit];
        btn.tag = i;
        [shareView addSubview:btn];
        [btn addTarget:self action:@selector(shareButtonHighlight:) forControlEvents:UIControlEventTouchDown];
        
        [btn addTarget:self action:@selector(shareBtnPressed:) forControlEvents:UIControlEventTouchUpInside];
    }
    return shareView;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.1f;
}
*/
-(CGFloat) sectionDataHeight:(int) section {
    switch (section)
    
    {
        case 0:
            return 300.0f;
        case 1:
            return [workoutTime count] * 50;
        case 2:
            return [exerciseList count] * 50;
        case 3:
            return 300;
    }
    return 100;

}
-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    switch (indexPath.section)
    
    {
        case 0:
            return 300;
        case 1:
            return 50;
        case 2:
            return 50;
        case 3:
            return 300;
        
    }
    return 100;
}

- (UIImageView *)rp_screenshotImageViewWithCroppingRect:(CGRect)croppingRect {
    // For dealing with Retina displays as well as non-Retina, we need to check
    // the scale factor, if it is available. Note that we use the size of teh cropping Rect
    // passed in, and not the size of the view we are taking a screenshot of.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(croppingRect.size, YES, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(croppingRect.size);
    }
    
    // Create a graphics context and translate it the view we want to crop so
    // that even in grabbing (0,0), that origin point now represents the actual
    // cropping origin desired:
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -croppingRect.origin.x, -croppingRect.origin.y);
    [self.view.layer renderInContext:ctx];
    
    // Retrieve a UIImage from the current image context:
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the image in a UIImageView:
    return [[UIImageView alloc] initWithImage:snapshotImage];

}

-(void)instaGramWallPost: (UIImage *) image
{
    NSURL *instagramURL = [NSURL URLWithString:@"instagram://app"];
    if([[UIApplication sharedApplication] canOpenURL:instagramURL]) //check for App is install or not
    {
        NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
        NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"insta.igo"]]; //add our image to the path
        [fileManager createFileAtPath:fullPath contents:imageData attributes:nil]; //finally save the path (image)
        NSLog(@"image saved");
        
        CGRect rect = CGRectMake(0 ,0 , 0, 0);
        UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, self.view.opaque, 0.0);
        [self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
        UIGraphicsEndImageContext();
        NSString *fileNameToSave = [NSString stringWithFormat:@"Documents/insta.igo"];
        NSString  *jpgPath = [NSHomeDirectory() stringByAppendingPathComponent:fileNameToSave];
//        NSLog(@"jpg path %@",jpgPath);
        NSString *newJpgPath = [NSString stringWithFormat:@"file://%@",jpgPath];
//        NSLog(@"with File path %@",newJpgPath);
        NSURL *igImageHookFile = [[NSURL alloc]initFileURLWithPath:newJpgPath];
//        NSLog(@"url Path %@",igImageHookFile);
        
        self.documentController.UTI = @"com.instagram.exclusivegram";
       // self.documentController = [self setupControllerWithURL:igImageHookFile usingDelegate:self];
        self.documentController=[UIDocumentInteractionController interactionControllerWithURL:igImageHookFile];
        NSString *caption = [NSString stringWithFormat:@"%@ just recorded a workout. Here is the progress.  Get your with #gyminutes", [Utilities getUserFullNameFromFacebookOrTwitter]]; //settext as Default Caption
        self.documentController.annotation=[NSDictionary dictionaryWithObjectsAndKeys:[NSString stringWithFormat:@"%@",caption],@"InstagramCaption", nil];
        [self.documentController presentOpenInMenuFromRect:rect inView: self.view animated:YES];
    }
    else
    {
        NSLog (@"Instagram not found");
    }
}

- (UIDocumentInteractionController *) setupControllerWithURL: (NSURL*) fileURL usingDelegate: (id <UIDocumentInteractionControllerDelegate>) interactionDelegate {
    NSLog(@"file url %@",fileURL);
    UIDocumentInteractionController *interactionController = [UIDocumentInteractionController interactionControllerWithURL: fileURL];
    interactionController.delegate = interactionDelegate;
    
    return interactionController;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
