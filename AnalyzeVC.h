//
//  AnalyzeVC.h
//  uLift
//
//  Created by Mayank Verma on 7/23/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AnalyzeVC : UIViewController
@property (nonatomic, retain) IBOutlet UISegmentedControl *analyseSegment;
@property (nonatomic, retain) IBOutlet UISegmentedControl *timeSegment;
@property (nonatomic, retain) IBOutlet UISegmentedControl *parentSegment;
@property (nonatomic, retain) IBOutlet UILabel *headerLabel;
@property (nonatomic, retain) NSString *routineName;
@end
