//
//  AnalyzeVC.m
//  uLift
//
//  Created by Mayank Verma on 7/23/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AnalyzeVC.h"
#import "NavMenu.h"
#import "commons.h"
#import "AnalyzeDetailed.h"

@interface AnalyzeVC () <ChartViewDelegate> {
    NSMutableArray *muscleName, *allDataDates, *workoutName;
    NSArray *allData;
    NSString *selectedMuscle, *selectedWorkout;
    NSDate *selectedDate;
    int oldTimerIndex;
}
@property (nonatomic, strong) IBOutlet PieChartView *pieChartView;
@property (nonatomic, strong) IBOutlet BarChartView *barChartView;

@end

@implementation AnalyzeVC
@synthesize analyseSegment, timeSegment, parentSegment;

- (void)viewDidLoad {
    [super viewDidLoad];

    muscleName = [[NSMutableArray alloc] init];
    workoutName = [[NSMutableArray alloc] init];
    self.title = _routineName;
    NSLog(@"Routine Name is %@", _routineName);
    
}
-(void) viewDidAppear:(BOOL)animated {
    //[KVNProgress showWithStatus:@"Loading.."];
    allData = nil;
    [muscleName removeAllObjects];
    [allDataDates removeAllObjects];
    [workoutName removeAllObjects];
    
    //_pieChartView.backgroundColor = [UIColor whiteColor];
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    
    if ([_routineName isEqualToString:@"All"]) {
        allData = [ExerciseSet MR_findAllSortedBy:@"majorMuscle,date,exerciseName"ascending:NO inContext:localContext];
    } else {
        NSPredicate *routineNamePredicate = [NSPredicate predicateWithFormat:@"routineName == %@", _routineName];
        allData = [ExerciseSet MR_findAllSortedBy:@"majorMuscle,date,exerciseName"ascending:NO withPredicate:routineNamePredicate    inContext:localContext];
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
    [dateFormatter setDateFormat:@"HH:mm:ss"];
    for (ExerciseSet *set in allData) {
        [allDataDates addObject:[dateFormatter dateFromString:set.timeStamp]];
//        NSLog(@"exercise name is %@ at timestamp %@, date %@", set.majorMuscle, set.timeStamp, set.date);
    }

    
    _pieChartView.delegate = self;
    [self setupChart];
    
    //[_pieChartView highlightValues:nil];
    NSMutableArray * uniqueMsc  = [NSMutableArray array];
    NSMutableSet * processedMsc = [NSMutableSet set];

    //muscle based data
    for (ExerciseSet *data in allData) {
        NSString *string = data.majorMuscle;
        if ([processedMsc containsObject:string] == NO) {
            [uniqueMsc addObject:string];
            [processedMsc addObject:string];
            [muscleName addObject:string];
        }
        // NSLog(@"Data %@: %@: %@: %@", data.date, data.exerciseName, data.majorMuscle, data.muscle);
    }
    
    NSMutableArray * uniqueWrk  = [NSMutableArray array];
    NSMutableSet * processedWrk = [NSMutableSet set];
    //workout based data
    for (ExerciseSet *data in allData) {
        NSString *string = data.workoutName;
        if (string == nil)
            continue;
        
        if ([processedWrk containsObject:string] == NO) {
            [uniqueWrk addObject:string];
            [processedWrk addObject:string];
            [workoutName addObject:string];
           // NSLog(@"Data %@: %@: %@: %@", data.date, data.exerciseName, data.majorMuscle, data.workoutName);
        }
    }

    
    analyseSegment.selectedSegmentIndex = 0;
    timeSegment.selectedSegmentIndex = 1;
    parentSegment.selectedSegmentIndex = 0;
    oldTimerIndex = 1;
    
    [analyseSegment addTarget:self action:@selector(analyseSegmentClicked:) forControlEvents:UIControlEventValueChanged];
    [timeSegment addTarget:self action:@selector(timeSegmentClicked:) forControlEvents:UIControlEventValueChanged];
    [parentSegment addTarget:self action:@selector(parentSegmentClicked:) forControlEvents:UIControlEventValueChanged];
    
    [self analyseSegmentClicked:analyseSegment];
    [self timeSegmentClicked:timeSegment];
    
   if ([KVNProgress isVisible]) {
        [KVNProgress dismiss];
    }
}


-(void) setDataValues: (NSMutableArray *) filteredArray {
    _pieChartView.data = nil;
    NSMutableArray *yVals1;
    PieChartData *data;
    PieChartDataSet *dataSet;

    yVals1 = [[NSMutableArray alloc] init];
    
    int count = 0;
    
    if (parentSegment.selectedSegmentIndex == 0)
        count = (int) [muscleName count];
    else if (parentSegment.selectedSegmentIndex == 1)
        count = (int) [workoutName count];
    
    double mult = 100;
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    switch (analyseSegment.selectedSegmentIndex) {
        case 0:
            for (int i = 0; i < count; i++)
            {
                NSPredicate *predicate;
                if (parentSegment.selectedSegmentIndex == 0)
                    predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", [muscleName objectAtIndex:i]];
                else
                    predicate = [NSPredicate predicateWithFormat:@"workoutName == %@", [workoutName objectAtIndex:i]];
                
                NSArray *allSets = [filteredArray filteredArrayUsingPredicate:predicate];
             //   NSLog(@"count %lu", (long) [allSets count]);
                //[yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:([allSets count] + mult / 5) xIndex:i]];
                if (parentSegment.selectedSegmentIndex == 0)
                    [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:([allSets count] + mult / 5) label:[muscleName objectAtIndex:i]]];
                else
                    [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:([allSets count] + mult / 5) label:[workoutName objectAtIndex:i]]];

            }
            break;/*
        case 1: {
            NSMutableArray *uniqueWorkout = [NSMutableArray array];
            
            for (int i = 0; i < count; i++)
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", [muscleName objectAtIndex:i]];
                NSArray *allWorkoutForMuscle = [filteredArray filteredArrayUsingPredicate:predicate];
               // NSLog(@"Workout: Muscle %@ count %lu", [muscleName objectAtIndex:i],                      (long) [allWorkoutForMuscle count]);
                
                //[_pieChartView highlightValues:nil];
                NSMutableSet * processedWorkout = [NSMutableSet set];
                for (ExerciseSet *data in allWorkoutForMuscle) {
                    NSString *string = [NSString stringWithFormat:@"%@,%@", data.majorMuscle, data.date];
                    if ([processedWorkout containsObject:string] == NO) {
                        [uniqueWorkout addObject:data];
                        [processedWorkout addObject:string];
                        NSLog(@"unique: %@", string);
                    }
                }
            }
            NSLog(@"%lu\n",  (long) [uniqueWorkout count]);

            for (int i = 0; i < count; i++)
            {
                NSPredicate *predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", [muscleName objectAtIndex:i]];
                NSArray *allWorkoutForMuscle = [uniqueWorkout filteredArrayUsingPredicate:predicate];
                NSLog(@"Workout: Muscle %@ count %lu", [muscleName objectAtIndex:i],                      (long) [allWorkoutForMuscle count]);
                [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:([allWorkoutForMuscle count] + mult / 5) xIndex:i]];
            }

        }
            break;
                   */
        case 1:
            for (int i = 0; i < count; i++)
            {
                NSPredicate *predicate;
                if (parentSegment.selectedSegmentIndex == 0)
                    predicate = [NSPredicate predicateWithFormat:@"majorMuscle == %@", [muscleName objectAtIndex:i]];
                else
                    predicate = [NSPredicate predicateWithFormat:@"workoutName == %@", [workoutName objectAtIndex:i]];
                
                NSArray *allWtMvd = [filteredArray filteredArrayUsingPredicate:predicate];
                float ttWeight = 0;
                for (int j = 0 ; j < [allWtMvd count]; j++) {
                    ExerciseSet *ex = [allWtMvd objectAtIndex:j];
                    ttWeight += [ex.rep intValue] * [ex.weight floatValue];
                }
//                NSLog(@"Muscle %@count %lu", [muscleName objectAtIndex:i], (long) [allWtMvdForMuscle count]);
                if (parentSegment.selectedSegmentIndex == 0)
                    [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:(ttWeight + mult / 5) label:[muscleName objectAtIndex:i]]];
                else
                    [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:(ttWeight + mult / 5) label:[workoutName objectAtIndex:i]]];
            }
            break;
            
        default:
            break;
    }
    
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    for (int i = 0; i < count; i++)
    {
        if (parentSegment.selectedSegmentIndex == 0)
            [xVals addObject:muscleName[i % muscleName.count]];
        else
            [xVals addObject:workoutName[i % workoutName.count]];
        
    }
    // NSLog(@"yVals %@", yVals1);
    dataSet = [[PieChartDataSet alloc] initWithValues:yVals1 label:@"Results"];
    dataSet.sliceSpace = 1.0;
    
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    for (int i = 0 ; i < count; i++) {
        if (parentSegment.selectedSegmentIndex == 0)
            [colors addObject:[Utilities getMajorMuscleColorSelected:[muscleName objectAtIndex:i]]];
        else {
//            NSPredicate *routinePredicate = [NSPredicate predicateWithFormat:@"routineName == %@", _routineName];
//            RoutineMetaInfo *routineInfo = [RoutineMetaInfo MR_findFirstWithPredicate:routinePredicate];
            [colors addObject: [UIColor randomFlatColor]];
        }
    }
    //    [colors addObjectsFromArray:ChartColorTemplates.vordiplom];
    //    [colors addObjectsFromArray:ChartColorTemplates.joyful];
    //    [colors addObjectsFromArray:ChartColorTemplates.colorful];
    //    [colors addObjectsFromArray:ChartColorTemplates.liberty];
    //    [colors addObjectsFromArray:ChartColorTemplates.pastel];
    //    [colors addObject:[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f]];
    //
    dataSet.colors = colors;
    
    data = [[PieChartData alloc] initWithDataSet:dataSet];
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14.f]];
    [data setValueTextColor:UIColor.whiteColor];
    
    _pieChartView.data = data;


}
-(void) parentSegmentClicked:(UISegmentedControl *) segmentedControl {
    NSLog(@"parent segment selected...");
    [self timeSegmentClicked:timeSegment];
}
-(void)analyseSegmentClicked:(UISegmentedControl *)segmentedControl {
//    [self setDataValues];
    [self timeSegmentClicked:timeSegment];
    NSLog(@"Analyze segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
}

-(void)timeSegmentClicked:(UISegmentedControl *)segmentedControl {

    bool showAnalysis = false;
    
    if (segmentedControl.selectedSegmentIndex > 1) {
        if ([Utilities showAnalyticsPackage]) {
            [self showPurchasePopUp:@"This is a paid feature."];
            timeSegment.selectedSegmentIndex = oldTimerIndex;
        } else
            showAnalysis = true;
    } else
        showAnalysis = true;
    
    if (showAnalysis == true) {
        oldTimerIndex = (int) segmentedControl.selectedSegmentIndex;
        
        NSString *dateStringForHeaders = @"";
        NSMutableArray *filteredArrayByDate = [[NSMutableArray alloc] init];
        NSLog(@"segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *today = [NSDate date];
        
        switch (segmentedControl.selectedSegmentIndex) {
            case 0:
                selectedDate = [today dateBySubtractingWeeks:1];
                dateStringForHeaders = @"last week.";
                break;
            case 1:
                selectedDate = [today dateBySubtractingWeeks:2];
                dateStringForHeaders = @"last 2 weeks.";
                break;
            case 2:
                selectedDate = [today dateBySubtractingMonths:1];
                dateStringForHeaders = @"last 1 month.";
                break;
            case 3:
                selectedDate = [today dateBySubtractingMonths:3];
                dateStringForHeaders = @"last 3 months.";
                break;
            case 4:
                selectedDate = [today dateBySubtractingMonths:6];
                dateStringForHeaders = @"last 6 months.";
                break;
            case 5:
                selectedDate = [today dateBySubtractingYears:1];
                dateStringForHeaders = @"last 1 year.";
                break;
            default:
                break;
        }
        
        for (ExerciseSet *ex in allData) {
            NSDate *date = [formatter dateFromString:ex.date];
            if ([date isLaterThanOrEqualTo:selectedDate]) {
                [filteredArrayByDate addObject:ex];
            } else {
            }
            
        }
        
        
        [self setDataValues:filteredArrayByDate];
        
        if (analyseSegment.selectedSegmentIndex == 0) {
            _headerLabel.text = [NSString stringWithFormat:@"Sets performed %@", dateStringForHeaders];
        } else if (analyseSegment.selectedSegmentIndex == 1) {
            //        _headerLabel.text = [NSString stringWithFormat:@"Workouts performed %@", dateStringForHeaders];
            //  } else {
            _headerLabel.text = [NSString stringWithFormat:@"Weight moved %@", dateStringForHeaders];
        }
    }
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) setupChart {
    _pieChartView.usePercentValuesEnabled = YES;
    _pieChartView.holeColor = [UIColor clearColor];
//    _pieChartView.centerText = [UIFont fontWithName:@"HelveticaNeue-Light" size:14.f];
    _pieChartView.holeRadiusPercent = 0.18;
    _pieChartView.transparentCircleRadiusPercent = 0.21;
    _pieChartView.descriptionText = @"";
    _pieChartView.drawCenterTextEnabled = YES;
    _pieChartView.drawHoleEnabled = NO;
    _pieChartView.rotationAngle = 0.0;
    _pieChartView.rotationEnabled = YES;
    _pieChartView.legend.enabled = NO;

//    ChartLegend *l = _pieChartView.legend;
//    l.position = ChartLegendPositionPiechartCenter;
//    l.xEntrySpace = 7.0;
//    l.yEntrySpace = 0.0;
//    l.yOffset = 0.0;
    
    [_pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];

}



#pragma mark - pieChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    if (parentSegment.selectedSegmentIndex == 0)
        selectedMuscle = [muscleName objectAtIndex:entry.x];
    else
        selectedWorkout = [workoutName objectAtIndex:entry.x];
    
    //NSLog(@"chartValueSelected %ld %@", (long)dataSetIndex, [muscleName objectAtIndex:entry.xIndex]);
    [self performSegueWithIdentifier:@"detailedAnalysisSegue" sender:self];

}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {

    if ([segue.identifier isEqualToString:@"detailedAnalysisSegue"]) {
        AnalyzeDetailed *dest = segue.destinationViewController;
        dest.muscle = selectedMuscle;
        dest.workout = selectedWorkout;
        dest.allData = allData;
        dest.routineName = _routineName;
        if (parentSegment.selectedSegmentIndex == 0)
            dest.muscleOrWorkout = 0;
        else
            dest.muscleOrWorkout = 1;
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ANALYTICS PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ANALYTICS_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium or Analytics Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}


@end
