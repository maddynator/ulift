//
//  AddExerciseVC.m
//  uLift
//
//  Created by Mayank Verma on 7/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "AddExerciseVC.h"
#import "AddExerciseForm.h"

@interface AddExerciseVC () {
    NSArray *muscleList, *typeList, *equipmentList, *expLevelList, *mechanicsList, *majorMuscleList;
}
@end

@implementation AddExerciseVC
@synthesize majorMuscle;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Add Exercise";
    AddExerciseForm *form = [[AddExerciseForm alloc] init];
    self.formController.form = form;
    muscleList = [[NSArray alloc] initWithObjects: @"Abs",
                           @"Back",
                           @"Biceps",
                           @"Calves",
                           @"Chest",
                           @"Forearm",
                           @"Glutes",
                           @"Hamstring",
                           @"Lats",
                           @"Quads",
                           @"Shoulders",
                           @"Traps",
                           @"Triceps", nil];

    majorMuscleList = [[NSArray alloc] initWithObjects: @"Abs",
                       @"Arms",
                  @"Back",
                  @"Chest",
                  @"Legs",
                  @"Shoulders", nil];

    typeList = [[NSArray alloc] initWithObjects: @"Strength",
                @"Plyometrics",
                @"Powerlifting",
                @"Weightlifting", nil];
    
    equipmentList = [[NSArray alloc] initWithObjects:@"Barbell",
                @"BodyWeight",
                @"Cable",
                @"Dumbbell",
                @"ExerciseBall",
                @"Machine",
                @"Kettlebell",
                @"Other", nil];
    
    expLevelList = [[NSArray alloc] initWithObjects:@"Beginner",
                    @"Intermediate",
                    @"Experienced", nil];
    
    mechanicsList = [[NSArray alloc] initWithObjects: @"Isolated",
                        @"Compound", nil];
    
    NSLog(@"major muscl %@", majorMuscle);
    for (int i = 0;i < [majorMuscleList count]; i++) {
        if ([majorMuscle isEqualToString:[majorMuscleList objectAtIndex:i]]) {
            form.muscleGroup = i;
            break;
        }
    }
    self.definesPresentationContext = YES;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)submitExerciseForm:(UITableViewCell<FXFormFieldCell> *)cell {
    AddExerciseForm *form = cell.field.form;
    
    
    if ([form.name length] < 3) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Error" subTitle:@"Exercise name too short." closeButtonTitle:@"Ok" duration:0.0];
        return;
    } else if (form.name == nil) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert showError:self title:@"Error" subTitle:@"Exercise name cannot be empty." closeButtonTitle:@"Ok" duration:0.0];
        return;
    }
    
    NSLog(@"exercise name lent %lu", (unsigned long)[form.name length]);
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"exerciseName == [c] %@", form.name];
    
    ExerciseList *temp = [ExerciseList MR_findFirstWithPredicate:predicate inContext:localContext];
    
    if (temp != nil) {
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert showError:self title:@"Error" subTitle:@"Exercise Already Present." closeButtonTitle:@"Ok" duration:0.0];
        return;
    }

    ExerciseList *exercise = [ExerciseList MR_createEntityInContext:localContext];
    exercise.exerciseName = form.name;
    exercise.equipment = [equipmentList objectAtIndex:form.equipment];
    exercise.muscle = [muscleList objectAtIndex:form.muscle];
    exercise.type = [typeList objectAtIndex:form.type];
    exercise.majorMuscle = [majorMuscleList objectAtIndex:form.muscleGroup];
    exercise.expLevel = [expLevelList objectAtIndex:form.experience];
    exercise.mechanics = [mechanicsList objectAtIndex:form.mechanics];
    
    [localContext MR_saveToPersistentStoreAndWait];
    
    PFObject *userEx = [PFObject objectWithClassName:@P_USEREXERCISE_CLASS];
    userEx[@"UserId"] = [PFUser currentUser].objectId;
    userEx[@"ExerciseName"] = exercise.exerciseName;
    userEx[@"ExpLevel"] = [expLevelList objectAtIndex:form.experience];
    userEx[@"Mechanics"] = [mechanicsList objectAtIndex:form.mechanics];
    userEx[@"Muscle"]  = [muscleList objectAtIndex:form.muscle];
    userEx[@"Type"] = [typeList objectAtIndex:form.type];
    userEx[@"Equipment"] = [equipmentList objectAtIndex:form.equipment];
    userEx[@"MajorMuscle"] = [majorMuscleList objectAtIndex:form.muscleGroup];
    [userEx saveEventually];
    
    [self.navigationController popViewControllerAnimated:YES];
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"ExInfo", [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@", exercise.exerciseName, exercise.equipment, exercise.muscle, exercise.majorMuscle, exercise.type, exercise.expLevel, exercise.mechanics],
                                   nil];
    [Flurry logEvent:@"DailyStatsRecorded" withParameters:articleParams];

//    Mixpanel *mixpanel = [Mixpanel sharedInstance];
//    [mixpanel track:@"DailyStatsRecorded" properties:articleParams];

}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
