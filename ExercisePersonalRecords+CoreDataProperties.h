//
//  ExercisePersonalRecords+CoreDataProperties.h
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "ExercisePersonalRecords+CoreDataClass.h"


NS_ASSUME_NONNULL_BEGIN

@interface ExercisePersonalRecords (CoreDataProperties)

+ (NSFetchRequest<ExercisePersonalRecords *> *)fetchRequest;

@property (nullable, nonatomic, copy) NSString *exerciseName;
@property (nullable, nonatomic, copy) NSDate *prDate;
@property (nullable, nonatomic, copy) NSNumber *prExerciseNumber;
@property (nullable, nonatomic, copy) NSNumber *prExerciseNumInGroup;
@property (nullable, nonatomic, copy) NSNumber *prGroupNumber;
@property (nullable, nonatomic, copy) NSNumber *prRep;
@property (nullable, nonatomic, copy) NSDate *prTime;
@property (nullable, nonatomic, copy) NSNumber *prUserWeight;
@property (nullable, nonatomic, copy) NSNumber *prWeight;
@property (nullable, nonatomic, copy) NSNumber *roundNumber;
@property (nullable, nonatomic, copy) NSString *routineName;
@property (nullable, nonatomic, copy) NSNumber *syncedState;
@property (nullable, nonatomic, copy) NSString *workoutName;

@end

NS_ASSUME_NONNULL_END
