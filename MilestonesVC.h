//
//  MilestonesVC.h
//  gyminutes
//
//  Created by Mayank Verma on 7/7/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface MilestonesVC : UIViewController <UITableViewDelegate, UITableViewDataSource, DZNEmptyDataSetSource, DZNEmptyDataSetDelegate>
@property (nonatomic, retain) IBOutlet UITableView *tableView;
@end
