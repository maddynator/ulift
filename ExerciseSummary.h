//
//  ExerciseSummary.h
//  uLift
//
//  Created by Mayank Verma on 10/7/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExerciseSummary : UITableViewController
@property (nonatomic, retain) NSString *date, *routineName, *workoutName;
@end
