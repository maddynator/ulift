//
//  Achievement+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "Achievement+CoreDataProperties.h"

@implementation Achievement (CoreDataProperties)

+ (NSFetchRequest<Achievement *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"Achievement"];
}

@dynamic dateAchieved;
@dynamic exerciseName;
@dynamic renewRate;
@dynamic routineName;
@dynamic syncedState;
@dynamic timeAchieved;
@dynamic title;
@dynamic workoutName;

@end
