//
//  NavMenu.m
//  uLift
//
//  Created by Mayank Verma on 7/17/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "NavMenu.h"
#import "commons.h"
#import "TodayWorkout.h"
#import "UserProfileVC.h"
#import "AnalyzeRoutine.h"

@interface NavMenu ()
@property(nonatomic, strong)RBMenu *menu;

@end

@implementation NavMenu

- (void)viewDidLoad {
    [super viewDidLoad];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    //creating the menu items
    TodayWorkout *firstViewController = [storyboard instantiateViewControllerWithIdentifier:@"TodayWorkout"];
    [self setViewControllers:@[firstViewController] animated:NO];
    
    RBMenuItem *item = [[RBMenuItem alloc]initMenuItemWithTitle:@"Home" withCompletionHandler:^(BOOL finished){
        
        TodayWorkout *firstViewController = [storyboard instantiateViewControllerWithIdentifier:@"TodayWorkout"];
        [self setViewControllers:@[firstViewController] animated:NO];
        
    }];
    
    RBMenuItem *item2 = [[RBMenuItem alloc]initMenuItemWithTitle:@"Profile" withCompletionHandler:^(BOOL finished){
        
        UserProfileVC *secondViewController = [storyboard instantiateViewControllerWithIdentifier:@"UserProfile"];
        [self setViewControllers:@[secondViewController] animated:NO];
    }];
    
    RBMenuItem *item3 = [[RBMenuItem alloc]initMenuItemWithTitle:@"Settings" withCompletionHandler:^(BOOL finished){
        
        UserProfileVC *secondViewController = [storyboard instantiateViewControllerWithIdentifier:@"Settings"];
        [self setViewControllers:@[secondViewController] animated:NO];
    }];
    
    RBMenuItem *item4 = [[RBMenuItem alloc]initMenuItemWithTitle:@"Analyze" withCompletionHandler:^(BOOL finished){
        
        AnalyzeRoutine *secondViewController = [storyboard instantiateViewControllerWithIdentifier:@"Analyze"];
        [self setViewControllers:@[secondViewController] animated:NO];
    }];

    RBMenuItem *item5 = [[RBMenuItem alloc]initMenuItemWithTitle:@"Routines" withCompletionHandler:^(BOOL finished){
        
        RoutineList *secondViewController = [storyboard instantiateViewControllerWithIdentifier:@"Routine"];
        [self setViewControllers:@[secondViewController] animated:NO];
    }];

    
    // Do any additional setup after loading the view, typically from a nib.
    _menu = [[RBMenu alloc] initWithItems:@[item, item2, item3, item4, item5] textColor:[UIColor whiteColor] hightLightTextColor:[UIColor blackColor] backgroundColor:FlatOrange andTextAlignment:RBMenuTextAlignmentLeft forViewController:self];
    _menu.height = 280;//230.0f;//190

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)showMenu{
    
    [_menu showMenu];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
