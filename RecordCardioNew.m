//
//  RecordCardioNew.m
//  gyminutes
//
//  Created by Mayank Verma on 6/19/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "RecordCardioNew.h"

@interface RecordCardioNew () {
    NSArray *cardioFields;
    NSMutableArray *cardioPlaceholders;
    NSMutableArray *_pickerData;
    int selectedIndex, selectedRow;

}
@property (nonatomic, strong) CNPPopupController *popupController;

@end

@implementation RecordCardioNew
@synthesize tableView;

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"Cardio Log";
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];

    cardioFields = @[@"Cardio Type", @"Training Type",  @"Duration", @"Distance", @"Empty Stomach", @"Calories Burned", @"Elevation", @"Start Time", @"Save"];
    
    NSString *distance = @"1 Mile";
    if ([Utilities isKgs]) {
        distance = @"1 Kms";
    }
    cardioPlaceholders = [[NSMutableArray alloc] initWithArray:@[@"RUNNING", @"HIGH INTENSITY (HIIT)", @"25 Mins", distance, @"YES", @"350", @"0", [dateFormatter stringFromDate:[NSDate date]], @"Save"]];
    
    // init table view
    tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    
    // must set delegate & dataSource, otherwise the the table will be empty and not responsive
    tableView.delegate = self;
    tableView.dataSource = self;
    tableView.separatorColor = [UIColor clearColor];
    tableView.allowsSelection = false;
    _pickerData = [[NSMutableArray alloc] init];
    // add to canvas
    [self.view addSubview:tableView];
}

#pragma mark - UITableViewDataSource
// number of section(s), now I assume there is only 1 section
- (NSInteger)numberOfSectionsInTableView:(UITableView *)theTableView
{
    return 1;
}

// number of row in the section, I assume there is only 1 row
- (NSInteger)tableView:(UITableView *)theTableView numberOfRowsInSection:(NSInteger)section
{
    return [cardioFields count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}
// the cell will be returned to the tableView
- (UITableViewCell *)tableView:(UITableView *)theTableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"HistoryCell";
    
    // Similar to UITableViewCell, but
    UITableViewCell *cell = (UITableViewCell *)[theTableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[UIButton class]]) {
            [lbl removeFromSuperview];
        }
    }
    
    if (indexPath.row == [cardioFields count] - 1) {
        UIButton *item = [[UIButton alloc] initWithFrame:CGRectMake(20, 15, CGRectGetWidth(self.tableView.frame) - 40, 30)];
        
        [item setTitle:[cardioPlaceholders objectAtIndex:indexPath.row] forState:UIControlStateNormal];
        item.backgroundColor = [UIColor whiteColor];
        [item setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
        item.layer.cornerRadius = 5;
        [item.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];

        [item addTarget:self action:@selector(saveClicked:) forControlEvents:UIControlEventTouchUpInside];
        cell.backgroundColor = [Utilities getAppColor];
        [cell.contentView addSubview:item];
        
    } else {
        cell.backgroundColor = [UIColor whiteColor];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(self.tableView.frame)/2, 40)];
    
    label.text  = [cardioFields objectAtIndex:indexPath.row];
    label.textColor = [Utilities getAppColor];
    [label setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:18]];
    label.numberOfLines = 0;
    
    UIButton *item = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(label.frame), 5, CGRectGetWidth(self.tableView.frame) /2 - 10, 30)];
    
    [item setTitle:[cardioPlaceholders objectAtIndex:indexPath.row] forState:UIControlStateNormal];
    item.backgroundColor = [Utilities getAppColor];
    [item setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    item.layer.cornerRadius = 5;
    [item.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:12]];
    item.tag = indexPath.row;
    [item addTarget:self action:@selector(pickerClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    [cell.contentView addSubview:label];
    [cell.contentView addSubview:item];
    }
    return cell;
}

-(IBAction)saveClicked:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - UITableViewDelegate
// when user tap the row, what action you want to perform
//- (void)tableView:(UITableView *)theTableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
-(IBAction)pickerClicked:(id)sender {
    //@[@"CARDIO TYPE", @"TRAINING TYPE",  @"DURATION", @"EMPTY STOMACH", @"CALORIES BURNED", @"ELEVATION", @"START TIME"];
    [_pickerData removeAllObjects];

    UIButton *button = (UIButton *) sender;
    selectedIndex =  (int)button.tag;
    
    switch (button.tag) {
        case 0: {
            [_pickerData addObject:@"RUNNING"];
            [_pickerData addObject:@"CYCLING"];
            [_pickerData addObject:@"SWIMMING"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"RUNNING"])
                selectedRow = 0;
            else if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"CYCLING"])
                selectedRow = 1;
            else
                selectedRow = 2;

        }
            break;
        case 1: {
            [_pickerData addObject:@"HIGH INTENSITY (HIIT)"];
            [_pickerData addObject:@"STEADY STATE"];
            [_pickerData addObject:@"LOW INTENSITY (LIIT)"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"HIGH INTENSITY (HIIT)"])
                selectedRow = 0;
            else if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"STEADY STATE"])
                selectedRow = 1;
            else
                selectedRow = 2;
        }
            break;
        case 2: {
            for (int i = 10; i <= 90; i++) {
                NSString *time = [NSString stringWithFormat:@"%d Mins", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i-10;
            }
            
        }
            break;
        case 3: {
            for (float i = 1; i <= 1000; i++) {
            NSString *time = [NSString stringWithFormat:@"%.2f", i];
            [_pickerData addObject:time];
            if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                selectedRow = i - 50;
            }
        }
            break;
        case 4: {
            
            [_pickerData addObject:@"YES"];
            [_pickerData addObject:@"NO"];
            if ([[cardioPlaceholders objectAtIndex:selectedIndex] isEqualToString:@"YES"])
                selectedRow = 0;
            else
                selectedRow = 1;
        }
            break;
        case 5: {
            for (int i = 50; i <= 1000; i++) {
                NSString *time = [NSString stringWithFormat:@"%d", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i - 50;
            }

        }
            break;
        case 6: {
            for (float i = 0 ; i <= 15; i +=.5) {
                NSString *time = [NSString stringWithFormat:@"%.1f", i];
                [_pickerData addObject:time];
                if ([time isEqualToString:[cardioPlaceholders objectAtIndex:selectedIndex]])
                    selectedRow = i*2;
            }
            
        }
            break;
            
        default:
            break;
    }
    
    [self showPicker: [cardioFields objectAtIndex:button.tag]];
    [self.picker selectRow:selectedRow inComponent:0 animated:YES];
}


#pragma UIPickerView - Delegate
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView {
    
    return 1;//Or return whatever as you intend
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    
    return [_pickerData count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [_pickerData objectAtIndex:row];
}

// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    NSLog(@"row selected is %@", _pickerData[row]);
    [cardioPlaceholders replaceObjectAtIndex:selectedIndex withObject:_pickerData[row]];
    [self.popupController dismissPopupControllerAnimated:YES];
    [self.tableView reloadData];
}

- (void)showPicker: (NSString *) titleButton {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:titleButton attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    
    // Connect data
    self.picker = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 250, 200)];
    self.picker.dataSource = self;
    self.picker.delegate = self;
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, self.picker]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    //self.popupController.theme.popupContentInsets = UIEdgeInsetsZero;
    
    [self.popupController presentPopupControllerAnimated:YES];
}


@end
