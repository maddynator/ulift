//
//  UpdateHandler.h
//  gyminutes
//
//  Created by Mayank Verma on 6/21/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "commons.h"

@interface UpdateHandler : NSObject

+(void) v2_update_UserProfile_membershipType;
+(void) updateUserProfileWithMemberShipInfo;
+(NSString *) getMembershipType;
+(void) v2_4_update_UserWorkout_to_AMRAP;
+(void) v2_4_update_WorkoutList_to_AMRAP;
@end
