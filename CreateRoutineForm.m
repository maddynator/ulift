//
//  CreateRoutineForm.m
//  uLift
//
//  Created by Mayank Verma on 8/21/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "CreateRoutineForm.h"

@implementation CreateRoutineForm


- (NSArray *)fields
{
    return @[
             @{FXFormFieldKey: @"name", FXFormFieldHeader: @"Routine Information",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"Unique name for routine."},

             @{FXFormFieldKey: @"goal",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords), FXFormFieldPlaceholder: @"Specific goal for routine."},

             @{FXFormFieldKey: @"summary",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"Summary of routine."},
             
             @{FXFormFieldKey: @"desc",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"Describe routine in detail."},
             
             @{FXFormFieldKey: @"instructions",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"How to perform this routine."},

             @{FXFormFieldKey: @"type", FXFormFieldOptions: @[@"Strength/PowerLifting", @"Bodybuilding", @"Sports"]},
             
             @{FXFormFieldKey: @"level", FXFormFieldOptions: @[@"Beginners(0-1 Year)", @"Intermediate (1-3 Year)", @"Advance (3-5 Years)", @"Monster (5+ years)"]},

             @{FXFormFieldKey:@"duration", @"textField.keyboardType": @(UIKeyboardTypeNumberPad),FXFormFieldPlaceholder: @"Like 90 days routine duration."},
             
             @{FXFormFieldKey: @"color", FXFormFieldOptions: @[@"Red", @"Blue", @"Pink", @"Magenta", @"Brown", @"Coffee", @"Maroon", @"Mint"]},
             
             @{FXFormFieldKey: @"faqs",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"Enter URL with faqs."},

             @{FXFormFieldKey: @"discalimer",
               @"textField.autocapitalizationType": @(UITextAutocapitalizationTypeWords),FXFormFieldPlaceholder: @"Please check with your primary care physician before performing this routine. The information is not intended to be a substitute for informed medical advice or care. In no event shall we (Gyminutes) be liable for damages of any kind arising from the use of this routine, including but not limited to, direct, indirect, incidental, punitive or consequential damages."}

             
             ];
}
@end
