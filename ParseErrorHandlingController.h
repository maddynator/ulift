//
//  ParseErrorHandlingController.h
//  gyminutes
//
//  Created by Mayank Verma on 1/13/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "commons.h"
@interface ParseErrorHandlingController : NSObject
+ (void)handleParseError:(NSError *)error;
@end
