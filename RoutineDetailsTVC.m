//
//  RoutineDetailsTVC.m
//  uLift
//
//  Created by Mayank Verma on 9/20/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "RoutineDetailsTVC.h"
#import "commons.h"
#import "RoutineWebView.h"

#define DISCLAIMER  "Please check with your primary care physician before performing this routine. The information is not intended to be a substitute for informed medical advice or care. In no event shall we (Gyminutes) be liable for damages of any kind arising from the use of this routine, including but not limited to, direct, indirect, incidental, punitive or consequential damages."
@interface RoutineDetailsTVC () {
    NSArray *tableContentHdr, *tableContent;
    NSString *routineName, *author;
    NSString *routineURL;
    NSDictionary *imageDict;
    NSDictionary *routineInfo;
}
@end

@implementation RoutineDetailsTVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    tableContentHdr = [[NSArray alloc] initWithObjects:@"Goal", @"Summary", @"Description", @"Duration", @"Instructions", @"Type", @"Level", @"FAQs", @"Disclaimer", nil];
    
    imageDict = @{@IAP_ROUTINE_BODYWEIGHT_MAX: @{@"Image": [UIImage imageNamed:@"IconWRBodyWeightMax"],
                                                 @"Goal": @"Get fit and stay active.",
                                                 @"Summary": @"Beginner full body workout routine.",
                                                 @"Description": @"Don't have time to go to gym. Thats Ok. We have been there too. With this bodyweight routine, you can workout any time, anywhere with the best equipment you have, \"YOUR BODY\".",
                                                 @"Duration": @"Perform workout for 60 days.",
                                                 @"Instructions": @"Perform the same workout 3 times a week.\nDay 1 - Workout\n Day 2 - Rest Day\nDay 3 - Workout\nDay 4 - Rest Day\nDay 5 - Workout\nDay 6 - Rest Day\nDay 7 - Rest Day",
                                                 @"Type": @"Bodybuilding",
                                                 @"Level": @"Beginners(0-1 Year)",
                                                 @"FAQs": @"http://www.gyminutesapp.com/BodyweightMax.html",
                                                 @"Disclaimer": @DISCLAIMER,
                                                 @"Name": @"Bodyweight Max"
                                                 },
                   
                   @IAP_ROUTINE_HYPERTROPHY_UNLIMITED: @{@"Image": [UIImage imageNamed:@"IconWRHypertrophyUnlimited"],
                                                         @"Goal": @"Get bigger muscle.",
                                                         @"Summary": @"Beginner bodybuilding routine with focus on higher volume workout",
                                                         @"Description": @"This routine is guaranteed to make blood rush into your muscle.",
                                                         @"Duration": @"Perform workout for 90 days.",
                                                         @"Instructions": @"It is recommended that you perform this routine for 90 days period. Routine has to be performed 5 days per week.\n\nDay 1 - Chest Day\nDay 2 - Back Day\nDay 3 - Rest\nDay 4 - Shoulders Day\nDay 5 - Arms Day \nDay 6 - Legs Day \nDay 7 - Rest Day\n",
                                                         @"Type": @"Bodybuilding",
                                                         @"Level": @"Beginners(0-1 Year)",
                                                         @"FAQs": @"http://www.gyminutesapp.com/HypertrophyUnlimited.html",
                                                         @"Disclaimer": @DISCLAIMER,
                                                         @"Name": @"Hypertrophy Unlimited"
                                                         },
                   @IAP_ROUTINE_2DAYSPLITPUSHPULL:@{@"Image": [UIImage imageNamed:@"IconWR2DaySplitPushPull"],
                                                    @"Goal": @"Separating your body parts by function, you will be able to give each muscle group adequate recovery between workouts and also hit the gym more often because the muscles you’re training each time are not exhausted from your previous days workout.",
                                                    @"Summary": @"This 2 Day PUSH/PULL workout organizes workouts such that you perform all pushing or pressing exercises during one workout, and pulling or rowing movements in the next workout.",
                                                    @"Description": @"2 Day Split workout is for people who don't have much time to workout but would like to get strong and build muscle.",
                                                    @"Duration": @"Perform workout for 60 days.",
                                                    @"Instructions": @"Perform Workout Push and Workout PULL alternatively. Perform maximum of 2 workouts per week. For example,\n\nDay 1 - Workout Push\nDay 2 - Rest Day\nDay 3 - Rest Day\nDay 4 - Workout Pull\nDay 5 - Rest Day\nDay 6 - Rest Day\nDay 7 - Rest Day\n",
                                                    @"Type": @"Strength/PowerLifting",
                                                    @"Level": @"Advance (3-5 Years)",
                                                    @"FAQs": @"http://www.gyminutesapp.com/2daysplitpushpull.html",
                                                    @"Disclaimer": @DISCLAIMER,
                                                    @"Name": @"2 Day Split:Push/Pull"
                                                    },
                   @IAP_ROUTINE_3DAYSPLIT:@{@"Image":  [UIImage imageNamed:@"IconWR3DaySplit"],
                                            @"Goal": @"Focus on building lean muscle by working out 3 days per week.",
                                            @"Summary": @"3 Day Lean Muscle workout strikes the perfect balanace between workout intensity, volume and the rest time between each set and exercise.",
                                            @"Description": @"3 Day Lean Muscle Workout is for people who play recreational sports but are looking to build lean muscle on off days. This workout divides the muscle into opporsite groups such that primary and secondary muscle overlap between two muscle group is minimal.",
                                            @"Duration": @"Perform workout for 90 days.",
                                            @"Instructions": @"Perform Workouts as below\n\nDay 1 - 3LM: Legs & Shoulders\nDay 2 - Rest Day\nDay 3 - 3LM: Chest, Biceps & Abs\nDay 4 - Rest Day\nDay 5 - 3LM: Back, Triceps & Traps\nDay 6 - Rest Day\nDay 7 - Rest Day\n",
                                            @"Type": @"Bodybuilding",
                                            @"Level": @"Beginners(0-1 Year)",
                                            @"FAQs": @"http://www.gyminutesapp.com/3dayleanmuscle.html",
                                            @"Disclaimer": @DISCLAIMER,
                                            @"Name": @"3 DAY LEAN MUSCLE"
                                            },
                   @IAP_ROUTINE_4DAYSPLIT: @{@"Image": [UIImage imageNamed:@"IconWR4DaySplit"],
                                             @"Goal": @"Build muscle mass by using high volume with medium intensity workout.",
                                             @"Summary": @"Packing on muscle mass with high volume bodybuilding style workout. Each workout focused on achieving the pump.",
                                             @"Description": @"4 Day Muscle Mass workouts provides the largest muscle groups with their individual workout day in order to eliminate overlap throughout the week. With high volume at focus, each workout provide all muscle maximum stimulation necessary for growth and packing on muscle  mass.",
                                             @"Duration": @"Perform workout for 90 days.",
                                             @"Instructions": @"Perform Workouts schedule is as below\n\nDay 1 - 4MM: Legs\nDay 2 - 4MM: Shoulders & Abs\nDay 3 - Rest Day\nDay 4 - 4MM: Back & Biceps\nDay 5 - 4MM: Chest & Triceps\nDay 6 - Rest Day\nDay 7 - Rest Day\n",
                                             @"Type": @"Bodybuilding",
                                             @"Level": @"Beginners(0-1 Year)",
                                             @"FAQs": @"http://www.gyminutesapp.com/4daymusclemass.html",
                                             @"Disclaimer": @DISCLAIMER,
                                             @"Name": @"4 DAY MUSCLE MASS"
                                             },
                   @IAP_ROUTINE_5DAYSPLIT: @{@"Image": [UIImage imageNamed:@"IconWR5DaySplit"],
                                             @"Goal": @"Build muscle by isolating each muscle group for maximum stimulation.",
                                             @"Summary": @"The best workout split for intermediate to advance users whose primary focus is building muscle.",
                                             @"Description": @"Pentagram Power focuses on only a few muscles per workout session. Each workout is designed to maximize the stimulation of your muscle fibers to effectively increase mass and strength.",
                                             @"Duration": @"Perform workout for 90 days.",
                                             @"Instructions": @"Perform Workouts schedule is as below\n\nDay 1 - CHEST POWER\nDay 2 - BACK POWER\nDay 3 - Rest Day/Cardio\nDay 4 - ARMS POWER\nDay 5 - LEGS POWER\nDay 6 - SHOULDER POWER\nDay 7 - Rest Day\n",
                                             @"Type": @"Bodybuilding",
                                             @"Level": @"Intermediate (1-3 Year)",
                                             @"FAQs": @"http://www.gyminutesapp.com/pentagrampower.html",
                                             @"Disclaimer": @DISCLAIMER,
                                             @"Name": @"Pentagram Power"
                                             },
                   @IAP_ROUTINE_6DAYSPLIT:@{@"Image": [UIImage imageNamed:@"IconWR6DaySplit"],
                                            @"Goal": @"The 6-Day Workout Split is a advance bodybuilding routine designed to maximize fat burn and muscle growth.",
                                            @"Summary": @"Focus on each muscle individually for maximum muscle gain",
                                            @"Description": @"This 6 day split workout routine is a brutal muscle building workout. Primarily based on volume training methodology to increase lean mass and isolate each muscle.",
                                            @"Duration": @"Perform workout for 90 days.",
                                            @"Instructions": @"Perform Workouts schedule is as below\n\nDay 1 - SS: Chest\nDay 2 - SS: Back\nDay 3 - SS: Abs\nDay 4 - SS: Biceps & Triceps\nDay 5 - SS: Legs\nDay 6 - SS: Shoulders\nDay 7 - Rest Day\n",
                                            @"Type": @"Bodybuilding",
                                            @"Level": @"Advance (3-5 Years)",
                                            @"FAQs": @"http://www.gyminutesapp.com/strongsix.html",
                                            @"Disclaimer": @DISCLAIMER,
                                            @"Name": @"Bodyweight Max"
                                            },
                   @IAP_ROUTINE_3DAYDUMBBELLDOMINATION: @{@"Image": [UIImage imageNamed:@"IconWR3DS"],
                                                          @"Goal": @"The workout focus on heavy volume but just by using dumbbells.",
                                                          @"Summary": @"Start building muscles at home or gym with just dumbbells.",
                                                          @"Description": @"Unlike other workout routines, you only need a pair of dumbbells and a bench to start building muscle. This workouts is best to do at home or at gym.",
                                                          @"Duration": @"Perform workout for 60 days.",
                                                          @"Instructions": @"Start with a dumbbell and progressively increase the load in each workout. Perform workouts as follows\n\nDay 1 - 3DS: Legs & Shoulders\nDay 2 - Rest Day\nDay 3 - 3DS: Arms & Abs\nDay 4 - Rest Day\nDay 5 - 3DS: Chest & Back\nDay 6 - Rest Day\nDay 7 - Rest Day\n",
                                                          @"Type": @"Bodybuilding",
                                                          @"Level": @"Beginners(0-1 Year)",
                                                          @"FAQs": @"http://www.gyminutesapp.com/3daydumbbelldomination.html",
                                                          @"Disclaimer": @DISCLAIMER,
                                                          @"Name": @"3 DAY DUMBBELL DOMINATION"
                                                          },
                   @IAP_ROUTINE_FOCUSSED_FOUR:@{@"Image": [UIImage imageNamed:@"IconWRFocussedFour"],
                                                @"Goal": @"Strengthen core, build muscle and get lean.",
                                                @"Summary": @"Focussed four is great for people who are in sports and want to step up their game.",
                                                @"Description": @"Focussed four workouts divides body into core and axis. Furthermore, each workout focus no just push/pull workout dynamics.",
                                                @"Duration": @"Perform workout for 90 days.",
                                                @"Instructions": @"Perform Workouts schedule is as below\n\nDay 1 - BODY EDGE PUSH\nDay 2 - BODY AXIS PULL\nDay 3 - Rest Day\nDay 4 - BODY EDGE PULL\nDay 5 - BODY AXIS PUSH\nDay 6 - Rest Day\nDay 7 - Rest Day\n",
                                                @"Type": @"Bodybuilding",
                                                @"Level": @"Intermediate (1-3 Year)",
                                                @"FAQs": @"http://www.gyminutesapp.com/focussedfour.html",
                                                @"Disclaimer": @DISCLAIMER,
                                                @"Name": @"FOCUSSED FOUR"
                                                },
                   @IAP_ROUTINE_GVT_I: @{@"Image": [UIImage imageNamed:@"IconWRGvt1"],
                                         @"Goal": @"Volume is the mantra. Building lean muscle is the result.",
                                         @"Summary": @"German Volume Training is best for building lean mass in short interval of time by using extensive volume technique.",
                                         @"Description": @"German Volume Training workout is effective because of targets a group of motor units. Each workout expose every muscle to extensive volume thus forcing muscle to grow in order to handle the stress.",
                                         @"Duration": @"Perform workout for 60 days.",
                                         @"Instructions": @"For each workout pick a weight and perform all ten sets of exercise with same weight. Best starting weight is 60% of 1 Rep Max.\n\nDay 1 - GVT: Chest & Back\nDay 2 - Rest Day\nDay 3 - GVT: Arms & Shoulders\nDay 4 - Rest Day \nDay 5 - GVT: Legs & Abs\nDay 6 - Rest Day \nDay 7 - Rest Day\n",
                                         @"Type": @"Bodybuilding",
                                         @"Level": @"Intermediate (1-3 Year)",
                                         @"FAQs": @"http://www.gyminutesapp.com/germanvolumetraining.html",
                                         @"Disclaimer": @DISCLAIMER,
                                         @"Name": @"GERMAN VOLUME TRAINING I"
                                         },
                   @IAP_ROUTINE_GVT_II: @{@"Image": [UIImage imageNamed:@"IconWRGvt2"],
                                          @"Goal": @"GVT II is for advance users who have already completed one cycle of GVT I. Volume is still the key but workouts are more intense.",
                                          @"Summary": @"German Volume Training is best for building lean mass in short interval of time by using extensive volume technique.",
                                          @"Description": @"Push muscle through extreme stress with GVT II. For advancse users only.",
                                          @"Duration": @"Perform workout for 60 days.",
                                          @"Instructions": @"For each workout pick a weight and perform all ten sets of exercise with same weight. Best starting weight is 60% of 1 Rep Max.\n\nDay 1 - GVT: Chest & Back\nDay 2 - Rest Day\nDay 3 - GVT: Arms & Shoulders\nDay 4 - Rest Day \nDay 5 - GVT: Legs & Abs\nDay 6 - Rest Day \nDay 7 - Rest Day\n",
                                          @"Type": @"Bodybuilding",
                                          @"Level": @"Advance (3-5 Years)",
                                          @"FAQs": @"http://www.gyminutesapp.com/germanvolumetraining.html",
                                          @"Disclaimer": @DISCLAIMER,
                                          @"Name": @"GERMAN VOLUME TRAINING II"
                                          },
                   @IAP_ROUTINE_STRONGLIFT5X5: @{@"Image": [UIImage imageNamed:@"IconWRStrongLift5x5"],
                                                 @"Goal": @"Build muscle and gain strength",
                                                 @"Summary": @"Beginner powerlifting routine.",
                                                 @"Description": @"Great routine for building muscle and getting strong. Perfect blend of intensity and volume.",
                                                 @"Duration": @"Perform workout for 60 days.",
                                                 @"Instructions": @"Perform Workout A and Workout B alternatively. Perform maximum of 3 workouts per week. For example,\n\n=> Week 1\nDay 1 - Workout A\nDay 2 - Rest Day\nDay 3 - Workout B\nDay 4 - Rest Day\nDay 5 - Workout A\nDay 6 - Rest Day\nDay 7 - Rest Day\n\n=> Week 2 \nDay 1 - Workout B\nDay 2 - Rest Day\nDay 3 - Workout A\nDay 4 - Rest Day\nDay 5- Workout B\nDay 6 - Rest Day\nDay 7 - Rest Day\nand so on.\n",
                                                 @"Type": @"Strength/PowerLifting",
                                                 @"Level": @"Beginners(0-1 Year)",
                                                 @"FAQs": @"http://www.gyminutesapp.com/StrongLift5x5.html",
                                                 @"Disclaimer": @DISCLAIMER,
                                                 @"Name": @"StrongLift 5x5"
                                                 },
                   };
    
    if (_isLocalStored == false) {
        /*
        NSLog(@"product id %@", self.product.productIdentifier);
        NSError *error;
        PFQuery *query = [PFQuery queryWithClassName:@P_IAP_ROUTINES];
        [query whereKey:@"RoutineBundleID" equalTo:self.product.productIdentifier];
        [query setLimit:1];
        NSArray *bundleArray = [query findObjects:&error];
        
        if (error || [bundleArray count] == 0) {
            SCLAlertView *alert = [[SCLAlertView alloc] init];
            [alert showError:self title:@"Request Failed" subTitle:@"Unable to get information about this routine. Please try later. Sorry for inconvinence." closeButtonTitle:@"Ok" duration:0.0f];
            return;
        }
        
        PFObject *data = [bundleArray objectAtIndex:0];
      //  NSLog(@"pasrse %lu ==> %@", (long)[bundleArray count], data);
        
        routineName = data[@"RoutineName"];
        author = (data[@"AuthorName"] == nil) ? @"Self" : data[@"AuthorName"];

        NSString *routineDuration = [NSString stringWithFormat:@"Perform workout for %@ days.", data[@"RoutineDuration"]];
        
        tableContent = [[NSArray alloc] initWithObjects:data[@"RoutineGoal"], data[@"RoutineSummary"], data[@"RoutineDescription"], routineDuration ,data[@"RoutineInstructions"]  , data[@"RoutineType"], data[@"RoutineLevel"], data[@"RoutineFAQs"], data[@"RoutineDisclaimer"], nil];
        
        NSNumber *price = data[@"RoutinePrice"];
         */
        NSString *buyBtnLbl;
        if ([self.product.price floatValue] == 0) {
            buyBtnLbl = @"BUY (Free)";
        } else {
            buyBtnLbl = [NSString stringWithFormat:@"BUY ($%@)", self.product.price];//data[@"RoutinePrice"]
        }
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:buyBtnLbl style:UIBarButtonItemStylePlain target:self action:@selector(buyRoutine:)];
        
    } else {
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", _routineNameLocal];
        
        RoutineMetaInfo *routine = [RoutineMetaInfo MR_findFirstWithPredicate:predicate inContext:localContext];
        routineName = routine.routineName;
        author = routine.routineAuthor;
        NSString *routineDuration = [NSString stringWithFormat:@"Perform workout for %@ days.", routine.routineDuration];
        
        // we need to nil check becuase NSArray does not add any object that is nil.
        tableContent = [[NSArray alloc] initWithObjects:(routine.routingGoal == nil) ? @"": routine.routingGoal,
                        (routine.routineSummary == nil) ? @"": routine.routineSummary,
                        (routine.routineDescription == nil) ? @"": routine.routineDescription,
                        (routineDuration == nil) ? @"": routineDuration,
                        (routine.routineInstructions  == nil) ? @"": routine.routineInstructions,
                        (routine.routineType == nil) ? @"": routine.routineType,
                        (routine.routineLevel == nil) ? @"": routine.routineLevel,
                        (routine.routineFaqs == nil) ? @"": routine.routineFaqs,
                        (routine.routineDisclosure == nil) ? @DISCLAIMER: routine.routineDisclosure,
                        nil];
        
//        NSLog(@"routine %@ found with information %@\n %@", _routineNameLocal,routine, tableContent);
    
    }
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
    self.tableView.estimatedRowHeight = 68.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;

    self.navigationController.navigationBarHidden = NO;

}

- (void)buyRoutine:(id)sender {
    
    SKProduct *product = self.product;
    NSLog(@"in here Buying %@...", product.productIdentifier);
    [[uLiftIAPHelper sharedInstance] buyProduct:product];
    
}
- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productPurchased:) name:IAPHelperProductPurchasedNotification object:nil];
    NSLog(@"notificatoin added...");
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    NSLog(@"notificatio removed");
}


-(void) viewDidAppear:(BOOL)animated {
    self.title = @"Routine Details";
    [self.tableView reloadData];
    if ([[uLiftIAPHelper sharedInstance] productPurchased:_product.productIdentifier]) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Purchased" style:UIBarButtonItemStylePlain target:self action:@selector(buyRoutine:)];
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [tableContentHdr count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    UILabel *headerView = (UILabel *)[cell.contentView viewWithTag:100];
    UILabel *content = (UILabel *)[cell.contentView viewWithTag:101];
    
    
    if ([[tableContentHdr objectAtIndex:indexPath.row] isEqualToString:@"FAQs"]) {
        content.textColor = FlatSkyBlueDark;
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
    } else {
        content.textColor = [UIColor blackColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    headerView.text =  [tableContentHdr objectAtIndex:indexPath.row];
    
    if (_isLocalStored == false) {
        NSDictionary *item = [imageDict objectForKey:_routineBundleId];
        content.text = [item objectForKey:headerView.text];
    } else {
        content.text = [tableContent objectAtIndex:indexPath.row];
    }
    content.textAlignment = NSTextAlignmentJustified;
    return cell;
}


-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *url = [tableContent objectAtIndex:indexPath.row];
    if ([[tableContentHdr objectAtIndex:indexPath.row] isEqualToString:@"FAQs"]) {
        if (url != nil) {
            routineURL = url;
            [self performSegueWithIdentifier:@"routineFaqSegue" sender:self];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewAutomaticDimension;
}

-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return CGRectGetWidth(self.view.frame)*3/4 - 50;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger) section
{
    
    NSDictionary *item = [imageDict objectForKey:_routineBundleId];
    
    
    UIImageView *headerImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)*3/4 - 50)];
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(headerImage.frame) - 30, CGRectGetWidth(self.view.frame), 30)];
    titleLabel.backgroundColor = [UIColor clearColor
                                  ];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont fontWithName:@"HelveticaNeue-CondensedBlack" size:20];
    
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.backgroundColor = [FlatBlack colorWithAlphaComponent:0.5];

    if (_isLocalStored == false) {
        [headerImage setImage:[item objectForKey:@"Image"]];
        [headerImage setContentMode:UIViewContentModeScaleToFill];
        titleLabel.text = [[item objectForKey:@"Name"] uppercaseString];
    } else {
        [headerImage setImage:[UIImage imageNamed:@"IconAppIcon"]];
        [headerImage setContentMode:UIViewContentModeScaleAspectFit];
        titleLabel.text = [routineName uppercaseString];
    }

    
//    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(titleLabel.frame), CGRectGetWidth(self.view.frame), 14)];
//    subTitleLabel.backgroundColor = [UIColor clearColor];
//    subTitleLabel.textColor = [UIColor whiteColor];
//    subTitleLabel.font = [UIFont systemFontOfSize:12];
//    subTitleLabel.text = [NSString stringWithFormat:@"By: %@", author];
//    subTitleLabel.textAlignment = NSTextAlignmentCenter;
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), CGRectGetWidth(self.view.frame)*3/4 + 35)];
    twoLineTitleView.backgroundColor = [Utilities getAppColor];
    
    [twoLineTitleView addSubview:headerImage];
    [twoLineTitleView addSubview:titleLabel];
//    [twoLineTitleView addSubview:subTitleLabel];
    
    return twoLineTitleView;

}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"routineFaqSegue"]) {
        RoutineWebView *dest = segue.destinationViewController;
        dest.urlToLoad = routineURL;
        dest.routineName = routineName;
        self.title = @"";
    }
        
}


@end
