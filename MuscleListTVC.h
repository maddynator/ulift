//
//  MuscleListTVC.h
//  uLift
//
//  Created by Mayank Verma on 6/28/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"

@interface MuscleListTVC : UITableViewController <UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *workoutName;
@property (nonatomic, retain) NSNumber *exerciseGroup;
@property (nonatomic, retain) NSNumber *exerciseNumInGroup;
@property (nonatomic) BOOL isTempEx;
//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic, strong) IBOutlet UITableView *customRepsPerSet;

@end
