//
//  AdminIAPWorkoutCreator.m
//  gyminutes
//
//  Created by Mayank Verma on 8/5/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "AdminIAPWorkoutCreator.h"
#import "commons.h"

@interface AdminIAPWorkoutCreator () {
}

@end
@implementation AdminIAPWorkoutCreator

-(void) viewDidLoad {
    [super viewDidLoad];
}

-(void) viewDidAppear:(BOOL)animated {

    
}

-(void) checkUserAndCreateWorkout {
    if ([[PFUser currentUser].email isEqualToString:@"mayankworkoutcreator@gmail.com"]) {
        [self syncIAPWorkoutInfo: @"Triceps Trifecta"];
    }
}

-(void) syncIAPWorkoutInfo: (NSString *) workoutName {
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@", workoutName];
    NSArray *workouts = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    NSLog(@"will sync workout info now... %lu", (long) [workouts count]);
   
    
    for (UserWorkout *workoutObj in workouts) {
        
        NSLog(@"sncing routine info %@ and workout info... %@, %@, %@", workoutObj.routineName, workoutObj.workoutName, workoutObj.exerciseName, [PFUser currentUser].objectId);
        NSLog(@"workout %@, %@, %@\n", workoutObj.syncedState, workoutObj.exerciseName, workoutObj.exerciseNumber);
/*
        PFQuery *query = [PFQuery queryWithClassName:@P_WORKOUT_INFO_CLASS];
        [query whereKey:@"RoutineName" equalTo:workoutObj.routineName];
        [query whereKey:@"WorkoutName" equalTo:workoutObj.workoutName];
        [query whereKey:@"ExerciseName" equalTo:workoutObj.exerciseName];
        [query setLimit: 1];
        [[query findObjectsInBackground] continueWithBlock:^id(BFTask *task) {
            if (task.error) {
                NSLog(@"Error: %@", task.error);
                return task;
            }
            PFObject *rObj;
            NSLog(@"number of elements found %lu", (long)[task.result count]);
            if ([task.result count] == 0) {
                NSLog(@"Creating a new workout..");
                rObj = [PFObject objectWithClassName:@P_IAP_WORKOUTS];
                rObj[@"CreatedDate"] = workoutObj.createdDate;
                rObj[@"ExerciseName"] = workoutObj.exerciseName;
                rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                rObj[@"IsActive"] = @YES;
                rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                rObj[@"Muscle"] = workoutObj.muscle;
                rObj[@"Reps"]  = workoutObj.reps;
                rObj[@"RestTimer"]  = workoutObj.restTimer;
                rObj[@"RoutineName"] = workoutObj.routineName;
                rObj[@"Sets"] = workoutObj.sets;
                rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                rObj[@"WorkoutName"] = workoutObj.workoutName;
                rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                [rObj saveEventually];
            } else {
                NSLog(@"updating old workout");
                rObj = [task.result objectAtIndex:0];
                rObj[@"CreatedDate"] = workoutObj.createdDate;
                rObj[@"ExerciseName"] = workoutObj.exerciseName;
                rObj[@"ExerciseNumber"] = workoutObj.exerciseNumber;
                rObj[@"HasDropSet"] = ([workoutObj.hasDropSet boolValue] == true) ? @YES: @NO ;
                rObj[@"HasPyramidSet"] = workoutObj.hasPyramidSet;
                rObj[@"IsActive"] = @YES;
                rObj[@"MajorMuscle"] = workoutObj.majorMuscle;
                rObj[@"Muscle"] = workoutObj.muscle;
                rObj[@"Reps"]  = workoutObj.reps;
                rObj[@"RestTimer"]  = workoutObj.restTimer;
                rObj[@"RoutineName"] = workoutObj.routineName;
                rObj[@"Sets"] = workoutObj.sets;
                rObj[@"SuperSetWith"] = workoutObj.superSetWith;
                rObj[@"WorkoutName"] = workoutObj.workoutName;
                rObj[@"ExerciseGroup"] = workoutObj.exerciseGroup;
                rObj[@"ExerciseNumInGroup"] = workoutObj.exerciseNumInGroup;
                [rObj saveEventually];
                
            }
            NSLog(@"Workout Synced.. %@", workoutObj.workoutName);
            return task.result;
        }];
     */
    }
 
}


@end
