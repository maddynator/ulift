//
//  RoutineAnalysusVC.m
//  gyminutes
//
//  Created by Mayank Verma on 5/15/16.
//  Copyright © 2016 Mayank Verma. All rights reserved.
//

#import "RoutineAnalysusVC.h"
#import "commons.h"
#import "Utilities.h"
#import "DBInterface.h"
@interface RoutineAnalysusVC() {
    NSMutableArray *dataArr, *heightArr, *xAxisLabelArr;
    float lblHeight ;
    int oldTimerIndex;
    NSDate *selectedDate;

}

@end
@implementation RoutineAnalysusVC

@synthesize collectionView, timeSegment;

-(void) viewDidLoad {
    [super viewDidLoad];
    lblHeight = 20;
    
    xAxisLabelArr = [[NSMutableArray alloc] init];
    timeSegment = [[UISegmentedControl alloc] initWithItems:@[@"1W", @"2W", @"1M", @"3M", @"6M", @"1Y"]];
    timeSegment.frame = CGRectMake(10, 5, CGRectGetWidth(self.view.frame) - 20, 30);
    [self.view addSubview:timeSegment];
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    collectionView=[[UICollectionView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(timeSegment.frame) + 5, CGRectGetWidth(self.view.frame), CGRectGetHeight(self.view.frame) - CGRectGetHeight(self.navigationController.tabBarController.tabBar.frame) - 95) collectionViewLayout:layout];
    [collectionView setDataSource:self];
    [collectionView setDelegate:self];
    
    [collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"cellIdentifier"];
    [collectionView setBackgroundColor:[UIColor whiteColor]];


    timeSegment.selectedSegmentIndex = 1;
    oldTimerIndex = 1;
    selectedDate = [[NSDate date] dateBySubtractingWeeks:2];

    [timeSegment addTarget:self action:@selector(timeSegmentClicked:) forControlEvents:UIControlEventValueChanged];

    dataArr = [[NSMutableArray alloc] init];
    heightArr = [[NSMutableArray alloc] init];
    [self.view addSubview:collectionView];

    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   @"RoutineName", _routineName,
                                   nil];
    [Flurry logEvent:@"RoutineAnalysis" withParameters:articleParams];

}
-(void) viewDidAppear:(BOOL)animated {
    [self timeSegmentClicked:timeSegment];
    [self.collectionView reloadData];

}
-(void)timeSegmentClicked:(UISegmentedControl *)segmentedControl {
    bool showAnalysis = false;
    
    if (segmentedControl.selectedSegmentIndex > 1) {
        if ([Utilities showAnalyticsPackage]) {
            [self showPurchasePopUp:@"This is a paid feature."];
            timeSegment.selectedSegmentIndex = oldTimerIndex;
        } else
            showAnalysis = true;
    } else
        showAnalysis = true;
    
    if (showAnalysis == true) {
        oldTimerIndex = (int) segmentedControl.selectedSegmentIndex;
        
        NSString *dateStringForHeaders = @"";
        NSLog(@"segment selected is %ld", (long)segmentedControl.selectedSegmentIndex);
        NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd"];
        NSDate *today = [NSDate date];
        
        switch (segmentedControl.selectedSegmentIndex) {
            case 0:
                selectedDate = [today dateBySubtractingWeeks:1];
                dateStringForHeaders = @"last week.";
                break;
            case 1:
                selectedDate = [today dateBySubtractingWeeks:2];
                dateStringForHeaders = @"last 2 weeks.";
                break;
            case 2:
                selectedDate = [today dateBySubtractingMonths:1];
                dateStringForHeaders = @"last 1 month.";
                break;
            case 3:
                selectedDate = [today dateBySubtractingMonths:3];
                dateStringForHeaders = @"last 3 months.";
                break;
            case 4:
                selectedDate = [today dateBySubtractingMonths:6];
                dateStringForHeaders = @"last 6 months.";
                break;
            case 5:
                selectedDate = [today dateBySubtractingYears:1];
                dateStringForHeaders = @"last 1 year.";
                break;
            default:
                break;
        }
        [self getAllData];
    }
}

-(void) showPurchasePopUp: (NSString *) feature {
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY ANALYTICS PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_ANALYTICS_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium or Analytics Package to modify workouts routines.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}

-(void) getAllData {
    [heightArr removeAllObjects];
    [dataArr removeAllObjects];
    NSMutableDictionary *temp;
    NSNumber *height = nil;
    NSArray *orderArray = nil;
    float heightChart = 200;

    NSDateFormatter *formatter  = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];

    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    // NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    NSMutableArray *workoutSetData = [[NSMutableArray alloc] init];
    NSArray *tempAllSets = nil;
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"routineName == %@",_routineName];
    tempAllSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:YES withPredicate:predicate inContext:localContext];
    
    for (ExerciseSet *ex in tempAllSets) {
        NSDate *date = [formatter dateFromString:ex.date];
        if ([date isLaterThanOrEqualTo:selectedDate]) {
            [workoutSetData addObject:ex];
        }
    }
    NSLog(@"TOT COUNT %lu", (long)[workoutSetData count]);
    temp = [DBInterface getRoutineDate:(NSArray *)workoutSetData];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    NSArray *tempSetsRoutines = nil;
    NSMutableArray *userSetsRoutine = [[NSMutableArray alloc] init];
    tempSetsRoutines = [ExerciseSet MR_findAllSortedBy:@"date,workoutName,timeStamp" ascending:NO withPredicate:predicate inContext:localContext];
    for (ExerciseSet *ex in tempSetsRoutines) {
        NSDate *date = [formatter dateFromString:ex.date];
        if ([date isLaterThanOrEqualTo:selectedDate]) {
            [userSetsRoutine addObject:ex];
        }
    }

    temp =[DBInterface getRoutineFavorites: userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];


//    temp =[DBInterface getRoutineWeekday: userSetsRoutine];
//    orderArray = temp[AT_ORDER];
//    if ([temp[DATA_TYPE] isEqualToString:BAR_CHART]) {
//        height = [NSNumber numberWithInt:heightChart];
//    } else {
//        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
//    }
//    [heightArr addObject:height];
//    [dataArr addObject:temp];

    temp =[DBInterface getRoutineTime: userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:BAR_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];

    
    temp =[DBInterface getRoutineOverallStats: userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
 
    NSArray *workout = nil;
    workout = [UserWorkout MR_findAllWithPredicate:predicate inContext:localContext];
    
    temp = [DBInterface getRoutineAvgWorkoutEst:workout];

    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];
    
    temp = [DBInterface getRoutineAvgWorkoutAct:userSetsRoutine];
    orderArray = temp[AT_ORDER];
    if ([temp[DATA_TYPE] isEqualToString:PIE_CHART]) {
        height = [NSNumber numberWithInt:heightChart];
    } else {
        height = [NSNumber numberWithInt:lblHeight * [orderArray count]];
    }
    [heightArr addObject:height];
    [dataArr addObject:temp];

    //all done.. lets reload..
    [self.collectionView reloadData];
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section{
    return UIEdgeInsetsMake(5, 0, 0, 0);
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [dataArr count];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)myCollectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[myCollectionView dequeueReusableCellWithReuseIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    
   // cell.backgroundColor=[UIColor colorWithGradientStyle:UIGradientStyleTopToBottom withFrame:cell.frame andColors:@[FlatGreen, FlatGreenDark]];
    cell.layer.cornerRadius = 5;
    cell.backgroundColor = [Utilities getAppColor];
    cell.layer.masksToBounds = NO;
    cell.layer.shadowOffset = CGSizeMake(-8, 8);
    cell.layer.shadowRadius = 5;
    cell.layer.shadowOpacity = 0.5;
    
    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[PieChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[BarChartView class]]) {
            [lbl removeFromSuperview];
        } else if ([lbl isKindOfClass:[LineChartView class]]) {
            [lbl removeFromSuperview];
        }
    }

    NSMutableDictionary *item = [dataArr objectAtIndex:indexPath.row];
    
    UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(5, 0, CGRectGetWidth(cell.frame) - 10, 30)];
    title.text = item[AT_NAME_KEY];
    title.textAlignment = NSTextAlignmentCenter;    
    title.textColor = [UIColor whiteColor];
    [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];

    int iterate = 0;
    if ([item[DATA_TYPE] isEqualToString:PIE_CHART]) {
        _pieChartView = [[PieChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 170)];
        [self setupPieChart];
        [self setPieChartDataValues:item];
        [cell.contentView addSubview:_pieChartView];
        
    } else if ([item[DATA_TYPE] isEqualToString:BAR_CHART]) {
        _barChartView = [[BarChartView alloc] initWithFrame: CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 200)];
        [self setupBarChart];
        [self setBarChartDataValues:item];
        [cell.contentView addSubview:_barChartView];
        
    } else {
        NSArray *orderDisp = item[AT_ORDER];
        for (NSString *key in orderDisp) {
            
            UILabel *left = [[UILabel alloc] initWithFrame:CGRectMake(5, CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            UILabel *right = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(left.frame), CGRectGetMaxY(title.frame) + iterate * lblHeight, CGRectGetWidth(cell.frame)/2 - 5, lblHeight)];
            left.text = key;
            right.text = [NSString stringWithFormat:@"%@", item[key]];
            left.textAlignment = NSTextAlignmentLeft;
            right.textAlignment = NSTextAlignmentRight;
            
            [left setFont:[UIFont fontWithName:@HAL_THIN_FONT size:14]];
            [right setFont:[UIFont fontWithName:@HAL_REG_FONT size:14]];
            
            left.textColor = [UIColor whiteColor];
            right.textColor = [UIColor whiteColor];
            
            [cell.contentView addSubview:left];
            [cell.contentView addSubview:right];
            iterate++;
        }
    }
    [cell.contentView addSubview:title];
    //[cell.contentView addSubview:subTitle];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(CGRectGetWidth(self.view.frame) - 20, [[heightArr objectAtIndex:indexPath.row] floatValue] + 35);
}

-(void) setupLineChartView {
    _lineChartView.delegate = self;
    
    _lineChartView.descriptionText = @"";
    _lineChartView.noDataText = @"";
    
    //    _chartView.highlighted = YES;
    _lineChartView.dragEnabled = YES;
    [_lineChartView setScaleEnabled:YES];
    _lineChartView.drawGridBackgroundEnabled = NO;
    _lineChartView.pinchZoomEnabled = YES;
    
    _lineChartView.backgroundColor = [UIColor whiteColor];//[UIColor colorWithWhite:204/255.f alpha:1.f];
    
    _lineChartView.legend.form = ChartLegendFormLine;
    _lineChartView.legend.font = [UIFont fontWithName:@HAL_REG_FONT size:11.f];
    _lineChartView.legend.textColor = [UIColor whiteColor];
    _lineChartView.legend.position = ChartLegendPositionBelowChartLeft;
    
    ChartXAxis *xAxis = _lineChartView.xAxis;
    xAxis.labelFont = [UIFont systemFontOfSize:12.f];
    xAxis.labelTextColor = UIColor.blackColor;
    xAxis.drawGridLinesEnabled = YES;
    xAxis.drawAxisLineEnabled = NO;
    
    
    ChartYAxis *leftAxis = _lineChartView.leftAxis;
    leftAxis.labelTextColor = FlatBlackDark;//[UIColor colorWithRed:51/255.f green:181/255.f blue:229/255.f alpha:1.f];
    //    leftAxis.customAxisMax = 200.0;
    leftAxis.drawGridLinesEnabled = NO;
    
    ChartYAxis *rightAxis = _lineChartView.rightAxis;
    rightAxis.labelTextColor = UIColor.redColor;
    rightAxis.drawZeroLineEnabled = NO;
    rightAxis.drawGridLinesEnabled = NO;
    rightAxis.enabled = NO;
    
    [_lineChartView animateWithXAxisDuration:2.5];
    
}

-(void) setupBarChart {
    _barChartView.delegate = self;
    
    _barChartView.descriptionText = @"";
    _barChartView.noDataText = @"";
 
    _barChartView.drawBarShadowEnabled = NO;
    _barChartView.drawValueAboveBarEnabled = YES;
    _barChartView.backgroundColor = [UIColor clearColor];
    _barChartView.gridBackgroundColor = [UIColor clearColor];
    _barChartView.drawGridBackgroundEnabled = NO;
    _barChartView.maxVisibleCount = 60;
    
    ChartXAxis *xAxis = _barChartView.xAxis;
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelFont = [UIFont systemFontOfSize:10.f];
    xAxis.drawGridLinesEnabled = NO;
    xAxis.labelTextColor = [UIColor whiteColor];
    xAxis.valueFormatter = self;
    xAxis.granularity = 1;
    
    ChartYAxis *leftAxis = _barChartView.leftAxis;
    leftAxis.enabled = YES;
    leftAxis.labelFont = [UIFont systemFontOfSize:10.f];
    leftAxis.labelCount = 8;
    leftAxis.labelTextColor = [UIColor whiteColor];
    leftAxis.labelPosition = YAxisLabelPositionOutsideChart;
    leftAxis.spaceTop = 0.15;
    leftAxis.gridColor = [UIColor clearColor];
    leftAxis.drawZeroLineEnabled = 0;
    leftAxis.spaceBottom = 0;
    
    ChartYAxis *rightAxis = _barChartView.rightAxis;
    rightAxis.enabled = NO;
//    rightAxis.drawGridLinesEnabled = NO;
//    rightAxis.labelFont = [UIFont systemFontOfSize:10.f];
//    rightAxis.labelCount = 8;
//    rightAxis.valueFormatter = leftAxis.valueFormatter;
//    rightAxis.spaceTop = 0.15;
//    rightAxis.spaceBottom = 0;
    
    _barChartView.legend.position = ChartLegendPositionBelowChartLeft;
    _barChartView.legend.form = ChartLegendFormSquare;
    _barChartView.legend.formSize = 9.0;
    _barChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _barChartView.legend.xEntrySpace = 4.0;
    
}

-(void) setBarChartDataValues: (NSMutableDictionary *) chartData {

    
    NSMutableArray *yVals = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    
    NSArray *items = chartData[@"xvals"];
    for (int i = 0; i < [items count]; i++)
    {
        [xVals addObject:[items objectAtIndex:i]];
    }
    
    NSArray *yvalItems = chartData[@"yvals"];
    for (int i = 0; i < [yvalItems count]; i++)
    {
        //int value = [[yvalItems objectAtIndex:i] intValue] ;
        [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[[yvalItems objectAtIndex:i]] label:[items objectAtIndex:i]]];
    }
    
    if ([xVals count] == 0 || [yVals count] == 0) {
        return;
    }
    xAxisLabelArr = xVals;
    
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@"Workout Performed"];
    set1.colors = @[FlatWhite];
    [set1 setDrawValuesEnabled:NO];

    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    
    BarChartData *data = [[BarChartData alloc] initWithDataSet:set1];
    [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:10.f]];
    
    _barChartView.data = data;

}

-(void) setupPieChart {
    _pieChartView.usePercentValuesEnabled = YES;
    _pieChartView.holeColor = [UIColor clearColor];
    _pieChartView.noDataText = @"No data.";
    _pieChartView.holeRadiusPercent = 0.40;
    _pieChartView.transparentCircleRadiusPercent = 0.21;
    _pieChartView.descriptionText = @"";
    _pieChartView.drawCenterTextEnabled = YES;
    _pieChartView.drawHoleEnabled = YES;
    _pieChartView.rotationAngle = 0.0;
    _pieChartView.rotationEnabled = YES;
    _pieChartView.legend.enabled = YES;
    _pieChartView.drawSliceTextEnabled = NO;
    
//        ChartLegend *l = _pieChartView.legend;
//        l.position = ChartLegendPositionPiechartCenter;
//        l.xEntrySpace = 7.0;
//        l.yEntrySpace = 0.0;
//        l.yOffset = 0.0;

    
    _pieChartView.legend.form = ChartLegendFormSquare;
    _pieChartView.legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    _pieChartView.legend.textColor = UIColor.whiteColor;
    _pieChartView.legend.position = ChartLegendPositionLeftOfChart;
    
    [_pieChartView animateWithXAxisDuration:1.5 yAxisDuration:1.5 easingOption:ChartEasingOptionEaseOutBack];
    
}


-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *colors = [[NSMutableArray alloc] init];
    
    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    bool colorFlag = false, isMajor = false, isMinor = false;
    
    if (chartData[AT_NAME_MUSCLE] != nil) {
        colorFlag = true;
        isMinor = true;
    }
    
    if (chartData[AT_NAME_MAJOR_MUSCLE] != nil) {
        colorFlag = true;
        isMajor = true;
    }
    
    for (id key in chartData)
    {
        if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE] || [key isEqualToString:AT_NAME_MUSCLE] || [key isEqualToString:AT_NAME_MAJOR_MUSCLE]) {
            continue;
        }
        if (colorFlag == true) {
            if (isMinor == true) {
                [colors addObject:[Utilities getExerciseColor:key]];
            }
            else if (isMajor == true) {
                [colors addObject:[Utilities getMajorMuscleColor:key]];
            }
        }
        [xVals addObject:key];
        [yVals1 addObject:[[PieChartDataEntry alloc] initWithValue:[chartData[key] floatValue] label:key]];
    }
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:yVals1 label:@""];
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    
    if (colorFlag == false) {
        [colors addObject:FlatRedDark];
        [colors addObject:FlatYellowDark];
        [colors addObject:FlatBlueDark];
        [colors addObject:FlatMagentaDark];
        [colors addObject:FlatGreenDark];
        [colors addObject:FlatPinkDark];
        [colors addObject:FlatOrangeDark];
        [colors addObject:FlatBlackDark];
        [colors addObject:FlatTealDark];
    }
    
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _pieChartView.data = data;
    [_pieChartView highlightValues:nil];
    
}

/*
-(void) setPieChartDataValues: (NSMutableDictionary *) chartData {
    
    NSMutableArray *yVals1 = [[NSMutableArray alloc] init];
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    NSMutableArray *colors = [[NSMutableArray alloc] init];

    
    // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
    int i = 0;
    bool colorFlag = false;
    for (id key in chartData)
    {
        if ([key isEqualToString:AT_NAME_KEY] || [key isEqualToString:DATA_TYPE]) {
            continue;
        }
        
        if ([key isEqualToString:AT_NAME_MUSCLE]) {
            colorFlag = true;
            continue;
        }
        [xVals addObject:key];
        [yVals1 addObject:[[BarChartDataEntry alloc] initWithValue:[chartData[key] floatValue] xIndex:i]];
        if (colorFlag == true) {
            [colors addObject:[Utilities getMajorMuscleColor:key]];
        }

    }
    
    
    PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithYVals:yVals1 label:@""];
    dataSet.sliceSpace = 0.0;
    
    // add a lot of colors
    if (colorFlag == false) {
        [colors addObject:FlatYellowDark];
        [colors addObject:FlatRedDark];
        [colors addObject:FlatGreenDark];
        [colors addObject:FlatBlueDark];
        [colors addObject:FlatPinkDark];
        [colors addObject:FlatOrangeDark];
    }
    
    dataSet.colors = colors;
    
    PieChartData *data = [[PieChartData alloc] initWithXVals:xVals dataSet:dataSet];
    
    NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
    pFormatter.numberStyle = NSNumberFormatterPercentStyle;
    pFormatter.maximumFractionDigits = 1;
    pFormatter.multiplier = @1.f;
    pFormatter.percentSymbol = @" %";
    [data setValueFormatter:pFormatter];
    [data setValueFont:[UIFont fontWithName:@HAL_BOLD_FONT size:11.f]];
    [data setValueTextColor:[UIColor whiteColor]];
    
    _pieChartView.data = data;
    [_pieChartView highlightValues:nil];
    
}
*/
#pragma mark - pieChartViewDelegate

- (void)chartValueSelected:(ChartViewBase * __nonnull)chartView entry:(ChartDataEntry * __nonnull)entry dataSetIndex:(NSInteger)dataSetIndex highlight:(ChartHighlight * __nonnull)highlight
{
    if ([chartView isKindOfClass:[LineChartView class]]) {
        NSLog(@"Line chart");
    } else if ([chartView isKindOfClass:[PieChartView class]]) {
        NSLog(@"Pie chart");
    }
    
}

- (void)chartValueNothingSelected:(ChartViewBase * __nonnull)chartView
{
    NSLog(@"chartValueNothingSelected");
}

- (NSString *)stringForValue:(double)value
                        axis:(ChartAxisBase *)axis
{
    return xAxisLabelArr[(int)value % xAxisLabelArr.count];
}
@end
