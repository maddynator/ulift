//
//  HomeTVC.m
//  uLift
//
//  Created by Mayank Verma on 11/7/15.
//  Copyright © 2015 Mayank Verma. All rights reserved.
//

#import "HomeTVC.h"

#import "commons.h"
#import "SelectRoutine.h"
#import "AnalyzeRoutine.h"
#import "RoutineList.h"
#import "RoutineAnalysusVC.h"
#import "TWSReleaseNotesView.h"
#import "UpdateHandler.h"
#import "TodayWorkoutItems.h"
#import "WorkoutUpdaterToCloud.h"
@interface HomeTVC ()  {
    NSArray *colorArray, *titleArray, *imageArray, *subTitleArray, *segueArray;
    NSMutableSet *processedWorkouts;
    NSMutableArray *processedDates;
    NSArray *totalWorkouts;
    NSTimer *syncTimer;
    BOOL warningDisplayed;
    NSString *deviceModel;
    BOOL isPremium, isData, isWorkout, isRoutine, isEarlyBird, isAnalytics;
    NSTimer *timer;
    NSArray *headersTitle;
    NSMutableDictionary *carouselData;
    SMPageControl *horizontalPageControl;
    NSArray *verionsNumbers, *versionData;
    int randomCard;
    
}
@property (nonatomic, strong) CNPPopupController *popupController;


@end

@implementation HomeTVC
@synthesize carousel;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Home";
    randomCard = 0;
    /*
     #define AT_FAV_MUSCLE           @"FAVORITE MUSCLE"
     #define AT_FAV_EXERCISE         @"FAVORITE EXERCISE"
     #define AT_FAV_WORKOUT          @"FAVORITE WORKOUT"
     #define AT_FAV_ROUTINE          @"FAVORITE ROUTINE"
     #define AT_LAST_WORKOUT         @"LAST WORKOUT"
     #define AT_TODAY_WORKOUT        @"TODAY WORKOUT"

     */

    headersTitle = @[@"INVITE FRIENDS", AT_RECORD_STATS, AT_LAST_WORKOUT, AT_FAV_EXERCISE, AT_FAV_MUSCLE, AT_FAV_WORKOUT];
    carouselData = [[NSMutableDictionary alloc] init];
    
    for (NSString *title in headersTitle) {
        if ([title isEqualToString:@"INVITE FRIENDS"]) {
            NSMutableDictionary *emptyData = [[NSMutableDictionary alloc] init];
            [emptyData setObject:@"FACEBOOK" forKey:@"facebook"];
            [emptyData setObject:@"EMAIL" forKey:@"email"];
            [emptyData setObject:@"MESSAGE" forKey:@"message"];
            [emptyData setObject:@"LIFT, LOVE, SHARE" forKey:@"Name"];
            [carouselData setObject:emptyData forKey:title];
        } else {
            NSMutableDictionary *emptyData = [[NSMutableDictionary alloc] init];
            [emptyData setObject:@"" forKey:@""];
            NSLog(@"setting empty for %@", title);
            [carouselData setObject:emptyData forKey:title];
        }
    }
  

    warningDisplayed = false;
    
    colorArray = [[NSArray alloc] initWithObjects:FlatOrange, FlatSkyBlueDark, FlatWatermelon,  FlatMintDark, nil];//FlatMagenta,
    titleArray = [[NSArray alloc] initWithObjects:[Utilities getCurrentDate], @"START", @"CREATE",  @"ANALYZE", nil];//@"RECORD",
    subTitleArray = [[NSArray alloc] initWithObjects:@"SUMMARY", @"WORKOUT", @"ROUTINE",  @"PROGRESS", nil];//@"STATISTICS",
    segueArray = [[NSArray alloc] initWithObjects:@"", @"selectRoutineSegue", @"routineSegue",  @"AnalyzeSegue", nil];//@"recordStatsSegue",
    
    if  ([[Utilities getAllExerciseForDate:[Utilities getCurrentDate]] count] != 0) {
        self.tabBarController.selectedIndex = 1;
    }
    
    self.tableView.scrollEnabled = false;
    
    processedWorkouts = [NSMutableSet set];
    processedDates = [[NSMutableArray alloc] init];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    UITabBarController *tabBarController = [self tabBarController];
    UITabBar *tabBar = tabBarController.tabBar;
    for (UITabBarItem  *tab in tabBar.items) {
        tab.image = [tab.image imageWithRenderingMode: UIImageRenderingModeAlwaysOriginal];
    }
    
    UIBarButtonItem *infoBtn = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"IconInfo"] style:UIBarButtonItemStyleDone target:self action:@selector(showLocalReleaseNotesView)];
    
    self.navigationItem.rightBarButtonItems = @[infoBtn];
}


-(void) processCurrentData {
    totalWorkouts = nil;
    [processedDates removeAllObjects];
    [processedWorkouts removeAllObjects];
    
    totalWorkouts = [ExerciseSet MR_findAllSortedBy:@"date" ascending:YES];
    NSMutableArray * unique  = [NSMutableArray array];
    for (ExerciseSet *data in totalWorkouts) {
        NSString *string = data.date;
        if ([string isEqualToString:[Utilities getCurrentDate]]) {
            continue;
        }
        if ([processedWorkouts containsObject:string] == NO) {
            [unique addObject:string];
            [processedWorkouts addObject:string];
            [processedDates addObject:string];
        }
    }

    [self getAllUserSets];

    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void) viewDidAppear:(BOOL)animated {

    //[self performSegueWithIdentifier:@"searchSegue" sender:self];
    //[self printAllRecords];
    randomCard = (int) random()%[headersTitle count];
    
    self.navigationController.navigationBar.barTintColor = [Utilities getAppColor];
    self.navigationController.tabBarController.tabBar.backgroundImage =[UIImage imageWithColor:[Utilities getAppColor] size:CGSizeMake(320, 49)];
    
    NSLog(@"new value os sync is %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ParseSyncDone"]);
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ParseSyncDone"] == false) {
       syncTimer = [NSTimer scheduledTimerWithTimeInterval:2 target:self selector:@selector(reload) userInfo:nil repeats:YES];
    }

    [self processCurrentData];
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"EnableHintKeyHome"])
        [self showCoachMarks];

    deviceModel = [Utilities getIphoneName];
//    NSLog(@"device model is %@", deviceModel);
    
    //[self getAppStoreForProduct:1082175631];
    
    isPremium = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPremium"];
    isData = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedDataPackage"];
    isWorkout = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedWorkoutPackage"];
    isRoutine = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedPowerRoutinePackage"];
    isAnalytics = [[NSUserDefaults standardUserDefaults] boolForKey:@"PurchasedAnalyticsPackage"];
    isEarlyBird = false;
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:FREE_VERSION_DATE_LIMIT_DATE];
    [comps setMonth:FREE_VERSION_DATE_LIMIT_MONTH];
    [comps setYear:FREE_VERSION_DATE_LIMIT_YEAR];
    
    NSDate *releaseDate = [[NSCalendar currentCalendar] dateFromComponents:comps];
    NSDate *accountDate = [PFUser currentUser].createdAt;
    if ([accountDate isEarlierThanOrEqualTo:releaseDate]) {
        isEarlyBird = true;
    }

    //NSLog(@"Status %d %d %d %d %d", isPremium, isData, isWorkout, isRoutine, isEarlyBird);
    if (!isEarlyBird && isPremium == false && isData == false && isWorkout == false && isRoutine == false && isAnalytics == false) {
        if (![[NSUserDefaults standardUserDefaults] boolForKey:@"FreeAppInstall"]) {
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"FreeAppInstall"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self showTrial:CNPPopupStyleCentered];
        }
    }
    
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ExerciseListDBSync"] == false) {
        NSLog(@"exercise DBSYNC NOT DONE..");
        [self getUpdatedEntries];
    } else {
        NSLog(@"EXercise DB is already up-to-date");
    }
    
    [UpdateHandler v2_update_UserProfile_membershipType];
    NSLog(@"user id %@", [[PFUser currentUser] objectId]);
    
    [self askUserToMigrateToAmrap];
}

- (void)showLocalReleaseNotesView
{
    // Create the release notes view

    NSString *currentAppVersion = [[NSBundle mainBundle] infoDictionary][@"CFBundleVersion"];
    NSLog(@"version is %@", currentAppVersion);
    TWSReleaseNotesView *releaseNotesView = [TWSReleaseNotesView viewWithReleaseNotesTitle:[NSString stringWithFormat:@"What's new\n%@:", currentAppVersion] text:@"Version 2.4 \n • [Feature Added]: Custom Reps and Rest Per Set. \n\nVersion 2.3 \n • [Feature Added]: Warmup calculator \n • Minor Bug fixes. \n\n Version 2.2 \n • Minor bug fixes. \n\n Version 2.1 \n• Moved to a new secure server. \n• Minor bug fixes.\n\n Version 2.0\n• Summary Cards on Home Screen\n• Ability to invite friends.\n• Analytical Engine V1: Extract useful data sets (100+) from your workouts.\n• Record Daily Stats including weight, bf and bmi.\n• New Milestrone and PR visualization tool. \n• Smart workout creation tool with analytics.\n• Redesigned user interface.\n•Change App Theme (\"More > Change Theme\")\n• Bug Fixes\n• Additional performance and stability improvements" closeButtonTitle:@"Close"]; //\n• \n• \n• \n
    
    // Show the release notes view
    [releaseNotesView showInView:self.view];
}

-(void) reload {
    NSLog(@"reloading.... every 2 sec....");
    if ([[NSUserDefaults standardUserDefaults] boolForKey:@"ParseSyncDone"] == true) {
        [syncTimer invalidate];
        syncTimer = nil;
        [self processCurrentData];
        NSLog(@"we are all done.. not sync is complete... %d", [[NSUserDefaults standardUserDefaults] boolForKey:@"ParseSyncDone"]);
    }
}

-(void) showCoachMarks {
    float width = self.view.frame.size.width;
    float topSummaryHeigth = (self.tableView.frame.size.height/2);
    float bottomHeight =  (self.tableView.frame.size.height/2)/3;
    
    CGRect coachmark1 = CGRectMake(0, 0, width, topSummaryHeigth);
    CGRect coachmark2 = CGRectMake(0, CGRectGetMaxY(coachmark1), width, bottomHeight);
    CGRect coachmark3 = CGRectMake(0, CGRectGetMaxY(coachmark2), width, bottomHeight);
    CGRect coachmark4 = CGRectMake(0, CGRectGetMaxY(coachmark3), width, bottomHeight);
    
    // Setup coach marks
    NSArray *coachMarks = @[
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark1],
                                @"caption": @"Summary of your past workouts is displayed here.",
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark2],
                                @"caption": @"To start a workout click here",
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark3],
                                @"caption": @"To create a new workout routine, click here.",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                },
                            @{
                                @"rect": [NSValue valueWithCGRect:coachmark4],
                                @"caption": @"To analyze your workout, click here.",
                                @"position":[NSNumber numberWithInteger:LABEL_POSITION_TOP],
                                @"showArrow":[NSNumber numberWithBool:YES]
                                }
                            
                            ];
    
    MPCoachMarks *coachMarksView = [[MPCoachMarks alloc] initWithFrame:self.view.bounds coachMarks:coachMarks];
    [self.tableView addSubview:coachMarksView];
    [[NSUserDefaults standardUserDefaults] setBool:NO   forKey:@"EnableHintKeyHome"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [coachMarksView start];
    //self.tabBarController.selectedIndex = 3;
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSLog(@"segue count is %lu", (long) [segueArray count]);
    return [segueArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    
    for (UILabel *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[UILabel class]]) {
            [lbl removeFromSuperview];
        }
    }

    for (UIView *lbl in [cell.contentView subviews]) {
        if ([lbl isKindOfClass:[iCarousel class]]) {
            [lbl removeFromSuperview];
        }
    }

    if (indexPath.row == 0) {
        
        float width = CGRectGetWidth(cell.frame);

        UILabel *paidLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 20)];
        if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
            carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(paidLbl.frame), width, 160)];
        } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
            carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(paidLbl.frame), width, 200)];
        } else {
            carousel = [[iCarousel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(paidLbl.frame) + 20, width, 200)];
        }
        carousel.delegate = self;
        carousel.dataSource = self;
        carousel.pagingEnabled = YES;
        carousel.type = iCarouselTypeLinear;
        carousel.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor whiteColor];
        carousel.currentItemIndex  = randomCard;
        
        int days = [Utilities daysSinceAccount];
        [paidLbl setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:12]];
        paidLbl.textAlignment = NSTextAlignmentCenter;
        paidLbl.textColor = [UIColor whiteColor];
        paidLbl.userInteractionEnabled = YES;
        UITapGestureRecognizer *tapGesture =
        [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showStore)];
        [paidLbl addGestureRecognizer:tapGesture];


        if (isEarlyBird == true) {
            paidLbl.text = @"PREMIUM";
            paidLbl.backgroundColor = FlatMintDark;
        } else {
            // 0 0 0 0
            if (isPremium == false && isData == false && isWorkout == false && isRoutine == false && isAnalytics == false) {
                if (days == FREE_VERSION_TRIAL_PERIOD - 1) {
                    paidLbl.text = [NSString stringWithFormat:@"PREMIUM ACCESS EXPIRES IN %d DAY", FREE_VERSION_TRIAL_PERIOD - days];
                }
                else if (days < FREE_VERSION_TRIAL_PERIOD) {
                    paidLbl.text = [NSString stringWithFormat:@"PREMIUM ACCESS EXPIRES IN %d DAYS", FREE_VERSION_TRIAL_PERIOD - days];
                }
                else if (days >= FREE_VERSION_TRIAL_PERIOD) {
                    paidLbl.text = @"PREMIUM ACCESS EXPIRED. TAP TO UPGRADE.";
                }
                paidLbl.backgroundColor = FlatRed;
            }
            
            UIColor *singleColor = FlatYellow;
            
            // 1 0 0 0
            if (isData && !isRoutine && !isWorkout && !isAnalytics) {
                paidLbl.text = @"DATA PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            
            // 0 1 0 0
            if (!isData && isRoutine && !isWorkout && !isAnalytics) {
                paidLbl.text = @"ROUTINE PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            
            // 0 0 1 0
            if (!isData && !isRoutine && isWorkout && !isAnalytics) {
                paidLbl.text = @"WORKOUT PACK\t";
                paidLbl.backgroundColor = singleColor;
            }

            // 0 0 0 1
            if (!isData && !isRoutine && !isWorkout && isAnalytics) {
                paidLbl.text = @"ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }

            // 1100
            if (isData && isRoutine && !isWorkout && !isAnalytics) {
                paidLbl.text = @"DATA, ROUTINE PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            // 1010
            if (isData && !isRoutine && isWorkout && !isAnalytics) {
                paidLbl.text = @"DATA, WORKOUT PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            // 1001
            if (isData && !isRoutine && !isWorkout && isAnalytics) {
                paidLbl.text = @"DATA, ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            
            // 0011
            if (!isData && !isRoutine && isWorkout && isAnalytics) {
                paidLbl.text = @"WORKOUT, ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            // 0101
            if (!isData && isRoutine && !isWorkout && isAnalytics) {
                paidLbl.text = @"ROUTINE, ANALYTICS PACK\t";
                paidLbl.backgroundColor = FlatMint;
            }
            // 0110
            if (!isData && isRoutine && isWorkout && !isAnalytics) {
                paidLbl.text = @"ROUTINE, WORKOUT PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            
            // 1110
            if (isData && isRoutine && isWorkout && !isAnalytics) {
                paidLbl.text = @"DATA, ROUTINE, WORKOUT PACK\t";
                paidLbl.backgroundColor = singleColor;

            }
            // 1101
            if (isData && isRoutine && !isWorkout && isAnalytics) {
                paidLbl.text = @"DATA, ROUTINE, ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            // 1011
            if (isData && !isRoutine && isWorkout && isAnalytics) {
                paidLbl.text = @"DATA, WORKOUT, ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            // 0111
            if (!isData && isRoutine && isWorkout && isAnalytics) {
                paidLbl.text = @"ROUTINE, WORKOUT, ANALYTICS PACK\t";
                paidLbl.backgroundColor = singleColor;
            }
            
            if (isData && isRoutine && isWorkout && isAnalytics) {
                paidLbl.text = @"PREMIUM PACK\t";
                paidLbl.backgroundColor = FlatMint;
            }
            
        
            if (isPremium) {
                paidLbl.text = @"PREMIUM PACK\t";
                paidLbl.backgroundColor = FlatMint;
            }
        }
        
        horizontalPageControl = [[SMPageControl alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(carousel.frame) + 5, CGRectGetWidth(self.view.frame), 30)];
        horizontalPageControl.alignment = SMPageControlAlignmentCenter;
        horizontalPageControl.hidesForSinglePage = YES;
        horizontalPageControl.enabled = false;
        horizontalPageControl.pageIndicatorTintColor = FlatBlack;
        horizontalPageControl.numberOfPages = [headersTitle count];
        horizontalPageControl.currentPage = carousel.currentItemIndex;
        horizontalPageControl.currentPageIndicatorTintColor = FlatWhite;
        
        [cell.contentView addSubview:horizontalPageControl];
        [cell.contentView addSubview:paidLbl];
        [cell.contentView addSubview:carousel];
    } else {
        UILabel *title = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(cell.frame)/2 - 30, CGRectGetWidth(cell.frame), 30)];
        title.textColor = [UIColor whiteColor];
        title.textAlignment = NSTextAlignmentCenter;
        //title.backgroundColor = FlatBlack;
        
        UILabel *subTitle = [[UILabel alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(title.frame), CGRectGetWidth(cell.frame), 30)];
        subTitle.textColor = [UIColor whiteColor];
        subTitle.textAlignment = NSTextAlignmentCenter;
        //subTitle.backgroundColor = FlatBlackDark;
        // Configure the cell
        title.text = [titleArray objectAtIndex:indexPath.row];
        subTitle.text = [subTitleArray objectAtIndex:indexPath.row];
        //    cell.layer.cornerRadius = 5;
        //cell.backgroundColor = [UIColor grayColor];
        title.textColor =[colorArray objectAtIndex:indexPath.row];
        subTitle.textColor = [colorArray objectAtIndex:indexPath.row];
                
        
        if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
        [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
            [subTitle setFont:[UIFont fontWithName:@HAL_THIN_FONT size:26]];
        } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
            [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:20]];
            [subTitle setFont:[UIFont fontWithName:@HAL_THIN_FONT size:24]];
        } else {
            [title setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
            [subTitle setFont:[UIFont fontWithName:@HAL_THIN_FONT size:28]];
        }

        [cell.contentView addSubview:title];
        [cell.contentView addSubview:subTitle];
        
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, cell.contentView.frame.size.height - 2.0, self.tableView.frame.size.width, 2)];
    
    lineView.backgroundColor = [UIColor colorWithGradientStyle:UIGradientStyleLeftToRight withFrame:lineView.frame andColors:@[FlatMagenta, FlatMint, FlatOrange]];
    [cell.contentView addSubview:lineView];


    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    float cellWidth = (self.tableView.frame.size.width);
    float cellHeight;
    if (indexPath.row !=0) {
        cellHeight   = ((self.tableView.frame.size.height)*1/2)/3;
    } else {
        cellHeight   = self.view.frame.size.height * 1/2;
    }
    
    return cellHeight;

}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if( indexPath.row == 0)
        return;
    
    [self performSegueWithIdentifier:[segueArray objectAtIndex:indexPath.row] sender:self];
}

#pragma mark - Navigation

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    NSString *date = [Utilities getCurrentDate];
    if ([segue.identifier isEqualToString:@"selectRoutineSegue"]) {
        SelectRoutine *destVC = segue.destinationViewController;
        destVC.date = date;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"routineSegue"]) {
        RoutineList *destVC = segue.destinationViewController;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"AnalyzeSegue"]) {
        AnalyzeRoutine *destVC = segue.destinationViewController;
        destVC.hidesBottomBarWhenPushed = TRUE;
    } else if ([segue.identifier isEqualToString:@"recordDailyStatsSegue"]) {
        DailyStatsVC *destVC = segue.destinationViewController;
        destVC.hidesBottomBarWhenPushed = TRUE;
    }
}

-(void) showStore{
    [self performSegueWithIdentifier:@"workoutStoreSegue" sender:self];
}

//-(void) getAppStoreForProduct: (NSInteger) storeId {
//    
//    NSString *const TAITunesAPI = @"https://itunes.apple.com";
//    
//    NSString *urlStr = [NSString stringWithFormat:@"%@/lookup?id=%@&country=%@", TAITunesAPI, @(storeId), @"us"];
//    NSLog(@"URL sending is %@", urlStr);
//
//    // 1
//    NSURL *url = [NSURL URLWithString:urlStr];
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    
//    // 2
//    AFHTTPRequestOperation *operation = [[AFHTTPRequestOperation alloc] initWithRequest:request];
//    operation.responseSerializer = [AFJSONResponseSerializer serializer];
//    
//    [operation setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
//        NSDictionary *data = (NSDictionary *)[responseObject[@"results"] firstObject];;
//        
//        NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
//        [returnData setObject:data[@"trackName"] forKey:@"AppName"];
//        [returnData setObject:data[@"formattedPrice"] forKey:@"Price"];
//        [returnData setObject:data[@"artworkUrl512"] forKey:@"Icon"];
//        [returnData setObject:[data[@"screenshotUrls"] firstObject] forKey:@"ScreenShot"];
//    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//    }];
//    
//    // 5
//    [operation start];
//}

- (void)showTrial:(CNPPopupStyle)popupStyle {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"WELCOME TO GYMINUTES" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSString *infoStr = [NSString stringWithFormat:@"As a new user, you get %d day FREE access to all PREMIUM FEATURES. After that, access to Premium features will be restricted.\n\nIf you find the app useful, please consider upgrading to PREMIUM PACK.",  FREE_VERSION_TRIAL_PERIOD];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 60)];
    [button setTitleColor:FlatMintDark forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
    [button setTitle:@"TRY PREMIUM PACK" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor whiteColor];
//    [button setTitleColor:FlatMintDark forState:UIControlStateNormal];
//    button.layer.borderColor = FlatMintDark.CGColor;
//    button.layer.borderWidth = 2;
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        [alert addButton:@"Yes" actionBlock:^{
           // [Utilities startWorkoutOnSignup:@"Bodyweight Max" workoutName:@"Full Body Workout"];
            [Utilities downloadIAPRoutineFromStore:@IAP_ROUTINE_BODYWEIGHT_MAX view:self];
        }];
        [alert showSuccess:self.parentViewController title:@"GET STARTED" subTitle:@"Would you like to download a FREE BODYWEIGHT workout?" closeButtonTitle:@"No" duration:0];
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    titleLabel.textColor = FlatOrangeDark;
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    //lineTwoLabel.attributedText = lineTwo;
    lineTwoLabel.text = infoStr;
    lineTwoLabel.textAlignment = NSTextAlignmentCenter;
    lineTwoLabel.textColor = FlatBlueDark;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineTwoLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.backgroundColor = FlatWhite;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    [self.popupController presentPopupControllerAnimated:YES];
}


-(void) getUpdatedEntries {
    NSDate *userAccountDate = [PFUser currentUser].createdAt;
    
    NSLog(@"UserCreated Data = %@", userAccountDate);
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *allExercise = [ExerciseList MR_findAllInContext:localContext];
    
    PFQuery *query = [PFQuery queryWithClassName:@P_EXERCISELIST_CLASS];
    [query whereKey:@"updatedAt" greaterThanOrEqualTo:userAccountDate];
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (!error) {
            for (PFObject *exercise in objects) {
                NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exercise[@"ExerciseName"]];
                NSArray *item = [allExercise filteredArrayUsingPredicate:exPredicate];
                
                if ([item count] == 0) {
                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                        ExerciseList *data = [ExerciseList MR_createEntityInContext:localContext];
                        data.exerciseName = exercise[@"ExerciseName"];
                        data.expLevel = exercise[@"ExpLevel"];
                        data.mechanics = exercise[@"Mechanics"];
                        data.muscle = exercise[@"Muscle"];
                        data.type = exercise[@"Type"];
                        data.equipment = exercise[@"Equipment"];
                        data.majorMuscle = exercise[@"MajorMuscle"];
                        data.syncedState = [NSNumber numberWithBool:SYNC_DONE];
                        
                        //NSLog(@"major muscle is %@", data.majorMuscle);
                        data.exercisePerformed = [NSNumber numberWithBool:false];
                     } completion:^(BOOL contextDidSave, NSError *error) {
                         NSLog(@"NEED to create exercise.. %@ DONE", item);

                     }];
                } else {
                    [MagicalRecord saveWithBlock:^(NSManagedObjectContext *localContext) {
                    ExerciseList *data = [item objectAtIndex:0];
                    data.exerciseName = exercise[@"ExerciseName"];
                    data.expLevel = exercise[@"ExpLevel"];
                    data.mechanics = exercise[@"Mechanics"];
                    data.muscle = exercise[@"Muscle"];
                    data.type = exercise[@"Type"];
                    data.equipment = exercise[@"Equipment"];
                    data.majorMuscle = exercise[@"MajorMuscle"];
                    data.syncedState = [NSNumber numberWithBool:SYNC_DONE];
//                    data.exercisePerformed = [NSNumber numberWithBool:false];
                    } completion:^(BOOL contextDidSave, NSError *error) {
                        NSLog(@"NEED to modify exercise.. %@ DONE", item);

                    }];
                }
            }
            [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"ExerciseListDBSync"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }];
}



- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return true;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
            return false;
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
            
    }
}


#pragma mark -
#pragma mark iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
//    NSNumber *item = (self.items)[(NSUInteger)index];
    NSLog(@"Tapped view number: %ld", (long)index);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel
{
    NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
    horizontalPageControl.currentPage = self.carousel.currentItemIndex;

}

-(void) getLastWorkoutInfo {
    NSMutableDictionary *dataDict = [[NSMutableDictionary alloc] init];
    
    NSString *workoutName;
    int dateArrayLen = (int)[processedDates count];
    if (dateArrayLen == 0) {
        workoutName = @"Welcome To GYMINUTES";// Create a workout routine or download a routine from our store.";
        [dataDict setObject:@"None" forKey:@"Routine"];
        [dataDict setObject:@"N/A" forKey:@"Date"];
        [dataDict setObject:@"0" forKey:@"Days Ago"];
        
    } else {
        NSString *lastDateInArray = [processedDates objectAtIndex:dateArrayLen - 1];
        NSLog(@"last date is %@", lastDateInArray);
        NSPredicate *exPredicate = [NSPredicate predicateWithFormat:@"date == %@", lastDateInArray];
        NSArray *setDataArray = [totalWorkouts filteredArrayUsingPredicate:exPredicate];
        ExerciseSet *data = [setDataArray objectAtIndex:0];
        
        if ([lastDateInArray isEqualToString:[Utilities getCurrentDate]]) {
        } else  {
            NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
            [formatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *lastDate = [formatter dateFromString:data.date];
            NSDate *todatDate = [NSDate date];
            unsigned int unitFlags = NSCalendarUnitDay;
            NSCalendar *currCalendar = [NSCalendar currentCalendar];
            NSDateComponents *conversionInfo = [currCalendar components:unitFlags fromDate:lastDate   toDate:todatDate  options:0];

            NSDateFormatter *simFormatter = [[NSDateFormatter alloc] init];
            [simFormatter setDateFormat:@"MMM dd"];

            
            workoutName = data.workoutName;
            [dataDict setObject:data.routineName forKey:@"Routine"];
            [dataDict setObject:[simFormatter stringFromDate:[formatter dateFromString:data.date]] forKey:@"Date"];
            [dataDict setObject:[NSNumber numberWithInt:(int)[conversionInfo day]] forKey:@"Days Ago"];
        }
    }
    [dataDict setObject:workoutName forKey:@"Name"];
    [carouselData setObject:dataDict forKey:AT_LAST_WORKOUT];
}


-(void) getAllUserSets {
    
// @[@"INVITE FRIENDS", @"LAST WORKOUT", @"FAVORITE MUSCLE", @"FAVORITE EXERCISE", @"FAVORITE WORKOUT"];
    
    NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_defaultContext];
    NSArray *userSets = [ExerciseSet MR_findAllSortedBy:@"date" ascending:NO inContext:localContext];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    
    [self getLastWorkoutInfo];
    [carouselData setObject:[DBInterface getDailyStatsCard:[formatter stringFromDate:[NSDate date]]] forKey:AT_RECORD_STATS];
    [carouselData setObject:[DBInterface getMostWorkedOutExercise:userSets] forKey:AT_FAV_EXERCISE];
    [carouselData setObject:[DBInterface getMostWorkedOutWorkout:userSets] forKey:AT_FAV_WORKOUT];
    [carouselData setObject:[DBInterface getMostWorkedOutMuscle:userSets] forKey:AT_FAV_MUSCLE];
    
//    NSLog(@"carousel DATA %@", carouselData);
}

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
  //  NSLog(@"num items %lu", (long) [headersTitle count]);
    return [headersTitle count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *header = nil;
    UIView *showView = nil;
    
    if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
    view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.view.frame) - 40, 160.0f)];
    } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.view.frame) - 40, 180.0f)];
    } else {
        view = [[UIView alloc] initWithFrame:CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.view.frame) - 40, 200.0f)];
    }
    
    //set up content
    header = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(view.frame), 30)];
    showView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(header.frame), CGRectGetWidth(view.frame), CGRectGetHeight(view.frame) - CGRectGetHeight(header.frame))];
    
    UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:header.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerTopLeft) cornerRadii:CGSizeMake(15.0, 15.0)];
    
    UIBezierPath *RPmaskPath = [UIBezierPath bezierPathWithRoundedRect:showView.bounds byRoundingCorners:(UIRectCornerBottomLeft | UIRectCornerBottomRight) cornerRadii:CGSizeMake(15.0, 15.0)];
    
    
    CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *RPmaskLayer = [[CAShapeLayer alloc] init];
    
    WPmaskLayer.frame = view.bounds;
    WPmaskLayer.path  = WPmaskPath.CGPath;
    RPmaskLayer.frame = view.bounds;
    RPmaskLayer.path  = RPmaskPath.CGPath;
    
    header.layer.mask = WPmaskLayer;
    showView.layer.mask = RPmaskLayer;
    
    
    header.textColor = [UIColor whiteColor];
    header.textAlignment = NSTextAlignmentCenter;
    [header setFont:[UIFont fontWithName:@HAL_REG_FONT size:16]];
    
    
    NSMutableDictionary *data = [carouselData objectForKey:[headersTitle objectAtIndex:index]];
   // NSLog(@"&*** %@ %ld", [headersTitle objectAtIndex:index], (long)index);
    UILabel *headerSubName = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(showView.frame), 30)];
    headerSubName.text = [data objectForKey:@"Name"];
    headerSubName.textAlignment = NSTextAlignmentCenter;
    [headerSubName setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
    headerSubName.textColor = [UIColor whiteColor];
    
 //   int len = (int)[[data allKeys] count];
//    NSLog(@"len %d", len);
    int count = 0;
    float height = (CGRectGetHeight(showView.frame) - 40)/3;
    
    if ([[headersTitle objectAtIndex:index] isEqualToString:@"INVITE FRIENDS"]) {
        for (NSString *key in data) {
            if ([key isEqualToString:@"Name"])
                continue;
            
            UIButton  *button = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(showView.frame)/4, count * height + CGRectGetHeight(headerSubName.frame) + 5, CGRectGetWidth(showView.frame)/2, height - 5)];
            [button setTitle:[NSString stringWithFormat:@"%@", [data objectForKey:key]] forState:UIControlStateNormal];
            [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
            [button setBackgroundColor:[UIColor whiteColor]];
            [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
            button.layer.cornerRadius = 5;
            button.tag = count;
            [button addTarget:self action:@selector(invitePressed:) forControlEvents:UIControlEventTouchUpInside];
            
            [showView addSubview:button];
            count++;
        }
    } else if ([[headersTitle objectAtIndex:index] isEqualToString:AT_RECORD_STATS]) {
        float maxY = 0;
        for (NSString *key in data) {
            if ([key isEqualToString:@"Name"])
                continue;
            
            height = 30;
            float topGap = 5;
            if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
                topGap = 0;
                height = 20;
            } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
                topGap = 5;
                height = 25;
            }
            
            
            UILabel  *labelVal = [[UILabel alloc] initWithFrame:CGRectMake(10, count * height + CGRectGetHeight(headerSubName.frame) + topGap, CGRectGetWidth(showView.frame)/2 - 10, height)];
            UILabel *labelName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelVal.frame)+ 5, count * height + CGRectGetHeight(headerSubName.frame) + topGap, CGRectGetWidth(showView.frame)/2 - 10, height)];
            labelVal.text = [NSString stringWithFormat:@"%@", [data objectForKey:key]];
            labelName.text = key;
            
        //    NSLog(@"VIEW: &&& %@ %@ %@", key, labelName.text, labelVal.text);
            labelName.textColor = [UIColor whiteColor];
            labelVal.textColor = [UIColor whiteColor];
            
            labelName.textAlignment = NSTextAlignmentLeft;
            labelVal.textAlignment = NSTextAlignmentRight;
            
            if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
                [labelName setFont:[UIFont fontWithName:@HAL_THIN_FONT size:16]];
                [labelVal setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:16]];
            } else {
                [labelName setFont:[UIFont fontWithName:@HAL_THIN_FONT size:24]];
                [labelVal setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
            }
            count++;
            
            [showView addSubview:labelName];
            [showView addSubview:labelVal];
            maxY = CGRectGetMaxY(labelVal.frame);
        }

        UIButton  *button = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetWidth(showView.frame)/4, maxY + 5, CGRectGetWidth(showView.frame)/2, 30)];
        [button setTitle:@"RECORD" forState:UIControlStateNormal];
        [button setTitleColor:[Utilities getAppColor] forState:UIControlStateNormal];
        [button setBackgroundColor:[UIColor whiteColor]];
        [button.titleLabel setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:14]];
        button.layer.cornerRadius = 5;
        [button addTarget:self action:@selector(recordBodyStats:) forControlEvents:UIControlEventTouchUpInside];        
        [showView addSubview:button];

    } else {
        for (NSString *key in data) {
            if ([key isEqualToString:@"Name"])
                continue;
            
            height = 30;
            float topGap = 30;
            if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
                topGap = 0;
                height  = 25;
            } else if ([deviceModel containsString:@"5"] || [deviceModel containsString:@"SE"] || [deviceModel containsString:@"iPod"]) {
                topGap = 5;
                height = 25;
            }
            
            UILabel  *labelVal = [[UILabel alloc] initWithFrame:CGRectMake(10, count * height + CGRectGetHeight(headerSubName.frame) + topGap, CGRectGetWidth(showView.frame)/2 - 10, height)];
            UILabel *labelName = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(labelVal.frame)+ 5, count * height + CGRectGetHeight(headerSubName.frame) + topGap, CGRectGetWidth(showView.frame)/2 - 10, height)];
            labelVal.text = [NSString stringWithFormat:@"%@", [data objectForKey:key]];
            labelName.text = key;
            
    //        NSLog(@"VIEW: &&& %@ %@ %@", key, labelName.text, labelVal.text);
            labelName.textColor = [UIColor whiteColor];
            labelVal.textColor = [UIColor whiteColor];
            
            labelName.textAlignment = NSTextAlignmentLeft;
            labelVal.textAlignment = NSTextAlignmentRight;
            
            [labelName setFont:[UIFont fontWithName:@HAL_THIN_FONT size:24]];
            [labelVal setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:24]];
            count++;
            
            [showView addSubview:labelName];
            [showView addSubview:labelVal];
            
        }
    }
    [showView addSubview:headerSubName];
    showView.backgroundColor = [UIColor clearColor];
    header.backgroundColor = [UIColor clearColor];
    
    header.tag = 10 + index;
    showView.tag = 20 + index;
    
 //   NSLog(@"Setting header:%ld showView:%ld", (long)header.tag, (long)showView.tag);
    //        view.layer.borderWidth = 1.0f;
    //        view.layer.cornerRadius = 5.0f;
    //        view.layer.borderColor = [UIColor whiteColor].CGColor;
    //
    
    view.backgroundColor = [Utilities getAppColor];
    view.layer.masksToBounds = NO;
    view.layer.shadowOffset = CGSizeMake(-10, 10);
    view.layer.shadowRadius = 5;
    view.layer.shadowOpacity = 0.5;
    
    [view addSubview:header];
    [view addSubview:showView];

    header.text = [headersTitle objectAtIndex:index];
    return view;
}

-(IBAction)invitePressed:(id)sender {
    UIButton *button = (UIButton *) sender;
    switch (button.tag) {
        case 0:
            NSLog(@"email pressed");
            [self emailInvite];
            break;
        case 1:
            NSLog(@"message pressed");
            [self messageInvite];
            break;
        case 2:
            NSLog(@"facebook pressed");
            [self facebookInvite];
            break;
        default:
            break;
    }
}

- (void) mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error
{
    
    switch (result)
    {
        case MFMailComposeResultCancelled:
            NSLog(@"Mail cancelled");
            break;
        case MFMailComposeResultSaved:
            NSLog(@"Mail saved");
            break;
        case MFMailComposeResultSent:
            NSLog(@"Mail sent");
            break;
        case MFMailComposeResultFailed: {
            NSLog(@"Mail sent failure: %@", [error localizedDescription]);
            [Flurry logError:@"emailFailure" message:[NSString stringWithFormat:@"Date:%@,UserID:%@", [NSDate date], [PFUser currentUser].objectId] error:error];
        }
            break;
        default:
            break;
    }
    
    
    // Close the Mail Interface
    [self dismissViewControllerAnimated:YES completion:NULL];
}

-(void) messageInvite {
    if(![MFMessageComposeViewController canSendText]) {
        UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Your device doesn't support SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [warningAlert show];
        return;
    }
    
    NSString *message = [NSString stringWithFormat:@"Hey, checkout gyminutes. This is a great workout tracking app. https://itunes.apple.com/us/app/gyminutes/id1059671198?ls=1&mt=8"];
    
    MFMessageComposeViewController *messageController = [[MFMessageComposeViewController alloc] init];
    messageController.messageComposeDelegate = self;
    [messageController setBody:message];
    
    
    // Present message view controller on screen
    [self presentViewController:messageController animated:YES completion:^{
        NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                       @"UserID", [PFUser currentUser].objectId,
                                       nil];
        [Flurry logEvent:@"sharedAppLinkViaMessage" withParameters:articleParams];
    }];

}
-(void) emailInvite {
    NSString *emailTitle = @"Checkout \"gyminutes\" for IOS.";
    // Email Content
    NSString *messageBody = @"Hey,\n\nCheckout GYMINUTES. Its the best workout tracking app.\n\n https://itunes.apple.com/us/app/gyminutes/id1059671198?ls=1&mt=8";
    // To address
    //    NSArray *toRecipents = [NSArray arrayWithObject:@[currentUser.email]];
    Class mailClass = (NSClassFromString(@"MFMailComposeViewController"));
    
    if (mailClass != nil) {
        MFMailComposeViewController *mc = [[MFMailComposeViewController alloc] init];
        mc.mailComposeDelegate = self;
        [mc setSubject:emailTitle];
        [mc setMessageBody:messageBody isHTML:YES];
        
        
        NSLog(@"readches here..");
        // Present mail view controller on screen
        if([mailClass canSendMail]) {
            NSLog(@"why i am crashing here...?");
            [self presentViewController:mc animated:YES completion:^{
                NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                               @"UserID", [PFUser currentUser].objectId,
                                               nil];
                [Flurry logEvent:@"sharedAppLinkViaEmail" withParameters:articleParams];
                
            }];
        }
    }
}
-(void) facebookInvite {
    FBSDKAppInviteContent *content =[[FBSDKAppInviteContent alloc] init];
    content.appLinkURL = [NSURL URLWithString:@"https://fb.me/574699316023143"];
    //optionally set previewImageURL
    content.appInvitePreviewImageURL = [NSURL URLWithString:@"https://s3-us-west-1.amazonaws.com/gyminutesapp.com/invitePic.png"];
    
    // present the dialog. Assumes self implements protocol `FBSDKAppInviteDialogDelegate`
    [FBSDKAppInviteDialog showFromViewController:self withContent:content delegate:self];
}
-(void) appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didCompleteWithResults:(NSDictionary *)results {
    NSLog(@"%lu friends invited %@", (unsigned long)[[results allKeys] count], results);
    
}

-(void) appInviteDialog:(FBSDKAppInviteDialog *)appInviteDialog didFailWithError:(NSError *)error {
    NSLog(@"app invite failed with reason %@", error);
}

- (void)messageComposeViewController:(MFMessageComposeViewController *)controller didFinishWithResult:(MessageComposeResult) result
{
    switch (result) {
        case MessageComposeResultCancelled:
            break;
            
        case MessageComposeResultFailed:
        {
            UIAlertView *warningAlert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Failed to send SMS!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [warningAlert show];
            [Flurry logError:@"msgFailure" message:[NSString stringWithFormat:@"Date:%@,UserID:%@", [NSDate date], [PFUser currentUser].objectId] error:nil];
            break;
        }
            
        case MessageComposeResultSent:
            break;
            
        default:
            break;
    }
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(IBAction)recordBodyStats:(id)sender {
    [self performSegueWithIdentifier:@"recordDailyStatsSegue" sender:self];
}

-(void) printAllRecords {
    
    NSArray *items = [ExercisePersonalRecords MR_findAll];
    NSLog(@"print all records data... %lu", (unsigned long)[items count]);
    for (ExercisePersonalRecords *rec in items) {
        NSLog(@"%@", rec);
    }
}

#pragma MIGRATE TO AMRAP
-(void) askUserToMigrateToAmrap {
    if ([[NSUserDefaults standardUserDefaults] boolForKey:USER_DEFAULT_UPDATE_TO_AMRAP] == true) {
        NSLog(@"not updating to amrap again");
        return;
    }
    
    [UpdateHandler v2_4_update_UserWorkout_to_AMRAP];
    [UpdateHandler v2_4_update_WorkoutList_to_AMRAP];
    [[NSUserDefaults standardUserDefaults] setBool:YES   forKey:USER_DEFAULT_UPDATE_TO_AMRAP];
    [[NSUserDefaults standardUserDefaults] synchronize];

    /*
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"NEW FEATURE !!" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:18], NSForegroundColorAttributeName : FlatBlack, NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"We recently added a new feature that allows you to define custom reps per set. You can also set AMRAP (As Many Reps As Possible) for any set under CREATE WORKOUT.\n\nWe hope this feature enhances your workout tracking experience. " attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_REG_FONT size:16], NSForegroundColorAttributeName : FlatBlack, NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 40)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"CLOSE" forState:UIControlStateNormal];
    button.backgroundColor = [Utilities getAppColor];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        [[NSUserDefaults standardUserDefaults] setBool:YES   forKey:USER_DEFAULT_UPDATE_TO_AMRAP];
        [[NSUserDefaults standardUserDefaults] synchronize];
    };
    
    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon"]];
    
    UILabel *lineTwoLabel = [[UILabel alloc] init];
    lineTwoLabel.numberOfLines = 0;
    lineTwoLabel.attributedText = lineTwo;
    lineTwoLabel.textAlignment = NSTextAlignmentJustified;
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, imageView, lineTwoLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleCentered;
    [self.popupController presentPopupControllerAnimated:YES];
     */
}


@end
