//
//  UserWorkout+CoreDataProperties.m
//  gyminutes
//
//  Created by Mayank Verma on 2/20/17.
//  Copyright © 2017 Mayank Verma. All rights reserved.
//

#import "UserWorkout+CoreDataProperties.h"

@implementation UserWorkout (CoreDataProperties)

+ (NSFetchRequest<UserWorkout *> *)fetchRequest {
	return [[NSFetchRequest alloc] initWithEntityName:@"UserWorkout"];
}

@dynamic createdDate;
@dynamic exerciseGroup;
@dynamic exerciseName;
@dynamic exerciseNumber;
@dynamic exerciseNumInGroup;
@dynamic hasDropSet;
@dynamic hasPyramidSet;
@dynamic majorMuscle;
@dynamic muscle;
@dynamic reps;
@dynamic restTimer;
@dynamic routineBundleId;
@dynamic routineName;
@dynamic sets;
@dynamic superSetWith;
@dynamic syncedState;
@dynamic workoutName;
@dynamic workoutNumber;
@dynamic workoutUserCreated;
@dynamic restPerSet;
@dynamic repsPerSet;

@end
