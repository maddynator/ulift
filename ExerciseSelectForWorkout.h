//
//  ExerciseSelectForWorkout.h
//  uLift
//
//  Created by Mayank Verma on 7/30/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commons.h"
@interface ExerciseSelectForWorkout : UIViewController <UITableViewDataSource, UITableViewDelegate,UISearchControllerDelegate, UISearchBarDelegate, UISearchResultsUpdating>// CNPPopupControllerDelegate>//UICollectionViewDataSource,UICollectionViewDelegateFlowLayout, UICollectionViewDelegate>

@property (nonatomic, retain) NSString *muscleGroup;
@property (nonatomic, retain) NSString *date;
@property (nonatomic, retain) NSString *routineName;
@property (nonatomic, retain) NSString *workoutName;
@property (nonatomic, retain) NSNumber *exerciseGroup;
@property (nonatomic, retain) NSNumber *exerciseNumInGroup;
@property (nonatomic) BOOL isTempExercise;
//@property (nonatomic, retain) IBOutlet UICollectionView *collectionView;
//@property (nonatomic, retain) IBOutlet UISearchBar *searchBar;
@property (nonatomic, retain) IBOutlet STCollapseTableView *myTableView;
@property (nonatomic, retain) IBOutlet UIToolbar *myToolBar;
@property (nonatomic, retain) IBOutlet UIToolbar *mySearchBarView;
@property (nonatomic, strong) UISearchController * searchController;
@property (nonatomic, strong) IBOutlet UITableView *customRepsPerSet;
@end
