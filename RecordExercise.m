//
//  RecordExercise.m
//  uLift
//
//  Created by Mayank Verma on 7/3/15.
//  Copyright (c) 2015 Mayank Verma. All rights reserved.
//

#import "RecordExercise.h"
#import <AudioToolbox/AudioToolbox.h>
#import "ExerciseHistoryTVC.h"
#import "ExerciseSummary.h"

/*
 
 
    THIS FILE IS DEPRECATED
 
 
 
 
 */

@interface RecordExercise () <UINavigationControllerDelegate>{
    NSArray *exerciseSets;
    BOOL updateField, additonalViewOpen, isDropSetVisible, secondReload;
    int selectedIndex, indexMax;
    float maxWeight;
    int maxRep;
    NSString *historyStr;
    int seatHeight, handAngle, timerValueToCountDown;
    ExerciseMetaInfo *exMetaInfo;
    NSManagedObjectContext *localContext;
    UIToolbar *multiSelectView;
    UIView *additionOptionView;
    NSArray *exerciseList;
    MZTimerLabel *timerBWEx, *timerBWSts;
    BOOL warmupFlag, addTempExOpen;
    UIButton *unMarkDropSet, *markDropSet;
    NSTimer *setUpdateTimer;
    NSMutableArray *setArray;
    UIToolbar* addOnKeyboard;

    // additional views
    NSArray *weightsLbs, *weightsKgs, *weightLbVal, *weightKgVal, *barbelWtLbs, *barbellWtKgs;
    NSMutableArray *weightLbsArray, *weightKgsArray;
    float totalWeight;
    float barbellWeight;
    TNRadioButtonGroup *barbellGroup;
    float lbORkg;
    UITableView *myBarbelTableView;
    UILabel *calculatedWeightLbl;
    
    UICollectionView *myCollectionView;
    NSMutableArray *dumbbellMenuWeights;

    NSArray *previousWorkoutSets;
}

@end
@implementation RecordExercise

@synthesize weightText, repText, setsTableView, weightMinus, weightPlus, repMinus, repPlus, saveBtn, cllearBtn, exerciseObj, dateForExercise, timer, seat, head, clockTimer, nextExBtn, prevExBtn, segmentControl, setsLbl;

-(void) viewDidLoad {
    [super viewDidLoad];

    // we also want to remove all notification when user return to this screen...

    secondReload = false;
    NSLog(@"Exercise Number is %@, %@ %@", _exerciseNumber, exerciseObj.exerciseNumber, exerciseObj.restSuggested);
    [self showWarmUpMessage:CNPPopupStyleCentered];
    
    addOnKeyboard = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 40)];
    
    addOnKeyboard.items = [NSArray arrayWithObjects:
                           //  [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStyleBordered target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Plate Calculator" style:UIBarButtonItemStyleDone target:self action:@selector(doneWithNumberPad)],
//                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           nil];
    
    weightText.inputAccessoryView = addOnKeyboard;

    addOnKeyboard.tintColor = FlatOrangeDark;
    
    weightsLbs = [[NSArray alloc] initWithObjects: @"45 Lbs", @"35 Lbs", @"25 Lbs", @"10 Lbs", @"5 Lbs", @"2.5 Lbs", nil];
    weightsKgs = [[NSArray alloc] initWithObjects:@"25 Kgs", @"20 Kgs", @"15 Kgs", @"10 Kgs", @"5 Kgs", @"2 Kgs", @"1.5 Kgs", @"1.0 Kgs", @"0.5 Kgs", nil];
    
    weightLbVal = [[NSArray alloc] initWithObjects:
                   [NSNumber numberWithFloat:45],
                   [NSNumber numberWithFloat:35],
                   [NSNumber numberWithFloat:25],
                   [NSNumber numberWithFloat:10],
                   [NSNumber numberWithFloat:5.0],
                   [NSNumber numberWithFloat:2.5],
                   nil];
    
    weightKgVal = [[NSArray alloc] initWithObjects:
                   [NSNumber numberWithFloat:25],
                   [NSNumber numberWithFloat:20],
                   [NSNumber numberWithFloat:15],
                   [NSNumber numberWithFloat:10],
                   [NSNumber numberWithFloat:5],
                   [NSNumber numberWithFloat:2],
                   [NSNumber numberWithFloat:1.5],
                   [NSNumber numberWithFloat:1.0],
                   [NSNumber numberWithFloat:.5],
                   nil];
    
    
    weightKgsArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0], [NSNumber numberWithFloat:0],[NSNumber numberWithFloat:0],[NSNumber numberWithFloat:0], nil];
    
    barbelWtLbs = [[NSArray alloc] initWithObjects:[NSNumber numberWithFloat:25], [NSNumber numberWithFloat:35], [NSNumber numberWithFloat:45], nil];
    barbellWtKgs = [[NSArray alloc] initWithObjects:[NSNumber numberWithFloat:10], [NSNumber numberWithFloat:15], [NSNumber numberWithFloat:20], nil];
    totalWeight = 0;
    
    self.navigationItem.hidesBackButton = YES;
    UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"END" style:UIBarButtonItemStylePlain target:self action:@selector(backButtonDidPressed:)];
    self.navigationItem.leftBarButtonItem = newBackButton;
    
}
- (void)backButtonDidPressed:(id)aResponder {
    // it can be useful to store this into a BOOL property
    SCLAlertView *alert = [[SCLAlertView alloc] init];
    
    [alert addButton:@"Exit Workout" actionBlock:^{
        [self.navigationController popViewControllerAnimated:TRUE];
    }];
    
    [alert showWarning:self title:@"END WORKOUT?" subTitle:@"Your workout is not complete. Do you still want to exit?" closeButtonTitle:@"Continue" duration:0.0f];
    
}


-(void) viewDidAppear:(BOOL)animated {
    // we need to do this in viewDidAppear because user can change from LB to KG in between and when we come back, we need to change text from lb to kg and or vice versa
    lbORkg = [Utilities isKgs];
    
    // this prevent user from opening multiple add temporary exercises menu.
    addTempExOpen = false;
    
    exerciseList = [Utilities getAllExerciseForDate:_date];
    
    timerValueToCountDown = [exerciseObj.restSuggested intValue];
    NSLog(@"timerValueToCountDown: %d", timerValueToCountDown);
    NSString *deviceModel = [Utilities getIphoneName];
    
    UIToolbar *test = [[UIToolbar alloc] init];
    test.barTintColor = FlatOrangeDark;
    UIBarButtonItem *flexiableItem = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:nil];
    seat = [[UIBarButtonItem alloc] initWithTitle:@"Seat" style:UIBarButtonItemStylePlain target:self action:@selector(seatSetting:)];
    head = [[UIBarButtonItem alloc] initWithTitle:@"Height" style:UIBarButtonItemStylePlain target:self action:@selector(handSetting:)];
    clockTimer = [[UIBarButtonItem alloc] initWithTitle:@"Timer" style:UIBarButtonItemStylePlain target:self action:@selector(timerValue:)];

    [seat setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@HAL_REG_FONT size:14.0], NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    [head setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                  [UIFont fontWithName:@HAL_REG_FONT size:14.0], NSFontAttributeName,
                                  [UIColor whiteColor], NSForegroundColorAttributeName,
                                  nil]
                        forState:UIControlStateNormal];
    
    [clockTimer setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                        [UIFont fontWithName:@HAL_REG_FONT size:14.0], NSFontAttributeName,
                                        [UIColor whiteColor], NSForegroundColorAttributeName,
                                        nil] 
                              forState:UIControlStateNormal];
    
    NSArray *items = [NSArray arrayWithObjects:seat, flexiableItem, head, flexiableItem, clockTimer, nil];
    
    [test setItems:items animated:YES];
    
    if ([deviceModel containsString:@"iPhone 4"] || [deviceModel containsString:@"iPhone 4S"] || [deviceModel containsString:@"iPad"]) {
        [test setFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 30, CGRectGetWidth(self.view.frame), 30)];
    } else {
        [test setFrame:CGRectMake(0, CGRectGetHeight(self.view.frame) - 35, CGRectGetWidth(self.view.frame), 35)];
    }
    [self.view addSubview:test];
    
    [self reloadAllData];
    warmupFlag = false;
    
    [self reloadHeaderButtons];

}

-(void) reloadHeaderButtons {

    nextExBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    prevExBtn.titleLabel.lineBreakMode = NSLineBreakByWordWrapping;
    
    nextExBtn.titleLabel.textAlignment = NSTextAlignmentRight;
    prevExBtn.titleLabel.textAlignment = NSTextAlignmentLeft;
    NSLog(@"exercise count is %lu", (long) [exerciseList count]);
    if ([exerciseList count] == 1) {
        prevExBtn.hidden = YES;
        nextExBtn.hidden = NO;
        [nextExBtn setTitle:@"Workout Summary" forState:UIControlStateNormal];
        return;
    }
    
    
    if ([_exerciseNumber intValue] == 0) {
        NSLog(@"First exercise");
        WorkoutList *next = [exerciseList objectAtIndex:[_exerciseNumber intValue] + 1];
        prevExBtn.hidden = YES;
        nextExBtn.hidden = NO;
        [nextExBtn setTitle:[NSString stringWithFormat:@"%@ >", next.exerciseName] forState:UIControlStateNormal];
    } else if ([_exerciseNumber intValue] == [exerciseList count] - 1) {
        NSLog(@"last exercise");
        prevExBtn.hidden = NO;
        nextExBtn.hidden = NO;
        WorkoutList *prev = [exerciseList objectAtIndex:[_exerciseNumber intValue] - 1];
        [prevExBtn setTitle:[NSString stringWithFormat:@"< %@", prev.exerciseName] forState:UIControlStateNormal];
        [nextExBtn setTitle:@"Workout Summary" forState:UIControlStateNormal];
    } else {
        
        NSLog(@"middle exrcise.... %@", _exerciseNumber);
        prevExBtn.hidden = NO;
        nextExBtn.hidden = NO;
        WorkoutList *prev = [exerciseList objectAtIndex:[_exerciseNumber intValue] - 1];
        WorkoutList *next = [exerciseList objectAtIndex:[_exerciseNumber intValue] + 1];
        
        [nextExBtn setTitle:[NSString stringWithFormat:@"%@ >", next.exerciseName] forState:UIControlStateNormal];
        [prevExBtn setTitle:[NSString stringWithFormat:@"< %@", prev.exerciseName] forState:UIControlStateNormal];

    }
}
-(IBAction) loadNextExercise:(id)sender {
    weightText.text = @"";
    repText.text = @"";
    
    secondReload  = true;
    int exNum = [_exerciseNumber intValue];
    if (exNum == [exerciseList count] - 1) {
        NSLog(@"Workout complete.... show summary...");
        [self showCoolDownMessage:CNPPopupStyleCentered];
        [self showPopupWithStyle:CNPPopupStyleActionSheet];
        return;
    }
    
    NSLog(@"Exnum is %d", exNum);

    exNum++;
    _exerciseNumber = [NSNumber numberWithInt:exNum];
    exerciseObj = [exerciseList objectAtIndex:exNum];
    NSLog(@"Exnum is %d", exNum);
    [self reloadHeaderButtons];
    [self reloadAllData];
}
-(IBAction) loadPreviousExercise:(id)sender {
    NSLog(@"Load Previous exercise...");
    weightText.text = @"";
    repText.text = @"";

    secondReload = true;
    int exNum = [_exerciseNumber intValue];
    exNum--;
    _exerciseNumber = [NSNumber numberWithInt:exNum];
    exerciseObj = [exerciseList objectAtIndex:exNum];
    [self reloadHeaderButtons];
    [self reloadAllData];
}


-(void) reloadAllData {
    localContext = [NSManagedObjectContext MR_defaultContext];
    indexMax = 0;
    maxWeight = 0;
    maxRep = 0;
    updateField = false;
    selectedIndex = -1;
    additionOptionView = false;
    isDropSetVisible = false;
    
    [self reloadExercise];
    setsLbl.text = [NSString stringWithFormat:@"Sets: %lu/%@", (unsigned long)[exerciseSets count], exerciseObj.setsSuggested];
//    dateForExercise.text = _date;
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.textColor = [UIColor whiteColor];
    titleLabel.font = [UIFont boldSystemFontOfSize:14];
    titleLabel.text = exerciseObj.exerciseName;
    [titleLabel sizeToFit];
    
    UILabel *subTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 22, 0, 0)];
    subTitleLabel.backgroundColor = [UIColor clearColor];
    subTitleLabel.textColor = [UIColor whiteColor];
    subTitleLabel.font = [UIFont systemFontOfSize:10];
    subTitleLabel.text = [NSString stringWithFormat:@"%@ (%d/%lu)", _date, [_exerciseNumber intValue] + 1, (long)[exerciseList count]];
    [subTitleLabel sizeToFit];
    
    UIView *twoLineTitleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, MAX(subTitleLabel.frame.size.width, titleLabel.frame.size.width), 30)];
    [twoLineTitleView addSubview:titleLabel];
    [twoLineTitleView addSubview:subTitleLabel];
    
    float widthDiff = subTitleLabel.frame.size.width - titleLabel.frame.size.width;
    
    if (widthDiff > 0) {
        CGRect frame = titleLabel.frame;
        frame.origin.x = widthDiff / 2;
        titleLabel.frame = CGRectIntegral(frame);
    }else{
        CGRect frame = subTitleLabel.frame;
        frame.origin.x = fabsf(widthDiff) / 2;
        subTitleLabel.frame = CGRectIntegral(frame);
    }
    
    self.navigationItem.titleView = twoLineTitleView;
   // self.title = twoLineTitleView;
    //self.title = exerciseObj.exerciseName;
    
    dateForExercise.hidden = YES;
    timer.delegate = self;
    [timer setCountDownTime:timerValueToCountDown];
    timer.resetTimerAfterFinish = NO; //IMPORTANT, if you needs custom text with finished, please do not set resetTimerAfterFinish to YES.
    timer.tag = 100;
    timer.timeFormat = @"mm:ss";//@"mm:ss SS";
    timer.timeLabel.backgroundColor = [UIColor clearColor];
    timer.timeLabel.font = [UIFont systemFontOfSize:14.0f];
    timer.timeLabel.textColor = [UIColor blackColor];
    timer.timeLabel.textAlignment = NSTextAlignmentCenter;
    timer.timerType = MZTimerLabelTypeTimer;
    
//    [self.nav addSubview:timer];
   


    weightText.textContainer.maximumNumberOfLines = 1;
    weightText.scrollEnabled = false;
    
   
    repText.textContainer.maximumNumberOfLines = 1;
    repText.scrollEnabled = false;
    
    self.setsTableView.dataSource = self;
    self.setsTableView.delegate = self;
    self.setsTableView.emptyDataSetDelegate = self;
    self.setsTableView.emptyDataSetSource = self;
    
    self.setsTableView.allowsMultipleSelectionDuringEditing = NO;

    weightText.delegate = self;
    repText.delegate = self;
    
    [weightText setReturnKeyType:UIReturnKeyDone];
    [repText setReturnKeyType:UIReturnKeyDone];

    // we need to find out, how many sets of current exercise have been performed.
    repText.text = [NSString stringWithFormat:@"%@",exerciseObj.repsSuggested];
    
    // if exercise is bodyweight, set the weight to 1 and show toast
    NSPredicate *exNamePredicate = [NSPredicate predicateWithFormat:@"exerciseName == %@", exerciseObj.exerciseName];
    ExerciseList *exType = [ExerciseList MR_findFirstWithPredicate:exNamePredicate];
    if ([exType.equipment isEqualToString:@"Bodyweight"]) {
        [self.view makeToast:@"For bodyweight exercises, the default weight is set to 1."
                    duration:2
                    position:CSToastPositionTop
                       title:@"Info"];
        weightText.text = @"1";

    }
    //timerValueToCountDown = [exerciseObj.restSuggested intValue];
    
    [saveBtn addTarget:self action:@selector(saveWorkout:) forControlEvents:UIControlEventTouchUpInside];
    [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]
                                   initWithTarget:self
                                   action:@selector(dismissKeyboard)];
    tap.cancelsTouchesInView = NO;

    exMetaInfo = [Utilities getExerciseMetaInfo:exerciseObj.exerciseName];

    if (exMetaInfo == nil) {
        NSLog(@"meta info absent");
        exMetaInfo = [ExerciseMetaInfo MR_createEntityInContext:localContext];
        exMetaInfo.exerciseName = exerciseObj.exerciseName;
        exMetaInfo.muscle = exerciseObj.muscle;
        exMetaInfo.seatHeight = [NSNumber numberWithInt:0];
        exMetaInfo.handAngle= [NSNumber numberWithInt:0];
        exMetaInfo.maxWeight = [NSNumber numberWithFloat:0];
        exMetaInfo.maxRep = [NSNumber numberWithInt:0];
        exMetaInfo.maxDate = _date;
        exMetaInfo.syncedState = [NSNumber numberWithBool:false];
        [localContext MR_saveToPersistentStoreAndWait];
        seatHeight = 0;
        handAngle = 0;
    } else {
        seatHeight = [exMetaInfo.seatHeight intValue];
        handAngle = [exMetaInfo.handAngle intValue];
        NSLog(@"meta info present %d %d", seatHeight, handAngle);
    }
    maxWeight = [exMetaInfo.maxWeight floatValue];
    maxRep = [exMetaInfo.maxRep intValue];
    
    [self.view addGestureRecognizer:tap];
    [self loadMetaInfo];
    [self buttonsBeautification];
    [self checkSaveBtn];
    [self reloadExercise];
    NSString *historyTitle = @"";
    historyStr = @"";
    
    NSArray *previousWorkout = [Utilities getSetForExerciseOnPreviosDate:exerciseObj.exerciseName date:_date rName:exerciseObj.routiineName wName:exerciseObj.workoutName];
    previousWorkoutSets = previousWorkout;
    
 //   NSLog(@"cont %lu, %@", [previousWorkout count], _date);
    if ([previousWorkout count] > 0) {
        
        ExerciseSet *firstSet = [previousWorkout objectAtIndex:0];
        historyTitle = [NSString stringWithFormat:@"Last Workout (%@)", firstSet.date];
        
        if ([Utilities showWorkoutPackage]) {
            NSString *temp = @"";
            for (int i = 0; i < 1; i++) {
                ExerciseSet *data = [previousWorkout objectAtIndex:i];
                NSLog(@"ex number is : %@", data.exerciseNumber);
                if ([data.isDropSet boolValue] == true) {
                    temp = [NSString stringWithFormat:@" %@(DS-%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                } else {
                    temp = [NSString stringWithFormat:@" %@(%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                }
            }
            historyStr = [NSString stringWithFormat:@"%@ UPGRADE TO PREMIUM", temp];
            [_history setFont:[UIFont fontWithName:@HAL_BOLD_FONT size:12]];
            _history.userInteractionEnabled = YES;
            UITapGestureRecognizer *tapGesture =
            [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showPurchasePopUpHistory)];
            [_history addGestureRecognizer:tapGesture];
        } else {
            
            for (int i = 0; i < [previousWorkout count]; i++) {
                
                ExerciseSet *data = [previousWorkout objectAtIndex:i];
                NSLog(@"ex number is : %@", data.exerciseNumber);
                if ([data.isDropSet boolValue] == true) {
                    historyStr = [NSString stringWithFormat:@" %@(DS-%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                } else {
                    
                    historyStr = [NSString stringWithFormat:@" %@(%d) %dx%.1f   ", historyStr, i + 1, [data.rep intValue], [data.weight floatValue]];
                }
            }
            [_history setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
        }
    } else {
        historyTitle = [NSString stringWithFormat:@"Last Workout"];
        historyStr = @"No Workout History";
        [_history setFont:[UIFont fontWithName:@HAL_REG_FONT size:12]];
    }
    

    _history.text = historyStr;
    _history.numberOfLines = 2;
    _history.textAlignment = NSTextAlignmentCenter;
    _history.backgroundColor = [Utilities getMajorMuscleColorSelected:exerciseObj.majorMuscle];
    _history.textColor = [UIColor whiteColor];

//    _history.hidden = YES;
    
    for (HMSegmentedControl *sgmt in [self.view subviews]) {
        if ([sgmt isKindOfClass:[HMSegmentedControl class]]) {
            [sgmt removeFromSuperview];
        }
    }
    segmentControl = [[HMSegmentedControl alloc] initWithSectionTitles:@[historyTitle, @"PR"]];
//    if (secondReload == true)
//        segmentControl.frame = CGRectMake(0, CGRectGetMinY(tableView.frame) - 53, CGRectGetWidth(self.view.frame), 15);
//    else
//        segmentControl.frame = CGRectMake(0, CGRectGetMinY(tableView.frame) - 10, CGRectGetWidth(self.view.frame), 15);

    segmentControl.frame = CGRectMake(0, CGRectGetMinY(setsTableView.frame) - 53, CGRectGetWidth(self.view.frame), 15);
    segmentControl.selectionStyle = HMSegmentedControlSelectionStyleFullWidthStripe;
    segmentControl.selectionIndicatorLocation = HMSegmentedControlSelectionIndicatorLocationDown;
    segmentControl.selectionIndicatorColor = FlatOrangeDark;
    segmentControl.verticalDividerEnabled = NO;
    segmentControl.verticalDividerColor = [UIColor grayColor];
    segmentControl.verticalDividerWidth = 1.0f;
    segmentControl.selectionIndicatorHeight = 4.0f;
    segmentControl.backgroundColor = [UIColor clearColor];
    segmentControl.titleTextAttributes = @{NSForegroundColorAttributeName : FlatGrayDark, NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:12]};
    [segmentControl addTarget:self action:@selector(segmentedControlChangedValue:) forControlEvents:UIControlEventValueChanged];
    [self.view addSubview:segmentControl];
    NSLog(@"****** segment control frame %f %f", CGRectGetMinY(segmentControl.frame), CGRectGetMinY(setsTableView.frame));
    
}
-(void)dismissKeyboard {
    [weightText resignFirstResponder];
    [repText resignFirstResponder];
}

- (void)segmentedControlChangedValue:(HMSegmentedControl *)segmentedControl {
    NSLog(@"Selected index %ld (via UIControlEventValueChanged)", (long)segmentedControl.selectedSegmentIndex);
    if (segmentedControl.selectedSegmentIndex == 0)
        _history.text = historyStr;
    else {
        if (exMetaInfo != nil) {
            if ([exMetaInfo.maxRep intValue] != 0 && [exMetaInfo.maxWeight floatValue] != 0) {
                if ([Utilities showWorkoutPackage]) {
                    _history.text = [NSString stringWithFormat:@"Set PR of %d x %.1f on UPGRADE TO PREMIUM", [exMetaInfo.maxRep intValue], [exMetaInfo.maxWeight floatValue]];
                } else {
                    _history.text = [NSString stringWithFormat:@"Set PR of %d x %.1f on %@", [exMetaInfo.maxRep intValue], [exMetaInfo.maxWeight floatValue], exMetaInfo.maxDate];
                }
            }
            else
                _history.text = @"No PR Yet.";
        } else
            _history.text = @"No PR Yet";
    }
}

-(void) loadMetaInfo {
    // seat btn text
    if ( exMetaInfo != nil) {
        seat.title = [NSString stringWithFormat:@"Seat (%d)", seatHeight];
        seat.image = [UIImage imageNamed:@"IconExSeat"];
    } else
        seat.title = @"Seat (0)";
    
    
    if (exMetaInfo != nil)
        head.title = [NSString stringWithFormat:@"Hand (%d)", handAngle];
    else
        head.title = @"Hand (0)";
    
    clockTimer.title = [NSString stringWithFormat:@"Timer (%d)", timerValueToCountDown];
}
-(void) buttonsBeautification {
    
    // hand btn text
    // timer btn test
    saveBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    saveBtn.layer.cornerRadius = 5;
    
    cllearBtn.layer.borderColor = [[UIColor grayColor] CGColor];
    cllearBtn.layer.cornerRadius = 5;
    
    UIBezierPath *WPmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];

    UIBezierPath *WMmaskPath = [UIBezierPath bezierPathWithRoundedRect:weightMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];

    UIBezierPath *RPmaskPath = [UIBezierPath bezierPathWithRoundedRect:repPlus.bounds byRoundingCorners:(UIRectCornerTopRight | UIRectCornerBottomRight) cornerRadii:CGSizeMake(5.0, 5.0)];

    UIBezierPath *RMmaskPath = [UIBezierPath bezierPathWithRoundedRect:repMinus.bounds byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerBottomLeft) cornerRadii:CGSizeMake(5.0, 5.0)];

    CAShapeLayer *WPmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *WMmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *RPmaskLayer = [[CAShapeLayer alloc] init];
    CAShapeLayer *RMmaskLayer = [[CAShapeLayer alloc] init];

    WPmaskLayer.frame = self.view.bounds;
    WPmaskLayer.path  = WPmaskPath.CGPath;
    WMmaskLayer.frame = self.view.bounds;
    WMmaskLayer.path  = WMmaskPath.CGPath;
    RPmaskLayer.frame = self.view.bounds;
    RPmaskLayer.path  = RPmaskPath.CGPath;
    RMmaskLayer.frame = self.view.bounds;
    RMmaskLayer.path  = RMmaskPath.CGPath;
    
    
    weightPlus.layer.mask = WPmaskLayer;
    weightMinus.layer.mask = WMmaskLayer;
    repPlus.layer.mask = RPmaskLayer;
    repMinus.layer.mask = RMmaskLayer;
}
-(void) reloadExercise {
    exerciseSets = nil;
  //  NSLog(@"%s", __FUNCTION__);
    
    exerciseSets = [Utilities getSetForExerciseOnDate:exerciseObj.exerciseName date:_date];
   // NSLog(@"count now is %ld", [exerciseSets count]);
    setsLbl.text = [NSString stringWithFormat:@"Sets: %lu/%@", (unsigned long)[exerciseSets count], exerciseObj.setsSuggested];
    [setsTableView reloadData];
    [self updateRepText];
}

-(void) textViewDidChange:(UITextView *)textView {
    [self checkSaveBtn];
}
-(void) checkSaveBtn {
    NSNumberFormatter *f = [[NSNumberFormatter alloc] init];
    f.numberStyle = NSNumberFormatterDecimalStyle;
    NSLog(@"WT: %@, RT: %@, %@",weightText.text, repText.text, [f numberFromString:weightText.text]);

    if ([weightText.text isEqualToString:@""] ||
        [weightText.text isEqualToString:@"0.0"] ||
        [weightText.text isEqualToString:@"0"] ||
        [repText.text isEqualToString:@""] ||
        [repText.text isEqualToString:@"0"] ||
        [repText.text isEqualToString:@"0.0"]) {
        
        saveBtn.backgroundColor = FlatGray;
        saveBtn.enabled = false;
    } else if ([f numberFromString:weightText.text] < 0 || [f numberFromString:repText.text] <= 0) {
        saveBtn.backgroundColor = FlatGray;
        saveBtn.enabled = false;
    }else {
        saveBtn.backgroundColor = FlatMint;
        saveBtn.enabled = true;
    }
}

-(void)timerLabel:(MZTimerLabel*)timerLabel finshedCountDownTimerWithTime:(NSTimeInterval)countTime{
    //time is up, what should I do master?
    if (timerLabel.tag == 101) {
//        timerBWEx.font = [UIFont fontWithName:@HAL_BOLD_FONT size:20];
//        timerBWEx.text = @"Times Up. Lets LIFT.";
//        [timerBWEx reset];
//        [self loadNextExercise:self];
    } else if (timerLabel.tag == 100) {
        NSLog(@"in here...");
        AudioServicesPlaySystemSound(1005);
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
        [self.popupController dismissPopupControllerAnimated:YES];
       // timer.hidden = YES;
        [timer pause];
        [timer reset];
    }

}
-(void) didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(IBAction)saveWorkout:(id)sender {
    
    if (updateField == false) {
        NSDate *now = [NSDate date];
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"HH:mm:ss";

        [dateFormatter setTimeZone:[NSTimeZone systemTimeZone]];
        

        ExerciseSet *set = [ExerciseSet MR_createEntityInContext:localContext];
        set.exerciseName  = exerciseObj.exerciseName;
        set.muscle = exerciseObj.muscle;
        set.majorMuscle = exerciseObj.majorMuscle;
        set.date = _date;
        set.rep = [NSNumber numberWithInteger:[repText.text integerValue]];
        set.weight = [NSNumber numberWithFloat:[weightText.text floatValue]];
        set.timeStamp =[dateFormatter stringFromDate:now];
        set.isDropSet = [NSNumber numberWithBool:false];
        set.isPyramidSet = [NSNumber numberWithBool:false];
        set.isSuperSet = [NSNumber numberWithBool:false];
        set.routineName = _routineName;
        set.workoutName = _workoutName;
        set.exerciseNumber = _exerciseNumber;
        set.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
        
        if ([set.weight floatValue] > maxWeight) {
            exMetaInfo.maxWeight = set.weight;
            exMetaInfo.maxRep = set.rep;
            exMetaInfo.maxDate = _date;
            maxWeight = [set.weight floatValue];
            maxRep = [set.rep intValue];
        } else if ([set.weight floatValue] == maxWeight) {
            if ([set.rep intValue] >= maxRep) {
                exMetaInfo.maxWeight = set.weight;
                exMetaInfo.maxRep = set.rep;
                exMetaInfo.maxDate = _date;
                maxWeight = [set.weight floatValue];
                maxRep = [set.rep intValue];
            }
        }
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                NSLog(@"set saved...");
                [Utilities addExerciseSetToParse:set];
            }
        }];
        [timer setCountDownTime:timerValueToCountDown];
        [timer pause];
        [timer reset];
        [timer start];
        
    } else {
        NSLog(@"updating ***** %d", selectedIndex);
        ExerciseSet *set = [exerciseSets objectAtIndex:selectedIndex];
        set.rep = [NSNumber numberWithInteger:[repText.text integerValue]];
        set.weight = [NSNumber numberWithFloat:[weightText.text floatValue]];

        
        if ([set.weight floatValue] > maxWeight) {
            exMetaInfo.maxWeight = set.weight;
            exMetaInfo.maxRep = set.rep;
            exMetaInfo.maxDate = _date;
            maxWeight = [set.weight floatValue];
            maxRep = [set.rep intValue];
        } else if ([set.weight floatValue] == maxWeight) {
            if ([set.rep intValue] >= maxRep) {
                exMetaInfo.maxWeight = set.weight;
                exMetaInfo.maxRep = set.rep;
                exMetaInfo.maxDate = _date;
                maxWeight = [set.weight floatValue];
                maxRep = [set.rep intValue];
            }
        }
        
        [localContext MR_saveToPersistentStoreWithCompletion:^(BOOL contextDidSave, NSError *error) {
            if (!error) {
                NSLog(@"set updated...");
                [Utilities updateExerciseSetToParse:set];
            }
        }];

        [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
        saveBtn.titleLabel.textColor = [UIColor whiteColor];
    //    selectedIndex = -1;
        NSIndexPath *path = [NSIndexPath indexPathWithIndex:selectedIndex];
        [self.setsTableView deselectRowAtIndexPath:path animated:YES];
    }
    
    [self reloadExercise];
    
    if ([exerciseSets count] == [exerciseObj.setsSuggested intValue]) {
        [timer pause];
        [timer reset];
        [self showNextExerciseMenuWithCountDownTimer:CNPPopupStyleCentered];
        [self scheduleNotificationAfterSec:[Utilities getDefaultRestTimerValueBetweenExercises]];
    } else {
        if (updateField == false) {
            [self scheduleNotificationAfterSec:timerValueToCountDown];
            [self showRestTimerBetweenSets:CNPPopupStyleActionSheet];
        }
    }
    
    //once we save everything.. updateField should be false when we exit from here.
    updateField = false;
    
    // updating rep text field to show correct number of reps required for next set.
    
}
-(void) updateRepText {
    NSLog(@"updating rep text now... %d", (int)exerciseSets.count);
    NSArray *repsInSet = [exerciseObj.repsPerSet componentsSeparatedByString:@","];
    if (exerciseSets.count < repsInSet.count) {
        if ([[repsInSet objectAtIndex:exerciseSets.count + 1] isEqualToString:@"MAX"]) {
            repText.text = [NSString stringWithFormat:@"20"];
        } else {
            repText.text = [NSString stringWithFormat:@"%d", [[repsInSet objectAtIndex:exerciseSets.count + 1] intValue]];
        }
    }
}
-(IBAction)weightInc:(id)sender {
    NSLog(@"%s", __func__);
    if ([Utilities isIncBy25Enabled])
        weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] + 2.5];
    else
        weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] + 5];
    
    [self checkSaveBtn];
}
-(IBAction)weightDec:(id)sender {
    int newValue = 0;
        NSLog(@"%s", __func__);
    if ([Utilities isIncBy25Enabled]) {
        newValue = [weightText.text intValue] - 2.5;
        if (newValue >= 0)
            weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] - 2.5];
    }
    else {
        newValue = [weightText.text intValue] - 5;
        if (newValue >= 0)
            weightText.text =  [NSString stringWithFormat:@"%.1f", [weightText.text floatValue] - 5];
    }
    
    
    [self checkSaveBtn];
}
-(IBAction)repInc:(id)sender {
    repText.text =  [NSString stringWithFormat:@"%d", [repText.text intValue] + 1];
    [self checkSaveBtn];
}
-(IBAction)repDec:(id)sender {
    int newValue = [repText.text intValue] - 1;
    if (newValue >= 0)
        repText.text =  [NSString stringWithFormat:@"%d", [repText.text intValue] - 1];
    
    [self checkSaveBtn];
}
-(IBAction)clearWorkout:(id)sender {

    [timer pause];
    [timer reset];
    weightText.text = @"";
    repText.text = @"";
    if (isDropSetVisible == true) {
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            
        } completion:^(BOOL finished) {
            isDropSetVisible = false;
        }];
    }
    NSLog(@"check and see if update is active %d", updateField);
    if (updateField == true) {
        updateField = false;
        [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
    }
    [self.setsTableView reloadData];
    [self checkSaveBtn];
}
-(IBAction)seatSetting:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Seat height recording is not available in Free Version."];
    } else {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Seat Height"];
        [dialog setMessage:@" "];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 1;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
    }
}
-(IBAction) handSetting:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Hand angle recording is not available in Free Version."];
    } else {
        UIAlertView* dialog = [[UIAlertView alloc] init];
        [dialog setDelegate:self];
        [dialog setTitle:@"Hand Angle"];
        [dialog setMessage:@" "];
        [dialog addButtonWithTitle:@"Cancel"];
        [dialog addButtonWithTitle:@"OK"];
        dialog.tag = 2;
        
        dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
        [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
        
        CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
        [dialog setTransform: moveUp];
        [dialog show];
    }
}
-(IBAction) timerValue:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Modifying rest timer is not available in Free Version."];
    } else {
    UIAlertView* dialog = [[UIAlertView alloc] init];
    [dialog setDelegate:self];
    [dialog setTitle:@"Additional Rest (sec)"];
    [dialog setMessage:@" "];
    [dialog addButtonWithTitle:@"Cancel"];
    [dialog addButtonWithTitle:@"OK"];
    dialog.tag = 3;
    
    dialog.alertViewStyle = UIAlertViewStylePlainTextInput;
    [dialog textFieldAtIndex:0].keyboardType = UIKeyboardTypeNumberPad;
    
    CGAffineTransform moveUp = CGAffineTransformMakeTranslation(0.0, 0.0);
    [dialog setTransform: moveUp];
    [dialog show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSString *test = [[alertView textFieldAtIndex:0] text];
    if ([test isEqualToString:@""]) {
        // do nothing..
        NSLog(@"did not enter anythign..");
    } else
        if (alertView.tag == 1) {
            NSLog(@"seat height is %@", test);
            seatHeight = [test intValue];
            exMetaInfo.seatHeight = [NSNumber numberWithInt:seatHeight];

            //making sure null is not set..
            if (handAngle == 0)
                exMetaInfo.handAngle = [NSNumber numberWithInt:0];

            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        } else if (alertView.tag == 2) {
            NSLog(@"Hand Height us %@", test);
            handAngle = [test intValue];
            
            //making sure null is not set..
            if (seatHeight == 0)
                exMetaInfo.seatHeight = [NSNumber numberWithInt:0];
            
            exMetaInfo.handAngle = [NSNumber numberWithInt:handAngle];
            exMetaInfo.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        } else if (alertView.tag == 3) {
            // updating settings bundle with new timer...
            timerValueToCountDown = [test intValue];
        } else if (alertView.tag == 4) {
//            exMetaInfo.
            // this is where we add comments..
        }
    [self loadMetaInfo];

}


- (void)showRestTimerBetweenSets:(CNPPopupStyle)popupStyle {
    
   
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
    
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Rest" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 2;
        lineOneLabel.attributedText = lineOne;
        lineOneLabel.textColor = FlatGreenDark;

    NSAttributedString *lineTwo = [[NSAttributedString alloc] initWithString:@"Last Week Set" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:20], NSParagraphStyleAttributeName : paragraphStyle}];
    
    UILabel *lineTwoLbl = [[UILabel alloc] init];
    lineTwoLbl.numberOfLines = 2;
    lineTwoLbl.attributedText = lineTwo;
    lineTwoLbl.textColor = FlatGreenDark;

    UILabel *lineTwoVal = [[UILabel alloc] init];
    lineTwoVal.numberOfLines = 2;
    
        timerBWSts = [[MZTimerLabel alloc] initWithFrame:CGRectMake(0, 0, 250,50)];
        timerBWSts.delegate = self;
        [timerBWSts setCountDownTime:timerValueToCountDown];
        timerBWSts.resetTimerAfterFinish = NO; //IMPORTANT, if you needs custom text with finished, please do not set resetTimerAfterFinish to YES.
        timerBWSts.timeFormat = @"m:ss";
        timerBWSts.tag = 110;
        timerBWSts.timeLabel.backgroundColor = [UIColor clearColor];
        timerBWSts.timeLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:60];
        timerBWSts.timeLabel.textColor = [UIColor blackColor];
        timerBWSts.timeLabel.textAlignment = NSTextAlignmentCenter;
        timerBWSts.timerType = MZTimerLabelTypeTimer;
        [timerBWSts start];
        [timer start];
    
//    NSLog(@"current exercise is %@ %lu", exerciseObj, [exerciseSets count]);
//    if ([exerciseSets count] < [previousWorkoutSets count]) {
//        ExerciseSet *set = [previousWorkoutSets objectAtIndex:[exerciseSets count]];
//        NSLog(@"NEXT SET IS : reps %@, weight %@", set.rep, set.weight);
//        lineTwoVal.text = [NSString stringWithFormat:@"%@ x %@", set.rep, set.weight];
//        self.popupController = [[CNPPopupController alloc] initWithContents:@[lineOneLabel, timerBWSts, lineTwoLbl, lineTwoVal]];
//    } else {
        self.popupController = [[CNPPopupController alloc] initWithContents:@[lineOneLabel, timerBWSts]];
//    }
    
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.shouldDismissOnBackgroundTouch = true;
    self.popupController.delegate = self;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromBottom;
    self.popupController.theme.dismissesOppositeDirection = YES;
    [self.popupController presentPopupControllerAnimated:YES];
}

- (void)showNextExerciseMenuWithCountDownTimer:(CNPPopupStyle)popupStyle {
    
    int exNum = [_exerciseNumber intValue];
    exNum++;
    

    if (exNum == [exerciseList count]) {
        NSLog(@"exnum is %d, total: %lu", exNum, (long)[exerciseList count]);
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Congrats!!" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Workout Complete." attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        titleLabel.textColor = FlatGreenDark;
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 2;
        lineOneLabel.attributedText = lineOne;
        lineOneLabel.textColor = FlatGreenDark;
        
        
        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button setTitle:@"Record More Sets" forState:UIControlStateNormal];
        button.backgroundColor = FlatOrangeDark;
        button.layer.cornerRadius = 4;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
        };
        
        CNPPopupButton *nextButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
        [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [nextButton setTitle:@"Finish Workout" forState:UIControlStateNormal];
        nextButton.backgroundColor = FlatMintDark;
        nextButton.layer.cornerRadius = 4;
        nextButton.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
            [self loadNextExercise:self];
        };
        
        
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, button, nextButton]];

    } else {
        
        //int exNumber = [NSNumber numberWithInt:exNum];
        WorkoutList *nextExName = [exerciseList objectAtIndex:exNum];
        
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Next Exercise" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:nextExName.exerciseName attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
        
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        titleLabel.textColor = FlatGreenDark;
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 2;
        lineOneLabel.attributedText = lineOne;
        lineOneLabel.textColor = [Utilities getMajorMuscleColor:exerciseObj.majorMuscle];
        lineOneLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:24];
        
        timerBWEx = [[MZTimerLabel alloc] initWithFrame:CGRectMake(0, 0, 250,50)];
        timerBWEx.delegate = self;
        [timerBWEx setCountDownTime:[Utilities getDefaultRestTimerValueBetweenExercises]];
        timerBWEx.resetTimerAfterFinish = NO; //IMPORTANT, if you needs custom text with finished, please do not set resetTimerAfterFinish to YES.
        timerBWEx.timeFormat = @"m:ss";
        timerBWEx.tag = 101;
        timerBWEx.timeLabel.backgroundColor = [UIColor clearColor];
        timerBWEx.timeLabel.font = [UIFont fontWithName:@HAL_BOLD_FONT size:40];
        timerBWEx.timeLabel.textColor = [UIColor blackColor];
        timerBWEx.timeLabel.textAlignment = NSTextAlignmentCenter;
        timerBWEx.timerType = MZTimerLabelTypeTimer;
        [timerBWEx start];
        
        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button setTitle:@"Record More Sets" forState:UIControlStateNormal];
        button.backgroundColor = FlatOrangeDark;
        button.layer.cornerRadius = 4;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
        };
        
        CNPPopupButton *nextButton = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 250, 50)];
        [nextButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        nextButton.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [nextButton setTitle:@"Let's Go >>" forState:UIControlStateNormal];
        nextButton.backgroundColor = FlatMintDark;
        nextButton.layer.cornerRadius = 4;
        nextButton.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
            [timerBWEx pause];
            [timerBWEx reset];
//            [Utilities syncExerciseSetToParse:_date];
            [self loadNextExercise:self];
        };
        
        
        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, lineOneLabel, timerBWEx, button, nextButton]];
    }
//    self.popupController.theme.backgroundColor = FlatOrange;
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.theme.shouldDismissOnBackgroundTouch = false;
    self.popupController.delegate = self;
    self.popupController.theme.presentationStyle = CNPPopupPresentationStyleSlideInFromRight;
    self.popupController.theme.dismissesOppositeDirection = YES;
    
    [self.popupController presentPopupControllerAnimated:YES];
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == myBarbelTableView) {
        if (lbORkg == 0)
            return [weightsLbs count];
        else
            return [weightsKgs count];
    } else
       return [exerciseSets count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == myBarbelTableView) {
        static NSString *CellIdentifier = @"newFriendCell";
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
        }
        
        for (UIView *lbl in cell.contentView.subviews) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            }
        }
        
        UILabel *weightName = [[UILabel alloc] initWithFrame:CGRectMake(5, 5, CGRectGetWidth(tableView.frame)/3, 30)];
        UILabel *val = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightName.frame), 5, CGRectGetWidth(tableView.frame)/3, 30)];
        UIStepper *stepper = [[UIStepper alloc] initWithFrame:CGRectMake(CGRectGetMaxX(val.frame), 5, CGRectGetWidth(tableView.frame)/3, 30)];
        [stepper addTarget:self action:@selector(stepperValueDidChanged:) forControlEvents:UIControlEventValueChanged];
        
        if (lbORkg == 0) {
            weightName.text = [weightsLbs objectAtIndex:indexPath.row];
            val.text = [NSString stringWithFormat:@"%@", [weightLbsArray objectAtIndex:indexPath.row]];
        }
        else {
            weightName.text = [weightsKgs objectAtIndex:indexPath.row];
            val.text = [NSString stringWithFormat:@"%@", [weightKgsArray objectAtIndex:indexPath.row]];
        }
        
        weightName.tag = 2;
        val.tag = 1;
        stepper.value = 0;
        weightName.textAlignment =     val.textAlignment = NSTextAlignmentCenter;
        
        [stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateNormal];
        //[stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateHighlighted];
        [stepper setBackgroundImage:self.emptyImageForSteper forState:UIControlStateDisabled];
        [stepper setDividerImage:self.emptyImageForSteper forLeftSegmentState:UIControlStateNormal rightSegmentState:UIControlStateNormal];
        stepper.tintColor = [UIColor blackColor];
        cell.backgroundColor = FlatWhiteDark;
        
        [cell.contentView addSubview:weightName];
        [cell.contentView addSubview:val];
        [cell.contentView addSubview:stepper];
        return cell;
        
    } else {
        MGSwipeTableCell *cell = [self.setsTableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
        
        float cellWidth = CGRectGetWidth(cell.frame);
        float smallCellWidth = ((cellWidth * 4)/6 - 50)/3;
        float bigCellWidth = (cellWidth - smallCellWidth*3)/2;
        
        UILabel *count = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, smallCellWidth, cell.frame.size.height)];
        UILabel *weight = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(count.frame), 0, bigCellWidth, cell.frame.size.height)];
        UILabel *weightLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weight.frame), 0, smallCellWidth, cell.frame.size.height)];
        UILabel *rep = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(weightLbl.frame), 0, bigCellWidth, cell.frame.size.height)];
        UILabel *repLbl = [[UILabel alloc] initWithFrame:CGRectMake(CGRectGetMaxX(rep.frame), 0, smallCellWidth, cell.frame.size.height)];
        
        for (UILabel *lbl in [cell.contentView subviews]) {
            if ([lbl isKindOfClass:[UILabel class]]) {
                [lbl removeFromSuperview];
            } else {
                //            NSLog(@"catn remove label");
            }
        }
        
        count.textAlignment = NSTextAlignmentCenter;
        weight.textAlignment = NSTextAlignmentRight;
        weightLbl.textAlignment = NSTextAlignmentLeft;
        rep.textAlignment = NSTextAlignmentRight;
        repLbl.textAlignment = NSTextAlignmentLeft;
        
        weightLbl.textColor = FlatGray;
        repLbl.textColor = FlatGray;
        
        ExerciseSet *data = [exerciseSets objectAtIndex:indexPath.row];
        count.text = [NSString stringWithFormat:@"%ld", [exerciseSets count] - (long)indexPath.row ];
        weight.text = [data.weight stringValue];
        weightLbl.text = [NSString stringWithFormat:@" %@",[Utilities getUnits]];
        rep.text = [data.rep stringValue];
        repLbl.text = @" Reps";
        NSLog(@"is this a drop set %d", [data.isDropSet boolValue]);
        if ([data.isDropSet boolValue]) {
            cell.backgroundColor = FlatMintDark;
            weightLbl.textColor = [UIColor whiteColor];
            repLbl.textColor = [UIColor whiteColor];
            //        cell.leftButtons = @[[MGSwipeButton buttonWithTitle:@"Not DropSet" backgroundColor:FlatMint]];
            //        cell.leftSwipeSettings.transition = MGSwipeTransition3D;
            
        }
        else
            cell.backgroundColor = [UIColor whiteColor];
        //NSLog(@"timestamp %@ %.0f", data.timeStamp, [data.weight floatValue]);
        
        
        //configure right buttons
        cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"Delete" backgroundColor:[UIColor redColor]]];
        cell.rightSwipeSettings.transition = MGSwipeTransition3D;
        cell.delegate = self;
        
        
        [cell.contentView addSubview:count];
        [cell.contentView addSubview:weight];
        [cell.contentView addSubview:weightLbl];
        [cell.contentView addSubview:rep];
        [cell.contentView addSubview:repLbl];
        return cell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    //TODO: remove this when adding search bar...
    return 0.1f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (tableView == myBarbelTableView)
    {
        return 33.0f;
    } else {
        
        NSString *deviceType = [Utilities getIphoneName];
        if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
            return 25;
        }
        return 35;
    }
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == myBarbelTableView) {
        // do nothing
    } else {
        int rowSelected = (int)[[self.setsTableView indexPathsForSelectedRows] count];
        NSLog(@"rows selected %d, Dropset %d", rowSelected, isDropSetVisible);
        if (rowSelected == 1) {
            ExerciseSet *data = [exerciseSets objectAtIndex:indexPath.row];
            weightText.text = [data.weight stringValue];
            repText.text = [data.rep stringValue];
            [saveBtn setTitle:@"Update" forState:UIControlStateNormal];
            updateField = true;
            selectedIndex = (int) indexPath.row;
        } else if (isDropSetVisible == false) {
            additionOptionView = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44)];
            
            [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseIn animations:^{
                
                additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.setsTableView.frame)  - CGRectGetHeight(self.navigationController.tabBarController.tabBar.frame), CGRectGetWidth(self.view.frame), 44);
                //self.headerView.frame  = CGRectMake(0, 5, 320,0);
                
            } completion:^(BOOL finished) {
                
            }];
            
            additionOptionView.backgroundColor = FlatMintDark;
            
            
            unMarkDropSet = [[UIButton alloc] initWithFrame:CGRectMake(10, 5, CGRectGetWidth(_history.frame)/2 - 20, 35)];
            unMarkDropSet.backgroundColor = [UIColor whiteColor];
            unMarkDropSet.layer.cornerRadius = 5.0f;
            [unMarkDropSet setTitle:@"Not Drop Set" forState:UIControlStateNormal];
            [unMarkDropSet setTitleColor:FlatMintDark forState:UIControlStateNormal];
            [unMarkDropSet addTarget:self action:@selector(unMarkDropSet:) forControlEvents:UIControlEventTouchUpInside];
            
            markDropSet = [[UIButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(unMarkDropSet.frame) + 20, 5, CGRectGetWidth(_history.frame)/2 - 20, 35)];
            markDropSet.backgroundColor = [UIColor whiteColor];
            markDropSet.layer.cornerRadius = 5.0f;
            [markDropSet setTitle:@"Drop Set" forState:UIControlStateNormal];
            [markDropSet setTitleColor:FlatMintDark forState:UIControlStateNormal];
            [markDropSet addTarget:self action:@selector(markDropSet:) forControlEvents:UIControlEventTouchUpInside];
            
            [additionOptionView addSubview:unMarkDropSet];
            [additionOptionView addSubview:markDropSet];
            
            [self.view addSubview:additionOptionView];
            
            NSLog(@"may be a dropset...");
            weightText.text = @"";
            repText.text = @"";
            [saveBtn setTitle:@"Save" forState:UIControlStateNormal];
            updateField = false;
            isDropSetVisible = true;
            
        }
    }
}
-(IBAction)markDropSet:(id)sender {
    
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Drop set support is not available in Free Version."];
    } else {
        
        NSLog(@"selected rows are mark drop set are : %@",[self.setsTableView indexPathsForSelectedRows]);
        //[additionOptionView removeFromSuperview];
        
        for (NSIndexPath *index in [self.setsTableView indexPathsForSelectedRows]) {
            NSLog(@"index is %ld", (long)index.row);
            ExerciseSet *set = [exerciseSets objectAtIndex:index.row];
            set.isDropSet = [NSNumber numberWithBool:true];
            set.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            isDropSetVisible = false;
        } completion:^(BOOL finished) {
            [self.setsTableView reloadData];
            [Utilities syncDropSetToParse:_date];
        }];
    }
}

-(IBAction)unMarkDropSet:(id)sender {
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Drop set support is not available in Free Version."];
    } else {
        NSLog(@"selected rows for unmark drop set are: %@",[self.setsTableView indexPathsForSelectedRows]);
        //[additionOptionView removeFromSuperview];
        
        for (NSIndexPath *index in [self.setsTableView indexPathsForSelectedRows]) {
            NSLog(@"index is %ld", (long)index.row);
            ExerciseSet *set = [exerciseSets objectAtIndex:index.row];
            set.isDropSet = [NSNumber numberWithBool:false];
            set.syncedState = [NSNumber numberWithBool:false];
            [localContext MR_saveToPersistentStoreAndWait];
        }
        
        [UIView animateWithDuration:0.5 delay:0.1 options:UIViewAnimationOptionCurveEaseIn animations:^{
            
            additionOptionView.frame = CGRectMake(0, CGRectGetMaxY(self.view.frame), CGRectGetWidth(self.view.frame), 44);
            //self.headerView.frame  = CGRectMake(0, 5, 320,0);
            isDropSetVisible = false;
        } completion:^(BOOL finished) {
            [self.setsTableView reloadData];
            [Utilities syncDropSetToParse:_date];
        }];
    }
    
}
-(void) showPurchasePopUpHistory {
    [self dismissKeyboard];
    
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY WORKOUT PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_WORKOUT_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"Last workout information if not available in Free version. Please upgrade to Premium Package or Workout Package."];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
    
}

-(void) showPurchasePopUp: (NSString *) feature {
    [self dismissKeyboard];
    
    SCLAlertView *error = [[SCLAlertView alloc] init];
    [error addButton:@"BUY PREMIUM PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_PREMIUM_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    [error addButton:@"BUY WORKOUT PACK" actionBlock:^{
        SKMutablePayment *payment = [[SKMutablePayment alloc] init] ;
        payment.productIdentifier = @IAP_WORKOUT_PACKAGE_ID;
        [[SKPaymentQueue defaultQueue] addPayment:payment];
    }];
    
    NSString *reason = [NSString stringWithFormat:@"%@ Please upgrade to Premium Package or Workout Package.", feature];
    [error showCustom:self.parentViewController image:[UIImage imageNamed:@"IconPremium"] color:FlatMintDark title:@"PAID FEATURE" subTitle:reason closeButtonTitle:nil duration:0.0f]; // Custom
    
    error.shouldDismissOnTapOutside = YES;
}
// Override to support conditional editing of the table view.
// This only needs to be implemented if you are going to be returning NO
// for some items. By default, all items are editable.

#pragma mark Swipe Delegate

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell canSwipe:(MGSwipeDirection) direction;
{
    return YES;
}

-(NSArray*) swipeTableCell:(MGSwipeTableCell*) cell swipeButtonsForDirection:(MGSwipeDirection)direction
             swipeSettings:(MGSwipeSettings*) swipeSettings expansionSettings:(MGSwipeExpansionSettings*) expansionSettings
{
    
    NSLog(@"was able to successfully swipe... ");
    return nil;
    
}

-(void) swipeTableCell:(MGSwipeTableCell*) cell didChangeSwipeState:(MGSwipeState)state gestureIsActive:(BOOL)gestureIsActive
{
    NSString * str;
    switch (state) {
        case MGSwipeStateNone: str = @"None"; break;
        case MGSwipeStateSwippingLeftToRight: str = @"SwippingLeftToRight"; break;
        case MGSwipeStateSwippingRightToLeft: str = @"SwippingRightToLeft"; break;
        case MGSwipeStateExpandingLeftToRight: str = @"ExpandingLeftToRight"; break;
        case MGSwipeStateExpandingRightToLeft: str = @"ExpandingRightToLeft"; break;
    }
    NSLog(@"Swipe state: %@ ::: Gesture: %@", str, gestureIsActive ? @"Active" : @"Ended");
}

-(BOOL) swipeTableCell:(MGSwipeTableCell*) cell tappedButtonAtIndex:(NSInteger) index direction:(MGSwipeDirection)direction fromExpansion:(BOOL) fromExpansion
{
    NSLog(@"Delegate: button tapped, %@ position, index %d, from Expansion: %@",
          direction == MGSwipeDirectionLeftToRight ? @"left" : @"right", (int)index, fromExpansion ? @"YES" : @"NO");
        NSIndexPath * path = [self.setsTableView indexPathForCell:cell];
    if (direction == MGSwipeDirectionRightToLeft && index == 0) {
        //delete button
        NSLog(@"delete pressed");

        
        SCLAlertView *alert = [[SCLAlertView alloc] init];
        
        [alert addButton:@"Yes" actionBlock:^{
            ExerciseSet *data = [exerciseSets objectAtIndex:path.row];
            [Utilities deleteExerciseSetToParse:data.date timeStamp:data.timeStamp];
            [data MR_deleteEntityInContext:localContext];
            [localContext MR_saveToPersistentStoreAndWait];

            [self reloadExercise];
            
        }];
        [alert showWarning:self title:@"Delete entry?" subTitle:@"Are you sure you want to delete this set?" closeButtonTitle:@"No" duration:0.0f];
        return NO; //Don't autohide to improve delete expansion animation
    } else if (direction == MGSwipeDirectionLeftToRight && index == 0) {
                /* // this is not called anymore...
        NSLog(@"not a drop set pressed");
        ExerciseSet *data = [exerciseSets objectAtIndex:path.row];
        data.isDropSet = [NSNumber numberWithBool:false];
        data.syncedState = [NSNumber numberWithBool:SYNC_NOT_DONE];
        [localContext MR_saveToPersistentStoreAndWait];
        [self reloadExercise];
        [Utilities updateExerciseSetToParse:data];
        return NO;
         */
    }
    
    return YES;
}

-(IBAction)viewHistory:(id)sender {
    [self performSegueWithIdentifier:@"exerciseHistory" sender:self];
}


#pragma mark - DZNEmptyDataSetSource Methods

- (NSAttributedString *)titleForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text;
    text = [NSString stringWithFormat:@"No set performed on %@.", _date];
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:17.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (NSAttributedString *)descriptionForEmptyDataSet:(UIScrollView *)scrollView
{
    NSString *text = @"Record a set by entering \nweight, reps and then clicking \"save\" ";
    
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont systemFontOfSize:15.0],
                                 NSForegroundColorAttributeName: [UIColor colorWithRed:170/255.0 green:171/255.0 blue:179/255.0 alpha:1.0],
                                 NSParagraphStyleAttributeName: paragraphStyle};
    
    return [[NSMutableAttributedString alloc] initWithString:text attributes:attributes];
}

- (UIImage *)imageForEmptyDataSet:(UIScrollView *)scrollView
{
    
    return [Utilities getMuscleImage:exerciseObj.majorMuscle];
}

- (UIColor *)backgroundColorForEmptyDataSet:(UIScrollView *)scrollView
{
    return [UIColor whiteColor];
}

- (UIView *)customViewForEmptyDataSet:(UIScrollView *)scrollView
{
    return nil;
}

- (CGFloat)spaceHeightForEmptyDataSet:(UIScrollView *)scrollView
{
    return 0;
}


#pragma mark - DZNEmptyDataSetSource Methods

- (BOOL)emptyDataSetShouldAllowTouch:(UIScrollView *)scrollView
{
    return YES;
}

- (BOOL)emptyDataSetShouldAllowScroll:(UIScrollView *)scrollView
{
    return NO;
}

- (void)emptyDataSetDidTapView:(UIScrollView *)scrollView
{
    NSLog(@"%s",__FUNCTION__);
}

-(void) showWarmUpMessage:(CNPPopupStyle ) popupStyle {
    
    NSArray *allExerciseSet = [Utilities getSetsForAllExerciseForDateSortedByTime:_date];

    if (warmupFlag == false && [allExerciseSet count] == 0) {
        NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
        paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
        paragraphStyle.alignment = NSTextAlignmentCenter;
        
        NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"WARM UP" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : paragraphStyle}];
        NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Before you start, perform a full body warmup for atleast 5 minutes. It increases blood flow to the muscles, prepare them for upcoming hard work and prevent injuries." attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_THIN_FONT size:18], NSParagraphStyleAttributeName : paragraphStyle}];
        
        CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
        [button setTitle:@"CLOSE" forState:UIControlStateNormal];
        button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
        button.layer.cornerRadius = 4;
        button.selectionHandler = ^(CNPPopupButton *button){
            [self.popupController dismissPopupControllerAnimated:YES];
            NSLog(@"Block for button: %@", button.titleLabel.text);
            warmupFlag = true;
        };
        
        UILabel *titleLabel = [[UILabel alloc] init];
        titleLabel.numberOfLines = 0;
        titleLabel.attributedText = title;
        
        UILabel *lineOneLabel = [[UILabel alloc] init];
        lineOneLabel.numberOfLines = 0;
        lineOneLabel.attributedText = lineOne;
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconExWarmUp"]];

        self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, imageView, lineOneLabel,  button]];
        self.popupController.theme = [CNPPopupTheme defaultTheme];
        self.popupController.theme.popupStyle = popupStyle;
        self.popupController.delegate = self;
        [self.popupController presentPopupControllerAnimated:YES];
    }
}


-(void) showCoolDownMessage:(CNPPopupStyle) popupStyle {
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"COOL DOWN" attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_BOLD_FONT size:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Great workout. Make sure to stretch properly. It helps muscle relax after the hard work, increase blood flow, prevent soreness and injuries." attributes:@{NSFontAttributeName : [UIFont fontWithName:@HAL_THIN_FONT size:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(0, 0, 200, 60)];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    [button setTitle:@"CLOSE" forState:UIControlStateNormal];
    button.backgroundColor = [UIColor colorWithRed:0.46 green:0.8 blue:1.0 alpha:1.0];
    button.layer.cornerRadius = 4;
    button.selectionHandler = ^(CNPPopupButton *button){
        [self.popupController dismissPopupControllerAnimated:YES];
        NSLog(@"Block for button: %@", button.titleLabel.text);
    };
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"IconExCoolDown"]];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.attributedText = title;
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[titleLabel, imageView, lineOneLabel, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = popupStyle;
    self.popupController.delegate = self;
    [self.popupController presentPopupControllerAnimated:YES];
}
- (void)showPopupWithStyle:(CNPPopupStyle)popupStyle {    
    [self performSegueWithIdentifier:@"showSummarySegue" sender:self];
    return;
}

-(void) shareButtonHighlight:(UIButton *) sender {
    sender.backgroundColor = FlatMintDark;
}

-(void)shareBtnPressed:(UIButton *)sender {
    if (sender.tag == 0) {
        // like btn
    } else if (sender.tag == 1) {
        // twitter btn
    } else if (sender.tag == 2) {
        // instagram btn
    } else if (sender.tag == 3) {
        // facebook share btn
    }
    NSLog(@"share btn pressed with tag %ld", (long)sender.tag);
    sender.backgroundColor = FlatMint;
}

/**
Takes a screenshot of a UIView at a specific point and size, as denoted by
the provided croppingRect parameter. Returns a UIImageView of this cropped
region.

CREDIT: This is based on @escrafford's answer at http://stackoverflow.com/a/15304222/535054
*/
- (UIImageView *)rp_screenshotImageViewWithCroppingRect:(CGRect)croppingRect {
    // For dealing with Retina displays as well as non-Retina, we need to check
    // the scale factor, if it is available. Note that we use the size of teh cropping Rect
    // passed in, and not the size of the view we are taking a screenshot of.
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(croppingRect.size, YES, [UIScreen mainScreen].scale);
    } else {
        UIGraphicsBeginImageContext(croppingRect.size);
    }
    
    // Create a graphics context and translate it the view we want to crop so
    // that even in grabbing (0,0), that origin point now represents the actual
    // cropping origin desired:
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(ctx, -croppingRect.origin.x, -croppingRect.origin.y);
    [self.view.layer renderInContext:ctx];
    
    // Retrieve a UIImage from the current image context:
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // Return the image in a UIImageView:
    return [[UIImageView alloc] initWithImage:snapshotImage];
}

#pragma mark - CNPPopupController Delegate

- (void)popupController:(CNPPopupController *)controller didDismissWithButtonTitle:(NSString *)title {
    //NSLog(@"Dismissed with button title: %@", title);
    if (controller == _barbelPopupController) {
        // do nothing
    } else {
        [timerBWSts pause];
        [timerBWSts reset];
    }
}

- (void)popupControllerDidPresent:(CNPPopupController *)controller {
    NSLog(@"Popup controller presented.");
}

// this method is called form story board link. Dont know why I did that way. n00b mistake. Leave it like that for now...

-(IBAction) quickAddExercise:(id)sender {
    if (addTempExOpen == false) {
        
        SCLAlertView *addExAlert = [[SCLAlertView alloc] init];
        
        [addExAlert addButton:@"Add To Today Workout" actionBlock:^{
            if ([Utilities showWorkoutPackage]) {
                addTempExOpen = false;
                [self showPurchasePopUp:@"Addition exercises cannot be added to a workout in Free Version."];
            } else {
                [self performSegueWithIdentifier:@"addTempExerciseSegue" sender:self];
            }
        }];
        
        [addExAlert addButton:@"Close" actionBlock:^{
            addTempExOpen = false;
        }];
        [addExAlert showInfo:self title:@"Additional Exercise" subTitle:@"Additional exercise will be added to today workout only and not the routine." closeButtonTitle:nil duration:0.0f];
        addExAlert.shouldDismissOnTapOutside = false;
        addExAlert.backgroundType = Blur;
        addTempExOpen = true;
    }

}
// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"exerciseHistory"]) {
        ExerciseHistoryTVC *destVC = segue.destinationViewController;
        destVC.exerciseObj = exerciseObj.exerciseName;
    } else if ([segue.identifier isEqualToString:@"showSummarySegue"]) {
        ExerciseSummary *destVC = segue.destinationViewController;
        destVC.date = _date;
        destVC.routineName = _routineName;
        destVC.workoutName = _workoutName;
    } else if ([segue.identifier isEqualToString:@"addTempExerciseSegue"]) {
        MuscleListTVC *dest = segue.destinationViewController;
        dest.date = _date;
        dest.routineName = _routineName;
        dest.workoutName = _workoutName;
        dest.isTempEx = true;
    }
    
}

-(void) scheduleNotificationAfterSec:(int) seconds {
    
    // we need to do this to make sure all previous ones are cleared.
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    // before we create any new ones, cancel all existing notifications
    //[[UIApplication sharedApplication]cancelAllLocalNotifications];
    [Utilities deleteLocalNotificationWithBody:@REST_TIMER_OVER_TEXT];
    
    if ([[[UIApplication sharedApplication] currentUserNotificationSettings] types] != UIUserNotificationTypeNone) {
        NSLog(@"***** notification scheduled.. **** ");
        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
        
        localNotification.fireDate = [[NSDate date] dateByAddingSeconds:timerValueToCountDown];
        localNotification.alertAction = NSLocalizedString(@"RestTimer", @"RestTimer");
        localNotification.alertBody = @REST_TIMER_OVER_TEXT;
        localNotification.timeZone = [NSTimeZone defaultTimeZone];
        localNotification.soundName = UILocalNotificationDefaultSoundName;

        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
        // this will fire the notification right away, it will still also fire at the date we set
        //        [[UIApplication sharedApplication] presentLocalNotificationNow:localNotification];
        
    }
}

-(void) showWeightMenu:(CNPPopupPresentationStyle *) popupStyle {
 // this will be added in phase 2..
}

#pragma mark barbellcalculator
-(void)doneWithNumberPad{
    NSLog(@"show additional keyboard based on option..");
    [weightText resignFirstResponder];
    if ([Utilities showWorkoutPackage]) {
        [self showPurchasePopUp:@"Plate calculator is not available in Free verion."];
    } else {
        [self showBarbelPopup];
    }
}

-(void) viewDidDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SELECTED_RADIO_BUTTON_CHANGED object:barbellGroup];
}
-(void) showBarbelPopup {
    
    NSDictionary *articleParams = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"UserID", [PFUser currentUser].objectId,
                                   nil];
    [Flurry logEvent:@"PlateCalculator" withParameters:articleParams];
    
    weightLbsArray = [[NSMutableArray alloc] initWithObjects:[NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      [NSNumber numberWithFloat:0],
                      nil];

    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title;
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"Select plates on one side only." attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:12], NSParagraphStyleAttributeName : paragraphStyle}];
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    if (lbORkg == 0)
        title = [[NSAttributedString alloc] initWithString:@"Barbell Weight (Lbs)" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    else
        title = [[NSAttributedString alloc] initWithString:@"Barbell Weight (Kgs)" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];

    UILabel *titleLabel = [[UILabel alloc] init];
    titleLabel.numberOfLines = 0;
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.attributedText = title;
    
    NSString *deviceType = [Utilities getIphoneName];
    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        myBarbelTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, 300, 100) style:UITableViewStylePlain];
    } else {
        myBarbelTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 100, 350, 150) style:UITableViewStylePlain];
    }
    myBarbelTableView.delegate = self;
    myBarbelTableView.dataSource = self;
    myBarbelTableView.tag = 501;
    myBarbelTableView.backgroundColor = FlatWhiteDark;
    
    TNRectangularRadioButtonData *bbZ = [[TNRectangularRadioButtonData alloc] init];
    bbZ.selected = NO;
    bbZ.borderColor = [UIColor blackColor];
    bbZ.rectangleColor = [UIColor blackColor];
    bbZ.borderWidth = bbZ.borderHeight = 12;
    bbZ.rectangleWidth = bbZ.rectangleHeight = 5;
    
    
    TNRectangularRadioButtonData *bbSm = [[TNRectangularRadioButtonData alloc] init];
    bbSm.selected = NO;
    bbSm.borderColor = [UIColor blackColor];
    bbSm.rectangleColor = [UIColor blackColor];
    bbSm.borderWidth = bbSm.borderHeight = 12;
    bbSm.rectangleWidth = bbSm.rectangleHeight = 5;
    
    
    TNRectangularRadioButtonData *bbMd = [[TNRectangularRadioButtonData alloc] init];
    bbMd.selected = NO;
    bbMd.borderColor = [UIColor blackColor];
    bbMd.rectangleColor = [UIColor blackColor];
    bbMd.borderWidth = bbMd.borderHeight = 12;
    bbMd.rectangleWidth = bbMd.rectangleHeight = 5;
    
    TNRectangularRadioButtonData *bbLg = [[TNRectangularRadioButtonData alloc] init];
    bbLg.selected = YES;
    bbLg.borderColor = [UIColor blackColor];
    bbLg.rectangleColor = [UIColor blackColor];
    bbLg.borderWidth = bbLg.borderHeight = 12;
    bbLg.rectangleWidth = bbLg.rectangleHeight = 5;
    
    if (lbORkg == 0) {
        bbZ.labelText = @"0 (BW)";
        bbZ.identifier = @"0";

        bbSm.labelText = @"25";
        bbSm.identifier = @"25";
        
        bbMd.labelText = @"35";
        bbMd.identifier = @"35";
        
        bbLg.labelText = @"45";
        bbLg.identifier = @"45";
        
        totalWeight = barbellWeight = 45;
    } else {
        bbZ.labelText = @"0 (BW)";
        bbZ.identifier = @"0";

        bbSm.labelText = @"10";
        bbSm.identifier = @"10";
        
        bbMd.labelText = @"15";
        bbMd.identifier = @"15";
        
        bbLg.labelText = @"20";
        bbLg.identifier = @"20";
        
        totalWeight = barbellWeight = 20;
        
    }
    
    barbellGroup = [[TNRadioButtonGroup alloc] initWithRadioButtonData:@[bbZ, bbSm, bbMd, bbLg] layout:TNRadioButtonGroupLayoutHorizontal];
    barbellGroup.identifier = @"Barbell";
    barbellGroup.position = CGPointMake(CGRectGetMidX(self.view.frame), 0);

    [barbellGroup create];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(babellGroupUpdated:) name:SELECTED_RADIO_BUTTON_CHANGED object:barbellGroup];
    
    UIView *topView;

    if ([deviceType containsString:@"iPhone 4"] || [deviceType containsString:@"iPhone 4S"] || [deviceType containsString:@"iPad"]) {
        topView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 300, 30)];
    } else {
       topView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, 350, 30)];
    }
    //topView.backgroundColor = FlatWhite;
    
    calculatedWeightLbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(topView.frame)/2, 30)];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", barbellWeight];
    
    CNPPopupButton *button = [[CNPPopupButton alloc] initWithFrame:CGRectMake(CGRectGetMaxX(calculatedWeightLbl.frame), 0, CGRectGetWidth(topView.frame)/2, 30)];
    [button setTitleColor:FlatBlueDark forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
    button.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;

    
    [button setTitle:@"DONE" forState:UIControlStateNormal];
    button.selectionHandler = ^(CNPPopupButton *button){
        [self checkSaveBtn];
        [self.barbelPopupController dismissPopupControllerAnimated:YES];
    };
    
    [topView addSubview:calculatedWeightLbl];
    [topView addSubview:button];
    
    self.barbelPopupController = [[CNPPopupController alloc] initWithContents:@[topView, titleLabel, lineOneLabel, barbellGroup, myBarbelTableView]];//, lineOneLabel, imageView, lineTwoLabel, customView, button]];
    self.barbelPopupController.theme = [CNPPopupTheme defaultTheme];
    self.barbelPopupController.theme.popupStyle = CNPPopupStyleActionSheet;
    self.barbelPopupController.delegate = self;
    self.barbelPopupController.theme.maskType = CNPPopupMaskTypeClear;
    self.barbelPopupController.theme.popupContentInsets = UIEdgeInsetsMake(5, 5, 5, 5);
    self.barbelPopupController.theme.contentVerticalPadding = 10;
    self.barbelPopupController.theme.backgroundColor = FlatWhiteDark;
    [self.barbelPopupController presentPopupControllerAnimated:YES];
}

- (IBAction)stepperValueDidChanged:(UIStepper *)sender {
    UITableViewCell *cell = (UITableViewCell *)[[sender superview] superview];
    // assuming your view controller is a subclass of UITableViewController, for example.
    NSIndexPath *indexPath = [myBarbelTableView indexPathForCell:cell];
    
    [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:@""];
    
    totalWeight = 0;
    
    float weightIndexLbVal;
    if (lbORkg == 0) {
        weightIndexLbVal = [[weightLbsArray objectAtIndex:indexPath.row] floatValue];
        if ((int)[sender value] > weightIndexLbVal) {
            // increment clicked..
            NSLog(@"increment clicked");
            [weightLbsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal + 1]];
        } else if ((int)[sender value] < weightIndexLbVal) {
            // decrement clicked
            NSLog(@"Decrement clicked");
            [weightLbsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal - 1]];
        }
        
        
        [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:[NSString stringWithFormat:@"%d", (int) [sender value]]];
        
        for (int i = 0; i < [weightLbsArray count]; i++) {
            totalWeight += [[weightLbVal objectAtIndex:i] floatValue] * [[weightLbsArray objectAtIndex:i] floatValue] * 2;
        }
    } else {
        weightIndexLbVal = [[weightKgsArray objectAtIndex:indexPath.row] floatValue];
        if ((int)[sender value] > weightIndexLbVal) {
            // increment clicked..
            NSLog(@"increment clicked");
            [weightKgsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal + 1]];
        } else if ((int)[sender value] < weightIndexLbVal) {
            // decrement clicked
            NSLog(@"Decrement clicked");
            [weightKgsArray replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithFloat:weightIndexLbVal - 1]];
        }
        
        
        [(UILabel*)[(UITableViewCell *)sender.superview viewWithTag:1] setText:[NSString stringWithFormat:@"%d", (int) [sender value]]];
        
        for (int i = 0; i < [weightKgsArray count]; i++) {
            totalWeight += [[weightKgVal objectAtIndex:i] floatValue] * [[weightKgsArray objectAtIndex:i] floatValue] * 2;
        }
    }
    
    totalWeight += barbellWeight;
    weightText.text = [NSString stringWithFormat:@"%.1f", totalWeight];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", totalWeight];
}
- (void)babellGroupUpdated:(NSNotification *)notification {
    NSLog(@"[MainView] barbell group updated to %@", barbellGroup.selectedRadioButton.data.identifier);
    totalWeight -= barbellWeight;
    
    if (lbORkg == 0) {
        if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"0"])
            barbellWeight = 0;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"25"])
            barbellWeight = 25;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"35"])
            barbellWeight = 35;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"45"])
            barbellWeight = 45;
    } else {
        if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"0"])
            barbellWeight = 0;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"10"])
            barbellWeight = 10;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"15"])
            barbellWeight = 15;
        else if ([barbellGroup.selectedRadioButton.data.identifier isEqualToString:@"20"])
            barbellWeight = 20;
    }
    
    totalWeight += barbellWeight;
    weightText.text = [NSString stringWithFormat:@"%.1f", totalWeight];
    calculatedWeightLbl.text = [NSString stringWithFormat:@"Total: %.1f", totalWeight];

}
- (UIImage *)emptyImageForSteper
{
    UIGraphicsBeginImageContext(CGSizeMake(1, 1));
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, [UIColor clearColor].CGColor);
    CGContextFillRect(context, CGRectMake(0, 0, 1, 1));
    UIImage * image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

#pragma mark dumbbell collectionview
/*
-(void) showDumbbellMenu {
    
    NSMutableParagraphStyle *paragraphStyle = NSMutableParagraphStyle.new;
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSAttributedString *title = [[NSAttributedString alloc] initWithString:@"Select Dumbbell Weight" attributes:@{NSFontAttributeName : [UIFont boldSystemFontOfSize:24], NSParagraphStyleAttributeName : paragraphStyle}];
    NSAttributedString *lineOne = [[NSAttributedString alloc] initWithString:@"You can add text and images" attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:18], NSParagraphStyleAttributeName : paragraphStyle}];
    
    
    dumbbellMenuWeights = [[NSMutableArray alloc] init];
    for (float i = 1; i < 81; i++) {
        [dumbbellMenuWeights addObject:[NSNumber numberWithFloat:(i * 2.5f)]];
    }
    
    UILabel *lineOneLabel = [[UILabel alloc] init];
    lineOneLabel.numberOfLines = 0;
    lineOneLabel.attributedText = lineOne;
    
    UICollectionViewFlowLayout *layout=[[UICollectionViewFlowLayout alloc] init];
    myCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 5, 300, CGRectGetHeight(self.view.frame)/2) collectionViewLayout:layout];
    myCollectionView.delegate = self;
    myCollectionView.dataSource = self;
    myCollectionView.backgroundColor = [UIColor whiteColor];
    [myCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"newFriendCell"];
    
    self.popupController = [[CNPPopupController alloc] initWithContents:@[myCollectionView]];//, lineOneLabel, imageView, lineTwoLabel, customView, button]];
    self.popupController.theme = [CNPPopupTheme defaultTheme];
    self.popupController.theme.popupStyle = CNPPopupStyleActionSheet;
    //    self.popupController.theme.backgroundColor = [UIColor redColor];
    self.popupController.delegate = self;
    self.popupController.theme.maskType = CNPPopupMaskTypeClear;
    self.popupController.theme.maxPopupWidth = 0;
    [self.popupController presentPopupControllerAnimated:YES];
    
}
 */
#pragma mark collectionview
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [dumbbellMenuWeights count];
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"newFriendCell";
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:CellIdentifier forIndexPath:indexPath];
    
    for (UIView *lbl in cell.contentView.subviews) {
        if ([lbl isKindOfClass:[UILabel class] ]) {
            [lbl removeFromSuperview];
        }
    }
    cell.backgroundColor = [UIColor whiteColor];
    UIImageView *bgImg = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(cell.frame), CGRectGetHeight(cell.frame))];
    [bgImg setImage:[UIImage imageNamed:@"hex1"]];
    
    UILabel *weightName = [[UILabel alloc] initWithFrame:CGRectMake(0, 20, CGRectGetWidth(cell.frame), 30)];
    weightName.text = [NSString stringWithFormat:@"%@", [dumbbellMenuWeights objectAtIndex:indexPath.row]];
    weightName.textAlignment = NSTextAlignmentCenter;
    
    weightName.textColor = [UIColor whiteColor];
    [cell.contentView addSubview:bgImg];
    [cell.contentView addSubview:weightName];
    return  cell;
}


-(void) collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    weightText.text = [NSString stringWithFormat:@"%@", [dumbbellMenuWeights objectAtIndex:indexPath.row]];

    
}

@end
